.class final Lcom/squareup/protos/client/timecards/StartTimecardResponse$ProtoAdapter_StartTimecardResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "StartTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StartTimecardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_StartTimecardResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/timecards/StartTimecardResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 157
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/StartTimecardResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 180
    new-instance v0, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;-><init>()V

    .line 181
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 182
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 189
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 187
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->employee_job_infos:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 186
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->valid(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;

    goto :goto_0

    .line 185
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/timecards/TimecardBreak;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->timecard_break(Lcom/squareup/protos/client/timecards/TimecardBreak;)Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;

    goto :goto_0

    .line 184
    :cond_3
    sget-object v3, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->timecard(Lcom/squareup/protos/client/timecards/Timecard;)Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;

    goto :goto_0

    .line 193
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 194
    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->build()Lcom/squareup/protos/client/timecards/StartTimecardResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 155
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$ProtoAdapter_StartTimecardResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/timecards/StartTimecardResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/StartTimecardResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 171
    sget-object v0, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 172
    sget-object v0, Lcom/squareup/protos/client/timecards/TimecardBreak;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 173
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->valid:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 174
    sget-object v0, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->employee_job_infos:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 175
    invoke-virtual {p2}, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 155
    check-cast p2, Lcom/squareup/protos/client/timecards/StartTimecardResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$ProtoAdapter_StartTimecardResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/timecards/StartTimecardResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/timecards/StartTimecardResponse;)I
    .locals 4

    .line 162
    sget-object v0, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/timecards/TimecardBreak;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    const/4 v3, 0x2

    .line 163
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->valid:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 164
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 165
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->employee_job_infos:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 166
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 155
    check-cast p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$ProtoAdapter_StartTimecardResponse;->encodedSize(Lcom/squareup/protos/client/timecards/StartTimecardResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/timecards/StartTimecardResponse;)Lcom/squareup/protos/client/timecards/StartTimecardResponse;
    .locals 2

    .line 199
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StartTimecardResponse;->newBuilder()Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;

    move-result-object p1

    .line 200
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/timecards/Timecard;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/timecards/Timecard;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 201
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/timecards/TimecardBreak;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/timecards/TimecardBreak;

    iput-object v0, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 202
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->employee_job_infos:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 203
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 204
    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$Builder;->build()Lcom/squareup/protos/client/timecards/StartTimecardResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 155
    check-cast p1, Lcom/squareup/protos/client/timecards/StartTimecardResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/timecards/StartTimecardResponse$ProtoAdapter_StartTimecardResponse;->redact(Lcom/squareup/protos/client/timecards/StartTimecardResponse;)Lcom/squareup/protos/client/timecards/StartTimecardResponse;

    move-result-object p1

    return-object p1
.end method
