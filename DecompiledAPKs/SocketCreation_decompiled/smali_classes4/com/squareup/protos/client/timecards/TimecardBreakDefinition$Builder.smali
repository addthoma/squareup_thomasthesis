.class public final Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TimecardBreakDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
        "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public break_name:Ljava/lang/String;

.field public enabled:Ljava/lang/Boolean;

.field public expected_duration_seconds:Ljava/lang/Integer;

.field public is_paid:Ljava/lang/Boolean;

.field public token:Ljava/lang/String;

.field public updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 176
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public break_name(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->break_name:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;
    .locals 9

    .line 229
    new-instance v8, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->break_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->expected_duration_seconds:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->is_paid:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->enabled:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 163
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->build()Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;

    move-result-object v0

    return-object v0
.end method

.method public enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public expected_duration_seconds(Ljava/lang/Integer;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->expected_duration_seconds:Ljava/lang/Integer;

    return-object p0
.end method

.method public is_paid(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;
    .locals 0

    .line 207
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->is_paid:Ljava/lang/Boolean;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public updated_at_timestamp(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/TimecardBreakDefinition$Builder;->updated_at_timestamp:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
