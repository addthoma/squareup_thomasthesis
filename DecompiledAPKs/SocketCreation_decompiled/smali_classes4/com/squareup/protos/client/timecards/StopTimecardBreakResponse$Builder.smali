.class public final Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StopTimecardBreakResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;",
        "Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public timecard:Lcom/squareup/protos/client/timecards/Timecard;

.field public timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

.field public valid:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;
    .locals 5

    .line 127
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;->valid:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;-><init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;->build()Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse;

    move-result-object v0

    return-object v0
.end method

.method public timecard(Lcom/squareup/protos/client/timecards/Timecard;)Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    return-object p0
.end method

.method public timecard_break(Lcom/squareup/protos/client/timecards/TimecardBreak;)Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    return-object p0
.end method

.method public valid(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardBreakResponse$Builder;->valid:Ljava/lang/Boolean;

    return-object p0
.end method
