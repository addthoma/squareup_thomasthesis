.class public final Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListTimecardBreakDefinitionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;",
        "Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public timecard_break_definition:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 81
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 82
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;->timecard_break_definition:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;
    .locals 3

    .line 94
    new-instance v0, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;->timecard_break_definition:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;->build()Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse;

    move-result-object v0

    return-object v0
.end method

.method public timecard_break_definition(Ljava/util/List;)Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;)",
            "Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;"
        }
    .end annotation

    .line 87
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 88
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/ListTimecardBreakDefinitionsResponse$Builder;->timecard_break_definition:Ljava/util/List;

    return-object p0
.end method
