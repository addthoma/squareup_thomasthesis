.class public final Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SubmitTipAndSettleBatchRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;",
        "Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public add_tip_and_settle_request_list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest;",
            ">;"
        }
    .end annotation
.end field

.field public client_request_uuid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 108
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;->add_tip_and_settle_request_list:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public add_tip_and_settle_request_list(Ljava/util/List;)Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleRequest;",
            ">;)",
            "Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;"
        }
    .end annotation

    .line 117
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;->add_tip_and_settle_request_list:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;
    .locals 4

    .line 132
    new-instance v0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;->add_tip_and_settle_request_list:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;->client_request_uuid:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;-><init>(Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;->build()Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest$Builder;->client_request_uuid:Ljava/lang/String;

    return-object p0
.end method
