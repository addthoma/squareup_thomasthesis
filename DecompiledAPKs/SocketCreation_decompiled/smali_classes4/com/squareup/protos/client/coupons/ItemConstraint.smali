.class public final Lcom/squareup/protos/client/coupons/ItemConstraint;
.super Lcom/squareup/wire/Message;
.source "ItemConstraint.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/coupons/ItemConstraint$ProtoAdapter_ItemConstraint;,
        Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/coupons/ItemConstraint;",
        "Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/coupons/ItemConstraint;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_QUANTITY:Ljava/lang/Integer;

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

.field private static final serialVersionUID:J


# instance fields
.field public final constraint_id:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final quantity:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.coupons.Coupon$ItemConstraintType#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/coupons/ItemConstraint$ProtoAdapter_ItemConstraint;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/ItemConstraint$ProtoAdapter_ItemConstraint;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/coupons/ItemConstraint;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 26
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/coupons/ItemConstraint;->DEFAULT_QUANTITY:Ljava/lang/Integer;

    .line 28
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->UNKNOWN:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    sput-object v0, Lcom/squareup/protos/client/coupons/ItemConstraint;->DEFAULT_TYPE:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Integer;Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;",
            ")V"
        }
    .end annotation

    .line 61
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/coupons/ItemConstraint;-><init>(Ljava/util/List;Ljava/lang/Integer;Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Integer;Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 66
    sget-object v0, Lcom/squareup/protos/client/coupons/ItemConstraint;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p4, "constraint_id"

    .line 67
    invoke-static {p4, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->constraint_id:Ljava/util/List;

    .line 68
    iput-object p2, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->quantity:Ljava/lang/Integer;

    .line 69
    iput-object p3, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 85
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/coupons/ItemConstraint;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 86
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/coupons/ItemConstraint;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/ItemConstraint;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/ItemConstraint;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->constraint_id:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/ItemConstraint;->constraint_id:Ljava/util/List;

    .line 88
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->quantity:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/coupons/ItemConstraint;->quantity:Ljava/lang/Integer;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/ItemConstraint;->type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    .line 90
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 95
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/ItemConstraint;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->constraint_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->quantity:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 101
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;
    .locals 2

    .line 74
    new-instance v0, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;-><init>()V

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->constraint_id:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;->constraint_id:Ljava/util/List;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->quantity:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;->quantity:Ljava/lang/Integer;

    .line 77
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    iput-object v1, v0, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;->type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/ItemConstraint;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/ItemConstraint;->newBuilder()Lcom/squareup/protos/client/coupons/ItemConstraint$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->constraint_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", constraint_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->constraint_id:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->quantity:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->quantity:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 111
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    if-eqz v1, :cond_2

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/ItemConstraint;->type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ItemConstraint{"

    .line 112
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
