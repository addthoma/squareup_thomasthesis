.class final Lcom/squareup/protos/client/coupons/Coupon$ProtoAdapter_Coupon;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Coupon.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/Coupon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Coupon"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 449
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/coupons/Coupon;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 486
    new-instance v0, Lcom/squareup/protos/client/coupons/Coupon$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/coupons/Coupon$Builder;-><init>()V

    .line 487
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 488
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 516
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 514
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->code(Ljava/lang/String;)Lcom/squareup/protos/client/coupons/Coupon$Builder;

    goto :goto_0

    .line 508
    :pswitch_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/coupons/Coupon$Reason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/coupons/Coupon$Reason;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->reason(Lcom/squareup/protos/client/coupons/Coupon$Reason;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 510
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 505
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/coupons/LoyaltyPoints;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->loyalty_points(Lcom/squareup/protos/client/coupons/LoyaltyPoints;)Lcom/squareup/protos/client/coupons/Coupon$Builder;

    goto :goto_0

    .line 504
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/coupons/Coupon$Builder;

    goto :goto_0

    .line 503
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->expires_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/coupons/Coupon$Builder;

    goto :goto_0

    .line 502
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/coupons/CouponReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/coupons/CouponReward;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->reward(Lcom/squareup/protos/client/coupons/CouponReward;)Lcom/squareup/protos/client/coupons/Coupon$Builder;

    goto :goto_0

    .line 501
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/coupons/CouponCondition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/coupons/CouponCondition;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->condition(Lcom/squareup/protos/client/coupons/CouponCondition;)Lcom/squareup/protos/client/coupons/Coupon$Builder;

    goto :goto_0

    .line 500
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->image_url(Ljava/lang/String;)Lcom/squareup/protos/client/coupons/Coupon$Builder;

    goto :goto_0

    .line 494
    :pswitch_8
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->item_constraint_type(Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;)Lcom/squareup/protos/client/coupons/Coupon$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v4

    .line 496
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 491
    :pswitch_9
    sget-object v3, Lcom/squareup/api/items/Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->discount(Lcom/squareup/api/items/Discount;)Lcom/squareup/protos/client/coupons/Coupon$Builder;

    goto/16 :goto_0

    .line 490
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->coupon_id(Ljava/lang/String;)Lcom/squareup/protos/client/coupons/Coupon$Builder;

    goto/16 :goto_0

    .line 520
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 521
    invoke-virtual {v0}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->build()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 447
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/coupons/Coupon$ProtoAdapter_Coupon;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 470
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 471
    sget-object v0, Lcom/squareup/api/items/Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 472
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 473
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/Coupon;->image_url:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 474
    sget-object v0, Lcom/squareup/protos/client/coupons/CouponCondition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/Coupon;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 475
    sget-object v0, Lcom/squareup/protos/client/coupons/CouponReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 476
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 477
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/Coupon;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 478
    sget-object v0, Lcom/squareup/protos/client/coupons/LoyaltyPoints;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/Coupon;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 479
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon$Reason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/Coupon;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 480
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/coupons/Coupon;->code:Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 481
    invoke-virtual {p2}, Lcom/squareup/protos/client/coupons/Coupon;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 447
    check-cast p2, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/coupons/Coupon$ProtoAdapter_Coupon;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/coupons/Coupon;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/coupons/Coupon;)I
    .locals 4

    .line 454
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/api/items/Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->discount:Lcom/squareup/api/items/Discount;

    const/4 v3, 0x2

    .line 455
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    const/4 v3, 0x3

    .line 456
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->image_url:Ljava/lang/String;

    const/4 v3, 0x4

    .line 457
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/coupons/CouponCondition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    const/4 v3, 0x5

    .line 458
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/coupons/CouponReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    const/4 v3, 0x6

    .line 459
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x7

    .line 460
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v3, 0x8

    .line 461
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/coupons/LoyaltyPoints;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    const/16 v3, 0x9

    .line 462
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/coupons/Coupon$Reason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->reason:Lcom/squareup/protos/client/coupons/Coupon$Reason;

    const/16 v3, 0xa

    .line 463
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/coupons/Coupon;->code:Ljava/lang/String;

    const/16 v3, 0xb

    .line 464
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 465
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/Coupon;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 447
    check-cast p1, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/coupons/Coupon$ProtoAdapter_Coupon;->encodedSize(Lcom/squareup/protos/client/coupons/Coupon;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/protos/client/coupons/Coupon;
    .locals 2

    .line 526
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/Coupon;->newBuilder()Lcom/squareup/protos/client/coupons/Coupon$Builder;

    move-result-object p1

    .line 527
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->discount:Lcom/squareup/api/items/Discount;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/items/Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->discount:Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/Discount;

    iput-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->discount:Lcom/squareup/api/items/Discount;

    .line 528
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/coupons/CouponCondition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/coupons/CouponCondition;

    iput-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    .line 529
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/coupons/CouponReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/coupons/CouponReward;

    iput-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    .line 530
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->expires_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 531
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 532
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/coupons/LoyaltyPoints;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    iput-object v0, p1, Lcom/squareup/protos/client/coupons/Coupon$Builder;->loyalty_points:Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    .line 533
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 534
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/Coupon$Builder;->build()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 447
    check-cast p1, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/coupons/Coupon$ProtoAdapter_Coupon;->redact(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object p1

    return-object p1
.end method
