.class public final Lcom/squareup/protos/client/coupons/LoyaltyPoints$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "LoyaltyPoints.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/coupons/LoyaltyPoints;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/coupons/LoyaltyPoints;",
        "Lcom/squareup/protos/client/coupons/LoyaltyPoints$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public point_value:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 86
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/coupons/LoyaltyPoints;
    .locals 3

    .line 99
    new-instance v0, Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    iget-object v1, p0, Lcom/squareup/protos/client/coupons/LoyaltyPoints$Builder;->point_value:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/coupons/LoyaltyPoints;-><init>(Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/protos/client/coupons/LoyaltyPoints$Builder;->build()Lcom/squareup/protos/client/coupons/LoyaltyPoints;

    move-result-object v0

    return-object v0
.end method

.method public point_value(Ljava/lang/Integer;)Lcom/squareup/protos/client/coupons/LoyaltyPoints$Builder;
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/squareup/protos/client/coupons/LoyaltyPoints$Builder;->point_value:Ljava/lang/Integer;

    return-object p0
.end method
