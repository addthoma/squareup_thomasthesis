.class public final Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetTicketResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/flipper/GetTicketResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/flipper/GetTicketResponse;",
        "Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public ms_frame:Lokio/ByteString;

.field public new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 109
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/flipper/GetTicketResponse;
    .locals 4

    .line 130
    new-instance v0, Lcom/squareup/protos/client/flipper/GetTicketResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;->ms_frame:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/flipper/GetTicketResponse;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/flipper/SealedTicket;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;->build()Lcom/squareup/protos/client/flipper/GetTicketResponse;

    move-result-object v0

    return-object v0
.end method

.method public ms_frame(Lokio/ByteString;)Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;->ms_frame:Lokio/ByteString;

    return-object p0
.end method

.method public new_ticket(Lcom/squareup/protos/client/flipper/SealedTicket;)Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/GetTicketResponse$Builder;->new_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    return-object p0
.end method
