.class final Lcom/squareup/protos/client/flipper/GetTicketRequest$ProtoAdapter_GetTicketRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetTicketRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/flipper/GetTicketRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetTicketRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/flipper/GetTicketRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 139
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/flipper/GetTicketRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/flipper/GetTicketRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 158
    new-instance v0, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;-><init>()V

    .line 159
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 160
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 165
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 163
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/flipper/SealedTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/flipper/SealedTicket;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->current_ticket(Lcom/squareup/protos/client/flipper/SealedTicket;)Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;

    goto :goto_0

    .line 162
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->ms_frame(Lokio/ByteString;)Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;

    goto :goto_0

    .line 169
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 170
    invoke-virtual {v0}, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->build()Lcom/squareup/protos/client/flipper/GetTicketRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 137
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/flipper/GetTicketRequest$ProtoAdapter_GetTicketRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/flipper/GetTicketRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/flipper/GetTicketRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 151
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/flipper/GetTicketRequest;->ms_frame:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 152
    sget-object v0, Lcom/squareup/protos/client/flipper/SealedTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/flipper/GetTicketRequest;->current_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 153
    invoke-virtual {p2}, Lcom/squareup/protos/client/flipper/GetTicketRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 137
    check-cast p2, Lcom/squareup/protos/client/flipper/GetTicketRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/flipper/GetTicketRequest$ProtoAdapter_GetTicketRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/flipper/GetTicketRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/flipper/GetTicketRequest;)I
    .locals 4

    .line 144
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/flipper/GetTicketRequest;->ms_frame:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/flipper/SealedTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/flipper/GetTicketRequest;->current_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    const/4 v3, 0x2

    .line 145
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/GetTicketRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 137
    check-cast p1, Lcom/squareup/protos/client/flipper/GetTicketRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/flipper/GetTicketRequest$ProtoAdapter_GetTicketRequest;->encodedSize(Lcom/squareup/protos/client/flipper/GetTicketRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/flipper/GetTicketRequest;)Lcom/squareup/protos/client/flipper/GetTicketRequest;
    .locals 2

    .line 175
    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/GetTicketRequest;->newBuilder()Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;

    move-result-object p1

    .line 176
    iget-object v0, p1, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->current_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/flipper/SealedTicket;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->current_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/flipper/SealedTicket;

    iput-object v0, p1, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->current_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    .line 177
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 178
    invoke-virtual {p1}, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->build()Lcom/squareup/protos/client/flipper/GetTicketRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 137
    check-cast p1, Lcom/squareup/protos/client/flipper/GetTicketRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/flipper/GetTicketRequest$ProtoAdapter_GetTicketRequest;->redact(Lcom/squareup/protos/client/flipper/GetTicketRequest;)Lcom/squareup/protos/client/flipper/GetTicketRequest;

    move-result-object p1

    return-object p1
.end method
