.class public final Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetTicketRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/flipper/GetTicketRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/flipper/GetTicketRequest;",
        "Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public current_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

.field public ms_frame:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 108
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/flipper/GetTicketRequest;
    .locals 4

    .line 133
    new-instance v0, Lcom/squareup/protos/client/flipper/GetTicketRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->ms_frame:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->current_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/flipper/GetTicketRequest;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/flipper/SealedTicket;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->build()Lcom/squareup/protos/client/flipper/GetTicketRequest;

    move-result-object v0

    return-object v0
.end method

.method public current_ticket(Lcom/squareup/protos/client/flipper/SealedTicket;)Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->current_ticket:Lcom/squareup/protos/client/flipper/SealedTicket;

    return-object p0
.end method

.method public ms_frame(Lokio/ByteString;)Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/flipper/GetTicketRequest$Builder;->ms_frame:Lokio/ByteString;

    return-object p0
.end method
