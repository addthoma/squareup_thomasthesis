.class public final Lcom/squareup/protos/client/ReceiptOption$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReceiptOption.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ReceiptOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/ReceiptOption;",
        "Lcom/squareup/protos/client/ReceiptOption$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public email:Ljava/lang/String;

.field public email_id:Ljava/lang/String;

.field public phone_number:Ljava/lang/String;

.field public phone_number_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 130
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/ReceiptOption;
    .locals 7

    .line 155
    new-instance v6, Lcom/squareup/protos/client/ReceiptOption;

    iget-object v1, p0, Lcom/squareup/protos/client/ReceiptOption$Builder;->email:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/ReceiptOption$Builder;->phone_number:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/ReceiptOption$Builder;->email_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/ReceiptOption$Builder;->phone_number_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/ReceiptOption;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/protos/client/ReceiptOption$Builder;->build()Lcom/squareup/protos/client/ReceiptOption;

    move-result-object v0

    return-object v0
.end method

.method public email(Ljava/lang/String;)Lcom/squareup/protos/client/ReceiptOption$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/client/ReceiptOption$Builder;->email:Ljava/lang/String;

    return-object p0
.end method

.method public email_id(Ljava/lang/String;)Lcom/squareup/protos/client/ReceiptOption$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/protos/client/ReceiptOption$Builder;->email_id:Ljava/lang/String;

    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/ReceiptOption$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lcom/squareup/protos/client/ReceiptOption$Builder;->phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public phone_number_id(Ljava/lang/String;)Lcom/squareup/protos/client/ReceiptOption$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/client/ReceiptOption$Builder;->phone_number_id:Ljava/lang/String;

    return-object p0
.end method
