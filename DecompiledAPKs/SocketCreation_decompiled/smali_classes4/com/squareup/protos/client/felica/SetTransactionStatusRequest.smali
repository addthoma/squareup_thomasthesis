.class public final Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;
.super Lcom/squareup/wire/Message;
.source "SetTransactionStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$ProtoAdapter_SetTransactionStatusRequest;,
        Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;",
        "Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CONNECTION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_MIRYO_TRANSACTION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/client/felica/Status;

.field public static final DEFAULT_TRANSACTION_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final connection_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final miryo_transaction_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/felica/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.felica.Status#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final transaction_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$ProtoAdapter_SetTransactionStatusRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$ProtoAdapter_SetTransactionStatusRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/client/felica/Status;->DEFAULT_DO_NOT_USE:Lcom/squareup/protos/client/felica/Status;

    sput-object v0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->DEFAULT_STATUS:Lcom/squareup/protos/client/felica/Status;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/felica/Status;Ljava/lang/String;)V
    .locals 6

    .line 67
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/felica/Status;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/felica/Status;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 72
    sget-object v0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 73
    iput-object p1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->connection_id:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->transaction_id:Ljava/lang/String;

    .line 75
    iput-object p3, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->status:Lcom/squareup/protos/client/felica/Status;

    .line 76
    iput-object p4, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->miryo_transaction_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 93
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 94
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->connection_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->connection_id:Ljava/lang/String;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->transaction_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->transaction_id:Ljava/lang/String;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->status:Lcom/squareup/protos/client/felica/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->status:Lcom/squareup/protos/client/felica/Status;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->miryo_transaction_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->miryo_transaction_id:Ljava/lang/String;

    .line 99
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 104
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->connection_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->transaction_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->status:Lcom/squareup/protos/client/felica/Status;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/felica/Status;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->miryo_transaction_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 111
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;
    .locals 2

    .line 81
    new-instance v0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;-><init>()V

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->connection_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->connection_id:Ljava/lang/String;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->transaction_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->transaction_id:Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->status:Lcom/squareup/protos/client/felica/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->status:Lcom/squareup/protos/client/felica/Status;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->miryo_transaction_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->miryo_transaction_id:Ljava/lang/String;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->newBuilder()Lcom/squareup/protos/client/felica/SetTransactionStatusRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->connection_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", connection_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->connection_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->transaction_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", transaction_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->transaction_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->status:Lcom/squareup/protos/client/felica/Status;

    if-eqz v1, :cond_2

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->status:Lcom/squareup/protos/client/felica/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 122
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->miryo_transaction_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", miryo_transaction_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/felica/SetTransactionStatusRequest;->miryo_transaction_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SetTransactionStatusRequest{"

    .line 123
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
