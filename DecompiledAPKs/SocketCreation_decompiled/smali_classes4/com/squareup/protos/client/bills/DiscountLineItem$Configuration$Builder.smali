.class public final Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DiscountLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public variable_amount_money:Lcom/squareup/protos/common/Money;

.field public variable_percentage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 424
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;
    .locals 4

    .line 445
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->variable_percentage:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->variable_amount_money:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 419
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    move-result-object v0

    return-object v0
.end method

.method public variable_amount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;
    .locals 0

    .line 439
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->variable_amount_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public variable_percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;
    .locals 0

    .line 431
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->variable_percentage:Ljava/lang/String;

    return-object p0
.end method
