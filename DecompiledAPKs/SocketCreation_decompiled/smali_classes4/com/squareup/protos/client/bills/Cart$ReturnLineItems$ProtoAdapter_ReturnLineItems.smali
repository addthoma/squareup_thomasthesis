.class final Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ProtoAdapter_ReturnLineItems;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReturnLineItems"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 8330
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8359
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;-><init>()V

    .line 8360
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 8361
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 8371
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 8369
    :pswitch_0
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_surcharge_line_item:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 8368
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_tip_line_item:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/ReturnTipLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 8367
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->rounding_adjustment(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    goto :goto_0

    .line 8366
    :pswitch_3
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->fee_line_item:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 8365
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->discount_line_item:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/DiscountLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 8364
    :pswitch_5
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_itemization:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 8363
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->source_bill_server_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    goto :goto_0

    .line 8375
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 8376
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8328
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ProtoAdapter_ReturnLineItems;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8347
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->source_bill_server_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8348
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8349
    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->discount_line_item:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8350
    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->fee_line_item:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8351
    sget-object v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8352
    sget-object v0, Lcom/squareup/protos/client/bills/ReturnTipLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8353
    sget-object v0, Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_surcharge_line_item:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8354
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8328
    check-cast p2, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ProtoAdapter_ReturnLineItems;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;)I
    .locals 4

    .line 8335
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->source_bill_server_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 8336
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/DiscountLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 8337
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->discount_line_item:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 8338
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->fee_line_item:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    const/4 v3, 0x5

    .line 8339
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ReturnTipLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 8340
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 8341
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_surcharge_line_item:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8342
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 8328
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ProtoAdapter_ReturnLineItems;->encodedSize(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;
    .locals 2

    .line 8381
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->newBuilder()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    move-result-object p1

    .line 8382
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_itemization:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 8383
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->discount_line_item:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/DiscountLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 8384
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->fee_line_item:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/FeeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 8385
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    .line 8386
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_tip_line_item:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/ReturnTipLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 8387
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_surcharge_line_item:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 8388
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 8389
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8328
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ProtoAdapter_ReturnLineItems;->redact(Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    move-result-object p1

    return-object p1
.end method
