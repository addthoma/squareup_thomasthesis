.class final Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$ProtoAdapter_AccountContact;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AccountContact"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2347
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2368
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;-><init>()V

    .line 2369
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2370
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 2376
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2374
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->account_phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;

    goto :goto_0

    .line 2373
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;

    goto :goto_0

    .line 2372
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;

    goto :goto_0

    .line 2380
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2381
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->build()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2345
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$ProtoAdapter_AccountContact;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2360
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->display_name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2361
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->contact_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2362
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->account_phone_number:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2363
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2345
    check-cast p2, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$ProtoAdapter_AccountContact;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;)I
    .locals 4

    .line 2352
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->display_name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->contact_token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 2353
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->account_phone_number:Ljava/lang/String;

    const/4 v3, 0x3

    .line 2354
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2355
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2345
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$ProtoAdapter_AccountContact;->encodedSize(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;
    .locals 0

    .line 2386
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;->newBuilder()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;

    move-result-object p1

    .line 2387
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2388
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->build()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2345
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$ProtoAdapter_AccountContact;->redact(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    move-result-object p1

    return-object p1
.end method
