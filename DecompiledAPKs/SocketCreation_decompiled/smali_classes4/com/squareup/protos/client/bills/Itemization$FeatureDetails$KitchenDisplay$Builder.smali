.class public final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public expedite_complete:Ljava/lang/Boolean;

.field public prep_complete:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4235
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;
    .locals 4

    .line 4250
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->prep_complete:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->expedite_complete:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4230
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    move-result-object v0

    return-object v0
.end method

.method public expedite_complete(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;
    .locals 0

    .line 4244
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->expedite_complete:Ljava/lang/Boolean;

    return-object p0
.end method

.method public prep_complete(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;
    .locals 0

    .line 4239
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay$Builder;->prep_complete:Ljava/lang/Boolean;

    return-object p0
.end method
