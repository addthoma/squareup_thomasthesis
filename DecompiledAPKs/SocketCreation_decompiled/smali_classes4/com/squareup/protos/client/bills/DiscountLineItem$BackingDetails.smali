.class public final Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;
.super Lcom/squareup/wire/Message;
.source "DiscountLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/DiscountLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BackingDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$ProtoAdapter_BackingDetails;,
        Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;,
        Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BACKING_TYPE:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

.field public static final DEFAULT_COUPON_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final backing_type:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.DiscountLineItem$BackingDetails$BackingType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final coupon_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final coupon_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final discount:Lcom/squareup/api/items/Discount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.Discount#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 496
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$ProtoAdapter_BackingDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$ProtoAdapter_BackingDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 502
    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;->ITEMS_SERVICE_DISCOUNT:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

    sput-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->DEFAULT_BACKING_TYPE:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/Discount;Ljava/lang/String;Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Discount;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 552
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;-><init>(Lcom/squareup/api/items/Discount;Ljava/lang/String;Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/items/Discount;Ljava/lang/String;Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/Discount;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 557
    sget-object v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 558
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    .line 559
    iput-object p2, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_id:Ljava/lang/String;

    .line 560
    iput-object p3, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

    const-string p1, "coupon_ids"

    .line 561
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 578
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 579
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    .line 580
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    .line 581
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_id:Ljava/lang/String;

    .line 582
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

    .line 583
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_ids:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_ids:Ljava/util/List;

    .line 584
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 589
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 591
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 592
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/items/Discount;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 593
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 594
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 595
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 596
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;
    .locals 2

    .line 566
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;-><init>()V

    .line 567
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->discount:Lcom/squareup/api/items/Discount;

    .line 568
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_id:Ljava/lang/String;

    .line 569
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->backing_type:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

    .line 570
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_ids:Ljava/util/List;

    .line 571
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 495
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 603
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 604
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    if-eqz v1, :cond_0

    const-string v1, ", discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 605
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", coupon_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 606
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

    if-eqz v1, :cond_2

    const-string v1, ", backing_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->backing_type:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 607
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", coupon_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->coupon_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BackingDetails{"

    .line 608
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
