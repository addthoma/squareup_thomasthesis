.class public final enum Lcom/squareup/protos/client/bills/CardData$ReaderType;
.super Ljava/lang/Enum;
.source "CardData.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ReaderType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardData$ReaderType$ProtoAdapter_ReaderType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum A10:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ENCRYPTED_KEYED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum KEYED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum MCR:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum O1:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum R4:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum S1:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum S3:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum SERVER_COMPLETED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum UNENCRYPTED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public static final enum X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 2718
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2720
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v2, 0x1

    const-string v3, "KEYED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->KEYED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2722
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v3, 0x2

    const-string v4, "UNENCRYPTED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNENCRYPTED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2724
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v4, 0x3

    const-string v5, "O1"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->O1:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2726
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v5, 0x4

    const-string v6, "S1"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->S1:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2728
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v6, 0x5

    const-string v7, "R4"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R4:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2730
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v7, 0x6

    const-string v8, "R6"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2732
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v8, 0x7

    const-string v9, "ENCRYPTED_KEYED"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ENCRYPTED_KEYED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2737
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/16 v9, 0x8

    const-string v10, "A10"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->A10:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2739
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/16 v10, 0x9

    const-string v11, "R12"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2741
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/16 v11, 0xa

    const-string v12, "X2"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2743
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/16 v12, 0xb

    const-string v13, "MCR"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->MCR:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2745
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/16 v13, 0xc

    const-string v14, "T2"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2747
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/16 v14, 0xd

    const-string v15, "SERVER_COMPLETED"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->SERVER_COMPLETED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2749
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/16 v15, 0xe

    const-string v14, "S3"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/client/bills/CardData$ReaderType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->S3:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2717
    sget-object v14, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->KEYED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNENCRYPTED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->O1:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->S1:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R4:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ENCRYPTED_KEYED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->A10:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->MCR:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->SERVER_COMPLETED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->S3:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    aput-object v1, v0, v15

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->$VALUES:[Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 2751
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$ReaderType$ProtoAdapter_ReaderType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$ReaderType$ProtoAdapter_ReaderType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 2755
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2756
    iput p3, p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 2778
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->S3:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2777
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->SERVER_COMPLETED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2776
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2775
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->MCR:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2774
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2773
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2772
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->A10:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2771
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ENCRYPTED_KEYED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2770
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2769
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R4:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2768
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->S1:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2767
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->O1:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2766
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNENCRYPTED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2765
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->KEYED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    .line 2764
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 1

    .line 2717
    const-class v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 1

    .line 2717
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->$VALUES:[Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/CardData$ReaderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 2785
    iget v0, p0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->value:I

    return v0
.end method
