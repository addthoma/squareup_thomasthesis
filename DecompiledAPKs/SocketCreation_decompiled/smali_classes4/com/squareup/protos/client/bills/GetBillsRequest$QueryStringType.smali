.class public final enum Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;
.super Ljava/lang/Enum;
.source "GetBillsRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QueryStringType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType$ProtoAdapter_QueryStringType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ENCRYPTED_READER_DATA_DUKPT_CHIP:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

.field public static final enum ENCRYPTED_READER_DATA_DUKPT_SWIPE:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

.field public static final enum ENCRYPTED_TRACK_2:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

.field public static final enum ENCRYPTED_TRACK_DATA:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

.field public static final enum PAN:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

.field public static final enum TRACK_2:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

.field public static final enum UNKNOWN_QUERY_STRING_TYPE:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

.field public static final enum USER_QUERY:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 234
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_QUERY_STRING_TYPE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->UNKNOWN_QUERY_STRING_TYPE:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    .line 240
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    const/4 v2, 0x1

    const-string v3, "USER_QUERY"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->USER_QUERY:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    .line 245
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    const/4 v3, 0x2

    const-string v4, "PAN"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->PAN:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    .line 250
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    const/4 v4, 0x3

    const-string v5, "ENCRYPTED_TRACK_2"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ENCRYPTED_TRACK_2:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    .line 255
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    const/4 v5, 0x4

    const-string v6, "ENCRYPTED_TRACK_DATA"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ENCRYPTED_TRACK_DATA:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    .line 260
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    const/4 v6, 0x5

    const-string v7, "TRACK_2"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->TRACK_2:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    .line 265
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    const/4 v7, 0x6

    const-string v8, "ENCRYPTED_READER_DATA_DUKPT_SWIPE"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ENCRYPTED_READER_DATA_DUKPT_SWIPE:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    .line 270
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    const/4 v8, 0x7

    const-string v9, "ENCRYPTED_READER_DATA_DUKPT_CHIP"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ENCRYPTED_READER_DATA_DUKPT_CHIP:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    .line 230
    sget-object v9, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->UNKNOWN_QUERY_STRING_TYPE:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->USER_QUERY:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->PAN:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ENCRYPTED_TRACK_2:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ENCRYPTED_TRACK_DATA:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->TRACK_2:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ENCRYPTED_READER_DATA_DUKPT_SWIPE:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ENCRYPTED_READER_DATA_DUKPT_CHIP:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->$VALUES:[Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    .line 272
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType$ProtoAdapter_QueryStringType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType$ProtoAdapter_QueryStringType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 276
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 277
    iput p3, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 292
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ENCRYPTED_READER_DATA_DUKPT_CHIP:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    return-object p0

    .line 291
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ENCRYPTED_READER_DATA_DUKPT_SWIPE:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    return-object p0

    .line 290
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->TRACK_2:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    return-object p0

    .line 289
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ENCRYPTED_TRACK_DATA:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    return-object p0

    .line 288
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->ENCRYPTED_TRACK_2:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    return-object p0

    .line 287
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->PAN:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    return-object p0

    .line 286
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->USER_QUERY:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    return-object p0

    .line 285
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->UNKNOWN_QUERY_STRING_TYPE:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;
    .locals 1

    .line 230
    const-class v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;
    .locals 1

    .line 230
    sget-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->$VALUES:[Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 299
    iget v0, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;->value:I

    return v0
.end method
