.class public final Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SquareProductAttributes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;",
        "Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public virtual_register:Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 297
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;
    .locals 3

    .line 308
    new-instance v0, Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale$Builder;->virtual_register:Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;-><init>(Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 294
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale$Builder;->build()Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale;

    move-result-object v0

    return-object v0
.end method

.method public virtual_register(Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;)Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale$Builder;
    .locals 0

    .line 302
    iput-object p1, p0, Lcom/squareup/protos/client/bills/SquareProductAttributes$RetailPointOfSale$Builder;->virtual_register:Lcom/squareup/protos/client/bills/CommonPointOfSaleAttributes$VirtualRegister;

    return-object p0
.end method
