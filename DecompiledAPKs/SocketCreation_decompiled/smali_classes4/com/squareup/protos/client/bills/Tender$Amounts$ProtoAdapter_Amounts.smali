.class final Lcom/squareup/protos/client/bills/Tender$Amounts$ProtoAdapter_Amounts;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$Amounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Amounts"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Tender$Amounts;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1428
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Tender$Amounts;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender$Amounts;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1457
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;-><init>()V

    .line 1458
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1459
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1469
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1467
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->auto_gratuity_percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    goto :goto_0

    .line 1466
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->auto_gratuity_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    goto :goto_0

    .line 1465
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->surcharge_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    goto :goto_0

    .line 1464
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->tip_percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    goto :goto_0

    .line 1463
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    goto :goto_0

    .line 1462
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->write_only_tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    goto :goto_0

    .line 1461
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    goto :goto_0

    .line 1473
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1474
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Amounts;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1426
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$Amounts$ProtoAdapter_Amounts;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Tender$Amounts;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender$Amounts;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1445
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1446
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Amounts;->write_only_tax_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1447
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1448
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_percentage:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1449
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Amounts;->surcharge_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1450
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Amounts;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1451
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Tender$Amounts;->auto_gratuity_percentage:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1452
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Tender$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1426
    check-cast p2, Lcom/squareup/protos/client/bills/Tender$Amounts;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Tender$Amounts$ProtoAdapter_Amounts;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Tender$Amounts;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Tender$Amounts;)I
    .locals 4

    .line 1433
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Amounts;->write_only_tax_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 1434
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x3

    .line 1435
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_percentage:Ljava/lang/String;

    const/4 v3, 0x4

    .line 1436
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Amounts;->surcharge_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 1437
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Amounts;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x6

    .line 1438
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Tender$Amounts;->auto_gratuity_percentage:Ljava/lang/String;

    const/4 v3, 0x7

    .line 1439
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1440
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$Amounts;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1426
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$Amounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$Amounts$ProtoAdapter_Amounts;->encodedSize(Lcom/squareup/protos/client/bills/Tender$Amounts;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Tender$Amounts;)Lcom/squareup/protos/client/bills/Tender$Amounts;
    .locals 2

    .line 1479
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$Amounts;->newBuilder()Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;

    move-result-object p1

    .line 1480
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->tip_money:Lcom/squareup/protos/common/Money;

    .line 1481
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->write_only_tax_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->write_only_tax_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->write_only_tax_money:Lcom/squareup/protos/common/Money;

    .line 1482
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->total_money:Lcom/squareup/protos/common/Money;

    .line 1483
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->surcharge_money:Lcom/squareup/protos/common/Money;

    .line 1484
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->auto_gratuity_money:Lcom/squareup/protos/common/Money;

    .line 1485
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1486
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Tender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Tender$Amounts;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1426
    check-cast p1, Lcom/squareup/protos/client/bills/Tender$Amounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Tender$Amounts$ProtoAdapter_Amounts;->redact(Lcom/squareup/protos/client/bills/Tender$Amounts;)Lcom/squareup/protos/client/bills/Tender$Amounts;

    move-result-object p1

    return-object p1
.end method
