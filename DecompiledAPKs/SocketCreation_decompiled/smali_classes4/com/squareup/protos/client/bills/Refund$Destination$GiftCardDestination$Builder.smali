.class public final Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Refund.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;",
        "Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public gift_card_balance:Lcom/squareup/protos/common/Money;

.field public gift_card_pan_suffix:Ljava/lang/String;

.field public gift_card_token:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 759
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;
    .locals 7

    .line 790
    new-instance v6, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->gift_card_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->gift_card_pan_suffix:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->type:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->gift_card_balance:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 750
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->build()Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination;

    move-result-object v0

    return-object v0
.end method

.method public gift_card_balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;
    .locals 0

    .line 784
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->gift_card_balance:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public gift_card_pan_suffix(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;
    .locals 0

    .line 771
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->gift_card_pan_suffix:Ljava/lang/String;

    return-object p0
.end method

.method public gift_card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;
    .locals 0

    .line 763
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->gift_card_token:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;)Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;
    .locals 0

    .line 776
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Builder;->type:Lcom/squareup/protos/client/bills/Refund$Destination$GiftCardDestination$Type;

    return-object p0
.end method
