.class final Lcom/squareup/protos/client/bills/CardData$A10Card$ProtoAdapter_A10Card;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$A10Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_A10Card"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/CardData$A10Card;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1706
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/CardData$A10Card;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData$A10Card;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1725
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;-><init>()V

    .line 1726
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1727
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 1739
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1732
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;)Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 1734
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 1729
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;

    goto :goto_0

    .line 1743
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1744
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$A10Card;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1704
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$A10Card$ProtoAdapter_A10Card;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/CardData$A10Card;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData$A10Card;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1718
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$A10Card;->encrypted_reader_data:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1719
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/CardData$A10Card;->entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1720
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$A10Card;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1704
    check-cast p2, Lcom/squareup/protos/client/bills/CardData$A10Card;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/CardData$A10Card$ProtoAdapter_A10Card;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/CardData$A10Card;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/CardData$A10Card;)I
    .locals 4

    .line 1711
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/CardData$A10Card;->encrypted_reader_data:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/CardData$A10Card;->entry_method:Lcom/squareup/protos/client/bills/CardData$A10Card$EntryMethod;

    const/4 v3, 0x3

    .line 1712
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1713
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$A10Card;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1704
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$A10Card;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$A10Card$ProtoAdapter_A10Card;->encodedSize(Lcom/squareup/protos/client/bills/CardData$A10Card;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/CardData$A10Card;)Lcom/squareup/protos/client/bills/CardData$A10Card;
    .locals 1

    .line 1749
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$A10Card;->newBuilder()Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 1750
    iput-object v0, p1, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->encrypted_reader_data:Lokio/ByteString;

    .line 1751
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1752
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$A10Card;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1704
    check-cast p1, Lcom/squareup/protos/client/bills/CardData$A10Card;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/CardData$A10Card$ProtoAdapter_A10Card;->redact(Lcom/squareup/protos/client/bills/CardData$A10Card;)Lcom/squareup/protos/client/bills/CardData$A10Card;

    move-result-object p1

    return-object p1
.end method
