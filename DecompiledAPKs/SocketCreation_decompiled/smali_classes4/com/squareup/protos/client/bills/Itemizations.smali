.class public final Lcom/squareup/protos/client/bills/Itemizations;
.super Ljava/lang/Object;
.source "Itemizations.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0002\u00a8\u0006\u0005"
    }
    d2 = {
        "featureDetailsBuilder",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;",
        "Lcom/squareup/protos/client/bills/Itemization;",
        "isAppointmentService",
        "",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final featureDetailsBuilder(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;
    .locals 1

    const-string v0, "$this$featureDetailsBuilder"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    invoke-direct {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;-><init>()V

    :goto_0
    return-object p0
.end method

.method public static final isAppointmentService(Lcom/squareup/protos/client/bills/Itemization;)Z
    .locals 1

    const-string v0, "$this$isAppointmentService"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    iget-object p0, p0, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    return p0
.end method
