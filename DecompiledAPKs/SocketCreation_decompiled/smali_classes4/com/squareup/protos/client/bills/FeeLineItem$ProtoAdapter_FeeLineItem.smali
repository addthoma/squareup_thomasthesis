.class final Lcom/squareup/protos/client/bills/FeeLineItem$ProtoAdapter_FeeLineItem;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FeeLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/FeeLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FeeLineItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/FeeLineItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1033
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/FeeLineItem;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/FeeLineItem;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1062
    new-instance v0, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;-><init>()V

    .line 1063
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1064
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1074
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1072
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    goto :goto_0

    .line 1071
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    goto :goto_0

    .line 1070
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/bills/TranslatedName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/TranslatedName;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_translated_name(Lcom/squareup/protos/client/bills/TranslatedName;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    goto :goto_0

    .line 1069
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    goto :goto_0

    .line 1068
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_display_details(Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    goto :goto_0

    .line 1067
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    goto :goto_0

    .line 1066
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->fee_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    goto :goto_0

    .line 1078
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1079
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1031
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/FeeLineItem$ProtoAdapter_FeeLineItem;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/FeeLineItem;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1050
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1051
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1052
    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1053
    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1054
    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1055
    sget-object v0, Lcom/squareup/protos/client/bills/TranslatedName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1056
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1057
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/FeeLineItem;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1031
    check-cast p2, Lcom/squareup/protos/client/bills/FeeLineItem;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/FeeLineItem$ProtoAdapter_FeeLineItem;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/FeeLineItem;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/FeeLineItem;)I
    .locals 4

    .line 1038
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x7

    .line 1039
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    const/4 v3, 0x2

    .line 1040
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    const/4 v3, 0x3

    .line 1041
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    const/4 v3, 0x4

    .line 1042
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/TranslatedName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    const/4 v3, 0x5

    .line 1043
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/FeeLineItem;->write_only_deleted:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 1044
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1045
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1031
    check-cast p1, Lcom/squareup/protos/client/bills/FeeLineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/FeeLineItem$ProtoAdapter_FeeLineItem;->encodedSize(Lcom/squareup/protos/client/bills/FeeLineItem;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/FeeLineItem;)Lcom/squareup/protos/client/bills/FeeLineItem;
    .locals 2

    .line 1084
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem;->newBuilder()Lcom/squareup/protos/client/bills/FeeLineItem$Builder;

    move-result-object p1

    .line 1085
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->fee_line_item_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 1086
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 1087
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/FeeLineItem$BackingDetails;

    .line 1088
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/FeeLineItem$DisplayDetails;

    .line 1089
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->amounts:Lcom/squareup/protos/client/bills/FeeLineItem$Amounts;

    .line 1090
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/bills/TranslatedName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/TranslatedName;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->read_only_translated_name:Lcom/squareup/protos/client/bills/TranslatedName;

    .line 1091
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1092
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/FeeLineItem$Builder;->build()Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1031
    check-cast p1, Lcom/squareup/protos/client/bills/FeeLineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/FeeLineItem$ProtoAdapter_FeeLineItem;->redact(Lcom/squareup/protos/client/bills/FeeLineItem;)Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object p1

    return-object p1
.end method
