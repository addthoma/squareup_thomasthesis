.class public final Lcom/squareup/protos/client/bills/AddTender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/AddTender;",
        "Lcom/squareup/protos/client/bills/AddTender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

.field public payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

.field public square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

.field public store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

.field public tender:Lcom/squareup/protos/client/bills/Tender;

.field public variable_capture_possible:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 179
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/AddTender;
    .locals 9

    .line 242
    new-instance v8, Lcom/squareup/protos/client/bills/AddTender;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/AddTender$Builder;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/AddTender$Builder;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/AddTender$Builder;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/AddTender$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/AddTender$Builder;->variable_capture_possible:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/AddTender;-><init>(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/PaymentInstrument;Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;Lcom/squareup/protos/client/bills/AddTender$Logging;Lcom/squareup/protos/client/bills/SquareProductAttributes;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 166
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddTender$Builder;->build()Lcom/squareup/protos/client/bills/AddTender;

    move-result-object v0

    return-object v0
.end method

.method public logging(Lcom/squareup/protos/client/bills/AddTender$Logging;)Lcom/squareup/protos/client/bills/AddTender$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTender$Builder;->logging:Lcom/squareup/protos/client/bills/AddTender$Logging;

    return-object p0
.end method

.method public payment_instrument(Lcom/squareup/protos/client/bills/PaymentInstrument;)Lcom/squareup/protos/client/bills/AddTender$Builder;
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTender$Builder;->payment_instrument:Lcom/squareup/protos/client/bills/PaymentInstrument;

    return-object p0
.end method

.method public square_product_attributes(Lcom/squareup/protos/client/bills/SquareProductAttributes;)Lcom/squareup/protos/client/bills/AddTender$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 226
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTender$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    return-object p0
.end method

.method public store_and_forward_payment_instrument(Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;)Lcom/squareup/protos/client/bills/AddTender$Builder;
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTender$Builder;->store_and_forward_payment_instrument:Lcom/squareup/protos/client/bills/StoreAndForwardPaymentInstrument;

    return-object p0
.end method

.method public tender(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/AddTender$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTender$Builder;->tender:Lcom/squareup/protos/client/bills/Tender;

    return-object p0
.end method

.method public variable_capture_possible(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/AddTender$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddTender$Builder;->variable_capture_possible:Ljava/lang/Boolean;

    return-object p0
.end method
