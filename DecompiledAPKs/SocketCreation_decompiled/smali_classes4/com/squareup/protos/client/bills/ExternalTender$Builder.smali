.class public final Lcom/squareup/protos/client/bills/ExternalTender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ExternalTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ExternalTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ExternalTender;",
        "Lcom/squareup/protos/client/bills/ExternalTender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public payment_source:Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/ExternalTender;
    .locals 3

    .line 90
    new-instance v0, Lcom/squareup/protos/client/bills/ExternalTender;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ExternalTender$Builder;->payment_source:Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/ExternalTender;-><init>(Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ExternalTender$Builder;->build()Lcom/squareup/protos/client/bills/ExternalTender;

    move-result-object v0

    return-object v0
.end method

.method public payment_source(Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;)Lcom/squareup/protos/client/bills/ExternalTender$Builder;
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ExternalTender$Builder;->payment_source:Lcom/squareup/protos/client/bills/ExternalTender$PaymentSource;

    return-object p0
.end method
