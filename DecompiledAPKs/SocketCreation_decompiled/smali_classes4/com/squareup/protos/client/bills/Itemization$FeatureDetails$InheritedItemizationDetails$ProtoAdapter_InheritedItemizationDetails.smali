.class final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$ProtoAdapter_InheritedItemizationDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_InheritedItemizationDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 5148
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5168
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;-><init>()V

    .line 5169
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 5170
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 5182
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 5175
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->create_reason(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 5177
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 5172
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->inherited_event_id_pairs:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5186
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 5187
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5146
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$ProtoAdapter_InheritedItemizationDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5161
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;->inherited_event_id_pairs:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5162
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;->create_reason:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5163
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5146
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$ProtoAdapter_InheritedItemizationDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;)I
    .locals 4

    .line 5153
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;->inherited_event_id_pairs:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;->create_reason:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$CreateReason;

    const/4 v3, 0x2

    .line 5154
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5155
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 5146
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$ProtoAdapter_InheritedItemizationDetails;->encodedSize(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;
    .locals 2

    .line 5192
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;

    move-result-object p1

    .line 5193
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->inherited_event_id_pairs:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 5194
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 5195
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 5146
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails$ProtoAdapter_InheritedItemizationDetails;->redact(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    move-result-object p1

    return-object p1
.end method
