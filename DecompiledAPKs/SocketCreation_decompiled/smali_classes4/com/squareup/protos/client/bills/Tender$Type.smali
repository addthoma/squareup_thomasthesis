.class public final enum Lcom/squareup/protos/client/bills/Tender$Type;
.super Ljava/lang/Enum;
.source "Tender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Tender$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/Tender$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/Tender$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Tender$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CARD:Lcom/squareup/protos/client/bills/Tender$Type;

.field public static final enum CASH:Lcom/squareup/protos/client/bills/Tender$Type;

.field public static final enum EXTERNAL:Lcom/squareup/protos/client/bills/Tender$Type;

.field public static final enum GENERIC:Lcom/squareup/protos/client/bills/Tender$Type;

.field public static final enum NO_SALE:Lcom/squareup/protos/client/bills/Tender$Type;

.field public static final enum OTHER:Lcom/squareup/protos/client/bills/Tender$Type;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/bills/Tender$Type;

.field public static final enum WALLET:Lcom/squareup/protos/client/bills/Tender$Type;

.field public static final enum ZERO_AMOUNT:Lcom/squareup/protos/client/bills/Tender$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 716
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 718
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v2, 0x1

    const-string v3, "CARD"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->CARD:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 720
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v3, 0x2

    const-string v4, "CASH"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->CASH:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 729
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v4, 0x3

    const-string v5, "NO_SALE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/bills/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->NO_SALE:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 731
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v5, 0x4

    const-string v6, "OTHER"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/bills/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->OTHER:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 733
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v6, 0x5

    const-string v7, "WALLET"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/bills/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->WALLET:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 735
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v7, 0x6

    const-string v8, "GENERIC"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/bills/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->GENERIC:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 742
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v8, 0x7

    const-string v9, "ZERO_AMOUNT"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/bills/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->ZERO_AMOUNT:Lcom/squareup/protos/client/bills/Tender$Type;

    .line 748
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Type;

    const/16 v9, 0x9

    const/16 v10, 0x8

    const-string v11, "EXTERNAL"

    invoke-direct {v0, v11, v10, v9}, Lcom/squareup/protos/client/bills/Tender$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->EXTERNAL:Lcom/squareup/protos/client/bills/Tender$Type;

    new-array v0, v9, [Lcom/squareup/protos/client/bills/Tender$Type;

    .line 715
    sget-object v9, Lcom/squareup/protos/client/bills/Tender$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$Type;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->CARD:Lcom/squareup/protos/client/bills/Tender$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->CASH:Lcom/squareup/protos/client/bills/Tender$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->NO_SALE:Lcom/squareup/protos/client/bills/Tender$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->OTHER:Lcom/squareup/protos/client/bills/Tender$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->WALLET:Lcom/squareup/protos/client/bills/Tender$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->GENERIC:Lcom/squareup/protos/client/bills/Tender$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->ZERO_AMOUNT:Lcom/squareup/protos/client/bills/Tender$Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->EXTERNAL:Lcom/squareup/protos/client/bills/Tender$Type;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->$VALUES:[Lcom/squareup/protos/client/bills/Tender$Type;

    .line 750
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Tender$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 754
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 755
    iput p3, p0, Lcom/squareup/protos/client/bills/Tender$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/Tender$Type;
    .locals 0

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    const/4 p0, 0x0

    return-object p0

    .line 771
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$Type;->EXTERNAL:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0

    .line 770
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$Type;->ZERO_AMOUNT:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0

    .line 769
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$Type;->GENERIC:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0

    .line 768
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$Type;->WALLET:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0

    .line 767
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$Type;->OTHER:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0

    .line 766
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$Type;->NO_SALE:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0

    .line 765
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$Type;->CASH:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0

    .line 764
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$Type;->CARD:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0

    .line 763
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/bills/Tender$Type;->UNKNOWN:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$Type;
    .locals 1

    .line 715
    const-class v0, Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/Tender$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/Tender$Type;
    .locals 1

    .line 715
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->$VALUES:[Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/Tender$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/Tender$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 778
    iget v0, p0, Lcom/squareup/protos/client/bills/Tender$Type;->value:I

    return v0
.end method
