.class final Lcom/squareup/protos/client/bills/Cart$AmountDetails$ProtoAdapter_AmountDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$AmountDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AmountDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Cart$AmountDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 8541
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$AmountDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8562
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;-><init>()V

    .line 8563
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 8564
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 8570
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 8568
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$Amounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->net(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    goto :goto_0

    .line 8567
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$Amounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->sale(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    goto :goto_0

    .line 8566
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/bills/Cart$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart$Amounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->return_(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    goto :goto_0

    .line 8574
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 8575
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8539
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$ProtoAdapter_AmountDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$AmountDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8554
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8555
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8556
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8557
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8539
    check-cast p2, Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$ProtoAdapter_AmountDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$AmountDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Cart$AmountDetails;)I
    .locals 4

    .line 8546
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    const/4 v3, 0x2

    .line 8547
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    const/4 v3, 0x3

    .line 8548
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8549
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 8539
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$ProtoAdapter_AmountDetails;->encodedSize(Lcom/squareup/protos/client/bills/Cart$AmountDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Cart$AmountDetails;)Lcom/squareup/protos/client/bills/Cart$AmountDetails;
    .locals 2

    .line 8580
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;

    move-result-object p1

    .line 8581
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$Amounts;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 8582
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$Amounts;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 8583
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$Amounts;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 8584
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 8585
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8539
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$AmountDetails$ProtoAdapter_AmountDetails;->redact(Lcom/squareup/protos/client/bills/Cart$AmountDetails;)Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    move-result-object p1

    return-object p1
.end method
