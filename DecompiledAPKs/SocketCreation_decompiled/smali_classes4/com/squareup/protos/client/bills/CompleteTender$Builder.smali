.class public final Lcom/squareup/protos/client/bills/CompleteTender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CompleteTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CompleteTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CompleteTender;",
        "Lcom/squareup/protos/client/bills/CompleteTender$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

.field public complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

.field public extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

.field public tender_id_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 127
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amounts(Lcom/squareup/protos/client/bills/CompleteTender$Amounts;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/CompleteTender;
    .locals 7

    .line 159
    new-instance v6, Lcom/squareup/protos/client/bills/CompleteTender;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->amounts:Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/CompleteTender;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CompleteTender$Amounts;Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender;

    move-result-object v0

    return-object v0
.end method

.method public complete_details(Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->complete_details:Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    return-object p0
.end method

.method public extra_tender_details(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->extra_tender_details:Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    return-object p0
.end method

.method public tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;
    .locals 0

    .line 131
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
