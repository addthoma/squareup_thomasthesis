.class final Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$ProtoAdapter_Configuration;
.super Lcom/squareup/wire/ProtoAdapter;
.source "DiscountLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Configuration"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 451
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 470
    new-instance v0, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;-><init>()V

    .line 471
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 472
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 477
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 475
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->variable_amount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;

    goto :goto_0

    .line 474
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->variable_percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;

    goto :goto_0

    .line 481
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 482
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 449
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$ProtoAdapter_Configuration;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 463
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->variable_percentage:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 464
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->variable_amount_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 465
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 449
    check-cast p2, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$ProtoAdapter_Configuration;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;)I
    .locals 4

    .line 456
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->variable_percentage:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->variable_amount_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 457
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 458
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 449
    check-cast p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$ProtoAdapter_Configuration;->encodedSize(Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;)Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;
    .locals 2

    .line 487
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;->newBuilder()Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;

    move-result-object p1

    .line 488
    iget-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->variable_amount_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->variable_amount_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->variable_amount_money:Lcom/squareup/protos/common/Money;

    .line 489
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 490
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 449
    check-cast p1, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration$ProtoAdapter_Configuration;->redact(Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;)Lcom/squareup/protos/client/bills/DiscountLineItem$Configuration;

    move-result-object p1

    return-object p1
.end method
