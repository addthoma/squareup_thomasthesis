.class public final Lcom/squareup/protos/client/bills/Refund$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Refund.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Refund;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Refund;",
        "Lcom/squareup/protos/client/bills/Refund$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;

.field public read_only_linked_tender:Lcom/squareup/protos/client/bills/Tender;

.field public read_only_localized_error_description:Ljava/lang/String;

.field public read_only_localized_error_title:Ljava/lang/String;

.field public read_only_refunded_at:Lcom/squareup/protos/client/ISO8601Date;

.field public reason:Ljava/lang/String;

.field public reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

.field public refund_id_pair:Lcom/squareup/protos/client/IdPair;

.field public state:Lcom/squareup/protos/client/bills/Refund$State;

.field public write_only_amount:Lcom/squareup/protos/common/Money;

.field public write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public write_only_linked_bill_server_token:Ljava/lang/String;

.field public write_only_linked_tender_server_token:Ljava/lang/String;

.field public write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 321
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Refund;
    .locals 18

    move-object/from16 v0, p0

    .line 443
    new-instance v17, Lcom/squareup/protos/client/bills/Refund;

    iget-object v2, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->state:Lcom/squareup/protos/client/bills/Refund$State;

    iget-object v4, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_linked_bill_server_token:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_linked_tender_server_token:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_amount:Lcom/squareup/protos/common/Money;

    iget-object v7, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v8, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    iget-object v9, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->reason:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_localized_error_title:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_localized_error_description:Ljava/lang/String;

    iget-object v12, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v13, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v14, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_linked_tender:Lcom/squareup/protos/client/bills/Tender;

    iget-object v15, v0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/protos/client/bills/Refund;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Refund$State;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/bills/Refund$Destination;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 292
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Refund$Builder;->build()Lcom/squareup/protos/client/bills/Refund;

    move-result-object v0

    return-object v0
.end method

.method public read_only_destination(Lcom/squareup/protos/client/bills/Refund$Destination;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 437
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_destination:Lcom/squareup/protos/client/bills/Refund$Destination;

    return-object p0
.end method

.method public read_only_linked_tender(Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 429
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_linked_tender:Lcom/squareup/protos/client/bills/Tender;

    return-object p0
.end method

.method public read_only_localized_error_description(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 405
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_localized_error_description:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_localized_error_title(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 395
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_localized_error_title:Ljava/lang/String;

    return-object p0
.end method

.method public read_only_refunded_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 421
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->read_only_refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public reason(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 386
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->reason:Ljava/lang/String;

    return-object p0
.end method

.method public reason_option(Lcom/squareup/protos/client/bills/Refund$ReasonOption;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 376
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->reason_option:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-object p0
.end method

.method public refund_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->refund_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public state(Lcom/squareup/protos/client/bills/Refund$State;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 334
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->state:Lcom/squareup/protos/client/bills/Refund$State;

    return-object p0
.end method

.method public write_only_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 359
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public write_only_creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 367
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public write_only_linked_bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 342
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_linked_bill_server_token:Ljava/lang/String;

    return-object p0
.end method

.method public write_only_linked_tender_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 351
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_linked_tender_server_token:Ljava/lang/String;

    return-object p0
.end method

.method public write_only_requested_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Refund$Builder;
    .locals 0

    .line 413
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Refund$Builder;->write_only_requested_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
