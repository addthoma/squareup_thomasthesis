.class public final Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReturnLineItems"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ProtoAdapter_ReturnLineItems;,
        Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;,
        Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SOURCE_BILL_SERVER_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final discount_line_item:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.DiscountLineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public final fee_line_item:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.FeeLineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public final return_itemization:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Cart$ReturnLineItems$ReturnItemization#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            ">;"
        }
    .end annotation
.end field

.field public final return_surcharge_line_item:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ReturnSurchargeLineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public final return_tip_line_item:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.ReturnTipLineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public final rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.RoundingAdjustmentLineItem#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final source_bill_server_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7917
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ProtoAdapter_ReturnLineItems;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ProtoAdapter_ReturnLineItems;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;Ljava/util/List;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;",
            "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;",
            ">;)V"
        }
    .end annotation

    .line 7996
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;",
            "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnSurchargeLineItem;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 8004
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 8005
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->source_bill_server_id:Ljava/lang/String;

    const-string p1, "return_itemization"

    .line 8006
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    const-string p1, "discount_line_item"

    .line 8007
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->discount_line_item:Ljava/util/List;

    const-string p1, "fee_line_item"

    .line 8008
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->fee_line_item:Ljava/util/List;

    .line 8009
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    const-string p1, "return_tip_line_item"

    .line 8010
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    const-string p1, "return_surcharge_line_item"

    .line 8011
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_surcharge_line_item:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 8031
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 8032
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    .line 8033
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->source_bill_server_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->source_bill_server_id:Ljava/lang/String;

    .line 8034
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    .line 8035
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->discount_line_item:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->discount_line_item:Ljava/util/List;

    .line 8036
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->fee_line_item:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->fee_line_item:Ljava/util/List;

    .line 8037
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    .line 8038
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    .line 8039
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_surcharge_line_item:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_surcharge_line_item:Ljava/util/List;

    .line 8040
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 8045
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 8047
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 8048
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->source_bill_server_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8049
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8050
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->discount_line_item:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8051
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->fee_line_item:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8052
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 8053
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 8054
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_surcharge_line_item:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 8055
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;
    .locals 2

    .line 8016
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;-><init>()V

    .line 8017
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->source_bill_server_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->source_bill_server_id:Ljava/lang/String;

    .line 8018
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_itemization:Ljava/util/List;

    .line 8019
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->discount_line_item:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->discount_line_item:Ljava/util/List;

    .line 8020
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->fee_line_item:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->fee_line_item:Ljava/util/List;

    .line 8021
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    .line 8022
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_tip_line_item:Ljava/util/List;

    .line 8023
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_surcharge_line_item:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->return_surcharge_line_item:Ljava/util/List;

    .line 8024
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 7916
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->newBuilder()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 8062
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8063
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->source_bill_server_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", source_bill_server_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->source_bill_server_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 8064
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", return_itemization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8065
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->discount_line_item:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", discount_line_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->discount_line_item:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8066
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->fee_line_item:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", fee_line_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->fee_line_item:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8067
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    if-eqz v1, :cond_4

    const-string v1, ", rounding_adjustment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8068
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", return_tip_line_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 8069
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_surcharge_line_item:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", return_surcharge_line_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_surcharge_line_item:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReturnLineItems{"

    .line 8070
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
