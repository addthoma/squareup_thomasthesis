.class public final Lcom/squareup/protos/client/bills/GetBillsRequest$Query$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBillsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillsRequest$Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetBillsRequest$Query;",
        "Lcom/squareup/protos/client/bills/GetBillsRequest$Query$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public query_string:Ljava/lang/String;

.field public query_string_type:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 393
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/GetBillsRequest$Query;
    .locals 4

    .line 411
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$Query;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Query$Builder;->query_string_type:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Query$Builder;->query_string:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/GetBillsRequest$Query;-><init>(Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 388
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillsRequest$Query$Builder;->build()Lcom/squareup/protos/client/bills/GetBillsRequest$Query;

    move-result-object v0

    return-object v0
.end method

.method public query_string(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillsRequest$Query$Builder;
    .locals 0

    .line 405
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Query$Builder;->query_string:Ljava/lang/String;

    return-object p0
.end method

.method public query_string_type(Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;)Lcom/squareup/protos/client/bills/GetBillsRequest$Query$Builder;
    .locals 0

    .line 397
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$Query$Builder;->query_string_type:Lcom/squareup/protos/client/bills/GetBillsRequest$QueryStringType;

    return-object p0
.end method
