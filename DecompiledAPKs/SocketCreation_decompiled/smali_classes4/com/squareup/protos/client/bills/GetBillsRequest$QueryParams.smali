.class public final Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;
.super Lcom/squareup/wire/Message;
.source "GetBillsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "QueryParams"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$ProtoAdapter_QueryParams;,
        Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;",
        "Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_QUERY_STRING:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.GetBillsRequest$InstrumentSearch#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final query_string:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 628
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$ProtoAdapter_QueryParams;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$ProtoAdapter_QueryParams;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;)V
    .locals 1

    .line 653
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;Lokio/ByteString;)V
    .locals 1

    .line 658
    sget-object v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 659
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p3

    const/4 v0, 0x1

    if-gt p3, v0, :cond_0

    .line 662
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->query_string:Ljava/lang/String;

    .line 663
    iput-object p2, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    return-void

    .line 660
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of query_string, instrument_search may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 678
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 679
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;

    .line 680
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->query_string:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->query_string:Ljava/lang/String;

    .line 681
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    .line 682
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 687
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 689
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 690
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->query_string:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 691
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 692
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;
    .locals 2

    .line 668
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;-><init>()V

    .line 669
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->query_string:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->query_string:Ljava/lang/String;

    .line 670
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    .line 671
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 627
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->newBuilder()Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 699
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 700
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->query_string:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", query_string="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->query_string:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 701
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    if-eqz v1, :cond_1

    const-string v1, ", instrument_search="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillsRequest$QueryParams;->instrument_search:Lcom/squareup/protos/client/bills/GetBillsRequest$InstrumentSearch;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "QueryParams{"

    .line 702
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
