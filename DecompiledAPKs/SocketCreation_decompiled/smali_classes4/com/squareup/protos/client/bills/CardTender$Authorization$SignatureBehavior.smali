.class public final enum Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;
.super Ljava/lang/Enum;
.source "CardTender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardTender$Authorization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SignatureBehavior"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior$ProtoAdapter_SignatureBehavior;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEFAULT_SIG_BEHAVIOR_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

.field public static final enum ISSUER_REQUEST_FOR_SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1236
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    const/4 v1, 0x0

    const-string v2, "DEFAULT_SIG_BEHAVIOR_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->DEFAULT_SIG_BEHAVIOR_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    .line 1241
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    const/4 v2, 0x1

    const-string v3, "ISSUER_REQUEST_FOR_SIGNATURE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->ISSUER_REQUEST_FOR_SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    .line 1235
    sget-object v3, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->DEFAULT_SIG_BEHAVIOR_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->ISSUER_REQUEST_FOR_SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    .line 1243
    new-instance v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior$ProtoAdapter_SignatureBehavior;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior$ProtoAdapter_SignatureBehavior;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1247
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1248
    iput p3, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1257
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->ISSUER_REQUEST_FOR_SIGNATURE:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    return-object p0

    .line 1256
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->DEFAULT_SIG_BEHAVIOR_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;
    .locals 1

    .line 1235
    const-class v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;
    .locals 1

    .line 1235
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->$VALUES:[Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1264
    iget v0, p0, Lcom/squareup/protos/client/bills/CardTender$Authorization$SignatureBehavior;->value:I

    return v0
.end method
