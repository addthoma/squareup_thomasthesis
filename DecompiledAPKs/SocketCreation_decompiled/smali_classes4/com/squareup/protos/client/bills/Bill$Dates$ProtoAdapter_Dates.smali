.class final Lcom/squareup/protos/client/bills/Bill$Dates$ProtoAdapter_Dates;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Bill.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Bill$Dates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Dates"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Bill$Dates;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 561
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Bill$Dates;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 580
    new-instance v0, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;-><init>()V

    .line 581
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 582
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 587
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 585
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->completed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    goto :goto_0

    .line 584
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    goto :goto_0

    .line 591
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 592
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->build()Lcom/squareup/protos/client/bills/Bill$Dates;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 559
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Bill$Dates$ProtoAdapter_Dates;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Bill$Dates;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Bill$Dates;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 573
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill$Dates;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 574
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Bill$Dates;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 575
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Bill$Dates;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 559
    check-cast p2, Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Bill$Dates$ProtoAdapter_Dates;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Bill$Dates;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Bill$Dates;)I
    .locals 4

    .line 566
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill$Dates;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Bill$Dates;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x3

    .line 567
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 568
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Bill$Dates;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 559
    check-cast p1, Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Bill$Dates$ProtoAdapter_Dates;->encodedSize(Lcom/squareup/protos/client/bills/Bill$Dates;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Bill$Dates;)Lcom/squareup/protos/client/bills/Bill$Dates;
    .locals 2

    .line 597
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Bill$Dates;->newBuilder()Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    move-result-object p1

    .line 598
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 599
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 600
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 601
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->build()Lcom/squareup/protos/client/bills/Bill$Dates;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 559
    check-cast p1, Lcom/squareup/protos/client/bills/Bill$Dates;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Bill$Dates$ProtoAdapter_Dates;->redact(Lcom/squareup/protos/client/bills/Bill$Dates;)Lcom/squareup/protos/client/bills/Bill$Dates;

    move-result-object p1

    return-object p1
.end method
