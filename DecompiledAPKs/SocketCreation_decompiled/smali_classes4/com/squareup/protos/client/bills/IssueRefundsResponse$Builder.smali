.class public final Lcom/squareup/protos/client/bills/IssueRefundsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "IssueRefundsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/IssueRefundsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/IssueRefundsResponse;",
        "Lcom/squareup/protos/client/bills/IssueRefundsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill:Lcom/squareup/protos/client/bills/Bill;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 94
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bill(Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/protos/client/bills/IssueRefundsResponse$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/client/bills/IssueRefundsResponse$Builder;->bill:Lcom/squareup/protos/client/bills/Bill;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/IssueRefundsResponse;
    .locals 4

    .line 112
    new-instance v0, Lcom/squareup/protos/client/bills/IssueRefundsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/IssueRefundsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/IssueRefundsResponse$Builder;->bill:Lcom/squareup/protos/client/bills/Bill;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/IssueRefundsResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bills/Bill;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/IssueRefundsResponse$Builder;->build()Lcom/squareup/protos/client/bills/IssueRefundsResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bills/IssueRefundsResponse$Builder;
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/protos/client/bills/IssueRefundsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
