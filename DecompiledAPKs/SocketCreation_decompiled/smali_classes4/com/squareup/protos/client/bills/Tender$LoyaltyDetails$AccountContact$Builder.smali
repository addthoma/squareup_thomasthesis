.class public final Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;",
        "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_phone_number:Ljava/lang/String;

.field public contact_token:Ljava/lang/String;

.field public display_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2321
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public account_phone_number(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;
    .locals 0

    .line 2335
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->account_phone_number:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;
    .locals 5

    .line 2341
    new-instance v0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->display_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->contact_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->account_phone_number:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2314
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->build()Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact;

    move-result-object v0

    return-object v0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;
    .locals 0

    .line 2330
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;
    .locals 0

    .line 2325
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails$AccountContact$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method
