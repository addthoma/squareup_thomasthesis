.class public final Lcom/squareup/protos/client/bills/Bill$Dates$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Bill.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Bill$Dates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Bill$Dates;",
        "Lcom/squareup/protos/client/bills/Bill$Dates$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public completed_at:Lcom/squareup/protos/client/ISO8601Date;

.field public created_at:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 532
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Bill$Dates;
    .locals 4

    .line 555
    new-instance v0, Lcom/squareup/protos/client/bills/Bill$Dates;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/Bill$Dates;-><init>(Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 527
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->build()Lcom/squareup/protos/client/bills/Bill$Dates;

    move-result-object v0

    return-object v0
.end method

.method public completed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Bill$Dates$Builder;
    .locals 0

    .line 549
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->completed_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Bill$Dates$Builder;
    .locals 0

    .line 540
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
