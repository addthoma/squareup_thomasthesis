.class public final Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;",
        "Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

.field public course_id_pair:Lcom/squareup/protos/client/IdPair;

.field public inherited_itemization_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

.field public kitchen_display:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

.field public pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

.field public seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4106
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public appointments_service_details(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;
    .locals 0

    .line 4134
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;
    .locals 9

    .line 4151
    new-instance v8, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->kitchen_display:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->inherited_itemization_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;-><init>(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4093
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    move-result-object v0

    return-object v0
.end method

.method public course_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;
    .locals 0

    .line 4119
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->course_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public inherited_itemization_details(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;
    .locals 0

    .line 4140
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->inherited_itemization_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;

    return-object p0
.end method

.method public kitchen_display(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;
    .locals 0

    .line 4110
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->kitchen_display:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$KitchenDisplay;

    return-object p0
.end method

.method public pricing_engine_state(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;
    .locals 0

    .line 4145
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->pricing_engine_state:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$PricingEngineState;

    return-object p0
.end method

.method public seating(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;
    .locals 0

    .line 4124
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->seating:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Seating;

    return-object p0
.end method
