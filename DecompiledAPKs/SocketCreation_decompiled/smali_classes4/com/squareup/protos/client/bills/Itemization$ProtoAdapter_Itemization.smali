.class final Lcom/squareup/protos/client/bills/Itemization$ProtoAdapter_Itemization;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Itemization.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Itemization;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Itemization"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Itemization;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 5393
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Itemization;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5434
    new-instance v0, Lcom/squareup/protos/client/bills/Itemization$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Itemization$Builder;-><init>()V

    .line 5435
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 5436
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 5459
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 5457
    :pswitch_1
    sget-object v3, Lcom/squareup/orders/model/Order$QuantityUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Builder;->measurement_unit(Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    goto :goto_0

    .line 5451
    :pswitch_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bills/Itemization$Builder;->quantity_entry_type(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/protos/client/bills/Itemization$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 5453
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bills/Itemization$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 5448
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Builder;->feature_details(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    goto :goto_0

    .line 5447
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Itemization$Builder;->event:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/Itemization$Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 5446
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    goto :goto_0

    .line 5445
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    goto :goto_0

    .line 5444
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/bills/Itemization$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization$Amounts;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Builder;->amounts(Lcom/squareup/protos/client/bills/Itemization$Amounts;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    goto :goto_0

    .line 5443
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Builder;->read_only_display_details(Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    goto :goto_0

    .line 5442
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_backing_details(Lcom/squareup/protos/client/bills/Itemization$BackingDetails;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    goto/16 :goto_0

    .line 5441
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Itemization$Configuration;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration(Lcom/squareup/protos/client/bills/Itemization$Configuration;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    goto/16 :goto_0

    .line 5440
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Builder;->custom_note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    goto/16 :goto_0

    .line 5439
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Builder;->quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    goto/16 :goto_0

    .line 5438
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Itemization$Builder;->itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    goto/16 :goto_0

    .line 5463
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 5464
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5391
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$ProtoAdapter_Itemization;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Itemization;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5416
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5417
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5418
    sget-object v0, Lcom/squareup/orders/model/Order$QuantityUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5419
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5420
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->custom_note:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5421
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5422
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5423
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5424
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5425
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5426
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->write_only_deleted:Ljava/lang/Boolean;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5427
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5428
    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5429
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Itemization;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5391
    check-cast p2, Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Itemization$ProtoAdapter_Itemization;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Itemization;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Itemization;)I
    .locals 4

    .line 5398
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->quantity:Ljava/lang/String;

    const/4 v3, 0x2

    .line 5399
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$QuantityUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    const/16 v3, 0xe

    .line 5400
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->quantity_entry_type:Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;

    const/16 v3, 0xd

    .line 5401
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->custom_note:Ljava/lang/String;

    const/4 v3, 0x3

    .line 5402
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    const/4 v3, 0x5

    .line 5403
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    const/4 v3, 0x6

    .line 5404
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    const/4 v3, 0x7

    .line 5405
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    const/16 v3, 0x8

    .line 5406
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v3, 0x9

    .line 5407
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->write_only_deleted:Ljava/lang/Boolean;

    const/16 v3, 0xa

    .line 5408
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 5409
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->event:Ljava/util/List;

    const/16 v3, 0xb

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Itemization;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    const/16 v3, 0xc

    .line 5410
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5411
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 5391
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$ProtoAdapter_Itemization;->encodedSize(Lcom/squareup/protos/client/bills/Itemization;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/client/bills/Itemization;
    .locals 2

    .line 5469
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object p1

    .line 5470
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 5471
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/orders/model/Order$QuantityUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$QuantityUnit;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->measurement_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 5472
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    .line 5473
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    .line 5474
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->read_only_display_details:Lcom/squareup/protos/client/bills/Itemization$DisplayDetails;

    .line 5475
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$Amounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    .line 5476
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 5477
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->event:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Event;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 5478
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Itemization$Builder;->feature_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    .line 5479
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 5480
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 5391
    check-cast p1, Lcom/squareup/protos/client/bills/Itemization;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Itemization$ProtoAdapter_Itemization;->redact(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/protos/client/bills/Itemization;

    move-result-object p1

    return-object p1
.end method
