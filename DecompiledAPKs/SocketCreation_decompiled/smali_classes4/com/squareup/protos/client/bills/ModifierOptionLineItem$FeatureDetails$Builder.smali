.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public conversational_mode:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
            ">;"
        }
    .end annotation
.end field

.field public quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 810
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 811
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->conversational_mode:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;
    .locals 4

    .line 830
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->conversational_mode:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;-><init>(Ljava/util/List;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 805
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    move-result-object v0

    return-object v0
.end method

.method public conversational_mode(Ljava/util/List;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;",
            ">;)",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;"
        }
    .end annotation

    .line 818
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 819
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->conversational_mode:Ljava/util/List;

    return-object p0
.end method

.method public quantities(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;
    .locals 0

    .line 824
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    return-object p0
.end method
