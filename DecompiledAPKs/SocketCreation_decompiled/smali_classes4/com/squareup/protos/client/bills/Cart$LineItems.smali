.class public final Lcom/squareup/protos/client/bills/Cart$LineItems;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LineItems"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$LineItems$ProtoAdapter_LineItems;,
        Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$LineItems;",
        "Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$LineItems;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final comp_itemization:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.DiningOptionLineItem#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final discount:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.DiscountLineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public final fee:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.FeeLineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public final itemization:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation
.end field

.field public final rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.RoundingAdjustmentLineItem#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final surcharge:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.SurchargeLineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public final void_itemization:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.Itemization#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 286
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$LineItems$ProtoAdapter_LineItems;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$LineItems$ProtoAdapter_LineItems;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;Lcom/squareup/protos/client/bills/DiningOptionLineItem;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;",
            "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
            "Lcom/squareup/protos/client/bills/DiningOptionLineItem;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
            ">;)V"
        }
    .end annotation

    .line 384
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/client/bills/Cart$LineItems;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;Lcom/squareup/protos/client/bills/DiningOptionLineItem;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;Lcom/squareup/protos/client/bills/DiningOptionLineItem;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;",
            "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
            "Lcom/squareup/protos/client/bills/DiningOptionLineItem;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 392
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p9, "itemization"

    .line 393
    invoke-static {p9, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    const-string p1, "fee"

    .line 394
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    const-string p1, "discount"

    .line 395
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->discount:Ljava/util/List;

    .line 396
    iput-object p4, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    .line 397
    iput-object p5, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    const-string p1, "comp_itemization"

    .line 398
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->comp_itemization:Ljava/util/List;

    const-string/jumbo p1, "void_itemization"

    .line 399
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    const-string p1, "surcharge"

    .line 400
    invoke-static {p1, p8}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 421
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 422
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$LineItems;

    .line 423
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$LineItems;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$LineItems;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    .line 424
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    .line 425
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->discount:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->discount:Ljava/util/List;

    .line 426
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    .line 427
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    .line 428
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->comp_itemization:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->comp_itemization:Ljava/util/List;

    .line 429
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    .line 430
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    .line 431
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 436
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 438
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$LineItems;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 439
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 440
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 441
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->discount:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 442
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 443
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/DiningOptionLineItem;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 444
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->comp_itemization:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 445
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 446
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 447
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;
    .locals 2

    .line 405
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;-><init>()V

    .line 406
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->itemization:Ljava/util/List;

    .line 407
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->fee:Ljava/util/List;

    .line 408
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->discount:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->discount:Ljava/util/List;

    .line 409
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    .line 410
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    .line 411
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->comp_itemization:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->comp_itemization:Ljava/util/List;

    .line 412
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->void_itemization:Ljava/util/List;

    .line 413
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->surcharge:Ljava/util/List;

    .line 414
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$LineItems;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 285
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$LineItems;->newBuilder()Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 454
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 455
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", itemization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 456
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", fee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->fee:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 457
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->discount:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->discount:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 458
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    if-eqz v1, :cond_3

    const-string v1, ", rounding_adjustment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 459
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    if-eqz v1, :cond_4

    const-string v1, ", default_dining_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->default_dining_option:Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 460
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->comp_itemization:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", comp_itemization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->comp_itemization:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 461
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", void_itemization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->void_itemization:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 462
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", surcharge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$LineItems;->surcharge:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LineItems{"

    .line 463
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
