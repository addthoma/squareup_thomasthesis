.class public final enum Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;
.super Ljava/lang/Enum;
.source "AddedTender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LoyaltyProgramType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType$ProtoAdapter_LoyaltyProgramType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum LEGACY:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

.field public static final enum PRIMARY:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

.field public static final enum UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1084
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    .line 1089
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    const/4 v2, 0x1

    const-string v3, "PRIMARY"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->PRIMARY:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    .line 1094
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    const/4 v3, 0x2

    const-string v4, "LEGACY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->LEGACY:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    .line 1083
    sget-object v4, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->PRIMARY:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->LEGACY:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->$VALUES:[Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    .line 1096
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType$ProtoAdapter_LoyaltyProgramType;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType$ProtoAdapter_LoyaltyProgramType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1100
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1101
    iput p3, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1111
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->LEGACY:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    return-object p0

    .line 1110
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->PRIMARY:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    return-object p0

    .line 1109
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->UNKNOWN_DO_NOT_USE:Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;
    .locals 1

    .line 1083
    const-class v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;
    .locals 1

    .line 1083
    sget-object v0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->$VALUES:[Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1118
    iget v0, p0, Lcom/squareup/protos/client/bills/AddedTender$LoyaltyStatus$LoyaltyProgramType;->value:I

    return v0
.end method
