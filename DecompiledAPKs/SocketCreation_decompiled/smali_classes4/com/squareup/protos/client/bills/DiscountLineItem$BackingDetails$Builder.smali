.class public final Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DiscountLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;",
        "Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public backing_type:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

.field public coupon_id:Ljava/lang/String;

.field public coupon_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public discount:Lcom/squareup/api/items/Discount;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 620
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 621
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public backing_type(Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;)Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;
    .locals 0

    .line 649
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->backing_type:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;
    .locals 7

    .line 668
    new-instance v6, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->discount:Lcom/squareup/api/items/Discount;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->backing_type:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_ids:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;-><init>(Lcom/squareup/api/items/Discount;Ljava/lang/String;Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$BackingType;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 611
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->build()Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    move-result-object v0

    return-object v0
.end method

.method public coupon_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 640
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_id:Ljava/lang/String;

    return-object p0
.end method

.method public coupon_ids(Ljava/util/List;)Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;"
        }
    .end annotation

    .line 661
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 662
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->coupon_ids:Ljava/util/List;

    return-object p0
.end method

.method public discount(Lcom/squareup/api/items/Discount;)Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;
    .locals 0

    .line 628
    iput-object p1, p0, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails$Builder;->discount:Lcom/squareup/api/items/Discount;

    return-object p0
.end method
