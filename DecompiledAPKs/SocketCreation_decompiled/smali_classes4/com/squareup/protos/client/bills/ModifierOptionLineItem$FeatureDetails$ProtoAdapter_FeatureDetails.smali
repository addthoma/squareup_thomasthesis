.class final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ProtoAdapter_FeatureDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FeatureDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1190
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1209
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;-><init>()V

    .line 1210
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1211
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 1216
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1214
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->quantities(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    goto :goto_0

    .line 1213
    :cond_1
    iget-object v3, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->conversational_mode:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1220
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1221
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1188
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ProtoAdapter_FeatureDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1202
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->conversational_mode:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1203
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1204
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1188
    check-cast p2, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ProtoAdapter_FeatureDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)I
    .locals 4

    .line 1195
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->conversational_mode:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    const/4 v3, 0x2

    .line 1196
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1197
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1188
    check-cast p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ProtoAdapter_FeatureDetails;->encodedSize(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;
    .locals 2

    .line 1226
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;

    move-result-object p1

    .line 1227
    iget-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->conversational_mode:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ConversationalMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1228
    iget-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->quantities:Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    .line 1229
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1230
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1188
    check-cast p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$ProtoAdapter_FeatureDetails;->redact(Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;

    move-result-object p1

    return-object p1
.end method
