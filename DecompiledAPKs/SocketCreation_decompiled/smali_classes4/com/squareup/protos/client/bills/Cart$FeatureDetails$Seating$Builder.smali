.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cover_count_change_log:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;",
            ">;"
        }
    .end annotation
.end field

.field public read_only_cover_count:Ljava/lang/Integer;

.field public seat:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 4168
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 4169
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->seat:Ljava/util/List;

    .line 4170
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->cover_count_change_log:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;
    .locals 5

    .line 4196
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->seat:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->cover_count_change_log:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->read_only_cover_count:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4161
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    move-result-object v0

    return-object v0
.end method

.method public cover_count_change_log(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;"
        }
    .end annotation

    .line 4180
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 4181
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->cover_count_change_log:Ljava/util/List;

    return-object p0
.end method

.method public read_only_cover_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;
    .locals 0

    .line 4190
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->read_only_cover_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public seat(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Seat;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;"
        }
    .end annotation

    .line 4174
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 4175
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating$Builder;->seat:Ljava/util/List;

    return-object p0
.end method
