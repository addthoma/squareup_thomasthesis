.class public final Lcom/squareup/protos/client/bills/StoredInstrument$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StoredInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/StoredInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/StoredInstrument;",
        "Lcom/squareup/protos/client/bills/StoredInstrument$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_token:Ljava/lang/String;

.field public customer_token:Ljava/lang/String;

.field public instrument_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 125
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/StoredInstrument;
    .locals 5

    .line 156
    new-instance v0, Lcom/squareup/protos/client/bills/StoredInstrument;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/StoredInstrument$Builder;->instrument_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/StoredInstrument$Builder;->customer_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/StoredInstrument$Builder;->contact_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/StoredInstrument;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/StoredInstrument$Builder;->build()Lcom/squareup/protos/client/bills/StoredInstrument;

    move-result-object v0

    return-object v0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/StoredInstrument$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/bills/StoredInstrument$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public customer_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/StoredInstrument$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/client/bills/StoredInstrument$Builder;->customer_token:Ljava/lang/String;

    return-object p0
.end method

.method public instrument_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/StoredInstrument$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/bills/StoredInstrument$Builder;->instrument_token:Ljava/lang/String;

    return-object p0
.end method
