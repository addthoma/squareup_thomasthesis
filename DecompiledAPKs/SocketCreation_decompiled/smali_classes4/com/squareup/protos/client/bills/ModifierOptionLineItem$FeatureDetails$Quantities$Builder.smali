.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public modifier_quantity:Ljava/lang/String;

.field public modifier_quantity_times_itemization_quantity:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1112
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;
    .locals 4

    .line 1139
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->modifier_quantity:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->modifier_quantity_times_itemization_quantity:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1107
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->build()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    move-result-object v0

    return-object v0
.end method

.method public modifier_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;
    .locals 0

    .line 1119
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->modifier_quantity:Ljava/lang/String;

    return-object p0
.end method

.method public modifier_quantity_times_itemization_quantity(Ljava/lang/String;)Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;
    .locals 0

    .line 1133
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->modifier_quantity_times_itemization_quantity:Ljava/lang/String;

    return-object p0
.end method
