.class public final Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddedTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;",
        "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

.field public phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

.field public receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 361
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;
    .locals 5

    .line 381
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;-><init>(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 354
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->build()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;

    move-result-object v0

    return-object v0
.end method

.method public email(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;
    .locals 0

    .line 370
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    return-object p0
.end method

.method public phone(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;
    .locals 0

    .line 375
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    return-object p0
.end method

.method public receipt_behavior(Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;
    .locals 0

    .line 365
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Builder;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    return-object p0
.end method
