.class public final Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/CardData$R4Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/CardData$R4Card;",
        "Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public encrypted_swipe_data:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1206
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/CardData$R4Card;
    .locals 3

    .line 1219
    new-instance v0, Lcom/squareup/protos/client/bills/CardData$R4Card;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;->encrypted_swipe_data:Lokio/ByteString;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/CardData$R4Card;-><init>(Lokio/ByteString;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1203
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$R4Card;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;
    .locals 0

    .line 1213
    iput-object p1, p0, Lcom/squareup/protos/client/bills/CardData$R4Card$Builder;->encrypted_swipe_data:Lokio/ByteString;

    return-object p0
.end method
