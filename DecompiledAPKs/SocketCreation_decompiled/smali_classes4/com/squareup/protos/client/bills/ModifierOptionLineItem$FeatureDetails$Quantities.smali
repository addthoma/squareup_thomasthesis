.class public final Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;
.super Lcom/squareup/wire/Message;
.source "ModifierOptionLineItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Quantities"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$ProtoAdapter_Quantities;,
        Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;",
        "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MODIFIER_QUANTITY:Ljava/lang/String; = ""

.field public static final DEFAULT_MODIFIER_QUANTITY_TIMES_ITEMIZATION_QUANTITY:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final modifier_quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final modifier_quantity_times_itemization_quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1025
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$ProtoAdapter_Quantities;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$ProtoAdapter_Quantities;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1058
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 1063
    sget-object v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 1064
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity:Ljava/lang/String;

    .line 1065
    iput-object p2, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity_times_itemization_quantity:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 1080
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 1081
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;

    .line 1082
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity:Ljava/lang/String;

    .line 1083
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity_times_itemization_quantity:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity_times_itemization_quantity:Ljava/lang/String;

    .line 1084
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1089
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 1091
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 1092
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 1093
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity_times_itemization_quantity:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 1094
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;
    .locals 2

    .line 1070
    new-instance v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;-><init>()V

    .line 1071
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->modifier_quantity:Ljava/lang/String;

    .line 1072
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity_times_itemization_quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->modifier_quantity_times_itemization_quantity:Ljava/lang/String;

    .line 1073
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 1024
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->newBuilder()Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1102
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", modifier_quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1103
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity_times_itemization_quantity:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", modifier_quantity_times_itemization_quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ModifierOptionLineItem$FeatureDetails$Quantities;->modifier_quantity_times_itemization_quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Quantities{"

    .line 1104
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
