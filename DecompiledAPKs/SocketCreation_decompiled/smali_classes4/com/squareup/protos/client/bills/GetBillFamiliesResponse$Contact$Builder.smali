.class public final Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBillFamiliesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_token:Ljava/lang/String;

.field public display_name:Ljava/lang/String;

.field public tender_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 474
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 475
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;->tender_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;
    .locals 5

    .line 499
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;->contact_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;->display_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;->tender_tokens:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 467
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact;

    move-result-object v0

    return-object v0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;
    .locals 0

    .line 479
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;
    .locals 0

    .line 484
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public tender_tokens(Ljava/util/List;)Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;"
        }
    .end annotation

    .line 492
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 493
    iput-object p1, p0, Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$Contact$Builder;->tender_tokens:Ljava/util/List;

    return-object p0
.end method
