.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;
.super Lcom/squareup/wire/Message;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BillPrintState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$ProtoAdapter_BillPrintState;,
        Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_READ_ONLY_PRINTED:Ljava/lang/Boolean;

.field public static final DEFAULT_WRITE_ONLY_PRINTED:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final read_only_printed:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final write_only_printed:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 4558
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$ProtoAdapter_BillPrintState;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$ProtoAdapter_BillPrintState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 4562
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->DEFAULT_WRITE_ONLY_PRINTED:Ljava/lang/Boolean;

    .line 4564
    sput-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->DEFAULT_READ_ONLY_PRINTED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1

    .line 4596
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;-><init>(Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 4601
    sget-object v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 4602
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 4603
    iput-object p2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_printed:Ljava/lang/Boolean;

    .line 4604
    iput-object p3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->read_only_printed:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 4620
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 4621
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    .line 4622
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 4623
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_printed:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_printed:Ljava/lang/Boolean;

    .line 4624
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->read_only_printed:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->read_only_printed:Ljava/lang/Boolean;

    .line 4625
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 4630
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 4632
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 4633
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4634
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_printed:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 4635
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->read_only_printed:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 4636
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;
    .locals 2

    .line 4609
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;-><init>()V

    .line 4610
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 4611
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_printed:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_printed:Ljava/lang/Boolean;

    .line 4612
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->read_only_printed:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->read_only_printed:Ljava/lang/Boolean;

    .line 4613
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 4557
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 4643
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4644
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_0

    const-string v1, ", write_only_client_printed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4645
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_printed:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", write_only_printed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_printed:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 4646
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->read_only_printed:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", read_only_printed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->read_only_printed:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BillPrintState{"

    .line 4647
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
