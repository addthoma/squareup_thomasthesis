.class final Lcom/squareup/protos/client/bills/AddTendersRequest$ProtoAdapter_AddTendersRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AddTendersRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddTendersRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AddTendersRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/AddTendersRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 657
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/AddTendersRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/AddTendersRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 688
    new-instance v0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;-><init>()V

    .line 689
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 690
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 701
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 699
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_group_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    goto :goto_0

    .line 698
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info_loyalty_options(Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    goto :goto_0

    .line 697
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->square_product_attributes(Lcom/squareup/protos/client/bills/SquareProductAttributes;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    goto :goto_0

    .line 696
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    goto :goto_0

    .line 695
    :pswitch_5
    iget-object v3, v0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 694
    :pswitch_6
    iget-object v3, v0, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->add_tender:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/bills/AddTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 693
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    goto :goto_0

    .line 692
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    goto :goto_0

    .line 705
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 706
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 655
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$ProtoAdapter_AddTendersRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/AddTendersRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 675
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTendersRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 676
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 677
    sget-object v0, Lcom/squareup/protos/client/bills/AddTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 678
    sget-object v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 679
    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 680
    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 681
    sget-object v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 682
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_group_token:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 683
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/AddTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 655
    check-cast p2, Lcom/squareup/protos/client/bills/AddTendersRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/AddTendersRequest$ProtoAdapter_AddTendersRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/AddTendersRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/AddTendersRequest;)I
    .locals 4

    .line 662
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v3, 0x2

    .line 663
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/AddTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 664
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->add_tender:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 665
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->cart:Lcom/squareup/protos/client/bills/Cart;

    const/4 v3, 0x6

    .line 666
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    const/4 v3, 0x7

    .line 667
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    const/16 v3, 0x8

    .line 668
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddTendersRequest;->tender_group_token:Ljava/lang/String;

    const/16 v3, 0x9

    .line 669
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 670
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 655
    check-cast p1, Lcom/squareup/protos/client/bills/AddTendersRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$ProtoAdapter_AddTendersRequest;->encodedSize(Lcom/squareup/protos/client/bills/AddTendersRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/protos/client/bills/AddTendersRequest;
    .locals 2

    .line 711
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersRequest;->newBuilder()Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;

    move-result-object p1

    .line 712
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 713
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->add_tender:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/AddTender;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 714
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 715
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bills/Cart;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    .line 716
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/SquareProductAttributes;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->square_product_attributes:Lcom/squareup/protos/client/bills/SquareProductAttributes;

    .line 717
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->tender_info_loyalty_options:Lcom/squareup/protos/client/bills/AddTendersRequest$TenderInfoLoyaltyOptions;

    .line 718
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 719
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 655
    check-cast p1, Lcom/squareup/protos/client/bills/AddTendersRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/AddTendersRequest$ProtoAdapter_AddTendersRequest;->redact(Lcom/squareup/protos/client/bills/AddTendersRequest;)Lcom/squareup/protos/client/bills/AddTendersRequest;

    move-result-object p1

    return-object p1
.end method
