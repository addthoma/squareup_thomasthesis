.class public final Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;
.super Lcom/squareup/wire/Message;
.source "ElectronicSignatureDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$ProtoAdapter_ElectronicSignatureDetails;,
        Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;",
        "Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CERTIFICATE_VERSION:Ljava/lang/Integer;

.field public static final DEFAULT_DISPLAY_SIGNATURE:Ljava/lang/String; = ""

.field public static final DEFAULT_ELECTRONIC_SIGNATURE:Ljava/lang/String; = ""

.field public static final DEFAULT_SIGNATURE_SEQUENCE_NUMBER:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final certificate_version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x1
    .end annotation
.end field

.field public final display_signature:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final electronic_signature:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final signature_sequence_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x3
    .end annotation
.end field

.field public final signed_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$ProtoAdapter_ElectronicSignatureDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$ProtoAdapter_ElectronicSignatureDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 30
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->DEFAULT_CERTIFICATE_VERSION:Ljava/lang/Integer;

    const-wide/16 v0, 0x0

    .line 34
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->DEFAULT_SIGNATURE_SEQUENCE_NUMBER:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;)V
    .locals 7

    .line 85
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V
    .locals 1

    .line 91
    sget-object v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 92
    iput-object p1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->certificate_version:Ljava/lang/Integer;

    .line 93
    iput-object p2, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->electronic_signature:Ljava/lang/String;

    .line 94
    iput-object p3, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signature_sequence_number:Ljava/lang/Long;

    .line 95
    iput-object p4, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->display_signature:Ljava/lang/String;

    .line 96
    iput-object p5, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signed_at:Lcom/squareup/protos/client/ISO8601Date;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 114
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 115
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->certificate_version:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->certificate_version:Ljava/lang/Integer;

    .line 117
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->electronic_signature:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->electronic_signature:Ljava/lang/String;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signature_sequence_number:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signature_sequence_number:Ljava/lang/Long;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->display_signature:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->display_signature:Ljava/lang/String;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signed_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 121
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 126
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 128
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->certificate_version:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->electronic_signature:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signature_sequence_number:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->display_signature:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 134
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;
    .locals 2

    .line 101
    new-instance v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;-><init>()V

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->certificate_version:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->certificate_version:Ljava/lang/Integer;

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->electronic_signature:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->electronic_signature:Ljava/lang/String;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signature_sequence_number:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->signature_sequence_number:Ljava/lang/Long;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->display_signature:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->display_signature:Ljava/lang/String;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signed_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->signed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/ElectronicSignatureDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->certificate_version:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", certificate_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->certificate_version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 143
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->electronic_signature:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", electronic_signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->electronic_signature:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signature_sequence_number:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", signature_sequence_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signature_sequence_number:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 145
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->display_signature:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", display_signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->display_signature:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    const-string v1, ", signed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/ElectronicSignatureDetails;->signed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ElectronicSignatureDetails{"

    .line 147
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
