.class public final Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddedTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;",
        "Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public email_id:Ljava/lang/String;

.field public obfuscated_email:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 507
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;
    .locals 4

    .line 525
    new-instance v0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email$Builder;->email_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email$Builder;->obfuscated_email:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 502
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email$Builder;->build()Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    move-result-object v0

    return-object v0
.end method

.method public email_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email$Builder;
    .locals 0

    .line 511
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email$Builder;->email_id:Ljava/lang/String;

    return-object p0
.end method

.method public obfuscated_email(Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email$Builder;
    .locals 0

    .line 519
    iput-object p1, p0, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email$Builder;->obfuscated_email:Ljava/lang/String;

    return-object p0
.end method
