.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bill_server_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5544
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bill_server_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture$Builder;
    .locals 0

    .line 5551
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture$Builder;->bill_server_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;
    .locals 3

    .line 5557
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture$Builder;->bill_server_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5541
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AuthAndDelayedCapture;

    move-result-object v0

    return-object v0
.end method
