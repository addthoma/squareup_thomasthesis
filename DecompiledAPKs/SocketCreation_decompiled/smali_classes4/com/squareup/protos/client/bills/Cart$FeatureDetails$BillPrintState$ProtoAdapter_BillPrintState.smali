.class final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$ProtoAdapter_BillPrintState;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BillPrintState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 4693
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4714
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;-><init>()V

    .line 4715
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 4716
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 4722
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 4720
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->read_only_printed(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;

    goto :goto_0

    .line 4719
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_printed(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;

    goto :goto_0

    .line 4718
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_client_printed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;

    goto :goto_0

    .line 4726
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 4727
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4691
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$ProtoAdapter_BillPrintState;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4706
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4707
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_printed:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4708
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->read_only_printed:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 4709
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 4691
    check-cast p2, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$ProtoAdapter_BillPrintState;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;)I
    .locals 4

    .line 4698
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->write_only_printed:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 4699
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->read_only_printed:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 4700
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4701
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 4691
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$ProtoAdapter_BillPrintState;->encodedSize(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;
    .locals 2

    .line 4732
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;

    move-result-object p1

    .line 4733
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_client_printed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 4734
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 4735
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 4691
    check-cast p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$ProtoAdapter_BillPrintState;->redact(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    move-result-object p1

    return-object p1
.end method
