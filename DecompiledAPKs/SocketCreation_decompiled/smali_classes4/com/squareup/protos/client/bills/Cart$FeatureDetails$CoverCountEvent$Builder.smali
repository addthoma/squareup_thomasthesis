.class public final Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;",
        "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public count:Ljava/lang/Integer;

.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public event_id:Ljava/lang/String;

.field public event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

.field public is_confirmed:Ljava/lang/Boolean;

.field public ordinal:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5281
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;
    .locals 10

    .line 5343
    new-instance v9, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    iget-object v1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->event_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    iget-object v3, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->count:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->is_confirmed:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    iget-object v6, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v7, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->ordinal:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;Ljava/lang/Integer;Ljava/lang/Boolean;Lcom/squareup/protos/client/CreatorDetails;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5266
    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent;

    move-result-object v0

    return-object v0
.end method

.method public count(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;
    .locals 0

    .line 5304
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->count:Ljava/lang/Integer;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;
    .locals 0

    .line 5329
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;
    .locals 0

    .line 5321
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public event_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;
    .locals 0

    .line 5288
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->event_id:Ljava/lang/String;

    return-object p0
.end method

.method public event_type(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;
    .locals 0

    .line 5296
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->event_type:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$EventType;

    return-object p0
.end method

.method public is_confirmed(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 5313
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->is_confirmed:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;
    .locals 0

    .line 5337
    iput-object p1, p0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoverCountEvent$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method
