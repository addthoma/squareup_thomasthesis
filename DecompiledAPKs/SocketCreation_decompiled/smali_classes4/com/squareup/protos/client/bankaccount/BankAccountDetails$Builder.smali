.class public final Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BankAccountDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bankaccount/BankAccountDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bankaccount/BankAccountDetails;",
        "Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_name:Ljava/lang/String;

.field public account_number:Ljava/lang/String;

.field public account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

.field public primary_institution_number:Ljava/lang/String;

.field public secondary_institution_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 161
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public account_name(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_name:Ljava/lang/String;

    return-object p0
.end method

.method public account_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_number:Ljava/lang/String;

    return-object p0
.end method

.method public account_type(Lcom/squareup/protos/client/bankaccount/BankAccountType;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;
    .locals 8

    .line 207
    new-instance v7, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->primary_institution_number:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->secondary_institution_number:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_number:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountType;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->build()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object v0

    return-object v0
.end method

.method public primary_institution_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->primary_institution_number:Ljava/lang/String;

    return-object p0
.end method

.method public secondary_institution_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->secondary_institution_number:Ljava/lang/String;

    return-object p0
.end method
