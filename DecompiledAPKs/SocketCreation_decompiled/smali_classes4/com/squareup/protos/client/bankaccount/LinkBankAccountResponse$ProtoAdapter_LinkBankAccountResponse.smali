.class final Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$ProtoAdapter_LinkBankAccountResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LinkBankAccountResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LinkBankAccountResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 231
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 252
    new-instance v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;-><init>()V

    .line 253
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 254
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 267
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 260
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->error(Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 262
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 257
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->error_message(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;

    goto :goto_0

    .line 256
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;

    goto :goto_0

    .line 271
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 272
    invoke-virtual {v0}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->build()Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 229
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$ProtoAdapter_LinkBankAccountResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 244
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 245
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;->error_message:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 246
    sget-object v0, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;->error:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 247
    invoke-virtual {p2}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 229
    check-cast p2, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$ProtoAdapter_LinkBankAccountResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;)I
    .locals 4

    .line 236
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;->success:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;->error_message:Ljava/lang/String;

    const/4 v3, 0x2

    .line 237
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;->error:Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$LinkBankAccountError;

    const/4 v3, 0x3

    .line 238
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 239
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$ProtoAdapter_LinkBankAccountResponse;->encodedSize(Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;
    .locals 0

    .line 277
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;->newBuilder()Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;

    move-result-object p1

    .line 278
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 279
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$Builder;->build()Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 229
    check-cast p1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse$ProtoAdapter_LinkBankAccountResponse;->redact(Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;

    move-result-object p1

    return-object p1
.end method
