.class public final Lcom/squareup/protos/client/irf/Field;
.super Lcom/squareup/wire/Message;
.source "Field.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/irf/Field$ProtoAdapter_Field;,
        Lcom/squareup/protos/client/irf/Field$FieldType;,
        Lcom/squareup/protos/client/irf/Field$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/irf/Field;",
        "Lcom/squareup/protos/client/irf/Field$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/irf/Field;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FIELD_LABEL:Ljava/lang/String; = ""

.field public static final DEFAULT_FIELD_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_FIELD_TYPE:Lcom/squareup/protos/client/irf/Field$FieldType;

.field public static final DEFAULT_PROMPT:Ljava/lang/String; = ""

.field public static final DEFAULT_REQUIRED:Ljava/lang/Boolean;

.field public static final DEFAULT_TOOLTIP:Ljava/lang/String; = ""

.field public static final DEFAULT_VALUE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final field_label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final field_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final field_type:Lcom/squareup/protos/client/irf/Field$FieldType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.irf.Field$FieldType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final option:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.irf.Option#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Option;",
            ">;"
        }
    .end annotation
.end field

.field public final prompt:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final required:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final tooltip:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final visibility:Lcom/squareup/protos/client/irf/Visibility;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.irf.Visibility#ADAPTER"
        tag = 0x9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/irf/Field$ProtoAdapter_Field;

    invoke-direct {v0}, Lcom/squareup/protos/client/irf/Field$ProtoAdapter_Field;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/irf/Field;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/client/irf/Field$FieldType;->UNKNOWN:Lcom/squareup/protos/client/irf/Field$FieldType;

    sput-object v0, Lcom/squareup/protos/client/irf/Field;->DEFAULT_FIELD_TYPE:Lcom/squareup/protos/client/irf/Field$FieldType;

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/irf/Field;->DEFAULT_REQUIRED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/irf/Field$FieldType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/client/irf/Visibility;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/irf/Field$FieldType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Option;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/client/irf/Visibility;",
            ")V"
        }
    .end annotation

    .line 118
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/irf/Field;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/irf/Field$FieldType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/client/irf/Visibility;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/irf/Field$FieldType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Lcom/squareup/protos/client/irf/Visibility;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/irf/Field$FieldType;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Option;",
            ">;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/client/irf/Visibility;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 124
    sget-object v0, Lcom/squareup/protos/client/irf/Field;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Field;->field_name:Ljava/lang/String;

    .line 126
    iput-object p2, p0, Lcom/squareup/protos/client/irf/Field;->value:Ljava/lang/String;

    .line 127
    iput-object p3, p0, Lcom/squareup/protos/client/irf/Field;->field_type:Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 128
    iput-object p4, p0, Lcom/squareup/protos/client/irf/Field;->prompt:Ljava/lang/String;

    .line 129
    iput-object p5, p0, Lcom/squareup/protos/client/irf/Field;->field_label:Ljava/lang/String;

    .line 130
    iput-object p6, p0, Lcom/squareup/protos/client/irf/Field;->tooltip:Ljava/lang/String;

    const-string p1, "option"

    .line 131
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/irf/Field;->option:Ljava/util/List;

    .line 132
    iput-object p8, p0, Lcom/squareup/protos/client/irf/Field;->required:Ljava/lang/Boolean;

    .line 133
    iput-object p9, p0, Lcom/squareup/protos/client/irf/Field;->visibility:Lcom/squareup/protos/client/irf/Visibility;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 155
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/irf/Field;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 156
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/irf/Field;

    .line 157
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Field;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/irf/Field;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/Field;->field_name:Ljava/lang/String;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/Field;->value:Ljava/lang/String;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_type:Lcom/squareup/protos/client/irf/Field$FieldType;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/Field;->field_type:Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->prompt:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/Field;->prompt:Ljava/lang/String;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_label:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/Field;->field_label:Ljava/lang/String;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->tooltip:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/Field;->tooltip:Ljava/lang/String;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->option:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/Field;->option:Ljava/util/List;

    .line 164
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->required:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/irf/Field;->required:Ljava/lang/Boolean;

    .line 165
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->visibility:Lcom/squareup/protos/client/irf/Visibility;

    iget-object p1, p1, Lcom/squareup/protos/client/irf/Field;->visibility:Lcom/squareup/protos/client/irf/Visibility;

    .line 166
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 171
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 173
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Field;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->value:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_type:Lcom/squareup/protos/client/irf/Field$FieldType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/irf/Field$FieldType;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->prompt:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_label:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->tooltip:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->option:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->required:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->visibility:Lcom/squareup/protos/client/irf/Visibility;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/irf/Visibility;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 183
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/irf/Field$Builder;
    .locals 2

    .line 138
    new-instance v0, Lcom/squareup/protos/client/irf/Field$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/irf/Field$Builder;-><init>()V

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/Field$Builder;->field_name:Ljava/lang/String;

    .line 140
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/Field$Builder;->value:Ljava/lang/String;

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_type:Lcom/squareup/protos/client/irf/Field$FieldType;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/Field$Builder;->field_type:Lcom/squareup/protos/client/irf/Field$FieldType;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->prompt:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/Field$Builder;->prompt:Ljava/lang/String;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_label:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/Field$Builder;->field_label:Ljava/lang/String;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->tooltip:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/Field$Builder;->tooltip:Ljava/lang/String;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->option:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/irf/Field$Builder;->option:Ljava/util/List;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->required:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/Field$Builder;->required:Ljava/lang/Boolean;

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->visibility:Lcom/squareup/protos/client/irf/Visibility;

    iput-object v1, v0, Lcom/squareup/protos/client/irf/Field$Builder;->visibility:Lcom/squareup/protos/client/irf/Visibility;

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Field;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/irf/Field$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Field;->newBuilder()Lcom/squareup/protos/client/irf/Field$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 190
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", field_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->value:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_type:Lcom/squareup/protos/client/irf/Field$FieldType;

    if-eqz v1, :cond_2

    const-string v1, ", field_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_type:Lcom/squareup/protos/client/irf/Field$FieldType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->prompt:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", prompt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->prompt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_label:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", field_label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->field_label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->tooltip:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", tooltip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->tooltip:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->option:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->option:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 198
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->required:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", required="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->required:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 199
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->visibility:Lcom/squareup/protos/client/irf/Visibility;

    if-eqz v1, :cond_8

    const-string v1, ", visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Field;->visibility:Lcom/squareup/protos/client/irf/Visibility;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Field{"

    .line 200
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
