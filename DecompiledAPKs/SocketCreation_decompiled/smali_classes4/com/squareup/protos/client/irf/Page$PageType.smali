.class public final enum Lcom/squareup/protos/client/irf/Page$PageType;
.super Ljava/lang/Enum;
.source "Page.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/irf/Page;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PageType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/irf/Page$PageType$ProtoAdapter_PageType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/irf/Page$PageType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/irf/Page$PageType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/irf/Page$PageType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CONFIRMATION:Lcom/squareup/protos/client/irf/Page$PageType;

.field public static final enum LANDING:Lcom/squareup/protos/client/irf/Page$PageType;

.field public static final enum NORMAL:Lcom/squareup/protos/client/irf/Page$PageType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/irf/Page$PageType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 138
    new-instance v0, Lcom/squareup/protos/client/irf/Page$PageType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/irf/Page$PageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Page$PageType;->UNKNOWN:Lcom/squareup/protos/client/irf/Page$PageType;

    .line 140
    new-instance v0, Lcom/squareup/protos/client/irf/Page$PageType;

    const/4 v2, 0x1

    const-string v3, "LANDING"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/irf/Page$PageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Page$PageType;->LANDING:Lcom/squareup/protos/client/irf/Page$PageType;

    .line 142
    new-instance v0, Lcom/squareup/protos/client/irf/Page$PageType;

    const/4 v3, 0x2

    const-string v4, "NORMAL"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/irf/Page$PageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Page$PageType;->NORMAL:Lcom/squareup/protos/client/irf/Page$PageType;

    .line 144
    new-instance v0, Lcom/squareup/protos/client/irf/Page$PageType;

    const/4 v4, 0x3

    const-string v5, "CONFIRMATION"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/irf/Page$PageType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/irf/Page$PageType;->CONFIRMATION:Lcom/squareup/protos/client/irf/Page$PageType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/irf/Page$PageType;

    .line 137
    sget-object v5, Lcom/squareup/protos/client/irf/Page$PageType;->UNKNOWN:Lcom/squareup/protos/client/irf/Page$PageType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/irf/Page$PageType;->LANDING:Lcom/squareup/protos/client/irf/Page$PageType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/irf/Page$PageType;->NORMAL:Lcom/squareup/protos/client/irf/Page$PageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/irf/Page$PageType;->CONFIRMATION:Lcom/squareup/protos/client/irf/Page$PageType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/irf/Page$PageType;->$VALUES:[Lcom/squareup/protos/client/irf/Page$PageType;

    .line 146
    new-instance v0, Lcom/squareup/protos/client/irf/Page$PageType$ProtoAdapter_PageType;

    invoke-direct {v0}, Lcom/squareup/protos/client/irf/Page$PageType$ProtoAdapter_PageType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/irf/Page$PageType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 150
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 151
    iput p3, p0, Lcom/squareup/protos/client/irf/Page$PageType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/irf/Page$PageType;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 162
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/irf/Page$PageType;->CONFIRMATION:Lcom/squareup/protos/client/irf/Page$PageType;

    return-object p0

    .line 161
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/irf/Page$PageType;->NORMAL:Lcom/squareup/protos/client/irf/Page$PageType;

    return-object p0

    .line 160
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/irf/Page$PageType;->LANDING:Lcom/squareup/protos/client/irf/Page$PageType;

    return-object p0

    .line 159
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/irf/Page$PageType;->UNKNOWN:Lcom/squareup/protos/client/irf/Page$PageType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Page$PageType;
    .locals 1

    .line 137
    const-class v0, Lcom/squareup/protos/client/irf/Page$PageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/irf/Page$PageType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/irf/Page$PageType;
    .locals 1

    .line 137
    sget-object v0, Lcom/squareup/protos/client/irf/Page$PageType;->$VALUES:[Lcom/squareup/protos/client/irf/Page$PageType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/irf/Page$PageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/irf/Page$PageType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 169
    iget v0, p0, Lcom/squareup/protos/client/irf/Page$PageType;->value:I

    return v0
.end method
