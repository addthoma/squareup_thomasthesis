.class public final Lcom/squareup/protos/client/irf/Page$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Page.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/irf/Page;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/irf/Page;",
        "Lcom/squareup/protos/client/irf/Page$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public page_type:Lcom/squareup/protos/client/irf/Page$PageType;

.field public section:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Section;",
            ">;"
        }
    .end annotation
.end field

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 112
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/irf/Page$Builder;->section:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/irf/Page;
    .locals 5

    .line 133
    new-instance v0, Lcom/squareup/protos/client/irf/Page;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Page$Builder;->page_type:Lcom/squareup/protos/client/irf/Page$PageType;

    iget-object v2, p0, Lcom/squareup/protos/client/irf/Page$Builder;->title:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/irf/Page$Builder;->section:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/irf/Page;-><init>(Lcom/squareup/protos/client/irf/Page$PageType;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Page$Builder;->build()Lcom/squareup/protos/client/irf/Page;

    move-result-object v0

    return-object v0
.end method

.method public page_type(Lcom/squareup/protos/client/irf/Page$PageType;)Lcom/squareup/protos/client/irf/Page$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Page$Builder;->page_type:Lcom/squareup/protos/client/irf/Page$PageType;

    return-object p0
.end method

.method public section(Ljava/util/List;)Lcom/squareup/protos/client/irf/Page$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/irf/Section;",
            ">;)",
            "Lcom/squareup/protos/client/irf/Page$Builder;"
        }
    .end annotation

    .line 126
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Page$Builder;->section:Ljava/util/List;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Page$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Page$Builder;->title:Ljava/lang/String;

    return-object p0
.end method
