.class public final Lcom/squareup/protos/client/irf/Criteria$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Criteria.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/irf/Criteria;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/irf/Criteria;",
        "Lcom/squareup/protos/client/irf/Criteria$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public conditional_target:Ljava/lang/String;

.field public criteria_type:Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 112
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/irf/Criteria;
    .locals 5

    .line 132
    new-instance v0, Lcom/squareup/protos/client/irf/Criteria;

    iget-object v1, p0, Lcom/squareup/protos/client/irf/Criteria$Builder;->conditional_target:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/irf/Criteria$Builder;->value:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/irf/Criteria$Builder;->criteria_type:Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/irf/Criteria;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/irf/Criteria$CriteriaType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/client/irf/Criteria$Builder;->build()Lcom/squareup/protos/client/irf/Criteria;

    move-result-object v0

    return-object v0
.end method

.method public conditional_target(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Criteria$Builder;
    .locals 0

    .line 116
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Criteria$Builder;->conditional_target:Ljava/lang/String;

    return-object p0
.end method

.method public criteria_type(Lcom/squareup/protos/client/irf/Criteria$CriteriaType;)Lcom/squareup/protos/client/irf/Criteria$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Criteria$Builder;->criteria_type:Lcom/squareup/protos/client/irf/Criteria$CriteriaType;

    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/squareup/protos/client/irf/Criteria$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/protos/client/irf/Criteria$Builder;->value:Ljava/lang/String;

    return-object p0
.end method
