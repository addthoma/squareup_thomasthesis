.class public final enum Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;
.super Ljava/lang/Enum;
.source "InventoryProductType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType$ProtoAdapter_InventoryProductType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_USE_PRODUCT_TYPE:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

.field public static final enum ITEM:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

.field public static final enum ITEM_VARIATION:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 14
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE_PRODUCT_TYPE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->DO_NOT_USE_PRODUCT_TYPE:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    .line 16
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    const/4 v2, 0x1

    const-string v3, "ITEM"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->ITEM:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    .line 18
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    const/4 v3, 0x2

    const-string v4, "ITEM_VARIATION"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->ITEM_VARIATION:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    .line 13
    sget-object v4, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->DO_NOT_USE_PRODUCT_TYPE:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->ITEM:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->ITEM_VARIATION:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->$VALUES:[Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    .line 20
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType$ProtoAdapter_InventoryProductType;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType$ProtoAdapter_InventoryProductType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput p3, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 35
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->ITEM_VARIATION:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    return-object p0

    .line 34
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->ITEM:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    return-object p0

    .line 33
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->DO_NOT_USE_PRODUCT_TYPE:Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->$VALUES:[Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 42
    iget v0, p0, Lcom/squareup/protos/client/posfe/inventory/sync/InventoryProductType;->value:I

    return v0
.end method
