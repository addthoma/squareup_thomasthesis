.class public final Lcom/squareup/protos/client/posfe/inventory/sync/Request;
.super Lcom/squareup/wire/Message;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/posfe/inventory/sync/Request$ProtoAdapter_Request;,
        Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/posfe/inventory/sync/Request;",
        "Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/posfe/inventory/sync/Request;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.posfe.inventory.sync.GetRequest#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.posfe.inventory.sync.PutRequest#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.posfe.inventory.sync.RequestScope#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$ProtoAdapter_Request;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$ProtoAdapter_Request;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;)V
    .locals 1

    .line 43
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/posfe/inventory/sync/Request;-><init>(Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;Lokio/ByteString;)V
    .locals 1

    .line 48
    sget-object v0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 49
    invoke-static {p2, p3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p4

    const/4 v0, 0x1

    if-gt p4, v0, :cond_0

    .line 52
    iput-object p1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    .line 53
    iput-object p2, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    .line 54
    iput-object p3, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    return-void

    .line 50
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of get_request, put_request may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 70
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 71
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request;

    .line 72
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    iget-object v3, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    .line 73
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    iget-object v3, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    .line 74
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    iget-object p1, p1, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    .line 75
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 80
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 86
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;
    .locals 2

    .line 59
    new-instance v0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;-><init>()V

    .line 60
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    .line 61
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    .line 62
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    iput-object v1, v0, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    .line 63
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->newBuilder()Lcom/squareup/protos/client/posfe/inventory/sync/Request$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    if-eqz v1, :cond_0

    const-string v1, ", scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->scope:Lcom/squareup/protos/client/posfe/inventory/sync/RequestScope;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 95
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    if-eqz v1, :cond_1

    const-string v1, ", get_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->get_request:Lcom/squareup/protos/client/posfe/inventory/sync/GetRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 96
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    if-eqz v1, :cond_2

    const-string v1, ", put_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/posfe/inventory/sync/Request;->put_request:Lcom/squareup/protos/client/posfe/inventory/sync/PutRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Request{"

    .line 97
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
