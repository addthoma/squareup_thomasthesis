.class public final Lcom/squareup/protos/client/dialogue/Message$Data$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Message.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/dialogue/Message$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/dialogue/Message$Data;",
        "Lcom/squareup/protos/client/dialogue/Message$Data$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public comment:Lcom/squareup/protos/client/dialogue/Comment;

.field public coupon:Lcom/squareup/protos/client/coupons/Coupon;

.field public transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 379
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/dialogue/Message$Data;
    .locals 5

    .line 399
    new-instance v0, Lcom/squareup/protos/client/dialogue/Message$Data;

    iget-object v1, p0, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    iget-object v2, p0, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    iget-object v3, p0, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/dialogue/Message$Data;-><init>(Lcom/squareup/protos/client/dialogue/Comment;Lcom/squareup/protos/client/coupons/Coupon;Lcom/squareup/protos/client/dialogue/Message$Transaction;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 372
    invoke-virtual {p0}, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->build()Lcom/squareup/protos/client/dialogue/Message$Data;

    move-result-object v0

    return-object v0
.end method

.method public comment(Lcom/squareup/protos/client/dialogue/Comment;)Lcom/squareup/protos/client/dialogue/Message$Data$Builder;
    .locals 0

    .line 383
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->comment:Lcom/squareup/protos/client/dialogue/Comment;

    return-object p0
.end method

.method public coupon(Lcom/squareup/protos/client/coupons/Coupon;)Lcom/squareup/protos/client/dialogue/Message$Data$Builder;
    .locals 0

    .line 388
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    return-object p0
.end method

.method public transaction(Lcom/squareup/protos/client/dialogue/Message$Transaction;)Lcom/squareup/protos/client/dialogue/Message$Data$Builder;
    .locals 0

    .line 393
    iput-object p1, p0, Lcom/squareup/protos/client/dialogue/Message$Data$Builder;->transaction:Lcom/squareup/protos/client/dialogue/Message$Transaction;

    return-object p0
.end method
