.class public final Lcom/squareup/protos/client/estimate/CancelEstimateRequest;
.super Lcom/squareup/wire/Message;
.source "CancelEstimateRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/estimate/CancelEstimateRequest$ProtoAdapter_CancelEstimateRequest;,
        Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/estimate/CancelEstimateRequest;",
        "Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/estimate/CancelEstimateRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SEND_EMAIL_TO_RECIPIENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final send_email_to_recipients:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final token_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$ProtoAdapter_CancelEstimateRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$ProtoAdapter_CancelEstimateRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 27
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->DEFAULT_VERSION:Ljava/lang/Integer;

    const/4 v0, 0x1

    .line 29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->DEFAULT_SEND_EMAIL_TO_RECIPIENTS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 1

    .line 51
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;-><init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Ljava/lang/Integer;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    .line 58
    iput-object p2, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->version:Ljava/lang/Integer;

    .line 59
    iput-object p3, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 75
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 76
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    .line 78
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->version:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->version:Ljava/lang/Integer;

    .line 79
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    .line 80
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 85
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 91
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;
    .locals 2

    .line 64
    new-instance v0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;-><init>()V

    .line 65
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    .line 66
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->version:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->version:Ljava/lang/Integer;

    .line 67
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->send_email_to_recipients:Ljava/lang/Boolean;

    .line 68
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->newBuilder()Lcom/squareup/protos/client/estimate/CancelEstimateRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", token_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->token_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 101
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", send_email_to_recipients="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/CancelEstimateRequest;->send_email_to_recipients:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CancelEstimateRequest{"

    .line 102
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
