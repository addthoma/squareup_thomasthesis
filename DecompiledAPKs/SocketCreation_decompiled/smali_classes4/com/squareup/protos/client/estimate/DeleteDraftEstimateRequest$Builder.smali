.class public final Lcom/squareup/protos/client/estimate/DeleteDraftEstimateRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeleteDraftEstimateRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/estimate/DeleteDraftEstimateRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/estimate/DeleteDraftEstimateRequest;",
        "Lcom/squareup/protos/client/estimate/DeleteDraftEstimateRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public token_pair:Lcom/squareup/protos/client/IdPair;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/estimate/DeleteDraftEstimateRequest;
    .locals 3

    .line 88
    new-instance v0, Lcom/squareup/protos/client/estimate/DeleteDraftEstimateRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/estimate/DeleteDraftEstimateRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/estimate/DeleteDraftEstimateRequest;-><init>(Lcom/squareup/protos/client/IdPair;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/client/estimate/DeleteDraftEstimateRequest$Builder;->build()Lcom/squareup/protos/client/estimate/DeleteDraftEstimateRequest;

    move-result-object v0

    return-object v0
.end method

.method public token_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/estimate/DeleteDraftEstimateRequest$Builder;
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/protos/client/estimate/DeleteDraftEstimateRequest$Builder;->token_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method
