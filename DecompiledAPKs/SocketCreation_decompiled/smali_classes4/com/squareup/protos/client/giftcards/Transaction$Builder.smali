.class public final Lcom/squareup/protos/client/giftcards/Transaction$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Transaction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/Transaction;",
        "Lcom/squareup/protos/client/giftcards/Transaction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount:Lcom/squareup/protos/common/Money;

.field public created_at:Lcom/squareup/protos/common/time/DateTime;

.field public description:Ljava/lang/String;

.field public id:Ljava/lang/Long;

.field public type:Lcom/squareup/protos/client/giftcards/Transaction$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 156
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/giftcards/Transaction$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/giftcards/Transaction;
    .locals 8

    .line 201
    new-instance v7, Lcom/squareup/protos/client/giftcards/Transaction;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->id:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->description:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->amount:Lcom/squareup/protos/common/Money;

    iget-object v5, p0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->type:Lcom/squareup/protos/client/giftcards/Transaction$Type;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/giftcards/Transaction;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/Transaction$Type;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 145
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->build()Lcom/squareup/protos/client/giftcards/Transaction;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/giftcards/Transaction$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/Transaction$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/Long;)Lcom/squareup/protos/client/giftcards/Transaction$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->id:Ljava/lang/Long;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/giftcards/Transaction$Type;)Lcom/squareup/protos/client/giftcards/Transaction$Builder;
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->type:Lcom/squareup/protos/client/giftcards/Transaction$Type;

    return-object p0
.end method
