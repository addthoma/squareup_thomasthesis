.class public final Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClearBalanceRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_request_uuid:Ljava/lang/String;

.field public gift_card_server_token:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;

.field public notes:Ljava/lang/String;

.field public reason:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 151
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;
    .locals 8

    .line 189
    new-instance v7, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->client_request_uuid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->gift_card_server_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->reason:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    iget-object v5, p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->notes:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 140
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->client_request_uuid:Ljava/lang/String;

    return-object p0
.end method

.method public gift_card_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->gift_card_server_token:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public notes(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->notes:Ljava/lang/String;

    return-object p0
.end method

.method public reason(Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;)Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Builder;->reason:Lcom/squareup/protos/client/giftcards/ClearBalanceRequest$Reason;

    return-object p0
.end method
