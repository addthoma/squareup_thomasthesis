.class public final Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetEGiftOrderConfigurationResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;",
        "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public all_egift_themes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation
.end field

.field public denomination_amounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field public order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 135
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 136
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->all_egift_themes:Ljava/util/List;

    .line 137
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->denomination_amounts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public all_egift_themes(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;)",
            "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;"
        }
    .end annotation

    .line 149
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->all_egift_themes:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;
    .locals 7

    .line 175
    new-instance v6, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->all_egift_themes:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iget-object v4, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->denomination_amounts:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->build()Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;

    move-result-object v0

    return-object v0
.end method

.method public denomination_amounts(Ljava/util/List;)Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;"
        }
    .end annotation

    .line 168
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->denomination_amounts:Ljava/util/List;

    return-object p0
.end method

.method public order_configuration(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;)Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
