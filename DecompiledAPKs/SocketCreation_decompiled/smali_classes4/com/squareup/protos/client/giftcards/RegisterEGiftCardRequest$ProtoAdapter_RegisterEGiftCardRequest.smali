.class final Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$ProtoAdapter_RegisterEGiftCardRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RegisterEGiftCardRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RegisterEGiftCardRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 207
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 232
    new-instance v0, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;-><init>()V

    .line 233
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 234
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 242
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 240
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    goto :goto_0

    .line 239
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    goto :goto_0

    .line 238
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->egift_theme_token(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    goto :goto_0

    .line 237
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->recipient_email(Ljava/lang/String;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    goto :goto_0

    .line 236
    :cond_4
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->delivery_date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    goto :goto_0

    .line 246
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 247
    invoke-virtual {v0}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$ProtoAdapter_RegisterEGiftCardRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 222
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->delivery_date:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 223
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->recipient_email:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 224
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->egift_theme_token:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 225
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->client_request_uuid:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 226
    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 227
    invoke-virtual {p2}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 205
    check-cast p2, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$ProtoAdapter_RegisterEGiftCardRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;)I
    .locals 4

    .line 212
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->delivery_date:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->recipient_email:Ljava/lang/String;

    const/4 v3, 0x2

    .line 213
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->egift_theme_token:Ljava/lang/String;

    const/4 v3, 0x3

    .line 214
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->client_request_uuid:Ljava/lang/String;

    const/4 v3, 0x4

    .line 215
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v3, 0x5

    .line 216
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 217
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 205
    check-cast p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$ProtoAdapter_RegisterEGiftCardRequest;->encodedSize(Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;
    .locals 2

    .line 252
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;->newBuilder()Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;

    move-result-object p1

    .line 253
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->delivery_date:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->delivery_date:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->delivery_date:Lcom/squareup/protos/common/time/DateTime;

    :cond_0
    const/4 v0, 0x0

    .line 254
    iput-object v0, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->recipient_email:Ljava/lang/String;

    .line 255
    iget-object v0, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/IdPair;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/IdPair;

    iput-object v0, p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 256
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 257
    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$Builder;->build()Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 205
    check-cast p1, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest$ProtoAdapter_RegisterEGiftCardRequest;->redact(Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;)Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;

    move-result-object p1

    return-object p1
.end method
