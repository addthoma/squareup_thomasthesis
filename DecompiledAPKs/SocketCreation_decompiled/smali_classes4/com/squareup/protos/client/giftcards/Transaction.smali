.class public final Lcom/squareup/protos/client/giftcards/Transaction;
.super Lcom/squareup/wire/Message;
.source "Transaction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/giftcards/Transaction$ProtoAdapter_Transaction;,
        Lcom/squareup/protos/client/giftcards/Transaction$Type;,
        Lcom/squareup/protos/client/giftcards/Transaction$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/giftcards/Transaction;",
        "Lcom/squareup/protos/client/giftcards/Transaction$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/giftcards/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/Long;

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/giftcards/Transaction$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final id:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/giftcards/Transaction$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.giftcards.Transaction$Type#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/protos/client/giftcards/Transaction$ProtoAdapter_Transaction;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/Transaction$ProtoAdapter_Transaction;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/giftcards/Transaction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 29
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/giftcards/Transaction;->DEFAULT_ID:Ljava/lang/Long;

    .line 33
    sget-object v0, Lcom/squareup/protos/client/giftcards/Transaction$Type;->UNKNOWN:Lcom/squareup/protos/client/giftcards/Transaction$Type;

    sput-object v0, Lcom/squareup/protos/client/giftcards/Transaction;->DEFAULT_TYPE:Lcom/squareup/protos/client/giftcards/Transaction$Type;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/Transaction$Type;)V
    .locals 7

    .line 81
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/giftcards/Transaction;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/Transaction$Type;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/giftcards/Transaction$Type;Lokio/ByteString;)V
    .locals 1

    .line 86
    sget-object v0, Lcom/squareup/protos/client/giftcards/Transaction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 87
    iput-object p1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->id:Ljava/lang/Long;

    .line 88
    iput-object p2, p0, Lcom/squareup/protos/client/giftcards/Transaction;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 89
    iput-object p3, p0, Lcom/squareup/protos/client/giftcards/Transaction;->description:Ljava/lang/String;

    .line 90
    iput-object p4, p0, Lcom/squareup/protos/client/giftcards/Transaction;->amount:Lcom/squareup/protos/common/Money;

    .line 91
    iput-object p5, p0, Lcom/squareup/protos/client/giftcards/Transaction;->type:Lcom/squareup/protos/client/giftcards/Transaction$Type;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 109
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/giftcards/Transaction;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 110
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/giftcards/Transaction;

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/Transaction;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/giftcards/Transaction;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->id:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/Transaction;->id:Ljava/lang/Long;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/Transaction;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/Transaction;->description:Ljava/lang/String;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/giftcards/Transaction;->amount:Lcom/squareup/protos/common/Money;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->type:Lcom/squareup/protos/client/giftcards/Transaction$Type;

    iget-object p1, p1, Lcom/squareup/protos/client/giftcards/Transaction;->type:Lcom/squareup/protos/client/giftcards/Transaction$Type;

    .line 116
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 121
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 123
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/Transaction;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->id:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->type:Lcom/squareup/protos/client/giftcards/Transaction$Type;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/giftcards/Transaction$Type;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 129
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/giftcards/Transaction$Builder;
    .locals 2

    .line 96
    new-instance v0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/giftcards/Transaction$Builder;-><init>()V

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->id:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->id:Ljava/lang/Long;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->description:Ljava/lang/String;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->type:Lcom/squareup/protos/client/giftcards/Transaction$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->type:Lcom/squareup/protos/client/giftcards/Transaction$Type;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/Transaction;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/giftcards/Transaction$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/client/giftcards/Transaction;->newBuilder()Lcom/squareup/protos/client/giftcards/Transaction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->id:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->id:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 138
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->description:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->type:Lcom/squareup/protos/client/giftcards/Transaction$Type;

    if-eqz v1, :cond_4

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/giftcards/Transaction;->type:Lcom/squareup/protos/client/giftcards/Transaction$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Transaction{"

    .line 142
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
