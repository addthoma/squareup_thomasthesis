.class public final Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetCardActivityRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public exclude_activity_types:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;"
        }
    .end annotation
.end field

.field public exclude_transaction_states:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;",
            ">;"
        }
    .end annotation
.end field

.field public include_activity_types:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;"
        }
    .end annotation
.end field

.field public include_transaction_states:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;",
            ">;"
        }
    .end annotation
.end field

.field public max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

.field public min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 419
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 420
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->exclude_activity_types:Ljava/util/List;

    .line 421
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->include_activity_types:Ljava/util/List;

    .line 422
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->include_transaction_states:Ljava/util/List;

    .line 423
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->exclude_transaction_states:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;
    .locals 9

    .line 484
    new-instance v8, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->exclude_activity_types:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->include_activity_types:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->include_transaction_states:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->exclude_transaction_states:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v6, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 406
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    move-result-object v0

    return-object v0
.end method

.method public exclude_activity_types(Ljava/util/List;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;)",
            "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;"
        }
    .end annotation

    .line 431
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 432
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->exclude_activity_types:Ljava/util/List;

    return-object p0
.end method

.method public exclude_transaction_states(Ljava/util/List;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;",
            ">;)",
            "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;"
        }
    .end annotation

    .line 461
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 462
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->exclude_transaction_states:Ljava/util/List;

    return-object p0
.end method

.method public include_activity_types(Ljava/util/List;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;)",
            "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;"
        }
    .end annotation

    .line 441
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 442
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->include_activity_types:Ljava/util/List;

    return-object p0
.end method

.method public include_transaction_states(Ljava/util/List;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;",
            ">;)",
            "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;"
        }
    .end annotation

    .line 451
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 452
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->include_transaction_states:Ljava/util/List;

    return-object p0
.end method

.method public max_occurred_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;
    .locals 0

    .line 478
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public min_occurred_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;
    .locals 0

    .line 470
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
