.class public final Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ProvisionDigitalWalletTokenResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public apple_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

.field public google_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 109
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public apple_pay_response(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->apple_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    const/4 p1, 0x0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->google_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->apple_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->google_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public google_pay_response(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->google_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

    const/4 p1, 0x0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->apple_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
