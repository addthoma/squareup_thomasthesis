.class final Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$ProtoAdapter_VerifyTwoFactorAuthenticationResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "VerifyTwoFactorAuthenticationResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_VerifyTwoFactorAuthenticationResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 153
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 171
    new-instance v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Builder;-><init>()V

    .line 172
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 173
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 184
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 177
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Builder;->result(Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;)Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 179
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 188
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 189
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 151
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$ProtoAdapter_VerifyTwoFactorAuthenticationResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 165
    sget-object v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;->result:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 166
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 151
    check-cast p2, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$ProtoAdapter_VerifyTwoFactorAuthenticationResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;)I
    .locals 3

    .line 158
    sget-object v0, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;->result:Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Result;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 159
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 151
    check-cast p1, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$ProtoAdapter_VerifyTwoFactorAuthenticationResponse;->encodedSize(Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;)Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;
    .locals 0

    .line 195
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Builder;

    move-result-object p1

    .line 196
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 197
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 151
    check-cast p1, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse$ProtoAdapter_VerifyTwoFactorAuthenticationResponse;->redact(Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;)Lcom/squareup/protos/client/bizbank/VerifyTwoFactorAuthenticationResponse;

    move-result-object p1

    return-object p1
.end method
