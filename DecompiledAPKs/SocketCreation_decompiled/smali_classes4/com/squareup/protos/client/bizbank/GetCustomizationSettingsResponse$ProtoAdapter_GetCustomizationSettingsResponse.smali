.class final Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$ProtoAdapter_GetCustomizationSettingsResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetCustomizationSettingsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetCustomizationSettingsResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 325
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 363
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;-><init>()V

    .line 364
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 365
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 386
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 384
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->idv_retries_remaining(Ljava/lang/Integer;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    goto :goto_0

    .line 383
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_birth_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    goto :goto_0

    .line 382
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    goto :goto_0

    .line 381
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->can_retry_idv(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    goto :goto_0

    .line 380
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->idv_needs_ssn(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    goto :goto_0

    .line 374
    :pswitch_5
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bizbank/IdvState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bizbank/IdvState;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->idv_state(Lcom/squareup/protos/client/bizbank/IdvState;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 376
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 371
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    goto :goto_0

    .line 370
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->max_ink_coverage(Ljava/lang/Float;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    goto :goto_0

    .line 369
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->min_ink_coverage(Ljava/lang/Float;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    goto/16 :goto_0

    .line 368
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->business_name(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    goto/16 :goto_0

    .line 367
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_name(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    goto/16 :goto_0

    .line 390
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 391
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 323
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$ProtoAdapter_GetCustomizationSettingsResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 347
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 348
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->business_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 349
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->min_ink_coverage:Ljava/lang/Float;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 350
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->max_ink_coverage:Ljava/lang/Float;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 351
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 352
    sget-object v0, Lcom/squareup/protos/client/bizbank/IdvState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 353
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_needs_ssn:Ljava/lang/Boolean;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 354
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->can_retry_idv:Ljava/lang/Boolean;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 355
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_retries_remaining:Ljava/lang/Integer;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 356
    sget-object v0, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 357
    sget-object v0, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 358
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 323
    check-cast p2, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$ProtoAdapter_GetCustomizationSettingsResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;)I
    .locals 4

    .line 330
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->business_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 331
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->min_ink_coverage:Ljava/lang/Float;

    const/4 v3, 0x3

    .line 332
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->max_ink_coverage:Ljava/lang/Float;

    const/4 v3, 0x4

    .line 333
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v3, 0x5

    .line 334
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/IdvState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_state:Lcom/squareup/protos/client/bizbank/IdvState;

    const/4 v3, 0x6

    .line 335
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_needs_ssn:Ljava/lang/Boolean;

    const/4 v3, 0x7

    .line 336
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->can_retry_idv:Ljava/lang/Boolean;

    const/16 v3, 0x8

    .line 337
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->idv_retries_remaining:Ljava/lang/Integer;

    const/16 v3, 0xb

    .line 338
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/16 v3, 0x9

    .line 339
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    const/16 v3, 0xa

    .line 340
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 341
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 323
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$ProtoAdapter_GetCustomizationSettingsResponse;->encodedSize(Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;
    .locals 3

    .line 396
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 397
    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_name:Ljava/lang/String;

    .line 398
    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->business_name:Ljava/lang/String;

    .line 399
    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/Status;

    iput-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 400
    :cond_0
    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 401
    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->owner_birth_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 402
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 403
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 323
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse$ProtoAdapter_GetCustomizationSettingsResponse;->redact(Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;)Lcom/squareup/protos/client/bizbank/GetCustomizationSettingsResponse;

    move-result-object p1

    return-object p1
.end method
