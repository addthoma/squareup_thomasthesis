.class public final Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
.super Lcom/squareup/wire/Message;
.source "ListCardsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ListCardsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CardData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$ProtoAdapter_CardData;,
        Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;,
        Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CARD_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field public static final DEFAULT_CARD_STATE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

.field public static final DEFAULT_CARD_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME_ON_CARD:Ljava/lang/String; = ""

.field public static final DEFAULT_PAN_LAST_FOUR:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bills.CardTender$Card$Brand#ADAPTER"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.ListCardsResponse$CardData$CardState#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final card_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final name_on_card:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final pan_last_four:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 118
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$ProtoAdapter_CardData;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$ProtoAdapter_CardData;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 124
    sget-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->UNKNOWN:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->DEFAULT_CARD_STATE:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 126
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    sput-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->DEFAULT_CARD_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 166
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;Lcom/squareup/protos/client/bills/CardTender$Card$Brand;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 171
    sget-object v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    .line 173
    iput-object p2, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 174
    iput-object p3, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 175
    iput-object p4, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    .line 176
    iput-object p5, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->name_on_card:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 194
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 195
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    .line 196
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    .line 197
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 198
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 199
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    .line 200
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->name_on_card:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->name_on_card:Ljava/lang/String;

    .line 201
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 206
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 208
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->name_on_card:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 214
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;
    .locals 2

    .line 181
    new-instance v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;-><init>()V

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_token:Ljava/lang/String;

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->pan_last_four:Ljava/lang/String;

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->name_on_card:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->name_on_card:Ljava/lang/String;

    .line 187
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->newBuilder()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", card_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    if-eqz v1, :cond_1

    const-string v1, ", card_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_state:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 224
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    if-eqz v1, :cond_2

    const-string v1, ", card_brand=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", pan_last_four="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->pan_last_four:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->name_on_card:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", name_on_card=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CardData{"

    .line 227
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
