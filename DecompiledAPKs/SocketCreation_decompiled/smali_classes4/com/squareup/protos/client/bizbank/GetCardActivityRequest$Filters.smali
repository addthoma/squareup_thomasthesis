.class public final Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;
.super Lcom/squareup/wire/Message;
.source "GetCardActivityRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Filters"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$ProtoAdapter_Filters;,
        Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final exclude_activity_types:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.CardActivityEvent$ActivityType#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;"
        }
    .end annotation
.end field

.field public final exclude_transaction_states:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.CardActivityEvent$TransactionState#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;",
            ">;"
        }
    .end annotation
.end field

.field public final include_activity_types:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.CardActivityEvent$ActivityType#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;"
        }
    .end annotation
.end field

.field public final include_transaction_states:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.bizbank.CardActivityEvent$TransactionState#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;",
            ">;"
        }
    .end annotation
.end field

.field public final max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.ISO8601Date#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 267
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$ProtoAdapter_Filters;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$ProtoAdapter_Filters;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;",
            ">;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            ")V"
        }
    .end annotation

    .line 334
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$TransactionState;",
            ">;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lcom/squareup/protos/client/ISO8601Date;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 342
    sget-object v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p7, "exclude_activity_types"

    .line 343
    invoke-static {p7, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_activity_types:Ljava/util/List;

    const-string p1, "include_activity_types"

    .line 344
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_activity_types:Ljava/util/List;

    const-string p1, "include_transaction_states"

    .line 345
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_transaction_states:Ljava/util/List;

    const-string p1, "exclude_transaction_states"

    .line 346
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_transaction_states:Ljava/util/List;

    .line 347
    iput-object p5, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 348
    iput-object p6, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 367
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 368
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    .line 369
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_activity_types:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_activity_types:Ljava/util/List;

    .line 370
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_activity_types:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_activity_types:Ljava/util/List;

    .line 371
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_transaction_states:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_transaction_states:Ljava/util/List;

    .line 372
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_transaction_states:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_transaction_states:Ljava/util/List;

    .line 373
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 374
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 375
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 380
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 382
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 383
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_activity_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 384
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_activity_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 385
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_transaction_states:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 386
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_transaction_states:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 387
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 388
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/ISO8601Date;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 389
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;
    .locals 2

    .line 353
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;-><init>()V

    .line 354
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_activity_types:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->exclude_activity_types:Ljava/util/List;

    .line 355
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_activity_types:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->include_activity_types:Ljava/util/List;

    .line 356
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_transaction_states:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->include_transaction_states:Ljava/util/List;

    .line 357
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_transaction_states:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->exclude_transaction_states:Ljava/util/List;

    .line 358
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 359
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iput-object v1, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 360
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 266
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->newBuilder()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 396
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 397
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_activity_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", exclude_activity_types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_activity_types:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 398
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_activity_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", include_activity_types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_activity_types:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 399
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_transaction_states:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", include_transaction_states="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->include_transaction_states:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 400
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_transaction_states:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", exclude_transaction_states="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->exclude_transaction_states:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 401
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_4

    const-string v1, ", min_occurred_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->min_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 402
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v1, :cond_5

    const-string v1, ", max_occurred_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->max_occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Filters{"

    .line 403
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
