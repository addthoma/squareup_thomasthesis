.class public final Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetCardBillingAddressResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;",
        "Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public billing_address:Lcom/squareup/protos/common/location/GlobalAddress;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 79
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public billing_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse$Builder;
    .locals 0

    .line 83
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse$Builder;->billing_address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;
    .locals 3

    .line 89
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse$Builder;->billing_address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;-><init>(Lcom/squareup/protos/common/location/GlobalAddress;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardBillingAddressResponse;

    move-result-object v0

    return-object v0
.end method
