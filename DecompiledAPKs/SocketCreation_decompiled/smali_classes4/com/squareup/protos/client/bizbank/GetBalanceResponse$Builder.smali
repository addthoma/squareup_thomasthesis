.class public final Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBalanceResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetBalanceResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bizbank/GetBalanceResponse;",
        "Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public balance:Lcom/squareup/protos/common/Money;

.field public max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

.field public pending_balance:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 115
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bizbank/GetBalanceResponse;
    .locals 5

    .line 144
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->pending_balance:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/GetBalanceResponse;

    move-result-object v0

    return-object v0
.end method

.method public max_instant_deposit_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->max_instant_deposit_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public pending_balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/bizbank/GetBalanceResponse$Builder;->pending_balance:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
