.class final Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$ProtoAdapter_GetCardActivityRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetCardActivityRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetCardActivityRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 577
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 596
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;-><init>()V

    .line 597
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 598
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 603
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 601
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->filters(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;

    goto :goto_0

    .line 600
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->batch_request(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;

    goto :goto_0

    .line 607
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 608
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 575
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$ProtoAdapter_GetCardActivityRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 589
    sget-object v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;->batch_request:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 590
    sget-object v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;->filters:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 591
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 575
    check-cast p2, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$ProtoAdapter_GetCardActivityRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;)I
    .locals 4

    .line 582
    sget-object v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;->batch_request:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;->filters:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    const/4 v3, 0x2

    .line 583
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 584
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 575
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$ProtoAdapter_GetCardActivityRequest;->encodedSize(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;
    .locals 2

    .line 613
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;->newBuilder()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;

    move-result-object p1

    .line 614
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->batch_request:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->batch_request:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->batch_request:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;

    .line 615
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->filters:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->filters:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->filters:Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    .line 616
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 617
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 575
    check-cast p1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$ProtoAdapter_GetCardActivityRequest;->redact(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    move-result-object p1

    return-object p1
.end method
