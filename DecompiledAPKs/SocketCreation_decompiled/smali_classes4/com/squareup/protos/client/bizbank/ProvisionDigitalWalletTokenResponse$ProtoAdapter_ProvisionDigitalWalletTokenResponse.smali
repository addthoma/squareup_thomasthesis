.class final Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ProtoAdapter_ProvisionDigitalWalletTokenResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ProvisionDigitalWalletTokenResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ProvisionDigitalWalletTokenResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 449
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 471
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;-><init>()V

    .line 472
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 473
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 479
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 477
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->google_pay_response(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;

    goto :goto_0

    .line 476
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->apple_pay_response(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;

    goto :goto_0

    .line 475
    :cond_2
    sget-object v3, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;

    goto :goto_0

    .line 483
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 484
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 447
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ProtoAdapter_ProvisionDigitalWalletTokenResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 463
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 464
    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;->apple_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 465
    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;->google_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 466
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 447
    check-cast p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ProtoAdapter_ProvisionDigitalWalletTokenResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;)I
    .locals 4

    .line 454
    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;->apple_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    const/4 v3, 0x2

    .line 455
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;->google_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

    const/4 v3, 0x3

    .line 456
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 457
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 447
    check-cast p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ProtoAdapter_ProvisionDigitalWalletTokenResponse;->encodedSize(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;
    .locals 2

    .line 489
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;->newBuilder()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;

    move-result-object p1

    .line 490
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/Status;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 491
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->apple_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->apple_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->apple_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ApplePayResponse;

    .line 492
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->google_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->google_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->google_pay_response:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$GooglePayResponse;

    .line 493
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 494
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 447
    check-cast p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse$ProtoAdapter_ProvisionDigitalWalletTokenResponse;->redact(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenResponse;

    move-result-object p1

    return-object p1
.end method
