.class final Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ProtoAdapter_ProvisionDigitalWalletTokenRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ProvisionDigitalWalletTokenRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ProvisionDigitalWalletTokenRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 557
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 581
    new-instance v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;-><init>()V

    .line 582
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 583
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 597
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 595
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->google_pay_request(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;

    goto :goto_0

    .line 594
    :cond_1
    sget-object v3, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->apple_pay_request(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;

    goto :goto_0

    .line 588
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->device_type(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 590
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 585
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->card_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;

    goto :goto_0

    .line 601
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 602
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 555
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ProtoAdapter_ProvisionDigitalWalletTokenRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 572
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;->card_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 573
    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;->device_type:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 574
    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;->apple_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 575
    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;->google_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 576
    invoke-virtual {p2}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 555
    check-cast p2, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ProtoAdapter_ProvisionDigitalWalletTokenRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;)I
    .locals 4

    .line 562
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;->card_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;->device_type:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$DeviceType;

    const/4 v3, 0x2

    .line 563
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;->apple_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    const/4 v3, 0x3

    .line 564
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;->google_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    const/4 v3, 0x4

    .line 565
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 566
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 555
    check-cast p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ProtoAdapter_ProvisionDigitalWalletTokenRequest;->encodedSize(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;
    .locals 2

    .line 607
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;->newBuilder()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;

    move-result-object p1

    .line 608
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->apple_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->apple_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->apple_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ApplePayRequest;

    .line 609
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->google_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->google_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    iput-object v0, p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->google_pay_request:Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$GooglePayRequest;

    .line 610
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 611
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 555
    check-cast p1, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest$ProtoAdapter_ProvisionDigitalWalletTokenRequest;->redact(Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;)Lcom/squareup/protos/client/bizbank/ProvisionDigitalWalletTokenRequest;

    move-result-object p1

    return-object p1
.end method
