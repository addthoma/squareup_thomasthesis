.class public final Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetOrderStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;",
        "Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public max_orders_to_retrieve:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;
    .locals 3

    .line 100
    new-instance v0, Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest$Builder;->max_orders_to_retrieve:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;-><init>(Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest$Builder;->build()Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;

    move-result-object v0

    return-object v0
.end method

.method public max_orders_to_retrieve(Ljava/lang/Integer;)Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest$Builder;
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest$Builder;->max_orders_to_retrieve:Ljava/lang/Integer;

    return-object p0
.end method
