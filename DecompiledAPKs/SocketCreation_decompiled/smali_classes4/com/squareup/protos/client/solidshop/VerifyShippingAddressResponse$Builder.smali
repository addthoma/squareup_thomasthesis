.class public final Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VerifyShippingAddressResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;",
        "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public corrected_address:Lcom/squareup/protos/common/location/GlobalAddress;

.field public corrected_field:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/client/Status;

.field public token:Ljava/lang/String;

.field public verification_status:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 161
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 162
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->corrected_field:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;
    .locals 8

    .line 211
    new-instance v7, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->verification_status:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    iget-object v3, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->corrected_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v5, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->corrected_field:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;Ljava/util/List;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->build()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    move-result-object v0

    return-object v0
.end method

.method public corrected_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->corrected_address:Lcom/squareup/protos/common/location/GlobalAddress;

    return-object p0
.end method

.method public corrected_field(Ljava/util/List;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$CorrectedField;",
            ">;)",
            "Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;"
        }
    .end annotation

    .line 204
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->corrected_field:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;
    .locals 0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public verification_status(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;
    .locals 0

    .line 177
    iput-object p1, p0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$Builder;->verification_status:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    return-object p0
.end method
