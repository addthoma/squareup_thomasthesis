.class public final Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;
.super Lcom/squareup/wire/Message;
.source "ProvisionReaderResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$ProtoAdapter_ProvisionReaderResponse;,
        Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;",
        "Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DUKPT_PIN_IKSN_TXT:Ljava/lang/String; = ""

.field public static final DEFAULT_DUKPT_PIN_KEY_TR31:Ljava/lang/String; = ""

.field public static final DEFAULT_DUKPT_SRED_IKSN_TXT:Ljava/lang/String; = ""

.field public static final DEFAULT_DUKPT_SRED_KEY_TR31:Ljava/lang/String; = ""

.field public static final DEFAULT_HSM_CERT:Ljava/lang/String; = ""

.field public static final DEFAULT_KBPK_BYTES:Lokio/ByteString;

.field public static final DEFAULT_KBPK_CIPHERTEXT:Ljava/lang/String; = ""

.field public static final DEFAULT_KBPK_SIGNATURE:Ljava/lang/String; = ""

.field public static final DEFAULT_KBPK_SIG_BYTES:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final dukpt_pin_iksn_txt:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final dukpt_pin_key_tr31:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final dukpt_sred_iksn_txt:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final dukpt_sred_key_tr31:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final hsm_cert:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final kbpk_bytes:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x7
    .end annotation
.end field

.field public final kbpk_ciphertext:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final kbpk_sig_bytes:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x8
    .end annotation
.end field

.field public final kbpk_signature:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$ProtoAdapter_ProvisionReaderResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$ProtoAdapter_ProvisionReaderResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 36
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->DEFAULT_KBPK_BYTES:Lokio/ByteString;

    .line 38
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->DEFAULT_KBPK_SIG_BYTES:Lokio/ByteString;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .line 160
    sget-object v11, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 167
    sget-object v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p11}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->status:Lcom/squareup/protos/client/Status;

    .line 169
    iput-object p2, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->hsm_cert:Ljava/lang/String;

    .line 170
    iput-object p3, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_ciphertext:Ljava/lang/String;

    .line 171
    iput-object p4, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_signature:Ljava/lang/String;

    .line 172
    iput-object p5, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_iksn_txt:Ljava/lang/String;

    .line 173
    iput-object p6, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_key_tr31:Ljava/lang/String;

    .line 174
    iput-object p7, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_bytes:Lokio/ByteString;

    .line 175
    iput-object p8, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_sig_bytes:Lokio/ByteString;

    .line 176
    iput-object p9, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_iksn_txt:Ljava/lang/String;

    .line 177
    iput-object p10, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_key_tr31:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 200
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 201
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;

    .line 202
    invoke-virtual {p0}, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->status:Lcom/squareup/protos/client/Status;

    .line 203
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->hsm_cert:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->hsm_cert:Ljava/lang/String;

    .line 204
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_ciphertext:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_ciphertext:Ljava/lang/String;

    .line 205
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_signature:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_signature:Ljava/lang/String;

    .line 206
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_iksn_txt:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_iksn_txt:Ljava/lang/String;

    .line 207
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_key_tr31:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_key_tr31:Ljava/lang/String;

    .line 208
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_bytes:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_bytes:Lokio/ByteString;

    .line 209
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_sig_bytes:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_sig_bytes:Lokio/ByteString;

    .line 210
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_iksn_txt:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_iksn_txt:Ljava/lang/String;

    .line 211
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_key_tr31:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_key_tr31:Ljava/lang/String;

    .line 212
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 217
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 219
    invoke-virtual {p0}, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->hsm_cert:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_ciphertext:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_signature:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_iksn_txt:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_key_tr31:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_bytes:Lokio/ByteString;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 227
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_sig_bytes:Lokio/ByteString;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 228
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_iksn_txt:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 229
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_key_tr31:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    .line 230
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;
    .locals 2

    .line 182
    new-instance v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;-><init>()V

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->hsm_cert:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->hsm_cert:Ljava/lang/String;

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_ciphertext:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->kbpk_ciphertext:Ljava/lang/String;

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_signature:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->kbpk_signature:Ljava/lang/String;

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_iksn_txt:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->dukpt_sred_iksn_txt:Ljava/lang/String;

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_key_tr31:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->dukpt_sred_key_tr31:Ljava/lang/String;

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_bytes:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->kbpk_bytes:Lokio/ByteString;

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_sig_bytes:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->kbpk_sig_bytes:Lokio/ByteString;

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_iksn_txt:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->dukpt_pin_iksn_txt:Ljava/lang/String;

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_key_tr31:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->dukpt_pin_key_tr31:Ljava/lang/String;

    .line 193
    invoke-virtual {p0}, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->newBuilder()Lcom/squareup/protos/client/enigma/ProvisionReaderResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 237
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 239
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->hsm_cert:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", hsm_cert="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->hsm_cert:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_ciphertext:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", kbpk_ciphertext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_ciphertext:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_signature:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", kbpk_signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_signature:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_iksn_txt:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", dukpt_sred_iksn_txt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_iksn_txt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_key_tr31:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", dukpt_sred_key_tr31="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_sred_key_tr31:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_bytes:Lokio/ByteString;

    if-eqz v1, :cond_6

    const-string v1, ", kbpk_bytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_bytes:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 245
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_sig_bytes:Lokio/ByteString;

    if-eqz v1, :cond_7

    const-string v1, ", kbpk_sig_bytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->kbpk_sig_bytes:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 246
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_iksn_txt:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", dukpt_pin_iksn_txt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_iksn_txt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_key_tr31:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", dukpt_pin_key_tr31="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/enigma/ProvisionReaderResponse;->dukpt_pin_key_tr31:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ProvisionReaderResponse{"

    .line 248
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
