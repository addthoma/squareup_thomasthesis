.class public final Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;
.super Lcom/squareup/wire/Message;
.source "ExpirationDate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$ProtoAdapter_ExpirationDate;,
        Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;",
        "Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MONTH:Ljava/lang/Integer;

.field public static final DEFAULT_YEAR:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final month:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final year:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        redacted = true
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$ProtoAdapter_ExpirationDate;

    invoke-direct {v0}, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$ProtoAdapter_ExpirationDate;-><init>()V

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 25
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->DEFAULT_YEAR:Ljava/lang/Integer;

    .line 27
    sput-object v0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->DEFAULT_MONTH:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    .line 50
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 55
    iput-object p1, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->year:Ljava/lang/Integer;

    .line 56
    iput-object p2, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->month:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 71
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 72
    :cond_1
    check-cast p1, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;

    .line 73
    invoke-virtual {p0}, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->year:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->year:Ljava/lang/Integer;

    .line 74
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->month:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->month:Ljava/lang/Integer;

    .line 75
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 80
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->year:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->month:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 85
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;-><init>()V

    .line 62
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->year:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;->year:Ljava/lang/Integer;

    .line 63
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->month:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;->month:Ljava/lang/Integer;

    .line 64
    invoke-virtual {p0}, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->newBuilder()Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->year:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", year=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/simple_instrument_store/model/ExpirationDate;->month:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", month=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ExpirationDate{"

    .line 95
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
