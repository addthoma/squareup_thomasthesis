.class final Lcom/squareup/protos/capital/external/business/models/PreviewOffer$ProtoAdapter_PreviewOffer;
.super Lcom/squareup/wire/ProtoAdapter;
.source "PreviewOffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/external/business/models/PreviewOffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PreviewOffer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/external/business/models/PreviewOffer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 212
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 237
    new-instance v0, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;-><init>()V

    .line 238
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 239
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 247
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 245
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->rfc3986_offer_selection_url(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;

    goto :goto_0

    .line 244
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->max_financed_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;

    goto :goto_0

    .line 243
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->offer_selection_url(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;

    goto :goto_0

    .line 242
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->product(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;

    goto :goto_0

    .line 241
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->offer_set_id(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;

    goto :goto_0

    .line 251
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 252
    invoke-virtual {v0}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->build()Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 210
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$ProtoAdapter_PreviewOffer;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/external/business/models/PreviewOffer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 227
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->offer_set_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 228
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->product:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 229
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->offer_selection_url:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 230
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->max_financed_amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 231
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->rfc3986_offer_selection_url:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 232
    invoke-virtual {p2}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 210
    check-cast p2, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$ProtoAdapter_PreviewOffer;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/external/business/models/PreviewOffer;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/external/business/models/PreviewOffer;)I
    .locals 4

    .line 217
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->offer_set_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->product:Ljava/lang/String;

    const/4 v3, 0x2

    .line 218
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->offer_selection_url:Ljava/lang/String;

    const/4 v3, 0x3

    .line 219
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->max_financed_amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 220
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->rfc3986_offer_selection_url:Ljava/lang/String;

    const/4 v3, 0x5

    .line 221
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 222
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 210
    check-cast p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$ProtoAdapter_PreviewOffer;->encodedSize(Lcom/squareup/protos/capital/external/business/models/PreviewOffer;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/external/business/models/PreviewOffer;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer;
    .locals 2

    .line 257
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;->newBuilder()Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;

    move-result-object p1

    .line 258
    iget-object v0, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->max_financed_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->max_financed_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->max_financed_amount:Lcom/squareup/protos/common/Money;

    .line 259
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 260
    invoke-virtual {p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$Builder;->build()Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 210
    check-cast p1, Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/external/business/models/PreviewOffer$ProtoAdapter_PreviewOffer;->redact(Lcom/squareup/protos/capital/external/business/models/PreviewOffer;)Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    move-result-object p1

    return-object p1
.end method
