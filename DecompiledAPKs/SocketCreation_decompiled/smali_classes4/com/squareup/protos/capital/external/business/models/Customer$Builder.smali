.class public final Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Customer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/external/business/models/Customer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/external/business/models/Customer;",
        "Lcom/squareup/protos/capital/external/business/models/Customer$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Lcom/squareup/protos/common/time/DateTime;

.field public eligibilities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/external/business/models/Eligibility;",
            ">;"
        }
    .end annotation
.end field

.field public id:Ljava/lang/String;

.field public partner_id:Ljava/lang/String;

.field public preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

.field public preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

.field public preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

.field public preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

.field public preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

.field public preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

.field public preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 263
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 264
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->eligibilities:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/capital/external/business/models/Customer;
    .locals 14

    .line 366
    new-instance v13, Lcom/squareup/protos/capital/external/business/models/Customer;

    iget-object v1, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->partner_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v4, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    iget-object v5, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    iget-object v6, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iget-object v7, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iget-object v8, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    iget-object v9, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->eligibilities:Ljava/util/List;

    iget-object v10, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    iget-object v11, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/capital/external/business/models/Customer;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/capital/external/business/models/PreviewOffer;Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;Lcom/squareup/protos/capital/external/business/models/PreviewPlan;Lcom/squareup/protos/capital/external/business/models/PreviewPlan;Lcom/squareup/protos/capital/external/business/models/PreviewPlan;Ljava/util/List;Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 240
    invoke-virtual {p0}, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->build()Lcom/squareup/protos/capital/external/business/models/Customer;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    .locals 0

    .line 288
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public eligibilities(Ljava/util/List;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/capital/external/business/models/Eligibility;",
            ">;)",
            "Lcom/squareup/protos/capital/external/business/models/Customer$Builder;"
        }
    .end annotation

    .line 343
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 344
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->eligibilities:Ljava/util/List;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public partner_id(Ljava/lang/String;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    .locals 0

    .line 280
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->partner_id:Ljava/lang/String;

    return-object p0
.end method

.method public preview_active_plan(Lcom/squareup/protos/capital/external/business/models/PreviewPlan;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    .locals 0

    .line 316
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_active_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    return-object p0
.end method

.method public preview_financing_request(Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    .locals 0

    .line 360
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    return-object p0
.end method

.method public preview_financing_request_in_review(Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_financing_request_in_review:Lcom/squareup/protos/capital/external/business/models/PreviewFinancingRequest;

    return-object p0
.end method

.method public preview_latest_closed_plan(Lcom/squareup/protos/capital/external/business/models/PreviewPlan;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    .locals 0

    .line 335
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_closed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    return-object p0
.end method

.method public preview_latest_completed_plan(Lcom/squareup/protos/capital/external/business/models/PreviewPlan;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    .locals 0

    .line 325
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_latest_completed_plan:Lcom/squareup/protos/capital/external/business/models/PreviewPlan;

    return-object p0
.end method

.method public preview_main_offer(Lcom/squareup/protos/capital/external/business/models/PreviewOffer;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_main_offer:Lcom/squareup/protos/capital/external/business/models/PreviewOffer;

    return-object p0
.end method

.method public preview_offer_flow(Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;)Lcom/squareup/protos/capital/external/business/models/Customer$Builder;
    .locals 0

    .line 352
    iput-object p1, p0, Lcom/squareup/protos/capital/external/business/models/Customer$Builder;->preview_offer_flow:Lcom/squareup/protos/capital/external/business/models/PreviewOfferFlow;

    return-object p0
.end method
