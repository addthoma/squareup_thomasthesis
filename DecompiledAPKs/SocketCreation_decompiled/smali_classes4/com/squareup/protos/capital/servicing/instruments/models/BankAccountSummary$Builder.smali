.class public final Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BankAccountSummary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;",
        "Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_number_suffix:Ljava/lang/String;

.field public bank_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public account_number_suffix(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary$Builder;->account_number_suffix:Ljava/lang/String;

    return-object p0
.end method

.method public bank_name(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary$Builder;->bank_name:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;
    .locals 4

    .line 117
    new-instance v0, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    iget-object v1, p0, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary$Builder;->bank_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary$Builder;->account_number_suffix:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary$Builder;->build()Lcom/squareup/protos/capital/servicing/instruments/models/BankAccountSummary;

    move-result-object v0

    return-object v0
.end method
