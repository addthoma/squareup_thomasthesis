.class final Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$ProtoAdapter_DetailAmounts;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Amounts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_DetailAmounts"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 480
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 505
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;-><init>()V

    .line 506
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 507
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 515
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 513
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->remaining_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;

    goto :goto_0

    .line 512
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;

    goto :goto_0

    .line 511
    :cond_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;

    goto :goto_0

    .line 510
    :cond_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->penalty_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;

    goto :goto_0

    .line 509
    :cond_4
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->paid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;

    goto :goto_0

    .line 519
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 520
    invoke-virtual {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 478
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$ProtoAdapter_DetailAmounts;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 495
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 496
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->penalty_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 497
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->past_due_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 498
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->due_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 499
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 500
    invoke-virtual {p2}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 478
    check-cast p2, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$ProtoAdapter_DetailAmounts;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;)I
    .locals 4

    .line 485
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->paid_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->penalty_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 486
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->past_due_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x3

    .line 487
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->due_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 488
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->remaining_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 489
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 490
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 478
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$ProtoAdapter_DetailAmounts;->encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;
    .locals 2

    .line 525
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;

    move-result-object p1

    .line 526
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    .line 527
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->penalty_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->penalty_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->penalty_money:Lcom/squareup/protos/common/Money;

    .line 528
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    .line 529
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->due_money:Lcom/squareup/protos/common/Money;

    .line 530
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 531
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 532
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 478
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts$ProtoAdapter_DetailAmounts;->redact(Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;)Lcom/squareup/protos/capital/servicing/plan/models/Amounts$DetailAmounts;

    move-result-object p1

    return-object p1
.end method
