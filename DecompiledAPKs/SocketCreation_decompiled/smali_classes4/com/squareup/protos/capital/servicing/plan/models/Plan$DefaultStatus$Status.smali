.class public final enum Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;
.super Ljava/lang/Enum;
.source "Plan.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status$ProtoAdapter_Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CURRENT:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

.field public static final enum DEFAULT_ACCELERATED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

.field public static final enum DEFAULT_DEACTIVATED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

.field public static final enum DEFAULT_DISCHARGED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

.field public static final enum DEFAULT_MATURITY:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

.field public static final enum DEFAULT_NON_MONETARY:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

.field public static final enum DEFAULT_PAST_DUE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

.field public static final enum DEFAULT_PAST_DUE_ABBREVIATED_CADENCE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

.field public static final enum DEFAULT_PAST_DUE_V2:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

.field public static final enum DEFAULT_UNCOLLECTIBLE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

.field public static final enum DS_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 904
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const/4 v1, 0x0

    const-string v2, "DS_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DS_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 906
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const/4 v2, 0x1

    const-string v3, "CURRENT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->CURRENT:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 908
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const/4 v3, 0x2

    const-string v4, "DEFAULT_PAST_DUE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_PAST_DUE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 910
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const/4 v4, 0x3

    const-string v5, "DEFAULT_NON_MONETARY"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_NON_MONETARY:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 912
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const/4 v5, 0x4

    const-string v6, "DEFAULT_ACCELERATED"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_ACCELERATED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 914
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const/4 v6, 0x5

    const-string v7, "DEFAULT_UNCOLLECTIBLE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_UNCOLLECTIBLE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 916
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const/4 v7, 0x6

    const-string v8, "DEFAULT_DISCHARGED"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_DISCHARGED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 918
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const/4 v8, 0x7

    const-string v9, "DEFAULT_PAST_DUE_ABBREVIATED_CADENCE"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_PAST_DUE_ABBREVIATED_CADENCE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 920
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const/16 v9, 0x8

    const-string v10, "DEFAULT_MATURITY"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_MATURITY:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 922
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const/16 v10, 0x9

    const-string v11, "DEFAULT_DEACTIVATED"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_DEACTIVATED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 924
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const/16 v11, 0xa

    const-string v12, "DEFAULT_PAST_DUE_V2"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_PAST_DUE_V2:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 903
    sget-object v12, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DS_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->CURRENT:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_PAST_DUE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_NON_MONETARY:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_ACCELERATED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_UNCOLLECTIBLE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_DISCHARGED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_PAST_DUE_ABBREVIATED_CADENCE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_MATURITY:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_DEACTIVATED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_PAST_DUE_V2:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->$VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    .line 926
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status$ProtoAdapter_Status;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status$ProtoAdapter_Status;-><init>()V

    sput-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 930
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 931
    iput p3, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 949
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_PAST_DUE_V2:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0

    .line 948
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_DEACTIVATED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0

    .line 947
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_MATURITY:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0

    .line 946
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_PAST_DUE_ABBREVIATED_CADENCE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0

    .line 945
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_DISCHARGED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0

    .line 944
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_UNCOLLECTIBLE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0

    .line 943
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_ACCELERATED:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0

    .line 942
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_NON_MONETARY:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0

    .line 941
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DEFAULT_PAST_DUE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0

    .line 940
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->CURRENT:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0

    .line 939
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->DS_DO_NOT_USE:Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;
    .locals 1

    .line 903
    const-class v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;
    .locals 1

    .line 903
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->$VALUES:[Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    invoke-virtual {v0}, [Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 956
    iget v0, p0, Lcom/squareup/protos/capital/servicing/plan/models/Plan$DefaultStatus$Status;->value:I

    return v0
.end method
