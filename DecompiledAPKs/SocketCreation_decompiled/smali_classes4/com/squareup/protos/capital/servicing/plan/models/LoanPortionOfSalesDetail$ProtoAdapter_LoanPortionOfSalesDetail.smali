.class final Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$ProtoAdapter_LoanPortionOfSalesDetail;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LoanPortionOfSalesDetail.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LoanPortionOfSalesDetail"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 594
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 653
    new-instance v0, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;-><init>()V

    .line 654
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 655
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 680
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 678
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->percentage_complete_including_pending(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto :goto_0

    .line 677
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto :goto_0

    .line 676
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_period_remaining_and_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto :goto_0

    .line 675
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto :goto_0

    .line 674
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto :goto_0

    .line 673
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_excluding_pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto :goto_0

    .line 672
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto :goto_0

    .line 671
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_and_past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto :goto_0

    .line 670
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->total_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto :goto_0

    .line 669
    :pswitch_9
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 668
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_paid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 667
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_required_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 666
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->percentage_complete(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 665
    :pswitch_d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->payment_period_ended_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 664
    :pswitch_e
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->pending_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 663
    :pswitch_f
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 662
    :pswitch_10
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->paid_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 661
    :pswitch_11
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 660
    :pswitch_12
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_started_at(Ljava/lang/String;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 659
    :pswitch_13
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 658
    :pswitch_14
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->repayment_terms(Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 657
    :pswitch_15
    sget-object v3, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->financing_terms(Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    goto/16 :goto_0

    .line 684
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 685
    invoke-virtual {v0}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 592
    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$ProtoAdapter_LoanPortionOfSalesDetail;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 626
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 627
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 628
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 629
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_started_at:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 630
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 631
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->paid_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 632
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 633
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->pending_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 634
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->payment_period_ended_at:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 635
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 636
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_required_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 637
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_paid_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 638
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 639
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 640
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 641
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 642
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 643
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 644
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 645
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 646
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 647
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete_including_pending:Ljava/lang/String;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 648
    invoke-virtual {p2}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 592
    check-cast p2, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$ProtoAdapter_LoanPortionOfSalesDetail;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;)I
    .locals 4

    .line 599
    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    const/4 v3, 0x2

    .line 600
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x3

    .line 601
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_started_at:Ljava/lang/String;

    const/4 v3, 0x4

    .line 602
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 603
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->paid_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x6

    .line 604
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x7

    .line 605
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->pending_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x8

    .line 606
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->payment_period_ended_at:Ljava/lang/String;

    const/16 v3, 0x9

    .line 607
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete:Ljava/lang/String;

    const/16 v3, 0xa

    .line 608
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_required_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xb

    .line 609
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_paid_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xc

    .line 610
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xd

    .line 611
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->total_due_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xe

    .line 612
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xf

    .line 613
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x10

    .line 614
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x11

    .line 615
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x12

    .line 616
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x13

    .line 617
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x14

    .line 618
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x15

    .line 619
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->percentage_complete_including_pending:Ljava/lang/String;

    const/16 v3, 0x16

    .line 620
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 621
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 592
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$ProtoAdapter_LoanPortionOfSalesDetail;->encodedSize(Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;
    .locals 2

    .line 690
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;->newBuilder()Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;

    move-result-object p1

    .line 691
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->financing_terms:Lcom/squareup/protos/capital/servicing/plan/models/FinancingTerms$Loan;

    .line 692
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->repayment_terms:Lcom/squareup/protos/capital/servicing/plan/models/RepaymentTerms$PortionOfSales;

    .line 693
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_money:Lcom/squareup/protos/common/Money;

    .line 694
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_money:Lcom/squareup/protos/common/Money;

    .line 695
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->paid_money:Lcom/squareup/protos/common/Money;

    .line 696
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_money:Lcom/squareup/protos/common/Money;

    .line 697
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->pending_money:Lcom/squareup/protos/common/Money;

    .line 698
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_required_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_required_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_required_money:Lcom/squareup/protos/common/Money;

    .line 699
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_paid_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_paid_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_paid_money:Lcom/squareup/protos/common/Money;

    .line 700
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_money:Lcom/squareup/protos/common/Money;

    .line 701
    :cond_9
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->total_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->total_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->total_due_money:Lcom/squareup/protos/common/Money;

    .line 702
    :cond_a
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->due_and_past_due_money:Lcom/squareup/protos/common/Money;

    .line 703
    :cond_b
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_money:Lcom/squareup/protos/common/Money;

    .line 704
    :cond_c
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->period_remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 705
    :cond_d
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->past_due_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 706
    :cond_e
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_pending_money:Lcom/squareup/protos/common/Money;

    .line 707
    :cond_f
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_10

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_period_remaining_and_pending_money:Lcom/squareup/protos/common/Money;

    .line 708
    :cond_10
    iget-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_11

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->remaining_excluding_past_due_and_pending_money:Lcom/squareup/protos/common/Money;

    .line 709
    :cond_11
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 710
    invoke-virtual {p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$Builder;->build()Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 592
    check-cast p1, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail$ProtoAdapter_LoanPortionOfSalesDetail;->redact(Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;)Lcom/squareup/protos/capital/servicing/plan/models/LoanPortionOfSalesDetail;

    move-result-object p1

    return-object p1
.end method
