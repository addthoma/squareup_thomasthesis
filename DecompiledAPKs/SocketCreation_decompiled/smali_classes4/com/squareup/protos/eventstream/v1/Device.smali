.class public final Lcom/squareup/protos/eventstream/v1/Device;
.super Lcom/squareup/wire/Message;
.source "Device.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/eventstream/v1/Device$ProtoAdapter_Device;,
        Lcom/squareup/protos/eventstream/v1/Device$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/eventstream/v1/Device;",
        "Lcom/squareup/protos/eventstream/v1/Device$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/eventstream/v1/Device;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADVERTISING_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_ANDROID_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_BRAND:Ljava/lang/String; = ""

.field public static final DEFAULT_INSTALLATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LANGUAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCALE_COUNTRY_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_MANUFACTURER:Ljava/lang/String; = ""

.field public static final DEFAULT_MODEL:Ljava/lang/String; = ""

.field public static final DEFAULT_NETWORK_CARRIER:Ljava/lang/String; = ""

.field public static final DEFAULT_ORIENTATION:Ljava/lang/String; = ""

.field public static final DEFAULT_SCREEN_DIAGONAL_BUCKET:Ljava/lang/Integer;

.field public static final DEFAULT_SCREEN_HEIGHT:Ljava/lang/Integer;

.field public static final DEFAULT_SCREEN_WIDTH:Ljava/lang/Integer;

.field public static final DEFAULT_SUBSCRIBER_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final advertising_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final android_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final brand:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final installation_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final language:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final locale_country_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xf
    .end annotation
.end field

.field public final manufacturer:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final model:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final network_carrier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final orientation:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final screen_diagonal_bucket:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xa
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final screen_height:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x8
    .end annotation
.end field

.field public final screen_width:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x9
    .end annotation
.end field

.field public final sim:Lcom/squareup/protos/eventstream/v1/Sim;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.eventstream.v1.Sim#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final subscriber_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Device$ProtoAdapter_Device;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Device$ProtoAdapter_Device;-><init>()V

    sput-object v0, Lcom/squareup/protos/eventstream/v1/Device;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 43
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/eventstream/v1/Device;->DEFAULT_SCREEN_HEIGHT:Ljava/lang/Integer;

    .line 45
    sput-object v0, Lcom/squareup/protos/eventstream/v1/Device;->DEFAULT_SCREEN_WIDTH:Ljava/lang/Integer;

    .line 47
    sput-object v0, Lcom/squareup/protos/eventstream/v1/Device;->DEFAULT_SCREEN_DIAGONAL_BUCKET:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/eventstream/v1/Sim;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    .line 167
    sget-object v16, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct/range {v0 .. v16}, Lcom/squareup/protos/eventstream/v1/Device;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/eventstream/v1/Sim;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/eventstream/v1/Sim;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 3

    move-object v0, p0

    .line 175
    sget-object v1, Lcom/squareup/protos/eventstream/v1/Device;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p16

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 176
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->manufacturer:Ljava/lang/String;

    move-object v1, p2

    .line 177
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->brand:Ljava/lang/String;

    move-object v1, p3

    .line 178
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->model:Ljava/lang/String;

    move-object v1, p4

    .line 179
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->language:Ljava/lang/String;

    move-object v1, p5

    .line 180
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->installation_id:Ljava/lang/String;

    move-object v1, p6

    .line 181
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->advertising_id:Ljava/lang/String;

    move-object v1, p7

    .line 182
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->android_id:Ljava/lang/String;

    move-object v1, p8

    .line 183
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->screen_height:Ljava/lang/Integer;

    move-object v1, p9

    .line 184
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->screen_width:Ljava/lang/Integer;

    move-object v1, p10

    .line 185
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->screen_diagonal_bucket:Ljava/lang/Integer;

    move-object v1, p11

    .line 186
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->sim:Lcom/squareup/protos/eventstream/v1/Sim;

    move-object v1, p12

    .line 187
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->subscriber_id:Ljava/lang/String;

    move-object/from16 v1, p13

    .line 188
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->network_carrier:Ljava/lang/String;

    move-object/from16 v1, p14

    .line 189
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->orientation:Ljava/lang/String;

    move-object/from16 v1, p15

    .line 190
    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device;->locale_country_code:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 218
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/eventstream/v1/Device;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 219
    :cond_1
    check-cast p1, Lcom/squareup/protos/eventstream/v1/Device;

    .line 220
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Device;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/Device;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->manufacturer:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->manufacturer:Ljava/lang/String;

    .line 221
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->brand:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->brand:Ljava/lang/String;

    .line 222
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->model:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->model:Ljava/lang/String;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->language:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->language:Ljava/lang/String;

    .line 224
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->installation_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->installation_id:Ljava/lang/String;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->advertising_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->advertising_id:Ljava/lang/String;

    .line 226
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->android_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->android_id:Ljava/lang/String;

    .line 227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_height:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->screen_height:Ljava/lang/Integer;

    .line 228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_width:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->screen_width:Ljava/lang/Integer;

    .line 229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_diagonal_bucket:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->screen_diagonal_bucket:Ljava/lang/Integer;

    .line 230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->sim:Lcom/squareup/protos/eventstream/v1/Sim;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->sim:Lcom/squareup/protos/eventstream/v1/Sim;

    .line 231
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->subscriber_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->subscriber_id:Ljava/lang/String;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->network_carrier:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->network_carrier:Ljava/lang/String;

    .line 233
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->orientation:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Device;->orientation:Ljava/lang/String;

    .line 234
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->locale_country_code:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/eventstream/v1/Device;->locale_country_code:Ljava/lang/String;

    .line 235
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 240
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_f

    .line 242
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Device;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->manufacturer:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->brand:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->model:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->language:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->installation_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->advertising_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->android_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_height:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 251
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_width:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 252
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_diagonal_bucket:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 253
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->sim:Lcom/squareup/protos/eventstream/v1/Sim;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/eventstream/v1/Sim;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 254
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->subscriber_id:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 255
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->network_carrier:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 256
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->orientation:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 257
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->locale_country_code:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_e
    add-int/2addr v0, v2

    .line 258
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_f
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 2

    .line 195
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Device$Builder;-><init>()V

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->manufacturer:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->manufacturer:Ljava/lang/String;

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->brand:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->brand:Ljava/lang/String;

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->model:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->model:Ljava/lang/String;

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->language:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->language:Ljava/lang/String;

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->installation_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->installation_id:Ljava/lang/String;

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->advertising_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->advertising_id:Ljava/lang/String;

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->android_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->android_id:Ljava/lang/String;

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_height:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->screen_height:Ljava/lang/Integer;

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_width:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->screen_width:Ljava/lang/Integer;

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_diagonal_bucket:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->screen_diagonal_bucket:Ljava/lang/Integer;

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->sim:Lcom/squareup/protos/eventstream/v1/Sim;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->sim:Lcom/squareup/protos/eventstream/v1/Sim;

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->subscriber_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->subscriber_id:Ljava/lang/String;

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->network_carrier:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->network_carrier:Ljava/lang/String;

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->orientation:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->orientation:Ljava/lang/String;

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->locale_country_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->locale_country_code:Ljava/lang/String;

    .line 211
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Device;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Device;->newBuilder()Lcom/squareup/protos/eventstream/v1/Device$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 265
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 266
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->manufacturer:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", manufacturer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->manufacturer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->brand:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->brand:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->model:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", model="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->model:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->language:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", language="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->language:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->installation_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", installation_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->installation_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->advertising_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", advertising_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->advertising_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->android_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", android_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->android_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_height:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    const-string v1, ", screen_height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_height:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 274
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_width:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    const-string v1, ", screen_width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_width:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 275
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_diagonal_bucket:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    const-string v1, ", screen_diagonal_bucket="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->screen_diagonal_bucket:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 276
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->sim:Lcom/squareup/protos/eventstream/v1/Sim;

    if-eqz v1, :cond_a

    const-string v1, ", sim="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->sim:Lcom/squareup/protos/eventstream/v1/Sim;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 277
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->subscriber_id:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", subscriber_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->subscriber_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->network_carrier:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", network_carrier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->network_carrier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->orientation:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", orientation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->orientation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->locale_country_code:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", locale_country_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Device;->locale_country_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_e
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Device{"

    .line 281
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
