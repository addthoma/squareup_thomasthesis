.class final Lcom/squareup/protos/eventstream/v1/EventMetadata$ProtoAdapter_EventMetadata;
.super Lcom/squareup/wire/ProtoAdapter;
.source "EventMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/EventMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EventMetadata"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/eventstream/v1/EventMetadata;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 258
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/eventstream/v1/EventMetadata;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/eventstream/v1/EventMetadata;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 285
    new-instance v0, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;-><init>()V

    .line 286
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 287
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 296
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 294
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->library_version(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    goto :goto_0

    .line 293
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->library_name(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    goto :goto_0

    .line 292
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->collector(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    goto :goto_0

    .line 291
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->collected_timestamp(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    goto :goto_0

    .line 290
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->recorded_timestamp(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    goto :goto_0

    .line 289
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->uuid(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    goto :goto_0

    .line 300
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 301
    invoke-virtual {v0}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->build()Lcom/squareup/protos/eventstream/v1/EventMetadata;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 256
    invoke-virtual {p0, p1}, Lcom/squareup/protos/eventstream/v1/EventMetadata$ProtoAdapter_EventMetadata;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/eventstream/v1/EventMetadata;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/eventstream/v1/EventMetadata;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 274
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/EventMetadata;->uuid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 275
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/EventMetadata;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 276
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 277
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collector:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 278
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_name:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 279
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_version:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 280
    invoke-virtual {p2}, Lcom/squareup/protos/eventstream/v1/EventMetadata;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 256
    check-cast p2, Lcom/squareup/protos/eventstream/v1/EventMetadata;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/eventstream/v1/EventMetadata$ProtoAdapter_EventMetadata;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/eventstream/v1/EventMetadata;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/eventstream/v1/EventMetadata;)I
    .locals 4

    .line 263
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;->uuid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x2

    .line 264
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x3

    .line 265
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;->collector:Ljava/lang/String;

    const/4 v3, 0x4

    .line 266
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_name:Ljava/lang/String;

    const/4 v3, 0x5

    .line 267
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;->library_version:Ljava/lang/String;

    const/4 v3, 0x6

    .line 268
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/EventMetadata;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 256
    check-cast p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/eventstream/v1/EventMetadata$ProtoAdapter_EventMetadata;->encodedSize(Lcom/squareup/protos/eventstream/v1/EventMetadata;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/eventstream/v1/EventMetadata;)Lcom/squareup/protos/eventstream/v1/EventMetadata;
    .locals 2

    .line 306
    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/EventMetadata;->newBuilder()Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;

    move-result-object p1

    .line 307
    iget-object v0, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->recorded_timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 308
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->collected_timestamp:Lcom/squareup/protos/common/time/DateTime;

    .line 309
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 310
    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/EventMetadata$Builder;->build()Lcom/squareup/protos/eventstream/v1/EventMetadata;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 256
    check-cast p1, Lcom/squareup/protos/eventstream/v1/EventMetadata;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/eventstream/v1/EventMetadata$ProtoAdapter_EventMetadata;->redact(Lcom/squareup/protos/eventstream/v1/EventMetadata;)Lcom/squareup/protos/eventstream/v1/EventMetadata;

    move-result-object p1

    return-object p1
.end method
