.class public final Lcom/squareup/protos/eventstream/v1/Device$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Device.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/eventstream/v1/Device;",
        "Lcom/squareup/protos/eventstream/v1/Device$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public advertising_id:Ljava/lang/String;

.field public android_id:Ljava/lang/String;

.field public brand:Ljava/lang/String;

.field public installation_id:Ljava/lang/String;

.field public language:Ljava/lang/String;

.field public locale_country_code:Ljava/lang/String;

.field public manufacturer:Ljava/lang/String;

.field public model:Ljava/lang/String;

.field public network_carrier:Ljava/lang/String;

.field public orientation:Ljava/lang/String;

.field public screen_diagonal_bucket:Ljava/lang/Integer;

.field public screen_height:Ljava/lang/Integer;

.field public screen_width:Ljava/lang/Integer;

.field public sim:Lcom/squareup/protos/eventstream/v1/Sim;

.field public subscriber_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 315
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public advertising_id(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 351
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->advertising_id:Ljava/lang/String;

    return-object p0
.end method

.method public android_id(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 359
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->android_id:Ljava/lang/String;

    return-object p0
.end method

.method public brand(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->brand:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/eventstream/v1/Device;
    .locals 20

    move-object/from16 v0, p0

    .line 411
    new-instance v18, Lcom/squareup/protos/eventstream/v1/Device;

    move-object/from16 v1, v18

    iget-object v2, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->manufacturer:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->brand:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->model:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->language:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->installation_id:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->advertising_id:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->android_id:Ljava/lang/String;

    iget-object v9, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->screen_height:Ljava/lang/Integer;

    iget-object v10, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->screen_width:Ljava/lang/Integer;

    iget-object v11, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->screen_diagonal_bucket:Ljava/lang/Integer;

    iget-object v12, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->sim:Lcom/squareup/protos/eventstream/v1/Sim;

    iget-object v13, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->subscriber_id:Ljava/lang/String;

    iget-object v14, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->network_carrier:Ljava/lang/String;

    iget-object v15, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->orientation:Ljava/lang/String;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->locale_country_code:Ljava/lang/String;

    move-object/from16 v16, v1

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v17

    move-object/from16 v1, v19

    invoke-direct/range {v1 .. v17}, Lcom/squareup/protos/eventstream/v1/Device;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/protos/eventstream/v1/Sim;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v18
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 284
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Device$Builder;->build()Lcom/squareup/protos/eventstream/v1/Device;

    move-result-object v0

    return-object v0
.end method

.method public installation_id(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 343
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->installation_id:Ljava/lang/String;

    return-object p0
.end method

.method public language(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->language:Ljava/lang/String;

    return-object p0
.end method

.method public locale_country_code(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 405
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->locale_country_code:Ljava/lang/String;

    return-object p0
.end method

.method public manufacturer(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 319
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->manufacturer:Ljava/lang/String;

    return-object p0
.end method

.method public model(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->model:Ljava/lang/String;

    return-object p0
.end method

.method public network_carrier(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 391
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->network_carrier:Ljava/lang/String;

    return-object p0
.end method

.method public orientation(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 396
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->orientation:Ljava/lang/String;

    return-object p0
.end method

.method public screen_diagonal_bucket(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 375
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->screen_diagonal_bucket:Ljava/lang/Integer;

    return-object p0
.end method

.method public screen_height(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 364
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->screen_height:Ljava/lang/Integer;

    return-object p0
.end method

.method public screen_width(Ljava/lang/Integer;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 369
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->screen_width:Ljava/lang/Integer;

    return-object p0
.end method

.method public sim(Lcom/squareup/protos/eventstream/v1/Sim;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0

    .line 380
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->sim:Lcom/squareup/protos/eventstream/v1/Sim;

    return-object p0
.end method

.method public subscriber_id(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Device$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 386
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Device$Builder;->subscriber_id:Ljava/lang/String;

    return-object p0
.end method
