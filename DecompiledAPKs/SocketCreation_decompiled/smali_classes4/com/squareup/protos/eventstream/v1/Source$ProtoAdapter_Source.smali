.class final Lcom/squareup/protos/eventstream/v1/Source$ProtoAdapter_Source;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Source.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/eventstream/v1/Source;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Source"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/eventstream/v1/Source;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 298
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/eventstream/v1/Source;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/eventstream/v1/Source;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 333
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Source$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Source$Builder;-><init>()V

    .line 334
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 335
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 348
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 346
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->region(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    goto :goto_0

    .line 345
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->city(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    goto :goto_0

    .line 344
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/location/Coordinates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/location/Coordinates;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->coordinates(Lcom/squareup/protos/common/location/Coordinates;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    goto :goto_0

    .line 343
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->ip_address(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    goto :goto_0

    .line 342
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->user_agent(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    goto :goto_0

    .line 341
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/eventstream/v1/Reader;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/eventstream/v1/Reader;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->reader(Lcom/squareup/protos/eventstream/v1/Reader;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    goto :goto_0

    .line 340
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/eventstream/v1/Device;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/eventstream/v1/Device;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->device(Lcom/squareup/protos/eventstream/v1/Device;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    goto :goto_0

    .line 339
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/eventstream/v1/OperatingSystem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->os(Lcom/squareup/protos/eventstream/v1/OperatingSystem;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    goto :goto_0

    .line 338
    :pswitch_8
    sget-object v3, Lcom/squareup/protos/eventstream/v1/Application;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/eventstream/v1/Application;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->application(Lcom/squareup/protos/eventstream/v1/Application;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    goto :goto_0

    .line 337
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->type(Ljava/lang/String;)Lcom/squareup/protos/eventstream/v1/Source$Builder;

    goto/16 :goto_0

    .line 352
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 353
    invoke-virtual {v0}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->build()Lcom/squareup/protos/eventstream/v1/Source;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 296
    invoke-virtual {p0, p1}, Lcom/squareup/protos/eventstream/v1/Source$ProtoAdapter_Source;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/eventstream/v1/Source;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/eventstream/v1/Source;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 318
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/Source;->type:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 319
    sget-object v0, Lcom/squareup/protos/eventstream/v1/Application;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/Source;->application:Lcom/squareup/protos/eventstream/v1/Application;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 320
    sget-object v0, Lcom/squareup/protos/eventstream/v1/OperatingSystem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/Source;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 321
    sget-object v0, Lcom/squareup/protos/eventstream/v1/Device;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/Source;->device:Lcom/squareup/protos/eventstream/v1/Device;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 322
    sget-object v0, Lcom/squareup/protos/eventstream/v1/Reader;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/Source;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 323
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/Source;->user_agent:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 324
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/Source;->ip_address:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 325
    sget-object v0, Lcom/squareup/protos/common/location/Coordinates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/Source;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 326
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/Source;->city:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 327
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/eventstream/v1/Source;->region:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 328
    invoke-virtual {p2}, Lcom/squareup/protos/eventstream/v1/Source;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 296
    check-cast p2, Lcom/squareup/protos/eventstream/v1/Source;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/eventstream/v1/Source$ProtoAdapter_Source;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/eventstream/v1/Source;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/eventstream/v1/Source;)I
    .locals 4

    .line 303
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/eventstream/v1/Source;->type:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/eventstream/v1/Application;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/Source;->application:Lcom/squareup/protos/eventstream/v1/Application;

    const/4 v3, 0x2

    .line 304
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/eventstream/v1/OperatingSystem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/Source;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    const/4 v3, 0x3

    .line 305
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/eventstream/v1/Device;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/Source;->device:Lcom/squareup/protos/eventstream/v1/Device;

    const/4 v3, 0x4

    .line 306
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/eventstream/v1/Reader;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/Source;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    const/4 v3, 0x5

    .line 307
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/Source;->user_agent:Ljava/lang/String;

    const/4 v3, 0x6

    .line 308
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/Source;->ip_address:Ljava/lang/String;

    const/4 v3, 0x7

    .line 309
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/location/Coordinates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/Source;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    const/16 v3, 0x8

    .line 310
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/Source;->city:Ljava/lang/String;

    const/16 v3, 0x9

    .line 311
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/eventstream/v1/Source;->region:Ljava/lang/String;

    const/16 v3, 0xa

    .line 312
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 313
    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/Source;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 296
    check-cast p1, Lcom/squareup/protos/eventstream/v1/Source;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/eventstream/v1/Source$ProtoAdapter_Source;->encodedSize(Lcom/squareup/protos/eventstream/v1/Source;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/eventstream/v1/Source;)Lcom/squareup/protos/eventstream/v1/Source;
    .locals 2

    .line 358
    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/Source;->newBuilder()Lcom/squareup/protos/eventstream/v1/Source$Builder;

    move-result-object p1

    .line 359
    iget-object v0, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->application:Lcom/squareup/protos/eventstream/v1/Application;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/eventstream/v1/Application;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->application:Lcom/squareup/protos/eventstream/v1/Application;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/eventstream/v1/Application;

    iput-object v0, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->application:Lcom/squareup/protos/eventstream/v1/Application;

    .line 360
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/eventstream/v1/OperatingSystem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    iput-object v0, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->os:Lcom/squareup/protos/eventstream/v1/OperatingSystem;

    .line 361
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->device:Lcom/squareup/protos/eventstream/v1/Device;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/eventstream/v1/Device;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->device:Lcom/squareup/protos/eventstream/v1/Device;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/eventstream/v1/Device;

    iput-object v0, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->device:Lcom/squareup/protos/eventstream/v1/Device;

    .line 362
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/eventstream/v1/Reader;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/eventstream/v1/Reader;

    iput-object v0, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->reader:Lcom/squareup/protos/eventstream/v1/Reader;

    .line 363
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/location/Coordinates;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/location/Coordinates;

    iput-object v0, p1, Lcom/squareup/protos/eventstream/v1/Source$Builder;->coordinates:Lcom/squareup/protos/common/location/Coordinates;

    .line 364
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 365
    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/Source$Builder;->build()Lcom/squareup/protos/eventstream/v1/Source;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 296
    check-cast p1, Lcom/squareup/protos/eventstream/v1/Source;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/eventstream/v1/Source$ProtoAdapter_Source;->redact(Lcom/squareup/protos/eventstream/v1/Source;)Lcom/squareup/protos/eventstream/v1/Source;

    move-result-object p1

    return-object p1
.end method
