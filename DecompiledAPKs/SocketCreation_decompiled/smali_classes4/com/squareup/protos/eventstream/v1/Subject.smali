.class public final Lcom/squareup/protos/eventstream/v1/Subject;
.super Lcom/squareup/wire/Message;
.source "Subject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/eventstream/v1/Subject$ProtoAdapter_Subject;,
        Lcom/squareup/protos/eventstream/v1/Subject$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/eventstream/v1/Subject;",
        "Lcom/squareup/protos/eventstream/v1/Subject$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/eventstream/v1/Subject;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCOUNT_COUNTRY_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_ANONYMIZED_USER_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_EMPLOYMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PERSON_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_USER_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final account_country_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final anonymized_user_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final employment_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final payment_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final person_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final user_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Subject$ProtoAdapter_Subject;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Subject$ProtoAdapter_Subject;-><init>()V

    sput-object v0, Lcom/squareup/protos/eventstream/v1/Subject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .line 121
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/eventstream/v1/Subject;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 127
    sget-object v0, Lcom/squareup/protos/eventstream/v1/Subject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->user_token:Ljava/lang/String;

    .line 129
    iput-object p2, p0, Lcom/squareup/protos/eventstream/v1/Subject;->anonymized_user_token:Ljava/lang/String;

    .line 130
    iput-object p3, p0, Lcom/squareup/protos/eventstream/v1/Subject;->account_country_code:Ljava/lang/String;

    .line 131
    iput-object p4, p0, Lcom/squareup/protos/eventstream/v1/Subject;->merchant_token:Ljava/lang/String;

    .line 132
    iput-object p5, p0, Lcom/squareup/protos/eventstream/v1/Subject;->unit_token:Ljava/lang/String;

    .line 133
    iput-object p6, p0, Lcom/squareup/protos/eventstream/v1/Subject;->person_token:Ljava/lang/String;

    .line 134
    iput-object p7, p0, Lcom/squareup/protos/eventstream/v1/Subject;->employment_token:Ljava/lang/String;

    .line 135
    iput-object p8, p0, Lcom/squareup/protos/eventstream/v1/Subject;->payment_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 156
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/eventstream/v1/Subject;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 157
    :cond_1
    check-cast p1, Lcom/squareup/protos/eventstream/v1/Subject;

    .line 158
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Subject;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/eventstream/v1/Subject;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->user_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Subject;->user_token:Ljava/lang/String;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->anonymized_user_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Subject;->anonymized_user_token:Ljava/lang/String;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->account_country_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Subject;->account_country_code:Ljava/lang/String;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Subject;->merchant_token:Ljava/lang/String;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Subject;->unit_token:Ljava/lang/String;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->person_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Subject;->person_token:Ljava/lang/String;

    .line 164
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->employment_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/eventstream/v1/Subject;->employment_token:Ljava/lang/String;

    .line 165
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->payment_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/eventstream/v1/Subject;->payment_token:Ljava/lang/String;

    .line 166
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 171
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 173
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Subject;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->user_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->anonymized_user_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->account_country_code:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->person_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->employment_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->payment_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 182
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/eventstream/v1/Subject$Builder;
    .locals 2

    .line 140
    new-instance v0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;-><init>()V

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->user_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->user_token:Ljava/lang/String;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->anonymized_user_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->anonymized_user_token:Ljava/lang/String;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->account_country_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->account_country_code:Ljava/lang/String;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->merchant_token:Ljava/lang/String;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->unit_token:Ljava/lang/String;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->person_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->person_token:Ljava/lang/String;

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->employment_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->employment_token:Ljava/lang/String;

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->payment_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->payment_token:Ljava/lang/String;

    .line 149
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Subject;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/eventstream/v1/Subject$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/eventstream/v1/Subject;->newBuilder()Lcom/squareup/protos/eventstream/v1/Subject$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->user_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", user_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->user_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->anonymized_user_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", anonymized_user_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->anonymized_user_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->account_country_code:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", account_country_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->account_country_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->person_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", person_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->person_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->employment_token:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", employment_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->employment_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->payment_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", payment_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/eventstream/v1/Subject;->payment_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Subject{"

    .line 198
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
