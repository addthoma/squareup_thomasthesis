.class final Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$ProtoAdapter_GetMessagingTokenResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "GetMessagingTokenResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_GetMessagingTokenResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 265
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 294
    new-instance v0, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;-><init>()V

    .line 295
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 296
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 306
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 304
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->device_look_up_token(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;

    goto :goto_0

    .line 303
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->recent_active(Ljava/lang/Boolean;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;

    goto :goto_0

    .line 302
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->hmac_token(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;

    goto :goto_0

    .line 301
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->app_id(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;

    goto :goto_0

    .line 300
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->api_key(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;

    goto :goto_0

    .line 299
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->domain_name(Ljava/lang/String;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;

    goto :goto_0

    .line 298
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->status(Ljava/lang/Boolean;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;

    goto :goto_0

    .line 310
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 311
    invoke-virtual {v0}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->build()Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 263
    invoke-virtual {p0, p1}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$ProtoAdapter_GetMessagingTokenResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 282
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->status:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 283
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->domain_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 284
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->api_key:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 285
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->app_id:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 286
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->hmac_token:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 287
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->recent_active:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 288
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->device_look_up_token:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 289
    invoke-virtual {p2}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 263
    check-cast p2, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$ProtoAdapter_GetMessagingTokenResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;)I
    .locals 4

    .line 270
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->status:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->domain_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 271
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->api_key:Ljava/lang/String;

    const/4 v3, 0x3

    .line 272
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->app_id:Ljava/lang/String;

    const/4 v3, 0x4

    .line 273
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->hmac_token:Ljava/lang/String;

    const/4 v3, 0x5

    .line 274
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->recent_active:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 275
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->device_look_up_token:Ljava/lang/String;

    const/4 v3, 0x7

    .line 276
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 277
    invoke-virtual {p1}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 263
    check-cast p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$ProtoAdapter_GetMessagingTokenResponse;->encodedSize(Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;
    .locals 1

    .line 316
    invoke-virtual {p1}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;->newBuilder()Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 317
    iput-object v0, p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->api_key:Ljava/lang/String;

    .line 318
    iput-object v0, p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->app_id:Ljava/lang/String;

    .line 319
    iput-object v0, p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->hmac_token:Ljava/lang/String;

    .line 320
    iput-object v0, p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->device_look_up_token:Ljava/lang/String;

    .line 321
    invoke-virtual {p1}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 322
    invoke-virtual {p1}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$Builder;->build()Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 263
    check-cast p1, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse$ProtoAdapter_GetMessagingTokenResponse;->redact(Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;)Lcom/squareup/protos/messagehub/service/GetMessagingTokenResponse;

    move-result-object p1

    return-object p1
.end method
