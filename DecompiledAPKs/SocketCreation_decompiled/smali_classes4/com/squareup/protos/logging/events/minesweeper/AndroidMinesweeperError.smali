.class public final Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;
.super Lcom/squareup/wire/Message;
.source "AndroidMinesweeperError.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ProtoAdapter_AndroidMinesweeperError;,
        Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;,
        Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;",
        "Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

.field private static final serialVersionUID:J


# instance fields
.field public final type:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.logging.events.minesweeper.AndroidMinesweeperError$ErrorType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ProtoAdapter_AndroidMinesweeperError;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ProtoAdapter_AndroidMinesweeperError;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;->UNKNOWN:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    sput-object v0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->DEFAULT_TYPE:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;)V
    .locals 1

    .line 39
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;-><init>(Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;Lokio/ByteString;)V
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 44
    iput-object p1, p0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->type:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 58
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 59
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;

    .line 60
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->type:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->type:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    .line 61
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 66
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 68
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 69
    iget-object v1, p0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->type:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 70
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;
    .locals 2

    .line 49
    new-instance v0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;-><init>()V

    .line 50
    iget-object v1, p0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->type:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;->type:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    .line 51
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->newBuilder()Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 78
    iget-object v1, p0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->type:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError;->type:Lcom/squareup/protos/logging/events/minesweeper/AndroidMinesweeperError$ErrorType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AndroidMinesweeperError{"

    .line 79
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
