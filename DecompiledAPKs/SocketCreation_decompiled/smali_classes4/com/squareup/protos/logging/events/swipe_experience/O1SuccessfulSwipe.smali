.class public final Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;
.super Lcom/squareup/wire/Message;
.source "O1SuccessfulSwipe.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$ProtoAdapter_O1SuccessfulSwipe;,
        Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_AUTHENTICATED_LENGTH:Ljava/lang/Integer;

.field public static final DEFAULT_COUNTER:Ljava/lang/Integer;

.field public static final DEFAULT_END_PERIOD:Ljava/lang/Integer;

.field public static final DEFAULT_ENTROPY:Ljava/lang/Integer;

.field public static final DEFAULT_RESETS:Ljava/lang/Integer;

.field public static final DEFAULT_START_PERIOD:Ljava/lang/Integer;

.field public static final DEFAULT_STATUS:Ljava/lang/Integer;

.field public static final DEFAULT_WAKEUPS:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final authenticated_length:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final counter:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x1
    .end annotation
.end field

.field public final end_period:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x8
    .end annotation
.end field

.field public final entropy:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field

.field public final resets:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final start_period:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x7
    .end annotation
.end field

.field public final status:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x6
    .end annotation
.end field

.field public final wakeups:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$ProtoAdapter_O1SuccessfulSwipe;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$ProtoAdapter_O1SuccessfulSwipe;-><init>()V

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 28
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->DEFAULT_COUNTER:Ljava/lang/Integer;

    .line 30
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->DEFAULT_ENTROPY:Ljava/lang/Integer;

    .line 32
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->DEFAULT_AUTHENTICATED_LENGTH:Ljava/lang/Integer;

    .line 34
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->DEFAULT_RESETS:Ljava/lang/Integer;

    .line 36
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->DEFAULT_WAKEUPS:Ljava/lang/Integer;

    .line 38
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->DEFAULT_STATUS:Ljava/lang/Integer;

    .line 40
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->DEFAULT_START_PERIOD:Ljava/lang/Integer;

    .line 42
    sput-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->DEFAULT_END_PERIOD:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 10

    .line 155
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 161
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->counter:Ljava/lang/Integer;

    .line 163
    iput-object p2, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->entropy:Ljava/lang/Integer;

    .line 164
    iput-object p3, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->authenticated_length:Ljava/lang/Integer;

    .line 165
    iput-object p4, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->resets:Ljava/lang/Integer;

    .line 166
    iput-object p5, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->wakeups:Ljava/lang/Integer;

    .line 167
    iput-object p6, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->status:Ljava/lang/Integer;

    .line 168
    iput-object p7, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->start_period:Ljava/lang/Integer;

    .line 169
    iput-object p8, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->end_period:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 190
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 191
    :cond_1
    check-cast p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;

    .line 192
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->counter:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->counter:Ljava/lang/Integer;

    .line 193
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->entropy:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->entropy:Ljava/lang/Integer;

    .line 194
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->authenticated_length:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->authenticated_length:Ljava/lang/Integer;

    .line 195
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->resets:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->resets:Ljava/lang/Integer;

    .line 196
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->wakeups:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->wakeups:Ljava/lang/Integer;

    .line 197
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->status:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->status:Ljava/lang/Integer;

    .line 198
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->start_period:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->start_period:Ljava/lang/Integer;

    .line 199
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->end_period:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->end_period:Ljava/lang/Integer;

    .line 200
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 205
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 207
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->counter:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->entropy:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->authenticated_length:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->resets:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->wakeups:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->status:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->start_period:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 215
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->end_period:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 216
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;
    .locals 2

    .line 174
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;-><init>()V

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->counter:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->counter:Ljava/lang/Integer;

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->entropy:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->entropy:Ljava/lang/Integer;

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->authenticated_length:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->authenticated_length:Ljava/lang/Integer;

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->resets:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->resets:Ljava/lang/Integer;

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->wakeups:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->wakeups:Ljava/lang/Integer;

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->status:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->status:Ljava/lang/Integer;

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->start_period:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->start_period:Ljava/lang/Integer;

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->end_period:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->end_period:Ljava/lang/Integer;

    .line 183
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->newBuilder()Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->counter:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", counter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->counter:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 225
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->entropy:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", entropy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->entropy:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 226
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->authenticated_length:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", authenticated_length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->authenticated_length:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 227
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->resets:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", resets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->resets:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 228
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->wakeups:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", wakeups="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->wakeups:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 229
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->status:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->status:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 230
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->start_period:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    const-string v1, ", start_period="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->start_period:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 231
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->end_period:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    const-string v1, ", end_period="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1SuccessfulSwipe;->end_period:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "O1SuccessfulSwipe{"

    .line 232
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
