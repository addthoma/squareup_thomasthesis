.class public final Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "O1DemodInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;",
        "Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public result:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 84
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;
    .locals 3

    .line 94
    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    iget-object v1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$Builder;->result:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;-><init>(Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo;

    move-result-object v0

    return-object v0
.end method

.method public result(Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;)Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$Builder;
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$Builder;->result:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    return-object p0
.end method
