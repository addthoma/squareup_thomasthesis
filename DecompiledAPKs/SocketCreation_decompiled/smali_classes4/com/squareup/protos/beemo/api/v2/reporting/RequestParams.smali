.class public final Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;
.super Lcom/squareup/wire/Message;
.source "RequestParams.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$ProtoAdapter_RequestParams;,
        Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;,
        Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EVENT_BASED:Ljava/lang/Boolean;

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_START_OF_DAY_OFFSET_MINUTES:Ljava/lang/Integer;

.field public static final DEFAULT_TZ_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v2.reporting.RequestParams$BeemoInternalRequestFlags#ADAPTER"
        tag = 0x64
    .end annotation
.end field

.field public final begin_time:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final creator_token:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final end_time:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final event_based:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v2.reporting.RequestFlags#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final selected_bill_token:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final selected_payment_token:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final sort_param:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v2.reporting.SortParams#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;",
            ">;"
        }
    .end annotation
.end field

.field public final start_of_day_offset_minutes:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xd
    .end annotation
.end field

.field public final subunit_merchant_token:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.api.v2.reporting.TimeWindow#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final tz_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 28
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$ProtoAdapter_RequestParams;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$ProtoAdapter_RequestParams;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->DEFAULT_EVENT_BASED:Ljava/lang/Boolean;

    .line 38
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->DEFAULT_START_OF_DAY_OFFSET_MINUTES:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;Ljava/lang/Integer;Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;",
            ">;",
            "Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;",
            ")V"
        }
    .end annotation

    .line 197
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;Ljava/lang/Integer;Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/time/DateTime;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;Ljava/lang/Integer;Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;Lokio/ByteString;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v2/reporting/SortParams;",
            ">;",
            "Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 206
    sget-object v1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 207
    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->merchant_token:Ljava/lang/String;

    move-object v1, p2

    .line 208
    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    move-object v1, p3

    .line 209
    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->end_time:Lcom/squareup/protos/common/time/DateTime;

    const-string v1, "creator_token"

    move-object v2, p4

    .line 210
    invoke-static {v1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->creator_token:Ljava/util/List;

    const-string v1, "selected_payment_token"

    move-object v2, p5

    .line 211
    invoke-static {v1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_payment_token:Ljava/util/List;

    const-string v1, "sort_param"

    move-object v2, p6

    .line 212
    invoke-static {v1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->sort_param:Ljava/util/List;

    move-object v1, p7

    .line 213
    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    const-string v1, "subunit_merchant_token"

    move-object v2, p8

    .line 214
    invoke-static {v1, p8}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->subunit_merchant_token:Ljava/util/List;

    move-object v1, p9

    .line 215
    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->tz_name:Ljava/lang/String;

    move-object v1, p10

    .line 216
    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->event_based:Ljava/lang/Boolean;

    const-string v1, "selected_bill_token"

    move-object v2, p11

    .line 217
    invoke-static {v1, p11}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_bill_token:Ljava/util/List;

    move-object v1, p12

    .line 218
    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    move-object/from16 v1, p13

    .line 219
    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->start_of_day_offset_minutes:Ljava/lang/Integer;

    move-object/from16 v1, p14

    .line 220
    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 247
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 248
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;

    .line 249
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->merchant_token:Ljava/lang/String;

    .line 250
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    .line 251
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->end_time:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->end_time:Lcom/squareup/protos/common/time/DateTime;

    .line 252
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->creator_token:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->creator_token:Ljava/util/List;

    .line 253
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_payment_token:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_payment_token:Ljava/util/List;

    .line 254
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->sort_param:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->sort_param:Ljava/util/List;

    .line 255
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    .line 256
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->subunit_merchant_token:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->subunit_merchant_token:Ljava/util/List;

    .line 257
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->tz_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->tz_name:Ljava/lang/String;

    .line 258
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->event_based:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->event_based:Ljava/lang/Boolean;

    .line 259
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_bill_token:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_bill_token:Ljava/util/List;

    .line 260
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    .line 261
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->start_of_day_offset_minutes:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->start_of_day_offset_minutes:Ljava/lang/Integer;

    .line 262
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    .line 263
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 268
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 270
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 271
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 272
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 273
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->end_time:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 274
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->creator_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 275
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_payment_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 276
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->sort_param:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 277
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 278
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->subunit_merchant_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 279
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->tz_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 280
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->event_based:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 281
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_bill_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 282
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 283
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->start_of_day_offset_minutes:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 284
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 285
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;
    .locals 2

    .line 225
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;-><init>()V

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->merchant_token:Ljava/lang/String;

    .line 227
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    .line 228
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->end_time:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->end_time:Lcom/squareup/protos/common/time/DateTime;

    .line 229
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->creator_token:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->creator_token:Ljava/util/List;

    .line 230
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_payment_token:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->selected_payment_token:Ljava/util/List;

    .line 231
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->sort_param:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->sort_param:Ljava/util/List;

    .line 232
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->subunit_merchant_token:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->subunit_merchant_token:Ljava/util/List;

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->tz_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->tz_name:Ljava/lang/String;

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->event_based:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->event_based:Ljava/lang/Boolean;

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_bill_token:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->selected_bill_token:Ljava/util/List;

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->start_of_day_offset_minutes:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->start_of_day_offset_minutes:Ljava/lang/Integer;

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    .line 240
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->newBuilder()Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 293
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_1

    const-string v1, ", begin_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->begin_time:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 295
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->end_time:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    const-string v1, ", end_time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->end_time:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 296
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->creator_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", creator_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->creator_token:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 297
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_payment_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", selected_payment_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_payment_token:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 298
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->sort_param:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", sort_param="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->sort_param:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 299
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    if-eqz v1, :cond_6

    const-string v1, ", time_filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->time_filter:Lcom/squareup/protos/beemo/api/v2/reporting/TimeWindow;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 300
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->subunit_merchant_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", subunit_merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->subunit_merchant_token:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 301
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->tz_name:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", tz_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->tz_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->event_based:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", event_based="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->event_based:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 303
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_bill_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, ", selected_bill_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->selected_bill_token:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 304
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    if-eqz v1, :cond_b

    const-string v1, ", request_flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 305
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->start_of_day_offset_minutes:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    const-string v1, ", start_of_day_offset_minutes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->start_of_day_offset_minutes:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 306
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    if-eqz v1, :cond_d

    const-string v1, ", beemo_internal_request_flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams;->beemo_internal_request_flags:Lcom/squareup/protos/beemo/api/v2/reporting/RequestParams$BeemoInternalRequestFlags;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RequestParams{"

    .line 307
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
