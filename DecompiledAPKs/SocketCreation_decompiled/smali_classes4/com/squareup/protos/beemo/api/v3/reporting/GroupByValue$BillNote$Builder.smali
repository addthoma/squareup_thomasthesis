.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public note:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6943
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;
    .locals 3

    .line 6953
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote$Builder;->note:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6940
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote;

    move-result-object v0

    return-object v0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote$Builder;
    .locals 0

    .line 6947
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$BillNote$Builder;->note:Ljava/lang/String;

    return-object p0
.end method
