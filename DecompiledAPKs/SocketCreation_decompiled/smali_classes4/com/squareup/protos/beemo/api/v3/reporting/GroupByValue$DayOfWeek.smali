.class public final enum Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;
.super Ljava/lang/Enum;
.source "GroupByValue.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DayOfWeek"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek$ProtoAdapter_DayOfWeek;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FRIDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

.field public static final enum MONDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

.field public static final enum SATURDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

.field public static final enum SUNDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

.field public static final enum THURSDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

.field public static final enum TUESDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

.field public static final enum WEDNESDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 3767
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    const/4 v1, 0x0

    const-string v2, "SUNDAY"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->SUNDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    .line 3769
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    const/4 v2, 0x1

    const-string v3, "MONDAY"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->MONDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    .line 3771
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    const/4 v3, 0x2

    const-string v4, "TUESDAY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->TUESDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    .line 3773
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    const/4 v4, 0x3

    const-string v5, "WEDNESDAY"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->WEDNESDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    .line 3775
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    const/4 v5, 0x4

    const-string v6, "THURSDAY"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->THURSDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    .line 3777
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    const/4 v6, 0x5

    const-string v7, "FRIDAY"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->FRIDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    .line 3779
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    const/4 v7, 0x6

    const-string v8, "SATURDAY"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->SATURDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    .line 3766
    sget-object v8, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->SUNDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->MONDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->TUESDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->WEDNESDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->THURSDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->FRIDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->SATURDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    .line 3781
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek$ProtoAdapter_DayOfWeek;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek$ProtoAdapter_DayOfWeek;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 3785
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3786
    iput p3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 3800
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->SATURDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    return-object p0

    .line 3799
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->FRIDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    return-object p0

    .line 3798
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->THURSDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    return-object p0

    .line 3797
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->WEDNESDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    return-object p0

    .line 3796
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->TUESDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    return-object p0

    .line 3795
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->MONDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    return-object p0

    .line 3794
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->SUNDAY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;
    .locals 1

    .line 3766
    const-class v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;
    .locals 1

    .line 3766
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->$VALUES:[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 3807
    iget v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DayOfWeek;->value:I

    return v0
.end method
