.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public unit_price:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3407
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;
    .locals 3

    .line 3417
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice$Builder;->unit_price:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;-><init>(Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3404
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice;

    move-result-object v0

    return-object v0
.end method

.method public unit_price(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice$Builder;
    .locals 0

    .line 3411
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$UnitPrice$Builder;->unit_price:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
