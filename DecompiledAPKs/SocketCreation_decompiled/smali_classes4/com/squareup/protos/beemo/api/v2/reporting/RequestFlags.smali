.class public final Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;
.super Lcom/squareup/wire/Message;
.source "RequestFlags.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$ProtoAdapter_RequestFlags;,
        Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;",
        "Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_PARTIAL_FANOUTS:Ljava/lang/Boolean;

.field public static final DEFAULT_FETCH_CONTACT_TOKENS:Ljava/lang/Boolean;

.field public static final DEFAULT_FORCE_INCLUDE_FUTURE_EFFECTIVE_AT_ADJUSTMENT_FEES:Ljava/lang/Boolean;

.field public static final DEFAULT_IGNORE_ITEMS_SERVICE_FAILURES:Ljava/lang/Boolean;

.field public static final DEFAULT_SKIP_COST_OF_GOODS_SOLD:Ljava/lang/Boolean;

.field public static final DEFAULT_SKIP_LEDGER_FEES:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_partial_fanouts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final fetch_contact_tokens:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final ignore_items_service_failures:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final skip_cost_of_goods_sold:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final skip_ledger_fees:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$ProtoAdapter_RequestFlags;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$ProtoAdapter_RequestFlags;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 25
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->DEFAULT_SKIP_LEDGER_FEES:Ljava/lang/Boolean;

    .line 27
    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->DEFAULT_SKIP_COST_OF_GOODS_SOLD:Ljava/lang/Boolean;

    .line 29
    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->DEFAULT_IGNORE_ITEMS_SERVICE_FAILURES:Ljava/lang/Boolean;

    .line 31
    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->DEFAULT_FORCE_INCLUDE_FUTURE_EFFECTIVE_AT_ADJUSTMENT_FEES:Ljava/lang/Boolean;

    .line 33
    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->DEFAULT_FETCH_CONTACT_TOKENS:Ljava/lang/Boolean;

    .line 35
    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->DEFAULT_ALLOW_PARTIAL_FANOUTS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 8

    .line 97
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 104
    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 105
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_ledger_fees:Ljava/lang/Boolean;

    .line 106
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_cost_of_goods_sold:Ljava/lang/Boolean;

    .line 107
    iput-object p3, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ignore_items_service_failures:Ljava/lang/Boolean;

    .line 108
    iput-object p4, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

    .line 109
    iput-object p5, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->fetch_contact_tokens:Ljava/lang/Boolean;

    .line 110
    iput-object p6, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->allow_partial_fanouts:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 129
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 130
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_ledger_fees:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_ledger_fees:Ljava/lang/Boolean;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_cost_of_goods_sold:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_cost_of_goods_sold:Ljava/lang/Boolean;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ignore_items_service_failures:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ignore_items_service_failures:Ljava/lang/Boolean;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

    .line 135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->fetch_contact_tokens:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->fetch_contact_tokens:Ljava/lang/Boolean;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->allow_partial_fanouts:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->allow_partial_fanouts:Ljava/lang/Boolean;

    .line 137
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 142
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_ledger_fees:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_cost_of_goods_sold:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ignore_items_service_failures:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->fetch_contact_tokens:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->allow_partial_fanouts:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 151
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;
    .locals 2

    .line 115
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;-><init>()V

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_ledger_fees:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->skip_ledger_fees:Ljava/lang/Boolean;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_cost_of_goods_sold:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->skip_cost_of_goods_sold:Ljava/lang/Boolean;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ignore_items_service_failures:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->ignore_items_service_failures:Ljava/lang/Boolean;

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->fetch_contact_tokens:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->fetch_contact_tokens:Ljava/lang/Boolean;

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->allow_partial_fanouts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->allow_partial_fanouts:Ljava/lang/Boolean;

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->newBuilder()Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_ledger_fees:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", skip_ledger_fees="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_ledger_fees:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 160
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_cost_of_goods_sold:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", skip_cost_of_goods_sold="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->skip_cost_of_goods_sold:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 161
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ignore_items_service_failures:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", ignore_items_service_failures="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->ignore_items_service_failures:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", force_include_future_effective_at_adjustment_fees="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->force_include_future_effective_at_adjustment_fees:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 163
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->fetch_contact_tokens:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", fetch_contact_tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->fetch_contact_tokens:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 164
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->allow_partial_fanouts:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", allow_partial_fanouts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v2/reporting/RequestFlags;->allow_partial_fanouts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RequestFlags{"

    .line 165
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
