.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;
.super Lcom/squareup/wire/Message;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeviceCredential"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$ProtoAdapter_DeviceCredential;,
        Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.beemo.translation_types.NameOrTranslationType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6140
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$ProtoAdapter_DeviceCredential;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$ProtoAdapter_DeviceCredential;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)V
    .locals 1

    .line 6168
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;-><init>(Ljava/lang/String;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lokio/ByteString;)V
    .locals 1

    .line 6173
    sget-object v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 6174
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->token:Ljava/lang/String;

    .line 6175
    iput-object p2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 6190
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 6191
    :cond_1
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    .line 6192
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->token:Ljava/lang/String;

    .line 6193
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 6194
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 6199
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 6201
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 6202
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6203
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 6204
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;
    .locals 2

    .line 6180
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;-><init>()V

    .line 6181
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;->token:Ljava/lang/String;

    .line 6182
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iput-object v1, v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    .line 6183
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 6139
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 6211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6212
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6213
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    if-eqz v1, :cond_1

    const-string v1, ", name_or_translation_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DeviceCredential{"

    .line 6214
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
