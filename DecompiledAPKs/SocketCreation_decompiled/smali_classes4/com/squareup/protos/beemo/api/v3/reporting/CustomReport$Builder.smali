.class public final Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CustomReport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

.field public group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

.field public reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

.field public sub_report:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 151
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 152
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->sub_report:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public aggregate(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;
    .locals 8

    .line 197
    new-instance v7, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->aggregate:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->sub_report:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    iget-object v5, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 140
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;

    move-result-object v0

    return-object v0
.end method

.method public group_by_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0
.end method

.method public group_by_value(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    const/4 p1, 0x0

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    return-object p0
.end method

.method public reporting_group_value(Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;
    .locals 0

    .line 190
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->reporting_group_value:Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;

    const/4 p1, 0x0

    .line 191
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->group_by_value:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;

    return-object p0
.end method

.method public sub_report(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;"
        }
    .end annotation

    .line 172
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 173
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/CustomReport$Builder;->sub_report:Ljava/util/List;

    return-object p0
.end method
