.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6222
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;
    .locals 4

    .line 6246
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;-><init>(Ljava/lang/String;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6217
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential;

    move-result-object v0

    return-object v0
.end method

.method public name_or_translation_type(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;
    .locals 0

    .line 6240
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;
    .locals 0

    .line 6229
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$DeviceCredential$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
