.class public final enum Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;
.super Ljava/lang/Enum;
.source "SquareProcessingFeeAmountDetails.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FeeType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType$ProtoAdapter_FeeType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum COST_PLUS:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

.field public static final enum FIXED_RATE:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

.field public static final enum NOT_APPLICABLE:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 34
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    const/4 v1, 0x0

    const-string v2, "FIXED_RATE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->FIXED_RATE:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    .line 39
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    const/4 v2, 0x1

    const-string v3, "COST_PLUS"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->COST_PLUS:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    .line 44
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    const/4 v3, 0x2

    const-string v4, "NOT_APPLICABLE"

    const/16 v5, 0x3e8

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->NOT_APPLICABLE:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    .line 30
    sget-object v4, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->FIXED_RATE:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->COST_PLUS:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->NOT_APPLICABLE:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->$VALUES:[Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    .line 46
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType$ProtoAdapter_FeeType;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType$ProtoAdapter_FeeType;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput p3, p0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/16 v0, 0x3e8

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 61
    :cond_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->NOT_APPLICABLE:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    return-object p0

    .line 60
    :cond_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->COST_PLUS:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    return-object p0

    .line 59
    :cond_2
    sget-object p0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->FIXED_RATE:Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;
    .locals 1

    .line 30
    const-class v0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;
    .locals 1

    .line 30
    sget-object v0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->$VALUES:[Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 68
    iget v0, p0, Lcom/squareup/protos/beemo/api/v2/models/SquareProcessingFeeAmountDetails$FeeType;->value:I

    return v0
.end method
