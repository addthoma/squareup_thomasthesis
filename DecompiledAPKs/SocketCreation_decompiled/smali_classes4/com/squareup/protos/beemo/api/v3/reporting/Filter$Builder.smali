.class public final Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Filter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Filter;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

.field public group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

.field public group_by_value:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;",
            ">;"
        }
    .end annotation
.end field

.field public grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

.field public reporting_group_value:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 161
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 162
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_value:Ljava/util/List;

    .line 163
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->reporting_group_value:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/Filter;
    .locals 8

    .line 213
    new-instance v7, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_value:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    iget-object v4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    iget-object v5, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->reporting_group_value:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;Ljava/util/List;Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;Ljava/util/List;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Filter;

    move-result-object v0

    return-object v0
.end method

.method public filter_mode(Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;
    .locals 0

    .line 189
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->filter_mode:Lcom/squareup/protos/beemo/api/v3/reporting/Filter$FilterMode;

    return-object p0
.end method

.method public group_by_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByType;

    return-object p0
.end method

.method public group_by_value(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;"
        }
    .end annotation

    .line 180
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->group_by_value:Ljava/util/List;

    return-object p0
.end method

.method public grouping_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->grouping_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupingType;

    return-object p0
.end method

.method public reporting_group_value(Ljava/util/List;)Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/beemo/api/v3/reporting/ReportingGroupValue;",
            ">;)",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;"
        }
    .end annotation

    .line 206
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 207
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/Filter$Builder;->reporting_group_value:Ljava/util/List;

    return-object p0
.end method
