.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public conversational_mode:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

.field public modifier_set_name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

.field public name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2892
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;
    .locals 5

    .line 2921
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier$Builder;->modifier_set_name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier$Builder;->conversational_mode:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2885
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier;

    move-result-object v0

    return-object v0
.end method

.method public conversational_mode(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier$Builder;
    .locals 0

    .line 2915
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier$Builder;->conversational_mode:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method

.method public modifier_set_name(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier$Builder;
    .locals 0

    .line 2907
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier$Builder;->modifier_set_name:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method

.method public name_or_translation_type(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier$Builder;
    .locals 0

    .line 2899
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Modifier$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method
