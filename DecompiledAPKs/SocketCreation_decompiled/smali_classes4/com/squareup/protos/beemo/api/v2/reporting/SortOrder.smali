.class public final enum Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;
.super Ljava/lang/Enum;
.source "SortOrder.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder$ProtoAdapter_SortOrder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ASC:Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

.field public static final enum DESC:Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 14
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "ASC"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;->ASC:Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    .line 19
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    const/4 v3, 0x2

    const-string v4, "DESC"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;->DESC:Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    new-array v0, v3, [Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    .line 10
    sget-object v3, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;->ASC:Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;->DESC:Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;->$VALUES:[Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    .line 21
    new-instance v0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder$ProtoAdapter_SortOrder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder$ProtoAdapter_SortOrder;-><init>()V

    sput-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput p3, p0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 35
    :cond_0
    sget-object p0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;->DESC:Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    return-object p0

    .line 34
    :cond_1
    sget-object p0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;->ASC:Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;->$VALUES:[Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    invoke-virtual {v0}, [Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 42
    iget v0, p0, Lcom/squareup/protos/beemo/api/v2/reporting/SortOrder;->value:I

    return v0
.end method
