.class final Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$ProtoAdapter_CostOfGoodsAmounts;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Aggregate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CostOfGoodsAmounts"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2381
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2410
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;-><init>()V

    .line 2411
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2412
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 2422
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2420
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->cogs_items_count(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;

    goto :goto_0

    .line 2419
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->profit_margin(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;

    goto :goto_0

    .line 2418
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->average_revenue_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;

    goto :goto_0

    .line 2417
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_revenue_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;

    goto :goto_0

    .line 2416
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_profit_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;

    goto :goto_0

    .line 2415
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->average_cost_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;

    goto :goto_0

    .line 2414
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_cost_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;

    goto :goto_0

    .line 2426
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2427
    invoke-virtual {v0}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2379
    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$ProtoAdapter_CostOfGoodsAmounts;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2398
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_cost_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2399
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_cost_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2400
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_profit_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2401
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_revenue_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2402
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_revenue_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2403
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->profit_margin:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2404
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->cogs_items_count:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2405
    invoke-virtual {p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2379
    check-cast p2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$ProtoAdapter_CostOfGoodsAmounts;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;)I
    .locals 4

    .line 2386
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_cost_money:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_cost_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 2387
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_profit_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x3

    .line 2388
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->total_revenue_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x4

    .line 2389
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->average_revenue_money:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x5

    .line 2390
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->profit_margin:Ljava/lang/String;

    const/4 v3, 0x6

    .line 2391
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->cogs_items_count:Ljava/lang/String;

    const/4 v3, 0x7

    .line 2392
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2393
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2379
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$ProtoAdapter_CostOfGoodsAmounts;->encodedSize(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;
    .locals 2

    .line 2432
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;->newBuilder()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;

    move-result-object p1

    .line 2433
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_cost_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_cost_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_cost_money:Lcom/squareup/protos/common/Money;

    .line 2434
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->average_cost_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->average_cost_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->average_cost_money:Lcom/squareup/protos/common/Money;

    .line 2435
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_profit_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_profit_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_profit_money:Lcom/squareup/protos/common/Money;

    .line 2436
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_revenue_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_revenue_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->total_revenue_money:Lcom/squareup/protos/common/Money;

    .line 2437
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->average_revenue_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->average_revenue_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->average_revenue_money:Lcom/squareup/protos/common/Money;

    .line 2438
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2439
    invoke-virtual {p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2379
    check-cast p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts$ProtoAdapter_CostOfGoodsAmounts;->redact(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;)Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details$CostOfGoodsAmounts;

    move-result-object p1

    return-object p1
.end method
