.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public abbreviation:Ljava/lang/String;

.field public icon_color:Lcom/squareup/protos/common/RGBAColor;

.field public image_url:Ljava/lang/String;

.field public name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2174
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public abbreviation(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;
    .locals 0

    .line 2209
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->abbreviation:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;
    .locals 7

    .line 2215
    new-instance v6, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    iget-object v2, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->image_url:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->icon_color:Lcom/squareup/protos/common/RGBAColor;

    iget-object v4, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->abbreviation:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;-><init>(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;Ljava/lang/String;Lcom/squareup/protos/common/RGBAColor;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2165
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item;

    move-result-object v0

    return-object v0
.end method

.method public icon_color(Lcom/squareup/protos/common/RGBAColor;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;
    .locals 0

    .line 2201
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->icon_color:Lcom/squareup/protos/common/RGBAColor;

    return-object p0
.end method

.method public image_url(Ljava/lang/String;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;
    .locals 0

    .line 2193
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->image_url:Ljava/lang/String;

    return-object p0
.end method

.method public name_or_translation_type(Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;
    .locals 0

    .line 2181
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$Item$Builder;->name_or_translation_type:Lcom/squareup/protos/beemo/translation_types/NameOrTranslationType;

    return-object p0
.end method
