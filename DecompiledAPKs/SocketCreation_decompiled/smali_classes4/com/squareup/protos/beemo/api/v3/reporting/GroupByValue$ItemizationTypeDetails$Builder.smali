.class public final Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GroupByValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;",
        "Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public itemization_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7970
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;
    .locals 3

    .line 7980
    new-instance v0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    iget-object v1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;->itemization_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;-><init>(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 7967
    invoke-virtual {p0}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;->build()Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails;

    move-result-object v0

    return-object v0
.end method

.method public itemization_type(Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;)Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;
    .locals 0

    .line 7974
    iput-object p1, p0, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$Builder;->itemization_type:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$ItemizationTypeDetails$ItemizationType;

    return-object p0
.end method
