.class public final Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CompleteActionItemRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;",
        "Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public name:Lcom/squareup/protos/checklist/common/ActionItemName;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;
    .locals 3

    .line 90
    new-instance v0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;

    iget-object v1, p0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;->name:Lcom/squareup/protos/checklist/common/ActionItemName;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;-><init>(Lcom/squareup/protos/checklist/common/ActionItemName;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;->build()Lcom/squareup/protos/checklist/service/CompleteActionItemRequest;

    move-result-object v0

    return-object v0
.end method

.method public name(Lcom/squareup/protos/checklist/common/ActionItemName;)Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/protos/checklist/service/CompleteActionItemRequest$Builder;->name:Lcom/squareup/protos/checklist/common/ActionItemName;

    return-object p0
.end method
