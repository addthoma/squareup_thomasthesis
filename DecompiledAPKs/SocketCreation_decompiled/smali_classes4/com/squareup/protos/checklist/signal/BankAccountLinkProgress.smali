.class public final enum Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;
.super Ljava/lang/Enum;
.source "BankAccountLinkProgress.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress$ProtoAdapter_BankAccountLinkProgress;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FAILED:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

.field public static final enum NONE:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

.field public static final enum PENDING:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

.field public static final enum VERIFIED:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

.field public static final enum VERIFIED_WITH_DEPOSITS_PENDING:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 21
    new-instance v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "NONE"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->NONE:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    .line 23
    new-instance v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    const/4 v3, 0x2

    const-string v4, "PENDING"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->PENDING:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    .line 25
    new-instance v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    const/4 v4, 0x3

    const-string v5, "VERIFIED"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->VERIFIED:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    .line 27
    new-instance v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    const/4 v5, 0x4

    const-string v6, "FAILED"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->FAILED:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    .line 29
    new-instance v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    const/4 v6, 0x5

    const-string v7, "VERIFIED_WITH_DEPOSITS_PENDING"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->VERIFIED_WITH_DEPOSITS_PENDING:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    new-array v0, v6, [Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    .line 20
    sget-object v6, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->NONE:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->PENDING:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->VERIFIED:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->FAILED:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->VERIFIED_WITH_DEPOSITS_PENDING:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->$VALUES:[Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    .line 31
    new-instance v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress$ProtoAdapter_BankAccountLinkProgress;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress$ProtoAdapter_BankAccountLinkProgress;-><init>()V

    sput-object v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput p3, p0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 48
    :cond_0
    sget-object p0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->VERIFIED_WITH_DEPOSITS_PENDING:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    return-object p0

    .line 47
    :cond_1
    sget-object p0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->FAILED:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    return-object p0

    .line 46
    :cond_2
    sget-object p0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->VERIFIED:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    return-object p0

    .line 45
    :cond_3
    sget-object p0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->PENDING:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    return-object p0

    .line 44
    :cond_4
    sget-object p0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->NONE:Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;
    .locals 1

    .line 20
    const-class v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->$VALUES:[Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    invoke-virtual {v0}, [Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 55
    iget v0, p0, Lcom/squareup/protos/checklist/signal/BankAccountLinkProgress;->value:I

    return v0
.end method
