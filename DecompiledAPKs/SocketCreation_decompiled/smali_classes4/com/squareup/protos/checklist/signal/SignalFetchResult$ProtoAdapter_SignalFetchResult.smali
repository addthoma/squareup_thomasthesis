.class final Lcom/squareup/protos/checklist/signal/SignalFetchResult$ProtoAdapter_SignalFetchResult;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SignalFetchResult.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/checklist/signal/SignalFetchResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SignalFetchResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/checklist/signal/SignalFetchResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 207
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/checklist/signal/SignalFetchResult;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/checklist/signal/SignalFetchResult;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 232
    new-instance v0, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;-><init>()V

    .line 233
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 234
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 249
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 247
    :cond_0
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->updated_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;

    goto :goto_0

    .line 246
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->created_at(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;

    goto :goto_0

    .line 245
    :cond_2
    sget-object v3, Lcom/squareup/protos/checklist/signal/SignalValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/checklist/signal/SignalValue;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->signal_value(Lcom/squareup/protos/checklist/signal/SignalValue;)Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;

    goto :goto_0

    .line 239
    :cond_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/checklist/signal/SignalName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/checklist/signal/SignalName;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->signal_name(Lcom/squareup/protos/checklist/signal/SignalName;)Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 241
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 236
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->identifier(Ljava/lang/String;)Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;

    goto :goto_0

    .line 253
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 254
    invoke-virtual {v0}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->build()Lcom/squareup/protos/checklist/signal/SignalFetchResult;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 205
    invoke-virtual {p0, p1}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$ProtoAdapter_SignalFetchResult;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/checklist/signal/SignalFetchResult;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/checklist/signal/SignalFetchResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 222
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->identifier:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 223
    sget-object v0, Lcom/squareup/protos/checklist/signal/SignalName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->signal_name:Lcom/squareup/protos/checklist/signal/SignalName;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 224
    sget-object v0, Lcom/squareup/protos/checklist/signal/SignalValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->signal_value:Lcom/squareup/protos/checklist/signal/SignalValue;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 225
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->created_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 226
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 227
    invoke-virtual {p2}, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 205
    check-cast p2, Lcom/squareup/protos/checklist/signal/SignalFetchResult;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$ProtoAdapter_SignalFetchResult;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/checklist/signal/SignalFetchResult;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/checklist/signal/SignalFetchResult;)I
    .locals 4

    .line 212
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->identifier:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->signal_name:Lcom/squareup/protos/checklist/signal/SignalName;

    const/4 v3, 0x2

    .line 213
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/checklist/signal/SignalValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->signal_value:Lcom/squareup/protos/checklist/signal/SignalValue;

    const/4 v3, 0x3

    .line 214
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->created_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x4

    .line 215
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v3, 0x5

    .line 216
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 217
    invoke-virtual {p1}, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 205
    check-cast p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$ProtoAdapter_SignalFetchResult;->encodedSize(Lcom/squareup/protos/checklist/signal/SignalFetchResult;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/checklist/signal/SignalFetchResult;)Lcom/squareup/protos/checklist/signal/SignalFetchResult;
    .locals 2

    .line 259
    invoke-virtual {p1}, Lcom/squareup/protos/checklist/signal/SignalFetchResult;->newBuilder()Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;

    move-result-object p1

    .line 260
    iget-object v0, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->signal_value:Lcom/squareup/protos/checklist/signal/SignalValue;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/checklist/signal/SignalValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->signal_value:Lcom/squareup/protos/checklist/signal/SignalValue;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/checklist/signal/SignalValue;

    iput-object v0, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->signal_value:Lcom/squareup/protos/checklist/signal/SignalValue;

    .line 261
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 262
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/DateTime;

    iput-object v0, p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->updated_at:Lcom/squareup/protos/common/time/DateTime;

    .line 263
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 264
    invoke-virtual {p1}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$Builder;->build()Lcom/squareup/protos/checklist/signal/SignalFetchResult;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 205
    check-cast p1, Lcom/squareup/protos/checklist/signal/SignalFetchResult;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/checklist/signal/SignalFetchResult$ProtoAdapter_SignalFetchResult;->redact(Lcom/squareup/protos/checklist/signal/SignalFetchResult;)Lcom/squareup/protos/checklist/signal/SignalFetchResult;

    move-result-object p1

    return-object p1
.end method
