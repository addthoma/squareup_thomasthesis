.class public final Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;
.super Lcom/squareup/wire/Message;
.source "CountrySpecificDetail.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$ProtoAdapter_CountrySpecificDetail;,
        Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;",
        "Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final au_account:Lcom/squareup/protos/bank_accounts/AuAccount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bank_accounts.AuAccount#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bank_accounts.CaAccount#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bank_accounts.FrAccount#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bank_accounts.GbAccount#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bank_accounts.JpAccount#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final us_account:Lcom/squareup/protos/bank_accounts/UsAccount;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.bank_accounts.UsAccount#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.privacyvault.VaultedData#ADAPTER"
        tag = 0x7
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$ProtoAdapter_CountrySpecificDetail;

    invoke-direct {v0}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$ProtoAdapter_CountrySpecificDetail;-><init>()V

    sput-object v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/privacyvault/VaultedData;Lcom/squareup/protos/bank_accounts/UsAccount;Lcom/squareup/protos/bank_accounts/CaAccount;Lcom/squareup/protos/bank_accounts/JpAccount;Lcom/squareup/protos/bank_accounts/AuAccount;Lcom/squareup/protos/bank_accounts/GbAccount;Lcom/squareup/protos/bank_accounts/FrAccount;)V
    .locals 9

    .line 93
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;-><init>(Lcom/squareup/protos/common/privacyvault/VaultedData;Lcom/squareup/protos/bank_accounts/UsAccount;Lcom/squareup/protos/bank_accounts/CaAccount;Lcom/squareup/protos/bank_accounts/JpAccount;Lcom/squareup/protos/bank_accounts/AuAccount;Lcom/squareup/protos/bank_accounts/GbAccount;Lcom/squareup/protos/bank_accounts/FrAccount;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/privacyvault/VaultedData;Lcom/squareup/protos/bank_accounts/UsAccount;Lcom/squareup/protos/bank_accounts/CaAccount;Lcom/squareup/protos/bank_accounts/JpAccount;Lcom/squareup/protos/bank_accounts/AuAccount;Lcom/squareup/protos/bank_accounts/GbAccount;Lcom/squareup/protos/bank_accounts/FrAccount;Lokio/ByteString;)V
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const/4 p8, 0x2

    new-array p8, p8, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p6, p8, v0

    const/4 v0, 0x1

    aput-object p7, p8, v0

    .line 100
    invoke-static {p2, p3, p4, p5, p8}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result p8

    if-gt p8, v0, :cond_0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    .line 104
    iput-object p2, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    .line 105
    iput-object p3, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    .line 106
    iput-object p4, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    .line 107
    iput-object p5, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    .line 108
    iput-object p6, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    .line 109
    iput-object p7, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    return-void

    .line 101
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of us_account, ca_account, jp_account, au_account, gb_account, fr_account may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 129
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 130
    :cond_1
    check-cast p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    iget-object v3, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    iget-object v3, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    iget-object v3, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    iget-object v3, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    .line 135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    iget-object v3, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    iget-object v3, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    .line 137
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    iget-object p1, p1, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    .line 138
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 143
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 145
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/privacyvault/VaultedData;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/bank_accounts/UsAccount;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/bank_accounts/CaAccount;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/bank_accounts/JpAccount;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/bank_accounts/AuAccount;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/bank_accounts/GbAccount;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/bank_accounts/FrAccount;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 153
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;
    .locals 2

    .line 114
    new-instance v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;-><init>()V

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    iput-object v1, v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    iput-object v1, v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    iput-object v1, v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    iput-object v1, v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    iput-object v1, v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    iput-object v1, v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    iput-object v1, v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    .line 122
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->newBuilder()Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    if-eqz v1, :cond_0

    const-string v1, ", vaulted_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    if-eqz v1, :cond_1

    const-string v1, ", us_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->us_account:Lcom/squareup/protos/bank_accounts/UsAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 163
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    if-eqz v1, :cond_2

    const-string v1, ", ca_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->ca_account:Lcom/squareup/protos/bank_accounts/CaAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 164
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    if-eqz v1, :cond_3

    const-string v1, ", jp_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->jp_account:Lcom/squareup/protos/bank_accounts/JpAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 165
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    if-eqz v1, :cond_4

    const-string v1, ", au_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->au_account:Lcom/squareup/protos/bank_accounts/AuAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 166
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    if-eqz v1, :cond_5

    const-string v1, ", gb_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->gb_account:Lcom/squareup/protos/bank_accounts/GbAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 167
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    if-eqz v1, :cond_6

    const-string v1, ", fr_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;->fr_account:Lcom/squareup/protos/bank_accounts/FrAccount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CountrySpecificDetail{"

    .line 168
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
