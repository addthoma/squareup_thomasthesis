.class public final Lcom/squareup/protos/bank_accounts/UsAccount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UsAccount.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bank_accounts/UsAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bank_accounts/UsAccount;",
        "Lcom/squareup/protos/bank_accounts/UsAccount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public routing_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bank_accounts/UsAccount;
    .locals 3

    .line 98
    new-instance v0, Lcom/squareup/protos/bank_accounts/UsAccount;

    iget-object v1, p0, Lcom/squareup/protos/bank_accounts/UsAccount$Builder;->routing_number:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/bank_accounts/UsAccount;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/bank_accounts/UsAccount$Builder;->build()Lcom/squareup/protos/bank_accounts/UsAccount;

    move-result-object v0

    return-object v0
.end method

.method public routing_number(Ljava/lang/String;)Lcom/squareup/protos/bank_accounts/UsAccount$Builder;
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/protos/bank_accounts/UsAccount$Builder;->routing_number:Ljava/lang/String;

    return-object p0
.end method
