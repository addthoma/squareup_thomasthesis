.class final Lcom/squareup/protos/deposits/CreateTransferResponse$ProtoAdapter_CreateTransferResponse;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreateTransferResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/CreateTransferResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CreateTransferResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/deposits/CreateTransferResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 148
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/deposits/CreateTransferResponse;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/CreateTransferResponse;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 169
    new-instance v0, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;-><init>()V

    .line 170
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 171
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 177
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 175
    :cond_0
    sget-object v3, Lcom/squareup/protos/deposits/LocalizedStatusMessage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->status_message(Lcom/squareup/protos/deposits/LocalizedStatusMessage;)Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;

    goto :goto_0

    .line 174
    :cond_1
    sget-object v3, Lcom/squareup/protos/deposits/BalanceActivityFailure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/deposits/BalanceActivityFailure;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->failure(Lcom/squareup/protos/deposits/BalanceActivityFailure;)Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;

    goto :goto_0

    .line 173
    :cond_2
    sget-object v3, Lcom/squareup/protos/deposits/BalanceActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/deposits/BalanceActivity;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->balance_activity(Lcom/squareup/protos/deposits/BalanceActivity;)Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;

    goto :goto_0

    .line 181
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 182
    invoke-virtual {v0}, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->build()Lcom/squareup/protos/deposits/CreateTransferResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/CreateTransferResponse$ProtoAdapter_CreateTransferResponse;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/CreateTransferResponse;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/CreateTransferResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 161
    sget-object v0, Lcom/squareup/protos/deposits/LocalizedStatusMessage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/CreateTransferResponse;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 162
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/CreateTransferResponse;->balance_activity:Lcom/squareup/protos/deposits/BalanceActivity;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 163
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/CreateTransferResponse;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 164
    invoke-virtual {p2}, Lcom/squareup/protos/deposits/CreateTransferResponse;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    check-cast p2, Lcom/squareup/protos/deposits/CreateTransferResponse;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/deposits/CreateTransferResponse$ProtoAdapter_CreateTransferResponse;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/CreateTransferResponse;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/deposits/CreateTransferResponse;)I
    .locals 4

    .line 153
    sget-object v0, Lcom/squareup/protos/deposits/LocalizedStatusMessage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/CreateTransferResponse;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/deposits/BalanceActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/CreateTransferResponse;->balance_activity:Lcom/squareup/protos/deposits/BalanceActivity;

    const/4 v3, 0x1

    .line 154
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/BalanceActivityFailure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/CreateTransferResponse;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    const/4 v3, 0x2

    .line 155
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/CreateTransferResponse;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 146
    check-cast p1, Lcom/squareup/protos/deposits/CreateTransferResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/CreateTransferResponse$ProtoAdapter_CreateTransferResponse;->encodedSize(Lcom/squareup/protos/deposits/CreateTransferResponse;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/deposits/CreateTransferResponse;)Lcom/squareup/protos/deposits/CreateTransferResponse;
    .locals 2

    .line 187
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/CreateTransferResponse;->newBuilder()Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;

    move-result-object p1

    .line 188
    iget-object v0, p1, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/deposits/LocalizedStatusMessage;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    iput-object v0, p1, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->status_message:Lcom/squareup/protos/deposits/LocalizedStatusMessage;

    .line 189
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->balance_activity:Lcom/squareup/protos/deposits/BalanceActivity;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivity;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->balance_activity:Lcom/squareup/protos/deposits/BalanceActivity;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/deposits/BalanceActivity;

    iput-object v0, p1, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->balance_activity:Lcom/squareup/protos/deposits/BalanceActivity;

    .line 190
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityFailure;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/deposits/BalanceActivityFailure;

    iput-object v0, p1, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->failure:Lcom/squareup/protos/deposits/BalanceActivityFailure;

    .line 191
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 192
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/CreateTransferResponse$Builder;->build()Lcom/squareup/protos/deposits/CreateTransferResponse;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 146
    check-cast p1, Lcom/squareup/protos/deposits/CreateTransferResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/CreateTransferResponse$ProtoAdapter_CreateTransferResponse;->redact(Lcom/squareup/protos/deposits/CreateTransferResponse;)Lcom/squareup/protos/deposits/CreateTransferResponse;

    move-result-object p1

    return-object p1
.end method
