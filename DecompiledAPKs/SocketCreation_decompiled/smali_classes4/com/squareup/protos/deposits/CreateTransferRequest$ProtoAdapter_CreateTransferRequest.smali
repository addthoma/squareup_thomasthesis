.class final Lcom/squareup/protos/deposits/CreateTransferRequest$ProtoAdapter_CreateTransferRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreateTransferRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/CreateTransferRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CreateTransferRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/deposits/CreateTransferRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 248
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/deposits/CreateTransferRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/CreateTransferRequest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 277
    new-instance v0, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;-><init>()V

    .line 278
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 279
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 296
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 294
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->client_ip(Ljava/lang/String;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    goto :goto_0

    .line 293
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->destination(Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    goto :goto_0

    .line 292
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->source(Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    goto :goto_0

    .line 286
    :pswitch_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/deposits/BalanceActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/deposits/BalanceActivityType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->balance_activity_type(Lcom/squareup/protos/deposits/BalanceActivityType;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 288
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 283
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    goto :goto_0

    .line 282
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    goto :goto_0

    .line 281
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    goto :goto_0

    .line 300
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 301
    invoke-virtual {v0}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->build()Lcom/squareup/protos/deposits/CreateTransferRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 246
    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/CreateTransferRequest$ProtoAdapter_CreateTransferRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/CreateTransferRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/CreateTransferRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 265
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/CreateTransferRequest;->idempotence_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 266
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/CreateTransferRequest;->unit_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 267
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/CreateTransferRequest;->amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 268
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/CreateTransferRequest;->balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 269
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/CreateTransferRequest;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 270
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/CreateTransferRequest;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 271
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/CreateTransferRequest;->client_ip:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 272
    invoke-virtual {p2}, Lcom/squareup/protos/deposits/CreateTransferRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 246
    check-cast p2, Lcom/squareup/protos/deposits/CreateTransferRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/deposits/CreateTransferRequest$ProtoAdapter_CreateTransferRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/CreateTransferRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/deposits/CreateTransferRequest;)I
    .locals 4

    .line 253
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->idempotence_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->unit_token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 254
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x3

    .line 255
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/BalanceActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->balance_activity_type:Lcom/squareup/protos/deposits/BalanceActivityType;

    const/4 v3, 0x4

    .line 256
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    const/4 v3, 0x5

    .line 257
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    const/4 v3, 0x6

    .line 258
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/CreateTransferRequest;->client_ip:Ljava/lang/String;

    const/4 v3, 0x7

    .line 259
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/CreateTransferRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 246
    check-cast p1, Lcom/squareup/protos/deposits/CreateTransferRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/CreateTransferRequest$ProtoAdapter_CreateTransferRequest;->encodedSize(Lcom/squareup/protos/deposits/CreateTransferRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/deposits/CreateTransferRequest;)Lcom/squareup/protos/deposits/CreateTransferRequest;
    .locals 2

    .line 306
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/CreateTransferRequest;->newBuilder()Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;

    move-result-object p1

    .line 307
    iget-object v0, p1, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 308
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    iput-object v0, p1, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->source:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    .line 309
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    iput-object v0, p1, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->destination:Lcom/squareup/protos/deposits/BalanceActivityEntityIdentifier;

    .line 310
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 311
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/CreateTransferRequest$Builder;->build()Lcom/squareup/protos/deposits/CreateTransferRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 246
    check-cast p1, Lcom/squareup/protos/deposits/CreateTransferRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/CreateTransferRequest$ProtoAdapter_CreateTransferRequest;->redact(Lcom/squareup/protos/deposits/CreateTransferRequest;)Lcom/squareup/protos/deposits/CreateTransferRequest;

    move-result-object p1

    return-object p1
.end method
