.class final Lcom/squareup/protos/deposits/BalanceActivityFailure$ProtoAdapter_BalanceActivityFailure;
.super Lcom/squareup/wire/ProtoAdapter;
.source "BalanceActivityFailure.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/deposits/BalanceActivityFailure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BalanceActivityFailure"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/deposits/BalanceActivityFailure;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 164
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/deposits/BalanceActivityFailure;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/BalanceActivityFailure;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 185
    new-instance v0, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;-><init>()V

    .line 186
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 187
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 207
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 205
    :cond_0
    sget-object v3, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->failure_metadata(Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;)Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;

    goto :goto_0

    .line 199
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->money_moving_blocker(Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;)Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 201
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 191
    :cond_2
    :try_start_1
    sget-object v4, Lcom/squareup/protos/ledger/service/EligibilityBlocker;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->eligibility_blocker(Lcom/squareup/protos/ledger/service/EligibilityBlocker;)Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 193
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 211
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 212
    invoke-virtual {v0}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->build()Lcom/squareup/protos/deposits/BalanceActivityFailure;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 162
    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/BalanceActivityFailure$ProtoAdapter_BalanceActivityFailure;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/deposits/BalanceActivityFailure;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/BalanceActivityFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    sget-object v0, Lcom/squareup/protos/ledger/service/EligibilityBlocker;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivityFailure;->eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 178
    sget-object v0, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivityFailure;->money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 179
    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/deposits/BalanceActivityFailure;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 180
    invoke-virtual {p2}, Lcom/squareup/protos/deposits/BalanceActivityFailure;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 162
    check-cast p2, Lcom/squareup/protos/deposits/BalanceActivityFailure;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/deposits/BalanceActivityFailure$ProtoAdapter_BalanceActivityFailure;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/deposits/BalanceActivityFailure;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/deposits/BalanceActivityFailure;)I
    .locals 4

    .line 169
    sget-object v0, Lcom/squareup/protos/ledger/service/EligibilityBlocker;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/BalanceActivityFailure;->eligibility_blocker:Lcom/squareup/protos/ledger/service/EligibilityBlocker;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivityFailure;->money_moving_blocker:Lcom/squareup/protos/ledger/service/MoneyMovingBlocker;

    const/4 v3, 0x2

    .line 170
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/deposits/BalanceActivityFailure;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    const/4 v3, 0x3

    .line 171
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/BalanceActivityFailure;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 162
    check-cast p1, Lcom/squareup/protos/deposits/BalanceActivityFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/BalanceActivityFailure$ProtoAdapter_BalanceActivityFailure;->encodedSize(Lcom/squareup/protos/deposits/BalanceActivityFailure;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/deposits/BalanceActivityFailure;)Lcom/squareup/protos/deposits/BalanceActivityFailure;
    .locals 2

    .line 217
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/BalanceActivityFailure;->newBuilder()Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;

    move-result-object p1

    .line 218
    iget-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    iput-object v0, p1, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->failure_metadata:Lcom/squareup/protos/deposits/BalanceActivityFailureMetadata;

    .line 219
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 220
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/BalanceActivityFailure$Builder;->build()Lcom/squareup/protos/deposits/BalanceActivityFailure;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 162
    check-cast p1, Lcom/squareup/protos/deposits/BalanceActivityFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/deposits/BalanceActivityFailure$ProtoAdapter_BalanceActivityFailure;->redact(Lcom/squareup/protos/deposits/BalanceActivityFailure;)Lcom/squareup/protos/deposits/BalanceActivityFailure;

    move-result-object p1

    return-object p1
.end method
