.class public final Lcom/squareup/protos/connect/v2/Payment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Payment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/Payment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/Payment;",
        "Lcom/squareup/protos/connect/v2/Payment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_id:Ljava/lang/String;

.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public app_processing_fee:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/AppProcessingFee;",
            ">;"
        }
    .end annotation
.end field

.field public approved_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public balance_details:Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

.field public billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

.field public buyer_email_address:Ljava/lang/String;

.field public capabilities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public card_details:Lcom/squareup/protos/connect/v2/CardPaymentDetails;

.field public cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

.field public created_at:Ljava/lang/String;

.field public customer_id:Ljava/lang/String;

.field public delay_action:Ljava/lang/String;

.field public delay_duration:Ljava/lang/String;

.field public delayed_until:Ljava/lang/String;

.field public dispute_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public employee_id:Ljava/lang/String;

.field public external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

.field public id:Ljava/lang/String;

.field public location_id:Ljava/lang/String;

.field public note:Ljava/lang/String;

.field public order_id:Ljava/lang/String;

.field public payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

.field public processing_fee:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/ProcessingFee;",
            ">;"
        }
    .end annotation
.end field

.field public receipt_number:Ljava/lang/String;

.field public receipt_url:Ljava/lang/String;

.field public reference_id:Ljava/lang/String;

.field public refund_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public refunded_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public risk_evaluation:Lcom/squareup/protos/connect/v2/RiskEvaluation;

.field public shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

.field public source_type:Ljava/lang/String;

.field public statement_description_identifier:Ljava/lang/String;

.field public status:Ljava/lang/String;

.field public tax_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public terminal_checkout_id:Ljava/lang/String;

.field public tip_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public total_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public updated_at:Ljava/lang/String;

.field public version:Ljava/lang/Long;

.field public version_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 951
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 952
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->processing_fee:Ljava/util/List;

    .line 953
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->app_processing_fee:Ljava/util/List;

    .line 954
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->refund_ids:Ljava/util/List;

    .line 955
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->dispute_ids:Ljava/util/List;

    .line 956
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->capabilities:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public account_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1336
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->account_id:Ljava/lang/String;

    return-object p0
.end method

.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 999
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public app_fee_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1055
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public app_processing_fee(Ljava/util/List;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/AppProcessingFee;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/Payment$Builder;"
        }
    .end annotation

    .line 1098
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1099
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->app_processing_fee:Ljava/util/List;

    return-object p0
.end method

.method public approved_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1065
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->approved_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public balance_details(Lcom/squareup/protos/connect/v2/BalancePaymentDetails;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1208
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->balance_details:Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    return-object p0
.end method

.method public billing_address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1356
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/Payment;
    .locals 2

    .line 1459
    new-instance v0, Lcom/squareup/protos/connect/v2/Payment;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/connect/v2/Payment;-><init>(Lcom/squareup/protos/connect/v2/Payment$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 866
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/Payment$Builder;->build()Lcom/squareup/protos/connect/v2/Payment;

    move-result-object v0

    return-object v0
.end method

.method public buyer_email_address(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1346
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->buyer_email_address:Ljava/lang/String;

    return-object p0
.end method

.method public capabilities(Ljava/util/List;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/Payment$Builder;"
        }
    .end annotation

    .line 1404
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1405
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->capabilities:Ljava/util/List;

    return-object p0
.end method

.method public card_details(Lcom/squareup/protos/connect/v2/CardPaymentDetails;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1197
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->card_details:Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    return-object p0
.end method

.method public cash_details(Lcom/squareup/protos/connect/v2/CashPaymentDetails;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1219
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    return-object p0
.end method

.method public created_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 976
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->created_at:Ljava/lang/String;

    return-object p0
.end method

.method public customer_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1272
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->customer_id:Ljava/lang/String;

    return-object p0
.end method

.method public delay_action(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1157
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->delay_action:Ljava/lang/String;

    return-object p0
.end method

.method public delay_duration(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1141
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->delay_duration:Ljava/lang/String;

    return-object p0
.end method

.method public delayed_until(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1172
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->delayed_until:Ljava/lang/String;

    return-object p0
.end method

.method public dispute_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/Payment$Builder;"
        }
    .end annotation

    .line 1303
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1304
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->dispute_ids:Ljava/util/List;

    return-object p0
.end method

.method public employee_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1282
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->employee_id:Ljava/lang/String;

    return-object p0
.end method

.method public external_details(Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1230
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 966
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public location_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1241
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->location_id:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1376
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public order_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1251
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->order_id:Ljava/lang/String;

    return-object p0
.end method

.method public payment_fees_spec(Lcom/squareup/protos/connect/v2/PaymentFeesSpec;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1077
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    return-object p0
.end method

.method public processing_fee(Ljava/util/List;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/ProcessingFee;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/Payment$Builder;"
        }
    .end annotation

    .line 1087
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1088
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->processing_fee:Ljava/util/List;

    return-object p0
.end method

.method public receipt_number(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1416
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->receipt_number:Ljava/lang/String;

    return-object p0
.end method

.method public receipt_url(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1427
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->receipt_url:Ljava/lang/String;

    return-object p0
.end method

.method public reference_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1262
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->reference_id:Ljava/lang/String;

    return-object p0
.end method

.method public refund_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/Payment$Builder;"
        }
    .end annotation

    .line 1292
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1293
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->refund_ids:Ljava/util/List;

    return-object p0
.end method

.method public refunded_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1111
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->refunded_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public risk_evaluation(Lcom/squareup/protos/connect/v2/RiskEvaluation;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1316
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->risk_evaluation:Lcom/squareup/protos/connect/v2/RiskEvaluation;

    return-object p0
.end method

.method public shipping_address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1366
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    return-object p0
.end method

.method public source_type(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1186
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->source_type:Ljava/lang/String;

    return-object p0
.end method

.method public statement_description_identifier(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1391
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->statement_description_identifier:Ljava/lang/String;

    return-object p0
.end method

.method public status(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1121
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->status:Ljava/lang/String;

    return-object p0
.end method

.method public tax_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1025
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public terminal_checkout_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1326
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->terminal_checkout_id:Ljava/lang/String;

    return-object p0
.end method

.method public tip_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1011
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1037
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public updated_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 986
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->updated_at:Ljava/lang/String;

    return-object p0
.end method

.method public version(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1453
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->version:Ljava/lang/Long;

    return-object p0
.end method

.method public version_token(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/Payment$Builder;
    .locals 0

    .line 1439
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/Payment$Builder;->version_token:Ljava/lang/String;

    return-object p0
.end method
