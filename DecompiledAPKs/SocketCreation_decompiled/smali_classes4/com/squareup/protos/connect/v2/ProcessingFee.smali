.class public final Lcom/squareup/protos/connect/v2/ProcessingFee;
.super Lcom/squareup/wire/Message;
.source "ProcessingFee.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/ProcessingFee$ProtoAdapter_ProcessingFee;,
        Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/ProcessingFee;",
        "Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/ProcessingFee;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EFFECTIVE_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final amount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final effective_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/ProcessingFee$ProtoAdapter_ProcessingFee;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/ProcessingFee$ProtoAdapter_ProcessingFee;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;)V
    .locals 1

    .line 71
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/connect/v2/ProcessingFee;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 76
    sget-object v0, Lcom/squareup/protos/connect/v2/ProcessingFee;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 77
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->effective_at:Ljava/lang/String;

    .line 78
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->type:Ljava/lang/String;

    .line 79
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 95
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/ProcessingFee;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 96
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/ProcessingFee;

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/ProcessingFee;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/ProcessingFee;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->effective_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/ProcessingFee;->effective_at:Ljava/lang/String;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->type:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/ProcessingFee;->type:Ljava/lang/String;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/ProcessingFee;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 100
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 105
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/ProcessingFee;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->effective_at:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->type:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 111
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;
    .locals 2

    .line 84
    new-instance v0, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;-><init>()V

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->effective_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->effective_at:Ljava/lang/String;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->type:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->type:Ljava/lang/String;

    .line 87
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/ProcessingFee;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/ProcessingFee;->newBuilder()Lcom/squareup/protos/connect/v2/ProcessingFee$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->effective_at:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", effective_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->effective_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->type:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", amount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/ProcessingFee;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ProcessingFee{"

    .line 122
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
