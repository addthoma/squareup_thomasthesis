.class public final Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RiskEvaluation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/RiskEvaluation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/RiskEvaluation;",
        "Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public level:Ljava/lang/String;

.field public numeric_score:Ljava/lang/Double;

.field public reasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public status:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 152
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 153
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->reasons:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/RiskEvaluation;
    .locals 7

    .line 202
    new-instance v6, Lcom/squareup/protos/connect/v2/RiskEvaluation;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->status:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->level:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->numeric_score:Ljava/lang/Double;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->reasons:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/RiskEvaluation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 143
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->build()Lcom/squareup/protos/connect/v2/RiskEvaluation;

    move-result-object v0

    return-object v0
.end method

.method public level(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->level:Ljava/lang/String;

    return-object p0
.end method

.method public numeric_score(Ljava/lang/Double;)Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->numeric_score:Ljava/lang/Double;

    return-object p0
.end method

.method public reasons(Ljava/util/List;)Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;"
        }
    .end annotation

    .line 195
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->reasons:Ljava/util/List;

    return-object p0
.end method

.method public status(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/RiskEvaluation$Builder;->status:Ljava/lang/String;

    return-object p0
.end method
