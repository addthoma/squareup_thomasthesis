.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;
.super Lcom/squareup/wire/Message;
.source "CatalogProductSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$ProtoAdapter_CatalogProductSet;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALL_PRODUCTS:Ljava/lang/Boolean;

.field public static final DEFAULT_MATCH_CUSTOM_AMOUNTS:Ljava/lang/Boolean;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY_EXACT:Ljava/lang/Long;

.field public static final DEFAULT_QUANTITY_MAX:Ljava/lang/Long;

.field public static final DEFAULT_QUANTITY_MIN:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final all_products:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final match_custom_amounts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final product_ids_all:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final product_ids_any:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final quantity_exact:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field

.field public final quantity_max:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x6
    .end annotation
.end field

.field public final quantity_min:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 32
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$ProtoAdapter_CatalogProductSet;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$ProtoAdapter_CatalogProductSet;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 38
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->DEFAULT_QUANTITY_EXACT:Ljava/lang/Long;

    .line 40
    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->DEFAULT_QUANTITY_MIN:Ljava/lang/Long;

    .line 42
    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->DEFAULT_QUANTITY_MAX:Ljava/lang/Long;

    const/4 v0, 0x0

    .line 44
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->DEFAULT_ALL_PRODUCTS:Ljava/lang/Boolean;

    .line 46
    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->DEFAULT_MATCH_CUSTOM_AMOUNTS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 164
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 170
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->name:Ljava/lang/String;

    const-string p1, "product_ids_any"

    .line 172
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_any:Ljava/util/List;

    const-string p1, "product_ids_all"

    .line 173
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_all:Ljava/util/List;

    .line 174
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_exact:Ljava/lang/Long;

    .line 175
    iput-object p5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_min:Ljava/lang/Long;

    .line 176
    iput-object p6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_max:Ljava/lang/Long;

    .line 177
    iput-object p7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->all_products:Ljava/lang/Boolean;

    .line 178
    iput-object p8, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->match_custom_amounts:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 199
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 200
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    .line 201
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->name:Ljava/lang/String;

    .line 202
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_any:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_any:Ljava/util/List;

    .line 203
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_all:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_all:Ljava/util/List;

    .line 204
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_exact:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_exact:Ljava/lang/Long;

    .line 205
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_min:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_min:Ljava/lang/Long;

    .line 206
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_max:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_max:Ljava/lang/Long;

    .line 207
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->all_products:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->all_products:Ljava/lang/Boolean;

    .line 208
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->match_custom_amounts:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->match_custom_amounts:Ljava/lang/Boolean;

    .line 209
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 214
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 216
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 217
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_any:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_all:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_exact:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_min:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_max:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->all_products:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->match_custom_amounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 225
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;
    .locals 2

    .line 183
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;-><init>()V

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->name:Ljava/lang/String;

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_any:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->product_ids_any:Ljava/util/List;

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_all:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->product_ids_all:Ljava/util/List;

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_exact:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->quantity_exact:Ljava/lang/Long;

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_min:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->quantity_min:Ljava/lang/Long;

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_max:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->quantity_max:Ljava/lang/Long;

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->all_products:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->all_products:Ljava/lang/Boolean;

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->match_custom_amounts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->match_custom_amounts:Ljava/lang/Boolean;

    .line 192
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 233
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_any:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", product_ids_any="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_any:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 235
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_all:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", product_ids_all="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->product_ids_all:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 236
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_exact:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", quantity_exact="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_exact:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 237
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_min:Ljava/lang/Long;

    if-eqz v1, :cond_4

    const-string v1, ", quantity_min="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_min:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 238
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_max:Ljava/lang/Long;

    if-eqz v1, :cond_5

    const-string v1, ", quantity_max="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->quantity_max:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 239
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->all_products:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", all_products="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->all_products:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 240
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->match_custom_amounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", match_custom_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;->match_custom_amounts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CatalogProductSet{"

    .line 241
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
