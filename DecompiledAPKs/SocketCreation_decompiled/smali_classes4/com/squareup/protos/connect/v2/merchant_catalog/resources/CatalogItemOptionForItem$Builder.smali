.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogItemOptionForItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public item_option_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 88
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem;
    .locals 3

    .line 101
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;->item_option_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem;

    move-result-object v0

    return-object v0
.end method

.method public item_option_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionForItem$Builder;->item_option_id:Ljava/lang/String;

    return-object p0
.end method
