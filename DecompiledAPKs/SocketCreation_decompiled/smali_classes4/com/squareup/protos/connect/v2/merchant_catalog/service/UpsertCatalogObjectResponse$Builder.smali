.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpsertCatalogObjectResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public id_mappings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 120
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 121
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->errors:Ljava/util/List;

    .line 122
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->id_mappings:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;
    .locals 5

    .line 153
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->id_mappings:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->catalog_object:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    return-object p0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;"
        }
    .end annotation

    .line 129
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public id_mappings(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;"
        }
    .end annotation

    .line 146
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse$Builder;->id_mappings:Ljava/util/List;

    return-object p0
.end method
