.class public final Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CancelTerminalCheckoutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;",
        "Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cancel_reason:Ljava/lang/String;

.field public checkout_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 106
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;
    .locals 4

    .line 132
    new-instance v0, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;->checkout_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;->cancel_reason:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;->build()Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest;

    move-result-object v0

    return-object v0
.end method

.method public cancel_reason(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;->cancel_reason:Ljava/lang/String;

    return-object p0
.end method

.method public checkout_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CancelTerminalCheckoutRequest$Builder;->checkout_id:Ljava/lang/String;

    return-object p0
.end method
