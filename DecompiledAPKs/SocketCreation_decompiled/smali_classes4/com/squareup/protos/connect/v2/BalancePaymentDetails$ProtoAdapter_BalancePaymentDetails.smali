.class final Lcom/squareup/protos/connect/v2/BalancePaymentDetails$ProtoAdapter_BalancePaymentDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "BalancePaymentDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/BalancePaymentDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BalancePaymentDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/BalancePaymentDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 166
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/BalancePaymentDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 187
    new-instance v0, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;-><init>()V

    .line 188
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 189
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 195
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 193
    :cond_0
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->errors:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 192
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->status(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;

    goto :goto_0

    .line 191
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->account_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;

    goto :goto_0

    .line 199
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 200
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->build()Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 164
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$ProtoAdapter_BalancePaymentDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/BalancePaymentDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 179
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;->account_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 180
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;->status:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 181
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;->errors:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 182
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 164
    check-cast p2, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$ProtoAdapter_BalancePaymentDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/BalancePaymentDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/BalancePaymentDetails;)I
    .locals 4

    .line 171
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;->account_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;->status:Ljava/lang/String;

    const/4 v3, 0x2

    .line 172
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 173
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;->errors:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 164
    check-cast p1, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$ProtoAdapter_BalancePaymentDetails;->encodedSize(Lcom/squareup/protos/connect/v2/BalancePaymentDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/BalancePaymentDetails;)Lcom/squareup/protos/connect/v2/BalancePaymentDetails;
    .locals 2

    .line 205
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;->newBuilder()Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;

    move-result-object p1

    .line 206
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->errors:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 207
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 208
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->build()Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 164
    check-cast p1, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$ProtoAdapter_BalancePaymentDetails;->redact(Lcom/squareup/protos/connect/v2/BalancePaymentDetails;)Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    move-result-object p1

    return-object p1
.end method
