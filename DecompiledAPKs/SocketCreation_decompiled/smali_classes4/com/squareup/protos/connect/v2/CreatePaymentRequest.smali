.class public final Lcom/squareup/protos/connect/v2/CreatePaymentRequest;
.super Lcom/squareup/wire/Message;
.source "CreatePaymentRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/CreatePaymentRequest$ProtoAdapter_CreatePaymentRequest;,
        Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/CreatePaymentRequest;",
        "Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/CreatePaymentRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCEPT_PARTIAL_AUTHORIZATION:Ljava/lang/Boolean;

.field public static final DEFAULT_AUTOCOMPLETE:Ljava/lang/Boolean;

.field public static final DEFAULT_BUYER_EMAIL_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_CREATE_ORDER:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOMER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_DELAY_ACTION:Ljava/lang/String; = ""

.field public static final DEFAULT_DELAY_DURATION:Ljava/lang/String; = ""

.field public static final DEFAULT_EMPLOYEE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IDEMPOTENCY_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_ORDER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_CONFIG_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_REFERENCE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_REQUESTED_STATEMENT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_SOURCE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_STATEMENT_DESCRIPTION_IDENTIFIER:Ljava/lang/String; = ""

.field public static final DEFAULT_TERMINAL_CHECKOUT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_VERIFICATION_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final accept_partial_authorization:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final amount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final autocomplete:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final auxiliary_info:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.AuxiliaryInfo#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x20
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/AuxiliaryInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final billing_address:Lcom/squareup/protos/connect/v2/resources/Address;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Address#ADAPTER"
        redacted = true
        tag = 0x15
    .end annotation
.end field

.field public final buyer_email_address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x14
    .end annotation
.end field

.field public final card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.CardPresentOptions#ADAPTER"
        tag = 0x21
    .end annotation
.end field

.field public final cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.CashPaymentDetails#ADAPTER"
        tag = 0x22
    .end annotation
.end field

.field public final create_order:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final customer_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final delay_action:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x24
    .end annotation
.end field

.field public final delay_duration:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final employee_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1d
    .end annotation
.end field

.field public final external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.ExternalPaymentDetails#ADAPTER"
        tag = 0x23
    .end annotation
.end field

.field public final geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.GeoLocation#ADAPTER"
        tag = 0x1f
    .end annotation
.end field

.field public final idempotency_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final location_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x17
    .end annotation
.end field

.field public final order_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final payment_config_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.PaymentFeesSpec#ADAPTER"
        tag = 0x19
    .end annotation
.end field

.field public final reference_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final requested_statement_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1c
    .end annotation
.end field

.field public final revenue_association_tags:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1b
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Address#ADAPTER"
        redacted = true
        tag = 0x16
    .end annotation
.end field

.field public final source_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final statement_description_identifier:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1a
    .end annotation
.end field

.field public final tax_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x13
    .end annotation
.end field

.field public final terminal_checkout_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final tip_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final verification_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x18
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$ProtoAdapter_CreatePaymentRequest;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$ProtoAdapter_CreatePaymentRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x1

    .line 49
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->DEFAULT_AUTOCOMPLETE:Ljava/lang/Boolean;

    const/4 v0, 0x0

    .line 51
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->DEFAULT_CREATE_ORDER:Ljava/lang/Boolean;

    .line 69
    sput-object v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->DEFAULT_ACCEPT_PARTIAL_AUTHORIZATION:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;Lokio/ByteString;)V
    .locals 1

    .line 566
    sget-object v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 567
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->source_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->source_id:Ljava/lang/String;

    .line 568
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    .line 569
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->idempotency_key:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->idempotency_key:Ljava/lang/String;

    .line 570
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 571
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 572
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 573
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 574
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    .line 575
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->delay_duration:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_duration:Ljava/lang/String;

    .line 576
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->delay_action:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_action:Ljava/lang/String;

    .line 577
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->autocomplete:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->autocomplete:Ljava/lang/Boolean;

    .line 578
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->create_order:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->create_order:Ljava/lang/Boolean;

    .line 579
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->order_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->order_id:Ljava/lang/String;

    .line 580
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->customer_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->customer_id:Ljava/lang/String;

    .line 581
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->payment_config_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_config_id:Ljava/lang/String;

    .line 582
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->location_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->location_id:Ljava/lang/String;

    .line 583
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->employee_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->employee_id:Ljava/lang/String;

    .line 584
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->reference_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->reference_id:Ljava/lang/String;

    .line 585
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->verification_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->verification_token:Ljava/lang/String;

    .line 586
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->terminal_checkout_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->terminal_checkout_id:Ljava/lang/String;

    .line 587
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->accept_partial_authorization:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->accept_partial_authorization:Ljava/lang/Boolean;

    .line 588
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->buyer_email_address:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->buyer_email_address:Ljava/lang/String;

    .line 589
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 590
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 591
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->note:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->note:Ljava/lang/String;

    .line 592
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->statement_description_identifier:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->statement_description_identifier:Ljava/lang/String;

    .line 593
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->requested_statement_description:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->requested_statement_description:Ljava/lang/String;

    .line 594
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->revenue_association_tags:Ljava/util/List;

    const-string v0, "revenue_association_tags"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->revenue_association_tags:Ljava/util/List;

    .line 595
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    .line 596
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->auxiliary_info:Ljava/util/List;

    const-string v0, "auxiliary_info"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->auxiliary_info:Ljava/util/List;

    .line 597
    iget-object p2, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    iput-object p2, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    .line 598
    iget-object p1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 643
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 644
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    .line 645
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->source_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->source_id:Ljava/lang/String;

    .line 646
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    .line 647
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->idempotency_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->idempotency_key:Ljava/lang/String;

    .line 648
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 649
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 650
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 651
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 652
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    .line 653
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_duration:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_duration:Ljava/lang/String;

    .line 654
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_action:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_action:Ljava/lang/String;

    .line 655
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->autocomplete:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->autocomplete:Ljava/lang/Boolean;

    .line 656
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->create_order:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->create_order:Ljava/lang/Boolean;

    .line 657
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->order_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->order_id:Ljava/lang/String;

    .line 658
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->customer_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->customer_id:Ljava/lang/String;

    .line 659
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_config_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_config_id:Ljava/lang/String;

    .line 660
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->location_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->location_id:Ljava/lang/String;

    .line 661
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->employee_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->employee_id:Ljava/lang/String;

    .line 662
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->reference_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->reference_id:Ljava/lang/String;

    .line 663
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->verification_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->verification_token:Ljava/lang/String;

    .line 664
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->terminal_checkout_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->terminal_checkout_id:Ljava/lang/String;

    .line 665
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->accept_partial_authorization:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->accept_partial_authorization:Ljava/lang/Boolean;

    .line 666
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->buyer_email_address:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->buyer_email_address:Ljava/lang/String;

    .line 667
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 668
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 669
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->note:Ljava/lang/String;

    .line 670
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->statement_description_identifier:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->statement_description_identifier:Ljava/lang/String;

    .line 671
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->requested_statement_description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->requested_statement_description:Ljava/lang/String;

    .line 672
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->revenue_association_tags:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->revenue_association_tags:Ljava/util/List;

    .line 673
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    .line 674
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->auxiliary_info:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->auxiliary_info:Ljava/util/List;

    .line 675
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    .line 676
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    .line 677
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 682
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1e

    .line 684
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 685
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->source_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 686
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/CardPresentOptions;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 687
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->idempotency_key:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 688
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 689
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 690
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 691
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 692
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/PaymentFeesSpec;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 693
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_duration:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 694
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_action:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 695
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->autocomplete:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 696
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->create_order:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 697
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->order_id:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 698
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->customer_id:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 699
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_config_id:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 700
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 701
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->employee_id:Ljava/lang/String;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 702
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 703
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->verification_token:Ljava/lang/String;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 704
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->terminal_checkout_id:Ljava/lang/String;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 705
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->accept_partial_authorization:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 706
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->buyer_email_address:Ljava/lang/String;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 707
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Address;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 708
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Address;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 709
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->note:Ljava/lang/String;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 710
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->statement_description_identifier:Ljava/lang/String;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 711
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->requested_statement_description:Ljava/lang/String;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 712
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->revenue_association_tags:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 713
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/GeoLocation;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 714
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->auxiliary_info:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 715
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/CashPaymentDetails;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 716
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;->hashCode()I

    move-result v2

    :cond_1d
    add-int/2addr v0, v2

    .line 717
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1e
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;
    .locals 2

    .line 603
    new-instance v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;-><init>()V

    .line 604
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->source_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->source_id:Ljava/lang/String;

    .line 605
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    .line 606
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->idempotency_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->idempotency_key:Ljava/lang/String;

    .line 607
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 608
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 609
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 610
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 611
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    .line 612
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_duration:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->delay_duration:Ljava/lang/String;

    .line 613
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_action:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->delay_action:Ljava/lang/String;

    .line 614
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->autocomplete:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->autocomplete:Ljava/lang/Boolean;

    .line 615
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->create_order:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->create_order:Ljava/lang/Boolean;

    .line 616
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->order_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->order_id:Ljava/lang/String;

    .line 617
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->customer_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->customer_id:Ljava/lang/String;

    .line 618
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_config_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->payment_config_id:Ljava/lang/String;

    .line 619
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->location_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->location_id:Ljava/lang/String;

    .line 620
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->employee_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->employee_id:Ljava/lang/String;

    .line 621
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->reference_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->reference_id:Ljava/lang/String;

    .line 622
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->verification_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->verification_token:Ljava/lang/String;

    .line 623
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->terminal_checkout_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->terminal_checkout_id:Ljava/lang/String;

    .line 624
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->accept_partial_authorization:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->accept_partial_authorization:Ljava/lang/Boolean;

    .line 625
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->buyer_email_address:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->buyer_email_address:Ljava/lang/String;

    .line 626
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 627
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 628
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->note:Ljava/lang/String;

    .line 629
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->statement_description_identifier:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->statement_description_identifier:Ljava/lang/String;

    .line 630
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->requested_statement_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->requested_statement_description:Ljava/lang/String;

    .line 631
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->revenue_association_tags:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->revenue_association_tags:Ljava/util/List;

    .line 632
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    .line 633
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->auxiliary_info:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->auxiliary_info:Ljava/util/List;

    .line 634
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    .line 635
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    .line 636
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->newBuilder()Lcom/squareup/protos/connect/v2/CreatePaymentRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 724
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 725
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->source_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", source_id=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 726
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    if-eqz v1, :cond_1

    const-string v1, ", card_present_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->card_present_options:Lcom/squareup/protos/connect/v2/CardPresentOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 727
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->idempotency_key:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", idempotency_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->idempotency_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 728
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", amount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 729
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", tip_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 730
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", tax_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 731
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", app_fee_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->app_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 732
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    if-eqz v1, :cond_7

    const-string v1, ", payment_fees_spec="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_fees_spec:Lcom/squareup/protos/connect/v2/PaymentFeesSpec;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 733
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_duration:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", delay_duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_duration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 734
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_action:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", delay_action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->delay_action:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 735
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->autocomplete:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", autocomplete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->autocomplete:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 736
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->create_order:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", create_order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->create_order:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 737
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->order_id:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", order_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->order_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 738
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->customer_id:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", customer_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->customer_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 739
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_config_id:Ljava/lang/String;

    if-eqz v1, :cond_e

    const-string v1, ", payment_config_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->payment_config_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 740
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_f

    const-string v1, ", location_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->location_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 741
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->employee_id:Ljava/lang/String;

    if-eqz v1, :cond_10

    const-string v1, ", employee_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->employee_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 742
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_11

    const-string v1, ", reference_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->reference_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 743
    :cond_11
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->verification_token:Ljava/lang/String;

    if-eqz v1, :cond_12

    const-string v1, ", verification_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->verification_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 744
    :cond_12
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->terminal_checkout_id:Ljava/lang/String;

    if-eqz v1, :cond_13

    const-string v1, ", terminal_checkout_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->terminal_checkout_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 745
    :cond_13
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->accept_partial_authorization:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    const-string v1, ", accept_partial_authorization="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->accept_partial_authorization:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 746
    :cond_14
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->buyer_email_address:Ljava/lang/String;

    if-eqz v1, :cond_15

    const-string v1, ", buyer_email_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 747
    :cond_15
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->billing_address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_16

    const-string v1, ", billing_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 748
    :cond_16
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->shipping_address:Lcom/squareup/protos/connect/v2/resources/Address;

    if-eqz v1, :cond_17

    const-string v1, ", shipping_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 749
    :cond_17
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->note:Ljava/lang/String;

    if-eqz v1, :cond_18

    const-string v1, ", note=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 750
    :cond_18
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->statement_description_identifier:Ljava/lang/String;

    if-eqz v1, :cond_19

    const-string v1, ", statement_description_identifier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->statement_description_identifier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 751
    :cond_19
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->requested_statement_description:Ljava/lang/String;

    if-eqz v1, :cond_1a

    const-string v1, ", requested_statement_description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->requested_statement_description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 752
    :cond_1a
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->revenue_association_tags:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1b

    const-string v1, ", revenue_association_tags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->revenue_association_tags:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 753
    :cond_1b
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    if-eqz v1, :cond_1c

    const-string v1, ", geo_location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->geo_location:Lcom/squareup/protos/connect/v2/GeoLocation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 754
    :cond_1c
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->auxiliary_info:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1d

    const-string v1, ", auxiliary_info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->auxiliary_info:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 755
    :cond_1d
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    if-eqz v1, :cond_1e

    const-string v1, ", cash_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->cash_details:Lcom/squareup/protos/connect/v2/CashPaymentDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 756
    :cond_1e
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    if-eqz v1, :cond_1f

    const-string v1, ", external_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->external_details:Lcom/squareup/protos/connect/v2/ExternalPaymentDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1f
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CreatePaymentRequest{"

    .line 757
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
