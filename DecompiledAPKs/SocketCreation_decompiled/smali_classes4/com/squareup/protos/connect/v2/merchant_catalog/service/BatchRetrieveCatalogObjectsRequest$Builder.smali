.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BatchRetrieveCatalogObjectsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public include_counts:Ljava/lang/Boolean;

.field public include_deleted_objects:Ljava/lang/Boolean;

.field public include_nested_objects:Ljava/lang/Boolean;

.field public include_related_objects:Ljava/lang/Boolean;

.field public object_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public version:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 205
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 206
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->object_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;
    .locals 9

    .line 287
    new-instance v8, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->object_ids:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_related_objects:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_counts:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->version:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_deleted_objects:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_nested_objects:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;-><init>(Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 192
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;

    move-result-object v0

    return-object v0
.end method

.method public include_counts(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_counts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_deleted_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_deleted_objects:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_nested_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_nested_objects:Ljava/lang/Boolean;

    return-object p0
.end method

.method public include_related_objects(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;
    .locals 0

    .line 234
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->include_related_objects:Ljava/lang/Boolean;

    return-object p0
.end method

.method public object_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;"
        }
    .end annotation

    .line 215
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 216
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->object_ids:Ljava/util/List;

    return-object p0
.end method

.method public version(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;
    .locals 0

    .line 264
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest$Builder;->version:Ljava/lang/Long;

    return-object p0
.end method
