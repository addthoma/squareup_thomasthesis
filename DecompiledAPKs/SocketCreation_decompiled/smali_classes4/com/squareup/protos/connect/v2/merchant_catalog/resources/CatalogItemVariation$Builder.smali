.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogItemVariation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public available_for_booking:Ljava/lang/Boolean;

.field public buyer_facing_name:Ljava/lang/String;

.field public catalog_measurement_unit_id:Ljava/lang/String;

.field public intermissions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission;",
            ">;"
        }
    .end annotation
.end field

.field public inventory_alert_threshold:Ljava/lang/Long;

.field public inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

.field public item_id:Ljava/lang/String;

.field public item_option_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field public location_overrides:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;",
            ">;"
        }
    .end annotation
.end field

.field public measurement_unit_id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public no_show_fee:Lcom/squareup/protos/connect/v2/common/Money;

.field public ordinal:Ljava/lang/Integer;

.field public price_description:Ljava/lang/String;

.field public price_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

.field public service_duration:Ljava/lang/Long;

.field public sku:Ljava/lang/String;

.field public track_inventory:Ljava/lang/Boolean;

.field public transition_time:Ljava/lang/Long;

.field public upc:Ljava/lang/String;

.field public user_data:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 512
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 513
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->location_overrides:Ljava/util/List;

    .line 514
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->item_option_values:Ljava/util/List;

    .line 515
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->intermissions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public available_for_booking(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 674
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->available_for_booking:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;
    .locals 2

    .line 752
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 467
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    move-result-object v0

    return-object v0
.end method

.method public buyer_facing_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 632
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->buyer_facing_name:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_measurement_unit_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 722
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->catalog_measurement_unit_id:Ljava/lang/String;

    return-object p0
.end method

.method public intermissions(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Intermission;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;"
        }
    .end annotation

    .line 745
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 746
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->intermissions:Ljava/util/List;

    return-object p0
.end method

.method public inventory_alert_threshold(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 624
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->inventory_alert_threshold:Ljava/lang/Long;

    return-object p0
.end method

.method public inventory_alert_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 611
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    return-object p0
.end method

.method public item_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 522
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->item_id:Ljava/lang/String;

    return-object p0
.end method

.method public item_option_values(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValueForItemVariation;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;"
        }
    .end annotation

    .line 710
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 711
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->item_option_values:Ljava/util/List;

    return-object p0
.end method

.method public location_overrides(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;"
        }
    .end annotation

    .line 591
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 592
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->location_overrides:Ljava/util/List;

    return-object p0
.end method

.method public measurement_unit_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 734
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->measurement_unit_id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 530
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public no_show_fee(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 686
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->no_show_fee:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 564
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public price_description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 663
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->price_description:Ljava/lang/String;

    return-object p0
.end method

.method public price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 583
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public pricing_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 575
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    return-object p0
.end method

.method public service_duration(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 653
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->service_duration:Ljava/lang/Long;

    return-object p0
.end method

.method public sku(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 538
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->sku:Ljava/lang/String;

    return-object p0
.end method

.method public track_inventory(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->track_inventory:Ljava/lang/Boolean;

    return-object p0
.end method

.method public transition_time(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 698
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->transition_time:Ljava/lang/Long;

    return-object p0
.end method

.method public upc(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 550
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->upc:Ljava/lang/String;

    return-object p0
.end method

.method public user_data(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;
    .locals 0

    .line 640
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation$Builder;->user_data:Ljava/lang/String;

    return-object p0
.end method
