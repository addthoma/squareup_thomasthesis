.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BatchUpsertCatalogObjectsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public id_mappings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
            ">;"
        }
    .end annotation
.end field

.field public objects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public updated_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 141
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 142
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->errors:Ljava/util/List;

    .line 143
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->objects:Ljava/util/List;

    .line 144
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->id_mappings:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;
    .locals 7

    .line 186
    new-instance v6, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->errors:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->objects:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->updated_at:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->id_mappings:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 132
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;

    move-result-object v0

    return-object v0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;"
        }
    .end annotation

    .line 151
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public id_mappings(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogIdMapping;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;"
        }
    .end annotation

    .line 179
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->id_mappings:Ljava/util/List;

    return-object p0
.end method

.method public objects(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;"
        }
    .end annotation

    .line 162
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->objects:Ljava/util/List;

    return-object p0
.end method

.method public updated_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse$Builder;->updated_at:Ljava/lang/String;

    return-object p0
.end method
