.class public final enum Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;
.super Ljava/lang/Enum;
.source "MeasurementUnit.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Volume"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume$ProtoAdapter_Volume;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum GENERIC_CUP:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final enum GENERIC_FLUID_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final enum GENERIC_GALLON:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final enum GENERIC_PINT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final enum GENERIC_QUART:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final enum GENERIC_SHOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final enum IMPERIAL_CUBIC_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final enum IMPERIAL_CUBIC_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final enum IMPERIAL_CUBIC_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final enum INVALID_VOLUME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final enum METRIC_LITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

.field public static final enum METRIC_MILLILITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .line 738
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/4 v1, 0x0

    const-string v2, "INVALID_VOLUME"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->INVALID_VOLUME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 745
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/4 v2, 0x1

    const-string v3, "GENERIC_FLUID_OUNCE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_FLUID_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 752
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/4 v3, 0x4

    const/4 v4, 0x2

    const-string v5, "GENERIC_SHOT"

    invoke-direct {v0, v5, v4, v3}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_SHOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 759
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/4 v5, 0x5

    const/4 v6, 0x3

    const-string v7, "GENERIC_CUP"

    invoke-direct {v0, v7, v6, v5}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_CUP:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 766
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/4 v7, 0x6

    const-string v8, "GENERIC_PINT"

    invoke-direct {v0, v8, v3, v7}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_PINT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 773
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/4 v8, 0x7

    const-string v9, "GENERIC_QUART"

    invoke-direct {v0, v9, v5, v8}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_QUART:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 780
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/16 v9, 0x8

    const-string v10, "GENERIC_GALLON"

    invoke-direct {v0, v10, v7, v9}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_GALLON:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 787
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/16 v10, 0x9

    const-string v11, "IMPERIAL_CUBIC_INCH"

    invoke-direct {v0, v11, v8, v10}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 794
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/16 v11, 0xa

    const-string v12, "IMPERIAL_CUBIC_FOOT"

    invoke-direct {v0, v12, v9, v11}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 801
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/16 v12, 0xb

    const-string v13, "IMPERIAL_CUBIC_YARD"

    invoke-direct {v0, v13, v10, v12}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 808
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const-string v13, "METRIC_MILLILITER"

    invoke-direct {v0, v13, v11, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_MILLILITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 815
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const-string v13, "METRIC_LITER"

    invoke-direct {v0, v13, v12, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_LITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 737
    sget-object v13, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->INVALID_VOLUME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_FLUID_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_SHOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_CUP:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_PINT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_QUART:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_GALLON:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_MILLILITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_LITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    aput-object v1, v0, v12

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->$VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    .line 817
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume$ProtoAdapter_Volume;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume$ProtoAdapter_Volume;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 821
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 822
    iput p3, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 839
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_YARD:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0

    .line 838
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_FOOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0

    .line 837
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->IMPERIAL_CUBIC_INCH:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0

    .line 836
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_GALLON:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0

    .line 835
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_QUART:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0

    .line 834
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_PINT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0

    .line 833
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_CUP:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0

    .line 832
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_SHOT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0

    .line 841
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_LITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0

    .line 840
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->METRIC_MILLILITER:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0

    .line 831
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->GENERIC_FLUID_OUNCE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0

    .line 830
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->INVALID_VOLUME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;
    .locals 1

    .line 737
    const-class v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;
    .locals 1

    .line 737
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->$VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 848
    iget v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Volume;->value:I

    return v0
.end method
