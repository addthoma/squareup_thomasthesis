.class public final enum Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;
.super Ljava/lang/Enum;
.source "Tender.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Level"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level$ProtoAdapter_Level;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum HIGH:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

.field public static final enum MODERATE:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

.field public static final enum NORMAL:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

.field public static final enum UNKNOWN_LEVEL:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1493
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_LEVEL"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->UNKNOWN_LEVEL:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    .line 1498
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    const/4 v2, 0x1

    const-string v3, "NORMAL"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->NORMAL:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    .line 1503
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    const/4 v3, 0x2

    const-string v4, "MODERATE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->MODERATE:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    .line 1508
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    const/4 v4, 0x3

    const-string v5, "HIGH"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->HIGH:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    .line 1492
    sget-object v5, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->UNKNOWN_LEVEL:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->NORMAL:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->MODERATE:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->HIGH:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    .line 1510
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level$ProtoAdapter_Level;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level$ProtoAdapter_Level;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1514
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1515
    iput p3, p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1526
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->HIGH:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    return-object p0

    .line 1525
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->MODERATE:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    return-object p0

    .line 1524
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->NORMAL:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    return-object p0

    .line 1523
    :cond_3
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->UNKNOWN_LEVEL:Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;
    .locals 1

    .line 1492
    const-class v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;
    .locals 1

    .line 1492
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1533
    iget v0, p0, Lcom/squareup/protos/connect/v2/resources/Tender$RiskEvaluation$Level;->value:I

    return v0
.end method
