.class public final Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TerminalCheckout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/TerminalCheckout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/TerminalCheckout;",
        "Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allowed_payment_methods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public app_id:Ljava/lang/String;

.field public cancel_reason:Ljava/lang/String;

.field public created_at:Ljava/lang/String;

.field public deadline:Ljava/lang/String;

.field public deadline_duration:Ljava/lang/String;

.field public device_options:Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

.field public id:Ljava/lang/String;

.field public metadata:Lcom/squareup/protos/connect/v2/CheckoutMetadata;

.field public note:Ljava/lang/String;

.field public options:Lcom/squareup/protos/connect/v2/PaymentOptions;

.field public owner_token:Ljava/lang/String;

.field public payment_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public reference_id:Ljava/lang/String;

.field public required_input:Ljava/lang/String;

.field public status:Ljava/lang/String;

.field public target_token:Ljava/lang/String;

.field public type:Ljava/lang/String;

.field public updated_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 485
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 486
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->allowed_payment_methods:Ljava/util/List;

    .line 487
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->payment_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allowed_payment_methods(Ljava/util/List;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;"
        }
    .end annotation

    .line 554
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 555
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->allowed_payment_methods:Ljava/util/List;

    return-object p0
.end method

.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 519
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public app_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 683
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->app_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/TerminalCheckout;
    .locals 2

    .line 722
    new-instance v0, Lcom/squareup/protos/connect/v2/TerminalCheckout;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/connect/v2/TerminalCheckout;-><init>(Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 444
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->build()Lcom/squareup/protos/connect/v2/TerminalCheckout;

    move-result-object v0

    return-object v0
.end method

.method public cancel_reason(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 638
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->cancel_reason:Ljava/lang/String;

    return-object p0
.end method

.method public created_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 661
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->created_at:Ljava/lang/String;

    return-object p0
.end method

.method public deadline(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 591
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->deadline:Ljava/lang/String;

    return-object p0
.end method

.method public deadline_duration(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 604
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->deadline_duration:Ljava/lang/String;

    return-object p0
.end method

.method public device_options(Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 575
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->device_options:Lcom/squareup/protos/connect/v2/DeviceCheckoutOptions;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 497
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public metadata(Lcom/squareup/protos/connect/v2/CheckoutMetadata;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 716
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->metadata:Lcom/squareup/protos/connect/v2/CheckoutMetadata;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 543
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public options(Lcom/squareup/protos/connect/v2/PaymentOptions;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 565
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->options:Lcom/squareup/protos/connect/v2/PaymentOptions;

    return-object p0
.end method

.method public owner_token(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 694
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->owner_token:Ljava/lang/String;

    return-object p0
.end method

.method public payment_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;"
        }
    .end annotation

    .line 649
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 650
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->payment_ids:Ljava/util/List;

    return-object p0
.end method

.method public reference_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 532
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->reference_id:Ljava/lang/String;

    return-object p0
.end method

.method public required_input(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 627
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->required_input:Ljava/lang/String;

    return-object p0
.end method

.method public status(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 616
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->status:Ljava/lang/String;

    return-object p0
.end method

.method public target_token(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 705
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->target_token:Ljava/lang/String;

    return-object p0
.end method

.method public type(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 509
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->type:Ljava/lang/String;

    return-object p0
.end method

.method public updated_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;
    .locals 0

    .line 672
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/TerminalCheckout$Builder;->updated_at:Ljava/lang/String;

    return-object p0
.end method
