.class final Lcom/squareup/protos/connect/v2/common/Coordinates$ProtoAdapter_Coordinates;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Coordinates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/Coordinates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Coordinates"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/common/Coordinates;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 134
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/common/Coordinates;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/common/Coordinates;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 153
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;-><init>()V

    .line 154
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 155
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 160
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 158
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->longitude(Ljava/lang/Double;)Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;

    goto :goto_0

    .line 157
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->latitude(Ljava/lang/Double;)Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;

    goto :goto_0

    .line 164
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 165
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->build()Lcom/squareup/protos/connect/v2/common/Coordinates;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/Coordinates$ProtoAdapter_Coordinates;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/common/Coordinates;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/common/Coordinates;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/Coordinates;->latitude:Ljava/lang/Double;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 147
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/common/Coordinates;->longitude:Ljava/lang/Double;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 148
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/common/Coordinates;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    check-cast p2, Lcom/squareup/protos/connect/v2/common/Coordinates;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/common/Coordinates$ProtoAdapter_Coordinates;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/common/Coordinates;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/common/Coordinates;)I
    .locals 4

    .line 139
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/common/Coordinates;->latitude:Ljava/lang/Double;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->DOUBLE:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/common/Coordinates;->longitude:Ljava/lang/Double;

    const/4 v3, 0x2

    .line 140
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/Coordinates;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 132
    check-cast p1, Lcom/squareup/protos/connect/v2/common/Coordinates;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/Coordinates$ProtoAdapter_Coordinates;->encodedSize(Lcom/squareup/protos/connect/v2/common/Coordinates;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/common/Coordinates;)Lcom/squareup/protos/connect/v2/common/Coordinates;
    .locals 0

    .line 170
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/Coordinates;->newBuilder()Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;

    move-result-object p1

    .line 171
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 172
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/common/Coordinates$Builder;->build()Lcom/squareup/protos/connect/v2/common/Coordinates;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 132
    check-cast p1, Lcom/squareup/protos/connect/v2/common/Coordinates;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/common/Coordinates$ProtoAdapter_Coordinates;->redact(Lcom/squareup/protos/connect/v2/common/Coordinates;)Lcom/squareup/protos/connect/v2/common/Coordinates;

    move-result-object p1

    return-object p1
.end method
