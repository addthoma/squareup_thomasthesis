.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogItemOptionValue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public color:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public item_option_id:Ljava/lang/String;

.field public item_variation_count:Ljava/lang/Long;

.field public name:Ljava/lang/String;

.field public ordinal:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 197
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;
    .locals 9

    .line 264
    new-instance v8, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->item_option_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->description:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->color:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->ordinal:Ljava/lang/Integer;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->item_variation_count:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 184
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    move-result-object v0

    return-object v0
.end method

.method public color(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->color:Ljava/lang/String;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public item_option_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;
    .locals 0

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->item_option_id:Ljava/lang/String;

    return-object p0
.end method

.method public item_variation_count(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->item_variation_count:Ljava/lang/Long;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method
