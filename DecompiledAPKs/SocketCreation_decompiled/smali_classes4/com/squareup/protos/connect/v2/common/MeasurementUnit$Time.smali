.class public final enum Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;
.super Ljava/lang/Enum;
.source "MeasurementUnit.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Time"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time$ProtoAdapter_Time;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum GENERIC_DAY:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

.field public static final enum GENERIC_HOUR:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

.field public static final enum GENERIC_MILLISECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

.field public static final enum GENERIC_MINUTE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

.field public static final enum GENERIC_SECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

.field public static final enum INVALID_TIME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 1006
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    const/4 v1, 0x0

    const-string v2, "INVALID_TIME"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->INVALID_TIME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    .line 1013
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    const/4 v2, 0x1

    const-string v3, "GENERIC_MILLISECOND"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MILLISECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    .line 1020
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    const/4 v3, 0x2

    const-string v4, "GENERIC_SECOND"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_SECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    .line 1027
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    const/4 v4, 0x3

    const-string v5, "GENERIC_MINUTE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MINUTE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    .line 1034
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    const/4 v5, 0x4

    const-string v6, "GENERIC_HOUR"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_HOUR:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    .line 1041
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    const/4 v6, 0x5

    const-string v7, "GENERIC_DAY"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_DAY:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    .line 1005
    sget-object v7, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->INVALID_TIME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MILLISECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_SECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MINUTE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_HOUR:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_DAY:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->$VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    .line 1043
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time$ProtoAdapter_Time;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time$ProtoAdapter_Time;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1047
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1048
    iput p3, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 1061
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_DAY:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    return-object p0

    .line 1060
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_HOUR:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    return-object p0

    .line 1059
    :cond_2
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MINUTE:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    return-object p0

    .line 1058
    :cond_3
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_SECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    return-object p0

    .line 1057
    :cond_4
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->GENERIC_MILLISECOND:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    return-object p0

    .line 1056
    :cond_5
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->INVALID_TIME:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;
    .locals 1

    .line 1005
    const-class v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;
    .locals 1

    .line 1005
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->$VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1068
    iget v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Time;->value:I

    return v0
.end method
