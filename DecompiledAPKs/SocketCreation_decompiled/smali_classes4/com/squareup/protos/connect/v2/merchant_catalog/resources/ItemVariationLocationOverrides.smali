.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;
.super Lcom/squareup/wire/Message;
.source "ItemVariationLocationOverrides.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$ProtoAdapter_ItemVariationLocationOverrides;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_INVENTORY_ALERT_THRESHOLD:Ljava/lang/Long;

.field public static final DEFAULT_INVENTORY_ALERT_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

.field public static final DEFAULT_LOCATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PRICE_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_PRICING_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

.field public static final DEFAULT_TRACK_INVENTORY:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final inventory_alert_threshold:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x6
    .end annotation
.end field

.field public final inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.InventoryAlertType#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final location_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final price_description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final price_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogPricingType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final track_inventory:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 26
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$ProtoAdapter_ItemVariationLocationOverrides;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$ProtoAdapter_ItemVariationLocationOverrides;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;->FIXED_PRICING:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->DEFAULT_PRICING_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    const/4 v0, 0x0

    .line 34
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->DEFAULT_TRACK_INVENTORY:Ljava/lang/Boolean;

    .line 36
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;->NONE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->DEFAULT_INVENTORY_ALERT_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    const-wide/16 v0, 0x0

    .line 38
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->DEFAULT_INVENTORY_ALERT_THRESHOLD:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 9

    .line 121
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;Ljava/lang/Boolean;Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 128
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->location_id:Ljava/lang/String;

    .line 130
    iput-object p2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 131
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    .line 132
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->track_inventory:Ljava/lang/Boolean;

    .line 133
    iput-object p5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    .line 134
    iput-object p6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_threshold:Ljava/lang/Long;

    .line 135
    iput-object p7, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_description:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 155
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 156
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;

    .line 157
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->location_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->location_id:Ljava/lang/String;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->track_inventory:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->track_inventory:Ljava/lang/Boolean;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    .line 162
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_threshold:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_threshold:Ljava/lang/Long;

    .line 163
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_description:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_description:Ljava/lang/String;

    .line 164
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 169
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 171
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->location_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->track_inventory:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_threshold:Ljava/lang/Long;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_description:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 179
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;
    .locals 2

    .line 140
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;-><init>()V

    .line 141
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->location_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->location_id:Ljava/lang/String;

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->track_inventory:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->track_inventory:Ljava/lang/Boolean;

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_threshold:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->inventory_alert_threshold:Ljava/lang/Long;

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->price_description:Ljava/lang/String;

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", location_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->location_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", price_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 189
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    if-eqz v1, :cond_2

    const-string v1, ", pricing_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->pricing_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->track_inventory:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", track_inventory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->track_inventory:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    if-eqz v1, :cond_4

    const-string v1, ", inventory_alert_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/InventoryAlertType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 192
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_threshold:Ljava/lang/Long;

    if-eqz v1, :cond_5

    const-string v1, ", inventory_alert_threshold="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->inventory_alert_threshold:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_description:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", price_description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/ItemVariationLocationOverrides;->price_description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ItemVariationLocationOverrides{"

    .line 194
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
