.class public final Lcom/squareup/protos/connect/v2/AuxiliaryInfo$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AuxiliaryInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/AuxiliaryInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/AuxiliaryInfo;",
        "Lcom/squareup/protos/connect/v2/AuxiliaryInfo$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public key:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/AuxiliaryInfo;
    .locals 4

    .line 114
    new-instance v0, Lcom/squareup/protos/connect/v2/AuxiliaryInfo;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/AuxiliaryInfo$Builder;->key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/AuxiliaryInfo$Builder;->value:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/AuxiliaryInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/AuxiliaryInfo$Builder;->build()Lcom/squareup/protos/connect/v2/AuxiliaryInfo;

    move-result-object v0

    return-object v0
.end method

.method public key(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/AuxiliaryInfo$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AuxiliaryInfo$Builder;->key:Ljava/lang/String;

    return-object p0
.end method

.method public value(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/AuxiliaryInfo$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/AuxiliaryInfo$Builder;->value:Ljava/lang/String;

    return-object p0
.end method
