.class final Lcom/squareup/protos/connect/v2/CardPaymentDetails$ProtoAdapter_CardPaymentDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CardPaymentDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/CardPaymentDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CardPaymentDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/CardPaymentDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 547
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/CardPaymentDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 592
    new-instance v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;-><init>()V

    .line 593
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 594
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 612
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 610
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->pan_fidelius_token(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto :goto_0

    .line 609
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/connect/v2/DeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/DeviceDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->device_details(Lcom/squareup/protos/connect/v2/DeviceDetails;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto :goto_0

    .line 608
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->statement_description(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto :goto_0

    .line 607
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->verification_results(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto :goto_0

    .line 606
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->verification_method(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto :goto_0

    .line 605
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->application_cryptogram(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto :goto_0

    .line 604
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->application_name(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto :goto_0

    .line 603
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->application_identifier(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto :goto_0

    .line 602
    :pswitch_8
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->errors:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 601
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->auth_result_code(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto/16 :goto_0

    .line 600
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->avs_status(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto/16 :goto_0

    .line 599
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->cvv_status(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto/16 :goto_0

    .line 598
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->entry_method(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto/16 :goto_0

    .line 597
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/connect/v2/resources/Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/resources/Card;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->card(Lcom/squareup/protos/connect/v2/resources/Card;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto/16 :goto_0

    .line 596
    :pswitch_e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->status(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    goto/16 :goto_0

    .line 616
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 617
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->build()Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 545
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$ProtoAdapter_CardPaymentDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/CardPaymentDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 572
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->status:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 573
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 574
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->entry_method:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 575
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->cvv_status:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 576
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->avs_status:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 577
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->auth_result_code:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 578
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_identifier:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 579
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_name:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 580
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_cryptogram:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 581
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_method:Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 582
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_results:Ljava/lang/String;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 583
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->statement_description:Ljava/lang/String;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 584
    sget-object v0, Lcom/squareup/protos/connect/v2/DeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 585
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->pan_fidelius_token:Ljava/lang/String;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 586
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->errors:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 587
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 545
    check-cast p2, Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$ProtoAdapter_CardPaymentDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/CardPaymentDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/CardPaymentDetails;)I
    .locals 4

    .line 552
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->status:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    const/4 v3, 0x2

    .line 553
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->entry_method:Ljava/lang/String;

    const/4 v3, 0x3

    .line 554
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->cvv_status:Ljava/lang/String;

    const/4 v3, 0x4

    .line 555
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->avs_status:Ljava/lang/String;

    const/4 v3, 0x5

    .line 556
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->auth_result_code:Ljava/lang/String;

    const/4 v3, 0x6

    .line 557
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_identifier:Ljava/lang/String;

    const/16 v3, 0x8

    .line 558
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_name:Ljava/lang/String;

    const/16 v3, 0x9

    .line 559
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->application_cryptogram:Ljava/lang/String;

    const/16 v3, 0xa

    .line 560
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_method:Ljava/lang/String;

    const/16 v3, 0xb

    .line 561
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->verification_results:Ljava/lang/String;

    const/16 v3, 0xc

    .line 562
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->statement_description:Ljava/lang/String;

    const/16 v3, 0xd

    .line 563
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/DeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    const/16 v3, 0xe

    .line 564
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->pan_fidelius_token:Ljava/lang/String;

    const/16 v3, 0xf

    .line 565
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 566
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->errors:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 545
    check-cast p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$ProtoAdapter_CardPaymentDetails;->encodedSize(Lcom/squareup/protos/connect/v2/CardPaymentDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/CardPaymentDetails;)Lcom/squareup/protos/connect/v2/CardPaymentDetails;
    .locals 2

    .line 622
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CardPaymentDetails;->newBuilder()Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;

    move-result-object p1

    .line 623
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Card;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/resources/Card;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->card:Lcom/squareup/protos/connect/v2/resources/Card;

    .line 624
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/connect/v2/DeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/DeviceDetails;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->device_details:Lcom/squareup/protos/connect/v2/DeviceDetails;

    :cond_1
    const/4 v0, 0x0

    .line 625
    iput-object v0, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->pan_fidelius_token:Ljava/lang/String;

    .line 626
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->errors:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Error;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 627
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 628
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$Builder;->build()Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 545
    check-cast p1, Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/CardPaymentDetails$ProtoAdapter_CardPaymentDetails;->redact(Lcom/squareup/protos/connect/v2/CardPaymentDetails;)Lcom/squareup/protos/connect/v2/CardPaymentDetails;

    move-result-object p1

    return-object p1
.end method
