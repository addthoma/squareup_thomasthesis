.class public final Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BalancePaymentDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/BalancePaymentDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/BalancePaymentDetails;",
        "Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public account_id:Ljava/lang/String;

.field public errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;"
        }
    .end annotation
.end field

.field public status:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 127
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 128
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->errors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public account_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->account_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/BalancePaymentDetails;
    .locals 5

    .line 160
    new-instance v0, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->account_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->status:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->errors:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 120
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->build()Lcom/squareup/protos/connect/v2/BalancePaymentDetails;

    move-result-object v0

    return-object v0
.end method

.method public errors(Ljava/util/List;)Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Error;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;"
        }
    .end annotation

    .line 153
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->errors:Ljava/util/List;

    return-object p0
.end method

.method public status(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/BalancePaymentDetails$Builder;->status:Ljava/lang/String;

    return-object p0
.end method
