.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;
.super Lcom/squareup/wire/Message;
.source "CatalogQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomAttributeFilter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$ProtoAdapter_CustomAttributeFilter;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;,
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CUSTOM_ATTRIBUTE_MAX_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_CUSTOM_ATTRIBUTE_MIN_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_CUSTOM_ATTRIBUTE_VALUE_EXACT:Ljava/lang/String; = ""

.field public static final DEFAULT_CUSTOM_ATTRIBUTE_VALUE_PREFIX:Ljava/lang/String; = ""

.field public static final DEFAULT_FILTER_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

.field private static final serialVersionUID:J


# instance fields
.field public final custom_attribute_definition_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final custom_attribute_max_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final custom_attribute_min_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final custom_attribute_value_exact:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final custom_attribute_value_prefix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.merchant_catalog.resources.CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 2256
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$ProtoAdapter_CustomAttributeFilter;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$ProtoAdapter_CustomAttributeFilter;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 2260
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;->CUSTOM_ATTRIBUTE_FILTER_TYPE_DO_NOT_USE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->DEFAULT_FILTER_TYPE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 2333
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 2340
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 2341
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    const-string p1, "custom_attribute_definition_ids"

    .line 2342
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_definition_ids:Ljava/util/List;

    .line 2343
    iput-object p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_exact:Ljava/lang/String;

    .line 2344
    iput-object p4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_prefix:Ljava/lang/String;

    .line 2345
    iput-object p5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_min_value:Ljava/lang/String;

    .line 2346
    iput-object p6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_max_value:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 2365
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 2366
    :cond_1
    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;

    .line 2367
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    .line 2368
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_definition_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_definition_ids:Ljava/util/List;

    .line 2369
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_exact:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_exact:Ljava/lang/String;

    .line 2370
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_prefix:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_prefix:Ljava/lang/String;

    .line 2371
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_min_value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_min_value:Ljava/lang/String;

    .line 2372
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_max_value:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_max_value:Ljava/lang/String;

    .line 2373
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 2378
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 2380
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 2381
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2382
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_definition_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2383
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_exact:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2384
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_prefix:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2385
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_min_value:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 2386
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_max_value:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 2387
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;
    .locals 2

    .line 2351
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;-><init>()V

    .line 2352
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    .line 2353
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_definition_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_definition_ids:Ljava/util/List;

    .line 2354
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_exact:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_value_exact:Ljava/lang/String;

    .line 2355
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_prefix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_value_prefix:Ljava/lang/String;

    .line 2356
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_min_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_min_value:Ljava/lang/String;

    .line 2357
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_max_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_max_value:Ljava/lang/String;

    .line 2358
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 2255
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->newBuilder()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 2394
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2395
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    if-eqz v1, :cond_0

    const-string v1, ", filter_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2396
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_definition_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", custom_attribute_definition_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_definition_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2397
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_exact:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", custom_attribute_value_exact="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_exact:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2398
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_prefix:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", custom_attribute_value_prefix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_value_prefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2399
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_min_value:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", custom_attribute_min_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_min_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2400
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_max_value:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", custom_attribute_max_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;->custom_attribute_max_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CustomAttributeFilter{"

    .line 2401
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
