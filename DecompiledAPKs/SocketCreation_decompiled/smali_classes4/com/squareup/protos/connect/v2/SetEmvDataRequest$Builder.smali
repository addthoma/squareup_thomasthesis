.class public final Lcom/squareup/protos/connect/v2/SetEmvDataRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SetEmvDataRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/SetEmvDataRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/SetEmvDataRequest;",
        "Lcom/squareup/protos/connect/v2/SetEmvDataRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public encrypted_emv_data:Ljava/lang/String;

.field public payment_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/SetEmvDataRequest;
    .locals 4

    .line 140
    new-instance v0, Lcom/squareup/protos/connect/v2/SetEmvDataRequest;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/SetEmvDataRequest$Builder;->payment_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/SetEmvDataRequest$Builder;->encrypted_emv_data:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/connect/v2/SetEmvDataRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/SetEmvDataRequest$Builder;->build()Lcom/squareup/protos/connect/v2/SetEmvDataRequest;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_emv_data(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/SetEmvDataRequest$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/SetEmvDataRequest$Builder;->encrypted_emv_data:Ljava/lang/String;

    return-object p0
.end method

.method public payment_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/SetEmvDataRequest$Builder;
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/SetEmvDataRequest$Builder;->payment_id:Ljava/lang/String;

    return-object p0
.end method
