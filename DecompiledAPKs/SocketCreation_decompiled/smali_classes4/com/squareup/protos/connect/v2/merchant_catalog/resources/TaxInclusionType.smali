.class public final enum Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;
.super Ljava/lang/Enum;
.source "TaxInclusionType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType$ProtoAdapter_TaxInclusionType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ADDITIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

.field public static final enum INCLUSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 21
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    const/4 v1, 0x0

    const-string v2, "ADDITIVE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;->ADDITIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    .line 31
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    const/4 v2, 0x1

    const-string v3, "INCLUSIVE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;->INCLUSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    .line 13
    sget-object v3, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;->ADDITIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;->INCLUSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    .line 33
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType$ProtoAdapter_TaxInclusionType;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType$ProtoAdapter_TaxInclusionType;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 47
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;->INCLUSIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    return-object p0

    .line 46
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;->ADDITIVE:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;->$VALUES:[Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 54
    iget v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/TaxInclusionType;->value:I

    return v0
.end method
