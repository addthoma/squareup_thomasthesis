.class public final enum Lcom/squareup/protos/connect/v2/AckReasons$AckReason;
.super Ljava/lang/Enum;
.source "AckReasons.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/AckReasons;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AckReason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/AckReasons$AckReason$ProtoAdapter_AckReason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/AckReasons$AckReason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/AckReasons$AckReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

.field public static final enum INITIAL_POLL:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

.field public static final enum OTHER:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

.field public static final enum POLL:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

.field public static final enum PUSH:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

.field public static final enum UNKNOWN:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

.field public static final enum USER_INITIATED:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 28
    new-instance v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->DO_NOT_USE:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 33
    new-instance v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    const/4 v2, 0x1

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->UNKNOWN:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 38
    new-instance v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    const/4 v3, 0x2

    const-string v4, "PUSH"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->PUSH:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 43
    new-instance v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    const/4 v4, 0x3

    const-string v5, "POLL"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->POLL:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 48
    new-instance v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    const/4 v5, 0x4

    const-string v6, "INITIAL_POLL"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->INITIAL_POLL:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 53
    new-instance v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    const/4 v6, 0x5

    const-string v7, "USER_INITIATED"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->USER_INITIATED:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 58
    new-instance v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    const/4 v7, 0x6

    const-string v8, "OTHER"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->OTHER:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 24
    sget-object v8, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->DO_NOT_USE:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->UNKNOWN:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->PUSH:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->POLL:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->INITIAL_POLL:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->USER_INITIATED:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->OTHER:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->$VALUES:[Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    .line 60
    new-instance v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason$ProtoAdapter_AckReason;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/AckReasons$AckReason$ProtoAdapter_AckReason;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 65
    iput p3, p0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/AckReasons$AckReason;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 79
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->OTHER:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-object p0

    .line 78
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->USER_INITIATED:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-object p0

    .line 77
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->INITIAL_POLL:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-object p0

    .line 76
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->POLL:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-object p0

    .line 75
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->PUSH:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-object p0

    .line 74
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->UNKNOWN:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-object p0

    .line 73
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->DO_NOT_USE:Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/AckReasons$AckReason;
    .locals 1

    .line 24
    const-class v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/AckReasons$AckReason;
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->$VALUES:[Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/AckReasons$AckReason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 86
    iget v0, p0, Lcom/squareup/protos/connect/v2/AckReasons$AckReason;->value:I

    return v0
.end method
