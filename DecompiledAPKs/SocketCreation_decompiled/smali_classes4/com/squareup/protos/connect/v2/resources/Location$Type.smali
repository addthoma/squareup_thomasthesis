.class public final enum Lcom/squareup/protos/connect/v2/resources/Location$Type;
.super Ljava/lang/Enum;
.source "Location.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Location;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/resources/Location$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/resources/Location$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/resources/Location$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/resources/Location$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum MOBILE:Lcom/squareup/protos/connect/v2/resources/Location$Type;

.field public static final enum PHYSICAL:Lcom/squareup/protos/connect/v2/resources/Location$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 971
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Location$Type;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "PHYSICAL"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/connect/v2/resources/Location$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Type;->PHYSICAL:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    .line 976
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Location$Type;

    const/4 v3, 0x2

    const-string v4, "MOBILE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/connect/v2/resources/Location$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Type;->MOBILE:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    new-array v0, v3, [Lcom/squareup/protos/connect/v2/resources/Location$Type;

    .line 967
    sget-object v3, Lcom/squareup/protos/connect/v2/resources/Location$Type;->PHYSICAL:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Location$Type;->MOBILE:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Type;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Location$Type;

    .line 978
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Location$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Location$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 982
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 983
    iput p3, p0, Lcom/squareup/protos/connect/v2/resources/Location$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/resources/Location$Type;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 992
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Location$Type;->MOBILE:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    return-object p0

    .line 991
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/resources/Location$Type;->PHYSICAL:Lcom/squareup/protos/connect/v2/resources/Location$Type;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Location$Type;
    .locals 1

    .line 967
    const-class v0, Lcom/squareup/protos/connect/v2/resources/Location$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/resources/Location$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/resources/Location$Type;
    .locals 1

    .line 967
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Location$Type;->$VALUES:[Lcom/squareup/protos/connect/v2/resources/Location$Type;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/resources/Location$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/resources/Location$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 999
    iget v0, p0, Lcom/squareup/protos/connect/v2/resources/Location$Type;->value:I

    return v0
.end method
