.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public DEPRECATED_custom_attributes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field public absent_at_location_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public catalog_v1_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;",
            ">;"
        }
    .end annotation
.end field

.field public category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

.field public custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

.field public custom_attribute_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field public dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

.field public discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

.field public id:Ljava/lang/String;

.field public image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

.field public image_id:Ljava/lang/String;

.field public is_deleted:Ljava/lang/Boolean;

.field public item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

.field public item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

.field public item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

.field public item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

.field public measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

.field public modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

.field public modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

.field public present_at_all_locations:Ljava/lang/Boolean;

.field public present_at_location_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

.field public product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

.field public quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

.field public resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

.field public subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

.field public tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

.field public tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

.field public time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

.field public type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

.field public updated_at:Ljava/lang/String;

.field public version:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 650
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 651
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_values:Ljava/util/List;

    .line 652
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->DEPRECATED_custom_attributes:Ljava/util/List;

    .line 653
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->catalog_v1_ids:Ljava/util/List;

    .line 654
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_location_ids:Ljava/util/List;

    .line 655
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->absent_at_location_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public DEPRECATED_custom_attributes(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;"
        }
    .end annotation

    .line 739
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 740
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->DEPRECATED_custom_attributes:Ljava/util/List;

    return-object p0
.end method

.method public absent_at_location_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;"
        }
    .end annotation

    .line 782
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 783
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->absent_at_location_ids:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;
    .locals 2

    .line 962
    new-instance v0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 585
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject;

    move-result-object v0

    return-object v0
.end method

.method public catalog_v1_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogV1Id;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;"
        }
    .end annotation

    .line 752
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 753
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->catalog_v1_ids:Ljava/util/List;

    return-object p0
.end method

.method public category_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 810
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->category_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCategory;

    return-object p0
.end method

.method public custom_attribute_definition_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 923
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_definition_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeDefinition;

    return-object p0
.end method

.method public custom_attribute_values(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;"
        }
    .end annotation

    .line 726
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 727
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->custom_attribute_values:Ljava/util/List;

    return-object p0
.end method

.method public dining_option_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 948
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->dining_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiningOption;

    return-object p0
.end method

.method public discount_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 834
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->discount_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogDiscount;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 681
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public image_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 882
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->image_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogImage;

    return-object p0
.end method

.method public image_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 793
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->image_id:Ljava/lang/String;

    return-object p0
.end method

.method public is_deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 714
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->is_deleted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public item_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 802
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItem;

    return-object p0
.end method

.method public item_option_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 906
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOption;

    return-object p0
.end method

.method public item_option_value_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 914
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_option_value_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemOptionValue;

    return-object p0
.end method

.method public item_variation_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 818
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->item_variation_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogItemVariation;

    return-object p0
.end method

.method public measurement_unit_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 890
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->measurement_unit_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogMeasurementUnit;

    return-object p0
.end method

.method public modifier_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 850
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifier;

    return-object p0
.end method

.method public modifier_list_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 842
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->modifier_list_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogModifierList;

    return-object p0
.end method

.method public present_at_all_locations(Ljava/lang/Boolean;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 765
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_all_locations:Ljava/lang/Boolean;

    return-object p0
.end method

.method public present_at_location_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;"
        }
    .end annotation

    .line 773
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 774
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->present_at_location_ids:Ljava/util/List;

    return-object p0
.end method

.method public pricing_rule_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 874
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->pricing_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogPricingRule;

    return-object p0
.end method

.method public product_set_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 866
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->product_set_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogProductSet;

    return-object p0
.end method

.method public quick_amounts_settings_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 932
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->quick_amounts_settings_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuickAmountsSettings;

    return-object p0
.end method

.method public resource_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 956
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->resource_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/Resource;

    return-object p0
.end method

.method public subscription_plan_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 898
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->subscription_plan_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogSubscriptionPlan;

    return-object p0
.end method

.method public tax_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 826
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTax;

    return-object p0
.end method

.method public tax_rule_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 940
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->tax_rule_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTaxRule;

    return-object p0
.end method

.method public time_period_data(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 858
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->time_period_data:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogTimePeriod;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 665
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;

    return-object p0
.end method

.method public updated_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 692
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->updated_at:Ljava/lang/String;

    return-object p0
.end method

.method public version(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;
    .locals 0

    .line 703
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObject$Builder;->version:Ljava/lang/Long;

    return-object p0
.end method
