.class final Lcom/squareup/protos/connect/v2/resources/Refund$ProtoAdapter_Refund;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Refund.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/resources/Refund;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Refund"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/connect/v2/resources/Refund;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 420
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/connect/v2/resources/Refund;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/resources/Refund;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 455
    new-instance v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;-><init>()V

    .line 456
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 457
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 477
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 475
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->additional_recipients:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 474
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->processing_fee_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;

    goto :goto_0

    .line 468
    :pswitch_3
    :try_start_0
    sget-object v4, Lcom/squareup/protos/connect/v2/resources/Refund$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->status(Lcom/squareup/protos/connect/v2/resources/Refund$Status;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 470
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 465
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;

    goto :goto_0

    .line 464
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->reason(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;

    goto :goto_0

    .line 463
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->created_at(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;

    goto :goto_0

    .line 462
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->tender_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;

    goto :goto_0

    .line 461
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->transaction_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;

    goto :goto_0

    .line 460
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->location_id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;

    goto/16 :goto_0

    .line 459
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->id(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/resources/Refund$Builder;

    goto/16 :goto_0

    .line 481
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 482
    invoke-virtual {v0}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Refund;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_0
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 418
    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/resources/Refund$ProtoAdapter_Refund;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/connect/v2/resources/Refund;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/resources/Refund;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 440
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Refund;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 441
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Refund;->location_id:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 442
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Refund;->transaction_id:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 443
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Refund;->tender_id:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 444
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Refund;->created_at:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 445
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Refund;->reason:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 446
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Refund;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 447
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Refund$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Refund;->status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 448
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Refund;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 449
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/connect/v2/resources/Refund;->additional_recipients:Ljava/util/List;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 450
    invoke-virtual {p2}, Lcom/squareup/protos/connect/v2/resources/Refund;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 418
    check-cast p2, Lcom/squareup/protos/connect/v2/resources/Refund;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/connect/v2/resources/Refund$ProtoAdapter_Refund;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/connect/v2/resources/Refund;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/connect/v2/resources/Refund;)I
    .locals 4

    .line 425
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->location_id:Ljava/lang/String;

    const/4 v3, 0x3

    .line 426
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->transaction_id:Ljava/lang/String;

    const/4 v3, 0x4

    .line 427
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->tender_id:Ljava/lang/String;

    const/4 v3, 0x5

    .line 428
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->created_at:Ljava/lang/String;

    const/4 v3, 0x6

    .line 429
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->reason:Ljava/lang/String;

    const/4 v3, 0x7

    .line 430
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x8

    .line 431
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Refund$Status;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->status:Lcom/squareup/protos/connect/v2/resources/Refund$Status;

    const/16 v3, 0x9

    .line 432
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0xa

    .line 433
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 434
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/connect/v2/resources/Refund;->additional_recipients:Ljava/util/List;

    const/16 v3, 0xb

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 435
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Refund;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 418
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Refund;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/resources/Refund$ProtoAdapter_Refund;->encodedSize(Lcom/squareup/protos/connect/v2/resources/Refund;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/connect/v2/resources/Refund;)Lcom/squareup/protos/connect/v2/resources/Refund;
    .locals 2

    .line 487
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Refund;->newBuilder()Lcom/squareup/protos/connect/v2/resources/Refund$Builder;

    move-result-object p1

    .line 488
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 489
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->processing_fee_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 490
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->additional_recipients:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/AdditionalRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 491
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 492
    invoke-virtual {p1}, Lcom/squareup/protos/connect/v2/resources/Refund$Builder;->build()Lcom/squareup/protos/connect/v2/resources/Refund;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 418
    check-cast p1, Lcom/squareup/protos/connect/v2/resources/Refund;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/connect/v2/resources/Refund$ProtoAdapter_Refund;->redact(Lcom/squareup/protos/connect/v2/resources/Refund;)Lcom/squareup/protos/connect/v2/resources/Refund;

    move-result-object p1

    return-object p1
.end method
