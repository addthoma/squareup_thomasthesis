.class public final Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogQuery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public custom_attribute_definition_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public custom_attribute_max_value:Ljava/lang/String;

.field public custom_attribute_min_value:Ljava/lang/String;

.field public custom_attribute_value_exact:Ljava/lang/String;

.field public custom_attribute_value_prefix:Ljava/lang/String;

.field public filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 2417
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 2418
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_definition_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;
    .locals 9

    .line 2477
    new-instance v8, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;

    iget-object v1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    iget-object v2, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_definition_ids:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_value_exact:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_value_prefix:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_min_value:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_max_value:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;-><init>(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2404
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->build()Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter;

    move-result-object v0

    return-object v0
.end method

.method public custom_attribute_definition_ids(Ljava/util/List;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;"
        }
    .end annotation

    .line 2435
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 2436
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_definition_ids:Ljava/util/List;

    return-object p0
.end method

.method public custom_attribute_max_value(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;
    .locals 0

    .line 2471
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_max_value:Ljava/lang/String;

    return-object p0
.end method

.method public custom_attribute_min_value(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;
    .locals 0

    .line 2463
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_min_value:Ljava/lang/String;

    return-object p0
.end method

.method public custom_attribute_value_exact(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;
    .locals 0

    .line 2447
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_value_exact:Ljava/lang/String;

    return-object p0
.end method

.method public custom_attribute_value_prefix(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;
    .locals 0

    .line 2455
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->custom_attribute_value_prefix:Ljava/lang/String;

    return-object p0
.end method

.method public filter_type(Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;)Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;
    .locals 0

    .line 2425
    iput-object p1, p0, Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$Builder;->filter_type:Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogQuery$FilteredItems$CustomAttributeFilter$FilterType;

    return-object p0
.end method
