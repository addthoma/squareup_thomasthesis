.class public final enum Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;
.super Ljava/lang/Enum;
.source "MeasurementUnit.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/connect/v2/common/MeasurementUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Generic"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic$ProtoAdapter_Generic;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum INVALID_GENERIC_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

.field public static final enum UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 955
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    const/4 v1, 0x0

    const-string v2, "INVALID_GENERIC_UNIT"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->INVALID_GENERIC_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    .line 962
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    const/4 v2, 0x1

    const-string v3, "UNIT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    .line 954
    sget-object v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->INVALID_GENERIC_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->$VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    .line 964
    new-instance v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic$ProtoAdapter_Generic;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic$ProtoAdapter_Generic;-><init>()V

    sput-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 968
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 969
    iput p3, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 978
    :cond_0
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    return-object p0

    .line 977
    :cond_1
    sget-object p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->INVALID_GENERIC_UNIT:Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;
    .locals 1

    .line 954
    const-class v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;
    .locals 1

    .line 954
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->$VALUES:[Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    invoke-virtual {v0}, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 985
    iget v0, p0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit$Generic;->value:I

    return v0
.end method
