.class public final enum Lcom/squareup/protos/teller/FailureReason;
.super Ljava/lang/Enum;
.source "FailureReason.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/teller/FailureReason$ProtoAdapter_FailureReason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/teller/FailureReason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/teller/FailureReason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/teller/FailureReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BANK_ACCOUNT_BLOCKED:Lcom/squareup/protos/teller/FailureReason;

.field public static final enum CURRENCY_MISMATCHED:Lcom/squareup/protos/teller/FailureReason;

.field public static final enum EXCEEDS_CLIENT_TRANSFER_LIMIT:Lcom/squareup/protos/teller/FailureReason;

.field public static final enum INVALID_BANK_ACCOUNT:Lcom/squareup/protos/teller/FailureReason;

.field public static final enum INVALID_ENTRY:Lcom/squareup/protos/teller/FailureReason;

.field public static final enum INVALID_MECHANISM:Lcom/squareup/protos/teller/FailureReason;

.field public static final enum INVALID_TRANSFER_AMOUNT:Lcom/squareup/protos/teller/FailureReason;

.field public static final enum MANUALLY_FAILED:Lcom/squareup/protos/teller/FailureReason;

.field public static final enum RETURN:Lcom/squareup/protos/teller/FailureReason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 17
    new-instance v0, Lcom/squareup/protos/teller/FailureReason;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "INVALID_BANK_ACCOUNT"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/teller/FailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/teller/FailureReason;->INVALID_BANK_ACCOUNT:Lcom/squareup/protos/teller/FailureReason;

    .line 22
    new-instance v0, Lcom/squareup/protos/teller/FailureReason;

    const/4 v3, 0x2

    const-string v4, "INVALID_TRANSFER_AMOUNT"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/teller/FailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/teller/FailureReason;->INVALID_TRANSFER_AMOUNT:Lcom/squareup/protos/teller/FailureReason;

    .line 27
    new-instance v0, Lcom/squareup/protos/teller/FailureReason;

    const/4 v4, 0x3

    const-string v5, "RETURN"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/teller/FailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/teller/FailureReason;->RETURN:Lcom/squareup/protos/teller/FailureReason;

    .line 32
    new-instance v0, Lcom/squareup/protos/teller/FailureReason;

    const/4 v5, 0x4

    const-string v6, "INVALID_ENTRY"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/teller/FailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/teller/FailureReason;->INVALID_ENTRY:Lcom/squareup/protos/teller/FailureReason;

    .line 37
    new-instance v0, Lcom/squareup/protos/teller/FailureReason;

    const/4 v6, 0x5

    const-string v7, "MANUALLY_FAILED"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/teller/FailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/teller/FailureReason;->MANUALLY_FAILED:Lcom/squareup/protos/teller/FailureReason;

    .line 43
    new-instance v0, Lcom/squareup/protos/teller/FailureReason;

    const/4 v7, 0x6

    const-string v8, "EXCEEDS_CLIENT_TRANSFER_LIMIT"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/teller/FailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/teller/FailureReason;->EXCEEDS_CLIENT_TRANSFER_LIMIT:Lcom/squareup/protos/teller/FailureReason;

    .line 48
    new-instance v0, Lcom/squareup/protos/teller/FailureReason;

    const/4 v8, 0x7

    const-string v9, "BANK_ACCOUNT_BLOCKED"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/teller/FailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/teller/FailureReason;->BANK_ACCOUNT_BLOCKED:Lcom/squareup/protos/teller/FailureReason;

    .line 53
    new-instance v0, Lcom/squareup/protos/teller/FailureReason;

    const/16 v9, 0x8

    const-string v10, "INVALID_MECHANISM"

    invoke-direct {v0, v10, v8, v9}, Lcom/squareup/protos/teller/FailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/teller/FailureReason;->INVALID_MECHANISM:Lcom/squareup/protos/teller/FailureReason;

    .line 58
    new-instance v0, Lcom/squareup/protos/teller/FailureReason;

    const/16 v10, 0x9

    const-string v11, "CURRENCY_MISMATCHED"

    invoke-direct {v0, v11, v9, v10}, Lcom/squareup/protos/teller/FailureReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/teller/FailureReason;->CURRENCY_MISMATCHED:Lcom/squareup/protos/teller/FailureReason;

    new-array v0, v10, [Lcom/squareup/protos/teller/FailureReason;

    .line 13
    sget-object v10, Lcom/squareup/protos/teller/FailureReason;->INVALID_BANK_ACCOUNT:Lcom/squareup/protos/teller/FailureReason;

    aput-object v10, v0, v1

    sget-object v1, Lcom/squareup/protos/teller/FailureReason;->INVALID_TRANSFER_AMOUNT:Lcom/squareup/protos/teller/FailureReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/teller/FailureReason;->RETURN:Lcom/squareup/protos/teller/FailureReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/teller/FailureReason;->INVALID_ENTRY:Lcom/squareup/protos/teller/FailureReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/teller/FailureReason;->MANUALLY_FAILED:Lcom/squareup/protos/teller/FailureReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/teller/FailureReason;->EXCEEDS_CLIENT_TRANSFER_LIMIT:Lcom/squareup/protos/teller/FailureReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/teller/FailureReason;->BANK_ACCOUNT_BLOCKED:Lcom/squareup/protos/teller/FailureReason;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/teller/FailureReason;->INVALID_MECHANISM:Lcom/squareup/protos/teller/FailureReason;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/teller/FailureReason;->CURRENCY_MISMATCHED:Lcom/squareup/protos/teller/FailureReason;

    aput-object v1, v0, v9

    sput-object v0, Lcom/squareup/protos/teller/FailureReason;->$VALUES:[Lcom/squareup/protos/teller/FailureReason;

    .line 60
    new-instance v0, Lcom/squareup/protos/teller/FailureReason$ProtoAdapter_FailureReason;

    invoke-direct {v0}, Lcom/squareup/protos/teller/FailureReason$ProtoAdapter_FailureReason;-><init>()V

    sput-object v0, Lcom/squareup/protos/teller/FailureReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 65
    iput p3, p0, Lcom/squareup/protos/teller/FailureReason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/teller/FailureReason;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 81
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/teller/FailureReason;->CURRENCY_MISMATCHED:Lcom/squareup/protos/teller/FailureReason;

    return-object p0

    .line 80
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/teller/FailureReason;->INVALID_MECHANISM:Lcom/squareup/protos/teller/FailureReason;

    return-object p0

    .line 79
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/teller/FailureReason;->BANK_ACCOUNT_BLOCKED:Lcom/squareup/protos/teller/FailureReason;

    return-object p0

    .line 78
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/teller/FailureReason;->EXCEEDS_CLIENT_TRANSFER_LIMIT:Lcom/squareup/protos/teller/FailureReason;

    return-object p0

    .line 77
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/teller/FailureReason;->MANUALLY_FAILED:Lcom/squareup/protos/teller/FailureReason;

    return-object p0

    .line 76
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/teller/FailureReason;->INVALID_ENTRY:Lcom/squareup/protos/teller/FailureReason;

    return-object p0

    .line 75
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/teller/FailureReason;->RETURN:Lcom/squareup/protos/teller/FailureReason;

    return-object p0

    .line 74
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/teller/FailureReason;->INVALID_TRANSFER_AMOUNT:Lcom/squareup/protos/teller/FailureReason;

    return-object p0

    .line 73
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/teller/FailureReason;->INVALID_BANK_ACCOUNT:Lcom/squareup/protos/teller/FailureReason;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/teller/FailureReason;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/teller/FailureReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/teller/FailureReason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/teller/FailureReason;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/teller/FailureReason;->$VALUES:[Lcom/squareup/protos/teller/FailureReason;

    invoke-virtual {v0}, [Lcom/squareup/protos/teller/FailureReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/teller/FailureReason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 88
    iget v0, p0, Lcom/squareup/protos/teller/FailureReason;->value:I

    return v0
.end method
