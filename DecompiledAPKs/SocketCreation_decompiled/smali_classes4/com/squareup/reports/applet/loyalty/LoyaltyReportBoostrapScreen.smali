.class public final Lcom/squareup/reports/applet/loyalty/LoyaltyReportBoostrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "LoyaltyReportBoostrapScreen.kt"

# interfaces
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\rH\u0016R\u0018\u0010\u0004\u001a\u0006\u0012\u0002\u0008\u00030\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/reports/applet/loyalty/LoyaltyReportBoostrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "Lcom/squareup/container/layer/InSection;",
        "()V",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/reports/applet/loyalty/LoyaltyReportBoostrapScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/reports/applet/loyalty/LoyaltyReportBoostrapScreen;

    invoke-direct {v0}, Lcom/squareup/reports/applet/loyalty/LoyaltyReportBoostrapScreen;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/loyalty/LoyaltyReportBoostrapScreen;->INSTANCE:Lcom/squareup/reports/applet/loyalty/LoyaltyReportBoostrapScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-static {p1}, Lcom/squareup/reports/applet/loyalty/LoyaltyReportBoostrapScreenKt;->access$getLoyaltyReportWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner;

    move-result-object p1

    .line 17
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-interface {p1, v0}, Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner;->startWorkflow(Lkotlin/Unit;)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/reports/applet/loyalty/LoyaltyReportScope;->INSTANCE:Lcom/squareup/reports/applet/loyalty/LoyaltyReportScope;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 11
    const-class v0, Lcom/squareup/reports/applet/loyalty/LoyaltyReportSection;

    return-object v0
.end method
