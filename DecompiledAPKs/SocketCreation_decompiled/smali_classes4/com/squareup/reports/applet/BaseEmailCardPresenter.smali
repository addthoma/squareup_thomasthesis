.class public abstract Lcom/squareup/reports/applet/BaseEmailCardPresenter;
.super Lmortar/ViewPresenter;
.source "BaseEmailCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/reports/applet/BaseEmailCardView;",
        ">;"
    }
.end annotation


# static fields
.field public static final CLOSE_CARD_DELAY_MS:I = 0x3e8


# instance fields
.field private final autoFinisher:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p2    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param

    .line 23
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 24
    new-instance v0, Lcom/squareup/util/RxWatchdog;

    invoke-direct {v0, p1, p2}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->autoFinisher:Lcom/squareup/util/RxWatchdog;

    return-void
.end method


# virtual methods
.method public abstract getActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;
.end method

.method public abstract getDefaultEmailRecipient()Ljava/lang/String;
.end method

.method protected isValidEmail(Ljava/lang/String;)Z
    .locals 0

    if-eqz p1, :cond_0

    .line 55
    invoke-static {p1}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic lambda$null$0$BaseEmailCardPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 36
    invoke-virtual {p0}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->onBackPressed()Z

    return-void
.end method

.method public synthetic lambda$onLoad$1$BaseEmailCardPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->autoFinisher:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/reports/applet/-$$Lambda$BaseEmailCardPresenter$VME6EyERoYVnm9vOlW31Ly3EKqU;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/-$$Lambda$BaseEmailCardPresenter$VME6EyERoYVnm9vOlW31Ly3EKqU;-><init>(Lcom/squareup/reports/applet/BaseEmailCardPresenter;)V

    .line 36
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public abstract onBackPressed()Z
.end method

.method public onEmailAnimationComplete(Z)V
    .locals 0

    if-nez p1, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->scheduleAutoFinish()V

    :cond_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 28
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/BaseEmailCardView;

    .line 32
    invoke-virtual {p0}, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->getActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/reports/applet/BaseEmailCardView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 34
    new-instance v0, Lcom/squareup/reports/applet/-$$Lambda$BaseEmailCardPresenter$V8tILOyfSE76P4dToTkgAMua9zk;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/-$$Lambda$BaseEmailCardPresenter$V8tILOyfSE76P4dToTkgAMua9zk;-><init>(Lcom/squareup/reports/applet/BaseEmailCardPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected scheduleAutoFinish()V
    .locals 5

    .line 59
    iget-object v0, p0, Lcom/squareup/reports/applet/BaseEmailCardPresenter;->autoFinisher:Lcom/squareup/util/RxWatchdog;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x3e8

    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public abstract sendEmail(Ljava/lang/String;)V
.end method
