.class public final Lcom/squareup/reports/applet/RealReportsAppletHeader;
.super Ljava/lang/Object;
.source "ReportsAppletHeader.kt"

# interfaces
.implements Lcom/squareup/reports/applet/ReportsAppletHeader;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/reports/applet/RealReportsAppletHeader;",
        "Lcom/squareup/reports/applet/ReportsAppletHeader;",
        "()V",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHeaderId()I
    .locals 1

    .line 12
    invoke-static {p0}, Lcom/squareup/reports/applet/ReportsAppletHeader$DefaultImpls;->getHeaderId(Lcom/squareup/reports/applet/ReportsAppletHeader;)I

    move-result v0

    return v0
.end method
