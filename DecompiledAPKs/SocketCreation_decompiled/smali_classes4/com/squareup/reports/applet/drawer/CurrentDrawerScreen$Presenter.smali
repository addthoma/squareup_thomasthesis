.class Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "CurrentDrawerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/reports/applet/drawer/CurrentDrawerView;",
        ">;"
    }
.end annotation


# static fields
.field private static final CLOSED_REMOTELY_DIALOG:Lcom/squareup/register/widgets/WarningDialogScreen;


# instance fields
.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

.field private final cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

.field private final cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

.field private final cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

.field private currentShiftSubscription:Lrx/Subscription;

.field final detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

.field private final device:Lcom/squareup/util/Device;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final fileScheduler:Lio/reactivex/Scheduler;

.field private final flow:Lflow/Flow;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final reportsAppletSectionsListPresenter:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

.field private final res:Lcom/squareup/util/Res;

.field private final service:Lcom/squareup/server/cashmanagement/CashManagementService;

.field private final userToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 81
    new-instance v0, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    sget v2, Lcom/squareup/cashmanagement/R$string;->current_drawer_closed_remotely_title:I

    sget v3, Lcom/squareup/cashmanagement/R$string;->current_drawer_closed_remotely_message:I

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/squareup/register/widgets/WarningDialogScreen;

    .line 85
    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "-CLOSED_REMOTELY"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;ZLjava/lang/String;)V

    sput-object v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->CLOSED_REMOTELY_DIALOG:Lcom/squareup/register/widgets/WarningDialogScreen;

    return-void
.end method

.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Ljava/lang/String;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;Lcom/squareup/cashmanagement/CashManagementSettings;Lcom/squareup/server/cashmanagement/CashManagementService;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/text/Formatter;Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 2
    .param p5    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Ljava/lang/String;",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;",
            "Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;",
            "Lcom/squareup/cashmanagement/CashManagementSettings;",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 114
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    move-object v1, p1

    .line 115
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->flow:Lflow/Flow;

    move-object v1, p2

    .line 116
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->res:Lcom/squareup/util/Res;

    move-object v1, p3

    .line 117
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->device:Lcom/squareup/util/Device;

    move-object v1, p4

    .line 118
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->userToken:Ljava/lang/String;

    move-object v1, p5

    .line 119
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->fileScheduler:Lio/reactivex/Scheduler;

    move-object v1, p6

    .line 120
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->mainScheduler:Lio/reactivex/Scheduler;

    move-object v1, p7

    .line 121
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object v1, p8

    .line 122
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p9

    .line 123
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->reportsAppletSectionsListPresenter:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    move-object v1, p10

    .line 124
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    move-object v1, p11

    .line 125
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    move-object v1, p12

    .line 126
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->service:Lcom/squareup/server/cashmanagement/CashManagementService;

    move-object v1, p13

    .line 127
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    move-object/from16 v1, p14

    .line 128
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    move-object/from16 v1, p15

    .line 129
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    move-object/from16 v1, p16

    .line 130
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    move-object/from16 v1, p17

    .line 131
    iput-object v1, v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;)Lcom/squareup/permissions/EmployeeManagement;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;Z)V
    .locals 0

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->endDrawerClickedSuccess(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;)Lflow/Flow;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->flow:Lflow/Flow;

    return-object p0
.end method

.method private configurePaidInOutCount()V
    .locals 5

    .line 313
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->getCurrentCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    .line 315
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    .line 316
    iget-object v3, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_OUT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-eq v3, v4, :cond_1

    iget-object v2, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v3, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-ne v2, v3, :cond_0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 320
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->configurePaidInOutRowWithCount(I)V

    return-void
.end method

.method private endDrawerClickedSuccess(Z)V
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    invoke-interface {v0, p1}, Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;->setHasViewAmountInCashDrawerPermission(Z)V

    .line 237
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;->INSTANCE:Lcom/squareup/reports/applet/drawer/EndCurrentDrawerScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 238
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {p1}, Lcom/squareup/cashdrawer/CashDrawerTracker;->openAllCashDrawers()V

    return-void
.end method

.method private requestOpenShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 3

    .line 189
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    .line 191
    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->showLoadingState()V

    .line 193
    new-instance v1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->userToken:Ljava/lang/String;

    .line 194
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->OPEN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    .line 195
    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->include_state(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    .line 197
    invoke-interface {v2}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->getCurrentCashDrawerShiftId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 196
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->selected_client_cash_drawer_shift_id(Ljava/util/List;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    move-result-object v1

    const/4 v2, 0x0

    .line 198
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->ignore_device_header_for_filter(Ljava/lang/Boolean;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;

    move-result-object v1

    .line 199
    invoke-virtual {v1}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;

    move-result-object v1

    .line 201
    new-instance v2, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$XJXEc6yAHJYFIhQx3ZkJfAl3Yok;

    invoke-direct {v2, p0, v1, p1}, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$XJXEc6yAHJYFIhQx3ZkJfAl3Yok;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    invoke-static {v0, v2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private updateCurrentDrawerScreen()V
    .locals 3

    .line 164
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->isCashManagementEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->showCashManagementDisabledState()V

    return-void

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->cashManagementEnabledAndNeedsLoading()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->enableCashManagement(Z)V

    .line 171
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->resetAndShowStartDrawerState()V

    return-void

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->getCurrentCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v0

    if-nez v0, :cond_2

    .line 177
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->resetAndShowStartDrawerState()V

    return-void

    .line 181
    :cond_2
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->REMOTELY_CLOSE_OPEN_CASH_DRAWERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 182
    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->requestOpenShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    goto :goto_0

    .line 184
    :cond_3
    invoke-virtual {p0, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->resetAndShowInProgressDrawerState(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public dropView(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V
    .locals 1

    .line 306
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->currentShiftSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 309
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->dropView(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V

    return-void
.end method

.method endDrawerClicked()V
    .locals 3

    .line 267
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_REPORTS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 269
    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_EXPECTED_IN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    .line 270
    invoke-static {v1}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/Employee;->hasAnyPermission(Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 271
    :goto_0
    invoke-direct {p0, v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->endDrawerClickedSuccess(Z)V

    goto :goto_1

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter$1;

    invoke-direct {v2, p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter$1;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    :goto_1
    return-void
.end method

.method goToCashManagementSettingsCardScreen()V
    .locals 3

    .line 256
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen;

    sget-object v2, Lcom/squareup/reports/applet/ReportsAppletScope;->INSTANCE:Lcom/squareup/reports/applet/ReportsAppletScope;

    invoke-direct {v1, v2}, Lcom/squareup/reports/applet/drawer/cashmanagementsettings/CashManagementSettingsCardScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$1$CurrentDrawerScreen$Presenter(Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 149
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->updateCurrentDrawerScreen()V

    return-void
.end method

.method public synthetic lambda$null$4$CurrentDrawerScreen$Presenter(Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsResponse;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 204
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->service:Lcom/squareup/server/cashmanagement/CashManagementService;

    invoke-interface {v0, p1}, Lcom/squareup/server/cashmanagement/CashManagementService;->getDrawerHistory(Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;)Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsResponse;

    move-result-object p1

    if-nez p1, :cond_0

    .line 205
    new-instance p1, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsResponse;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsResponse;-><init>(Ljava/util/List;)V

    :cond_0
    return-object p1
.end method

.method public synthetic lambda$null$5$CurrentDrawerScreen$Presenter(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 210
    iget-object p2, p2, Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsResponse;->cash_drawer_shifts:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 212
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->flow:Lflow/Flow;

    sget-object p2, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->CLOSED_REMOTELY_DIALOG:Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 214
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    iget-object p2, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashManagementExtraPermissionsHolder:Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    invoke-interface {p1, p2}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->endCurrentCashDrawerShift(Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;)V

    .line 216
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->reportsAppletSectionsListPresenter:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->refreshIfSidebar()V

    goto :goto_0

    .line 218
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->resetAndShowInProgressDrawerState(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$6$CurrentDrawerScreen$Presenter(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;Ljava/lang/Throwable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 221
    instance-of v0, p2, Lretrofit/RetrofitError;

    if-eqz v0, :cond_1

    .line 222
    move-object v0, p2

    check-cast v0, Lretrofit/RetrofitError;

    invoke-virtual {v0}, Lretrofit/RetrofitError;->getKind()Lretrofit/RetrofitError$Kind;

    move-result-object v0

    .line 223
    sget-object v1, Lretrofit/RetrofitError$Kind;->NETWORK:Lretrofit/RetrofitError$Kind;

    if-eq v0, v1, :cond_0

    sget-object v1, Lretrofit/RetrofitError$Kind;->HTTP:Lretrofit/RetrofitError$Kind;

    if-ne v0, v1, :cond_1

    .line 226
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->resetAndShowInProgressDrawerState(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    return-void

    .line 230
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method

.method public synthetic lambda$onLoad$0$CurrentDrawerScreen$Presenter(Lcom/squareup/reports/applet/drawer/CurrentDrawerView;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    :cond_0
    if-nez p2, :cond_1

    .line 143
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->resetAndShowStartDrawerState()V

    :cond_1
    return-void
.end method

.method public synthetic lambda$onLoad$2$CurrentDrawerScreen$Presenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/cashmanagement/CashManagementSettings;->cashManagementEnabled()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$Jg2oXYeprKcjFXo-tPngvH41rao;

    invoke-direct {v1, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$Jg2oXYeprKcjFXo-tPngvH41rao;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;)V

    .line 149
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$3$CurrentDrawerScreen$Presenter()V
    .locals 2

    .line 156
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public synthetic lambda$requestOpenShift$7$CurrentDrawerScreen$Presenter(Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 202
    new-instance v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$uwCALNCIVFBbq4IHUShYjXRwtcs;

    invoke-direct {v0, p0, p1}, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$uwCALNCIVFBbq4IHUShYjXRwtcs;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;Lcom/squareup/protos/client/cashdrawers/GetCashDrawerShiftsRequest;)V

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->fileScheduler:Lio/reactivex/Scheduler;

    .line 207
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 208
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$y0QOVxlB5qS1pIBnOIXZe7-ZhkM;

    invoke-direct {v0, p0, p2}, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$y0QOVxlB5qS1pIBnOIXZe7-ZhkM;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    new-instance v1, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$VY8sqDvhw-_yxKE792nWZpmUYCo;

    invoke-direct {v1, p0, p2}, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$VY8sqDvhw-_yxKE792nWZpmUYCo;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    .line 209
    invoke-virtual {p1, v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 135
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    .line 136
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    .line 137
    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->currentCashDrawerShiftOrNull()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$Z8ATQdGsjt1okVAaajNnktvIQHY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$Z8ATQdGsjt1okVAaajNnktvIQHY;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;Lcom/squareup/reports/applet/drawer/CurrentDrawerView;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->currentShiftSubscription:Lrx/Subscription;

    .line 147
    new-instance v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$GWcT7y5B3tFRowRHQJIhxw1SAXw;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$GWcT7y5B3tFRowRHQJIhxw1SAXw;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 151
    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    .line 152
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->resetConfig()V

    .line 153
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/reports/applet/R$string;->reports_current_drawer:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    .line 155
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    .line 156
    new-instance v0, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$70cHEwTMUVskSSrNCivre1OI7ME;

    invoke-direct {v0, p0}, Lcom/squareup/reports/applet/drawer/-$$Lambda$CurrentDrawerScreen$Presenter$70cHEwTMUVskSSrNCivre1OI7ME;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 158
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/reports/applet/R$string;->reports_current_drawer:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 159
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->hideUpButton()V

    :goto_0
    return-void
.end method

.method paidInOutClicked()V
    .locals 3

    .line 287
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_REPORTS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    sget-object v0, Lcom/squareup/permissions/Permission;->VIEW_CASH_DRAWER_REPORTS:Lcom/squareup/permissions/Permission;

    goto :goto_0

    .line 290
    :cond_0
    sget-object v0, Lcom/squareup/permissions/Permission;->OPEN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    .line 292
    :goto_0
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v2, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter$2;

    invoke-direct {v2, p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter$2;-><init>(Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;)V

    invoke-virtual {v1, v0, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method resetAndShowInProgressDrawerState(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->setShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    .line 249
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->detailsPresenter:Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CashDrawerDetailsPresenter;->formatForCurrentDrawer()V

    .line 250
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    iget-object p1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->setShiftDescription(Ljava/lang/String;)V

    .line 251
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->showInProgressDrawerState()V

    .line 252
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->configurePaidInOutCount()V

    return-void
.end method

.method resetAndShowStartDrawerState()V
    .locals 3

    .line 242
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashManagementSettings:Lcom/squareup/cashmanagement/CashManagementSettings;

    .line 243
    invoke-virtual {v2}, Lcom/squareup/cashmanagement/CashManagementSettings;->getDefaultStartingCash()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 242
    invoke-virtual {v0, v1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->setStartingCashDefault(Ljava/lang/CharSequence;)V

    .line 244
    invoke-virtual {p0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;

    invoke-virtual {v0}, Lcom/squareup/reports/applet/drawer/CurrentDrawerView;->showStartDrawerState()V

    return-void
.end method

.method saveShiftDescription(Ljava/lang/String;)V
    .locals 1

    .line 300
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-interface {v0}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->getCurrentCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-interface {v0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->updateCurrentCashDrawerShiftDescription(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method startCashDrawerShift(Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-interface {v0, p1}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->createNewOpenCashDrawerShift(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object p1

    .line 261
    invoke-virtual {p0, p1}, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->resetAndShowInProgressDrawerState(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    .line 262
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/CurrentDrawerScreen$Presenter;->reportsAppletSectionsListPresenter:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    invoke-virtual {p1}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->refreshIfSidebar()V

    return-void
.end method
