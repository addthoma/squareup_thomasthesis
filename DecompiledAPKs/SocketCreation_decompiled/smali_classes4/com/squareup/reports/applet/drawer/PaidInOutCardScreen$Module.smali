.class public abstract Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Module;
.super Ljava/lang/Object;
.source "PaidInOutCardScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract providePaidInOutScreenPresenter(Lcom/squareup/reports/applet/drawer/PaidInOutCardScreen$Presenter;)Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
