.class public abstract Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;
.super Lmortar/ViewPresenter;
.source "PaidInOutScreenPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/reports/applet/drawer/PaidInOutView;",
        ">;"
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;

.field private amount:Ljava/lang/String;

.field private cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

.field private cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

.field private currency:Lcom/squareup/protos/common/CurrencyCode;

.field private dateFormatter:Ljava/text/DateFormat;

.field private description:Ljava/lang/String;

.field private events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;"
        }
    .end annotation
.end field

.field private flow:Lflow/Flow;

.field private moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/cashmanagement/CashDrawerShiftManager;Lcom/squareup/money/PriceLocaleHelper;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/cashdrawer/CashDrawerTracker;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManager;",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/cashdrawer/CashDrawerTracker;",
            ")V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const-string v0, ""

    .line 37
    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->amount:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->description:Ljava/lang/String;

    .line 45
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->flow:Lflow/Flow;

    .line 46
    iput-object p2, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->res:Lcom/squareup/util/Res;

    .line 47
    iput-object p3, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    .line 48
    iput-object p4, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 49
    iput-object p5, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->dateFormatter:Ljava/text/DateFormat;

    .line 50
    iput-object p6, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 51
    iput-object p7, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 52
    iput-object p8, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    return-void
.end method

.method private getPaidInOutEventsInOrder()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
            ">;"
        }
    .end annotation

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 71
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    invoke-interface {v1}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->getCurrentCashDrawerShift()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    .line 72
    iget-object v3, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-eq v3, v4, :cond_1

    iget-object v3, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_OUT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-ne v3, v4, :cond_0

    .line 73
    :cond_1
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 78
    :cond_2
    sget-object v1, Lcom/squareup/reports/applet/drawer/-$$Lambda$PaidInOutScreenPresenter$vG2n_hQ7I4yTb-kxcKLZJCNiXXU;->INSTANCE:Lcom/squareup/reports/applet/drawer/-$$Lambda$PaidInOutScreenPresenter$vG2n_hQ7I4yTb-kxcKLZJCNiXXU;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v0
.end method

.method static synthetic lambda$getPaidInOutEventsInOrder$0(Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;)I
    .locals 2

    .line 79
    iget-object p0, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 80
    iget-object p0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object p0, p0, Lcom/squareup/protos/client/ISO8601Date;->date_string:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide p0

    .line 81
    invoke-static {p0, p1, v0, v1}, Ljava/lang/Long;->compare(JJ)I

    move-result p0

    return p0
.end method


# virtual methods
.method addPaidInEvent(Lcom/squareup/protos/common/Money;Ljava/lang/String;)V
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->addPaidInOutEvent(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;)V

    .line 59
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {p1}, Lcom/squareup/cashdrawer/CashDrawerTracker;->openAllCashDrawers()V

    return-void
.end method

.method addPaidOutEvent(Lcom/squareup/protos/common/Money;Ljava/lang/String;)V
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManager;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_OUT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/cashmanagement/CashDrawerShiftManager;->addPaidInOutEvent(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Ljava/lang/String;)V

    .line 64
    iget-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {p1}, Lcom/squareup/cashdrawer/CashDrawerTracker;->openAllCashDrawers()V

    return-void
.end method

.method createAdapter()Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;
    .locals 7

    .line 88
    new-instance v6, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;

    iget-object v2, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v3, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->dateFormatter:Ljava/text/DateFormat;

    iget-object v4, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v5, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->res:Lcom/squareup/util/Res;

    move-object v0, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;-><init>(Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;Lcom/squareup/money/PriceLocaleHelper;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;)V

    iput-object v6, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->adapter:Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;

    .line 90
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->adapter:Lcom/squareup/reports/applet/drawer/PaidInOutRecyclerAdapter;

    return-object v0
.end method

.method public getAmount()Ljava/lang/String;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->amount:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->description:Ljava/lang/String;

    return-object v0
.end method

.method getEvent(I)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->events:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    return-object p1
.end method

.method getPaidInOutTotal()Lcom/squareup/protos/common/Money;
    .locals 5

    .line 102
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->events:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    .line 104
    iget-object v3, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_IN:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-ne v3, v4, :cond_1

    .line 105
    iget-object v2, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    .line 106
    :cond_1
    iget-object v3, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->PAID_OUT:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    if-ne v3, v4, :cond_0

    .line 107
    iget-object v2, v2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->event_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public getSize()I
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->events:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method protected abstract onLoad(Landroid/os/Bundle;)V
.end method

.method public setAmount(Ljava/lang/String;)V
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->amount:Ljava/lang/String;

    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->description:Ljava/lang/String;

    return-void
.end method

.method updateEvents()V
    .locals 1

    .line 114
    invoke-direct {p0}, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->getPaidInOutEventsInOrder()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/reports/applet/drawer/PaidInOutScreenPresenter;->events:Ljava/util/List;

    return-void
.end method
