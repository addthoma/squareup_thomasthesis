.class Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "ReportsAppletSectionsListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->handleSelectionWithPermission(Lcom/squareup/applet/AppletSection;Ljava/util/Set;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

.field final synthetic val$pos:I

.field final synthetic val$selectedSection:Lcom/squareup/applet/AppletSection;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;Lcom/squareup/applet/AppletSection;I)V
    .locals 0

    .line 68
    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$1;->this$0:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    iput-object p2, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$1;->val$selectedSection:Lcom/squareup/applet/AppletSection;

    iput p3, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$1;->val$pos:I

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$1;->val$selectedSection:Lcom/squareup/applet/AppletSection;

    instance-of v0, v0, Lcom/squareup/reports/applet/drawer/DrawerHistorySection;

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$1;->this$0:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    .line 73
    invoke-static {v0}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->access$100(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;)Lcom/squareup/permissions/EmployeeManagement;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$1;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->getEmployeeByToken(Ljava/lang/String;)Lcom/squareup/permissions/Employee;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_EXPECTED_IN_CASH_DRAWER:Lcom/squareup/permissions/Permission;

    .line 76
    invoke-static {v1}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/Employee;->hasAnyPermission(Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 77
    :goto_0
    iget-object v1, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$1;->this$0:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    invoke-static {v1}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->access$200(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;)Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/squareup/cashmanagement/CashManagementExtraPermissionsHolder;->setHasViewAmountInCashDrawerPermission(Z)V

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$1;->this$0:Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;

    iget v1, p0, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter$1;->val$pos:I

    invoke-static {v0, v1}, Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;->access$301(Lcom/squareup/reports/applet/ReportsAppletSectionsListPresenter;I)V

    return-void
.end method
