.class public interface abstract Lcom/squareup/reports/applet/ReportsAppletParentComponent;
.super Ljava/lang/Object;
.source "ReportsAppletParentComponent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/ReportsAppletParentComponent$PhoneComponent;,
        Lcom/squareup/reports/applet/ReportsAppletParentComponent$TabletComponent;,
        Lcom/squareup/reports/applet/ReportsAppletParentComponent$BaseComponent;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001:\u0003\u0006\u0007\u0008J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/reports/applet/ReportsAppletParentComponent;",
        "",
        "phoneComponent",
        "Lcom/squareup/reports/applet/ReportsAppletParentComponent$PhoneComponent;",
        "tabletComponent",
        "Lcom/squareup/reports/applet/ReportsAppletParentComponent$TabletComponent;",
        "BaseComponent",
        "PhoneComponent",
        "TabletComponent",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract phoneComponent()Lcom/squareup/reports/applet/ReportsAppletParentComponent$PhoneComponent;
.end method

.method public abstract tabletComponent()Lcom/squareup/reports/applet/ReportsAppletParentComponent$TabletComponent;
.end method
