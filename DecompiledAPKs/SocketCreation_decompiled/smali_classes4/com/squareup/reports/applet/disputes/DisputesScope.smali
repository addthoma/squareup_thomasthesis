.class public final Lcom/squareup/reports/applet/disputes/DisputesScope;
.super Lcom/squareup/reports/applet/InReportsAppletScope;
.source "DisputesScope.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesScope.kt\ncom/squareup/reports/applet/disputes/DisputesScope\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,22:1\n24#2,4:23\n*E\n*S KotlinDebug\n*F\n+ 1 DisputesScope.kt\ncom/squareup/reports/applet/disputes/DisputesScope\n*L\n20#1,4:23\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/reports/applet/disputes/DisputesScope;",
        "Lcom/squareup/reports/applet/InReportsAppletScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/disputes/DisputesScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/reports/applet/disputes/DisputesScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/reports/applet/disputes/DisputesScope;

    invoke-direct {v0}, Lcom/squareup/reports/applet/disputes/DisputesScope;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/disputes/DisputesScope;->INSTANCE:Lcom/squareup/reports/applet/disputes/DisputesScope;

    .line 23
    new-instance v0, Lcom/squareup/reports/applet/disputes/DisputesScope$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/reports/applet/disputes/DisputesScope$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 26
    sput-object v0, Lcom/squareup/reports/applet/disputes/DisputesScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/reports/applet/InReportsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-super {p0, p1}, Lcom/squareup/reports/applet/InReportsAppletScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 13
    const-class v1, Lcom/squareup/disputes/DisputesWorkflowRunner$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/disputes/DisputesWorkflowRunner$ParentComponent;

    .line 14
    invoke-interface {p1}, Lcom/squareup/disputes/DisputesWorkflowRunner$ParentComponent;->disputesWorkflowRunnerFactory()Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;

    move-result-object p1

    .line 15
    const-class v1, Lcom/squareup/reports/applet/disputes/DisputesSection;

    invoke-virtual {p1, v1}, Lcom/squareup/disputes/DisputesWorkflowRunner$Factory;->create(Ljava/lang/Class;)Lcom/squareup/disputes/DisputesWorkflowRunner;

    move-result-object p1

    const-string v1, "builder"

    .line 16
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/disputes/DisputesWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method
