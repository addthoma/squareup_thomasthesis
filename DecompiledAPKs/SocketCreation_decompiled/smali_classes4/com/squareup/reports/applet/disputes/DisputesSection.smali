.class public final Lcom/squareup/reports/applet/disputes/DisputesSection;
.super Lcom/squareup/applet/AppletSection;
.source "DisputesSection.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/reports/applet/disputes/DisputesSection;",
        "Lcom/squareup/applet/AppletSection;",
        "handlesDisputes",
        "Lcom/squareup/disputes/api/HandlesDisputes;",
        "(Lcom/squareup/disputes/api/HandlesDisputes;)V",
        "getInitialScreen",
        "Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/disputes/api/HandlesDisputes;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "handlesDisputes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    new-instance v0, Lcom/squareup/reports/applet/disputes/DisputesSection$1;

    invoke-direct {v0, p1}, Lcom/squareup/reports/applet/disputes/DisputesSection$1;-><init>(Lcom/squareup/disputes/api/HandlesDisputes;)V

    check-cast v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/reports/applet/disputes/DisputesSection;->getInitialScreen()Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;
    .locals 3

    .line 20
    new-instance v0, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v1}, Lcom/squareup/reports/applet/disputes/DisputesBootstrapScreen;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
