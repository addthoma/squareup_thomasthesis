.class public final Lcom/squareup/reports/applet/ReportsApplet_Factory;
.super Ljava/lang/Object;
.source "ReportsApplet_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/ReportsApplet;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final currentDrawerSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final disputesSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/disputes/DisputesSection;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final handlesDisputesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;"
        }
    .end annotation
.end field

.field private final reportsAppletEntryPointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletEntryPoint;",
            ">;"
        }
    .end annotation
.end field

.field private final reportsAppletHeaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletHeader;",
            ">;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletEntryPoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/disputes/DisputesSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletHeader;",
            ">;)V"
        }
    .end annotation

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p2, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p3, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p4, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->reportsAppletEntryPointProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p5, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->handlesDisputesProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p6, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->disputesSectionProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p7, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->currentDrawerSectionProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p8, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->resourcesProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p9, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->reportsAppletHeaderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/ReportsApplet_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletEntryPoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/disputes/DisputesSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/ReportsAppletHeader;",
            ">;)",
            "Lcom/squareup/reports/applet/ReportsApplet_Factory;"
        }
    .end annotation

    .line 75
    new-instance v10, Lcom/squareup/reports/applet/ReportsApplet_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/reports/applet/ReportsApplet_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/ReportsAppletEntryPoint;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Landroid/content/res/Resources;Lcom/squareup/reports/applet/ReportsAppletHeader;)Lcom/squareup/reports/applet/ReportsApplet;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/reports/applet/ReportsAppletEntryPoint;",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            "Lcom/squareup/reports/applet/disputes/DisputesSection;",
            "Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;",
            "Landroid/content/res/Resources;",
            "Lcom/squareup/reports/applet/ReportsAppletHeader;",
            ")",
            "Lcom/squareup/reports/applet/ReportsApplet;"
        }
    .end annotation

    .line 83
    new-instance v10, Lcom/squareup/reports/applet/ReportsApplet;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/reports/applet/ReportsApplet;-><init>(Ldagger/Lazy;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/ReportsAppletEntryPoint;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Landroid/content/res/Resources;Lcom/squareup/reports/applet/ReportsAppletHeader;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/ReportsApplet;
    .locals 10

    .line 64
    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->reportsAppletEntryPointProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/reports/applet/ReportsAppletEntryPoint;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->handlesDisputesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/disputes/api/HandlesDisputes;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->disputesSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/reports/applet/disputes/DisputesSection;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->currentDrawerSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/squareup/reports/applet/ReportsApplet_Factory;->reportsAppletHeaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/reports/applet/ReportsAppletHeader;

    invoke-static/range {v1 .. v9}, Lcom/squareup/reports/applet/ReportsApplet_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/ReportsAppletEntryPoint;Lcom/squareup/disputes/api/HandlesDisputes;Lcom/squareup/reports/applet/disputes/DisputesSection;Lcom/squareup/reports/applet/drawer/CurrentDrawerSection;Landroid/content/res/Resources;Lcom/squareup/reports/applet/ReportsAppletHeader;)Lcom/squareup/reports/applet/ReportsApplet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/reports/applet/ReportsApplet_Factory;->get()Lcom/squareup/reports/applet/ReportsApplet;

    move-result-object v0

    return-object v0
.end method
