.class public final Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$AllEmployees;
.super Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;
.source "ReportConfigEmployeeFilterCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AllEmployees"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$AllEmployees;",
        "Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;",
        "()V",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$AllEmployees;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 158
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$AllEmployees;

    invoke-direct {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$AllEmployees;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$AllEmployees;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData$AllEmployees;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    .line 158
    invoke-direct/range {v0 .. v6}, Lcom/squareup/reports/applet/sales/v1/ReportConfigEmployeeFilterCoordinator$ScreenData;-><init>(ZZZZZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
