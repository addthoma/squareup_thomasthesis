.class public Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;
.super Ljava/lang/Object;
.source "SalesSummaryRowGenerator.java"


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final formatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->formatter:Lcom/squareup/text/Formatter;

    .line 30
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 31
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->res:Lcom/squareup/util/Res;

    .line 32
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private addCoverCountRowIfNeeded(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;)V"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_COVER_COUNTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 113
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getSubscriptions()Lcom/squareup/settings/server/Subscriptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/Subscriptions;->hasOrHadRestaurantsSubscription()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 115
    sget v0, Lcom/squareup/reports/applet/R$string;->sales_report_cover_count:I

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->cover_count:Ljava/lang/Long;

    .line 116
    invoke-virtual {p1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object p1

    .line 115
    invoke-direct {p0, v0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILjava/lang/String;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private addServiceChargeRowIfNeeded(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;)V"
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_AUTO_GRATUITY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v0, v0, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 124
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 125
    sget v0, Lcom/squareup/reports/applet/R$string;->sales_report_item_gross_sales:I

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v0, v1}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegularGray(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    sget v0, Lcom/squareup/reports/applet/R$string;->sales_report_service_charges:I

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->surcharge_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegularGray(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private hasThirdPartyCardMoney(Lcom/squareup/reports/applet/sales/v1/PaymentMethods;)Z
    .locals 2

    .line 133
    iget-object v0, p1, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->thirdPartyCardMoney:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->thirdPartyCardMoney:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->thirdPartyCardMoney:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-wide/16 v0, 0x0

    .line 135
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private rowMedium(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;
    .locals 2

    .line 144
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->formatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-object v0
.end method

.method private rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->formatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILjava/lang/String;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object p1

    return-object p1
.end method

.method private rowRegular(ILjava/lang/String;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;
    .locals 2

    .line 153
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-object v0
.end method

.method private rowRegularGray(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;
    .locals 4

    .line 158
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->formatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    .line 159
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRow;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/marketfont/MarketFont$Weight;Ljava/lang/Integer;)V

    return-object v0
.end method

.method private showSwedishRounding(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Z
    .locals 1

    .line 139
    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1}, Lcom/squareup/currency_db/Currencies;->getSwedishRoundingInterval(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p1

    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public buildPaymentsRows(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Lcom/squareup/reports/applet/sales/v1/PaymentMethods;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
            "Lcom/squareup/reports/applet/sales/v1/PaymentMethods;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;"
        }
    .end annotation

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 90
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_total_collected:I

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_collected_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v1, v2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowMedium(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_cash:I

    iget-object v2, p2, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->cashMoney:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v1, v2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->ELIGIBLE_FOR_SQUARE_CARD_PROCESSING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_card:I

    iget-object v2, p2, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->cardMoney:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v1, v2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    :cond_0
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_gift_card:I

    iget-object v2, p2, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->giftCardMoney:Lcom/squareup/protos/common/Money;

    .line 97
    invoke-direct {p0, v1, v2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v1

    .line 96
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_other:I

    iget-object v2, p2, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->otherMoney:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v1, v2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    invoke-direct {p0, p2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->hasThirdPartyCardMoney(Lcom/squareup/reports/applet/sales/v1/PaymentMethods;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 100
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_third_party_card:I

    iget-object p2, p2, Lcom/squareup/reports/applet/sales/v1/PaymentMethods;->thirdPartyCardMoney:Lcom/squareup/protos/common/Money;

    .line 101
    invoke-direct {p0, v1, p2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object p2

    .line 100
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_1
    sget p2, Lcom/squareup/reports/applet/R$string;->sales_report_fees:I

    iget-object v1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v1, v1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->processing_fee_money:Lcom/squareup/protos/common/Money;

    .line 104
    invoke-direct {p0, p2, v1}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object p2

    .line 103
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    sget p2, Lcom/squareup/transferreports/R$string;->reports_net_total:I

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_total_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, p2, p1}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowMedium(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public buildSalesRows(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportRow;",
            ">;"
        }
    .end annotation

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 38
    invoke-direct {p0, p1, v0}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->addCoverCountRowIfNeeded(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Ljava/util/List;)V

    .line 39
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_ALTERNATE_SALES_SUMMARY:Lcom/squareup/settings/server/Features$Feature;

    .line 40
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    sget v2, Lcom/squareup/reports/applet/R$string;->sales_report_product_sales:I

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->product_sales_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v2, v3}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowMedium(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    sget v2, Lcom/squareup/reports/applet/R$string;->sales_report_returns:I

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->product_sales_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v2, v3}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 47
    :cond_0
    sget v2, Lcom/squareup/reports/applet/R$string;->sales_report_gross_sales:I

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->sales:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v2, v3}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowMedium(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    invoke-direct {p0, p1, v0}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->addServiceChargeRowIfNeeded(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;Ljava/util/List;)V

    .line 50
    sget v2, Lcom/squareup/reports/applet/R$string;->sales_report_returns:I

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->refunds:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v2, v3}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    :goto_0
    sget v2, Lcom/squareup/reports/applet/R$string;->sales_report_discounts:I

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->discount_money:Lcom/squareup/protos/common/Money;

    .line 55
    invoke-direct {p0, v2, v3}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v2

    .line 54
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    sget v2, Lcom/squareup/reports/applet/R$string;->sales_report_net_sales:I

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->net_sales_money:Lcom/squareup/protos/common/Money;

    .line 57
    invoke-direct {p0, v2, v3}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowMedium(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v2

    .line 56
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    sget v2, Lcom/squareup/reports/applet/R$string;->sales_report_tax:I

    iget-object v3, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v3, v3, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tax_money:Lcom/squareup/protos/common/Money;

    .line 59
    invoke-direct {p0, v2, v3}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v2

    .line 58
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v1, :cond_1

    .line 62
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_gross_sales:I

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gross_sales_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v1, v2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowMedium(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    :cond_1
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->HIDE_TIPS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 67
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_tip:I

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->tip_money:Lcom/squareup/protos/common/Money;

    .line 68
    invoke-direct {p0, v1, v2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v1

    .line 67
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_2
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/GiftCardSettings;->canUseGiftCards()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 72
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_gift_card_sales:I

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->gift_card_net_sales_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v1, v2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    :cond_3
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_partial_refund:I

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->credit_money:Lcom/squareup/protos/common/Money;

    .line 77
    invoke-direct {p0, v1, v2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v1

    .line 76
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->showSwedishRounding(Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 79
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_cash_rounding:I

    iget-object v2, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object v2, v2, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->swedish_rounding_money:Lcom/squareup/protos/common/Money;

    invoke-direct {p0, v1, v2}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowRegular(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    :cond_4
    sget v1, Lcom/squareup/reports/applet/R$string;->sales_report_total:I

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate;->net:Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;

    iget-object p1, p1, Lcom/squareup/protos/beemo/api/v3/reporting/Aggregate$Details;->total_sales_money:Lcom/squareup/protos/common/Money;

    .line 83
    invoke-direct {p0, v1, p1}, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;->rowMedium(ILcom/squareup/protos/common/Money;)Lcom/squareup/reports/applet/sales/v1/SalesReportRow;

    move-result-object p1

    .line 82
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method
