.class public final Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;
.super Ljava/lang/Object;
.source "CustomReportEndpoint_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;",
        ">;"
    }
.end annotation


# instance fields
.field private final customReportServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/service/CustomReportService;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final merchantTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final nameOrTranslationTypeFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentMethodProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final salesSummaryRowGeneratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/service/CustomReportService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->customReportServiceProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->merchantTokenProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->deviceIdProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->salesSummaryRowGeneratorProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->nameOrTranslationTypeFormatterProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->paymentMethodProcessorProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p9, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/service/CustomReportService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/DeviceIdProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;"
        }
    .end annotation

    .line 70
    new-instance v10, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/customreport/data/service/CustomReportService;Ljava/lang/String;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;Lcom/squareup/util/Res;)Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/service/CustomReportService;",
            "Ljava/lang/String;",
            "Lcom/squareup/settings/DeviceIdProvider;",
            "Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;",
            "Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;"
        }
    .end annotation

    .line 79
    new-instance v10, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;-><init>(Lcom/squareup/customreport/data/service/CustomReportService;Ljava/lang/String;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;Lcom/squareup/util/Res;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;
    .locals 10

    .line 60
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->customReportServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/customreport/data/service/CustomReportService;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->merchantTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->deviceIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/DeviceIdProvider;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->salesSummaryRowGeneratorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->nameOrTranslationTypeFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->paymentMethodProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/util/Res;

    invoke-static/range {v1 .. v9}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->newInstance(Lcom/squareup/customreport/data/service/CustomReportService;Ljava/lang/String;Lcom/squareup/settings/DeviceIdProvider;Lcom/squareup/reports/applet/sales/v1/SalesSummaryRowGenerator;Lcom/squareup/customreport/data/util/NameOrTranslationTypeFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;Lcom/squareup/reports/applet/sales/v1/PaymentMethodProcessor;Lcom/squareup/util/Res;)Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint_Factory;->get()Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    move-result-object v0

    return-object v0
.end method
