.class public final Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;
.super Lcom/squareup/reports/applet/sales/v1/InReportConfigScope;
.source "ReportConfigScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Component;,
        Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;

    invoke-direct {v0}, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;-><init>()V

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;

    .line 179
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;->INSTANCE:Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/ReportConfigScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/reports/applet/sales/v1/InReportConfigScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 182
    sget v0, Lcom/squareup/reports/applet/R$layout;->report_config_card_view:I

    return v0
.end method
