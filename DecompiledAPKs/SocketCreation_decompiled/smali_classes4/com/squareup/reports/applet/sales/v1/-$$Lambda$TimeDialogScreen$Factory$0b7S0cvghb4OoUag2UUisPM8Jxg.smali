.class public final synthetic Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final synthetic f$0:Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;

.field private final synthetic f$1:Landroid/widget/TimePicker;

.field private final synthetic f$2:Landroid/content/Context;

.field private final synthetic f$3:Landroid/view/View;

.field private final synthetic f$4:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;Landroid/widget/TimePicker;Landroid/content/Context;Landroid/view/View;Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;->f$0:Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;->f$1:Landroid/widget/TimePicker;

    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;->f$2:Landroid/content/Context;

    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;->f$3:Landroid/view/View;

    iput-object p5, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;->f$4:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;->f$0:Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;->f$1:Landroid/widget/TimePicker;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;->f$2:Landroid/content/Context;

    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;->f$3:Landroid/view/View;

    iget-object v4, p0, Lcom/squareup/reports/applet/sales/v1/-$$Lambda$TimeDialogScreen$Factory$0b7S0cvghb4OoUag2UUisPM8Jxg;->f$4:Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;

    move-object v5, p1

    check-cast v5, Lcom/squareup/reports/applet/sales/v1/ReportConfig;

    invoke-static/range {v0 .. v5}, Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen$Factory;->lambda$create$1(Lcom/squareup/reports/applet/sales/v1/TimeDialogScreen;Landroid/widget/TimePicker;Landroid/content/Context;Landroid/view/View;Lcom/squareup/reports/applet/sales/v1/ReportConfigRunner;Lcom/squareup/reports/applet/sales/v1/ReportConfig;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method
