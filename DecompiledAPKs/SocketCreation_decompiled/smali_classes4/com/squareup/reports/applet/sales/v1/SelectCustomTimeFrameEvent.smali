.class public final Lcom/squareup/reports/applet/sales/v1/SelectCustomTimeFrameEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "SelectCustomTimeFrameEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/v1/SelectCustomTimeFrameEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "date_start",
        "",
        "date_end",
        "(Ljava/lang/String;Ljava/lang/String;)V",
        "getDate_end",
        "()Ljava/lang/String;",
        "getDate_start",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final date_end:Ljava/lang/String;

.field private final date_start:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string v0, "date_start"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "date_end"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Reports: Changed Time Frame"

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SelectCustomTimeFrameEvent;->date_start:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SelectCustomTimeFrameEvent;->date_end:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getDate_end()Ljava/lang/String;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SelectCustomTimeFrameEvent;->date_end:Ljava/lang/String;

    return-object v0
.end method

.method public final getDate_start()Ljava/lang/String;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SelectCustomTimeFrameEvent;->date_start:Ljava/lang/String;

    return-object v0
.end method
