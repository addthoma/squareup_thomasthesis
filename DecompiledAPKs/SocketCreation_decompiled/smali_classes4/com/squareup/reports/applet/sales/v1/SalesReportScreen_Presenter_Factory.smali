.class public final Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "SalesReportScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final customReportEndpointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final eventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final formatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final printerDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final reportConfigSubjectProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final salesReportPayloadFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final shortMoneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;",
            ">;)V"
        }
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p5, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p6, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->customReportEndpointProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p7, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->printerDispatcherProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p8, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->reportConfigSubjectProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p9, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p10, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p11, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p12, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p13, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p14, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->salesReportPayloadFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;",
            ">;)",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;"
        }
    .end annotation

    .line 98
    new-instance v15, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;Lrx/subjects/BehaviorSubject;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;",
            "Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;",
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/reports/applet/sales/v1/ReportConfig;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;",
            ")",
            "Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;"
        }
    .end annotation

    .line 107
    new-instance v15, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;-><init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;Lrx/subjects/BehaviorSubject;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;)V

    return-object v15
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;
    .locals 15

    .line 85
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->formatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->eventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/badbus/BadEventSink;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->customReportEndpointProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->printerDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->reportConfigSubjectProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lrx/subjects/BehaviorSubject;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->currencyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->shortMoneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->salesReportPayloadFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;

    invoke-static/range {v1 .. v14}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/text/Formatter;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/reports/applet/sales/v1/CustomReportEndpoint;Lcom/squareup/salesreport/print/SalesReportPrintingDispatcher;Lrx/subjects/BehaviorSubject;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/reports/applet/sales/v1/print/SalesReportPayloadFactory;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen_Presenter_Factory;->get()Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
