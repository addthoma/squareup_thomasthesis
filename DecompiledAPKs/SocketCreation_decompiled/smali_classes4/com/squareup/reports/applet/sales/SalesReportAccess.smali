.class public final Lcom/squareup/reports/applet/sales/SalesReportAccess;
.super Lcom/squareup/applet/SectionAccess;
.source "SalesReportAccess.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSalesReportAccess.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SalesReportAccess.kt\ncom/squareup/reports/applet/sales/SalesReportAccess\n*L\n1#1,28:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/reports/applet/sales/SalesReportAccess;",
        "Lcom/squareup/applet/SectionAccess;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/Features;)V",
        "getPermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "reports-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 1

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/SalesReportAccess;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public getPermissions()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 14
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v0, Ljava/util/Set;

    .line 15
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/SalesReportAccess;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_REPORTS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 16
    sget-object v1, Lcom/squareup/permissions/Permission;->VIEW_SALES_REPORTS:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 18
    :cond_0
    sget-object v1, Lcom/squareup/permissions/Permission;->SUMMARIES_AND_REPORTS:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 21
    :goto_0
    iget-object v1, p0, Lcom/squareup/reports/applet/sales/SalesReportAccess;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_VIEW_DETAILED_SALES_REPORTS_PERMISSION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 22
    sget-object v1, Lcom/squareup/permissions/Permission;->DETAILED_SALES_REPORTS:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    return-object v0
.end method
