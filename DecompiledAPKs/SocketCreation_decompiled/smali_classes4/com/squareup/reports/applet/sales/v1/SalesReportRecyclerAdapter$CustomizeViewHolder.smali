.class Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CustomizeViewHolder;
.super Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;
.source "SalesReportRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomizeViewHolder"
.end annotation


# instance fields
.field private final salesReportOverviewDateRange:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;Landroid/view/View;)V
    .locals 0

    .line 360
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CustomizeViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    .line 361
    invoke-direct {p0, p2}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 362
    sget p1, Lcom/squareup/reports/applet/R$id;->sales_report_customize_date_range:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CustomizeViewHolder;->salesReportOverviewDateRange:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public bind()V
    .locals 2

    .line 366
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CustomizeViewHolder;->salesReportOverviewDateRange:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter$CustomizeViewHolder;->this$0:Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;->access$000(Lcom/squareup/reports/applet/sales/v1/SalesReportRecyclerAdapter;)Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$Presenter;->getFormattedDateRange()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
