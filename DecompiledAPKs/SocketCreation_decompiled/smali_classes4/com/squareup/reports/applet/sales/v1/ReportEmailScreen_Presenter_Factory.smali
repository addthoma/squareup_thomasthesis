.class public final Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "ReportEmailScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final retrofitQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final salesSummaryEmailSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->salesSummaryEmailSettingProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p5, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p6, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->retrofitQueueProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p7, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p8, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p9, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)",
            "Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;"
        }
    .end annotation

    .line 71
    new-instance v10, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/analytics/Analytics;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            "Lcom/squareup/analytics/Analytics;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")",
            "Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;"
        }
    .end annotation

    .line 78
    new-instance v10, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;-><init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/analytics/Analytics;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;
    .locals 10

    .line 62
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->salesSummaryEmailSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/LocalSetting;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->retrofitQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-static/range {v1 .. v9}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->newInstance(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/analytics/Analytics;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen_Presenter_Factory;->get()Lcom/squareup/reports/applet/sales/v1/ReportEmailScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
