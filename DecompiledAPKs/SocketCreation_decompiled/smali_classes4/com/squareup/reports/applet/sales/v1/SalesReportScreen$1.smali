.class synthetic Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$1;
.super Ljava/lang/Object;
.source "SalesReportScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/reports/applet/sales/v1/SalesReportScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$reports$applet$sales$v1$LoadingReportFailedEvent$ErrorType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 303
    invoke-static {}, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->values()[Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$1;->$SwitchMap$com$squareup$reports$applet$sales$v1$LoadingReportFailedEvent$ErrorType:[I

    :try_start_0
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$1;->$SwitchMap$com$squareup$reports$applet$sales$v1$LoadingReportFailedEvent$ErrorType:[I

    sget-object v1, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->CLIENT_ERROR:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    invoke-virtual {v1}, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/reports/applet/sales/v1/SalesReportScreen$1;->$SwitchMap$com$squareup$reports$applet$sales$v1$LoadingReportFailedEvent$ErrorType:[I

    sget-object v1, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->SESSION_EXPIRED:Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;

    invoke-virtual {v1}, Lcom/squareup/reports/applet/sales/v1/LoadingReportFailedEvent$ErrorType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method
