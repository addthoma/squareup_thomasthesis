.class public final Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "RealSalesReportWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final feeTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/SalesReportViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/SalesReportWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/SalesReportViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/SalesReportWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;->feeTutorialProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/SalesReportViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/salesreport/SalesReportWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;)",
            "Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/salesreport/SalesReportViewFactory;Lcom/squareup/salesreport/SalesReportWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/feetutorial/FeeTutorial;)Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;
    .locals 1

    .line 51
    new-instance v0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;-><init>(Lcom/squareup/salesreport/SalesReportViewFactory;Lcom/squareup/salesreport/SalesReportWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/feetutorial/FeeTutorial;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/salesreport/SalesReportViewFactory;

    iget-object v1, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/salesreport/SalesReportWorkflow;

    iget-object v2, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/PosContainer;

    iget-object v3, p0, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;->feeTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/feetutorial/FeeTutorial;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;->newInstance(Lcom/squareup/salesreport/SalesReportViewFactory;Lcom/squareup/salesreport/SalesReportWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/feetutorial/FeeTutorial;)Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner_Factory;->get()Lcom/squareup/reports/applet/sales/v2/RealSalesReportWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
