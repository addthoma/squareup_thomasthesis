.class public final Lcom/squareup/posapp/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/posapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abbreviation_label:I = 0x7f120000

.field public static final abc_action_bar_home_description:I = 0x7f120001

.field public static final abc_action_bar_up_description:I = 0x7f120002

.field public static final abc_action_menu_overflow_description:I = 0x7f120003

.field public static final abc_action_mode_done:I = 0x7f120004

.field public static final abc_activity_chooser_view_see_all:I = 0x7f120005

.field public static final abc_activitychooserview_choose_application:I = 0x7f120006

.field public static final abc_capital_off:I = 0x7f120007

.field public static final abc_capital_on:I = 0x7f120008

.field public static final abc_menu_alt_shortcut_label:I = 0x7f120009

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f12000a

.field public static final abc_menu_delete_shortcut_label:I = 0x7f12000b

.field public static final abc_menu_enter_shortcut_label:I = 0x7f12000c

.field public static final abc_menu_function_shortcut_label:I = 0x7f12000d

.field public static final abc_menu_meta_shortcut_label:I = 0x7f12000e

.field public static final abc_menu_shift_shortcut_label:I = 0x7f12000f

.field public static final abc_menu_space_shortcut_label:I = 0x7f120010

.field public static final abc_menu_sym_shortcut_label:I = 0x7f120011

.field public static final abc_prepend_shortcut_label:I = 0x7f120012

.field public static final abc_search_hint:I = 0x7f120013

.field public static final abc_searchview_description_clear:I = 0x7f120014

.field public static final abc_searchview_description_query:I = 0x7f120015

.field public static final abc_searchview_description_search:I = 0x7f120016

.field public static final abc_searchview_description_submit:I = 0x7f120017

.field public static final abc_searchview_description_voice:I = 0x7f120018

.field public static final abc_shareactionprovider_share_with:I = 0x7f120019

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f12001a

.field public static final abc_toolbar_collapse_description:I = 0x7f12001b

.field public static final about:I = 0x7f12001c

.field public static final about_device_settings_device_storage:I = 0x7f12001d

.field public static final about_device_settings_location_services:I = 0x7f12001e

.field public static final about_device_settings_phone_access:I = 0x7f12001f

.field public static final about_help:I = 0x7f120020

.field public static final accept_cash:I = 0x7f120021

.field public static final accept_credit_cards:I = 0x7f120022

.field public static final accept_credit_cards_offline:I = 0x7f120023

.field public static final accept_credit_subtitle_no_free_reader:I = 0x7f120024

.field public static final accept_credit_subtitle_non_payment:I = 0x7f120025

.field public static final accept_documents_legal_text:I = 0x7f120026

.field public static final accept_documents_prompt_message:I = 0x7f120027

.field public static final accept_documents_prompt_title:I = 0x7f120028

.field public static final accept_donations:I = 0x7f120029

.field public static final access_denied:I = 0x7f12002a

.field public static final access_denied_message:I = 0x7f12002b

.field public static final accessibility_button_label:I = 0x7f12002c

.field public static final accessibility_scrubber_default_content_description:I = 0x7f12002d

.field public static final accessible_keypad_clear_digits:I = 0x7f12002e

.field public static final accessible_keypad_error_too_few_digits:I = 0x7f12002f

.field public static final accessible_keypad_error_too_many_digits:I = 0x7f120030

.field public static final accessible_keypad_first_try_instructions:I = 0x7f120031

.field public static final accessible_keypad_first_try_title:I = 0x7f120032

.field public static final accessible_keypad_last_try_title:I = 0x7f120033

.field public static final accessible_keypad_multiple_digits_entered:I = 0x7f120034

.field public static final accessible_keypad_retry_title:I = 0x7f120035

.field public static final accessible_keypad_single_digit_entered:I = 0x7f120036

.field public static final account_freeze_applet_alert:I = 0x7f120037

.field public static final account_holder:I = 0x7f120038

.field public static final account_password:I = 0x7f120039

.field public static final account_status_error_message:I = 0x7f12003a

.field public static final account_status_error_title:I = 0x7f12003b

.field public static final account_tab:I = 0x7f12003c

.field public static final account_type:I = 0x7f12003d

.field public static final accounting_money_format_negative:I = 0x7f12003e

.field public static final activate:I = 0x7f12003f

.field public static final activate_account:I = 0x7f120040

.field public static final activate_confirmation_dialog_body:I = 0x7f120041

.field public static final activate_confirmation_dialog_title:I = 0x7f120042

.field public static final activate_link:I = 0x7f120043

.field public static final activation_account_title:I = 0x7f120044

.field public static final activation_call_to_action_no_free_reader:I = 0x7f120045

.field public static final activation_declined_explanation:I = 0x7f120046

.field public static final activation_declined_explanation_contact_support:I = 0x7f120047

.field public static final activation_declined_title:I = 0x7f120048

.field public static final activation_legal_dialog_title:I = 0x7f120049

.field public static final activation_legal_documents_ca:I = 0x7f12004a

.field public static final activation_legal_documents_helper_prompt_ca:I = 0x7f12004b

.field public static final activation_legal_documents_helper_prompt_jp:I = 0x7f12004c

.field public static final activation_legal_documents_helper_prompt_other:I = 0x7f12004d

.field public static final activation_legal_documents_helper_prompt_us:I = 0x7f12004e

.field public static final activation_legal_documents_jp:I = 0x7f12004f

.field public static final activation_legal_documents_other:I = 0x7f120050

.field public static final activation_legal_documents_us:I = 0x7f120051

.field public static final active_bank_account_uppercase:I = 0x7f120052

.field public static final active_disputes_badge:I = 0x7f120053

.field public static final activity_applet_custom_tip:I = 0x7f120054

.field public static final activity_applet_load_all_activity:I = 0x7f120055

.field public static final activity_applet_network_error_message:I = 0x7f120056

.field public static final activity_applet_network_error_title:I = 0x7f120057

.field public static final activity_applet_no_payments_message:I = 0x7f120058

.field public static final activity_applet_no_payments_title:I = 0x7f120059

.field public static final activity_applet_no_previous_transactions_message:I = 0x7f12005a

.field public static final activity_applet_no_previous_transactions_title:I = 0x7f12005b

.field public static final activity_applet_search_dip:I = 0x7f12005c

.field public static final activity_applet_search_dip_tap:I = 0x7f12005d

.field public static final activity_applet_search_no_payments_message:I = 0x7f12005e

.field public static final activity_applet_search_no_payments_title:I = 0x7f12005f

.field public static final activity_applet_search_no_payments_title_deprecated:I = 0x7f120060

.field public static final activity_applet_search_none:I = 0x7f120061

.field public static final activity_applet_search_swipe:I = 0x7f120062

.field public static final activity_applet_search_swipe_dip:I = 0x7f120063

.field public static final activity_applet_search_swipe_dip_tap:I = 0x7f120064

.field public static final activity_applet_search_with_card_reader:I = 0x7f120065

.field public static final activity_applet_search_without_card_reader:I = 0x7f120066

.field public static final activity_applet_search_x2_swipe_dip:I = 0x7f120067

.field public static final activity_applet_search_x2_swipe_dip_tap:I = 0x7f120068

.field public static final activity_applet_tap_disabled:I = 0x7f120069

.field public static final activity_applet_tap_enabled:I = 0x7f12006a

.field public static final activity_search_request_swipe:I = 0x7f12006b

.field public static final add:I = 0x7f12006c

.field public static final add_additional_recipient_v2:I = 0x7f12006d

.field public static final add_additional_recipient_v2_plural:I = 0x7f12006e

.field public static final add_additional_recipient_v2_single:I = 0x7f12006f

.field public static final add_additional_recipients:I = 0x7f120070

.field public static final add_amount_add_funding_source:I = 0x7f120071

.field public static final add_amount_button_text:I = 0x7f120072

.field public static final add_amount_help_text:I = 0x7f120073

.field public static final add_amount_help_text_deposit_settings:I = 0x7f120074

.field public static final add_amount_source:I = 0x7f120075

.field public static final add_amount_success_message:I = 0x7f120076

.field public static final add_amount_title:I = 0x7f120077

.field public static final add_another_payment:I = 0x7f120078

.field public static final add_attachment:I = 0x7f120079

.field public static final add_bank_account:I = 0x7f12007a

.field public static final add_bank_account_message_title:I = 0x7f12007b

.field public static final add_card:I = 0x7f12007c

.field public static final add_card_on_file:I = 0x7f12007d

.field public static final add_customer:I = 0x7f12007e

.field public static final add_gift_card_or_swipe:I = 0x7f12007f

.field public static final add_item:I = 0x7f120080

.field public static final add_item_note:I = 0x7f120081

.field public static final add_item_price:I = 0x7f120082

.field public static final add_line_item:I = 0x7f120083

.field public static final add_money:I = 0x7f120084

.field public static final add_money_button_label:I = 0x7f120085

.field public static final add_money_debit_card_required_message:I = 0x7f120086

.field public static final add_money_debit_card_required_primary_button:I = 0x7f120087

.field public static final add_money_debit_card_required_title:I = 0x7f120088

.field public static final add_money_hint:I = 0x7f120089

.field public static final add_money_loading_message:I = 0x7f12008a

.field public static final add_money_pending_card_message:I = 0x7f12008b

.field public static final add_money_pending_card_primary_button:I = 0x7f12008c

.field public static final add_money_pending_card_title:I = 0x7f12008d

.field public static final add_note:I = 0x7f12008e

.field public static final add_other:I = 0x7f12008f

.field public static final add_other_type:I = 0x7f120090

.field public static final add_payment_schedule:I = 0x7f120091

.field public static final add_photo:I = 0x7f120092

.field public static final add_processing_time:I = 0x7f120093

.field public static final add_recipients_title:I = 0x7f120094

.field public static final add_tip:I = 0x7f120095

.field public static final add_tip_button:I = 0x7f120096

.field public static final add_to_google_pay:I = 0x7f120097

.field public static final add_to_google_pay_cancelled_message:I = 0x7f120098

.field public static final add_to_google_pay_cancelled_title:I = 0x7f120099

.field public static final add_to_google_pay_error_message:I = 0x7f12009a

.field public static final add_to_google_pay_error_title:I = 0x7f12009b

.field public static final add_to_google_pay_success_message:I = 0x7f12009c

.field public static final add_to_google_pay_success_title:I = 0x7f12009d

.field public static final add_variation:I = 0x7f12009e

.field public static final additional:I = 0x7f12009f

.field public static final additional_email_to_cc:I = 0x7f1200a0

.field public static final additional_emails:I = 0x7f1200a1

.field public static final additional_info_subtitle:I = 0x7f1200a2

.field public static final additional_info_subtitle_vague:I = 0x7f1200a3

.field public static final additional_recipients:I = 0x7f1200a4

.field public static final additional_recipients_helper:I = 0x7f1200a5

.field public static final additional_tip:I = 0x7f1200a6

.field public static final address:I = 0x7f1200a7

.field public static final adjust_inventory_saving:I = 0x7f1200a8

.field public static final adjust_stock:I = 0x7f1200a9

.field public static final adjust_stock_error_message_add_stock:I = 0x7f1200aa

.field public static final adjust_stock_error_message_recount_stock:I = 0x7f1200ab

.field public static final adjust_stock_error_message_remove_stock:I = 0x7f1200ac

.field public static final adjust_stock_error_title_add_stock:I = 0x7f1200ad

.field public static final adjust_stock_error_title_recount_stock:I = 0x7f1200ae

.field public static final adjust_stock_error_title_remove_stock:I = 0x7f1200af

.field public static final adjust_stock_help_text:I = 0x7f1200b0

.field public static final adjust_stock_reason_damage:I = 0x7f1200b1

.field public static final adjust_stock_reason_loss:I = 0x7f1200b2

.field public static final adjust_stock_reason_receive:I = 0x7f1200b3

.field public static final adjust_stock_reason_recount:I = 0x7f1200b4

.field public static final adjust_stock_reason_return:I = 0x7f1200b5

.field public static final adjust_stock_reason_theft:I = 0x7f1200b6

.field public static final adjust_stock_select_reason_uppercase:I = 0x7f1200b7

.field public static final adjust_stock_specify_number_add_stock:I = 0x7f1200b8

.field public static final adjust_stock_specify_number_help_text_new_stock:I = 0x7f1200b9

.field public static final adjust_stock_specify_number_help_text_previous_stock:I = 0x7f1200ba

.field public static final adjust_stock_specify_number_in_stock:I = 0x7f1200bb

.field public static final adjust_stock_specify_number_remove_stock:I = 0x7f1200bc

.field public static final adjust_stock_specify_number_unit_cost:I = 0x7f1200bd

.field public static final after_number:I = 0x7f1200be

.field public static final after_one_invoice:I = 0x7f1200bf

.field public static final after_plural_invoices:I = 0x7f1200c0

.field public static final alert_later:I = 0x7f1200c1

.field public static final alert_text:I = 0x7f1200c2

.field public static final alert_title:I = 0x7f1200c3

.field public static final alert_verify:I = 0x7f1200c4

.field public static final all_items:I = 0x7f1200c5

.field public static final allow_automatic_payments_label:I = 0x7f1200c6

.field public static final allow_offline_mode:I = 0x7f1200c7

.field public static final allow_offline_mode_short:I = 0x7f1200c8

.field public static final am:I = 0x7f1200c9

.field public static final amount:I = 0x7f1200ca

.field public static final amount_charged:I = 0x7f1200cb

.field public static final announcements:I = 0x7f1200cc

.field public static final any_amount_over:I = 0x7f1200cd

.field public static final apartment_suite_hint:I = 0x7f1200ce

.field public static final apartment_unit_hint:I = 0x7f1200cf

.field public static final app_name:I = 0x7f1200d0

.field public static final app_name_displayed_in_app:I = 0x7f1200d1

.field public static final appbar_scrolling_view_behavior:I = 0x7f1200d2

.field public static final application_title:I = 0x7f1200d3

.field public static final applied_reward_to_cart:I = 0x7f1200d4

.field public static final apply:I = 0x7f1200d5

.field public static final apply_items:I = 0x7f1200d6

.field public static final apply_items_exact_many:I = 0x7f1200d7

.field public static final apply_items_exact_one:I = 0x7f1200d8

.field public static final apply_items_range:I = 0x7f1200d9

.field public static final apply_modifier_set:I = 0x7f1200da

.field public static final apply_reward_to_cart:I = 0x7f1200db

.field public static final approved:I = 0x7f1200dc

.field public static final appsflyer_dev_key:I = 0x7f1200dd

.field public static final archive_invoice:I = 0x7f1200de

.field public static final are_you_sure:I = 0x7f1200df

.field public static final ask_community:I = 0x7f1200e0

.field public static final ask_community_subtext:I = 0x7f1200e1

.field public static final assign_option_to_item_fetch_all_variations_failed_dialog_message:I = 0x7f1200e2

.field public static final assign_option_to_item_fetch_all_variations_failed_dialog_title:I = 0x7f1200e3

.field public static final assign_option_to_item_fetch_all_variations_spinner_label:I = 0x7f1200e4

.field public static final assign_option_to_item_select_options_action_bar_primary_button_text:I = 0x7f1200e5

.field public static final assign_option_to_item_select_options_action_bar_title:I = 0x7f1200e6

.field public static final assign_option_to_item_select_options_available_options_section_header:I = 0x7f1200e7

.field public static final assign_option_to_item_select_options_create_option_button_text:I = 0x7f1200e8

.field public static final assign_option_to_item_select_options_search_bar_hint:I = 0x7f1200e9

.field public static final assign_stock_error_message:I = 0x7f1200ea

.field public static final assign_stock_error_title:I = 0x7f1200eb

.field public static final auth_square_card_idv_cancel_leave:I = 0x7f1200ec

.field public static final auth_square_card_idv_cancel_message:I = 0x7f1200ed

.field public static final auth_square_card_idv_cancel_title:I = 0x7f1200ee

.field public static final auth_square_card_idv_cancel_try_again:I = 0x7f1200ef

.field public static final auth_square_card_idv_error_message:I = 0x7f1200f0

.field public static final auth_square_card_idv_error_title:I = 0x7f1200f1

.field public static final auth_square_card_idv_failure_close:I = 0x7f1200f2

.field public static final auth_square_card_idv_failure_message:I = 0x7f1200f3

.field public static final auth_square_card_idv_failure_title:I = 0x7f1200f4

.field public static final auth_square_card_idv_failure_try_again:I = 0x7f1200f5

.field public static final auth_square_card_idv_verifying:I = 0x7f1200f6

.field public static final auth_square_card_missing_address_message:I = 0x7f1200f7

.field public static final auth_square_card_missing_address_title:I = 0x7f1200f8

.field public static final auth_square_card_missing_birthdate_message:I = 0x7f1200f9

.field public static final auth_square_card_missing_birthdate_title:I = 0x7f1200fa

.field public static final auth_square_card_missing_ssn_message:I = 0x7f1200fb

.field public static final auth_square_card_missing_ssn_title:I = 0x7f1200fc

.field public static final auth_square_card_owner_address_label:I = 0x7f1200fd

.field public static final auth_square_card_owner_birthdate_label:I = 0x7f1200fe

.field public static final auth_square_card_owner_name_label:I = 0x7f1200ff

.field public static final auth_square_card_personal_info_header:I = 0x7f120100

.field public static final auth_square_card_ssn_info_caption:I = 0x7f120101

.field public static final auth_square_card_ssn_info_header:I = 0x7f120102

.field public static final auth_square_card_ssn_info_label:I = 0x7f120103

.field public static final auth_square_card_title:I = 0x7f120104

.field public static final authorization_failed:I = 0x7f120105

.field public static final auto_capture_body:I = 0x7f120106

.field public static final auto_capture_title:I = 0x7f120107

.field public static final auto_gratuity_add_a_tip:I = 0x7f120108

.field public static final auto_gratuity_disclosure:I = 0x7f120109

.field public static final auto_gratuity_name_customer_facing:I = 0x7f12010a

.field public static final auto_gratuity_name_merchant_facing:I = 0x7f12010b

.field public static final auto_void_body_crash:I = 0x7f12010c

.field public static final auto_void_body_timeout:I = 0x7f12010d

.field public static final auto_void_title:I = 0x7f12010e

.field public static final automatic_deposits_enabled_body:I = 0x7f12010f

.field public static final automatic_deposits_enabled_title:I = 0x7f120110

.field public static final available_balance_uppercase:I = 0x7f120111

.field public static final awaiting_bank_account_verification:I = 0x7f120112

.field public static final awaiting_bank_account_verification_card_skip_message:I = 0x7f120113

.field public static final awaiting_bank_account_verification_message:I = 0x7f120114

.field public static final awaiting_bank_account_verification_schedule_failure_message:I = 0x7f120115

.field public static final awaiting_bank_account_verification_schedule_success_message:I = 0x7f120116

.field public static final awaiting_debit_authorization:I = 0x7f120117

.field public static final awaiting_email_confirmation:I = 0x7f120118

.field public static final awaiting_tip:I = 0x7f120119

.field public static final awaiting_verification:I = 0x7f12011a

.field public static final back:I = 0x7f12011b

.field public static final back_button_content_description:I = 0x7f12011c

.field public static final balance:I = 0x7f12011d

.field public static final balance_activity_action_bar_title:I = 0x7f12011e

.field public static final balance_activity_all_tab_bottom_message:I = 0x7f12011f

.field public static final balance_activity_details_error_message:I = 0x7f120120

.field public static final balance_activity_details_error_retry:I = 0x7f120121

.field public static final balance_activity_details_expense_type_title:I = 0x7f120122

.field public static final balance_activity_empty_message_all:I = 0x7f120123

.field public static final balance_activity_empty_message_card_spend:I = 0x7f120124

.field public static final balance_activity_empty_message_transfers:I = 0x7f120125

.field public static final balance_activity_error:I = 0x7f120126

.field public static final balance_activity_loading_message:I = 0x7f120127

.field public static final balance_activity_navigate_to_transfer_reports:I = 0x7f120128

.field public static final balance_activity_retry:I = 0x7f120129

.field public static final balance_activity_sub_label_card_payment:I = 0x7f12012a

.field public static final balance_activity_sub_label_card_processing:I = 0x7f12012b

.field public static final balance_activity_sub_label_dispute_lost:I = 0x7f12012c

.field public static final balance_activity_sub_label_instant_transfer_out:I = 0x7f12012d

.field public static final balance_activity_sub_label_payroll_cancellation:I = 0x7f12012e

.field public static final balance_activity_sub_label_transfer_in:I = 0x7f12012f

.field public static final balance_activity_sub_label_transfer_out:I = 0x7f120130

.field public static final balance_activity_tab_all:I = 0x7f120131

.field public static final balance_activity_tab_card_spend:I = 0x7f120132

.field public static final balance_activity_tab_card_transfers:I = 0x7f120133

.field public static final balance_activity_title_completed:I = 0x7f120134

.field public static final balance_activity_title_pending:I = 0x7f120135

.field public static final balance_sections_error_title:I = 0x7f120136

.field public static final balance_split_all_caps:I = 0x7f120137

.field public static final balance_transaction_detail_at_uppercase:I = 0x7f120138

.field public static final balance_transaction_detail_business:I = 0x7f120139

.field public static final balance_transaction_detail_date:I = 0x7f12013a

.field public static final balance_transaction_detail_expense_type_uppercase:I = 0x7f12013b

.field public static final balance_transaction_detail_personal:I = 0x7f12013c

.field public static final balance_transaction_detail_time:I = 0x7f12013d

.field public static final balance_transaction_detail_total_titlecase:I = 0x7f12013e

.field public static final balance_transaction_detail_total_uppercase:I = 0x7f12013f

.field public static final balance_transaction_details_uppercase:I = 0x7f120140

.field public static final bank_account:I = 0x7f120141

.field public static final bank_account_fine_print:I = 0x7f120142

.field public static final bank_account_fine_print_au:I = 0x7f120143

.field public static final bank_account_fine_print_continued:I = 0x7f120144

.field public static final bank_account_fine_print_gb:I = 0x7f120145

.field public static final bank_account_hint:I = 0x7f120146

.field public static final bank_account_holder_hint:I = 0x7f120147

.field public static final bank_account_linked:I = 0x7f120148

.field public static final bank_account_linking_failed:I = 0x7f120149

.field public static final bank_account_linking_failed_message:I = 0x7f12014a

.field public static final bank_account_number:I = 0x7f12014b

.field public static final bank_account_number_confirm:I = 0x7f12014c

.field public static final bank_account_required_message:I = 0x7f12014d

.field public static final bank_account_required_primary_button:I = 0x7f12014e

.field public static final bank_account_required_title:I = 0x7f12014f

.field public static final bank_account_settings:I = 0x7f120150

.field public static final bank_account_title:I = 0x7f120151

.field public static final bank_account_troubleshooting_url:I = 0x7f120152

.field public static final bank_account_type_business_checking:I = 0x7f120153

.field public static final bank_account_type_checking:I = 0x7f120154

.field public static final bank_account_type_savings:I = 0x7f120155

.field public static final bank_account_verification_in_progress:I = 0x7f120156

.field public static final bank_account_verification_in_progress_message:I = 0x7f120157

.field public static final bank_account_verified:I = 0x7f120158

.field public static final bank_account_verified_message:I = 0x7f120159

.field public static final bank_call_to_action:I = 0x7f12015a

.field public static final bank_code:I = 0x7f12015b

.field public static final bank_confirmation_approved_headline:I = 0x7f12015c

.field public static final bank_confirmation_approved_prompt:I = 0x7f12015d

.field public static final bank_confirmation_headline:I = 0x7f12015e

.field public static final bank_confirmation_prompt:I = 0x7f12015f

.field public static final bank_failed_prompt:I = 0x7f120160

.field public static final bank_name_and_account_type:I = 0x7f120161

.field public static final bank_or_building_society_information:I = 0x7f120162

.field public static final banner_title:I = 0x7f120163

.field public static final barcode_scanner_connected:I = 0x7f120164

.field public static final barcode_scanner_disconnected:I = 0x7f120165

.field public static final barcode_scanners_help_message:I = 0x7f120166

.field public static final barcode_scanners_none_found:I = 0x7f120167

.field public static final barcode_scanners_settings_label:I = 0x7f120168

.field public static final barcode_scanners_uppercase_available:I = 0x7f120169

.field public static final battery_level_hud_content:I = 0x7f12016a

.field public static final bic:I = 0x7f12016b

.field public static final bill_history_empty_view_with_dip_card_reader:I = 0x7f12016c

.field public static final bill_history_empty_view_with_swipe_and_dip_card_readers:I = 0x7f12016d

.field public static final bill_history_empty_view_with_swipe_card_reader:I = 0x7f12016e

.field public static final bill_history_empty_view_without_card_reader:I = 0x7f12016f

.field public static final bill_history_loyalty_earned:I = 0x7f120170

.field public static final bill_history_loyalty_earned_reward_plural:I = 0x7f120171

.field public static final bill_history_loyalty_earned_reward_single:I = 0x7f120172

.field public static final bill_history_loyalty_earned_singular:I = 0x7f120173

.field public static final bill_history_loyalty_earned_zero:I = 0x7f120174

.field public static final bill_history_loyalty_enrolled_in_loyalty:I = 0x7f120175

.field public static final bill_history_loyalty_title_uppercase:I = 0x7f120176

.field public static final bill_history_loyalty_zero_earned_reason_buyer_declined:I = 0x7f120177

.field public static final bill_history_loyalty_zero_earned_reason_client_disabled_loyalty:I = 0x7f120178

.field public static final bill_history_loyalty_zero_earned_reason_missing_loyalty_info:I = 0x7f120179

.field public static final bill_history_loyalty_zero_earned_reason_not_yet_subscribed:I = 0x7f12017a

.field public static final bill_history_loyalty_zero_earned_reason_offline_mode:I = 0x7f12017b

.field public static final bill_history_loyalty_zero_earned_reason_purchase_did_not_qualify:I = 0x7f12017c

.field public static final bill_history_loyalty_zero_earned_reason_returned_from_loyalty_screen:I = 0x7f12017d

.field public static final bill_history_loyalty_zero_earned_reason_returned_from_receipt_screen:I = 0x7f12017e

.field public static final bill_history_loyalty_zero_earned_reason_skipped_screen:I = 0x7f12017f

.field public static final bill_history_loyalty_zero_earned_reason_spend_too_low:I = 0x7f120180

.field public static final bill_history_tender_section_cashier:I = 0x7f120181

.field public static final bill_to:I = 0x7f120182

.field public static final ble_pairing_already_bonded_message:I = 0x7f120183

.field public static final ble_pairing_failed_message:I = 0x7f120184

.field public static final ble_pairing_failed_title:I = 0x7f120185

.field public static final ble_pairing_failed_version_message:I = 0x7f120186

.field public static final ble_pairing_failed_version_title:I = 0x7f120187

.field public static final blocked_audio_message:I = 0x7f120188

.field public static final blocked_audio_title:I = 0x7f120189

.field public static final blocked_by_buyer_facing_display_cancel:I = 0x7f12018a

.field public static final blocked_by_buyer_facing_display_charge:I = 0x7f12018b

.field public static final blocked_by_loyalty_checkin_body:I = 0x7f12018c

.field public static final blocked_by_loyalty_checkin_title:I = 0x7f12018d

.field public static final book_new_appointment_with_customer:I = 0x7f12018e

.field public static final book_new_appointment_with_customer_shortened:I = 0x7f12018f

.field public static final bottom_sheet_behavior:I = 0x7f120190

.field public static final branch_code:I = 0x7f120191

.field public static final brightness_uppercase:I = 0x7f120192

.field public static final browser_chooser_title:I = 0x7f120193

.field public static final bsb_number:I = 0x7f120194

.field public static final bug_report_description:I = 0x7f120195

.field public static final bug_report_name:I = 0x7f120196

.field public static final bug_report_title:I = 0x7f120197

.field public static final bulk_settle_add_tips_title:I = 0x7f120198

.field public static final bulk_settle_button_hint:I = 0x7f120199

.field public static final bulk_settle_confirm_settle_button:I = 0x7f12019a

.field public static final bulk_settle_finish_unsettled_body_many:I = 0x7f12019b

.field public static final bulk_settle_finish_unsettled_body_one:I = 0x7f12019c

.field public static final bulk_settle_finish_unsettled_discard_tips_many:I = 0x7f12019d

.field public static final bulk_settle_finish_unsettled_discard_tips_one:I = 0x7f12019e

.field public static final bulk_settle_finish_unsettled_title_many:I = 0x7f12019f

.field public static final bulk_settle_finish_unsettled_title_one:I = 0x7f1201a0

.field public static final bulk_settle_no_payments:I = 0x7f1201a1

.field public static final bulk_settle_no_search_results:I = 0x7f1201a2

.field public static final bulk_settle_receipt_number:I = 0x7f1201a3

.field public static final bulk_settle_search_hint:I = 0x7f1201a4

.field public static final bulk_settle_settle_button_nonzero:I = 0x7f1201a5

.field public static final bulk_settle_settle_button_zero:I = 0x7f1201a6

.field public static final bulk_settle_settle_failed_body_many:I = 0x7f1201a7

.field public static final bulk_settle_settle_failed_body_one:I = 0x7f1201a8

.field public static final bulk_settle_tender_row_total:I = 0x7f1201a9

.field public static final bulk_settle_tender_row_total_with_percentage:I = 0x7f1201aa

.field public static final bulk_settle_uppercase_header_all_tips:I = 0x7f1201ab

.field public static final bulk_settle_uppercase_header_your_tips:I = 0x7f1201ac

.field public static final bulk_settle_uppercase_sort_amount:I = 0x7f1201ad

.field public static final bulk_settle_uppercase_sort_employee:I = 0x7f1201ae

.field public static final bulk_settle_uppercase_sort_time:I = 0x7f1201af

.field public static final bulk_settle_view_all_button:I = 0x7f1201b0

.field public static final business_address_different_as_home:I = 0x7f1201b1

.field public static final business_address_heading:I = 0x7f1201b2

.field public static final business_address_mobile_business:I = 0x7f1201b3

.field public static final business_address_mobile_business_help:I = 0x7f1201b4

.field public static final business_address_same_as_home:I = 0x7f1201b5

.field public static final business_address_subheading:I = 0x7f1201b6

.field public static final business_address_warning_no_po_box_allowed_message:I = 0x7f1201b7

.field public static final business_address_warning_no_po_box_allowed_title:I = 0x7f1201b8

.field public static final business_address_warning_try_again:I = 0x7f1201b9

.field public static final business_address_warning_try_again_message:I = 0x7f1201ba

.field public static final business_checking:I = 0x7f1201bb

.field public static final business_info_heading:I = 0x7f1201bc

.field public static final business_info_subheading:I = 0x7f1201bd

.field public static final business_name_hint:I = 0x7f1201be

.field public static final button_0:I = 0x7f1201bf

.field public static final button_00:I = 0x7f1201c0

.field public static final button_1:I = 0x7f1201c1

.field public static final button_2:I = 0x7f1201c2

.field public static final button_3:I = 0x7f1201c3

.field public static final button_4:I = 0x7f1201c4

.field public static final button_5:I = 0x7f1201c5

.field public static final button_6:I = 0x7f1201c6

.field public static final button_7:I = 0x7f1201c7

.field public static final button_8:I = 0x7f1201c8

.field public static final button_9:I = 0x7f1201c9

.field public static final button_backspace:I = 0x7f1201ca

.field public static final button_cancel:I = 0x7f1201cb

.field public static final button_clear:I = 0x7f1201cc

.field public static final button_decimal:I = 0x7f1201cd

.field public static final button_minus_content_description:I = 0x7f1201ce

.field public static final button_plus:I = 0x7f1201cf

.field public static final button_plus_content_description:I = 0x7f1201d0

.field public static final button_skip:I = 0x7f1201d1

.field public static final button_submit:I = 0x7f1201d2

.field public static final buy_items_any:I = 0x7f1201d3

.field public static final buy_items_exact_many:I = 0x7f1201d4

.field public static final buy_items_exact_one:I = 0x7f1201d5

.field public static final buy_items_min_many:I = 0x7f1201d6

.field public static final buy_items_min_one:I = 0x7f1201d7

.field public static final buyer_add_a_tip:I = 0x7f1201d8

.field public static final buyer_add_tip:I = 0x7f1201d9

.field public static final buyer_amount_tip_action_bar:I = 0x7f1201da

.field public static final buyer_approved:I = 0x7f1201db

.field public static final buyer_auth_declined_message:I = 0x7f1201dc

.field public static final buyer_auth_declined_title:I = 0x7f1201dd

.field public static final buyer_authorizing:I = 0x7f1201de

.field public static final buyer_banner_item_edited:I = 0x7f1201df

.field public static final buyer_banner_item_removed:I = 0x7f1201e0

.field public static final buyer_card_brand_amex:I = 0x7f1201e1

.field public static final buyer_card_brand_amex_phrase:I = 0x7f1201e2

.field public static final buyer_card_brand_cup:I = 0x7f1201e3

.field public static final buyer_card_brand_cup_phrase:I = 0x7f1201e4

.field public static final buyer_card_brand_discover:I = 0x7f1201e5

.field public static final buyer_card_brand_discover_phrase:I = 0x7f1201e6

.field public static final buyer_card_brand_gift_card:I = 0x7f1201e7

.field public static final buyer_card_brand_gift_card_phrase:I = 0x7f1201e8

.field public static final buyer_card_brand_interac:I = 0x7f1201e9

.field public static final buyer_card_brand_interac_phrase:I = 0x7f1201ea

.field public static final buyer_card_brand_jcb:I = 0x7f1201eb

.field public static final buyer_card_brand_jcb_phrase:I = 0x7f1201ec

.field public static final buyer_card_brand_mastercard:I = 0x7f1201ed

.field public static final buyer_card_brand_square_capital_card:I = 0x7f1201ee

.field public static final buyer_card_brand_square_capital_card_phrase:I = 0x7f1201ef

.field public static final buyer_card_brand_unknown:I = 0x7f1201f0

.field public static final buyer_card_brand_unknown_phrase:I = 0x7f1201f1

.field public static final buyer_card_brand_visa:I = 0x7f1201f2

.field public static final buyer_card_brand_visa_phrase:I = 0x7f1201f3

.field public static final buyer_cart_confirm_and_pay:I = 0x7f1201f4

.field public static final buyer_cart_list_total:I = 0x7f1201f5

.field public static final buyer_cart_quantity:I = 0x7f1201f6

.field public static final buyer_cart_section_header_purchase:I = 0x7f1201f7

.field public static final buyer_cart_section_header_return:I = 0x7f1201f8

.field public static final buyer_cart_tax_additive_row:I = 0x7f1201f9

.field public static final buyer_cart_title:I = 0x7f1201fa

.field public static final buyer_cart_title_exchange_even:I = 0x7f1201fb

.field public static final buyer_cart_title_offline:I = 0x7f1201fc

.field public static final buyer_cart_title_refund:I = 0x7f1201fd

.field public static final buyer_checkout_send_receipt:I = 0x7f1201fe

.field public static final buyer_clear:I = 0x7f1201ff

.field public static final buyer_clear_signature:I = 0x7f120200

.field public static final buyer_copy_link:I = 0x7f120201

.field public static final buyer_coupon_subtitle:I = 0x7f120202

.field public static final buyer_coupon_tip:I = 0x7f120203

.field public static final buyer_custom_tip_amount:I = 0x7f120204

.field public static final buyer_display_settings_label:I = 0x7f120205

.field public static final buyer_display_tip_percentage_hint:I = 0x7f120206

.field public static final buyer_done_label:I = 0x7f120207

.field public static final buyer_message_all_done:I = 0x7f120208

.field public static final buyer_message_all_done_dipped:I = 0x7f120209

.field public static final buyer_message_approved:I = 0x7f12020a

.field public static final buyer_message_approved_dipped:I = 0x7f12020b

.field public static final buyer_message_authorizing_dip:I = 0x7f12020c

.field public static final buyer_message_preparing:I = 0x7f12020d

.field public static final buyer_message_warning_card_removed:I = 0x7f12020e

.field public static final buyer_message_warning_swipe_non_scheme:I = 0x7f12020f

.field public static final buyer_message_warning_swipe_scheme:I = 0x7f120210

.field public static final buyer_no_thanks:I = 0x7f120211

.field public static final buyer_order_name_action_bar:I = 0x7f120212

.field public static final buyer_order_name_call_to_action:I = 0x7f120213

.field public static final buyer_payment_note_tip:I = 0x7f120214

.field public static final buyer_please_sign_here:I = 0x7f120215

.field public static final buyer_please_sign_here_manual:I = 0x7f120216

.field public static final buyer_printed_receipt_additions_label_and_price:I = 0x7f120217

.field public static final buyer_printed_receipt_aid:I = 0x7f120218

.field public static final buyer_printed_receipt_authorization_number:I = 0x7f120219

.field public static final buyer_printed_receipt_comp:I = 0x7f12021a

.field public static final buyer_printed_receipt_discount:I = 0x7f12021b

.field public static final buyer_printed_receipt_disposition_approved_uppercase:I = 0x7f12021c

.field public static final buyer_printed_receipt_entry_method_contactless:I = 0x7f12021d

.field public static final buyer_printed_receipt_entry_method_emv:I = 0x7f12021e

.field public static final buyer_printed_receipt_entry_method_keyed:I = 0x7f12021f

.field public static final buyer_printed_receipt_entry_method_on_file:I = 0x7f120220

.field public static final buyer_printed_receipt_entry_method_swipe:I = 0x7f120221

.field public static final buyer_printed_receipt_invoice:I = 0x7f120222

.field public static final buyer_printed_receipt_invoice_number:I = 0x7f120223

.field public static final buyer_printed_receipt_keypad_item_name:I = 0x7f120224

.field public static final buyer_printed_receipt_label_and_price:I = 0x7f120225

.field public static final buyer_printed_receipt_non_taxable:I = 0x7f120226

.field public static final buyer_printed_receipt_number:I = 0x7f120227

.field public static final buyer_printed_receipt_remaining_balance:I = 0x7f120228

.field public static final buyer_printed_receipt_rounding:I = 0x7f120229

.field public static final buyer_printed_receipt_subtotal:I = 0x7f12022a

.field public static final buyer_printed_receipt_subtotal_purchase:I = 0x7f12022b

.field public static final buyer_printed_receipt_subtotal_return:I = 0x7f12022c

.field public static final buyer_printed_receipt_tax_id:I = 0x7f12022d

.field public static final buyer_printed_receipt_tax_invoice:I = 0x7f12022e

.field public static final buyer_printed_receipt_taxes_included_items:I = 0x7f12022f

.field public static final buyer_printed_receipt_taxes_included_items_format:I = 0x7f120230

.field public static final buyer_printed_receipt_taxes_included_total:I = 0x7f120231

.field public static final buyer_printed_receipt_taxes_included_total_format:I = 0x7f120232

.field public static final buyer_printed_receipt_taxes_inclusive_tax_format:I = 0x7f120233

.field public static final buyer_printed_receipt_tender_amount:I = 0x7f120234

.field public static final buyer_printed_receipt_tender_card:I = 0x7f120235

.field public static final buyer_printed_receipt_tender_card_detail_all:I = 0x7f120236

.field public static final buyer_printed_receipt_tender_card_detail_no_entry_method:I = 0x7f120237

.field public static final buyer_printed_receipt_tender_card_detail_no_suffix:I = 0x7f120238

.field public static final buyer_printed_receipt_tender_cash:I = 0x7f120239

.field public static final buyer_printed_receipt_tender_change:I = 0x7f12023a

.field public static final buyer_printed_receipt_tender_no_sale:I = 0x7f12023b

.field public static final buyer_printed_receipt_tender_other:I = 0x7f12023c

.field public static final buyer_printed_receipt_test_title:I = 0x7f12023d

.field public static final buyer_printed_receipt_test_title_with_printer_name:I = 0x7f12023e

.field public static final buyer_printed_receipt_ticket:I = 0x7f12023f

.field public static final buyer_printed_receipt_tip:I = 0x7f120240

.field public static final buyer_printed_receipt_title:I = 0x7f120241

.field public static final buyer_printed_receipt_total:I = 0x7f120242

.field public static final buyer_printed_receipt_unit_cost_each:I = 0x7f120243

.field public static final buyer_printed_receipt_verification_method_on_device:I = 0x7f120244

.field public static final buyer_printed_receipt_verification_method_pin:I = 0x7f120245

.field public static final buyer_processing:I = 0x7f120246

.field public static final buyer_receipt_add_customer:I = 0x7f120247

.field public static final buyer_receipt_email:I = 0x7f120248

.field public static final buyer_receipt_email_hint:I = 0x7f120249

.field public static final buyer_receipt_invalid_email:I = 0x7f12024a

.field public static final buyer_receipt_invalid_input:I = 0x7f12024b

.field public static final buyer_receipt_invalid_sms:I = 0x7f12024c

.field public static final buyer_receipt_no_receipt:I = 0x7f12024d

.field public static final buyer_receipt_print:I = 0x7f12024e

.field public static final buyer_receipt_print_formal:I = 0x7f12024f

.field public static final buyer_receipt_printed:I = 0x7f120250

.field public static final buyer_receipt_send:I = 0x7f120251

.field public static final buyer_receipt_sent:I = 0x7f120252

.field public static final buyer_receipt_text:I = 0x7f120253

.field public static final buyer_receipt_text_hint:I = 0x7f120254

.field public static final buyer_receipt_text_receipt:I = 0x7f120255

.field public static final buyer_receipt_view_customer:I = 0x7f120256

.field public static final buyer_refund_policy_title:I = 0x7f120257

.field public static final buyer_remaining_card_balance:I = 0x7f120258

.field public static final buyer_remaining_card_balance_for_hud:I = 0x7f120259

.field public static final buyer_remaining_payment_due:I = 0x7f12025a

.field public static final buyer_remote_receipt_cash_with_change_title:I = 0x7f12025b

.field public static final buyer_send_receipt_all_done:I = 0x7f12025c

.field public static final buyer_send_receipt_all_done_email:I = 0x7f12025d

.field public static final buyer_send_receipt_all_done_text:I = 0x7f12025e

.field public static final buyer_send_receipt_button_long:I = 0x7f12025f

.field public static final buyer_send_receipt_button_short:I = 0x7f120260

.field public static final buyer_send_receipt_digital_subtitle:I = 0x7f120261

.field public static final buyer_send_receipt_email:I = 0x7f120262

.field public static final buyer_send_receipt_printed:I = 0x7f120263

.field public static final buyer_send_receipt_subtitle:I = 0x7f120264

.field public static final buyer_send_receipt_text:I = 0x7f120265

.field public static final buyer_send_receipt_title:I = 0x7f120266

.field public static final buyer_send_receipt_title_cash_change:I = 0x7f120267

.field public static final buyer_send_receipt_title_cash_change_only:I = 0x7f120268

.field public static final buyer_send_receipt_title_cash_no_change:I = 0x7f120269

.field public static final buyer_send_receipt_title_no_change:I = 0x7f12026a

.field public static final buyer_send_receipt_title_no_change_sub:I = 0x7f12026b

.field public static final buyer_send_receipt_title_no_change_sub_with_remaining_balance:I = 0x7f12026c

.field public static final buyer_signature_disclaimer:I = 0x7f12026d

.field public static final buyer_signature_disclaimer_no_auth:I = 0x7f12026e

.field public static final buyer_signature_disclaimer_no_name:I = 0x7f12026f

.field public static final buyer_signature_disclaimer_no_name_no_auth:I = 0x7f120270

.field public static final buyer_tip_action_bar:I = 0x7f120271

.field public static final buyer_tip_additional_tip:I = 0x7f120272

.field public static final buyer_tip_custom:I = 0x7f120273

.field public static final buyer_tip_no_additional_tip:I = 0x7f120274

.field public static final buyer_tip_no_tip:I = 0x7f120275

.field public static final buyer_tip_no_tip_amount_plus_auto_grat:I = 0x7f120276

.field public static final buyer_tip_no_tip_amount_plus_service_charge:I = 0x7f120277

.field public static final buyer_tip_no_tip_cap:I = 0x7f120278

.field public static final buyer_tip_plus_amount:I = 0x7f120279

.field public static final buyer_tip_plus_auto_grat_plus_amount:I = 0x7f12027a

.field public static final buyer_tip_plus_service_charge_plus_amount:I = 0x7f12027b

.field public static final buyer_view_refund_policy:I = 0x7f12027c

.field public static final call_support:I = 0x7f12027d

.field public static final call_your_bank_message:I = 0x7f12027e

.field public static final call_your_bank_title:I = 0x7f12027f

.field public static final cancel:I = 0x7f120280

.field public static final cancel_and_issue_refund:I = 0x7f120281

.field public static final cancel_bank_verification:I = 0x7f120282

.field public static final cancel_bizbank_error_message:I = 0x7f120283

.field public static final cancel_bizbank_error_title:I = 0x7f120284

.field public static final cancel_card_action_bar:I = 0x7f120285

.field public static final cancel_card_button_text:I = 0x7f120286

.field public static final cancel_card_error_message:I = 0x7f120287

.field public static final cancel_card_error_title:I = 0x7f120288

.field public static final cancel_card_message:I = 0x7f120289

.field public static final cancel_card_message_title:I = 0x7f12028a

.field public static final cancel_card_never_received_message:I = 0x7f12028b

.field public static final cancel_card_only_message:I = 0x7f12028c

.field public static final cancel_card_reason_generic:I = 0x7f12028d

.field public static final cancel_card_reason_lost:I = 0x7f12028e

.field public static final cancel_card_reason_never_received:I = 0x7f12028f

.field public static final cancel_card_reason_stolen:I = 0x7f120290

.field public static final cancel_card_reasons_title:I = 0x7f120291

.field public static final cancel_invoice_confirmation_text:I = 0x7f120292

.field public static final cancel_invoice_confirmation_text_partial_payment:I = 0x7f120293

.field public static final cancel_invoice_notify_recipients:I = 0x7f120294

.field public static final cancel_lost_card_action_bar:I = 0x7f120295

.field public static final cancel_never_received_card_action_bar:I = 0x7f120296

.field public static final cancel_refund:I = 0x7f120297

.field public static final cancel_split_tender_transaction_message:I = 0x7f120298

.field public static final cancel_split_tender_transaction_title:I = 0x7f120299

.field public static final cancel_stolen_card_action_bar:I = 0x7f12029a

.field public static final cancel_transaction:I = 0x7f12029b

.field public static final cancel_transaction_content_description:I = 0x7f12029c

.field public static final cancel_verification:I = 0x7f12029d

.field public static final cancel_verification_failed:I = 0x7f12029e

.field public static final cancel_verification_message:I = 0x7f12029f

.field public static final cancel_verification_title:I = 0x7f1202a0

.field public static final canceled_bizbank_succeeded_action_bar:I = 0x7f1202a1

.field public static final canceled_bizbank_succeeded_help:I = 0x7f1202a2

.field public static final canceled_bizbank_succeeded_message:I = 0x7f1202a3

.field public static final canceled_bizbank_succeeded_message_title:I = 0x7f1202a4

.field public static final canceled_card_leave_feedback:I = 0x7f1202a5

.field public static final canceled_card_leave_feedback_button_text:I = 0x7f1202a6

.field public static final canceled_card_leave_feedback_hint:I = 0x7f1202a7

.field public static final canceled_card_leave_feedback_message:I = 0x7f1202a8

.field public static final canceled_card_success_bizbank_enabled_message:I = 0x7f1202a9

.field public static final canceled_card_success_disable_bizbank_button_text:I = 0x7f1202aa

.field public static final canceled_card_success_message:I = 0x7f1202ab

.field public static final canceled_card_success_order_replacement_button_text:I = 0x7f1202ac

.field public static final canceled_card_success_title:I = 0x7f1202ad

.field public static final canceling:I = 0x7f1202ae

.field public static final canceling_bizbank_spinner_message:I = 0x7f1202af

.field public static final canceling_card_spinner_message:I = 0x7f1202b0

.field public static final cannot_process_refund_title:I = 0x7f1202b1

.field public static final cannot_sign_out:I = 0x7f1202b2

.field public static final cannot_sign_out_message:I = 0x7f1202b3

.field public static final cannot_sign_out_message_open_cash_drawer_shift:I = 0x7f1202b4

.field public static final cannot_sign_out_message_plural:I = 0x7f1202b5

.field public static final capital_application_pending_body_message:I = 0x7f1202b6

.field public static final capital_application_pending_body_title:I = 0x7f1202b7

.field public static final capital_application_pending_hero_message:I = 0x7f1202b8

.field public static final capital_application_pending_hero_title:I = 0x7f1202b9

.field public static final capital_error_message:I = 0x7f1202ba

.field public static final capital_error_title:I = 0x7f1202bb

.field public static final capital_manage_plan:I = 0x7f1202bc

.field public static final capital_no_offer_message:I = 0x7f1202bd

.field public static final capital_no_offer_title:I = 0x7f1202be

.field public static final capital_offer_choose_amount_action:I = 0x7f1202bf

.field public static final capital_offer_loan_up_to:I = 0x7f1202c0

.field public static final capital_offer_screen_body_credit_score:I = 0x7f1202c1

.field public static final capital_offer_screen_body_funding:I = 0x7f1202c2

.field public static final capital_offer_screen_body_payback:I = 0x7f1202c3

.field public static final capital_offer_screen_loading:I = 0x7f1202c4

.field public static final capital_offer_screen_loan_offer:I = 0x7f1202c5

.field public static final capital_offer_screen_motivation_text:I = 0x7f1202c6

.field public static final capital_offer_screen_reason_easy_application:I = 0x7f1202c7

.field public static final capital_offer_screen_reason_fast_funding:I = 0x7f1202c8

.field public static final capital_offer_screen_reason_payback:I = 0x7f1202c9

.field public static final capital_plan_complete:I = 0x7f1202ca

.field public static final capital_plan_screen_min_payment:I = 0x7f1202cb

.field public static final capital_plan_screen_outstanding:I = 0x7f1202cc

.field public static final capital_plan_screen_past_due:I = 0x7f1202cd

.field public static final capital_plan_screen_total_paid:I = 0x7f1202ce

.field public static final capital_plan_screen_total_remaining:I = 0x7f1202cf

.field public static final card:I = 0x7f1202d0

.field public static final card_activation_confirm_address_message:I = 0x7f1202d1

.field public static final card_activation_confirm_card_action_bar:I = 0x7f1202d2

.field public static final card_activation_confirm_card_incorrect_info_error_message:I = 0x7f1202d3

.field public static final card_activation_confirm_card_incorrect_info_error_title:I = 0x7f1202d4

.field public static final card_activation_confirm_card_message_no_reader:I = 0x7f1202d5

.field public static final card_activation_confirm_card_message_with_reader:I = 0x7f1202d6

.field public static final card_activation_confirm_card_title:I = 0x7f1202d7

.field public static final card_activation_confirm_code_action_bar:I = 0x7f1202d8

.field public static final card_activation_confirm_code_already_used_action_bar:I = 0x7f1202d9

.field public static final card_activation_confirm_code_already_used_message:I = 0x7f1202da

.field public static final card_activation_confirm_code_already_used_title:I = 0x7f1202db

.field public static final card_activation_confirm_code_expired_message:I = 0x7f1202dc

.field public static final card_activation_confirm_code_expired_title:I = 0x7f1202dd

.field public static final card_activation_confirm_code_expired_token_error_message:I = 0x7f1202de

.field public static final card_activation_confirm_code_expired_token_error_title:I = 0x7f1202df

.field public static final card_activation_confirm_code_help:I = 0x7f1202e0

.field public static final card_activation_confirm_code_help_resend_email:I = 0x7f1202e1

.field public static final card_activation_confirm_code_help_resend_email_failed_title:I = 0x7f1202e2

.field public static final card_activation_confirm_code_hint:I = 0x7f1202e3

.field public static final card_activation_confirm_code_invalid_token_error_message:I = 0x7f1202e4

.field public static final card_activation_confirm_code_invalid_token_error_title:I = 0x7f1202e5

.field public static final card_activation_confirm_code_message:I = 0x7f1202e6

.field public static final card_activation_confirm_code_title:I = 0x7f1202e7

.field public static final card_activation_confirming_card:I = 0x7f1202e8

.field public static final card_activation_confirming_code:I = 0x7f1202e9

.field public static final card_activation_creating_pin:I = 0x7f1202ea

.field public static final card_activation_creating_pin_button_label:I = 0x7f1202eb

.field public static final card_activation_generic_error_message:I = 0x7f1202ec

.field public static final card_activation_generic_error_title:I = 0x7f1202ed

.field public static final card_activation_sending_email:I = 0x7f1202ee

.field public static final card_activity_declined:I = 0x7f1202ef

.field public static final card_activity_load_more_error:I = 0x7f1202f0

.field public static final card_activity_load_more_retry_button:I = 0x7f1202f1

.field public static final card_activity_pending:I = 0x7f1202f2

.field public static final card_brand_amex:I = 0x7f1202f3

.field public static final card_brand_amex_short:I = 0x7f1202f4

.field public static final card_brand_amex_short_uppercase:I = 0x7f1202f5

.field public static final card_brand_cup:I = 0x7f1202f6

.field public static final card_brand_cup_uppercase:I = 0x7f1202f7

.field public static final card_brand_discover:I = 0x7f1202f8

.field public static final card_brand_discover_uppercase:I = 0x7f1202f9

.field public static final card_brand_eftpos:I = 0x7f1202fa

.field public static final card_brand_felica:I = 0x7f1202fb

.field public static final card_brand_felica_uppercase:I = 0x7f1202fc

.field public static final card_brand_interac:I = 0x7f1202fd

.field public static final card_brand_interac_uppercase:I = 0x7f1202fe

.field public static final card_brand_jcb:I = 0x7f1202ff

.field public static final card_brand_jcb_uppercase:I = 0x7f120300

.field public static final card_brand_mastercard:I = 0x7f120301

.field public static final card_brand_mastercard_uppercase:I = 0x7f120302

.field public static final card_brand_square_capital_card:I = 0x7f120303

.field public static final card_brand_square_capital_card_uppercase:I = 0x7f120304

.field public static final card_brand_unknown:I = 0x7f120305

.field public static final card_brand_unknown_uppercase:I = 0x7f120306

.field public static final card_brand_visa:I = 0x7f120307

.field public static final card_brand_visa_uppercase:I = 0x7f120308

.field public static final card_disabled_message:I = 0x7f120309

.field public static final card_formatted_first_last:I = 0x7f12030a

.field public static final card_formatted_title_first:I = 0x7f12030b

.field public static final card_formatted_title_first_last:I = 0x7f12030c

.field public static final card_formatted_title_last:I = 0x7f12030d

.field public static final card_help:I = 0x7f12030e

.field public static final card_info:I = 0x7f12030f

.field public static final card_linking_failed:I = 0x7f120310

.field public static final card_not_read_message:I = 0x7f120311

.field public static final card_not_read_title:I = 0x7f120312

.field public static final card_not_supported:I = 0x7f120313

.field public static final card_not_supported_bank_pending_message:I = 0x7f120314

.field public static final card_not_supported_bank_success_message:I = 0x7f120315

.field public static final card_not_supported_message:I = 0x7f120316

.field public static final card_on_file:I = 0x7f120317

.field public static final card_on_file_action_bar_title:I = 0x7f120318

.field public static final card_on_file_charge_confirmation_body:I = 0x7f120319

.field public static final card_on_file_charge_confirmation_body_with_type:I = 0x7f12031a

.field public static final card_on_file_charge_confirmation_title:I = 0x7f12031b

.field public static final card_on_file_expired:I = 0x7f12031c

.field public static final card_ordered_activate_button_text_pending_2fa:I = 0x7f12031d

.field public static final card_ordered_activate_button_text_shipped:I = 0x7f12031e

.field public static final card_ordered_activate_on_dashboard_text:I = 0x7f12031f

.field public static final card_ordered_activate_on_dashboard_title:I = 0x7f120320

.field public static final card_ordered_activation_complete_action_bar:I = 0x7f120321

.field public static final card_ordered_activation_complete_message:I = 0x7f120322

.field public static final card_ordered_activation_complete_title:I = 0x7f120323

.field public static final card_ordered_confirm_address_action_bar:I = 0x7f120324

.field public static final card_ordered_create_pin_action_bar:I = 0x7f120325

.field public static final card_ordered_create_pin_confirmation_hint:I = 0x7f120326

.field public static final card_ordered_create_pin_error_message:I = 0x7f120327

.field public static final card_ordered_create_pin_error_title:I = 0x7f120328

.field public static final card_ordered_create_pin_hint:I = 0x7f120329

.field public static final card_ordered_create_pin_message:I = 0x7f12032a

.field public static final card_ordered_create_pin_weak_error_button_text:I = 0x7f12032b

.field public static final card_ordered_create_pin_weak_error_message:I = 0x7f12032c

.field public static final card_ordered_create_pin_weak_error_title:I = 0x7f12032d

.field public static final card_ordered_deposits_info_action_bar:I = 0x7f12032e

.field public static final card_ordered_deposits_info_link:I = 0x7f12032f

.field public static final card_ordered_deposits_info_link_url:I = 0x7f120330

.field public static final card_ordered_deposits_info_message:I = 0x7f120331

.field public static final card_ordered_deposits_info_title:I = 0x7f120332

.field public static final card_ordered_message_issued:I = 0x7f120333

.field public static final card_ordered_message_pending_2fa:I = 0x7f120334

.field public static final card_ordered_message_shipped:I = 0x7f120335

.field public static final card_ordered_missing_address_message:I = 0x7f120336

.field public static final card_ordered_missing_address_title:I = 0x7f120337

.field public static final card_processing_not_activated_text:I = 0x7f120338

.field public static final card_processing_not_activated_title:I = 0x7f120339

.field public static final card_reader_details_help_message:I = 0x7f12033a

.field public static final card_reader_details_help_message_no_contactless:I = 0x7f12033b

.field public static final card_spend:I = 0x7f12033c

.field public static final card_spend_detail:I = 0x7f12033d

.field public static final card_suspended_dialog_message:I = 0x7f12033e

.field public static final card_suspended_dialog_title:I = 0x7f12033f

.field public static final card_suspended_message:I = 0x7f120340

.field public static final card_suspended_message_learn_more:I = 0x7f120341

.field public static final card_toggle_card_details_label:I = 0x7f120342

.field public static final card_toggle_label:I = 0x7f120343

.field public static final card_upsell_message:I = 0x7f120344

.field public static final cards_on_file:I = 0x7f120345

.field public static final cards_required_to_be_present_plural:I = 0x7f120346

.field public static final cards_required_to_be_present_singular:I = 0x7f120347

.field public static final cart_comps:I = 0x7f120348

.field public static final cart_discounts:I = 0x7f120349

.field public static final cart_item_price:I = 0x7f12034a

.field public static final cart_items_all:I = 0x7f12034b

.field public static final cart_items_none:I = 0x7f12034c

.field public static final cart_items_one:I = 0x7f12034d

.field public static final cart_items_some:I = 0x7f12034e

.field public static final cart_menu_drop_down_button_content_description:I = 0x7f12034f

.field public static final cart_menu_drop_down_content_description:I = 0x7f120350

.field public static final cart_remove_comp_item:I = 0x7f120351

.field public static final cart_remove_comp_item_confirm:I = 0x7f120352

.field public static final cart_remove_item:I = 0x7f120353

.field public static final cart_remove_item_confirm:I = 0x7f120354

.field public static final cart_remove_service:I = 0x7f120355

.field public static final cart_remove_service_confirm:I = 0x7f120356

.field public static final cart_tax_reset:I = 0x7f120357

.field public static final cart_tax_row:I = 0x7f120358

.field public static final cart_tax_row_included:I = 0x7f120359

.field public static final cart_tax_row_off:I = 0x7f12035a

.field public static final cart_taxes_title:I = 0x7f12035b

.field public static final cart_total:I = 0x7f12035c

.field public static final cart_variation:I = 0x7f12035d

.field public static final cart_view_title:I = 0x7f12035e

.field public static final cash:I = 0x7f12035f

.field public static final cash_drawer_actual_in_drawer:I = 0x7f120360

.field public static final cash_drawer_cash_refunds:I = 0x7f120361

.field public static final cash_drawer_cash_sales:I = 0x7f120362

.field public static final cash_drawer_connected:I = 0x7f120363

.field public static final cash_drawer_description:I = 0x7f120364

.field public static final cash_drawer_difference:I = 0x7f120365

.field public static final cash_drawer_disconnected:I = 0x7f120366

.field public static final cash_drawer_end_of_drawer:I = 0x7f120367

.field public static final cash_drawer_expected_in_drawer:I = 0x7f120368

.field public static final cash_drawer_failed_to_connect:I = 0x7f120369

.field public static final cash_drawer_paid_in_out:I = 0x7f12036a

.field public static final cash_drawer_print_report:I = 0x7f12036b

.field public static final cash_drawer_report_paid_in:I = 0x7f12036c

.field public static final cash_drawer_report_paid_out:I = 0x7f12036d

.field public static final cash_drawer_report_title:I = 0x7f12036e

.field public static final cash_drawer_start_of_drawer:I = 0x7f12036f

.field public static final cash_drawer_starting_cash:I = 0x7f120370

.field public static final cash_drawer_warning_text:I = 0x7f120371

.field public static final cash_drawers_charge_button_open:I = 0x7f120372

.field public static final cash_drawers_charge_button_open_confirm:I = 0x7f120373

.field public static final cash_drawers_help_message:I = 0x7f120374

.field public static final cash_drawers_none_found:I = 0x7f120375

.field public static final cash_drawers_settings_label:I = 0x7f120376

.field public static final cash_drawers_test_open:I = 0x7f120377

.field public static final cash_drawers_uppercase_available:I = 0x7f120378

.field public static final cash_management:I = 0x7f120379

.field public static final cash_management_auto_email_enable_hint:I = 0x7f12037a

.field public static final cash_management_auto_email_enable_label:I = 0x7f12037b

.field public static final cash_management_auto_print_enable_hint:I = 0x7f12037c

.field public static final cash_management_auto_print_enable_label:I = 0x7f12037d

.field public static final cash_management_default_starting_cash_label_uppercase:I = 0x7f12037e

.field public static final cash_management_disabled_message_subtitle:I = 0x7f12037f

.field public static final cash_management_disabled_message_title:I = 0x7f120380

.field public static final cash_management_email_address_hint:I = 0x7f120381

.field public static final cash_management_email_recipient_label_uppercase:I = 0x7f120382

.field public static final cash_management_enable_hint:I = 0x7f120383

.field public static final cash_management_enable_label:I = 0x7f120384

.field public static final cash_management_off:I = 0x7f120385

.field public static final cash_management_on:I = 0x7f120386

.field public static final cash_management_settings_save:I = 0x7f120387

.field public static final cash_management_unclosed_drawer_message:I = 0x7f120388

.field public static final cash_management_unclosed_drawer_title:I = 0x7f120389

.field public static final catalogMeasurementUnitAbbreviation_area_imperialAcre:I = 0x7f12038a

.field public static final catalogMeasurementUnitAbbreviation_area_imperialSquareFoot:I = 0x7f12038b

.field public static final catalogMeasurementUnitAbbreviation_area_imperialSquareInch:I = 0x7f12038c

.field public static final catalogMeasurementUnitAbbreviation_area_imperialSquareMile:I = 0x7f12038d

.field public static final catalogMeasurementUnitAbbreviation_area_imperialSquareYard:I = 0x7f12038e

.field public static final catalogMeasurementUnitAbbreviation_area_metricSquareCentimeter:I = 0x7f12038f

.field public static final catalogMeasurementUnitAbbreviation_area_metricSquareKilometer:I = 0x7f120390

.field public static final catalogMeasurementUnitAbbreviation_area_metricSquareMeter:I = 0x7f120391

.field public static final catalogMeasurementUnitAbbreviation_length_imperialFoot:I = 0x7f120392

.field public static final catalogMeasurementUnitAbbreviation_length_imperialInch:I = 0x7f120393

.field public static final catalogMeasurementUnitAbbreviation_length_imperialMile:I = 0x7f120394

.field public static final catalogMeasurementUnitAbbreviation_length_imperialYard:I = 0x7f120395

.field public static final catalogMeasurementUnitAbbreviation_length_metricCentimeter:I = 0x7f120396

.field public static final catalogMeasurementUnitAbbreviation_length_metricKilometer:I = 0x7f120397

.field public static final catalogMeasurementUnitAbbreviation_length_metricMeter:I = 0x7f120398

.field public static final catalogMeasurementUnitAbbreviation_length_metricMillimeter:I = 0x7f120399

.field public static final catalogMeasurementUnitAbbreviation_time_genericDay:I = 0x7f12039a

.field public static final catalogMeasurementUnitAbbreviation_time_genericHour:I = 0x7f12039b

.field public static final catalogMeasurementUnitAbbreviation_time_genericMillisecond:I = 0x7f12039c

.field public static final catalogMeasurementUnitAbbreviation_time_genericMinute:I = 0x7f12039d

.field public static final catalogMeasurementUnitAbbreviation_time_genericSecond:I = 0x7f12039e

.field public static final catalogMeasurementUnitAbbreviation_volume_genericCup:I = 0x7f12039f

.field public static final catalogMeasurementUnitAbbreviation_volume_genericFluidOunce:I = 0x7f1203a0

.field public static final catalogMeasurementUnitAbbreviation_volume_genericGallon:I = 0x7f1203a1

.field public static final catalogMeasurementUnitAbbreviation_volume_genericPint:I = 0x7f1203a2

.field public static final catalogMeasurementUnitAbbreviation_volume_genericQuart:I = 0x7f1203a3

.field public static final catalogMeasurementUnitAbbreviation_volume_genericShot:I = 0x7f1203a4

.field public static final catalogMeasurementUnitAbbreviation_volume_imperialCubicFoot:I = 0x7f1203a5

.field public static final catalogMeasurementUnitAbbreviation_volume_imperialCubicInch:I = 0x7f1203a6

.field public static final catalogMeasurementUnitAbbreviation_volume_imperialCubicYard:I = 0x7f1203a7

.field public static final catalogMeasurementUnitAbbreviation_volume_metricLiter:I = 0x7f1203a8

.field public static final catalogMeasurementUnitAbbreviation_volume_metricMilliliter:I = 0x7f1203a9

.field public static final catalogMeasurementUnitAbbreviation_weight_imperialPound:I = 0x7f1203aa

.field public static final catalogMeasurementUnitAbbreviation_weight_imperialStone:I = 0x7f1203ab

.field public static final catalogMeasurementUnitAbbreviation_weight_imperialWeightOunce:I = 0x7f1203ac

.field public static final catalogMeasurementUnitAbbreviation_weight_metricGram:I = 0x7f1203ad

.field public static final catalogMeasurementUnitAbbreviation_weight_metricKilogram:I = 0x7f1203ae

.field public static final catalogMeasurementUnitAbbreviation_weight_metricMilligram:I = 0x7f1203af

.field public static final catalogMeasurementUnitName_area_imperialAcre:I = 0x7f1203b0

.field public static final catalogMeasurementUnitName_area_imperialSquareFoot:I = 0x7f1203b1

.field public static final catalogMeasurementUnitName_area_imperialSquareInch:I = 0x7f1203b2

.field public static final catalogMeasurementUnitName_area_imperialSquareMile:I = 0x7f1203b3

.field public static final catalogMeasurementUnitName_area_imperialSquareYard:I = 0x7f1203b4

.field public static final catalogMeasurementUnitName_area_metricSquareCentimeter:I = 0x7f1203b5

.field public static final catalogMeasurementUnitName_area_metricSquareKilometer:I = 0x7f1203b6

.field public static final catalogMeasurementUnitName_area_metricSquareMeter:I = 0x7f1203b7

.field public static final catalogMeasurementUnitName_length_imperialFoot:I = 0x7f1203b8

.field public static final catalogMeasurementUnitName_length_imperialInch:I = 0x7f1203b9

.field public static final catalogMeasurementUnitName_length_imperialMile:I = 0x7f1203ba

.field public static final catalogMeasurementUnitName_length_imperialYard:I = 0x7f1203bb

.field public static final catalogMeasurementUnitName_length_metricCentimeter:I = 0x7f1203bc

.field public static final catalogMeasurementUnitName_length_metricKilometer:I = 0x7f1203bd

.field public static final catalogMeasurementUnitName_length_metricMeter:I = 0x7f1203be

.field public static final catalogMeasurementUnitName_length_metricMillimeter:I = 0x7f1203bf

.field public static final catalogMeasurementUnitName_time_genericDay:I = 0x7f1203c0

.field public static final catalogMeasurementUnitName_time_genericHour:I = 0x7f1203c1

.field public static final catalogMeasurementUnitName_time_genericMillisecond:I = 0x7f1203c2

.field public static final catalogMeasurementUnitName_time_genericMinute:I = 0x7f1203c3

.field public static final catalogMeasurementUnitName_time_genericSecond:I = 0x7f1203c4

.field public static final catalogMeasurementUnitName_volume_genericCup:I = 0x7f1203c5

.field public static final catalogMeasurementUnitName_volume_genericFluidOunce:I = 0x7f1203c6

.field public static final catalogMeasurementUnitName_volume_genericGallon:I = 0x7f1203c7

.field public static final catalogMeasurementUnitName_volume_genericPint:I = 0x7f1203c8

.field public static final catalogMeasurementUnitName_volume_genericQuart:I = 0x7f1203c9

.field public static final catalogMeasurementUnitName_volume_genericShot:I = 0x7f1203ca

.field public static final catalogMeasurementUnitName_volume_imperialCubicFoot:I = 0x7f1203cb

.field public static final catalogMeasurementUnitName_volume_imperialCubicInch:I = 0x7f1203cc

.field public static final catalogMeasurementUnitName_volume_imperialCubicYard:I = 0x7f1203cd

.field public static final catalogMeasurementUnitName_volume_metricLiter:I = 0x7f1203ce

.field public static final catalogMeasurementUnitName_volume_metricMilliliter:I = 0x7f1203cf

.field public static final catalogMeasurementUnitName_weight_imperialPound:I = 0x7f1203d0

.field public static final catalogMeasurementUnitName_weight_imperialStone:I = 0x7f1203d1

.field public static final catalogMeasurementUnitName_weight_imperialWeightOunce:I = 0x7f1203d2

.field public static final catalogMeasurementUnitName_weight_metricGram:I = 0x7f1203d3

.field public static final catalogMeasurementUnitName_weight_metricKilogram:I = 0x7f1203d4

.field public static final catalogMeasurementUnitName_weight_metricMilligram:I = 0x7f1203d5

.field public static final categories_hint_url:I = 0x7f1203d6

.field public static final categories_many:I = 0x7f1203d7

.field public static final category_create:I = 0x7f1203d8

.field public static final category_delete_button:I = 0x7f1203d9

.field public static final category_delete_message:I = 0x7f1203da

.field public static final category_label_with_name:I = 0x7f1203db

.field public static final category_name_required_warning_message:I = 0x7f1203dc

.field public static final category_name_required_warning_title:I = 0x7f1203dd

.field public static final category_new_hint:I = 0x7f1203de

.field public static final category_one:I = 0x7f1203df

.field public static final category_row_delete_content_description:I = 0x7f1203e0

.field public static final cc:I = 0x7f1203e1

.field public static final ce_card_number_hint:I = 0x7f1203e2

.field public static final ce_card_number_hint_focused:I = 0x7f1203e3

.field public static final ce_card_number_hint_with_dip:I = 0x7f1203e4

.field public static final ce_card_number_hint_with_dip_and_tap:I = 0x7f1203e5

.field public static final ce_card_number_hint_with_only_dip_and_tap:I = 0x7f1203e6

.field public static final ce_card_number_hint_with_only_swipe_and_dip:I = 0x7f1203e7

.field public static final ce_card_number_last_four_content_description:I = 0x7f1203e8

.field public static final ce_card_number_only_hint:I = 0x7f1203e9

.field public static final ce_save_card_to_customer_footer:I = 0x7f1203ea

.field public static final ce_save_card_to_customer_title:I = 0x7f1203eb

.field public static final change_account:I = 0x7f1203ec

.field public static final change_bank_account:I = 0x7f1203ed

.field public static final change_bank_account_url:I = 0x7f1203ee

.field public static final change_hud_amount_change:I = 0x7f1203ef

.field public static final change_hud_no_change:I = 0x7f1203f0

.field public static final change_hud_out_of_total:I = 0x7f1203f1

.field public static final change_hud_payment_complete:I = 0x7f1203f2

.field public static final character_counter_content_description:I = 0x7f1203f3

.field public static final character_counter_overflowed_content_description:I = 0x7f1203f4

.field public static final character_counter_pattern:I = 0x7f1203f5

.field public static final charge:I = 0x7f1203f6

.field public static final charge_amount:I = 0x7f1203f7

.field public static final charge_amount_confirm:I = 0x7f1203f8

.field public static final charge_amount_including_tax:I = 0x7f1203f9

.field public static final charge_amount_now:I = 0x7f1203fa

.field public static final charge_amount_now_description:I = 0x7f1203fb

.field public static final charge_now:I = 0x7f1203fc

.field public static final charge_now_disabled_description:I = 0x7f1203fd

.field public static final check:I = 0x7f1203fe

.field public static final check_compatibility_url:I = 0x7f1203ff

.field public static final check_your_inbox:I = 0x7f120400

.field public static final check_your_inbox_message:I = 0x7f120401

.field public static final checking:I = 0x7f120402

.field public static final checkout_applet_name:I = 0x7f120403

.field public static final checkout_flow_email_receipt_disclaimer_au_japan:I = 0x7f120404

.field public static final checkout_flow_email_receipt_disclaimer_canada:I = 0x7f120405

.field public static final checkout_flow_email_receipt_disclaimer_default:I = 0x7f120406

.field public static final checkout_flow_email_receipt_disclaimer_uk:I = 0x7f120407

.field public static final checkout_flow_email_receipt_disclaimer_us:I = 0x7f120408

.field public static final checkout_flow_receipt_disclaimer:I = 0x7f120409

.field public static final checkout_flow_sms_receipt_disclaimer_au_japan:I = 0x7f12040a

.field public static final checkout_flow_sms_receipt_disclaimer_canada:I = 0x7f12040b

.field public static final checkout_flow_sms_receipt_disclaimer_default:I = 0x7f12040c

.field public static final checkout_flow_sms_receipt_disclaimer_uk:I = 0x7f12040d

.field public static final checkout_flow_sms_receipt_disclaimer_us:I = 0x7f12040e

.field public static final checkout_link_screen_deactivated_msg:I = 0x7f12040f

.field public static final checkout_link_settings:I = 0x7f120410

.field public static final checkout_link_settings_help_text:I = 0x7f120411

.field public static final checkout_link_settings_to_checkout_links_url:I = 0x7f120412

.field public static final checkout_link_share_help_text:I = 0x7f120413

.field public static final checkout_link_share_subject:I = 0x7f120414

.field public static final checkout_link_staging_with_item_id:I = 0x7f120415

.field public static final checkout_link_toggle_label:I = 0x7f120416

.field public static final checkout_link_with_item_id:I = 0x7f120417

.field public static final checkoutlink_clipboard_label:I = 0x7f120418

.field public static final checkoutlink_copied_toast_message:I = 0x7f120419

.field public static final chip_card_reader_tutorial:I = 0x7f12041a

.field public static final chip_card_reader_tutorial_subtext:I = 0x7f12041b

.field public static final chip_text:I = 0x7f12041c

.field public static final choose_a_category:I = 0x7f12041d

.field public static final choose_account_type:I = 0x7f12041e

.field public static final choose_contact_for_reward_title:I = 0x7f12041f

.field public static final choose_photo:I = 0x7f120420

.field public static final city_hint:I = 0x7f120421

.field public static final clear:I = 0x7f120422

.field public static final clear_card:I = 0x7f120423

.field public static final clear_items:I = 0x7f120424

.field public static final clear_photo:I = 0x7f120425

.field public static final clear_sale:I = 0x7f120426

.field public static final clear_text_box_content_description:I = 0x7f120427

.field public static final clear_text_end_icon_content_description:I = 0x7f120428

.field public static final clear_uppercase:I = 0x7f120429

.field public static final clock_skew_error_button:I = 0x7f12042a

.field public static final clock_skew_error_message:I = 0x7f12042b

.field public static final clock_skew_error_title:I = 0x7f12042c

.field public static final close:I = 0x7f12042d

.field public static final close_button_content_description:I = 0x7f12042e

.field public static final close_content_description:I = 0x7f12042f

.field public static final cnp_invalid_credit_card_message:I = 0x7f120430

.field public static final cnp_invalid_credit_card_title:I = 0x7f120431

.field public static final cnp_invalid_gift_card_message:I = 0x7f120432

.field public static final cnp_invalid_gift_card_title:I = 0x7f120433

.field public static final collect_payments:I = 0x7f120434

.field public static final collected_by:I = 0x7f120435

.field public static final color_blue:I = 0x7f120436

.field public static final color_brown:I = 0x7f120437

.field public static final color_cyan:I = 0x7f120438

.field public static final color_gray:I = 0x7f120439

.field public static final color_green:I = 0x7f12043a

.field public static final color_light_green:I = 0x7f12043b

.field public static final color_pink:I = 0x7f12043c

.field public static final color_purple:I = 0x7f12043d

.field public static final color_red:I = 0x7f12043e

.field public static final color_yellow:I = 0x7f12043f

.field public static final combined_name_and_display_name:I = 0x7f120440

.field public static final combined_rate_format:I = 0x7f120441

.field public static final common_google_play_services_enable_button:I = 0x7f120442

.field public static final common_google_play_services_enable_text:I = 0x7f120443

.field public static final common_google_play_services_enable_title:I = 0x7f120444

.field public static final common_google_play_services_install_button:I = 0x7f120445

.field public static final common_google_play_services_install_text:I = 0x7f120446

.field public static final common_google_play_services_install_title:I = 0x7f120447

.field public static final common_google_play_services_notification_channel_name:I = 0x7f120448

.field public static final common_google_play_services_notification_ticker:I = 0x7f120449

.field public static final common_google_play_services_unknown_issue:I = 0x7f12044a

.field public static final common_google_play_services_unsupported_text:I = 0x7f12044b

.field public static final common_google_play_services_update_button:I = 0x7f12044c

.field public static final common_google_play_services_update_text:I = 0x7f12044d

.field public static final common_google_play_services_update_title:I = 0x7f12044e

.field public static final common_google_play_services_updating_text:I = 0x7f12044f

.field public static final common_google_play_services_wear_update_text:I = 0x7f120450

.field public static final common_open_on_phone:I = 0x7f120451

.field public static final common_signin_button_text:I = 0x7f120452

.field public static final common_signin_button_text_long:I = 0x7f120453

.field public static final community_reward_browser_url:I = 0x7f120454

.field public static final community_reward_description:I = 0x7f120455

.field public static final community_reward_description_empty:I = 0x7f120456

.field public static final community_reward_description_link:I = 0x7f120457

.field public static final community_reward_label_amount:I = 0x7f120458

.field public static final community_reward_label_instant_discount:I = 0x7f120459

.field public static final community_reward_label_total:I = 0x7f12045a

.field public static final community_reward_title:I = 0x7f12045b

.field public static final comp_initial:I = 0x7f12045c

.field public static final comp_item:I = 0x7f12045d

.field public static final comp_reason_attributed:I = 0x7f12045e

.field public static final comp_reason_default:I = 0x7f12045f

.field public static final comp_ticket:I = 0x7f120460

.field public static final comp_ticket_help_text:I = 0x7f120461

.field public static final comp_uppercase_reason:I = 0x7f120462

.field public static final compact_money_format_billion:I = 0x7f120463

.field public static final compact_money_format_million:I = 0x7f120464

.field public static final compact_money_format_thousand:I = 0x7f120465

.field public static final compact_number_format_billion:I = 0x7f120466

.field public static final compact_number_format_million:I = 0x7f120467

.field public static final compact_number_format_myllion:I = 0x7f120468

.field public static final compact_number_format_myriad:I = 0x7f120469

.field public static final compact_number_format_myriad_myllion:I = 0x7f12046a

.field public static final compact_number_format_thousand:I = 0x7f12046b

.field public static final compact_number_unformatted:I = 0x7f12046c

.field public static final compare_report_label:I = 0x7f12046d

.field public static final comped_with_reason:I = 0x7f12046e

.field public static final complete_payment_in_progress_message:I = 0x7f12046f

.field public static final complete_payment_in_progress_title:I = 0x7f120470

.field public static final complete_signup_notification_content:I = 0x7f120471

.field public static final complete_signup_notification_title:I = 0x7f120472

.field public static final conditional_taxes_help_text_for_cart:I = 0x7f120473

.field public static final conditional_taxes_help_text_for_taxes_settings:I = 0x7f120474

.field public static final configure_item_detail_override_price_title:I = 0x7f120475

.field public static final configure_item_option_section_header:I = 0x7f120476

.field public static final configure_item_price_required_dialog_message:I = 0x7f120477

.field public static final configure_item_price_required_dialog_title:I = 0x7f120478

.field public static final configure_item_summary:I = 0x7f120479

.field public static final configure_item_summary_quantity:I = 0x7f12047a

.field public static final configure_item_summary_quantity_only:I = 0x7f12047b

.field public static final confirm:I = 0x7f12047c

.field public static final confirm_bank_account_failed:I = 0x7f12047d

.field public static final confirm_collect_cash_message:I = 0x7f12047e

.field public static final confirm_collect_cash_title:I = 0x7f12047f

.field public static final confirm_email:I = 0x7f120480

.field public static final confirm_identity_heading:I = 0x7f120481

.field public static final confirm_identity_subheading:I = 0x7f120482

.field public static final confirm_instant_transfer:I = 0x7f120483

.field public static final confirm_save:I = 0x7f120484

.field public static final confirm_transfer_button:I = 0x7f120485

.field public static final confirm_transfer_title:I = 0x7f120486

.field public static final confirmation_popup_discard:I = 0x7f120487

.field public static final confirmation_popup_resume:I = 0x7f120488

.field public static final connect_reader_offline_instruction:I = 0x7f120489

.field public static final connect_scale_using_usb_footer:I = 0x7f12048a

.field public static final connect_scale_using_usb_to_continue_label:I = 0x7f12048b

.field public static final connected_scales_limit_reached_warning_button_title:I = 0x7f12048c

.field public static final connected_scales_limit_reached_warning_message:I = 0x7f12048d

.field public static final connected_scales_limit_reached_warning_title:I = 0x7f12048e

.field public static final connection_error_message:I = 0x7f12048f

.field public static final connection_error_msg:I = 0x7f120490

.field public static final connection_error_title:I = 0x7f120491

.field public static final connection_error_try_again:I = 0x7f120492

.field public static final connection_type_and_weight_unit_pattern:I = 0x7f120493

.field public static final connection_type_bluetooth:I = 0x7f120494

.field public static final connection_type_usb:I = 0x7f120495

.field public static final contact:I = 0x7f120496

.field public static final contact_square_support:I = 0x7f120497

.field public static final contact_support_reapply:I = 0x7f120498

.field public static final contactless:I = 0x7f120499

.field public static final contactless_action_required_message:I = 0x7f12049a

.field public static final contactless_action_required_title:I = 0x7f12049b

.field public static final contactless_card_declined_message:I = 0x7f12049c

.field public static final contactless_card_declined_title:I = 0x7f12049d

.field public static final contactless_chip_card_insertion_required_description:I = 0x7f12049e

.field public static final contactless_chip_card_insertion_required_title:I = 0x7f12049f

.field public static final contactless_interface_unavailable_message:I = 0x7f1204a0

.field public static final contactless_interface_unavailable_title:I = 0x7f1204a1

.field public static final contactless_limit_exceeded_insert_card_message:I = 0x7f1204a2

.field public static final contactless_limit_exceeded_insert_card_title:I = 0x7f1204a3

.field public static final contactless_limit_exceeded_try_another_card_message:I = 0x7f1204a4

.field public static final contactless_limit_exceeded_try_another_card_title:I = 0x7f1204a5

.field public static final contactless_one_card_message:I = 0x7f1204a6

.field public static final contactless_one_card_title:I = 0x7f1204a7

.field public static final contactless_payment_methods_not_ready:I = 0x7f1204a8

.field public static final contactless_payment_methods_ready:I = 0x7f1204a9

.field public static final contactless_payment_methods_ready_show_contactless_row:I = 0x7f1204aa

.field public static final contactless_plus_chip_reader_tutorial:I = 0x7f1204ab

.field public static final contactless_plus_chip_reader_tutorial_subtext:I = 0x7f1204ac

.field public static final contactless_reader_disconnected_title:I = 0x7f1204ad

.field public static final contactless_reader_required:I = 0x7f1204ae

.field public static final contactless_ready_message:I = 0x7f1204af

.field public static final contactless_ready_title:I = 0x7f1204b0

.field public static final contactless_ready_to_tap:I = 0x7f1204b1

.field public static final contactless_tap_again_message:I = 0x7f1204b2

.field public static final contactless_tap_again_title:I = 0x7f1204b3

.field public static final contactless_too_many_taps_message:I = 0x7f1204b4

.field public static final contactless_unable_to_process_message:I = 0x7f1204b5

.field public static final contactless_unable_to_process_refund_message:I = 0x7f1204b6

.field public static final contactless_unable_to_process_title:I = 0x7f1204b7

.field public static final contactless_unlock_phone_and_try_again_message:I = 0x7f1204b8

.field public static final contactless_unlock_phone_to_pay_title:I = 0x7f1204b9

.field public static final contactless_unlock_phone_to_search_title:I = 0x7f1204ba

.field public static final contactless_waiting_for_tap_message:I = 0x7f1204bb

.field public static final contactless_waiting_for_tap_title:I = 0x7f1204bc

.field public static final content_description_back:I = 0x7f1204bd

.field public static final content_description_clear_search:I = 0x7f1204be

.field public static final content_description_close:I = 0x7f1204bf

.field public static final content_description_date_of_birth:I = 0x7f1204c0

.field public static final content_description_drawer:I = 0x7f1204c1

.field public static final content_description_enter_email:I = 0x7f1204c2

.field public static final content_description_enter_email_confirm:I = 0x7f1204c3

.field public static final content_description_enter_first_name:I = 0x7f1204c4

.field public static final content_description_enter_full_ssn:I = 0x7f1204c5

.field public static final content_description_enter_last_four_ssn:I = 0x7f1204c6

.field public static final content_description_enter_last_name:I = 0x7f1204c7

.field public static final content_description_enter_name:I = 0x7f1204c8

.field public static final content_description_enter_phone_number:I = 0x7f1204c9

.field public static final content_description_navigate_burger_button:I = 0x7f1204ca

.field public static final content_description_navigate_up_button:I = 0x7f1204cb

.field public static final content_description_navigate_x_button:I = 0x7f1204cc

.field public static final content_description_search_invoices:I = 0x7f1204cd

.field public static final content_description_search_items:I = 0x7f1204ce

.field public static final continue_label:I = 0x7f1204cf

.field public static final continue_to_enter_pin:I = 0x7f1204d0

.field public static final continue_verification:I = 0x7f1204d1

.field public static final conversational_mode_add:I = 0x7f1204d2

.field public static final conversational_mode_allergy:I = 0x7f1204d3

.field public static final conversational_mode_extra:I = 0x7f1204d4

.field public static final conversational_mode_no:I = 0x7f1204d5

.field public static final conversational_mode_side:I = 0x7f1204d6

.field public static final conversational_mode_sub:I = 0x7f1204d7

.field public static final conversational_mode_unknown:I = 0x7f1204d8

.field public static final convert_items:I = 0x7f1204d9

.field public static final could_not_enable_same_day_deposit:I = 0x7f1204da

.field public static final could_not_enable_same_day_deposit_message:I = 0x7f1204db

.field public static final count_of_affected_variations_alert_confirm_button_text_for_deletion:I = 0x7f1204dc

.field public static final count_of_affected_variations_alert_confirm_button_text_for_editing:I = 0x7f1204dd

.field public static final count_of_affected_variations_alert_message_for_deletion_plural:I = 0x7f1204de

.field public static final count_of_affected_variations_alert_message_for_deletion_singular:I = 0x7f1204df

.field public static final count_of_affected_variations_alert_message_for_editing_plural:I = 0x7f1204e0

.field public static final count_of_affected_variations_alert_message_for_editing_singular:I = 0x7f1204e1

.field public static final count_of_affected_variations_alert_title_for_deletion:I = 0x7f1204e2

.field public static final count_of_affected_variations_alert_title_for_editing:I = 0x7f1204e3

.field public static final counter_view_minus_icon_description:I = 0x7f1204e4

.field public static final counter_view_plus_icon_description:I = 0x7f1204e5

.field public static final country_albania:I = 0x7f1204e6

.field public static final country_algeria:I = 0x7f1204e7

.field public static final country_americansamoa:I = 0x7f1204e8

.field public static final country_angola:I = 0x7f1204e9

.field public static final country_anguilla:I = 0x7f1204ea

.field public static final country_antiguaandbarbuda:I = 0x7f1204eb

.field public static final country_argentina:I = 0x7f1204ec

.field public static final country_armenia:I = 0x7f1204ed

.field public static final country_aruba:I = 0x7f1204ee

.field public static final country_australia:I = 0x7f1204ef

.field public static final country_austria:I = 0x7f1204f0

.field public static final country_azerbaijan:I = 0x7f1204f1

.field public static final country_bahamas:I = 0x7f1204f2

.field public static final country_bahrain:I = 0x7f1204f3

.field public static final country_bangladesh:I = 0x7f1204f4

.field public static final country_barbados:I = 0x7f1204f5

.field public static final country_belarus:I = 0x7f1204f6

.field public static final country_belgium:I = 0x7f1204f7

.field public static final country_belize:I = 0x7f1204f8

.field public static final country_benin:I = 0x7f1204f9

.field public static final country_bermuda:I = 0x7f1204fa

.field public static final country_bhutan:I = 0x7f1204fb

.field public static final country_bolivia_plurinationalstateof:I = 0x7f1204fc

.field public static final country_bosniaandherzegovina:I = 0x7f1204fd

.field public static final country_botswana:I = 0x7f1204fe

.field public static final country_brazil:I = 0x7f1204ff

.field public static final country_bruneidarussalam:I = 0x7f120500

.field public static final country_bulgaria:I = 0x7f120501

.field public static final country_burkinafaso:I = 0x7f120502

.field public static final country_caboverde:I = 0x7f120503

.field public static final country_cambodia:I = 0x7f120504

.field public static final country_cameroon:I = 0x7f120505

.field public static final country_canada:I = 0x7f120506

.field public static final country_caymanislands:I = 0x7f120507

.field public static final country_chad:I = 0x7f120508

.field public static final country_chile:I = 0x7f120509

.field public static final country_china:I = 0x7f12050a

.field public static final country_colombia:I = 0x7f12050b

.field public static final country_congo:I = 0x7f12050c

.field public static final country_costarica:I = 0x7f12050d

.field public static final country_coted_ivoire:I = 0x7f12050e

.field public static final country_croatia:I = 0x7f12050f

.field public static final country_cyprus:I = 0x7f120510

.field public static final country_czechrepublic:I = 0x7f120511

.field public static final country_denmark:I = 0x7f120512

.field public static final country_dominica:I = 0x7f120513

.field public static final country_dominicanrepublic:I = 0x7f120514

.field public static final country_ecuador:I = 0x7f120515

.field public static final country_egypt:I = 0x7f120516

.field public static final country_elsalvador:I = 0x7f120517

.field public static final country_estonia:I = 0x7f120518

.field public static final country_fiji:I = 0x7f120519

.field public static final country_finland:I = 0x7f12051a

.field public static final country_france:I = 0x7f12051b

.field public static final country_gabon:I = 0x7f12051c

.field public static final country_gambia:I = 0x7f12051d

.field public static final country_germany:I = 0x7f12051e

.field public static final country_ghana:I = 0x7f12051f

.field public static final country_greece:I = 0x7f120520

.field public static final country_grenada:I = 0x7f120521

.field public static final country_guam:I = 0x7f120522

.field public static final country_guatemala:I = 0x7f120523

.field public static final country_guinea_bissau:I = 0x7f120524

.field public static final country_guyana:I = 0x7f120525

.field public static final country_haiti:I = 0x7f120526

.field public static final country_honduras:I = 0x7f120527

.field public static final country_hongkong:I = 0x7f120528

.field public static final country_hungary:I = 0x7f120529

.field public static final country_iceland:I = 0x7f12052a

.field public static final country_india:I = 0x7f12052b

.field public static final country_indonesia:I = 0x7f12052c

.field public static final country_ireland:I = 0x7f12052d

.field public static final country_israel:I = 0x7f12052e

.field public static final country_italy:I = 0x7f12052f

.field public static final country_jamaica:I = 0x7f120530

.field public static final country_japan:I = 0x7f120531

.field public static final country_jordan:I = 0x7f120532

.field public static final country_kazakhstan:I = 0x7f120533

.field public static final country_kenya:I = 0x7f120534

.field public static final country_korea_republicof:I = 0x7f120535

.field public static final country_kuwait:I = 0x7f120536

.field public static final country_kyrgyzstan:I = 0x7f120537

.field public static final country_laopeople_sdemocraticrepublic:I = 0x7f120538

.field public static final country_latvia:I = 0x7f120539

.field public static final country_lebanon:I = 0x7f12053a

.field public static final country_lesotho:I = 0x7f12053b

.field public static final country_liberia:I = 0x7f12053c

.field public static final country_libya:I = 0x7f12053d

.field public static final country_liechtenstein:I = 0x7f12053e

.field public static final country_lithuania:I = 0x7f12053f

.field public static final country_luxembourg:I = 0x7f120540

.field public static final country_macao:I = 0x7f120541

.field public static final country_madagascar:I = 0x7f120542

.field public static final country_malawi:I = 0x7f120543

.field public static final country_malaysia:I = 0x7f120544

.field public static final country_mali:I = 0x7f120545

.field public static final country_malta:I = 0x7f120546

.field public static final country_mauritania:I = 0x7f120547

.field public static final country_mauritius:I = 0x7f120548

.field public static final country_mexico:I = 0x7f120549

.field public static final country_micronesia_federatedstatesof:I = 0x7f12054a

.field public static final country_moldova_republicof:I = 0x7f12054b

.field public static final country_mongolia:I = 0x7f12054c

.field public static final country_montserrat:I = 0x7f12054d

.field public static final country_morocco:I = 0x7f12054e

.field public static final country_mozambique:I = 0x7f12054f

.field public static final country_myanmar:I = 0x7f120550

.field public static final country_namibia:I = 0x7f120551

.field public static final country_nepal:I = 0x7f120552

.field public static final country_netherlands:I = 0x7f120553

.field public static final country_newzealand:I = 0x7f120554

.field public static final country_nicaragua:I = 0x7f120555

.field public static final country_niger:I = 0x7f120556

.field public static final country_nigeria:I = 0x7f120557

.field public static final country_northernmarianaislands:I = 0x7f120558

.field public static final country_northmacedonia_therepublicof:I = 0x7f120559

.field public static final country_norway:I = 0x7f12055a

.field public static final country_oman:I = 0x7f12055b

.field public static final country_pakistan:I = 0x7f12055c

.field public static final country_palau:I = 0x7f12055d

.field public static final country_panama:I = 0x7f12055e

.field public static final country_papuanewguinea:I = 0x7f12055f

.field public static final country_paraguay:I = 0x7f120560

.field public static final country_peru:I = 0x7f120561

.field public static final country_philippines:I = 0x7f120562

.field public static final country_poland:I = 0x7f120563

.field public static final country_portugal:I = 0x7f120564

.field public static final country_puertorico:I = 0x7f120565

.field public static final country_qatar:I = 0x7f120566

.field public static final country_romania:I = 0x7f120567

.field public static final country_russianfederation:I = 0x7f120568

.field public static final country_rwanda:I = 0x7f120569

.field public static final country_saintkittsandnevis:I = 0x7f12056a

.field public static final country_saintlucia:I = 0x7f12056b

.field public static final country_saintvincentandthegrenadines:I = 0x7f12056c

.field public static final country_saotomeandprincipe:I = 0x7f12056d

.field public static final country_saudiarabia:I = 0x7f12056e

.field public static final country_selection_prompt_message:I = 0x7f12056f

.field public static final country_selection_prompt_message_no_payments:I = 0x7f120570

.field public static final country_selection_prompt_title:I = 0x7f120571

.field public static final country_senegal:I = 0x7f120572

.field public static final country_serbia:I = 0x7f120573

.field public static final country_seychelles:I = 0x7f120574

.field public static final country_sierraleone:I = 0x7f120575

.field public static final country_singapore:I = 0x7f120576

.field public static final country_slovakia:I = 0x7f120577

.field public static final country_slovenia:I = 0x7f120578

.field public static final country_solomonislands:I = 0x7f120579

.field public static final country_southafrica:I = 0x7f12057a

.field public static final country_spain:I = 0x7f12057b

.field public static final country_srilanka:I = 0x7f12057c

.field public static final country_suriname:I = 0x7f12057d

.field public static final country_swaziland:I = 0x7f12057e

.field public static final country_sweden:I = 0x7f12057f

.field public static final country_switzerland:I = 0x7f120580

.field public static final country_taiwan_provinceofchina:I = 0x7f120581

.field public static final country_tajikistan:I = 0x7f120582

.field public static final country_tanzania_unitedrepublicof:I = 0x7f120583

.field public static final country_thailand:I = 0x7f120584

.field public static final country_togo:I = 0x7f120585

.field public static final country_tonga:I = 0x7f120586

.field public static final country_trinidadandtobago:I = 0x7f120587

.field public static final country_tunisia:I = 0x7f120588

.field public static final country_turkey:I = 0x7f120589

.field public static final country_turkmenistan:I = 0x7f12058a

.field public static final country_turksandcaicosislands:I = 0x7f12058b

.field public static final country_tuvalu:I = 0x7f12058c

.field public static final country_uganda:I = 0x7f12058d

.field public static final country_ukraine:I = 0x7f12058e

.field public static final country_unitedarabemirates:I = 0x7f12058f

.field public static final country_unitedkingdom:I = 0x7f120590

.field public static final country_unitedstates:I = 0x7f120591

.field public static final country_uruguay:I = 0x7f120592

.field public static final country_uzbekistan:I = 0x7f120593

.field public static final country_venezuela_bolivarianrepublicof:I = 0x7f120594

.field public static final country_vietnam:I = 0x7f120595

.field public static final country_virginislands_british:I = 0x7f120596

.field public static final country_virginislands_us:I = 0x7f120597

.field public static final country_yemen:I = 0x7f120598

.field public static final country_zambia:I = 0x7f120599

.field public static final country_zimbabwe:I = 0x7f12059a

.field public static final county_hint:I = 0x7f12059b

.field public static final coupon_applied:I = 0x7f12059c

.field public static final coupon_discount_description_format_amount:I = 0x7f12059d

.field public static final coupon_discount_description_format_percentage:I = 0x7f12059e

.field public static final coupon_discount_description_format_percentage_and_amount:I = 0x7f12059f

.field public static final coupon_free_item:I = 0x7f1205a0

.field public static final coupon_name_cart_scope_format_amount:I = 0x7f1205a1

.field public static final coupon_name_cart_scope_format_percentage:I = 0x7f1205a2

.field public static final coupon_name_cart_scope_format_percentage_and_amount:I = 0x7f1205a3

.field public static final coupon_name_item_scope_format_amount:I = 0x7f1205a4

.field public static final coupon_name_item_scope_format_percentage:I = 0x7f1205a5

.field public static final coupon_name_item_scope_format_percentage_and_amount:I = 0x7f1205a6

.field public static final coupon_post_auth_title:I = 0x7f1205a7

.field public static final coupon_redeem_reward:I = 0x7f1205a8

.field public static final coupon_redeem_reward_subtitle:I = 0x7f1205a9

.field public static final coupon_redeem_rewards:I = 0x7f1205aa

.field public static final coupon_search_by_code:I = 0x7f1205ab

.field public static final coupon_search_by_code_hint:I = 0x7f1205ac

.field public static final coupon_search_by_code_hint_alphanumeric:I = 0x7f1205ad

.field public static final coupon_search_failed:I = 0x7f1205ae

.field public static final coupon_search_reward_already_in_cart_title:I = 0x7f1205af

.field public static final coupon_search_reward_already_redeemed_title:I = 0x7f1205b0

.field public static final coupon_search_reward_conflicting_msg:I = 0x7f1205b1

.field public static final coupon_search_reward_conflicting_title:I = 0x7f1205b2

.field public static final coupon_search_reward_expired_title:I = 0x7f1205b3

.field public static final coupon_search_reward_not_found:I = 0x7f1205b4

.field public static final coupon_search_reward_not_found_helper:I = 0x7f1205b5

.field public static final coupon_search_reward_not_found_subtitle:I = 0x7f1205b6

.field public static final coupon_search_reward_not_found_title:I = 0x7f1205b7

.field public static final coupon_search_try_again:I = 0x7f1205b8

.field public static final coupon_use_later:I = 0x7f1205b9

.field public static final cover_count_plural:I = 0x7f1205ba

.field public static final cover_count_singular:I = 0x7f1205bb

.field public static final create:I = 0x7f1205bc

.field public static final create_discount:I = 0x7f1205bd

.field public static final create_invoice:I = 0x7f1205be

.field public static final create_item:I = 0x7f1205bf

.field public static final create_item_tutorial:I = 0x7f1205c0

.field public static final create_item_tutorial_adjust_inventory:I = 0x7f1205c1

.field public static final create_item_tutorial_complete_dialog_button_primary:I = 0x7f1205c2

.field public static final create_item_tutorial_complete_dialog_button_secondary:I = 0x7f1205c3

.field public static final create_item_tutorial_complete_dialog_content:I = 0x7f1205c4

.field public static final create_item_tutorial_complete_dialog_title:I = 0x7f1205c5

.field public static final create_item_tutorial_create_category:I = 0x7f1205c6

.field public static final create_item_tutorial_enter_item_name:I = 0x7f1205c7

.field public static final create_item_tutorial_enter_item_price:I = 0x7f1205c8

.field public static final create_item_tutorial_phone_select_all_items:I = 0x7f1205c9

.field public static final create_item_tutorial_phone_select_create_item:I = 0x7f1205ca

.field public static final create_item_tutorial_phone_select_items:I = 0x7f1205cb

.field public static final create_item_tutorial_save_item:I = 0x7f1205cc

.field public static final create_item_tutorial_skip_tutorial_dialog_button_primary:I = 0x7f1205cd

.field public static final create_item_tutorial_skip_tutorial_dialog_button_secondary:I = 0x7f1205ce

.field public static final create_item_tutorial_skip_tutorial_dialog_content:I = 0x7f1205cf

.field public static final create_item_tutorial_skip_tutorial_dialog_title:I = 0x7f1205d0

.field public static final create_item_tutorial_start_tutorial_dialog_button_primary:I = 0x7f1205d1

.field public static final create_item_tutorial_start_tutorial_dialog_button_secondary:I = 0x7f1205d2

.field public static final create_item_tutorial_start_tutorial_dialog_content:I = 0x7f1205d3

.field public static final create_item_tutorial_start_tutorial_dialog_title:I = 0x7f1205d4

.field public static final create_item_tutorial_subtext:I = 0x7f1205d5

.field public static final create_item_tutorial_tablet_drag_and_drop_item:I = 0x7f1205d6

.field public static final create_item_tutorial_tablet_enter_edit_mode_tooltip:I = 0x7f1205d7

.field public static final create_item_tutorial_tablet_select_create_item:I = 0x7f1205d8

.field public static final create_item_tutorial_tablet_tap_done_editing:I = 0x7f1205d9

.field public static final create_item_tutorial_tablet_tap_empty_tile_tooltip:I = 0x7f1205da

.field public static final create_link_already_exists_error_msg:I = 0x7f1205db

.field public static final create_link_donation_help_text:I = 0x7f1205dc

.field public static final create_link_error_msg:I = 0x7f1205dd

.field public static final create_link_help_text:I = 0x7f1205de

.field public static final create_link_name_hint:I = 0x7f1205df

.field public static final create_link_name_label:I = 0x7f1205e0

.field public static final create_link_price_error_msg:I = 0x7f1205e1

.field public static final create_link_price_label:I = 0x7f1205e2

.field public static final create_links_help_text_body:I = 0x7f1205e3

.field public static final create_links_help_text_title:I = 0x7f1205e4

.field public static final create_modifier_set:I = 0x7f1205e5

.field public static final create_new_dialog_create_discount:I = 0x7f1205e6

.field public static final create_new_dialog_create_item:I = 0x7f1205e7

.field public static final create_pin_label:I = 0x7f1205e8

.field public static final create_recurring_series:I = 0x7f1205e9

.field public static final create_service:I = 0x7f1205ea

.field public static final create_ticket_group:I = 0x7f1205eb

.field public static final create_unit_button_text:I = 0x7f1205ec

.field public static final create_unit_failed_alert_message_due_to_network:I = 0x7f1205ed

.field public static final create_unit_failed_alert_message_not_due_to_network:I = 0x7f1205ee

.field public static final create_unit_failed_alert_title:I = 0x7f1205ef

.field public static final credit_card:I = 0x7f1205f0

.field public static final credit_debit:I = 0x7f1205f1

.field public static final crm_activity_list_header_uppercase_v2:I = 0x7f1205f2

.field public static final crm_activity_list_header_v2:I = 0x7f1205f3

.field public static final crm_activity_list_view_all_v2:I = 0x7f1205f4

.field public static final crm_activity_summary_customer_since:I = 0x7f1205f5

.field public static final crm_activity_summary_last_visited:I = 0x7f1205f6

.field public static final crm_activity_summary_title_avg_spend:I = 0x7f1205f7

.field public static final crm_activity_summary_visit_frequency:I = 0x7f1205f8

.field public static final crm_add_coupon:I = 0x7f1205f9

.field public static final crm_add_coupon_amount:I = 0x7f1205fa

.field public static final crm_add_coupon_disclaimer:I = 0x7f1205fb

.field public static final crm_add_customer_email_address_validation_error:I = 0x7f1205fc

.field public static final crm_add_customer_first_name_validation_error:I = 0x7f1205fd

.field public static final crm_add_customer_last_name_validation_error:I = 0x7f1205fe

.field public static final crm_add_customer_title:I = 0x7f1205ff

.field public static final crm_add_customer_to_invoice_title:I = 0x7f120600

.field public static final crm_add_customer_to_manual_group:I = 0x7f120601

.field public static final crm_add_filter_label:I = 0x7f120602

.field public static final crm_add_from_address_book:I = 0x7f120603

.field public static final crm_add_note_hint:I = 0x7f120604

.field public static final crm_add_note_label:I = 0x7f120605

.field public static final crm_add_to_manual_group:I = 0x7f120606

.field public static final crm_add_to_manual_group_another:I = 0x7f120607

.field public static final crm_add_to_sale:I = 0x7f120608

.field public static final crm_added_customers_to_group_many_format:I = 0x7f120609

.field public static final crm_added_customers_to_group_one_format:I = 0x7f12060a

.field public static final crm_adding_customers_to_group_error:I = 0x7f12060b

.field public static final crm_adding_customers_to_group_many_format:I = 0x7f12060c

.field public static final crm_adding_customers_to_group_one_format:I = 0x7f12060d

.field public static final crm_additional_info_title:I = 0x7f12060e

.field public static final crm_address_header:I = 0x7f12060f

.field public static final crm_adjust_points_add:I = 0x7f120610

.field public static final crm_adjust_points_remove:I = 0x7f120611

.field public static final crm_adjust_points_title:I = 0x7f120612

.field public static final crm_adjust_punches_error:I = 0x7f120613

.field public static final crm_all_notes_title:I = 0x7f120614

.field public static final crm_all_past_appointments_title:I = 0x7f120615

.field public static final crm_all_upcoming_appointments_title:I = 0x7f120616

.field public static final crm_apply_filters_label:I = 0x7f120617

.field public static final crm_birthday_header:I = 0x7f120618

.field public static final crm_birthday_picker_provide_year:I = 0x7f120619

.field public static final crm_book_appointment:I = 0x7f12061a

.field public static final crm_born_on_format:I = 0x7f12061b

.field public static final crm_buyer_summary_title:I = 0x7f12061c

.field public static final crm_cancel:I = 0x7f12061d

.field public static final crm_cardonfile_addcard_button:I = 0x7f12061e

.field public static final crm_cardonfile_addcard_dropdown:I = 0x7f12061f

.field public static final crm_cardonfile_customer_email:I = 0x7f120620

.field public static final crm_cardonfile_customer_email_body:I = 0x7f120621

.field public static final crm_cardonfile_customer_email_disclaimer:I = 0x7f120622

.field public static final crm_cardonfile_customer_email_disclaimer_uk:I = 0x7f120623

.field public static final crm_cardonfile_customer_email_hint:I = 0x7f120624

.field public static final crm_cardonfile_dip_retry:I = 0x7f120625

.field public static final crm_cardonfile_expiry:I = 0x7f120626

.field public static final crm_cardonfile_expiry_phone:I = 0x7f120627

.field public static final crm_cardonfile_expiry_tablet:I = 0x7f120628

.field public static final crm_cardonfile_header:I = 0x7f120629

.field public static final crm_cardonfile_not_available_offline:I = 0x7f12062a

.field public static final crm_cardonfile_read_error:I = 0x7f12062b

.field public static final crm_cardonfile_savecard_header:I = 0x7f12062c

.field public static final crm_cardonfile_savecard_new_customer:I = 0x7f12062d

.field public static final crm_cardonfile_savecard_save_failure:I = 0x7f12062e

.field public static final crm_cardonfile_savecard_save_failure_subtitle:I = 0x7f12062f

.field public static final crm_cardonfile_savecard_saved:I = 0x7f120630

.field public static final crm_cardonfile_savecard_saving:I = 0x7f120631

.field public static final crm_cardonfile_swipe_retry:I = 0x7f120632

.field public static final crm_cardonfile_unlink_card_content_description:I = 0x7f120633

.field public static final crm_cardonfile_unlink_confirm:I = 0x7f120634

.field public static final crm_cardonfile_unlink_confirm_body:I = 0x7f120635

.field public static final crm_cardonfile_unlink_failed:I = 0x7f120636

.field public static final crm_cardonfile_unlink_loading:I = 0x7f120637

.field public static final crm_cardonfile_unlink_success:I = 0x7f120638

.field public static final crm_cardonfile_verifypostal_button:I = 0x7f120639

.field public static final crm_cardonfile_verifypostal_help_text:I = 0x7f12063a

.field public static final crm_cardonfile_verifypostal_message:I = 0x7f12063b

.field public static final crm_cardonfile_verifypostal_message_no_card_name:I = 0x7f12063c

.field public static final crm_cardonfile_verifypostal_title:I = 0x7f12063d

.field public static final crm_cardonfile_verifypostcode_message:I = 0x7f12063e

.field public static final crm_cardonfile_verifypostcode_message_no_card_name:I = 0x7f12063f

.field public static final crm_cardonfile_verifypostcode_message_uk:I = 0x7f120640

.field public static final crm_cardonfile_verifypostcode_message_uk_no_card_name:I = 0x7f120641

.field public static final crm_cardonfile_verifypostcode_title:I = 0x7f120642

.field public static final crm_cardonfile_verifyzip_message:I = 0x7f120643

.field public static final crm_cardonfile_verifyzip_message_no_card_name:I = 0x7f120644

.field public static final crm_cardonfile_verifyzip_title:I = 0x7f120645

.field public static final crm_choose_filters_search_hint:I = 0x7f120646

.field public static final crm_choose_filters_title:I = 0x7f120647

.field public static final crm_choose_groups_title:I = 0x7f120648

.field public static final crm_choose_many:I = 0x7f120649

.field public static final crm_choose_one:I = 0x7f12064a

.field public static final crm_comment_negative_feedback:I = 0x7f12064b

.field public static final crm_comment_positive_feedback:I = 0x7f12064c

.field public static final crm_company_hint:I = 0x7f12064d

.field public static final crm_confirm_customer_deletion_loyalty_title:I = 0x7f12064e

.field public static final crm_confirm_customer_deletion_many_format:I = 0x7f12064f

.field public static final crm_confirm_customer_deletion_one_details:I = 0x7f120650

.field public static final crm_confirm_customer_deletion_one_details_loyalty_sms:I = 0x7f120651

.field public static final crm_confirm_customer_deletion_one_format:I = 0x7f120652

.field public static final crm_confirm_customer_deletion_title:I = 0x7f120653

.field public static final crm_confirm_customer_loyalty_deletion_many_format:I = 0x7f120654

.field public static final crm_confirm_customer_loyalty_deletion_one_format:I = 0x7f120655

.field public static final crm_contact_default_display_header_label:I = 0x7f120656

.field public static final crm_contact_default_display_initials:I = 0x7f120657

.field public static final crm_contact_default_display_name:I = 0x7f120658

.field public static final crm_contact_deleting_error:I = 0x7f120659

.field public static final crm_contact_list_deselect_all:I = 0x7f12065a

.field public static final crm_contact_list_select_all:I = 0x7f12065b

.field public static final crm_contact_loading_error:I = 0x7f12065c

.field public static final crm_contact_saving_error:I = 0x7f12065d

.field public static final crm_contact_search_empty:I = 0x7f12065e

.field public static final crm_contact_search_error:I = 0x7f12065f

.field public static final crm_conversation_loading_error:I = 0x7f120660

.field public static final crm_conversation_no_response_error:I = 0x7f120661

.field public static final crm_conversation_view_profile:I = 0x7f120662

.field public static final crm_coupon_creation:I = 0x7f120663

.field public static final crm_coupon_details:I = 0x7f120664

.field public static final crm_coupon_expiration:I = 0x7f120665

.field public static final crm_coupon_redeemed_date:I = 0x7f120666

.field public static final crm_coupons_and_rewards_manage_action:I = 0x7f120667

.field public static final crm_coupons_and_rewards_manage_title:I = 0x7f120668

.field public static final crm_coupons_and_rewards_view_all_title:I = 0x7f120669

.field public static final crm_coupons_and_rewards_void_action:I = 0x7f12066a

.field public static final crm_coupons_and_rewards_void_confirm_plural:I = 0x7f12066b

.field public static final crm_coupons_and_rewards_void_confirm_single:I = 0x7f12066c

.field public static final crm_coupons_and_rewards_void_confirm_title:I = 0x7f12066d

.field public static final crm_coupons_and_rewards_void_error:I = 0x7f12066e

.field public static final crm_coupons_and_rewards_void_result_plural:I = 0x7f12066f

.field public static final crm_coupons_and_rewards_void_result_single:I = 0x7f120670

.field public static final crm_coupons_and_rewards_void_voiding:I = 0x7f120671

.field public static final crm_coupons_uppercase:I = 0x7f120672

.field public static final crm_coupons_view_all:I = 0x7f120673

.field public static final crm_create_customer_label:I = 0x7f120674

.field public static final crm_create_first_manual_group:I = 0x7f120675

.field public static final crm_create_group_label:I = 0x7f120676

.field public static final crm_create_group_title:I = 0x7f120677

.field public static final crm_create_manual_group:I = 0x7f120678

.field public static final crm_create_manual_group_first:I = 0x7f120679

.field public static final crm_create_new_customer_label:I = 0x7f12067a

.field public static final crm_create_new_customer_label_format:I = 0x7f12067b

.field public static final crm_create_new_customer_title:I = 0x7f12067c

.field public static final crm_create_note_title:I = 0x7f12067d

.field public static final crm_created:I = 0x7f12067e

.field public static final crm_creation_source_title:I = 0x7f12067f

.field public static final crm_creation_source_unknown:I = 0x7f120680

.field public static final crm_custom_attribute_email_hint:I = 0x7f120681

.field public static final crm_custom_attribute_phone_hint:I = 0x7f120682

.field public static final crm_customer_added_format:I = 0x7f120683

.field public static final crm_customer_management_off:I = 0x7f120684

.field public static final crm_customer_management_on:I = 0x7f120685

.field public static final crm_customer_management_settings_header_label:I = 0x7f120686

.field public static final crm_customer_management_settings_in_cart_hint:I = 0x7f120687

.field public static final crm_customer_management_settings_in_cart_label:I = 0x7f120688

.field public static final crm_customer_management_settings_post_transaction_hint:I = 0x7f120689

.field public static final crm_customer_management_settings_post_transaction_label:I = 0x7f12068a

.field public static final crm_customer_management_settings_save_card_hint:I = 0x7f12068b

.field public static final crm_customer_management_settings_save_card_label:I = 0x7f12068c

.field public static final crm_customer_management_settings_save_card_label_short:I = 0x7f12068d

.field public static final crm_customer_management_settings_save_card_post_transaction_hint:I = 0x7f12068e

.field public static final crm_customer_management_settings_save_card_post_transaction_label:I = 0x7f12068f

.field public static final crm_customer_name_row_content_description:I = 0x7f120690

.field public static final crm_customers_deleted_error:I = 0x7f120691

.field public static final crm_customers_deleted_many_format:I = 0x7f120692

.field public static final crm_customers_deleted_one_format:I = 0x7f120693

.field public static final crm_customers_selected_many_pattern:I = 0x7f120694

.field public static final crm_customers_selected_one:I = 0x7f120695

.field public static final crm_customers_selected_zero:I = 0x7f120696

.field public static final crm_customers_will_be_added_to_group_many_pattern:I = 0x7f120697

.field public static final crm_customers_will_be_added_to_group_one:I = 0x7f120698

.field public static final crm_customers_will_be_merged_into_format:I = 0x7f120699

.field public static final crm_delete:I = 0x7f12069a

.field public static final crm_delete_confirmation_title:I = 0x7f12069b

.field public static final crm_delete_customer:I = 0x7f12069c

.field public static final crm_delete_group_confirm_label:I = 0x7f12069d

.field public static final crm_delete_group_label:I = 0x7f12069e

.field public static final crm_delete_note_label:I = 0x7f12069f

.field public static final crm_delete_note_label_confirm:I = 0x7f1206a0

.field public static final crm_deleting_customers:I = 0x7f1206a1

.field public static final crm_display_name_label:I = 0x7f1206a2

.field public static final crm_display_to_customer:I = 0x7f1206a3

.field public static final crm_duplicates_merged_error:I = 0x7f1206a4

.field public static final crm_duplicates_merged_format:I = 0x7f1206a5

.field public static final crm_edit_customer_address:I = 0x7f1206a6

.field public static final crm_edit_customer_company:I = 0x7f1206a7

.field public static final crm_edit_customer_email_address:I = 0x7f1206a8

.field public static final crm_edit_customer_first_name:I = 0x7f1206a9

.field public static final crm_edit_customer_groups:I = 0x7f1206aa

.field public static final crm_edit_customer_last_name:I = 0x7f1206ab

.field public static final crm_edit_customer_reference_id:I = 0x7f1206ac

.field public static final crm_edit_group_label:I = 0x7f1206ad

.field public static final crm_edit_personal_information_label:I = 0x7f1206ae

.field public static final crm_email_collection_done:I = 0x7f1206af

.field public static final crm_email_collection_enter_email_address:I = 0x7f1206b0

.field public static final crm_email_collection_no_thanks_button:I = 0x7f1206b1

.field public static final crm_email_collection_on_the_list:I = 0x7f1206b2

.field public static final crm_email_collection_prompt:I = 0x7f1206b3

.field public static final crm_email_collection_settings_header_label:I = 0x7f1206b4

.field public static final crm_email_collection_settings_screen_dialog_confirmation:I = 0x7f1206b5

.field public static final crm_email_collection_settings_screen_dialog_decline:I = 0x7f1206b6

.field public static final crm_email_collection_settings_screen_dialog_message:I = 0x7f1206b7

.field public static final crm_email_collection_settings_screen_dialog_title:I = 0x7f1206b8

.field public static final crm_email_collection_settings_screen_hint:I = 0x7f1206b9

.field public static final crm_email_collection_settings_screen_label:I = 0x7f1206ba

.field public static final crm_email_collection_settings_url:I = 0x7f1206bb

.field public static final crm_email_collection_submit:I = 0x7f1206bc

.field public static final crm_email_hint:I = 0x7f1206bd

.field public static final crm_employee_full_name:I = 0x7f1206be

.field public static final crm_empty_directory_subtitle:I = 0x7f1206bf

.field public static final crm_empty_directory_title:I = 0x7f1206c0

.field public static final crm_failed_to_delete_group:I = 0x7f1206c1

.field public static final crm_failed_to_load_customers:I = 0x7f1206c2

.field public static final crm_failed_to_load_filters:I = 0x7f1206c3

.field public static final crm_failed_to_save_group:I = 0x7f1206c4

.field public static final crm_feedback_empty_list_warning:I = 0x7f1206c5

.field public static final crm_feedback_no_message:I = 0x7f1206c6

.field public static final crm_feedback_title:I = 0x7f1206c7

.field public static final crm_filter_bubble_label:I = 0x7f1206c8

.field public static final crm_filter_customers_menu_label:I = 0x7f1206c9

.field public static final crm_filter_customers_title:I = 0x7f1206ca

.field public static final crm_filter_disjunction_separator_pattern:I = 0x7f1206cb

.field public static final crm_filter_search_empty:I = 0x7f1206cc

.field public static final crm_filter_visit_frequency_value_pattern:I = 0x7f1206cd

.field public static final crm_first_name_hint:I = 0x7f1206ce

.field public static final crm_frequent_items_heading:I = 0x7f1206cf

.field public static final crm_frequent_items_heading_uppercase:I = 0x7f1206d0

.field public static final crm_frequent_items_last_year:I = 0x7f1206d1

.field public static final crm_frequent_items_section_all_items:I = 0x7f1206d2

.field public static final crm_frequent_items_subtitle_format:I = 0x7f1206d3

.field public static final crm_frequent_items_subtitle_purchase_plural:I = 0x7f1206d4

.field public static final crm_frequent_items_subtitle_purchase_singular:I = 0x7f1206d5

.field public static final crm_frequent_items_uncategorized:I = 0x7f1206d6

.field public static final crm_group_loading_error:I = 0x7f1206d7

.field public static final crm_group_name_hint:I = 0x7f1206d8

.field public static final crm_group_saving_error:I = 0x7f1206d9

.field public static final crm_grouped_in_format:I = 0x7f1206da

.field public static final crm_groups_choose_groups_title:I = 0x7f1206db

.field public static final crm_groups_create_group_label:I = 0x7f1206dc

.field public static final crm_groups_group_name_label:I = 0x7f1206dd

.field public static final crm_groups_group_saving_error:I = 0x7f1206de

.field public static final crm_groups_hint:I = 0x7f1206df

.field public static final crm_groups_manual_groups_uppercase:I = 0x7f1206e0

.field public static final crm_groups_no_groups_exist_message:I = 0x7f1206e1

.field public static final crm_groups_smart_groups_uppercase:I = 0x7f1206e2

.field public static final crm_groups_title:I = 0x7f1206e3

.field public static final crm_invoice_details_all:I = 0x7f1206e4

.field public static final crm_invoice_list_header:I = 0x7f1206e5

.field public static final crm_invoice_row:I = 0x7f1206e6

.field public static final crm_invoices_uppercase:I = 0x7f1206e7

.field public static final crm_last_name_hint:I = 0x7f1206e8

.field public static final crm_loyalty_adjust_status_add:I = 0x7f1206e9

.field public static final crm_loyalty_adjust_status_add_reason_one:I = 0x7f1206ea

.field public static final crm_loyalty_adjust_status_add_reason_title:I = 0x7f1206eb

.field public static final crm_loyalty_adjust_status_add_reason_two:I = 0x7f1206ec

.field public static final crm_loyalty_adjust_status_remove:I = 0x7f1206ed

.field public static final crm_loyalty_adjust_status_remove_reason_one:I = 0x7f1206ee

.field public static final crm_loyalty_adjust_status_remove_reason_three:I = 0x7f1206ef

.field public static final crm_loyalty_adjust_status_remove_reason_title:I = 0x7f1206f0

.field public static final crm_loyalty_adjust_status_remove_reason_two:I = 0x7f1206f1

.field public static final crm_loyalty_adjust_status_sms_disclaimer:I = 0x7f1206f2

.field public static final crm_loyalty_delete_account_body:I = 0x7f1206f3

.field public static final crm_loyalty_delete_account_body_sms_warning:I = 0x7f1206f4

.field public static final crm_loyalty_delete_account_overflow:I = 0x7f1206f5

.field public static final crm_loyalty_delete_account_title:I = 0x7f1206f6

.field public static final crm_loyalty_deleting_account:I = 0x7f1206f7

.field public static final crm_loyalty_deleting_failure:I = 0x7f1206f8

.field public static final crm_loyalty_deleting_success:I = 0x7f1206f9

.field public static final crm_loyalty_lifetime_points:I = 0x7f1206fa

.field public static final crm_loyalty_member_since:I = 0x7f1206fb

.field public static final crm_loyalty_program_null_state:I = 0x7f1206fc

.field public static final crm_loyalty_program_phone:I = 0x7f1206fd

.field public static final crm_loyalty_program_section_adjust_status:I = 0x7f1206fe

.field public static final crm_loyalty_program_section_all_loyalty_rewards:I = 0x7f1206ff

.field public static final crm_loyalty_program_section_copy_phone:I = 0x7f120700

.field public static final crm_loyalty_program_section_edit_phone:I = 0x7f120701

.field public static final crm_loyalty_program_section_header_uppercase:I = 0x7f120702

.field public static final crm_loyalty_program_section_see_all_reward_tiers:I = 0x7f120703

.field public static final crm_loyalty_program_section_send_status:I = 0x7f120704

.field public static final crm_loyalty_program_section_send_status_body:I = 0x7f120705

.field public static final crm_loyalty_program_section_send_status_confirm:I = 0x7f120706

.field public static final crm_loyalty_program_section_view_expiring_points:I = 0x7f120707

.field public static final crm_loyalty_program_section_view_expiring_points_empty:I = 0x7f120708

.field public static final crm_loyalty_program_section_view_expiring_points_error:I = 0x7f120709

.field public static final crm_loyalty_program_section_view_expiring_points_title:I = 0x7f12070a

.field public static final crm_loyalty_program_section_view_expiring_points_value:I = 0x7f12070b

.field public static final crm_loyalty_redeem_row_action:I = 0x7f12070c

.field public static final crm_loyalty_transfer_account:I = 0x7f12070d

.field public static final crm_loyalty_transfer_account_overflow:I = 0x7f12070e

.field public static final crm_loyalty_transfer_account_short:I = 0x7f12070f

.field public static final crm_loyalty_transferring_account:I = 0x7f120710

.field public static final crm_loyalty_transferring_failure:I = 0x7f120711

.field public static final crm_loyalty_transferring_success:I = 0x7f120712

.field public static final crm_manual_group_create_description:I = 0x7f120713

.field public static final crm_merge_customer_profile_label:I = 0x7f120714

.field public static final crm_merge_customers_confirmation_title:I = 0x7f120715

.field public static final crm_merge_customers_label:I = 0x7f120716

.field public static final crm_merge_label:I = 0x7f120717

.field public static final crm_merged_customers_format:I = 0x7f120718

.field public static final crm_merging_customers_error:I = 0x7f120719

.field public static final crm_merging_customers_format:I = 0x7f12071a

.field public static final crm_merging_duplicates:I = 0x7f12071b

.field public static final crm_no_comment_negative_feedback:I = 0x7f12071c

.field public static final crm_no_comment_positive_feedback:I = 0x7f12071d

.field public static final crm_no_groups_exist:I = 0x7f12071e

.field public static final crm_note_char_max:I = 0x7f12071f

.field public static final crm_note_creator_timestamp:I = 0x7f120720

.field public static final crm_note_deleting_error:I = 0x7f120721

.field public static final crm_note_header:I = 0x7f120722

.field public static final crm_note_saving_error:I = 0x7f120723

.field public static final crm_notes_section_all_notes:I = 0x7f120724

.field public static final crm_notes_section_header_uppercase:I = 0x7f120725

.field public static final crm_other_information_edit_uppercase:I = 0x7f120726

.field public static final crm_personal_information_header_uppercase:I = 0x7f120727

.field public static final crm_phone_hint:I = 0x7f120728

.field public static final crm_points_add:I = 0x7f120729

.field public static final crm_points_balance:I = 0x7f12072a

.field public static final crm_points_current_unadjusted_balance:I = 0x7f12072b

.field public static final crm_points_earned_format:I = 0x7f12072c

.field public static final crm_points_negative_adjusted_balance:I = 0x7f12072d

.field public static final crm_points_positive_adjusted_balance:I = 0x7f12072e

.field public static final crm_points_remove:I = 0x7f12072f

.field public static final crm_profile_attachments_add_files:I = 0x7f120730

.field public static final crm_profile_attachments_delete:I = 0x7f120731

.field public static final crm_profile_attachments_delete_cancelled:I = 0x7f120732

.field public static final crm_profile_attachments_delete_file_fallback_name:I = 0x7f120733

.field public static final crm_profile_attachments_delete_file_message:I = 0x7f120734

.field public static final crm_profile_attachments_delete_file_title:I = 0x7f120735

.field public static final crm_profile_attachments_delete_success:I = 0x7f120736

.field public static final crm_profile_attachments_download:I = 0x7f120737

.field public static final crm_profile_attachments_filename_template:I = 0x7f120738

.field public static final crm_profile_attachments_general_failure:I = 0x7f120739

.field public static final crm_profile_attachments_header:I = 0x7f12073a

.field public static final crm_profile_attachments_header_uppercase:I = 0x7f12073b

.field public static final crm_profile_attachments_options:I = 0x7f12073c

.field public static final crm_profile_attachments_rename:I = 0x7f12073d

.field public static final crm_profile_attachments_rename_file_cancelled:I = 0x7f12073e

.field public static final crm_profile_attachments_rename_file_hint:I = 0x7f12073f

.field public static final crm_profile_attachments_rename_file_message:I = 0x7f120740

.field public static final crm_profile_attachments_rename_file_title:I = 0x7f120741

.field public static final crm_profile_attachments_take_a_photo:I = 0x7f120742

.field public static final crm_profile_attachments_upload_failed:I = 0x7f120743

.field public static final crm_profile_attachments_upload_files:I = 0x7f120744

.field public static final crm_profile_attachments_upload_in_progress:I = 0x7f120745

.field public static final crm_profile_attachments_upload_photo:I = 0x7f120746

.field public static final crm_profile_attachments_upload_succeeded:I = 0x7f120747

.field public static final crm_recent_header_uppercase:I = 0x7f120748

.field public static final crm_reference_id_hint:I = 0x7f120749

.field public static final crm_reminder:I = 0x7f12074a

.field public static final crm_reminder_none:I = 0x7f12074b

.field public static final crm_reminder_one_day:I = 0x7f12074c

.field public static final crm_reminder_one_month:I = 0x7f12074d

.field public static final crm_reminder_one_week:I = 0x7f12074e

.field public static final crm_reminder_remove:I = 0x7f12074f

.field public static final crm_reminder_remove_confirm:I = 0x7f120750

.field public static final crm_reminder_row:I = 0x7f120751

.field public static final crm_reminder_tomorrow:I = 0x7f120752

.field public static final crm_remove_all_filters_label:I = 0x7f120753

.field public static final crm_remove_customer_from_estimate:I = 0x7f120754

.field public static final crm_remove_customer_from_estimate_shortened:I = 0x7f120755

.field public static final crm_remove_customer_from_invoice:I = 0x7f120756

.field public static final crm_remove_customer_from_invoice_shortened:I = 0x7f120757

.field public static final crm_remove_customer_from_sale:I = 0x7f120758

.field public static final crm_remove_customer_from_sale_shortened:I = 0x7f120759

.field public static final crm_remove_filter_label:I = 0x7f12075a

.field public static final crm_remove_from_group:I = 0x7f12075b

.field public static final crm_resolve_duplicates_label:I = 0x7f12075c

.field public static final crm_resolve_duplicates_title:I = 0x7f12075d

.field public static final crm_rightpane_default_many:I = 0x7f12075e

.field public static final crm_rightpane_default_one:I = 0x7f12075f

.field public static final crm_rightpane_default_zero_noresults:I = 0x7f120760

.field public static final crm_rightpane_multiselect_many:I = 0x7f120761

.field public static final crm_rightpane_multiselect_one:I = 0x7f120762

.field public static final crm_rightpane_multiselect_zero:I = 0x7f120763

.field public static final crm_save:I = 0x7f120764

.field public static final crm_save_filters_label:I = 0x7f120765

.field public static final crm_save_filters_title:I = 0x7f120766

.field public static final crm_search_customers_hint:I = 0x7f120767

.field public static final crm_search_customers_hint_filtering:I = 0x7f120768

.field public static final crm_section_view_all:I = 0x7f120769

.field public static final crm_select_customers_add_to_group_label:I = 0x7f12076a

.field public static final crm_select_customers_delete_label:I = 0x7f12076b

.field public static final crm_select_customers_menu_label:I = 0x7f12076c

.field public static final crm_select_customers_merge_label:I = 0x7f12076d

.field public static final crm_select_group_title:I = 0x7f12076e

.field public static final crm_select_loyalty_phone:I = 0x7f12076f

.field public static final crm_select_loyalty_phone_primary_button_label:I = 0x7f120770

.field public static final crm_select_loyalty_phone_primary_message:I = 0x7f120771

.field public static final crm_select_loyalty_phone_secondary_message:I = 0x7f120772

.field public static final crm_send_message_error:I = 0x7f120773

.field public static final crm_send_message_hint:I = 0x7f120774

.field public static final crm_send_message_label:I = 0x7f120775

.field public static final crm_send_message_send:I = 0x7f120776

.field public static final crm_send_message_title:I = 0x7f120777

.field public static final crm_send_message_warning:I = 0x7f120778

.field public static final crm_titlecase_customers:I = 0x7f120779

.field public static final crm_update_loyalty_conflict_message:I = 0x7f12077a

.field public static final crm_update_loyalty_conflict_negative:I = 0x7f12077b

.field public static final crm_update_loyalty_conflict_neutral:I = 0x7f12077c

.field public static final crm_update_loyalty_conflict_positive:I = 0x7f12077d

.field public static final crm_update_loyalty_conflict_title:I = 0x7f12077e

.field public static final crm_update_loyalty_phone_error:I = 0x7f12077f

.field public static final crm_update_loyalty_phone_hint:I = 0x7f120780

.field public static final crm_update_loyalty_phone_title:I = 0x7f120781

.field public static final crm_view_customer_label:I = 0x7f120782

.field public static final crm_view_feedback:I = 0x7f120783

.field public static final crm_view_groups_label:I = 0x7f120784

.field public static final crm_view_note_title:I = 0x7f120785

.field public static final currency_format_cents:I = 0x7f120786

.field public static final current_customer_cards_on_file:I = 0x7f120787

.field public static final current_customer_cards_on_file_offline_mode:I = 0x7f120788

.field public static final current_drawer_actual_in_drawer_hint:I = 0x7f120789

.field public static final current_drawer_closed_remotely_message:I = 0x7f12078a

.field public static final current_drawer_closed_remotely_title:I = 0x7f12078b

.field public static final current_drawer_confirm_end_drawer:I = 0x7f12078c

.field public static final current_drawer_confirm_start_drawer:I = 0x7f12078d

.field public static final current_drawer_drawer_description_hint:I = 0x7f12078e

.field public static final current_drawer_drawer_description_label_uppercase:I = 0x7f12078f

.field public static final current_drawer_end_drawer:I = 0x7f120790

.field public static final current_drawer_open:I = 0x7f120791

.field public static final current_drawer_report_saved:I = 0x7f120792

.field public static final current_drawer_start_drawer:I = 0x7f120793

.field public static final current_drawer_starting_cash_label_uppercase:I = 0x7f120794

.field public static final current_drawer_turn_on_cash_management:I = 0x7f120795

.field public static final current_sale:I = 0x7f120796

.field public static final custom:I = 0x7f120797

.field public static final custom_surcharge_amount:I = 0x7f120798

.field public static final custom_surcharge_percentage:I = 0x7f120799

.field public static final customer_card_on_file:I = 0x7f12079a

.field public static final customer_card_on_file_offline_mode:I = 0x7f12079b

.field public static final customer_cards_on_file:I = 0x7f12079c

.field public static final customer_checkout_default:I = 0x7f12079d

.field public static final customer_checkout_default_hint:I = 0x7f12079e

.field public static final customer_checkout_settings_label:I = 0x7f12079f

.field public static final customer_checkout_show_cart:I = 0x7f1207a0

.field public static final customer_checkout_show_cart_hint:I = 0x7f1207a1

.field public static final customer_checkout_standard:I = 0x7f1207a2

.field public static final customer_gift_card_on_file:I = 0x7f1207a3

.field public static final customer_no_cards_on_file:I = 0x7f1207a4

.field public static final customer_no_cards_on_file_help_text:I = 0x7f1207a5

.field public static final customer_no_cards_on_file_v2:I = 0x7f1207a6

.field public static final customer_no_gift_cards_on_file:I = 0x7f1207a7

.field public static final customer_number_of_cards_on_file_many:I = 0x7f1207a8

.field public static final customer_number_of_cards_on_file_one:I = 0x7f1207a9

.field public static final customer_typing_body:I = 0x7f1207aa

.field public static final customer_unit_contact_full_name_format:I = 0x7f1207ab

.field public static final customer_unit_contact_info_format:I = 0x7f1207ac

.field public static final customers_applet_title:I = 0x7f1207ad

.field public static final customize_report_action_button:I = 0x7f1207ae

.field public static final customize_report_all_day:I = 0x7f1207af

.field public static final customize_report_all_devices:I = 0x7f1207b0

.field public static final customize_report_all_employees:I = 0x7f1207b1

.field public static final customize_report_card_title:I = 0x7f1207b2

.field public static final customize_report_device_filter:I = 0x7f1207b3

.field public static final customize_report_employee_filter_deselect_all:I = 0x7f1207b4

.field public static final customize_report_employee_filter_select_all:I = 0x7f1207b5

.field public static final customize_report_employee_filter_title:I = 0x7f1207b6

.field public static final customize_report_employee_name:I = 0x7f1207b7

.field public static final customize_report_end_time_label:I = 0x7f1207b8

.field public static final customize_report_filter_device_header_uppercase:I = 0x7f1207b9

.field public static final customize_report_filter_employee_header_uppercase:I = 0x7f1207ba

.field public static final customize_report_multiple_employee_filter_description:I = 0x7f1207bb

.field public static final customize_report_start_time_label:I = 0x7f1207bc

.field public static final cvv:I = 0x7f1207bd

.field public static final cvv_hint:I = 0x7f1207be

.field public static final dangling_miryo_payment:I = 0x7f1207bf

.field public static final dashboard:I = 0x7f1207c0

.field public static final dashboard_account_relative_url:I = 0x7f1207c1

.field public static final dashboard_discount_url:I = 0x7f1207c2

.field public static final dashboard_employees_relative_url:I = 0x7f1207c3

.field public static final dashboard_items_library_url:I = 0x7f1207c4

.field public static final dashboard_modifier_url:I = 0x7f1207c5

.field public static final dashboard_taxes_url:I = 0x7f1207c6

.field public static final dashboard_team_permissions_relative_url:I = 0x7f1207c7

.field public static final dashboard_timecards_relative_url:I = 0x7f1207c8

.field public static final date_format:I = 0x7f1207c9

.field public static final date_hint:I = 0x7f1207ca

.field public static final date_of_birth:I = 0x7f1207cb

.field public static final date_picker_month_format:I = 0x7f1207cc

.field public static final date_picker_next_month:I = 0x7f1207cd

.field public static final date_picker_previous_month:I = 0x7f1207ce

.field public static final date_picker_provide_year:I = 0x7f1207cf

.field public static final date_picker_remove:I = 0x7f1207d0

.field public static final date_range_tooltip:I = 0x7f1207d1

.field public static final date_selected:I = 0x7f1207d2

.field public static final date_unit_days:I = 0x7f1207d3

.field public static final date_unit_months:I = 0x7f1207d4

.field public static final date_unit_weeks:I = 0x7f1207d5

.field public static final date_unit_years:I = 0x7f1207d6

.field public static final day:I = 0x7f1207d7

.field public static final day_and_date:I = 0x7f1207d8

.field public static final day_format:I = 0x7f1207d9

.field public static final day_name_format:I = 0x7f1207da

.field public static final days:I = 0x7f1207db

.field public static final deactivate:I = 0x7f1207dc

.field public static final deactivate_confirmation_dialog_body:I = 0x7f1207dd

.field public static final deactivate_confirmation_dialog_title:I = 0x7f1207de

.field public static final deactivate_link:I = 0x7f1207df

.field public static final debit_card:I = 0x7f1207e0

.field public static final debit_card_hint:I = 0x7f1207e1

.field public static final debit_card_required_message:I = 0x7f1207e2

.field public static final default_itemization_name:I = 0x7f1207e3

.field public static final default_setup_guide_intro_card_subtitle:I = 0x7f1207e4

.field public static final delete:I = 0x7f1207e5

.field public static final delete_button_confirm_stage_text:I = 0x7f1207e6

.field public static final delete_confirmation_dialog_body:I = 0x7f1207e7

.field public static final delete_confirmation_dialog_title:I = 0x7f1207e8

.field public static final delete_item:I = 0x7f1207e9

.field public static final delete_link:I = 0x7f1207ea

.field public static final delete_unit_failed_alert_message_due_to_network:I = 0x7f1207eb

.field public static final delete_unit_failed_alert_message_not_due_to_network:I = 0x7f1207ec

.field public static final delete_unit_failed_alert_title:I = 0x7f1207ed

.field public static final deposit:I = 0x7f1207ee

.field public static final deposit_amount_greater_than_zero:I = 0x7f1207ef

.field public static final deposit_amount_larger_than_invoice:I = 0x7f1207f0

.field public static final deposit_amount_less_than_completed:I = 0x7f1207f1

.field public static final deposit_options_hint:I = 0x7f1207f2

.field public static final deposit_options_hint_jp:I = 0x7f1207f3

.field public static final deposit_options_url:I = 0x7f1207f4

.field public static final deposit_percent:I = 0x7f1207f5

.field public static final deposit_request_all_caps:I = 0x7f1207f6

.field public static final deposit_schedule_arrival_day_and_time:I = 0x7f1207f7

.field public static final deposit_schedule_automatic:I = 0x7f1207f8

.field public static final deposit_schedule_automatic_description:I = 0x7f1207f9

.field public static final deposit_schedule_close_of_day:I = 0x7f1207fa

.field public static final deposit_schedule_manual:I = 0x7f1207fb

.field public static final deposit_schedule_manual_description:I = 0x7f1207fc

.field public static final deposit_schedule_title:I = 0x7f1207fd

.field public static final deposit_speed:I = 0x7f1207fe

.field public static final deposit_speed_custom:I = 0x7f1207ff

.field public static final deposit_speed_hint:I = 0x7f120800

.field public static final deposit_speed_one_to_two_bus_days:I = 0x7f120801

.field public static final deposit_speed_one_to_two_business_days:I = 0x7f120802

.field public static final deposit_speed_same_day:I = 0x7f120803

.field public static final deposit_speed_same_day_fee:I = 0x7f120804

.field public static final deposit_speed_subtitle:I = 0x7f120805

.field public static final deposit_speed_title:I = 0x7f120806

.field public static final deposit_to_bank:I = 0x7f120807

.field public static final deposits_frozen_help_link_url:I = 0x7f120808

.field public static final deposits_frozen_info:I = 0x7f120809

.field public static final deposits_frozen_info_link:I = 0x7f12080a

.field public static final deposits_frozen_title_uppercase:I = 0x7f12080b

.field public static final deposits_frozen_verification_url:I = 0x7f12080c

.field public static final deposits_frozen_verify_account:I = 0x7f12080d

.field public static final deposits_report:I = 0x7f12080e

.field public static final deposits_report_active_sales:I = 0x7f12080f

.field public static final deposits_report_bank_name_and_bank_account_suffix:I = 0x7f120810

.field public static final deposits_report_card_payments_in_deposit_uppercase:I = 0x7f120811

.field public static final deposits_report_collected_uppercase:I = 0x7f120812

.field public static final deposits_report_current_balance_uppercase:I = 0x7f120813

.field public static final deposits_report_deposit:I = 0x7f120814

.field public static final deposits_report_deposit_uppercase:I = 0x7f120815

.field public static final deposits_report_estimated_fees:I = 0x7f120816

.field public static final deposits_report_fee_details_learn_more:I = 0x7f120817

.field public static final deposits_report_fee_details_message:I = 0x7f120818

.field public static final deposits_report_fee_details_title:I = 0x7f120819

.field public static final deposits_report_fees:I = 0x7f12081a

.field public static final deposits_report_help_button:I = 0x7f12081b

.field public static final deposits_report_history_uppercase:I = 0x7f12081c

.field public static final deposits_report_instant_deposit:I = 0x7f12081d

.field public static final deposits_report_instant_deposit_fee:I = 0x7f12081e

.field public static final deposits_report_net_total_uppercase:I = 0x7f12081f

.field public static final deposits_report_next_business_day_deposit:I = 0x7f120820

.field public static final deposits_report_no_deposit:I = 0x7f120821

.field public static final deposits_report_pay_in:I = 0x7f120822

.field public static final deposits_report_pay_in_date:I = 0x7f120823

.field public static final deposits_report_pay_in_source:I = 0x7f120824

.field public static final deposits_report_pending_deposit:I = 0x7f120825

.field public static final deposits_report_pending_withdrawal:I = 0x7f120826

.field public static final deposits_report_same_day_deposit:I = 0x7f120827

.field public static final deposits_report_same_day_deposit_fee:I = 0x7f120828

.field public static final deposits_report_sending_date:I = 0x7f120829

.field public static final deposits_report_sent_date_time:I = 0x7f12082a

.field public static final deposits_report_summary_uppercase:I = 0x7f12082b

.field public static final deposits_report_total_collected:I = 0x7f12082c

.field public static final deposits_report_withdrawal:I = 0x7f12082d

.field public static final deposits_report_withdrawing_date:I = 0x7f12082e

.field public static final deposits_settings:I = 0x7f12082f

.field public static final detail_searchable_list_no_search_results:I = 0x7f120830

.field public static final details:I = 0x7f120831

.field public static final details_header:I = 0x7f120832

.field public static final device_code_auth_failed_message:I = 0x7f120833

.field public static final device_code_hint:I = 0x7f120834

.field public static final device_code_hint_link_text:I = 0x7f120835

.field public static final device_code_login_failed_message:I = 0x7f120836

.field public static final device_code_login_password_hint:I = 0x7f120837

.field public static final device_code_sign_in_to_square:I = 0x7f120838

.field public static final device_code_url:I = 0x7f120839

.field public static final device_name:I = 0x7f12083a

.field public static final device_name_hint:I = 0x7f12083b

.field public static final device_no_name:I = 0x7f12083c

.field public static final devplat_error_cannot_verify_fingerprint:I = 0x7f12083d

.field public static final devplat_error_customer_not_supported:I = 0x7f12083e

.field public static final devplat_error_dev_portal_error_help:I = 0x7f12083f

.field public static final devplat_error_illegal_location_id:I = 0x7f120840

.field public static final devplat_error_invalid_api_version:I = 0x7f120841

.field public static final devplat_error_invalid_api_version_dialog_title:I = 0x7f120842

.field public static final devplat_error_invalid_charge_amount:I = 0x7f120843

.field public static final devplat_error_invalid_client_id:I = 0x7f120844

.field public static final devplat_error_invalid_currency:I = 0x7f120845

.field public static final devplat_error_invalid_customer_id:I = 0x7f120846

.field public static final devplat_error_invalid_fingerprint:I = 0x7f120847

.field public static final devplat_error_invalid_sandbox_client_id:I = 0x7f120848

.field public static final devplat_error_invalid_start_method:I = 0x7f120849

.field public static final devplat_error_invalid_start_method_dialog_title:I = 0x7f12084a

.field public static final devplat_error_invalid_timeout:I = 0x7f12084b

.field public static final devplat_error_invalid_web_callback_uri:I = 0x7f12084c

.field public static final devplat_error_missing_api_version:I = 0x7f12084d

.field public static final devplat_error_missing_charge_amount:I = 0x7f12084e

.field public static final devplat_error_missing_client_id:I = 0x7f12084f

.field public static final devplat_error_missing_currency:I = 0x7f120850

.field public static final devplat_error_missing_result:I = 0x7f120851

.field public static final devplat_error_missing_signature:I = 0x7f120852

.field public static final devplat_error_missing_tender_type:I = 0x7f120853

.field public static final devplat_error_missing_web_callback_uri:I = 0x7f120854

.field public static final devplat_error_no_employee_logged_in:I = 0x7f120855

.field public static final devplat_error_no_network:I = 0x7f120856

.field public static final devplat_error_no_server:I = 0x7f120857

.field public static final devplat_error_note_too_long:I = 0x7f120858

.field public static final devplat_error_timeout_too_high:I = 0x7f120859

.field public static final devplat_error_timeout_too_low:I = 0x7f12085a

.field public static final devplat_error_transaction_canceled:I = 0x7f12085b

.field public static final devplat_error_transaction_in_progress:I = 0x7f12085c

.field public static final devplat_error_transaction_stale:I = 0x7f12085d

.field public static final devplat_error_unknown_error:I = 0x7f12085e

.field public static final devplat_error_unknown_package:I = 0x7f12085f

.field public static final devplat_error_unsupported_api_version:I = 0x7f120860

.field public static final devplat_error_unsupported_web_api_version:I = 0x7f120861

.field public static final devplat_error_user_not_activated:I = 0x7f120862

.field public static final devplat_error_user_not_logged_in:I = 0x7f120863

.field public static final diagnostics:I = 0x7f120864

.field public static final diagnostics_logged:I = 0x7f120865

.field public static final dialog_cancel:I = 0x7f120866

.field public static final dialog_confirm:I = 0x7f120867

.field public static final dialog_dismiss:I = 0x7f120868

.field public static final dialog_okay:I = 0x7f120869

.field public static final dialog_try_again:I = 0x7f12086a

.field public static final digit_0:I = 0x7f12086b

.field public static final digit_1:I = 0x7f12086c

.field public static final digit_2:I = 0x7f12086d

.field public static final digit_3:I = 0x7f12086e

.field public static final digit_4:I = 0x7f12086f

.field public static final digit_5:I = 0x7f120870

.field public static final digit_6:I = 0x7f120871

.field public static final digit_7:I = 0x7f120872

.field public static final digit_8:I = 0x7f120873

.field public static final digit_9:I = 0x7f120874

.field public static final direct_debit_guarantee_url:I = 0x7f120875

.field public static final direct_debit_logo:I = 0x7f120876

.field public static final disable:I = 0x7f120877

.field public static final discard_payment_prompt_cancel:I = 0x7f120878

.field public static final discard_payment_prompt_confirm:I = 0x7f120879

.field public static final discard_payment_prompt_message:I = 0x7f12087a

.field public static final discard_payment_prompt_message_split_tender:I = 0x7f12087b

.field public static final discard_payment_prompt_title:I = 0x7f12087c

.field public static final discount_delete:I = 0x7f12087d

.field public static final discount_help_text:I = 0x7f12087e

.field public static final discount_modify_tax_basis_title:I = 0x7f12087f

.field public static final discount_name_hint:I = 0x7f120880

.field public static final discount_name_required_warning_message:I = 0x7f120881

.field public static final discount_name_required_warning_title:I = 0x7f120882

.field public static final discount_options:I = 0x7f120883

.field public static final discount_receipt_format:I = 0x7f120884

.field public static final discount_rule_type_date_range:I = 0x7f120885

.field public static final discount_rule_type_date_range_ends:I = 0x7f120886

.field public static final discount_rule_type_date_range_starts:I = 0x7f120887

.field public static final discount_rule_type_items:I = 0x7f120888

.field public static final discount_rule_type_schedule:I = 0x7f120889

.field public static final discount_rules_help_text:I = 0x7f12088a

.field public static final discount_variable_auto_apply_warning_text:I = 0x7f12088b

.field public static final discount_variable_auto_apply_warning_title:I = 0x7f12088c

.field public static final discount_warning_no_matching_item:I = 0x7f12088d

.field public static final discounts_hint_url:I = 0x7f12088e

.field public static final dismiss:I = 0x7f12088f

.field public static final display_disconnected_successful_payment_body:I = 0x7f120890

.field public static final display_disconnected_successful_payment_title:I = 0x7f120891

.field public static final display_disconnected_unsuccessful_payment_body:I = 0x7f120892

.field public static final display_disconnected_unsuccessful_payment_title:I = 0x7f120893

.field public static final dispute_accept:I = 0x7f120894

.field public static final dispute_action_dialog_button:I = 0x7f120895

.field public static final dispute_action_dialog_message:I = 0x7f120896

.field public static final dispute_action_dialog_title:I = 0x7f120897

.field public static final dispute_card_type:I = 0x7f120898

.field public static final dispute_challenge:I = 0x7f120899

.field public static final dispute_challenge_submission:I = 0x7f12089a

.field public static final dispute_date:I = 0x7f12089b

.field public static final dispute_deadline:I = 0x7f12089c

.field public static final dispute_detail_description_accepted:I = 0x7f12089d

.field public static final dispute_detail_description_lost:I = 0x7f12089e

.field public static final dispute_detail_description_no_action_low:I = 0x7f12089f

.field public static final dispute_detail_description_no_action_refunded:I = 0x7f1208a0

.field public static final dispute_detail_description_pending:I = 0x7f1208a1

.field public static final dispute_detail_description_reopened:I = 0x7f1208a2

.field public static final dispute_detail_description_under_review:I = 0x7f1208a3

.field public static final dispute_detail_description_won:I = 0x7f1208a4

.field public static final dispute_detail_held_description:I = 0x7f1208a5

.field public static final dispute_detail_title_closed:I = 0x7f1208a6

.field public static final dispute_detail_title_open:I = 0x7f1208a7

.field public static final dispute_detail_title_pending:I = 0x7f1208a8

.field public static final dispute_detail_title_under_review:I = 0x7f1208a9

.field public static final dispute_held_amount:I = 0x7f1208aa

.field public static final dispute_list_action_required:I = 0x7f1208ab

.field public static final dispute_list_pending:I = 0x7f1208ac

.field public static final dispute_list_resolved_accepted:I = 0x7f1208ad

.field public static final dispute_list_resolved_cardholder:I = 0x7f1208ae

.field public static final dispute_list_resolved_merchant:I = 0x7f1208af

.field public static final dispute_list_summary_funds:I = 0x7f1208b0

.field public static final dispute_list_summary_protection:I = 0x7f1208b1

.field public static final dispute_list_summary_total:I = 0x7f1208b2

.field public static final dispute_load_more_error_body:I = 0x7f1208b3

.field public static final dispute_load_more_error_title:I = 0x7f1208b4

.field public static final dispute_reason:I = 0x7f1208b5

.field public static final dispute_reason_amount_differs:I = 0x7f1208b6

.field public static final dispute_reason_cancelled:I = 0x7f1208b7

.field public static final dispute_reason_compliance:I = 0x7f1208b8

.field public static final dispute_reason_customer_requests_credit:I = 0x7f1208b9

.field public static final dispute_reason_dissatisfied:I = 0x7f1208ba

.field public static final dispute_reason_duplicate:I = 0x7f1208bb

.field public static final dispute_reason_emv_liability_shift:I = 0x7f1208bc

.field public static final dispute_reason_fraud:I = 0x7f1208bd

.field public static final dispute_reason_no_knowledge:I = 0x7f1208be

.field public static final dispute_reason_not_as_described:I = 0x7f1208bf

.field public static final dispute_reason_not_received:I = 0x7f1208c0

.field public static final dispute_reason_paid_by_other_means:I = 0x7f1208c1

.field public static final dispute_reason_unauthorized:I = 0x7f1208c2

.field public static final dispute_reason_unknown:I = 0x7f1208c3

.field public static final dispute_receipt:I = 0x7f1208c4

.field public static final dispute_receipt_token:I = 0x7f1208c5

.field public static final dispute_skipped_file_upload:I = 0x7f1208c6

.field public static final dispute_view_submission:I = 0x7f1208c7

.field public static final disputed_amount:I = 0x7f1208c8

.field public static final disputes_empty_message:I = 0x7f1208c9

.field public static final disputes_empty_title:I = 0x7f1208ca

.field public static final disputes_error_title:I = 0x7f1208cb

.field public static final disputes_error_try_again:I = 0x7f1208cc

.field public static final disputes_learn_more:I = 0x7f1208cd

.field public static final disputes_learn_more_url:I = 0x7f1208ce

.field public static final disputes_notification_dialog_content:I = 0x7f1208cf

.field public static final disputes_notification_dialog_primary:I = 0x7f1208d0

.field public static final disputes_notification_dialog_title:I = 0x7f1208d1

.field public static final donation_link_price_label:I = 0x7f1208d2

.field public static final done:I = 0x7f1208d3

.field public static final done_editing:I = 0x7f1208d4

.field public static final download_file_error_description:I = 0x7f1208d5

.field public static final download_file_error_title:I = 0x7f1208d6

.field public static final download_file_error_title_default:I = 0x7f1208d7

.field public static final download_invoice_pdf_path:I = 0x7f1208d8

.field public static final drafts:I = 0x7f1208d9

.field public static final drawer_history_drawer_report:I = 0x7f1208da

.field public static final drawer_history_email_drawer_report:I = 0x7f1208db

.field public static final drawer_history_email_message:I = 0x7f1208dc

.field public static final drawer_history_email_message_offline:I = 0x7f1208dd

.field public static final drawer_history_null_state_subtitle:I = 0x7f1208de

.field public static final drawer_history_null_state_title:I = 0x7f1208df

.field public static final drawer_history_previous_drawers_uppercase:I = 0x7f1208e0

.field public static final drawer_history_send_drawer_report:I = 0x7f1208e1

.field public static final drawer_history_unclosed_drawers_uppercase:I = 0x7f1208e2

.field public static final drop_down_accessibility_action_collapsed:I = 0x7f1208e3

.field public static final drop_down_accessibility_action_expanded:I = 0x7f1208e4

.field public static final due_date:I = 0x7f1208e5

.field public static final duplicate_account_message:I = 0x7f1208e6

.field public static final duplicate_account_title:I = 0x7f1208e7

.field public static final duplicate_gift_card_message:I = 0x7f1208e8

.field public static final duplicate_gift_card_title:I = 0x7f1208e9

.field public static final duplicate_sku_warning_item_description_multiple_variations:I = 0x7f1208ea

.field public static final duplicate_sku_warning_item_description_one_of_variations:I = 0x7f1208eb

.field public static final duplicate_sku_warning_item_description_only_one_variation:I = 0x7f1208ec

.field public static final duplicate_sku_warning_more_then_three_skus:I = 0x7f1208ed

.field public static final duplicate_sku_warning_one_sku_more_than_three_item:I = 0x7f1208ee

.field public static final duplicate_sku_warning_one_sku_one_item:I = 0x7f1208ef

.field public static final duplicate_sku_warning_one_sku_three_item:I = 0x7f1208f0

.field public static final duplicate_sku_warning_one_sku_two_item:I = 0x7f1208f1

.field public static final duplicate_sku_warning_three_skus:I = 0x7f1208f2

.field public static final duplicate_sku_warning_two_skus:I = 0x7f1208f3

.field public static final duplicate_sku_warning_unnamed:I = 0x7f1208f4

.field public static final duration:I = 0x7f1208f5

.field public static final duration_cancel:I = 0x7f1208f6

.field public static final duration_save:I = 0x7f1208f7

.field public static final duration_title:I = 0x7f1208f8

.field public static final edit:I = 0x7f1208f9

.field public static final edit_balance:I = 0x7f1208fa

.field public static final edit_balance_title:I = 0x7f1208fb

.field public static final edit_category_tile:I = 0x7f1208fc

.field public static final edit_deposit_title:I = 0x7f1208fd

.field public static final edit_discount:I = 0x7f1208fe

.field public static final edit_discount_all_items:I = 0x7f1208ff

.field public static final edit_discount_evenly_across_items:I = 0x7f120900

.field public static final edit_discount_item_plural:I = 0x7f120901

.field public static final edit_discount_item_single:I = 0x7f120902

.field public static final edit_discount_item_zero:I = 0x7f120903

.field public static final edit_invoice_add_customer:I = 0x7f120904

.field public static final edit_invoice_continue:I = 0x7f120905

.field public static final edit_invoice_copy_link:I = 0x7f120906

.field public static final edit_invoice_save:I = 0x7f120907

.field public static final edit_item:I = 0x7f120908

.field public static final edit_item_add_item_options_button_label:I = 0x7f120909

.field public static final edit_item_assign_unit_internet_not_available_alert_message:I = 0x7f12090a

.field public static final edit_item_assign_unit_internet_not_available_alert_title:I = 0x7f12090b

.field public static final edit_item_assign_unit_inventory_not_updated_alert_message:I = 0x7f12090c

.field public static final edit_item_assign_unit_inventory_not_updated_alert_title:I = 0x7f12090d

.field public static final edit_item_confirm_discard_changes_dialog_message:I = 0x7f12090e

.field public static final edit_item_confirm_discard_changes_dialog_title:I = 0x7f12090f

.field public static final edit_item_default_category_button_text:I = 0x7f120910

.field public static final edit_item_description:I = 0x7f120911

.field public static final edit_item_details_dashboard:I = 0x7f120912

.field public static final edit_item_edit_item_options_button_label:I = 0x7f120913

.field public static final edit_item_image_load_failed:I = 0x7f120914

.field public static final edit_item_item_option_help_text:I = 0x7f120915

.field public static final edit_item_item_options_section_header:I = 0x7f120916

.field public static final edit_item_item_options_unsupported_add_options_with_flat_variations_message:I = 0x7f120917

.field public static final edit_item_item_options_unsupported_add_options_with_flat_variations_title:I = 0x7f120918

.field public static final edit_item_item_options_unsupported_add_variation_message:I = 0x7f120919

.field public static final edit_item_item_options_unsupported_add_variation_title:I = 0x7f12091a

.field public static final edit_item_item_options_unsupported_add_variation_with_remote_variations_message:I = 0x7f12091b

.field public static final edit_item_item_options_unsupported_delete_variation_message:I = 0x7f12091c

.field public static final edit_item_item_options_unsupported_delete_variation_title:I = 0x7f12091d

.field public static final edit_item_item_options_unsupported_edit_options_with_remote_variations_message:I = 0x7f12091e

.field public static final edit_item_item_options_unsupported_edit_options_with_remote_variations_title:I = 0x7f12091f

.field public static final edit_item_label:I = 0x7f120920

.field public static final edit_item_modifier_set_dashboard_hint:I = 0x7f120921

.field public static final edit_item_modifier_sets_dashboard_hint:I = 0x7f120922

.field public static final edit_item_name_hint:I = 0x7f120923

.field public static final edit_item_name_none:I = 0x7f120924

.field public static final edit_item_name_required_dialog_message:I = 0x7f120925

.field public static final edit_item_name_required_dialog_title:I = 0x7f120926

.field public static final edit_item_photo_instructions:I = 0x7f120927

.field public static final edit_item_price_add:I = 0x7f120928

.field public static final edit_item_price_hint:I = 0x7f120929

.field public static final edit_item_price_message:I = 0x7f12092a

.field public static final edit_item_price_variable:I = 0x7f12092b

.field public static final edit_item_remove_variation:I = 0x7f12092c

.field public static final edit_item_select_category_none:I = 0x7f12092d

.field public static final edit_item_select_category_title:I = 0x7f12092e

.field public static final edit_item_set_label:I = 0x7f12092f

.field public static final edit_item_sku_none:I = 0x7f120930

.field public static final edit_item_unit_type:I = 0x7f120931

.field public static final edit_item_unit_type_default:I = 0x7f120932

.field public static final edit_item_unit_type_default_standard_unit_value:I = 0x7f120933

.field public static final edit_item_unit_type_selector_help_text:I = 0x7f120934

.field public static final edit_item_unit_type_value:I = 0x7f120935

.field public static final edit_items_hint:I = 0x7f120936

.field public static final edit_option_create_screen_title:I = 0x7f120937

.field public static final edit_option_display_name_section_hint:I = 0x7f120938

.field public static final edit_option_display_name_section_title:I = 0x7f120939

.field public static final edit_option_edit_screen_title:I = 0x7f12093a

.field public static final edit_option_invalid_option_duplicate_option_name_message:I = 0x7f12093b

.field public static final edit_option_invalid_option_duplicate_option_name_title:I = 0x7f12093c

.field public static final edit_option_invalid_option_duplicate_value_name_message:I = 0x7f12093d

.field public static final edit_option_invalid_option_duplicate_value_name_title:I = 0x7f12093e

.field public static final edit_option_invalid_option_empty_value_name_message:I = 0x7f12093f

.field public static final edit_option_invalid_option_empty_value_name_title:I = 0x7f120940

.field public static final edit_option_name_section_hint:I = 0x7f120941

.field public static final edit_option_name_section_title:I = 0x7f120942

.field public static final edit_option_new_value_hint:I = 0x7f120943

.field public static final edit_option_option_list_section_header:I = 0x7f120944

.field public static final edit_option_value_color_field_label:I = 0x7f120945

.field public static final edit_option_value_screen_delete_button_text:I = 0x7f120946

.field public static final edit_option_value_screen_title:I = 0x7f120947

.field public static final edit_option_values_section_header:I = 0x7f120948

.field public static final edit_payment_schedule:I = 0x7f120949

.field public static final edit_payment_title:I = 0x7f12094a

.field public static final edit_photo:I = 0x7f12094b

.field public static final edit_price_point:I = 0x7f12094c

.field public static final edit_price_point_help:I = 0x7f12094d

.field public static final edit_restaurant_item:I = 0x7f12094e

.field public static final edit_retail_item:I = 0x7f12094f

.field public static final edit_service:I = 0x7f120950

.field public static final edit_service_add_processing_time:I = 0x7f120951

.field public static final edit_service_after_appointment:I = 0x7f120952

.field public static final edit_service_assigned_employees:I = 0x7f120953

.field public static final edit_service_bookable_by_customers_online:I = 0x7f120954

.field public static final edit_service_call_for_price:I = 0x7f120955

.field public static final edit_service_cancellation_fee:I = 0x7f120956

.field public static final edit_service_display_price:I = 0x7f120957

.field public static final edit_service_display_price_help_label:I = 0x7f120958

.field public static final edit_service_display_price_hint:I = 0x7f120959

.field public static final edit_service_duration:I = 0x7f12095a

.field public static final edit_service_employees_all:I = 0x7f12095b

.field public static final edit_service_employees_none:I = 0x7f12095c

.field public static final edit_service_employees_plural:I = 0x7f12095d

.field public static final edit_service_employees_singular:I = 0x7f12095e

.field public static final edit_service_extra_time:I = 0x7f12095f

.field public static final edit_service_final_duration:I = 0x7f120960

.field public static final edit_service_fixed_price_type:I = 0x7f120961

.field public static final edit_service_initial_duration:I = 0x7f120962

.field public static final edit_service_no_price:I = 0x7f120963

.field public static final edit_service_price_type:I = 0x7f120964

.field public static final edit_service_price_type_helper_text:I = 0x7f120965

.field public static final edit_service_processing_duration:I = 0x7f120966

.field public static final edit_service_select_employees:I = 0x7f120967

.field public static final edit_service_select_price_type:I = 0x7f120968

.field public static final edit_service_starting_at:I = 0x7f120969

.field public static final edit_service_unit_type_default:I = 0x7f12096a

.field public static final edit_service_variable_price:I = 0x7f12096b

.field public static final edit_service_variable_price_type:I = 0x7f12096c

.field public static final edit_ticket_group:I = 0x7f12096d

.field public static final edit_unit_abbreviation_hint:I = 0x7f12096e

.field public static final edit_unit_duplicate_custom_unit_name_message:I = 0x7f12096f

.field public static final edit_unit_duplicate_custom_unit_name_title:I = 0x7f120970

.field public static final edit_unit_failed_alert_message_due_to_network:I = 0x7f120971

.field public static final edit_unit_failed_alert_message_not_due_to_network:I = 0x7f120972

.field public static final edit_unit_failed_alert_title:I = 0x7f120973

.field public static final edit_unit_name_hint:I = 0x7f120974

.field public static final edit_unit_precision_hint:I = 0x7f120975

.field public static final edit_unit_screen_delete_button_initial_stage_text:I = 0x7f120976

.field public static final edit_unit_screen_title_create_custom:I = 0x7f120977

.field public static final edit_unit_screen_title_create_standard:I = 0x7f120978

.field public static final edit_unit_screen_title_edit_custom:I = 0x7f120979

.field public static final edit_unit_screen_title_edit_standard:I = 0x7f12097a

.field public static final edit_unit_screen_title_edit_unknown_or_generic:I = 0x7f12097b

.field public static final edit_variation:I = 0x7f12097c

.field public static final egiftcard_choose_amount:I = 0x7f12097d

.field public static final egiftcard_choose_design:I = 0x7f12097e

.field public static final egiftcard_choose_email:I = 0x7f12097f

.field public static final egiftcard_choose_email_hint:I = 0x7f120980

.field public static final egiftcard_custom_amount:I = 0x7f120981

.field public static final egiftcard_custom_amount_hint:I = 0x7f120982

.field public static final egiftcard_custom_amount_title:I = 0x7f120983

.field public static final egiftcard_done:I = 0x7f120984

.field public static final egiftcard_registered_body:I = 0x7f120985

.field public static final egiftcard_registered_error:I = 0x7f120986

.field public static final egiftcard_registered_error_body:I = 0x7f120987

.field public static final egiftcard_registered_error_try_again:I = 0x7f120988

.field public static final egiftcard_registered_title:I = 0x7f120989

.field public static final egiftcard_return_to_cart_actionbar:I = 0x7f12098a

.field public static final egiftcard_x2_seller_amount:I = 0x7f12098b

.field public static final egiftcard_x2_seller_design:I = 0x7f12098c

.field public static final egiftcard_x2_seller_email:I = 0x7f12098d

.field public static final egiftcard_x2_seller_title:I = 0x7f12098e

.field public static final electronic_gift_card:I = 0x7f12098f

.field public static final email:I = 0x7f120990

.field public static final email_address:I = 0x7f120991

.field public static final email_domain_1:I = 0x7f120992

.field public static final email_domain_2:I = 0x7f120993

.field public static final email_domain_3:I = 0x7f120994

.field public static final email_domain_4:I = 0x7f120995

.field public static final email_domain_5:I = 0x7f120996

.field public static final email_domain_6:I = 0x7f120997

.field public static final email_mismatch:I = 0x7f120998

.field public static final email_mismatch_message:I = 0x7f120999

.field public static final email_report_connect_message:I = 0x7f12099a

.field public static final email_report_connect_message_title:I = 0x7f12099b

.field public static final email_report_edit_label:I = 0x7f12099c

.field public static final email_report_include_items:I = 0x7f12099d

.field public static final email_report_send_label:I = 0x7f12099e

.field public static final email_report_sent_message_title:I = 0x7f12099f

.field public static final email_report_title:I = 0x7f1209a0

.field public static final email_resending_failed:I = 0x7f1209a1

.field public static final email_resent_body:I = 0x7f1209a2

.field public static final email_resent_title:I = 0x7f1209a3

.field public static final email_suggestion:I = 0x7f1209a4

.field public static final email_support_ledger:I = 0x7f1209a5

.field public static final emoney:I = 0x7f1209a6

.field public static final emoney_amount:I = 0x7f1209a7

.field public static final emoney_authorizing:I = 0x7f1209a8

.field public static final emoney_brand_id:I = 0x7f1209a9

.field public static final emoney_brand_quicpay:I = 0x7f1209aa

.field public static final emoney_brand_suica:I = 0x7f1209ab

.field public static final emoney_cancel_dialog_subtitle:I = 0x7f1209ac

.field public static final emoney_canceling:I = 0x7f1209ad

.field public static final emoney_check_balance:I = 0x7f1209ae

.field public static final emoney_current_balance_message:I = 0x7f1209af

.field public static final emoney_minimum_amount_message:I = 0x7f1209b0

.field public static final emoney_miryo_cancel_dialog_discard:I = 0x7f1209b1

.field public static final emoney_miryo_cancel_dialog_keep:I = 0x7f1209b2

.field public static final emoney_miryo_cancel_dialog_subtitle:I = 0x7f1209b3

.field public static final emoney_miryo_cancel_dialog_title:I = 0x7f1209b4

.field public static final emoney_miryo_error:I = 0x7f1209b5

.field public static final emoney_miryo_error_no_reader:I = 0x7f1209b6

.field public static final emoney_miryo_error_subtitle:I = 0x7f1209b7

.field public static final emoney_miryo_failure_subtitle:I = 0x7f1209b8

.field public static final emoney_online_only:I = 0x7f1209b9

.field public static final emoney_online_processing:I = 0x7f1209ba

.field public static final emoney_payment:I = 0x7f1209bb

.field public static final emoney_payment_complete:I = 0x7f1209bc

.field public static final emoney_payment_complete_miryo_subtitle:I = 0x7f1209bd

.field public static final emoney_payment_error_cancel:I = 0x7f1209be

.field public static final emoney_payment_error_miryo_title:I = 0x7f1209bf

.field public static final emoney_payment_error_retry:I = 0x7f1209c0

.field public static final emoney_payment_error_title:I = 0x7f1209c1

.field public static final emoney_payment_title:I = 0x7f1209c2

.field public static final emoney_payment_title_with_balance:I = 0x7f1209c3

.field public static final emoney_payment_title_with_balance_check:I = 0x7f1209c4

.field public static final emoney_processing:I = 0x7f1209c5

.field public static final emoney_processing_message:I = 0x7f1209c6

.field public static final emoney_reader_disconnected:I = 0x7f1209c7

.field public static final emoney_reader_disconnected_subtitle:I = 0x7f1209c8

.field public static final emoney_reader_disconnected_title:I = 0x7f1209c9

.field public static final emoney_reader_incompatible:I = 0x7f1209ca

.field public static final emoney_reader_not_ready_title:I = 0x7f1209cb

.field public static final emoney_reader_timed_out:I = 0x7f1209cc

.field public static final emoney_selection_subtitle:I = 0x7f1209cd

.field public static final emoney_something_went_wrong:I = 0x7f1209ce

.field public static final emoney_split_payment:I = 0x7f1209cf

.field public static final emoney_tap_again:I = 0x7f1209d0

.field public static final emoney_tap_card_prompt:I = 0x7f1209d1

.field public static final emoney_tmn_msg_common_error:I = 0x7f1209d2

.field public static final emoney_tmn_msg_different_card:I = 0x7f1209d3

.field public static final emoney_tmn_msg_exceeded_limit_amount:I = 0x7f1209d4

.field public static final emoney_tmn_msg_expired_card:I = 0x7f1209d5

.field public static final emoney_tmn_msg_insufficient_balance:I = 0x7f1209d6

.field public static final emoney_tmn_msg_invalid_card:I = 0x7f1209d7

.field public static final emoney_tmn_msg_locked_mobile_service:I = 0x7f1209d8

.field public static final emoney_tmn_msg_read_error:I = 0x7f1209d9

.field public static final emoney_tmn_msg_several_suica_card:I = 0x7f1209da

.field public static final emoney_tmn_msg_write_error:I = 0x7f1209db

.field public static final emoney_unknown_error:I = 0x7f1209dc

.field public static final emoney_verify_balance:I = 0x7f1209dd

.field public static final emoney_verify_balance_failed_title:I = 0x7f1209de

.field public static final employee_management_break_tracking_end_break_button_text:I = 0x7f1209df

.field public static final employee_management_break_tracking_end_break_early_button_text:I = 0x7f1209e0

.field public static final employee_management_break_tracking_end_break_late_message:I = 0x7f1209e1

.field public static final employee_management_break_tracking_end_break_message:I = 0x7f1209e2

.field public static final employee_management_break_tracking_end_break_one_min_message:I = 0x7f1209e3

.field public static final employee_management_break_tracking_end_break_progress_title:I = 0x7f1209e4

.field public static final employee_management_break_tracking_end_break_sub_one_min_message:I = 0x7f1209e5

.field public static final employee_management_break_tracking_end_break_success_message:I = 0x7f1209e6

.field public static final employee_management_break_tracking_general_error_message:I = 0x7f1209e7

.field public static final employee_management_break_tracking_general_error_title:I = 0x7f1209e8

.field public static final employee_management_break_tracking_list_button_text:I = 0x7f1209e9

.field public static final employee_management_break_tracking_list_button_text_one_min:I = 0x7f1209ea

.field public static final employee_management_break_tracking_list_title:I = 0x7f1209eb

.field public static final employee_management_break_tracking_list_title_one_break_def:I = 0x7f1209ec

.field public static final employee_management_break_tracking_modal_log_out:I = 0x7f1209ed

.field public static final employee_management_break_tracking_modal_message:I = 0x7f1209ee

.field public static final employee_management_break_tracking_modal_message_break_over:I = 0x7f1209ef

.field public static final employee_management_break_tracking_modal_message_one_min:I = 0x7f1209f0

.field public static final employee_management_break_tracking_modal_message_sub_one_min:I = 0x7f1209f1

.field public static final employee_management_break_tracking_modal_title_not_over:I = 0x7f1209f2

.field public static final employee_management_break_tracking_modal_title_over:I = 0x7f1209f3

.field public static final employee_management_break_tracking_no_internet_error_message:I = 0x7f1209f4

.field public static final employee_management_break_tracking_no_internet_error_title:I = 0x7f1209f5

.field public static final employee_management_break_tracking_sheet_title_not_over:I = 0x7f1209f6

.field public static final employee_management_break_tracking_sheet_title_over:I = 0x7f1209f7

.field public static final employee_management_break_tracking_shift_summary:I = 0x7f1209f8

.field public static final employee_management_break_tracking_start_button_text:I = 0x7f1209f9

.field public static final employee_management_break_tracking_start_error_message:I = 0x7f1209fa

.field public static final employee_management_break_tracking_start_error_title:I = 0x7f1209fb

.field public static final employee_management_break_tracking_start_progress_title:I = 0x7f1209fc

.field public static final employee_management_break_tracking_start_success_message:I = 0x7f1209fd

.field public static final employee_management_break_tracking_start_success_title:I = 0x7f1209fe

.field public static final employee_management_break_tracking_success_continue_button_text:I = 0x7f1209ff

.field public static final employee_management_break_tracking_switch_jobs_button_text:I = 0x7f120a00

.field public static final employee_management_clock_in_button:I = 0x7f120a01

.field public static final employee_management_clock_in_confirmation_message:I = 0x7f120a02

.field public static final employee_management_clock_in_confirmation_sub_header:I = 0x7f120a03

.field public static final employee_management_clock_in_out:I = 0x7f120a04

.field public static final employee_management_clock_in_out_content_description:I = 0x7f120a05

.field public static final employee_management_clock_in_progress_title:I = 0x7f120a06

.field public static final employee_management_clock_out_button_text:I = 0x7f120a07

.field public static final employee_management_clock_out_confirmation_button_text:I = 0x7f120a08

.field public static final employee_management_clock_out_confirmation_shift_summary:I = 0x7f120a09

.field public static final employee_management_clock_out_confirmation_shift_summary_job:I = 0x7f120a0a

.field public static final employee_management_clock_out_progress_title:I = 0x7f120a0b

.field public static final employee_management_clock_out_success_shift_summary:I = 0x7f120a0c

.field public static final employee_management_clock_out_success_title:I = 0x7f120a0d

.field public static final employee_management_clock_out_success_title_multiple_wages:I = 0x7f120a0e

.field public static final employee_management_dashboard_url:I = 0x7f120a0f

.field public static final employee_management_enabled_toggle:I = 0x7f120a10

.field public static final employee_management_enabled_toggle_description:I = 0x7f120a11

.field public static final employee_management_enabled_toggle_description_dashboard:I = 0x7f120a12

.field public static final employee_management_enabled_toggle_short:I = 0x7f120a13

.field public static final employee_management_guest_button:I = 0x7f120a14

.field public static final employee_management_guest_mode_toggle:I = 0x7f120a15

.field public static final employee_management_guest_mode_toggle_description:I = 0x7f120a16

.field public static final employee_management_guest_mode_toggle_short:I = 0x7f120a17

.field public static final employee_management_jobs_list_modal_message:I = 0x7f120a18

.field public static final employee_management_jobs_list_title:I = 0x7f120a19

.field public static final employee_management_lock_button_full:I = 0x7f120a1a

.field public static final employee_management_lock_button_guest:I = 0x7f120a1b

.field public static final employee_management_lock_button_login:I = 0x7f120a1c

.field public static final employee_management_lock_button_logout:I = 0x7f120a1d

.field public static final employee_management_logged_out:I = 0x7f120a1e

.field public static final employee_management_off:I = 0x7f120a1f

.field public static final employee_management_on:I = 0x7f120a20

.field public static final employee_management_passcode_description:I = 0x7f120a21

.field public static final employee_management_passcode_description_link_text:I = 0x7f120a22

.field public static final employee_management_passcode_toggle_always:I = 0x7f120a23

.field public static final employee_management_passcode_toggle_never:I = 0x7f120a24

.field public static final employee_management_passcode_toggle_restricted:I = 0x7f120a25

.field public static final employee_management_passcode_uppercase_heading:I = 0x7f120a26

.field public static final employee_management_timecard_hrs_fraction:I = 0x7f120a27

.field public static final employee_management_timecard_hrs_mins_zero:I = 0x7f120a28

.field public static final employee_management_timecard_hrs_plural:I = 0x7f120a29

.field public static final employee_management_timecard_hrs_singular:I = 0x7f120a2a

.field public static final employee_management_timecard_mins_plural:I = 0x7f120a2b

.field public static final employee_management_timecard_mins_singular:I = 0x7f120a2c

.field public static final employee_management_timeout_option_toggle_1m:I = 0x7f120a2d

.field public static final employee_management_timeout_option_toggle_30s:I = 0x7f120a2e

.field public static final employee_management_timeout_option_toggle_5m:I = 0x7f120a2f

.field public static final employee_management_timeout_option_toggle_never:I = 0x7f120a30

.field public static final employee_management_title:I = 0x7f120a31

.field public static final employee_management_track_time_toggle:I = 0x7f120a32

.field public static final employee_management_track_time_toggle_description:I = 0x7f120a33

.field public static final employee_management_track_time_toggle_description_old:I = 0x7f120a34

.field public static final employee_management_track_time_toggle_short:I = 0x7f120a35

.field public static final employee_management_transaction_lock_mode_toggle:I = 0x7f120a36

.field public static final employee_short_name:I = 0x7f120a37

.field public static final employee_short_name_unassigned:I = 0x7f120a38

.field public static final employee_short_name_unknown:I = 0x7f120a39

.field public static final empty:I = 0x7f120a3a

.field public static final empty_card_activity_message:I = 0x7f120a3b

.field public static final empty_discount_standalone_percent_character:I = 0x7f120a3c

.field public static final empty_disputes_title:I = 0x7f120a3d

.field public static final empty_string:I = 0x7f120a3e

.field public static final emv_account_selection_credit:I = 0x7f120a3f

.field public static final emv_account_selection_debit:I = 0x7f120a40

.field public static final emv_account_selection_default:I = 0x7f120a41

.field public static final emv_account_selection_savings:I = 0x7f120a42

.field public static final emv_app_update_required_button:I = 0x7f120a43

.field public static final emv_app_update_required_message:I = 0x7f120a44

.field public static final emv_app_update_required_title:I = 0x7f120a45

.field public static final emv_cancel_payment:I = 0x7f120a46

.field public static final emv_canceled:I = 0x7f120a47

.field public static final emv_card_error_message:I = 0x7f120a48

.field public static final emv_card_error_title:I = 0x7f120a49

.field public static final emv_card_removed_msg:I = 0x7f120a4a

.field public static final emv_card_removed_msg_not_canceled:I = 0x7f120a4b

.field public static final emv_card_removed_title:I = 0x7f120a4c

.field public static final emv_contact_support:I = 0x7f120a4d

.field public static final emv_declined:I = 0x7f120a4e

.field public static final emv_device_unsupported_msg:I = 0x7f120a4f

.field public static final emv_device_unsupported_title:I = 0x7f120a50

.field public static final emv_do_not_remove_card:I = 0x7f120a51

.field public static final emv_fallback_title:I = 0x7f120a52

.field public static final emv_fwup_fail_msg:I = 0x7f120a53

.field public static final emv_fwup_fail_title:I = 0x7f120a54

.field public static final emv_must_dip_msg:I = 0x7f120a55

.field public static final emv_must_dip_title:I = 0x7f120a56

.field public static final emv_pin_blocked_msg:I = 0x7f120a57

.field public static final emv_pin_blocked_title:I = 0x7f120a58

.field public static final emv_pin_card_info:I = 0x7f120a59

.field public static final emv_pin_final_retry:I = 0x7f120a5a

.field public static final emv_prepare_payment:I = 0x7f120a5b

.field public static final emv_reader_disconnected_msg:I = 0x7f120a5c

.field public static final emv_reader_disconnected_title:I = 0x7f120a5d

.field public static final emv_reader_error_msg:I = 0x7f120a5e

.field public static final emv_reader_tampered_msg:I = 0x7f120a5f

.field public static final emv_reader_tampered_title:I = 0x7f120a60

.field public static final emv_request_swipe_activity_search_message:I = 0x7f120a61

.field public static final emv_request_tap_activity_search_message:I = 0x7f120a62

.field public static final emv_request_tap_activity_search_title:I = 0x7f120a63

.field public static final emv_request_tap_payment_flow_card_message:I = 0x7f120a64

.field public static final emv_request_tap_payment_flow_card_title:I = 0x7f120a65

.field public static final emv_request_tap_refund:I = 0x7f120a66

.field public static final emv_scheme_fallback_msg:I = 0x7f120a67

.field public static final emv_securesession_denied_msg:I = 0x7f120a68

.field public static final emv_securesession_failed_msg:I = 0x7f120a69

.field public static final emv_securesession_failed_title:I = 0x7f120a6a

.field public static final emv_select_account:I = 0x7f120a6b

.field public static final emv_std_msg_try_again_message:I = 0x7f120a6c

.field public static final emv_std_msg_try_again_title:I = 0x7f120a6d

.field public static final emv_tech_fallback_msg:I = 0x7f120a6e

.field public static final emv_unknown_error:I = 0x7f120a6f

.field public static final emv_warning_screen_done:I = 0x7f120a70

.field public static final enable_device_settings_description:I = 0x7f120a71

.field public static final enable_device_settings_learn_more:I = 0x7f120a72

.field public static final enable_device_settings_title:I = 0x7f120a73

.field public static final enable_offline_mode:I = 0x7f120a74

.field public static final enabled:I = 0x7f120a75

.field public static final end:I = 0x7f120a76

.field public static final end_cash_drawer_shift:I = 0x7f120a77

.field public static final end_date:I = 0x7f120a78

.field public static final ended_at_label:I = 0x7f120a79

.field public static final ends_at_label:I = 0x7f120a7a

.field public static final enter_offline_mode:I = 0x7f120a7b

.field public static final enter_passcode:I = 0x7f120a7c

.field public static final enter_passcode_account_owner:I = 0x7f120a7d

.field public static final enter_passcode_account_owner_or_admin:I = 0x7f120a7e

.field public static final enter_reward_code_hint:I = 0x7f120a7f

.field public static final enter_reward_code_hint_alphanumeric:I = 0x7f120a80

.field public static final enter_reward_code_title:I = 0x7f120a81

.field public static final enter_your_pin:I = 0x7f120a82

.field public static final error_card_activity_message:I = 0x7f120a83

.field public static final error_default:I = 0x7f120a84

.field public static final error_icon_content_description:I = 0x7f120a85

.field public static final error_updating_expense_category:I = 0x7f120a86

.field public static final estimate:I = 0x7f120a87

.field public static final estimate_banner_download_invoices_app:I = 0x7f120a88

.field public static final estimate_banner_message_has_estimates:I = 0x7f120a89

.field public static final estimate_banner_message_no_estimates:I = 0x7f120a8a

.field public static final estimate_banner_title_has_estimates:I = 0x7f120a8b

.field public static final estimate_banner_title_no_estimates:I = 0x7f120a8c

.field public static final estimate_banner_view_in_invoices_app:I = 0x7f120a8d

.field public static final estimate_details_title:I = 0x7f120a8e

.field public static final estimated_completion_date:I = 0x7f120a8f

.field public static final eventstream_product_name:I = 0x7f120a90

.field public static final exchange:I = 0x7f120a91

.field public static final exchange_with_reason:I = 0x7f120a92

.field public static final exit_tutorial_dialog_message:I = 0x7f120a93

.field public static final exit_tutorial_dialog_title:I = 0x7f120a94

.field public static final expected_verified_date:I = 0x7f120a95

.field public static final expense_type_business:I = 0x7f120a96

.field public static final expense_type_personal:I = 0x7f120a97

.field public static final expiration_content_description:I = 0x7f120a98

.field public static final expiration_date_content_description:I = 0x7f120a99

.field public static final expiration_date_hint:I = 0x7f120a9a

.field public static final expiration_date_label:I = 0x7f120a9b

.field public static final expiration_hint:I = 0x7f120a9c

.field public static final expiration_policy:I = 0x7f120a9d

.field public static final export_report_email_label:I = 0x7f120a9e

.field public static final export_report_label:I = 0x7f120a9f

.field public static final export_report_print_label:I = 0x7f120aa0

.field public static final exposed_dropdown_menu_content_description:I = 0x7f120aa1

.field public static final external_file_viewing_failed:I = 0x7f120aa2

.field public static final extra_time_helper_text:I = 0x7f120aa3

.field public static final fab_transformation_scrim_behavior:I = 0x7f120aa4

.field public static final fab_transformation_sheet_behavior:I = 0x7f120aa5

.field public static final facebook_url:I = 0x7f120aa6

.field public static final failed:I = 0x7f120aa7

.field public static final failed_restock_attempt:I = 0x7f120aa8

.field public static final favorite_tile_all_discounts:I = 0x7f120aa9

.field public static final favorite_tile_all_items:I = 0x7f120aaa

.field public static final favorite_tile_all_items_and_services:I = 0x7f120aab

.field public static final favorite_tile_rewards_search:I = 0x7f120aac

.field public static final favorite_tile_unknown_abbrev:I = 0x7f120aad

.field public static final favorite_tile_unknown_clicked:I = 0x7f120aae

.field public static final favorite_tile_unknown_name:I = 0x7f120aaf

.field public static final favorite_tooltip_message:I = 0x7f120ab0

.field public static final favorite_tooltip_title:I = 0x7f120ab1

.field public static final fcm_fallback_notification_channel_label:I = 0x7f120ab2

.field public static final feature_tour:I = 0x7f120ab3

.field public static final feature_tour_subtext:I = 0x7f120ab4

.field public static final fee_walkthrough:I = 0x7f120ab5

.field public static final fee_walkthrough_subtext:I = 0x7f120ab6

.field public static final feedback_and_more:I = 0x7f120ab7

.field public static final feedback_error:I = 0x7f120ab8

.field public static final feedback_success:I = 0x7f120ab9

.field public static final feedback_url:I = 0x7f120aba

.field public static final fetching_card_details_spinner_message:I = 0x7f120abb

.field public static final field_validation_error:I = 0x7f120abc

.field public static final final_time:I = 0x7f120abd

.field public static final finalize_account_setup:I = 0x7f120abe

.field public static final finish:I = 0x7f120abf

.field public static final firmware_update_notification_title:I = 0x7f120ac0

.field public static final firmware_updating:I = 0x7f120ac1

.field public static final firmware_version:I = 0x7f120ac2

.field public static final first_invoice_tutorial_add_customer:I = 0x7f120ac3

.field public static final first_invoice_tutorial_add_customer_info:I = 0x7f120ac4

.field public static final first_invoice_tutorial_add_customer_info_save:I = 0x7f120ac5

.field public static final first_invoice_tutorial_add_item:I = 0x7f120ac6

.field public static final first_invoice_tutorial_add_item_info:I = 0x7f120ac7

.field public static final first_invoice_tutorial_add_item_info_save:I = 0x7f120ac8

.field public static final first_invoice_tutorial_choose_customer:I = 0x7f120ac9

.field public static final first_invoice_tutorial_choose_item:I = 0x7f120aca

.field public static final first_invoice_tutorial_create:I = 0x7f120acb

.field public static final first_invoice_tutorial_preview:I = 0x7f120acc

.field public static final first_invoice_tutorial_preview_confirm:I = 0x7f120acd

.field public static final first_invoice_walkthrough:I = 0x7f120ace

.field public static final first_invoice_walkthrough_subtext:I = 0x7f120acf

.field public static final first_name:I = 0x7f120ad0

.field public static final first_payment_dialog_action:I = 0x7f120ad1

.field public static final first_payment_dialog_content:I = 0x7f120ad2

.field public static final first_payment_dialog_skip_walkthrough:I = 0x7f120ad3

.field public static final first_payment_walkthrough:I = 0x7f120ad4

.field public static final first_payment_walkthrough_subtext:I = 0x7f120ad5

.field public static final footnote_message_adjustments:I = 0x7f120ad6

.field public static final foreground_service_notification_message:I = 0x7f120ad7

.field public static final foreground_service_notification_title:I = 0x7f120ad8

.field public static final forgot_password:I = 0x7f120ad9

.field public static final forgot_password_failed:I = 0x7f120ada

.field public static final forgot_password_send:I = 0x7f120adb

.field public static final forgot_password_sending:I = 0x7f120adc

.field public static final forgot_password_subtitle:I = 0x7f120add

.field public static final forgot_password_title:I = 0x7f120ade

.field public static final forward_button_content_description:I = 0x7f120adf

.field public static final free:I = 0x7f120ae0

.field public static final free_processing_url:I = 0x7f120ae1

.field public static final freeze_notification_message:I = 0x7f120ae2

.field public static final freeze_notification_title:I = 0x7f120ae3

.field public static final fridays:I = 0x7f120ae4

.field public static final full_refund:I = 0x7f120ae5

.field public static final gen2_denial_body:I = 0x7f120ae6

.field public static final gen2_denial_cancel:I = 0x7f120ae7

.field public static final gen2_denial_confirm:I = 0x7f120ae8

.field public static final gen2_denial_learn_more:I = 0x7f120ae9

.field public static final gen2_denial_title:I = 0x7f120aea

.field public static final gen2_eol_learn_more_url:I = 0x7f120aeb

.field public static final gen2_eol_request_reader_url:I = 0x7f120aec

.field public static final generic_unit_name:I = 0x7f120aed

.field public static final get_direct_debit_info_failure_message:I = 0x7f120aee

.field public static final get_direct_debit_info_failure_title:I = 0x7f120aef

.field public static final get_help_with_card:I = 0x7f120af0

.field public static final get_new_reader:I = 0x7f120af1

.field public static final get_set_up:I = 0x7f120af2

.field public static final get_started:I = 0x7f120af3

.field public static final getting_started_accept_payment:I = 0x7f120af4

.field public static final getting_started_accept_payment_link:I = 0x7f120af5

.field public static final getting_started_customer_display_position:I = 0x7f120af6

.field public static final getting_started_customer_display_position_link:I = 0x7f120af7

.field public static final gift_card:I = 0x7f120af8

.field public static final gift_card_account_unsupported_message:I = 0x7f120af9

.field public static final gift_card_account_unsupported_title:I = 0x7f120afa

.field public static final gift_card_activating:I = 0x7f120afb

.field public static final gift_card_activation_error_message:I = 0x7f120afc

.field public static final gift_card_activation_failed:I = 0x7f120afd

.field public static final gift_card_activation_hint:I = 0x7f120afe

.field public static final gift_card_activity_header_caps:I = 0x7f120aff

.field public static final gift_card_add_value:I = 0x7f120b00

.field public static final gift_card_add_value_uppercase:I = 0x7f120b01

.field public static final gift_card_check_balance:I = 0x7f120b02

.field public static final gift_card_checking:I = 0x7f120b03

.field public static final gift_card_checking_failure:I = 0x7f120b04

.field public static final gift_card_checking_failure_not_active_desc:I = 0x7f120b05

.field public static final gift_card_checking_failure_not_active_title:I = 0x7f120b06

.field public static final gift_card_choose_type_hint:I = 0x7f120b07

.field public static final gift_card_clear_balance:I = 0x7f120b08

.field public static final gift_card_clear_balance_amount:I = 0x7f120b09

.field public static final gift_card_clear_balance_failure:I = 0x7f120b0a

.field public static final gift_card_clear_balance_reason_cash_out:I = 0x7f120b0b

.field public static final gift_card_clear_balance_reason_lost_gift_card:I = 0x7f120b0c

.field public static final gift_card_clear_balance_reason_other:I = 0x7f120b0d

.field public static final gift_card_clear_balance_reason_other_hint:I = 0x7f120b0e

.field public static final gift_card_clear_balance_reason_reset_gift_card:I = 0x7f120b0f

.field public static final gift_card_clear_balance_reason_title_caps:I = 0x7f120b10

.field public static final gift_card_clear_balance_warning:I = 0x7f120b11

.field public static final gift_card_current_balance_label:I = 0x7f120b12

.field public static final gift_card_custom:I = 0x7f120b13

.field public static final gift_card_custom_amount:I = 0x7f120b14

.field public static final gift_card_enter_or_swipe:I = 0x7f120b15

.field public static final gift_card_hint_url:I = 0x7f120b16

.field public static final gift_card_history_load_failure:I = 0x7f120b17

.field public static final gift_card_invalid_subtitle:I = 0x7f120b18

.field public static final gift_card_invalid_title:I = 0x7f120b19

.field public static final gift_card_load:I = 0x7f120b1a

.field public static final gift_card_load_amount:I = 0x7f120b1b

.field public static final gift_card_lookup_hint:I = 0x7f120b1c

.field public static final gift_card_name_balance_format:I = 0x7f120b1d

.field public static final gift_card_number_hint:I = 0x7f120b1e

.field public static final gift_card_number_hint_enter_card:I = 0x7f120b1f

.field public static final gift_card_on_file:I = 0x7f120b20

.field public static final gift_card_on_file_action_bar_title:I = 0x7f120b21

.field public static final gift_card_order_url:I = 0x7f120b22

.field public static final gift_card_purchase_limit_exceeded_message:I = 0x7f120b23

.field public static final gift_card_sell_e_gift_card:I = 0x7f120b24

.field public static final gift_card_sell_physical_gift_card:I = 0x7f120b25

.field public static final gift_card_unsupported_message:I = 0x7f120b26

.field public static final gift_card_unsupported_message_keyed:I = 0x7f120b27

.field public static final gift_card_unsupported_title:I = 0x7f120b28

.field public static final gift_card_uppercase:I = 0x7f120b29

.field public static final gift_card_value:I = 0x7f120b2a

.field public static final gift_cards_on_file:I = 0x7f120b2b

.field public static final giftcards_settings_egift_enable_in_pos:I = 0x7f120b2c

.field public static final giftcards_settings_egiftcard_custom_policy_hint:I = 0x7f120b2d

.field public static final giftcards_settings_egiftcard_custom_policy_uppercase:I = 0x7f120b2e

.field public static final giftcards_settings_egiftcard_designs:I = 0x7f120b2f

.field public static final giftcards_settings_egiftcard_designs_add:I = 0x7f120b30

.field public static final giftcards_settings_egiftcard_designs_add_action_button:I = 0x7f120b31

.field public static final giftcards_settings_egiftcard_designs_amount_plural:I = 0x7f120b32

.field public static final giftcards_settings_egiftcard_designs_amount_singular:I = 0x7f120b33

.field public static final giftcards_settings_egiftcard_designs_crop_photo_action:I = 0x7f120b34

.field public static final giftcards_settings_egiftcard_designs_crop_photo_error:I = 0x7f120b35

.field public static final giftcards_settings_egiftcard_designs_crop_photo_title:I = 0x7f120b36

.field public static final giftcards_settings_egiftcard_designs_upload_custom:I = 0x7f120b37

.field public static final giftcards_settings_egiftcard_designs_upload_custom_failed:I = 0x7f120b38

.field public static final giftcards_settings_egiftcard_designs_upload_custom_not_available:I = 0x7f120b39

.field public static final giftcards_settings_egiftcard_designs_upload_custom_success:I = 0x7f120b3a

.field public static final giftcards_settings_egiftcard_min_max_load_uppercase:I = 0x7f120b3b

.field public static final giftcards_settings_egiftcard_section_title_uppercase:I = 0x7f120b3c

.field public static final giftcards_settings_load_error:I = 0x7f120b3d

.field public static final giftcards_settings_plastic_description:I = 0x7f120b3e

.field public static final giftcards_settings_plastic_description_link_text:I = 0x7f120b3f

.field public static final giftcards_settings_save_error:I = 0x7f120b40

.field public static final giftcards_settings_title:I = 0x7f120b41

.field public static final giftcards_url:I = 0x7f120b42

.field public static final git_sha:I = 0x7f120b43

.field public static final go_back:I = 0x7f120b44

.field public static final go_to_google_play:I = 0x7f120b45

.field public static final google_play_square_pos_url:I = 0x7f120b46

.field public static final gross_amount_atm_withdrawal:I = 0x7f120b47

.field public static final gross_amount_cashback:I = 0x7f120b48

.field public static final gross_amount_instant_transfer_fee:I = 0x7f120b49

.field public static final hardware_disclaimer:I = 0x7f120b4a

.field public static final hardware_disclaimer_detail:I = 0x7f120b4b

.field public static final hardware_serial_number:I = 0x7f120b4c

.field public static final have_feedback:I = 0x7f120b4d

.field public static final hello_world:I = 0x7f120b4e

.field public static final help:I = 0x7f120b4f

.field public static final help_url:I = 0x7f120b50

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f120b51

.field public static final hide_instant_deposit_hint:I = 0x7f120b52

.field public static final holder_name:I = 0x7f120b53

.field public static final hs__agent_message_voice_over:I = 0x7f120b54

.field public static final hs__agent_message_with_name_voice_over:I = 0x7f120b55

.field public static final hs__app_review_button:I = 0x7f120b56

.field public static final hs__attach_screenshot_btn:I = 0x7f120b57

.field public static final hs__attachment_downloaded__voice_over:I = 0x7f120b58

.field public static final hs__attachment_downloading_voice_over:I = 0x7f120b59

.field public static final hs__attachment_not_downloaded_voice_over:I = 0x7f120b5a

.field public static final hs__authentication_failed:I = 0x7f120b5b

.field public static final hs__ca_msg:I = 0x7f120b5c

.field public static final hs__change_btn:I = 0x7f120b5d

.field public static final hs__chat_hint:I = 0x7f120b5e

.field public static final hs__confirmation_footer_msg:I = 0x7f120b5f

.field public static final hs__confirmation_msg:I = 0x7f120b60

.field public static final hs__connecting:I = 0x7f120b61

.field public static final hs__contact_us_btn:I = 0x7f120b62

.field public static final hs__conversation_detail_error:I = 0x7f120b63

.field public static final hs__conversation_end_msg:I = 0x7f120b64

.field public static final hs__conversation_header:I = 0x7f120b65

.field public static final hs__conversation_issue_id_header:I = 0x7f120b66

.field public static final hs__conversation_publish_id_voice_over:I = 0x7f120b67

.field public static final hs__conversation_redacted_status:I = 0x7f120b68

.field public static final hs__conversation_redacted_status_multiple:I = 0x7f120b69

.field public static final hs__conversation_rejected_status:I = 0x7f120b6a

.field public static final hs__conversation_started_message:I = 0x7f120b6b

.field public static final hs__conversations_divider_voice_over:I = 0x7f120b6c

.field public static final hs__copied_to_clipboard:I = 0x7f120b6d

.field public static final hs__copy:I = 0x7f120b6e

.field public static final hs__copy_to_clipboard_tooltip:I = 0x7f120b6f

.field public static final hs__could_not_open_attachment_msg:I = 0x7f120b70

.field public static final hs__could_not_reach_support_msg:I = 0x7f120b71

.field public static final hs__cr_msg:I = 0x7f120b72

.field public static final hs__csat_additonal_feedback_message:I = 0x7f120b73

.field public static final hs__csat_dislike_message:I = 0x7f120b74

.field public static final hs__csat_disliked_rating_message:I = 0x7f120b75

.field public static final hs__csat_like_message:I = 0x7f120b76

.field public static final hs__csat_liked_rating_message:I = 0x7f120b77

.field public static final hs__csat_message:I = 0x7f120b78

.field public static final hs__csat_ok_rating_message:I = 0x7f120b79

.field public static final hs__csat_option_message:I = 0x7f120b7a

.field public static final hs__csat_ratingbar:I = 0x7f120b7b

.field public static final hs__csat_submit_toast:I = 0x7f120b7c

.field public static final hs__data_not_found_msg:I = 0x7f120b7d

.field public static final hs__date_input_validation_error:I = 0x7f120b7e

.field public static final hs__default_notification_channel_desc:I = 0x7f120b7f

.field public static final hs__default_notification_channel_name:I = 0x7f120b80

.field public static final hs__default_notification_content_title:I = 0x7f120b81

.field public static final hs__description_invalid_length_error:I = 0x7f120b82

.field public static final hs__done_btn:I = 0x7f120b83

.field public static final hs__email_hint:I = 0x7f120b84

.field public static final hs__email_input_validation_error:I = 0x7f120b85

.field public static final hs__email_required_hint:I = 0x7f120b86

.field public static final hs__faq_fetching_fail_message:I = 0x7f120b87

.field public static final hs__faqs_search_footer:I = 0x7f120b88

.field public static final hs__feedback_button:I = 0x7f120b89

.field public static final hs__fetching_faqs_message:I = 0x7f120b8a

.field public static final hs__file_not_found_msg:I = 0x7f120b8b

.field public static final hs__help_header:I = 0x7f120b8c

.field public static final hs__image_downloaded_voice_over:I = 0x7f120b8d

.field public static final hs__image_downloading_voice_over:I = 0x7f120b8e

.field public static final hs__image_not_downloaded_voice_over:I = 0x7f120b8f

.field public static final hs__invalid_description_error:I = 0x7f120b90

.field public static final hs__invalid_email_error:I = 0x7f120b91

.field public static final hs__invalid_faq_publish_id_error:I = 0x7f120b92

.field public static final hs__invalid_section_publish_id_error:I = 0x7f120b93

.field public static final hs__issue_archival_message:I = 0x7f120b94

.field public static final hs__jump_button_voice_over:I = 0x7f120b95

.field public static final hs__jump_button_with_new_message_voice_over:I = 0x7f120b96

.field public static final hs__landscape_date_input_validation_error:I = 0x7f120b97

.field public static final hs__landscape_email_input_validation_error:I = 0x7f120b98

.field public static final hs__landscape_input_validation_dialog_title:I = 0x7f120b99

.field public static final hs__landscape_number_input_validation_error:I = 0x7f120b9a

.field public static final hs__mark_no:I = 0x7f120b9b

.field public static final hs__mark_yes:I = 0x7f120b9c

.field public static final hs__mark_yes_no_question:I = 0x7f120b9d

.field public static final hs__message_not_sent:I = 0x7f120b9e

.field public static final hs__messages_loading_error_text:I = 0x7f120b9f

.field public static final hs__messages_loading_text:I = 0x7f120ba0

.field public static final hs__network_error_msg:I = 0x7f120ba1

.field public static final hs__network_error_pre_issue_creation:I = 0x7f120ba2

.field public static final hs__network_reconnecting_error:I = 0x7f120ba3

.field public static final hs__network_unavailable_msg:I = 0x7f120ba4

.field public static final hs__new_conversation_btn:I = 0x7f120ba5

.field public static final hs__new_conversation_footer_generic_reason:I = 0x7f120ba6

.field public static final hs__new_conversation_header:I = 0x7f120ba7

.field public static final hs__new_conversation_hint:I = 0x7f120ba8

.field public static final hs__no_internet_error:I = 0x7f120ba9

.field public static final hs__no_internet_error_mgs:I = 0x7f120baa

.field public static final hs__no_search_results_message:I = 0x7f120bab

.field public static final hs__number_input_validation_error:I = 0x7f120bac

.field public static final hs__permission_denied_message:I = 0x7f120bad

.field public static final hs__permission_denied_snackbar_action:I = 0x7f120bae

.field public static final hs__permission_not_granted:I = 0x7f120baf

.field public static final hs__permission_rationale_snackbar_action_label:I = 0x7f120bb0

.field public static final hs__picker_clear_query_voice_over:I = 0x7f120bb1

.field public static final hs__picker_no_results:I = 0x7f120bb2

.field public static final hs__picker_option_list_item_voice_over:I = 0x7f120bb3

.field public static final hs__picker_options_expand_header_voice_over:I = 0x7f120bb4

.field public static final hs__picker_options_list_collapse_btn_voice_over:I = 0x7f120bb5

.field public static final hs__picker_search_edit_back_btn_voice_over:I = 0x7f120bb6

.field public static final hs__picker_search_hint:I = 0x7f120bb7

.field public static final hs__picker_search_icon_voice_over:I = 0x7f120bb8

.field public static final hs__question_header:I = 0x7f120bb9

.field public static final hs__question_helpful_message:I = 0x7f120bba

.field public static final hs__question_unhelpful_message:I = 0x7f120bbb

.field public static final hs__rate_button:I = 0x7f120bbc

.field public static final hs__remove_screenshot_btn:I = 0x7f120bbd

.field public static final hs__retry_button_voice_over:I = 0x7f120bbe

.field public static final hs__retry_faq_fetching_button:I = 0x7f120bbf

.field public static final hs__review_close_button:I = 0x7f120bc0

.field public static final hs__review_message:I = 0x7f120bc1

.field public static final hs__review_request_message:I = 0x7f120bc2

.field public static final hs__review_title:I = 0x7f120bc3

.field public static final hs__screenshot_add:I = 0x7f120bc4

.field public static final hs__screenshot_cloud_attach_error:I = 0x7f120bc5

.field public static final hs__screenshot_limit_error:I = 0x7f120bc6

.field public static final hs__screenshot_remove:I = 0x7f120bc7

.field public static final hs__screenshot_upload_error_msg:I = 0x7f120bc8

.field public static final hs__search_footer:I = 0x7f120bc9

.field public static final hs__search_hint:I = 0x7f120bca

.field public static final hs__search_result_message:I = 0x7f120bcb

.field public static final hs__search_result_title:I = 0x7f120bcc

.field public static final hs__search_title:I = 0x7f120bcd

.field public static final hs__select_a_question_message:I = 0x7f120bce

.field public static final hs__send_anyway:I = 0x7f120bcf

.field public static final hs__send_msg_btn:I = 0x7f120bd0

.field public static final hs__sending_fail_msg:I = 0x7f120bd1

.field public static final hs__sending_msg:I = 0x7f120bd2

.field public static final hs__ssl_handshake_error:I = 0x7f120bd3

.field public static final hs__ssl_peer_unverified_error:I = 0x7f120bd4

.field public static final hs__starting_download:I = 0x7f120bd5

.field public static final hs__submit_conversation_btn:I = 0x7f120bd6

.field public static final hs__tap_to_retry:I = 0x7f120bd7

.field public static final hs__try_again_msg:I = 0x7f120bd8

.field public static final hs__user_failed_message_voice_over:I = 0x7f120bd9

.field public static final hs__user_sending_message_voice_over:I = 0x7f120bda

.field public static final hs__user_sent_message_voice_over:I = 0x7f120bdb

.field public static final hs__user_setup_retry_description:I = 0x7f120bdc

.field public static final hs__username_blank_error:I = 0x7f120bdd

.field public static final hs__username_hint:I = 0x7f120bde

.field public static final hud:I = 0x7f120bdf

.field public static final hud_charge_reader_charging_title:I = 0x7f120be0

.field public static final hud_charge_reader_dead_title:I = 0x7f120be1

.field public static final hud_charge_reader_low_title:I = 0x7f120be2

.field public static final hud_charge_reader_message:I = 0x7f120be3

.field public static final hud_chip_reader_connected:I = 0x7f120be4

.field public static final hud_chip_reader_disconnected:I = 0x7f120be5

.field public static final hud_contactless_chip_reader_connected:I = 0x7f120be6

.field public static final hud_contactless_chip_reader_disconnected:I = 0x7f120be7

.field public static final hud_legacy_reader_connected:I = 0x7f120be8

.field public static final hud_legacy_reader_disconnected:I = 0x7f120be9

.field public static final hud_miura_reader_disconnected:I = 0x7f120bea

.field public static final hud_reinsert_chip_card:I = 0x7f120beb

.field public static final hud_reinsert_chip_card_to_charge:I = 0x7f120bec

.field public static final hud_reinsert_chip_card_to_search:I = 0x7f120bed

.field public static final hud_remove_chip_card:I = 0x7f120bee

.field public static final icon_content_description:I = 0x7f120bef

.field public static final image_attachment:I = 0x7f120bf0

.field public static final image_attachment_upload:I = 0x7f120bf1

.field public static final image_upload:I = 0x7f120bf2

.field public static final inactive:I = 0x7f120bf3

.field public static final including_tax:I = 0x7f120bf4

.field public static final incorrect_pin:I = 0x7f120bf5

.field public static final initial_time:I = 0x7f120bf6

.field public static final installments_get_started:I = 0x7f120bf7

.field public static final installments_get_started_hint:I = 0x7f120bf8

.field public static final installments_link_instead:I = 0x7f120bf9

.field public static final installments_link_options_url:I = 0x7f120bfa

.field public static final installments_manual_card_entry_hint:I = 0x7f120bfb

.field public static final installments_manual_card_entry_square_support:I = 0x7f120bfc

.field public static final installments_manual_card_entry_url:I = 0x7f120bfd

.field public static final installments_options_amount:I = 0x7f120bfe

.field public static final installments_options_enter_card_number:I = 0x7f120bff

.field public static final installments_options_hint:I = 0x7f120c00

.field public static final installments_options_link:I = 0x7f120c01

.field public static final installments_qr_code_error_hint:I = 0x7f120c02

.field public static final installments_qr_code_loaded_hint:I = 0x7f120c03

.field public static final installments_scan_code:I = 0x7f120c04

.field public static final installments_scan_prompt:I = 0x7f120c05

.field public static final installments_select_tender_title_disabled:I = 0x7f120c06

.field public static final installments_select_tender_title_enabled:I = 0x7f120c07

.field public static final installments_sent_hint:I = 0x7f120c08

.field public static final installments_sent_title:I = 0x7f120c09

.field public static final installments_sms_hint:I = 0x7f120c0a

.field public static final installments_sms_input_hint:I = 0x7f120c0b

.field public static final installments_sms_prompt:I = 0x7f120c0c

.field public static final installments_sms_send:I = 0x7f120c0d

.field public static final installments_text_me:I = 0x7f120c0e

.field public static final instant_deposits_allow_switch:I = 0x7f120c0f

.field public static final instant_deposits_app_update_message:I = 0x7f120c10

.field public static final instant_deposits_app_update_title:I = 0x7f120c11

.field public static final instant_deposits_app_update_update:I = 0x7f120c12

.field public static final instant_deposits_button_hint:I = 0x7f120c13

.field public static final instant_deposits_button_hint_uk:I = 0x7f120c14

.field public static final instant_deposits_button_text:I = 0x7f120c15

.field public static final instant_deposits_card_failed:I = 0x7f120c16

.field public static final instant_deposits_card_linked:I = 0x7f120c17

.field public static final instant_deposits_card_linked_message:I = 0x7f120c18

.field public static final instant_deposits_check_your_inbox:I = 0x7f120c19

.field public static final instant_deposits_could_not_load_deposit_schedule:I = 0x7f120c1a

.field public static final instant_deposits_deposit_failed:I = 0x7f120c1b

.field public static final instant_deposits_deposit_result_card_title:I = 0x7f120c1c

.field public static final instant_deposits_deposit_schedule_close_of_day:I = 0x7f120c1d

.field public static final instant_deposits_deposit_schedule_hint:I = 0x7f120c1e

.field public static final instant_deposits_deposit_schedule_hint_without_same_day_deposit:I = 0x7f120c1f

.field public static final instant_deposits_deposit_schedule_url:I = 0x7f120c20

.field public static final instant_deposits_link_card_url:I = 0x7f120c21

.field public static final instant_deposits_link_debit_card:I = 0x7f120c22

.field public static final instant_deposits_link_debit_card_editor_hint:I = 0x7f120c23

.field public static final instant_deposits_link_debit_card_helper_text:I = 0x7f120c24

.field public static final instant_deposits_link_debit_card_instructions:I = 0x7f120c25

.field public static final instant_deposits_link_expired:I = 0x7f120c26

.field public static final instant_deposits_linked_card_hint:I = 0x7f120c27

.field public static final instant_deposits_linking_debit_card:I = 0x7f120c28

.field public static final instant_deposits_network_error_message:I = 0x7f120c29

.field public static final instant_deposits_network_error_title:I = 0x7f120c2a

.field public static final instant_deposits_pending_verification:I = 0x7f120c2b

.field public static final instant_deposits_processing_deposit:I = 0x7f120c2c

.field public static final instant_deposits_resend_email:I = 0x7f120c2d

.field public static final instant_deposits_section_name_deposit_schedule:I = 0x7f120c2e

.field public static final instant_deposits_section_name_instant_deposit:I = 0x7f120c2f

.field public static final instant_deposits_set_up_instant_deposit:I = 0x7f120c30

.field public static final instant_deposits_success_title:I = 0x7f120c31

.field public static final instant_deposits_success_title_uk:I = 0x7f120c32

.field public static final instant_deposits_title:I = 0x7f120c33

.field public static final instant_deposits_try_another_debit_card:I = 0x7f120c34

.field public static final instant_deposits_unavailable:I = 0x7f120c35

.field public static final instant_deposits_unavailable_hint:I = 0x7f120c36

.field public static final instant_deposits_unavailable_learn_more:I = 0x7f120c37

.field public static final instant_deposits_unavailable_troubleshooting:I = 0x7f120c38

.field public static final instant_deposits_update_debit_card_information:I = 0x7f120c39

.field public static final instant_deposits_verification_email_sent:I = 0x7f120c3a

.field public static final instant_deposits_verification_email_sent_message:I = 0x7f120c3b

.field public static final instant_transfer_terms:I = 0x7f120c3c

.field public static final instant_transfer_terms_url:I = 0x7f120c3d

.field public static final instant_transfers_description:I = 0x7f120c3e

.field public static final instant_transfers_description_uk:I = 0x7f120c3f

.field public static final institution_number:I = 0x7f120c40

.field public static final instruction_to_your_bank_or_building_society_body:I = 0x7f120c41

.field public static final instruction_to_your_bank_or_building_society_title:I = 0x7f120c42

.field public static final intent_how_custom:I = 0x7f120c43

.field public static final intent_how_custom_description:I = 0x7f120c44

.field public static final intent_how_preset:I = 0x7f120c45

.field public static final intent_how_preset_description:I = 0x7f120c46

.field public static final intent_how_title:I = 0x7f120c47

.field public static final intent_where_computer:I = 0x7f120c48

.field public static final intent_where_invoice:I = 0x7f120c49

.field public static final intent_where_phone:I = 0x7f120c4a

.field public static final intent_where_subtitle:I = 0x7f120c4b

.field public static final intent_where_tablet:I = 0x7f120c4c

.field public static final intent_where_title:I = 0x7f120c4d

.field public static final intent_where_website:I = 0x7f120c4e

.field public static final invalid_16_digit_pan:I = 0x7f120c4f

.field public static final invalid_balance_date_message:I = 0x7f120c50

.field public static final invalid_balance_date_title:I = 0x7f120c51

.field public static final invalid_bank_message:I = 0x7f120c52

.field public static final invalid_branch_message:I = 0x7f120c53

.field public static final invalid_bsb_number_message:I = 0x7f120c54

.field public static final invalid_date:I = 0x7f120c55

.field public static final invalid_deposit_amount_title:I = 0x7f120c56

.field public static final invalid_ein:I = 0x7f120c57

.field public static final invalid_ein_length:I = 0x7f120c58

.field public static final invalid_email:I = 0x7f120c59

.field public static final invalid_email_message:I = 0x7f120c5a

.field public static final invalid_institution_number_message:I = 0x7f120c5b

.field public static final invalid_last4_ssn_message:I = 0x7f120c5c

.field public static final invalid_last4_ssn_message_with_itin:I = 0x7f120c5d

.field public static final invalid_password:I = 0x7f120c5e

.field public static final invalid_password_subtext:I = 0x7f120c5f

.field public static final invalid_phone_number:I = 0x7f120c60

.field public static final invalid_phone_number_message:I = 0x7f120c61

.field public static final invalid_postal_code:I = 0x7f120c62

.field public static final invalid_reminder_in_past:I = 0x7f120c63

.field public static final invalid_reminder_on_invoice_send_date:I = 0x7f120c64

.field public static final invalid_reminder_prior_to_send_date:I = 0x7f120c65

.field public static final invalid_routing:I = 0x7f120c66

.field public static final invalid_routing_message:I = 0x7f120c67

.field public static final invalid_sort_code_message:I = 0x7f120c68

.field public static final invalid_ssn:I = 0x7f120c69

.field public static final invalid_ssn_length:I = 0x7f120c6a

.field public static final invalid_transit_number_message:I = 0x7f120c6b

.field public static final inventory_receive_stock:I = 0x7f120c6c

.field public static final inventory_stock_count:I = 0x7f120c6d

.field public static final inventory_stock_count_reload:I = 0x7f120c6e

.field public static final inventory_update_required_message:I = 0x7f120c6f

.field public static final inventory_update_required_title:I = 0x7f120c70

.field public static final inventory_view_stock:I = 0x7f120c71

.field public static final invoice:I = 0x7f120c72

.field public static final invoice_add_attachment:I = 0x7f120c73

.field public static final invoice_add_message:I = 0x7f120c74

.field public static final invoice_add_reminder:I = 0x7f120c75

.field public static final invoice_allow_automatic_payments:I = 0x7f120c76

.field public static final invoice_allow_buyer_save_cof:I = 0x7f120c77

.field public static final invoice_allow_tip:I = 0x7f120c78

.field public static final invoice_amount:I = 0x7f120c79

.field public static final invoice_archived:I = 0x7f120c7a

.field public static final invoice_attachment_default_name:I = 0x7f120c7b

.field public static final invoice_attachment_legal_disclaimer:I = 0x7f120c7c

.field public static final invoice_automatic_payment_auto_enable_buyer_cof:I = 0x7f120c7d

.field public static final invoice_automatic_payment_fee_message:I = 0x7f120c7e

.field public static final invoice_automatic_payment_fee_message_v2:I = 0x7f120c7f

.field public static final invoice_automatic_payment_fee_message_with_rate:I = 0x7f120c80

.field public static final invoice_automatic_payment_fee_message_with_rate_v2:I = 0x7f120c81

.field public static final invoice_automatic_payment_off:I = 0x7f120c82

.field public static final invoice_automatic_payment_on:I = 0x7f120c83

.field public static final invoice_automatic_payment_url:I = 0x7f120c84

.field public static final invoice_automatic_payments:I = 0x7f120c85

.field public static final invoice_automatic_payments_description_subtitle:I = 0x7f120c86

.field public static final invoice_automatic_payments_description_title:I = 0x7f120c87

.field public static final invoice_automatic_reminders:I = 0x7f120c88

.field public static final invoice_automatic_reminders_explanation:I = 0x7f120c89

.field public static final invoice_automatic_reminders_off:I = 0x7f120c8a

.field public static final invoice_automatic_reminders_on:I = 0x7f120c8b

.field public static final invoice_automatic_reminders_schedule_for:I = 0x7f120c8c

.field public static final invoice_automatic_reminders_sent_on:I = 0x7f120c8d

.field public static final invoice_automatic_reminders_wont_be_sent_after_paid:I = 0x7f120c8e

.field public static final invoice_cancel_title:I = 0x7f120c8f

.field public static final invoice_canceled:I = 0x7f120c90

.field public static final invoice_canceled_message:I = 0x7f120c91

.field public static final invoice_charge_cof:I = 0x7f120c92

.field public static final invoice_charge_cof_action:I = 0x7f120c93

.field public static final invoice_charge_cof_concise:I = 0x7f120c94

.field public static final invoice_charged:I = 0x7f120c95

.field public static final invoice_charged_receipt_sent:I = 0x7f120c96

.field public static final invoice_charged_title:I = 0x7f120c97

.field public static final invoice_choose_charge_date:I = 0x7f120c98

.field public static final invoice_choose_contact:I = 0x7f120c99

.field public static final invoice_choose_due_date:I = 0x7f120c9a

.field public static final invoice_choose_send_date:I = 0x7f120c9b

.field public static final invoice_choose_start_date:I = 0x7f120c9c

.field public static final invoice_connection_error:I = 0x7f120c9d

.field public static final invoice_connection_error_cancel:I = 0x7f120c9e

.field public static final invoice_connection_error_charge:I = 0x7f120c9f

.field public static final invoice_connection_error_create:I = 0x7f120ca0

.field public static final invoice_connection_error_delete:I = 0x7f120ca1

.field public static final invoice_connection_error_delete_series:I = 0x7f120ca2

.field public static final invoice_connection_error_end_series:I = 0x7f120ca3

.field public static final invoice_connection_error_invoice_service_series_subtitle:I = 0x7f120ca4

.field public static final invoice_connection_error_invoice_service_subtitle:I = 0x7f120ca5

.field public static final invoice_connection_error_message:I = 0x7f120ca6

.field public static final invoice_connection_error_preview:I = 0x7f120ca7

.field public static final invoice_connection_error_resend:I = 0x7f120ca8

.field public static final invoice_connection_error_save:I = 0x7f120ca9

.field public static final invoice_connection_error_save_recurring_draft:I = 0x7f120caa

.field public static final invoice_connection_error_schedule:I = 0x7f120cab

.field public static final invoice_connection_error_schedule_recurring:I = 0x7f120cac

.field public static final invoice_connection_error_send:I = 0x7f120cad

.field public static final invoice_connection_error_send_reminder:I = 0x7f120cae

.field public static final invoice_connection_error_update:I = 0x7f120caf

.field public static final invoice_connection_error_update_series:I = 0x7f120cb0

.field public static final invoice_connection_error_upload:I = 0x7f120cb1

.field public static final invoice_create_discard:I = 0x7f120cb2

.field public static final invoice_create_discard_message:I = 0x7f120cb3

.field public static final invoice_created:I = 0x7f120cb4

.field public static final invoice_created_subtitle:I = 0x7f120cb5

.field public static final invoice_custom:I = 0x7f120cb6

.field public static final invoice_date:I = 0x7f120cb7

.field public static final invoice_default_headertext:I = 0x7f120cb8

.field public static final invoice_default_message:I = 0x7f120cb9

.field public static final invoice_delete_series_title:I = 0x7f120cba

.field public static final invoice_delete_title:I = 0x7f120cbb

.field public static final invoice_deleted:I = 0x7f120cbc

.field public static final invoice_deleted_series:I = 0x7f120cbd

.field public static final invoice_delivery_method:I = 0x7f120cbe

.field public static final invoice_delivery_method_cof_default:I = 0x7f120cbf

.field public static final invoice_delivery_method_email:I = 0x7f120cc0

.field public static final invoice_delivery_method_manual:I = 0x7f120cc1

.field public static final invoice_delivery_method_manual_support:I = 0x7f120cc2

.field public static final invoice_delivery_method_select:I = 0x7f120cc3

.field public static final invoice_delivery_method_title:I = 0x7f120cc4

.field public static final invoice_delivery_more_options:I = 0x7f120cc5

.field public static final invoice_delivery_share_body:I = 0x7f120cc6

.field public static final invoice_delivery_share_subject:I = 0x7f120cc7

.field public static final invoice_delivery_share_using:I = 0x7f120cc8

.field public static final invoice_detail_add_payment:I = 0x7f120cc9

.field public static final invoice_detail_amount_paid:I = 0x7f120cca

.field public static final invoice_detail_amount_paid_after_refund:I = 0x7f120ccb

.field public static final invoice_detail_amount_refunded:I = 0x7f120ccc

.field public static final invoice_detail_amount_remaining:I = 0x7f120ccd

.field public static final invoice_detail_charge:I = 0x7f120cce

.field public static final invoice_detail_charged:I = 0x7f120ccf

.field public static final invoice_detail_connection_error_message:I = 0x7f120cd0

.field public static final invoice_detail_customer:I = 0x7f120cd1

.field public static final invoice_detail_delete_draft:I = 0x7f120cd2

.field public static final invoice_detail_delete_draft_confirmation:I = 0x7f120cd3

.field public static final invoice_detail_delete_draft_recurring_confirmation:I = 0x7f120cd4

.field public static final invoice_detail_download_invoice:I = 0x7f120cd5

.field public static final invoice_detail_due:I = 0x7f120cd6

.field public static final invoice_detail_duplicate:I = 0x7f120cd7

.field public static final invoice_detail_edit_button:I = 0x7f120cd8

.field public static final invoice_detail_email:I = 0x7f120cd9

.field public static final invoice_detail_end_series:I = 0x7f120cda

.field public static final invoice_detail_end_series_confirmation:I = 0x7f120cdb

.field public static final invoice_detail_frequency:I = 0x7f120cdc

.field public static final invoice_detail_id:I = 0x7f120cdd

.field public static final invoice_detail_invoice_title:I = 0x7f120cde

.field public static final invoice_detail_invoice_total:I = 0x7f120cdf

.field public static final invoice_detail_send_reminder:I = 0x7f120ce0

.field public static final invoice_detail_sent:I = 0x7f120ce1

.field public static final invoice_detail_series_title:I = 0x7f120ce2

.field public static final invoice_detail_share_link:I = 0x7f120ce3

.field public static final invoice_detail_show_full_invoice:I = 0x7f120ce4

.field public static final invoice_detail_take_payment:I = 0x7f120ce5

.field public static final invoice_detail_title:I = 0x7f120ce6

.field public static final invoice_detail_view_invoices_in_series:I = 0x7f120ce7

.field public static final invoice_detail_view_transaction:I = 0x7f120ce8

.field public static final invoice_details_title:I = 0x7f120ce9

.field public static final invoice_display_state_active_series_null_state:I = 0x7f120cea

.field public static final invoice_display_state_archived:I = 0x7f120ceb

.field public static final invoice_display_state_canceled:I = 0x7f120cec

.field public static final invoice_display_state_canceled_null_state:I = 0x7f120ced

.field public static final invoice_display_state_draft:I = 0x7f120cee

.field public static final invoice_display_state_draft_null_state:I = 0x7f120cef

.field public static final invoice_display_state_draft_series_null_state:I = 0x7f120cf0

.field public static final invoice_display_state_ended_series_null_state:I = 0x7f120cf1

.field public static final invoice_display_state_failed:I = 0x7f120cf2

.field public static final invoice_display_state_failed_null_state:I = 0x7f120cf3

.field public static final invoice_display_state_overdue:I = 0x7f120cf4

.field public static final invoice_display_state_overdue_null_state:I = 0x7f120cf5

.field public static final invoice_display_state_paid:I = 0x7f120cf6

.field public static final invoice_display_state_paid_null_state:I = 0x7f120cf7

.field public static final invoice_display_state_partially_paid:I = 0x7f120cf8

.field public static final invoice_display_state_recurring:I = 0x7f120cf9

.field public static final invoice_display_state_recurring_null_state:I = 0x7f120cfa

.field public static final invoice_display_state_refunded:I = 0x7f120cfb

.field public static final invoice_display_state_refunded_null_state:I = 0x7f120cfc

.field public static final invoice_display_state_scheduled:I = 0x7f120cfd

.field public static final invoice_display_state_scheduled_null_state:I = 0x7f120cfe

.field public static final invoice_display_state_undelivered:I = 0x7f120cff

.field public static final invoice_display_state_undelivered_null_state:I = 0x7f120d00

.field public static final invoice_display_state_unknown:I = 0x7f120d01

.field public static final invoice_display_state_unknown_null_state:I = 0x7f120d02

.field public static final invoice_display_state_unpaid:I = 0x7f120d03

.field public static final invoice_display_state_unpaid_null_state:I = 0x7f120d04

.field public static final invoice_duplicate_reminder_date:I = 0x7f120d05

.field public static final invoice_duplicate_reminder_date_explanation:I = 0x7f120d06

.field public static final invoice_each_invoice_due:I = 0x7f120d07

.field public static final invoice_edit_cancel:I = 0x7f120d08

.field public static final invoice_edit_cancel_confirm:I = 0x7f120d09

.field public static final invoice_edit_charge_invoice:I = 0x7f120d0a

.field public static final invoice_edit_create_invoice:I = 0x7f120d0b

.field public static final invoice_edit_delete_draft:I = 0x7f120d0c

.field public static final invoice_edit_delete_draft_confirm:I = 0x7f120d0d

.field public static final invoice_edit_delete_draft_recurring_confirm:I = 0x7f120d0e

.field public static final invoice_edit_discard:I = 0x7f120d0f

.field public static final invoice_edit_discard_message:I = 0x7f120d10

.field public static final invoice_edit_invoice:I = 0x7f120d11

.field public static final invoice_edit_new_invoice:I = 0x7f120d12

.field public static final invoice_edit_new_recurring_series:I = 0x7f120d13

.field public static final invoice_edit_recurring_series:I = 0x7f120d14

.field public static final invoice_edit_save_and_close:I = 0x7f120d15

.field public static final invoice_edit_save_as_draft:I = 0x7f120d16

.field public static final invoice_edit_schedule_invoice:I = 0x7f120d17

.field public static final invoice_edit_send_invoice:I = 0x7f120d18

.field public static final invoice_edit_update_invoice:I = 0x7f120d19

.field public static final invoice_end_of_month:I = 0x7f120d1a

.field public static final invoice_end_series:I = 0x7f120d1b

.field public static final invoice_file_attachment_count_limit_description:I = 0x7f120d1c

.field public static final invoice_file_attachment_count_limit_title:I = 0x7f120d1d

.field public static final invoice_file_attachment_size_limit_description:I = 0x7f120d1e

.field public static final invoice_file_attachment_size_limit_title:I = 0x7f120d1f

.field public static final invoice_file_attachment_source_attach_pdf:I = 0x7f120d20

.field public static final invoice_frequency:I = 0x7f120d21

.field public static final invoice_frequency_end:I = 0x7f120d22

.field public static final invoice_frequency_one_time:I = 0x7f120d23

.field public static final invoice_frequency_repeat_every:I = 0x7f120d24

.field public static final invoice_gift_card_unspported_message:I = 0x7f120d25

.field public static final invoice_hint_id:I = 0x7f120d26

.field public static final invoice_hint_message:I = 0x7f120d27

.field public static final invoice_hint_recipient_additional_email:I = 0x7f120d28

.field public static final invoice_hint_recipient_email:I = 0x7f120d29

.field public static final invoice_hint_recipient_name:I = 0x7f120d2a

.field public static final invoice_hint_title:I = 0x7f120d2b

.field public static final invoice_history_title:I = 0x7f120d2c

.field public static final invoice_id:I = 0x7f120d2d

.field public static final invoice_image_download_failed:I = 0x7f120d2e

.field public static final invoice_in_fourteen_days:I = 0x7f120d2f

.field public static final invoice_in_seven_days:I = 0x7f120d30

.field public static final invoice_in_thirty_days:I = 0x7f120d31

.field public static final invoice_link_copied:I = 0x7f120d32

.field public static final invoice_message:I = 0x7f120d33

.field public static final invoice_method:I = 0x7f120d34

.field public static final invoice_next_invoice:I = 0x7f120d35

.field public static final invoice_no_contacts_message:I = 0x7f120d36

.field public static final invoice_no_contacts_title:I = 0x7f120d37

.field public static final invoice_no_due_date:I = 0x7f120d38

.field public static final invoice_no_invoice_null_state_subtitle:I = 0x7f120d39

.field public static final invoice_no_invoice_null_state_title:I = 0x7f120d3a

.field public static final invoice_no_invoices_for_filter_message:I = 0x7f120d3b

.field public static final invoice_no_invoices_for_filter_title:I = 0x7f120d3c

.field public static final invoice_no_search_results_message:I = 0x7f120d3d

.field public static final invoice_offline_mode:I = 0x7f120d3e

.field public static final invoice_paid:I = 0x7f120d3f

.field public static final invoice_paid_remaining_balance:I = 0x7f120d40

.field public static final invoice_paid_transaction_complete:I = 0x7f120d41

.field public static final invoice_payment_schedule_title:I = 0x7f120d42

.field public static final invoice_preview_button:I = 0x7f120d43

.field public static final invoice_preview_title:I = 0x7f120d44

.field public static final invoice_prod_url:I = 0x7f120d45

.field public static final invoice_prod_url_pay_page:I = 0x7f120d46

.field public static final invoice_recurring_period_daily:I = 0x7f120d47

.field public static final invoice_recurring_period_daily_ending:I = 0x7f120d48

.field public static final invoice_recurring_period_daily_plural:I = 0x7f120d49

.field public static final invoice_recurring_period_daily_plural_ending:I = 0x7f120d4a

.field public static final invoice_recurring_period_daily_plural_short:I = 0x7f120d4b

.field public static final invoice_recurring_period_daily_short:I = 0x7f120d4c

.field public static final invoice_recurring_period_monthly:I = 0x7f120d4d

.field public static final invoice_recurring_period_monthly_end_of_month:I = 0x7f120d4e

.field public static final invoice_recurring_period_monthly_end_of_month_ending:I = 0x7f120d4f

.field public static final invoice_recurring_period_monthly_ending:I = 0x7f120d50

.field public static final invoice_recurring_period_monthly_plural:I = 0x7f120d51

.field public static final invoice_recurring_period_monthly_plural_end_of_month:I = 0x7f120d52

.field public static final invoice_recurring_period_monthly_plural_end_of_month_ending:I = 0x7f120d53

.field public static final invoice_recurring_period_monthly_plural_ending:I = 0x7f120d54

.field public static final invoice_recurring_period_monthly_plural_short:I = 0x7f120d55

.field public static final invoice_recurring_period_monthly_short:I = 0x7f120d56

.field public static final invoice_recurring_period_weekly:I = 0x7f120d57

.field public static final invoice_recurring_period_weekly_ending:I = 0x7f120d58

.field public static final invoice_recurring_period_weekly_plural:I = 0x7f120d59

.field public static final invoice_recurring_period_weekly_plural_ending:I = 0x7f120d5a

.field public static final invoice_recurring_period_weekly_plural_short:I = 0x7f120d5b

.field public static final invoice_recurring_period_weekly_short:I = 0x7f120d5c

.field public static final invoice_recurring_period_yearly:I = 0x7f120d5d

.field public static final invoice_recurring_period_yearly_ending:I = 0x7f120d5e

.field public static final invoice_recurring_period_yearly_plural:I = 0x7f120d5f

.field public static final invoice_recurring_period_yearly_plural_ending:I = 0x7f120d60

.field public static final invoice_recurring_period_yearly_plural_short:I = 0x7f120d61

.field public static final invoice_recurring_period_yearly_short:I = 0x7f120d62

.field public static final invoice_recurring_start_date_eom_error_message:I = 0x7f120d63

.field public static final invoice_recurring_start_date_eom_error_title:I = 0x7f120d64

.field public static final invoice_reminder:I = 0x7f120d65

.field public static final invoice_reminder_confirmation_body:I = 0x7f120d66

.field public static final invoice_reminder_confirmation_title:I = 0x7f120d67

.field public static final invoice_reminder_error_message_default:I = 0x7f120d68

.field public static final invoice_reminder_send_after_due_date:I = 0x7f120d69

.field public static final invoice_reminder_send_after_due_date_option:I = 0x7f120d6a

.field public static final invoice_reminder_send_after_due_date_plural_option:I = 0x7f120d6b

.field public static final invoice_reminder_send_before_due_date:I = 0x7f120d6c

.field public static final invoice_reminder_send_before_due_date_option:I = 0x7f120d6d

.field public static final invoice_reminder_send_before_due_date_plural_option:I = 0x7f120d6e

.field public static final invoice_reminder_send_on_date:I = 0x7f120d6f

.field public static final invoice_reminder_sent:I = 0x7f120d70

.field public static final invoice_reminder_title:I = 0x7f120d71

.field public static final invoice_remove_reminder:I = 0x7f120d72

.field public static final invoice_request_shipping_address:I = 0x7f120d73

.field public static final invoice_request_tip:I = 0x7f120d74

.field public static final invoice_resend:I = 0x7f120d75

.field public static final invoice_resend_title:I = 0x7f120d76

.field public static final invoice_resent:I = 0x7f120d77

.field public static final invoice_save_cof_helper:I = 0x7f120d78

.field public static final invoice_save_cof_recurring_helper:I = 0x7f120d79

.field public static final invoice_save_recurring_title:I = 0x7f120d7a

.field public static final invoice_save_title:I = 0x7f120d7b

.field public static final invoice_saved:I = 0x7f120d7c

.field public static final invoice_saved_recurring:I = 0x7f120d7d

.field public static final invoice_schedule:I = 0x7f120d7e

.field public static final invoice_schedule_recurring_title:I = 0x7f120d7f

.field public static final invoice_scheduled:I = 0x7f120d80

.field public static final invoice_scheduled_recurring_title:I = 0x7f120d81

.field public static final invoice_scheduled_title:I = 0x7f120d82

.field public static final invoice_search_active_series:I = 0x7f120d83

.field public static final invoice_search_all_invoices:I = 0x7f120d84

.field public static final invoice_search_all_outstanding:I = 0x7f120d85

.field public static final invoice_search_archived:I = 0x7f120d86

.field public static final invoice_search_draft_series:I = 0x7f120d87

.field public static final invoice_search_drafts:I = 0x7f120d88

.field public static final invoice_search_in_series:I = 0x7f120d89

.field public static final invoice_search_inactive_series:I = 0x7f120d8a

.field public static final invoice_search_paid:I = 0x7f120d8b

.field public static final invoice_search_scheduled:I = 0x7f120d8c

.field public static final invoice_send:I = 0x7f120d8d

.field public static final invoice_send_action:I = 0x7f120d8e

.field public static final invoice_send_automatic_reminders:I = 0x7f120d8f

.field public static final invoice_send_immediately:I = 0x7f120d90

.field public static final invoice_send_reminder_button:I = 0x7f120d91

.field public static final invoice_send_reminder_confirmation_message:I = 0x7f120d92

.field public static final invoice_send_reminder_custom_message_placeholder:I = 0x7f120d93

.field public static final invoice_send_reminder_custom_message_title:I = 0x7f120d94

.field public static final invoice_send_reminder_title:I = 0x7f120d95

.field public static final invoice_send_title:I = 0x7f120d96

.field public static final invoice_sent:I = 0x7f120d97

.field public static final invoice_series_ended:I = 0x7f120d98

.field public static final invoice_set_default_message:I = 0x7f120d99

.field public static final invoice_set_default_message_error:I = 0x7f120d9a

.field public static final invoice_set_default_message_helper:I = 0x7f120d9b

.field public static final invoice_set_default_message_saving:I = 0x7f120d9c

.field public static final invoice_set_default_message_set:I = 0x7f120d9d

.field public static final invoice_share_link_confirmation:I = 0x7f120d9e

.field public static final invoice_share_link_support_url:I = 0x7f120d9f

.field public static final invoice_staging_url:I = 0x7f120da0

.field public static final invoice_staging_url_pay_page:I = 0x7f120da1

.field public static final invoice_start:I = 0x7f120da2

.field public static final invoice_state_filter_all_outstanding:I = 0x7f120da3

.field public static final invoice_state_filter_all_outstanding_null_state:I = 0x7f120da4

.field public static final invoice_state_filter_all_sent:I = 0x7f120da5

.field public static final invoice_state_filter_all_sent_null_state:I = 0x7f120da6

.field public static final invoice_state_filter_unsuccessful:I = 0x7f120da7

.field public static final invoice_state_filter_unsuccessful_null_state:I = 0x7f120da8

.field public static final invoice_status_canceled:I = 0x7f120da9

.field public static final invoice_status_failed:I = 0x7f120daa

.field public static final invoice_status_overdue:I = 0x7f120dab

.field public static final invoice_status_paid:I = 0x7f120dac

.field public static final invoice_status_recurring:I = 0x7f120dad

.field public static final invoice_status_refunded:I = 0x7f120dae

.field public static final invoice_status_scheduled:I = 0x7f120daf

.field public static final invoice_status_unpaid:I = 0x7f120db0

.field public static final invoice_status_unpaid_no_due_date:I = 0x7f120db1

.field public static final invoice_timeline_button:I = 0x7f120db2

.field public static final invoice_timeline_title:I = 0x7f120db3

.field public static final invoice_unable_to_load_image:I = 0x7f120db4

.field public static final invoice_unarchived:I = 0x7f120db5

.field public static final invoice_unavailable_for_split_tender:I = 0x7f120db6

.field public static final invoice_unsupported_message_discounts:I = 0x7f120db7

.field public static final invoice_unsupported_message_modifiers:I = 0x7f120db8

.field public static final invoice_unsupported_offline_mode:I = 0x7f120db9

.field public static final invoice_unsupported_title:I = 0x7f120dba

.field public static final invoice_update_settings_failure_title:I = 0x7f120dbb

.field public static final invoice_updated:I = 0x7f120dbc

.field public static final invoice_updated_recurring_title:I = 0x7f120dbd

.field public static final invoice_upon_receipt:I = 0x7f120dbe

.field public static final invoice_validation_error_deposit_after_balance:I = 0x7f120dbf

.field public static final invoice_validation_error_no_customer:I = 0x7f120dc0

.field public static final invoice_validation_error_no_customer_name:I = 0x7f120dc1

.field public static final invoice_validation_error_no_email:I = 0x7f120dc2

.field public static final invoice_view_message:I = 0x7f120dc3

.field public static final invoices_app_landing_page_url:I = 0x7f120dc4

.field public static final invoices_app_package_name:I = 0x7f120dc5

.field public static final issue_refund:I = 0x7f120dc6

.field public static final item_appeareance_dialog_cancel:I = 0x7f120dc7

.field public static final item_appeareance_dialog_confirm_message:I = 0x7f120dc8

.field public static final item_appeareance_dialog_confirm_title:I = 0x7f120dc9

.field public static final item_appeareance_dialog_yes:I = 0x7f120dca

.field public static final item_appeareance_image_tile_label:I = 0x7f120dcb

.field public static final item_appeareance_option_image:I = 0x7f120dcc

.field public static final item_appeareance_option_text:I = 0x7f120dcd

.field public static final item_appeareance_settings_label:I = 0x7f120dce

.field public static final item_appeareance_text_tile_label:I = 0x7f120dcf

.field public static final item_count_plural:I = 0x7f120dd0

.field public static final item_count_single:I = 0x7f120dd1

.field public static final item_editing_banner_category:I = 0x7f120dd2

.field public static final item_editing_banner_discount:I = 0x7f120dd3

.field public static final item_editing_banner_inactive_category:I = 0x7f120dd4

.field public static final item_editing_banner_inactive_discount:I = 0x7f120dd5

.field public static final item_editing_banner_inactive_item:I = 0x7f120dd6

.field public static final item_editing_banner_inactive_menu_item:I = 0x7f120dd7

.field public static final item_editing_banner_inactive_mod_set:I = 0x7f120dd8

.field public static final item_editing_banner_inactive_product:I = 0x7f120dd9

.field public static final item_editing_banner_inactive_service:I = 0x7f120dda

.field public static final item_editing_banner_inactive_tax:I = 0x7f120ddb

.field public static final item_editing_banner_item:I = 0x7f120ddc

.field public static final item_editing_banner_menu_item:I = 0x7f120ddd

.field public static final item_editing_banner_mod_set:I = 0x7f120dde

.field public static final item_editing_banner_not_shared_category:I = 0x7f120ddf

.field public static final item_editing_banner_not_shared_discount:I = 0x7f120de0

.field public static final item_editing_banner_not_shared_item:I = 0x7f120de1

.field public static final item_editing_banner_not_shared_menu_item:I = 0x7f120de2

.field public static final item_editing_banner_not_shared_mod_set:I = 0x7f120de3

.field public static final item_editing_banner_not_shared_product:I = 0x7f120de4

.field public static final item_editing_banner_not_shared_service:I = 0x7f120de5

.field public static final item_editing_banner_not_shared_tax:I = 0x7f120de6

.field public static final item_editing_banner_one_other_location_category:I = 0x7f120de7

.field public static final item_editing_banner_one_other_location_discount:I = 0x7f120de8

.field public static final item_editing_banner_one_other_location_item:I = 0x7f120de9

.field public static final item_editing_banner_one_other_location_menu_item:I = 0x7f120dea

.field public static final item_editing_banner_one_other_location_mod_set:I = 0x7f120deb

.field public static final item_editing_banner_one_other_location_product:I = 0x7f120dec

.field public static final item_editing_banner_one_other_location_service:I = 0x7f120ded

.field public static final item_editing_banner_one_other_location_tax:I = 0x7f120dee

.field public static final item_editing_banner_product:I = 0x7f120def

.field public static final item_editing_banner_service:I = 0x7f120df0

.field public static final item_editing_banner_tax:I = 0x7f120df1

.field public static final item_editing_category_delete_confirmation_cancel:I = 0x7f120df2

.field public static final item_editing_category_delete_confirmation_delete:I = 0x7f120df3

.field public static final item_editing_category_delete_confirmation_maybe_empty:I = 0x7f120df4

.field public static final item_editing_category_delete_confirmation_title:I = 0x7f120df5

.field public static final item_editing_category_delete_confirmation_with_items:I = 0x7f120df6

.field public static final item_editing_changes_not_available_category:I = 0x7f120df7

.field public static final item_editing_changes_not_available_discount:I = 0x7f120df8

.field public static final item_editing_changes_not_available_item:I = 0x7f120df9

.field public static final item_editing_changes_not_available_menu_item:I = 0x7f120dfa

.field public static final item_editing_changes_not_available_mod_set:I = 0x7f120dfb

.field public static final item_editing_changes_not_available_product:I = 0x7f120dfc

.field public static final item_editing_changes_not_available_service:I = 0x7f120dfd

.field public static final item_editing_changes_not_available_tax:I = 0x7f120dfe

.field public static final item_editing_changes_saved_restored_connection:I = 0x7f120dff

.field public static final item_editing_confirm_price_update_dialog_title:I = 0x7f120e00

.field public static final item_editing_delete_from_location_discount:I = 0x7f120e01

.field public static final item_editing_delete_from_location_item:I = 0x7f120e02

.field public static final item_editing_delete_from_location_menu_item:I = 0x7f120e03

.field public static final item_editing_delete_from_location_mod_set:I = 0x7f120e04

.field public static final item_editing_delete_from_location_product:I = 0x7f120e05

.field public static final item_editing_delete_from_location_service:I = 0x7f120e06

.field public static final item_editing_delete_from_location_tax:I = 0x7f120e07

.field public static final item_editing_delete_menu_item:I = 0x7f120e08

.field public static final item_editing_delete_product:I = 0x7f120e09

.field public static final item_editing_delete_service:I = 0x7f120e0a

.field public static final item_editing_read_only_category:I = 0x7f120e0b

.field public static final item_editing_read_only_description:I = 0x7f120e0c

.field public static final item_editing_read_only_details_menu_item:I = 0x7f120e0d

.field public static final item_editing_read_only_details_product:I = 0x7f120e0e

.field public static final item_editing_read_only_details_service:I = 0x7f120e0f

.field public static final item_editing_read_only_name:I = 0x7f120e10

.field public static final item_editing_save_failed_message:I = 0x7f120e11

.field public static final item_editing_save_failed_title:I = 0x7f120e12

.field public static final item_editing_save_will_affect_other_locations_item:I = 0x7f120e13

.field public static final item_editing_save_will_affect_other_locations_one_other_item:I = 0x7f120e14

.field public static final item_editing_saving:I = 0x7f120e15

.field public static final item_editing_select_location_update_price_dialog_title:I = 0x7f120e16

.field public static final item_editing_select_location_update_price_item:I = 0x7f120e17

.field public static final item_editing_select_location_update_price_one_other_item:I = 0x7f120e18

.field public static final item_editing_too_many_variations_dialog_message:I = 0x7f120e19

.field public static final item_editing_too_many_variations_dialog_title:I = 0x7f120e1a

.field public static final item_editing_update_all_locations:I = 0x7f120e1b

.field public static final item_editing_update_this_location_only:I = 0x7f120e1c

.field public static final item_library_all_discounts:I = 0x7f120e1d

.field public static final item_library_all_gift_cards:I = 0x7f120e1e

.field public static final item_library_all_items:I = 0x7f120e1f

.field public static final item_library_all_items_and_services:I = 0x7f120e20

.field public static final item_library_all_services:I = 0x7f120e21

.field public static final item_library_categories:I = 0x7f120e22

.field public static final item_library_create_new:I = 0x7f120e23

.field public static final item_library_custom_amount:I = 0x7f120e24

.field public static final item_library_empty_categories:I = 0x7f120e25

.field public static final item_library_empty_category_note_title:I = 0x7f120e26

.field public static final item_library_empty_discounts_note_title:I = 0x7f120e27

.field public static final item_library_empty_gift_cards_note_title:I = 0x7f120e28

.field public static final item_library_empty_items_title:I = 0x7f120e29

.field public static final item_library_empty_note_message_items_applet:I = 0x7f120e2a

.field public static final item_library_empty_note_services_message_items_applet:I = 0x7f120e2b

.field public static final item_library_empty_note_services_title:I = 0x7f120e2c

.field public static final item_library_empty_note_title:I = 0x7f120e2d

.field public static final item_library_empty_services_title:I = 0x7f120e2e

.field public static final item_library_multiple_durations:I = 0x7f120e2f

.field public static final item_library_multiple_prices:I = 0x7f120e30

.field public static final item_library_no_search_results:I = 0x7f120e31

.field public static final item_library_note_suggested:I = 0x7f120e32

.field public static final item_library_per_unit:I = 0x7f120e33

.field public static final item_library_redeem_rewards:I = 0x7f120e34

.field public static final item_library_variable_amount_discount:I = 0x7f120e35

.field public static final item_library_variable_percentage_discount:I = 0x7f120e36

.field public static final item_library_variable_price:I = 0x7f120e37

.field public static final item_library_variable_price_negation:I = 0x7f120e38

.field public static final item_list_add_to_grid_title:I = 0x7f120e39

.field public static final item_list_no_items_title:I = 0x7f120e3a

.field public static final item_note:I = 0x7f120e3b

.field public static final item_one:I = 0x7f120e3c

.field public static final item_options_learn_more_url:I = 0x7f120e3d

.field public static final item_setup:I = 0x7f120e3e

.field public static final item_setup_grid:I = 0x7f120e3f

.field public static final item_setup_message:I = 0x7f120e40

.field public static final item_variation_default_name:I = 0x7f120e41

.field public static final item_variation_default_name_regular:I = 0x7f120e42

.field public static final item_variation_price_hint_unfocused:I = 0x7f120e43

.field public static final item_variation_sku_hint:I = 0x7f120e44

.field public static final items:I = 0x7f120e45

.field public static final items_and_services:I = 0x7f120e46

.field public static final items_applet_all_items_title:I = 0x7f120e47

.field public static final items_applet_all_services_title:I = 0x7f120e48

.field public static final items_applet_categories_null_message:I = 0x7f120e49

.field public static final items_applet_categories_null_title:I = 0x7f120e4a

.field public static final items_applet_categories_title:I = 0x7f120e4b

.field public static final items_applet_category_message:I = 0x7f120e4c

.field public static final items_applet_create_category:I = 0x7f120e4d

.field public static final items_applet_detail_searchable_list_empty_view_message:I = 0x7f120e4e

.field public static final items_applet_detail_searchable_list_empty_view_title_for_units:I = 0x7f120e4f

.field public static final items_applet_discount_too_complicated_dialog_button:I = 0x7f120e50

.field public static final items_applet_discount_too_complicated_dialog_message:I = 0x7f120e51

.field public static final items_applet_discount_too_complicated_dialog_title:I = 0x7f120e52

.field public static final items_applet_discounts_null_message:I = 0x7f120e53

.field public static final items_applet_discounts_null_title:I = 0x7f120e54

.field public static final items_applet_discounts_title:I = 0x7f120e55

.field public static final items_applet_edit_category:I = 0x7f120e56

.field public static final items_applet_edit_modifier_set:I = 0x7f120e57

.field public static final items_applet_items_null_message:I = 0x7f120e58

.field public static final items_applet_items_null_title:I = 0x7f120e59

.field public static final items_applet_modifiers_null_message:I = 0x7f120e5a

.field public static final items_applet_modifiers_null_title:I = 0x7f120e5b

.field public static final items_applet_modifiers_title:I = 0x7f120e5c

.field public static final items_applet_no_categories:I = 0x7f120e5d

.field public static final items_applet_no_discounts:I = 0x7f120e5e

.field public static final items_applet_no_gift_cards:I = 0x7f120e5f

.field public static final items_applet_no_items:I = 0x7f120e60

.field public static final items_applet_no_items_in_category:I = 0x7f120e61

.field public static final items_applet_no_modifier_sets:I = 0x7f120e62

.field public static final items_applet_search_categories:I = 0x7f120e63

.field public static final items_applet_search_category:I = 0x7f120e64

.field public static final items_applet_search_discounts:I = 0x7f120e65

.field public static final items_applet_search_modifier_sets:I = 0x7f120e66

.field public static final items_applet_search_units:I = 0x7f120e67

.field public static final items_applet_services_null_message:I = 0x7f120e68

.field public static final items_applet_services_null_title:I = 0x7f120e69

.field public static final items_applet_setup_item_grid:I = 0x7f120e6a

.field public static final items_applet_units_title:I = 0x7f120e6b

.field public static final items_applet_uppercase_modifiers:I = 0x7f120e6c

.field public static final items_from_multiple_source_bills_warning:I = 0x7f120e6d

.field public static final items_hint_url:I = 0x7f120e6e

.field public static final items_plural:I = 0x7f120e6f

.field public static final jedi_content_unavailable_text:I = 0x7f120e70

.field public static final jedi_network_unavailable_text:I = 0x7f120e71

.field public static final jedi_unexpected_rendering_error:I = 0x7f120e72

.field public static final jp_formal_receipt_item_header:I = 0x7f120e73

.field public static final jp_formal_receipt_name:I = 0x7f120e74

.field public static final jp_formal_receipt_received_confirmation:I = 0x7f120e75

.field public static final jp_formal_receipt_title:I = 0x7f120e76

.field public static final jp_formal_receipt_total:I = 0x7f120e77

.field public static final keep:I = 0x7f120e78

.field public static final key_injection_failed:I = 0x7f120e79

.field public static final key_injection_started:I = 0x7f120e7a

.field public static final keypad:I = 0x7f120e7b

.field public static final keypad_card_details:I = 0x7f120e7c

.field public static final keypad_enter_your_pin:I = 0x7f120e7d

.field public static final keypad_incorrect_pin:I = 0x7f120e7e

.field public static final keypad_last_retry:I = 0x7f120e7f

.field public static final keypad_mode_accessibility_button_text:I = 0x7f120e80

.field public static final keypad_mode_high_contrast_button_text:I = 0x7f120e81

.field public static final keypad_mode_standard_pin_button_text:I = 0x7f120e82

.field public static final keypad_select_mode_title:I = 0x7f120e83

.field public static final keypad_select_mode_title_content_description:I = 0x7f120e84

.field public static final keypad_try_again:I = 0x7f120e85

.field public static final kitchen_printing_cardholder_name_name_order:I = 0x7f120e86

.field public static final kitchen_printing_cardholder_name_retrieving:I = 0x7f120e87

.field public static final kitchen_printing_cardholder_name_swipe:I = 0x7f120e88

.field public static final kitchen_printing_cardholder_name_swipe_or_insert:I = 0x7f120e89

.field public static final kitchen_printing_cardholder_name_unavailable:I = 0x7f120e8a

.field public static final kitchen_printing_confirm_discard_print_jobs_cancel:I = 0x7f120e8b

.field public static final kitchen_printing_confirm_discard_print_jobs_confirm:I = 0x7f120e8c

.field public static final kitchen_printing_confirm_discard_print_jobs_message:I = 0x7f120e8d

.field public static final kitchen_printing_confirm_discard_print_jobs_title:I = 0x7f120e8e

.field public static final kitchen_printing_discarded_prints_many:I = 0x7f120e8f

.field public static final kitchen_printing_discarded_prints_one:I = 0x7f120e90

.field public static final kitchen_printing_error_affected_prints:I = 0x7f120e91

.field public static final kitchen_printing_error_cover_open_title:I = 0x7f120e92

.field public static final kitchen_printing_error_cutter_body:I = 0x7f120e93

.field public static final kitchen_printing_error_cutter_title:I = 0x7f120e94

.field public static final kitchen_printing_error_issue_body_network:I = 0x7f120e95

.field public static final kitchen_printing_error_issue_body_reboot:I = 0x7f120e96

.field public static final kitchen_printing_error_issue_body_support_center:I = 0x7f120e97

.field public static final kitchen_printing_error_issue_title:I = 0x7f120e98

.field public static final kitchen_printing_error_jam_body:I = 0x7f120e99

.field public static final kitchen_printing_error_jam_title:I = 0x7f120e9a

.field public static final kitchen_printing_error_mechanical_body:I = 0x7f120e9b

.field public static final kitchen_printing_error_mechanical_title:I = 0x7f120e9c

.field public static final kitchen_printing_error_out_of_paper_body:I = 0x7f120e9d

.field public static final kitchen_printing_error_out_of_paper_title:I = 0x7f120e9e

.field public static final kitchen_printing_error_printer_jobs_phone:I = 0x7f120e9f

.field public static final kitchen_printing_error_printer_jobs_tablet:I = 0x7f120ea0

.field public static final kitchen_printing_error_printer_stations_jobs_phone:I = 0x7f120ea1

.field public static final kitchen_printing_error_printer_stations_jobs_tablet:I = 0x7f120ea2

.field public static final kitchen_printing_error_reprint_failed:I = 0x7f120ea3

.field public static final kitchen_printing_error_title:I = 0x7f120ea4

.field public static final kitchen_printing_error_title_message:I = 0x7f120ea5

.field public static final kitchen_printing_error_unavailable_title:I = 0x7f120ea6

.field public static final kitchen_printing_name_hint:I = 0x7f120ea7

.field public static final kitchen_printing_order:I = 0x7f120ea8

.field public static final kitchen_printing_price_name_pattern:I = 0x7f120ea9

.field public static final kitchen_printing_printer_errors:I = 0x7f120eaa

.field public static final kitchen_printing_reprint:I = 0x7f120eab

.field public static final kitchen_printing_try_again:I = 0x7f120eac

.field public static final label_color:I = 0x7f120ead

.field public static final landing_pointofsale_logo:I = 0x7f120eae

.field public static final last_4_ssn_hint:I = 0x7f120eaf

.field public static final last_days:I = 0x7f120eb0

.field public static final last_name:I = 0x7f120eb1

.field public static final later:I = 0x7f120eb2

.field public static final latin_digit_one:I = 0x7f120eb3

.field public static final learn_about_square:I = 0x7f120eb4

.field public static final learn_more:I = 0x7f120eb5

.field public static final learn_more_about_deposits:I = 0x7f120eb6

.field public static final learn_more_lowercase_more:I = 0x7f120eb7

.field public static final learn_more_url_gb:I = 0x7f120eb8

.field public static final learn_more_with_arrow:I = 0x7f120eb9

.field public static final legacy_guest_mode_expiration_dialog_button_text:I = 0x7f120eba

.field public static final legacy_guest_mode_expiration_dialog_message:I = 0x7f120ebb

.field public static final legacy_guest_mode_expiration_dialog_title:I = 0x7f120ebc

.field public static final legal:I = 0x7f120ebd

.field public static final letters_abc_uppercase:I = 0x7f120ebe

.field public static final letters_def_uppercase:I = 0x7f120ebf

.field public static final letters_empty:I = 0x7f120ec0

.field public static final letters_ghi_uppercase:I = 0x7f120ec1

.field public static final letters_jkl_uppercase:I = 0x7f120ec2

.field public static final letters_mno_uppercase:I = 0x7f120ec3

.field public static final letters_pqrs_uppercase:I = 0x7f120ec4

.field public static final letters_tuv_uppercase:I = 0x7f120ec5

.field public static final letters_wxyz_uppercase:I = 0x7f120ec6

.field public static final libraries:I = 0x7f120ec7

.field public static final library:I = 0x7f120ec8

.field public static final library_go_to_items_applet:I = 0x7f120ec9

.field public static final licenses:I = 0x7f120eca

.field public static final link:I = 0x7f120ecb

.field public static final link_activated_msg:I = 0x7f120ecc

.field public static final link_bank_account:I = 0x7f120ecd

.field public static final link_bank_account_continue:I = 0x7f120ece

.field public static final link_bank_account_failed:I = 0x7f120ecf

.field public static final link_bank_account_notification_browser_dialog_message:I = 0x7f120ed0

.field public static final link_bank_account_notification_content:I = 0x7f120ed1

.field public static final link_bank_account_notification_title:I = 0x7f120ed2

.field public static final link_bank_account_subtext:I = 0x7f120ed3

.field public static final link_bank_account_subtitle:I = 0x7f120ed4

.field public static final link_bank_account_url:I = 0x7f120ed5

.field public static final link_deactivated_msg:I = 0x7f120ed6

.field public static final link_debit_card_subtitle:I = 0x7f120ed7

.field public static final link_deleted_msg:I = 0x7f120ed8

.field public static final link_purpose:I = 0x7f120ed9

.field public static final linked_bank_account_uppercase:I = 0x7f120eda

.field public static final list_pattern_long_final_separator:I = 0x7f120edb

.field public static final list_pattern_long_nonfinal_separator:I = 0x7f120edc

.field public static final list_pattern_long_two_separator:I = 0x7f120edd

.field public static final list_pattern_short:I = 0x7f120ede

.field public static final list_three:I = 0x7f120edf

.field public static final list_two:I = 0x7f120ee0

.field public static final load_balance_error_message_body:I = 0x7f120ee1

.field public static final load_balance_error_message_title:I = 0x7f120ee2

.field public static final load_bank_account_error_title:I = 0x7f120ee3

.field public static final load_card_activity_error_message_body:I = 0x7f120ee4

.field public static final load_card_activity_error_message_title:I = 0x7f120ee5

.field public static final load_deposits_error_title:I = 0x7f120ee6

.field public static final loading:I = 0x7f120ee7

.field public static final loading_register:I = 0x7f120ee8

.field public static final loading_register_failed:I = 0x7f120ee9

.field public static final loading_register_failed_button_configure_settings:I = 0x7f120eea

.field public static final loading_register_failed_button_try_again:I = 0x7f120eeb

.field public static final loading_register_failed_description:I = 0x7f120eec

.field public static final loading_register_failed_screen_title:I = 0x7f120eed

.field public static final loading_register_sync_percent:I = 0x7f120eee

.field public static final location_go_to_location_settings:I = 0x7f120eef

.field public static final location_off_message:I = 0x7f120ef0

.field public static final location_off_title:I = 0x7f120ef1

.field public static final location_use_gps_satellites:I = 0x7f120ef2

.field public static final location_use_wireless_networks:I = 0x7f120ef3

.field public static final location_waiting_message:I = 0x7f120ef4

.field public static final location_waiting_speed_up_message:I = 0x7f120ef5

.field public static final location_waiting_title:I = 0x7f120ef6

.field public static final location_why:I = 0x7f120ef7

.field public static final login_email_hint:I = 0x7f120ef8

.field public static final login_failed_message:I = 0x7f120ef9

.field public static final login_failed_title:I = 0x7f120efa

.field public static final login_new_account:I = 0x7f120efb

.field public static final login_password_hint:I = 0x7f120efc

.field public static final long_friday:I = 0x7f120efd

.field public static final long_monday:I = 0x7f120efe

.field public static final long_saturday:I = 0x7f120eff

.field public static final long_sunday:I = 0x7f120f00

.field public static final long_thursday:I = 0x7f120f01

.field public static final long_tuesday:I = 0x7f120f02

.field public static final long_wednesday:I = 0x7f120f03

.field public static final loyalty_adjust_points:I = 0x7f120f04

.field public static final loyalty_adjust_points_step_one_description:I = 0x7f120f05

.field public static final loyalty_adjust_points_step_one_title:I = 0x7f120f06

.field public static final loyalty_adjust_points_step_three_description:I = 0x7f120f07

.field public static final loyalty_adjust_points_step_three_title:I = 0x7f120f08

.field public static final loyalty_adjust_points_step_two_description:I = 0x7f120f09

.field public static final loyalty_adjust_points_step_two_title:I = 0x7f120f0a

.field public static final loyalty_adjust_points_subtext:I = 0x7f120f0b

.field public static final loyalty_all_done:I = 0x7f120f0c

.field public static final loyalty_cart_diff_banner:I = 0x7f120f0d

.field public static final loyalty_cart_row_label:I = 0x7f120f0e

.field public static final loyalty_cart_row_value:I = 0x7f120f0f

.field public static final loyalty_cash_app:I = 0x7f120f10

.field public static final loyalty_cash_app_sms:I = 0x7f120f11

.field public static final loyalty_check_in_balance_text:I = 0x7f120f12

.field public static final loyalty_check_in_error_check_in:I = 0x7f120f13

.field public static final loyalty_check_in_error_redeeming:I = 0x7f120f14

.field public static final loyalty_check_in_redeem_card_points_text:I = 0x7f120f15

.field public static final loyalty_check_in_redeemed_reward:I = 0x7f120f16

.field public static final loyalty_check_in_welcome_back_rewards_subtitle:I = 0x7f120f17

.field public static final loyalty_congratulations_earned_multiple_points:I = 0x7f120f18

.field public static final loyalty_congratulations_linked_card:I = 0x7f120f19

.field public static final loyalty_congratulations_linked_phone:I = 0x7f120f1a

.field public static final loyalty_congratulations_multiple_available_rewards:I = 0x7f120f1b

.field public static final loyalty_congratulations_multiple_available_single_reward:I = 0x7f120f1c

.field public static final loyalty_congratulations_no_points_newly_enrolled_sms:I = 0x7f120f1d

.field public static final loyalty_congratulations_reward_single_tier:I = 0x7f120f1e

.field public static final loyalty_congratulations_title:I = 0x7f120f1f

.field public static final loyalty_congratulations_total_points_multiple:I = 0x7f120f20

.field public static final loyalty_congratulations_welcome_check_sms:I = 0x7f120f21

.field public static final loyalty_congratulations_welcome_multiple_points:I = 0x7f120f22

.field public static final loyalty_congratulations_welcome_one_point:I = 0x7f120f23

.field public static final loyalty_did_not_qualify:I = 0x7f120f24

.field public static final loyalty_enroll:I = 0x7f120f25

.field public static final loyalty_enroll_almost_there:I = 0x7f120f26

.field public static final loyalty_enroll_button_check_in:I = 0x7f120f27

.field public static final loyalty_enroll_button_help:I = 0x7f120f28

.field public static final loyalty_enroll_button_help_marketing_integration:I = 0x7f120f29

.field public static final loyalty_enroll_button_help_shorter:I = 0x7f120f2a

.field public static final loyalty_enroll_button_help_shorter_marketing_integration:I = 0x7f120f2b

.field public static final loyalty_enroll_button_multiple_points:I = 0x7f120f2c

.field public static final loyalty_enroll_button_no_stars:I = 0x7f120f2d

.field public static final loyalty_enroll_button_one_point:I = 0x7f120f2e

.field public static final loyalty_enroll_join_loyalty:I = 0x7f120f2f

.field public static final loyalty_enroll_phone_edit_hint:I = 0x7f120f30

.field public static final loyalty_enroll_phone_edit_prefilled_phone_hint:I = 0x7f120f31

.field public static final loyalty_enroll_qualify:I = 0x7f120f32

.field public static final loyalty_enroll_step_one_description:I = 0x7f120f33

.field public static final loyalty_enroll_step_one_title:I = 0x7f120f34

.field public static final loyalty_enroll_step_three_description:I = 0x7f120f35

.field public static final loyalty_enroll_step_three_title:I = 0x7f120f36

.field public static final loyalty_enroll_step_two_description:I = 0x7f120f37

.field public static final loyalty_enroll_step_two_title:I = 0x7f120f38

.field public static final loyalty_enroll_subtext:I = 0x7f120f39

.field public static final loyalty_join_loyalty:I = 0x7f120f3a

.field public static final loyalty_no_thanks_button:I = 0x7f120f3b

.field public static final loyalty_points_amount:I = 0x7f120f3c

.field public static final loyalty_points_amount_new:I = 0x7f120f3d

.field public static final loyalty_points_available:I = 0x7f120f3e

.field public static final loyalty_points_default_plural:I = 0x7f120f3f

.field public static final loyalty_points_default_singular:I = 0x7f120f40

.field public static final loyalty_points_earned_preview_multiple:I = 0x7f120f41

.field public static final loyalty_points_earned_preview_single:I = 0x7f120f42

.field public static final loyalty_points_with_suffix:I = 0x7f120f43

.field public static final loyalty_program_name:I = 0x7f120f44

.field public static final loyalty_redeem:I = 0x7f120f45

.field public static final loyalty_redeem_rewards_step_one_description:I = 0x7f120f46

.field public static final loyalty_redeem_rewards_step_one_title:I = 0x7f120f47

.field public static final loyalty_redeem_rewards_step_three_description:I = 0x7f120f48

.field public static final loyalty_redeem_rewards_step_three_title:I = 0x7f120f49

.field public static final loyalty_redeem_rewards_step_two_description:I = 0x7f120f4a

.field public static final loyalty_redeem_rewards_step_two_title:I = 0x7f120f4b

.field public static final loyalty_redeem_subtext:I = 0x7f120f4c

.field public static final loyalty_report_average_spend:I = 0x7f120f4d

.field public static final loyalty_report_average_visits:I = 0x7f120f4e

.field public static final loyalty_report_customer_visits_plural:I = 0x7f120f4f

.field public static final loyalty_report_customer_visits_singular:I = 0x7f120f50

.field public static final loyalty_report_customize_report:I = 0x7f120f51

.field public static final loyalty_report_date_pattern:I = 0x7f120f52

.field public static final loyalty_report_date_range:I = 0x7f120f53

.field public static final loyalty_report_loyalty_customers:I = 0x7f120f54

.field public static final loyalty_report_network_error_button:I = 0x7f120f55

.field public static final loyalty_report_network_error_message:I = 0x7f120f56

.field public static final loyalty_report_network_error_title:I = 0x7f120f57

.field public static final loyalty_report_non_loyalty_customers:I = 0x7f120f58

.field public static final loyalty_report_not_available:I = 0x7f120f59

.field public static final loyalty_report_not_available_shorthand:I = 0x7f120f5a

.field public static final loyalty_report_rewards_redeemed:I = 0x7f120f5b

.field public static final loyalty_report_save:I = 0x7f120f5c

.field public static final loyalty_report_selector_last_month:I = 0x7f120f5d

.field public static final loyalty_report_selector_last_year:I = 0x7f120f5e

.field public static final loyalty_report_selector_this_month:I = 0x7f120f5f

.field public static final loyalty_report_selector_this_year:I = 0x7f120f60

.field public static final loyalty_report_summary:I = 0x7f120f61

.field public static final loyalty_report_title:I = 0x7f120f62

.field public static final loyalty_report_top_customers:I = 0x7f120f63

.field public static final loyalty_report_total:I = 0x7f120f64

.field public static final loyalty_report_view_dashboard:I = 0x7f120f65

.field public static final loyalty_report_view_dashboard_details:I = 0x7f120f66

.field public static final loyalty_report_view_dashboard_link:I = 0x7f120f67

.field public static final loyalty_reward_requirement_no_points:I = 0x7f120f68

.field public static final loyalty_reward_tier_requirement:I = 0x7f120f69

.field public static final loyalty_rewards_amount:I = 0x7f120f6a

.field public static final loyalty_rewards_amount_new:I = 0x7f120f6b

.field public static final loyalty_rewards_available:I = 0x7f120f6c

.field public static final loyalty_rewards_no_available:I = 0x7f120f6d

.field public static final loyalty_rewards_term_plural:I = 0x7f120f6e

.field public static final loyalty_rewards_term_singular:I = 0x7f120f6f

.field public static final loyalty_row_add_customer_to_claim:I = 0x7f120f70

.field public static final loyalty_row_coupons_available:I = 0x7f120f71

.field public static final loyalty_row_enroll_to_claim:I = 0x7f120f72

.field public static final loyalty_row_rewards_available:I = 0x7f120f73

.field public static final loyalty_rule_item:I = 0x7f120f74

.field public static final loyalty_rule_spend:I = 0x7f120f75

.field public static final loyalty_rule_visit:I = 0x7f120f76

.field public static final loyalty_send_status:I = 0x7f120f77

.field public static final loyalty_settings_enable_loyalty:I = 0x7f120f78

.field public static final loyalty_settings_enable_loyalty_subtitle:I = 0x7f120f79

.field public static final loyalty_settings_front_of_transaction_subtitle:I = 0x7f120f7a

.field public static final loyalty_settings_front_of_transaction_title:I = 0x7f120f7b

.field public static final loyalty_settings_off:I = 0x7f120f7c

.field public static final loyalty_settings_on:I = 0x7f120f7d

.field public static final loyalty_settings_show_nonqualifying:I = 0x7f120f7e

.field public static final loyalty_settings_show_nonqualifying_short:I = 0x7f120f7f

.field public static final loyalty_settings_show_nonqualifying_subtitle:I = 0x7f120f80

.field public static final loyalty_settings_timeout_30:I = 0x7f120f81

.field public static final loyalty_settings_timeout_60:I = 0x7f120f82

.field public static final loyalty_settings_timeout_90:I = 0x7f120f83

.field public static final loyalty_settings_timeout_subtitle:I = 0x7f120f84

.field public static final loyalty_settings_timeout_title:I = 0x7f120f85

.field public static final loyalty_settings_title:I = 0x7f120f86

.field public static final loyalty_status_sent:I = 0x7f120f87

.field public static final loyalty_status_sent_to_phone:I = 0x7f120f88

.field public static final loyalty_url:I = 0x7f120f89

.field public static final make_this_invoice_recurring:I = 0x7f120f8a

.field public static final managing_bank_building_society_body:I = 0x7f120f8b

.field public static final managing_bank_building_society_title:I = 0x7f120f8c

.field public static final manual_card_entry:I = 0x7f120f8d

.field public static final manual_credit_card_entry:I = 0x7f120f8e

.field public static final manual_credit_card_entry_offline_mode:I = 0x7f120f8f

.field public static final manual_deposits_enabled_body_with_id:I = 0x7f120f90

.field public static final manual_deposits_enabled_body_without_id:I = 0x7f120f91

.field public static final manual_deposits_enabled_title:I = 0x7f120f92

.field public static final manual_gift_card_entry:I = 0x7f120f93

.field public static final manual_gift_card_entry_offline_mode:I = 0x7f120f94

.field public static final masked_numbers:I = 0x7f120f95

.field public static final merchant_category_heading:I = 0x7f120f96

.field public static final merchant_profile_add_logo:I = 0x7f120f97

.field public static final merchant_profile_add_photo:I = 0x7f120f98

.field public static final merchant_profile_business_address_dialog_button:I = 0x7f120f99

.field public static final merchant_profile_business_address_dialog_message:I = 0x7f120f9a

.field public static final merchant_profile_business_address_dialog_mobile_business:I = 0x7f120f9b

.field public static final merchant_profile_business_address_dialog_no_address:I = 0x7f120f9c

.field public static final merchant_profile_business_address_dialog_title:I = 0x7f120f9d

.field public static final merchant_profile_business_address_title:I = 0x7f120f9e

.field public static final merchant_profile_business_address_warning_no_po_box_allowed_message:I = 0x7f120f9f

.field public static final merchant_profile_business_address_warning_no_po_box_allowed_title:I = 0x7f120fa0

.field public static final merchant_profile_business_description_hint:I = 0x7f120fa1

.field public static final merchant_profile_business_name_hint:I = 0x7f120fa2

.field public static final merchant_profile_change_logo:I = 0x7f120fa3

.field public static final merchant_profile_change_logo_message:I = 0x7f120fa4

.field public static final merchant_profile_confirm_business_address_dialog_button_negative:I = 0x7f120fa5

.field public static final merchant_profile_confirm_business_address_dialog_button_positive:I = 0x7f120fa6

.field public static final merchant_profile_confirm_business_address_dialog_message_address:I = 0x7f120fa7

.field public static final merchant_profile_confirm_business_address_dialog_message_mobile:I = 0x7f120fa8

.field public static final merchant_profile_confirm_business_address_dialog_title:I = 0x7f120fa9

.field public static final merchant_profile_contact_email:I = 0x7f120faa

.field public static final merchant_profile_contact_email_selected:I = 0x7f120fab

.field public static final merchant_profile_contact_facebook:I = 0x7f120fac

.field public static final merchant_profile_contact_facebook_selected:I = 0x7f120fad

.field public static final merchant_profile_contact_phone:I = 0x7f120fae

.field public static final merchant_profile_contact_phone_selected:I = 0x7f120faf

.field public static final merchant_profile_contact_twitter:I = 0x7f120fb0

.field public static final merchant_profile_contact_twitter_selected:I = 0x7f120fb1

.field public static final merchant_profile_contact_website:I = 0x7f120fb2

.field public static final merchant_profile_contact_website_selected:I = 0x7f120fb3

.field public static final merchant_profile_featured_image_error_small:I = 0x7f120fb4

.field public static final merchant_profile_featured_image_error_small_title:I = 0x7f120fb5

.field public static final merchant_profile_featured_image_error_wrong_type:I = 0x7f120fb6

.field public static final merchant_profile_featured_image_error_wrong_type_title:I = 0x7f120fb7

.field public static final merchant_profile_featured_image_hint:I = 0x7f120fb8

.field public static final merchant_profile_load_failed:I = 0x7f120fb9

.field public static final merchant_profile_mobile_business_help_text:I = 0x7f120fba

.field public static final merchant_profile_mobile_business_switch_short:I = 0x7f120fbb

.field public static final merchant_profile_photo_on_receipt_switch:I = 0x7f120fbc

.field public static final merchant_profile_photo_on_receipt_switch_short:I = 0x7f120fbd

.field public static final merchant_profile_row_value:I = 0x7f120fbe

.field public static final merchant_profile_saving_image:I = 0x7f120fbf

.field public static final merchant_profile_saving_image_failed:I = 0x7f120fc0

.field public static final merchant_profile_saving_profile_failed_text:I = 0x7f120fc1

.field public static final merchant_profile_saving_profile_failed_title:I = 0x7f120fc2

.field public static final merchant_profile_title:I = 0x7f120fc3

.field public static final message:I = 0x7f120fc4

.field public static final message_us:I = 0x7f120fc5

.field public static final messagebar_missing_microphone_permission:I = 0x7f120fc6

.field public static final messagebar_reader_checking_manifest_updates:I = 0x7f120fc7

.field public static final messagebar_reader_connecting:I = 0x7f120fc8

.field public static final messagebar_reader_fwup_blocking_progress:I = 0x7f120fc9

.field public static final messagebar_reader_fwup_blocking_progress_no_details:I = 0x7f120fca

.field public static final messagebar_reader_fwup_rebooting:I = 0x7f120fcb

.field public static final messagebar_reader_fwup_rebooting_will_reboot:I = 0x7f120fcc

.field public static final messagebar_reader_not_ready:I = 0x7f120fcd

.field public static final messagebar_reader_not_ready_and_tap:I = 0x7f120fce

.field public static final messagebar_reader_offline:I = 0x7f120fcf

.field public static final messagebar_reader_offline_mode:I = 0x7f120fd0

.field public static final messagebar_reader_offline_swipe_only:I = 0x7f120fd1

.field public static final messagebar_reader_ready:I = 0x7f120fd2

.field public static final messagebar_reader_ready_card_inserted:I = 0x7f120fd3

.field public static final messagebar_reader_ready_card_inserted_press_charge:I = 0x7f120fd4

.field public static final messagebar_reader_ready_card_swiped:I = 0x7f120fd5

.field public static final messagebar_reader_ready_card_swiped_press_charge:I = 0x7f120fd6

.field public static final messagebar_reader_ready_low_battery_plural:I = 0x7f120fd7

.field public static final messagebar_reader_ready_low_battery_single:I = 0x7f120fd8

.field public static final messagebar_reader_ss_connecting:I = 0x7f120fd9

.field public static final messagebar_reader_unavailable:I = 0x7f120fda

.field public static final messagebar_reader_unavailable_and_tap:I = 0x7f120fdb

.field public static final messagebar_readers_connecting_plural:I = 0x7f120fdc

.field public static final messagebar_readers_connecting_single:I = 0x7f120fdd

.field public static final messagebar_readers_not_ready_plural:I = 0x7f120fde

.field public static final messagebar_readers_not_ready_single:I = 0x7f120fdf

.field public static final messagebar_readers_ready_plural:I = 0x7f120fe0

.field public static final messagebar_readers_ready_single:I = 0x7f120fe1

.field public static final messagebar_readers_updating_plural:I = 0x7f120fe2

.field public static final messagebar_readers_updating_single:I = 0x7f120fe3

.field public static final messages_count_multiple:I = 0x7f120fe4

.field public static final messages_count_one:I = 0x7f120fe5

.field public static final messages_notification_content:I = 0x7f120fe6

.field public static final messages_notification_title:I = 0x7f120fe7

.field public static final messages_visit_help_center:I = 0x7f120fe8

.field public static final missing_account_fields:I = 0x7f120fe9

.field public static final missing_account_holder:I = 0x7f120fea

.field public static final missing_account_number:I = 0x7f120feb

.field public static final missing_account_type:I = 0x7f120fec

.field public static final missing_address_fields:I = 0x7f120fed

.field public static final missing_address_fields_message:I = 0x7f120fee

.field public static final missing_address_fields_title:I = 0x7f120fef

.field public static final missing_date_of_birth:I = 0x7f120ff0

.field public static final missing_debit_card_info:I = 0x7f120ff1

.field public static final missing_debit_card_info_message:I = 0x7f120ff2

.field public static final missing_ein:I = 0x7f120ff3

.field public static final missing_ein_body:I = 0x7f120ff4

.field public static final missing_name_fields:I = 0x7f120ff5

.field public static final missing_personal_fields:I = 0x7f120ff6

.field public static final missing_personal_fields_with_itin:I = 0x7f120ff7

.field public static final missing_phone_number_field:I = 0x7f120ff8

.field public static final missing_required_field:I = 0x7f120ff9

.field public static final mobile_phone_number:I = 0x7f120ffa

.field public static final modifier_assignment_title:I = 0x7f120ffb

.field public static final modifier_assignment_title_no_name:I = 0x7f120ffc

.field public static final modifier_atm_withdrawal_fee:I = 0x7f120ffd

.field public static final modifier_cash_back:I = 0x7f120ffe

.field public static final modifier_community_reward:I = 0x7f120fff

.field public static final modifier_count_plural:I = 0x7f121000

.field public static final modifier_count_single:I = 0x7f121001

.field public static final modifier_count_zero:I = 0x7f121002

.field public static final modifier_hint_url:I = 0x7f121003

.field public static final modifier_instant_transfer_fee:I = 0x7f121004

.field public static final modifier_items_count_plural:I = 0x7f121005

.field public static final modifier_items_count_single:I = 0x7f121006

.field public static final modifier_items_count_zero:I = 0x7f121007

.field public static final modifier_new_hint:I = 0x7f121008

.field public static final modifier_no_modes:I = 0x7f121009

.field public static final modifier_one_mode:I = 0x7f12100a

.field public static final modifier_option_delete_content_description:I = 0x7f12100b

.field public static final modifier_option_name_hint:I = 0x7f12100c

.field public static final modifier_option_name_required_warning_message:I = 0x7f12100d

.field public static final modifier_option_required_warning_message:I = 0x7f12100e

.field public static final modifier_option_required_warning_title:I = 0x7f12100f

.field public static final modifier_options_count_plural:I = 0x7f121010

.field public static final modifier_options_count_single:I = 0x7f121011

.field public static final modifier_options_count_zero:I = 0x7f121012

.field public static final modifier_options_hint:I = 0x7f121013

.field public static final modifier_required_warning_message:I = 0x7f121014

.field public static final modifier_required_warning_title:I = 0x7f121015

.field public static final modifier_set:I = 0x7f121016

.field public static final modifier_set_advanced_modifier_instruction:I = 0x7f121017

.field public static final modifier_set_delete_button:I = 0x7f121018

.field public static final modifier_set_name_required_warning_message:I = 0x7f121019

.field public static final modifier_set_null_subtitle:I = 0x7f12101a

.field public static final modifier_set_null_title:I = 0x7f12101b

.field public static final modifier_three_modes:I = 0x7f12101c

.field public static final modifier_two_modes:I = 0x7f12101d

.field public static final modifiers_fixed_null_title:I = 0x7f12101e

.field public static final mondays:I = 0x7f12101f

.field public static final month:I = 0x7f121020

.field public static final month_name_format:I = 0x7f121021

.field public static final month_only_name_format:I = 0x7f121022

.field public static final months:I = 0x7f121023

.field public static final more:I = 0x7f121024

.field public static final mtrl_badge_numberless_content_description:I = 0x7f121025

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f121026

.field public static final mtrl_exceed_max_badge_number_suffix:I = 0x7f121027

.field public static final mtrl_picker_a11y_next_month:I = 0x7f121028

.field public static final mtrl_picker_a11y_prev_month:I = 0x7f121029

.field public static final mtrl_picker_announce_current_selection:I = 0x7f12102a

.field public static final mtrl_picker_cancel:I = 0x7f12102b

.field public static final mtrl_picker_confirm:I = 0x7f12102c

.field public static final mtrl_picker_date_header_selected:I = 0x7f12102d

.field public static final mtrl_picker_date_header_title:I = 0x7f12102e

.field public static final mtrl_picker_date_header_unselected:I = 0x7f12102f

.field public static final mtrl_picker_day_of_week_column_header:I = 0x7f121030

.field public static final mtrl_picker_invalid_format:I = 0x7f121031

.field public static final mtrl_picker_invalid_format_example:I = 0x7f121032

.field public static final mtrl_picker_invalid_format_use:I = 0x7f121033

.field public static final mtrl_picker_invalid_range:I = 0x7f121034

.field public static final mtrl_picker_navigate_to_year_description:I = 0x7f121035

.field public static final mtrl_picker_out_of_range:I = 0x7f121036

.field public static final mtrl_picker_range_header_only_end_selected:I = 0x7f121037

.field public static final mtrl_picker_range_header_only_start_selected:I = 0x7f121038

.field public static final mtrl_picker_range_header_selected:I = 0x7f121039

.field public static final mtrl_picker_range_header_title:I = 0x7f12103a

.field public static final mtrl_picker_range_header_unselected:I = 0x7f12103b

.field public static final mtrl_picker_save:I = 0x7f12103c

.field public static final mtrl_picker_text_input_date_hint:I = 0x7f12103d

.field public static final mtrl_picker_text_input_date_range_end_hint:I = 0x7f12103e

.field public static final mtrl_picker_text_input_date_range_start_hint:I = 0x7f12103f

.field public static final mtrl_picker_text_input_day_abbr:I = 0x7f121040

.field public static final mtrl_picker_text_input_month_abbr:I = 0x7f121041

.field public static final mtrl_picker_text_input_year_abbr:I = 0x7f121042

.field public static final mtrl_picker_toggle_to_calendar_input_mode:I = 0x7f121043

.field public static final mtrl_picker_toggle_to_day_selection:I = 0x7f121044

.field public static final mtrl_picker_toggle_to_text_input_mode:I = 0x7f121045

.field public static final mtrl_picker_toggle_to_year_selection:I = 0x7f121046

.field public static final multiple_due_dates:I = 0x7f121047

.field public static final multiple_reminder_schedules:I = 0x7f121048

.field public static final multiple_sku_results:I = 0x7f121049

.field public static final multiple_sku_results_variation_row_subtitle:I = 0x7f12104a

.field public static final name:I = 0x7f12104b

.field public static final name_cannot_exceed_255:I = 0x7f12104c

.field public static final name_is_required:I = 0x7f12104d

.field public static final name_on_bank_account:I = 0x7f12104e

.field public static final nanp_phone_hint:I = 0x7f12104f

.field public static final native_library_load_error_msg:I = 0x7f121050

.field public static final native_library_load_error_msg_blocked:I = 0x7f121051

.field public static final native_library_load_error_title:I = 0x7f121052

.field public static final navigation_open_tab_all_items:I = 0x7f121053

.field public static final navigation_open_tab_all_items_and_services:I = 0x7f121054

.field public static final navigation_open_tab_categories:I = 0x7f121055

.field public static final navigation_open_tab_discounts:I = 0x7f121056

.field public static final navigation_open_tab_gift_cards:I = 0x7f121057

.field public static final navigation_open_tab_hint:I = 0x7f121058

.field public static final navigation_open_tab_keypad:I = 0x7f121059

.field public static final navigation_open_tab_library:I = 0x7f12105a

.field public static final navigation_open_tab_page:I = 0x7f12105b

.field public static final network_error_failed:I = 0x7f12105c

.field public static final network_error_message:I = 0x7f12105d

.field public static final network_error_title:I = 0x7f12105e

.field public static final network_failure_message_description:I = 0x7f12105f

.field public static final network_failure_message_title:I = 0x7f121060

.field public static final never:I = 0x7f121061

.field public static final never_collect_signature_help_url:I = 0x7f121062

.field public static final new_badge:I = 0x7f121063

.field public static final new_card:I = 0x7f121064

.field public static final new_category:I = 0x7f121065

.field public static final new_discount:I = 0x7f121066

.field public static final new_feature_automatic_reminders:I = 0x7f121067

.field public static final new_item:I = 0x7f121068

.field public static final new_message_one:I = 0x7f121069

.field public static final new_message_two_or_more:I = 0x7f12106a

.field public static final new_sale:I = 0x7f12106b

.field public static final new_sale_with_customer:I = 0x7f12106c

.field public static final new_sale_with_customer_shortened:I = 0x7f12106d

.field public static final new_service:I = 0x7f12106e

.field public static final new_tutorial_notification_content_plural:I = 0x7f12106f

.field public static final new_tutorial_notification_content_singular:I = 0x7f121070

.field public static final new_tutorial_notification_title_plural:I = 0x7f121071

.field public static final new_tutorial_notification_title_singular:I = 0x7f121072

.field public static final new_word:I = 0x7f121073

.field public static final next:I = 0x7f121074

.field public static final next_business_day_deposit:I = 0x7f121075

.field public static final next_business_day_deposit_description:I = 0x7f121076

.field public static final nfc_enabled_visit_settings_button:I = 0x7f121077

.field public static final nfc_enabled_warning_message:I = 0x7f121078

.field public static final nfc_enabled_warning_title:I = 0x7f121079

.field public static final no:I = 0x7f12107a

.field public static final no_account_notifications_view_message:I = 0x7f12107b

.field public static final no_account_notifications_view_title:I = 0x7f12107c

.field public static final no_announcements_description:I = 0x7f12107d

.field public static final no_announcements_title:I = 0x7f12107e

.field public static final no_connection:I = 0x7f12107f

.field public static final no_custom_invoice_id:I = 0x7f121080

.field public static final no_customer:I = 0x7f121081

.field public static final no_duration:I = 0x7f121082

.field public static final no_file_viewer_description_default:I = 0x7f121083

.field public static final no_file_viewer_description_image:I = 0x7f121084

.field public static final no_file_viewer_description_pdf:I = 0x7f121085

.field public static final no_file_viewer_title:I = 0x7f121086

.field public static final no_file_viewer_title_default:I = 0x7f121087

.field public static final no_internet_connection:I = 0x7f121088

.field public static final no_invoice_title:I = 0x7f121089

.field public static final no_linked_bank_account_error_message:I = 0x7f12108a

.field public static final no_reader_connected:I = 0x7f12108b

.field public static final no_results:I = 0x7f12108c

.field public static final no_results_subtext:I = 0x7f12108d

.field public static final no_sale:I = 0x7f12108e

.field public static final no_sale_on_date_time_format:I = 0x7f12108f

.field public static final no_sale_uppercase:I = 0x7f121090

.field public static final no_thanks:I = 0x7f121091

.field public static final no_whats_new_notifications_view_message:I = 0x7f121092

.field public static final no_whats_new_notifications_view_title:I = 0x7f121093

.field public static final noho_editrow_plugin_clear:I = 0x7f121094

.field public static final noho_editrow_plugin_view_password:I = 0x7f121095

.field public static final noho_standard_black:I = 0x7f121096

.field public static final noho_standard_blue:I = 0x7f121097

.field public static final noho_standard_brown:I = 0x7f121098

.field public static final noho_standard_cyan:I = 0x7f121099

.field public static final noho_standard_cyan_dark:I = 0x7f12109a

.field public static final noho_standard_gold:I = 0x7f12109b

.field public static final noho_standard_green:I = 0x7f12109c

.field public static final noho_standard_indigo:I = 0x7f12109d

.field public static final noho_standard_lilac:I = 0x7f12109e

.field public static final noho_standard_lime:I = 0x7f12109f

.field public static final noho_standard_maroon:I = 0x7f1210a0

.field public static final noho_standard_orange:I = 0x7f1210a1

.field public static final noho_standard_pink:I = 0x7f1210a2

.field public static final noho_standard_plum:I = 0x7f1210a3

.field public static final noho_standard_purple:I = 0x7f1210a4

.field public static final noho_standard_teal:I = 0x7f1210a5

.field public static final noho_standard_white:I = 0x7f1210a6

.field public static final noho_time_picker_hour_minute_separator:I = 0x7f1210a7

.field public static final not_activated_account_error_message:I = 0x7f1210a8

.field public static final notification_center_applet_name:I = 0x7f1210a9

.field public static final notification_channel_account_issues:I = 0x7f1210aa

.field public static final notification_channel_background_processing:I = 0x7f1210ab

.field public static final notification_channel_messages:I = 0x7f1210ac

.field public static final notification_channel_payments:I = 0x7f1210ad

.field public static final notification_channel_profile_errors:I = 0x7f1210ae

.field public static final notification_channel_storage_errors:I = 0x7f1210af

.field public static final notification_channel_support_messaging:I = 0x7f1210b0

.field public static final notification_channel_updates:I = 0x7f1210b1

.field public static final notification_incomplete_transaction_text:I = 0x7f1210b2

.field public static final notification_incomplete_transaction_title:I = 0x7f1210b3

.field public static final notification_open_in_browser_content_description:I = 0x7f1210b4

.field public static final notification_preferences_action_bar_title:I = 0x7f1210b5

.field public static final notification_preferences_email_address_label:I = 0x7f1210b6

.field public static final notification_preferences_email_label:I = 0x7f1210b7

.field public static final notification_preferences_email_message:I = 0x7f1210b8

.field public static final notification_preferences_error_fetching_notifications_title:I = 0x7f1210b9

.field public static final notification_preferences_error_message:I = 0x7f1210ba

.field public static final notification_preferences_error_updating_notifications_title:I = 0x7f1210bb

.field public static final notification_preferences_inscreen_error_updating_notifications:I = 0x7f1210bc

.field public static final notification_preferences_preferences_card_declines:I = 0x7f1210bd

.field public static final notification_preferences_preferences_label:I = 0x7f1210be

.field public static final notification_preferences_top_message:I = 0x7f1210bf

.field public static final ntep_certification_number:I = 0x7f1210c0

.field public static final ntep_model:I = 0x7f1210c1

.field public static final ntep_title:I = 0x7f1210c2

.field public static final number_0:I = 0x7f1210c3

.field public static final number_2:I = 0x7f1210c4

.field public static final number_3:I = 0x7f1210c5

.field public static final number_4:I = 0x7f1210c6

.field public static final o1_reminder_ja_body:I = 0x7f1210c7

.field public static final o1_reminder_ja_confirm:I = 0x7f1210c8

.field public static final o1_reminder_ja_title:I = 0x7f1210c9

.field public static final o1_reminder_ja_url:I = 0x7f1210ca

.field public static final offline_mode:I = 0x7f1210cb

.field public static final offline_mode_cannot_purchase_gift_cards:I = 0x7f1210cc

.field public static final offline_mode_cnp_card_not_accepted:I = 0x7f1210cd

.field public static final offline_mode_declined:I = 0x7f1210ce

.field public static final offline_mode_enable_hint:I = 0x7f1210cf

.field public static final offline_mode_enabled_off:I = 0x7f1210d0

.field public static final offline_mode_enabled_on:I = 0x7f1210d1

.field public static final offline_mode_for_more_information:I = 0x7f1210d2

.field public static final offline_mode_for_more_information_quick_enable:I = 0x7f1210d3

.field public static final offline_mode_invalid_location:I = 0x7f1210d4

.field public static final offline_mode_no_limit:I = 0x7f1210d5

.field public static final offline_mode_non_whitelisted_brand:I = 0x7f1210d6

.field public static final offline_mode_transaction_limit_hint:I = 0x7f1210d7

.field public static final offline_mode_transaction_limit_hint_no_tips:I = 0x7f1210d8

.field public static final offline_mode_transaction_limit_message:I = 0x7f1210d9

.field public static final offline_mode_transaction_limit_title:I = 0x7f1210da

.field public static final offline_mode_unavailable:I = 0x7f1210db

.field public static final offline_mode_unavailable_hint:I = 0x7f1210dc

.field public static final offline_mode_url:I = 0x7f1210dd

.field public static final offline_mode_warning:I = 0x7f1210de

.field public static final offline_payment_failed:I = 0x7f1210df

.field public static final offline_payment_failed_message:I = 0x7f1210e0

.field public static final ok:I = 0x7f1210e1

.field public static final okay:I = 0x7f1210e2

.field public static final on_a_set_date:I = 0x7f1210e3

.field public static final on_date:I = 0x7f1210e4

.field public static final onboarding_accept_cash_only:I = 0x7f1210e5

.field public static final onboarding_actionbar_cancel:I = 0x7f1210e6

.field public static final onboarding_actionbar_continue:I = 0x7f1210e7

.field public static final onboarding_actionbar_later:I = 0x7f1210e8

.field public static final onboarding_actionbar_save:I = 0x7f1210e9

.field public static final onboarding_actionbar_save_address:I = 0x7f1210ea

.field public static final onboarding_actionbar_send:I = 0x7f1210eb

.field public static final onboarding_activate_in_app:I = 0x7f1210ec

.field public static final onboarding_activate_message:I = 0x7f1210ed

.field public static final onboarding_activate_on_web:I = 0x7f1210ee

.field public static final onboarding_alternative_activate_in_app:I = 0x7f1210ef

.field public static final onboarding_alternative_activate_message:I = 0x7f1210f0

.field public static final onboarding_answer_all:I = 0x7f1210f1

.field public static final onboarding_answers_fail:I = 0x7f1210f2

.field public static final onboarding_answers_progress:I = 0x7f1210f3

.field public static final onboarding_apply_failure_title:I = 0x7f1210f4

.field public static final onboarding_apply_progress_title:I = 0x7f1210f5

.field public static final onboarding_bank_account_confirm_later:I = 0x7f1210f6

.field public static final onboarding_bank_add_failure_title:I = 0x7f1210f7

.field public static final onboarding_bank_add_progress:I = 0x7f1210f8

.field public static final onboarding_card_reader_options_have:I = 0x7f1210f9

.field public static final onboarding_card_reader_options_send:I = 0x7f1210fa

.field public static final onboarding_confirm_address_title:I = 0x7f1210fb

.field public static final onboarding_confirm_magstripe_title:I = 0x7f1210fc

.field public static final onboarding_confirm_same_day_deposit_fee_message:I = 0x7f1210fd

.field public static final onboarding_confirm_same_day_deposit_fee_title:I = 0x7f1210fe

.field public static final onboarding_country:I = 0x7f1210ff

.field public static final onboarding_federal_has_ein:I = 0x7f121100

.field public static final onboarding_federal_no_ein:I = 0x7f121101

.field public static final onboarding_finalize_account_setup:I = 0x7f121102

.field public static final onboarding_finished_heading:I = 0x7f121103

.field public static final onboarding_finished_subheading:I = 0x7f121104

.field public static final onboarding_get_web_link_fail:I = 0x7f121105

.field public static final onboarding_help_url:I = 0x7f121106

.field public static final onboarding_modal_button_later:I = 0x7f121107

.field public static final onboarding_modal_button_setup_payments:I = 0x7f121108

.field public static final onboarding_modal_idv_estimate_error_message:I = 0x7f121109

.field public static final onboarding_modal_idv_estimate_error_title:I = 0x7f12110a

.field public static final onboarding_modal_idv_invoice_error_message:I = 0x7f12110b

.field public static final onboarding_modal_idv_invoice_error_title:I = 0x7f12110c

.field public static final onboarding_modal_idv_invoice_reminder_message:I = 0x7f12110d

.field public static final onboarding_modal_idv_invoice_reminder_title:I = 0x7f12110e

.field public static final onboarding_modal_send_with_no_idv_message:I = 0x7f12110f

.field public static final onboarding_modal_send_with_no_idv_title:I = 0x7f121110

.field public static final onboarding_panel_error_button:I = 0x7f121111

.field public static final onboarding_panel_error_link:I = 0x7f121112

.field public static final onboarding_panel_error_support:I = 0x7f121113

.field public static final onboarding_panel_error_title:I = 0x7f121114

.field public static final onboarding_panel_processing:I = 0x7f121115

.field public static final onboarding_panel_unknown_component:I = 0x7f121116

.field public static final onboarding_quiz_timeout_explanation:I = 0x7f121117

.field public static final onboarding_quiz_timeout_title:I = 0x7f121118

.field public static final onboarding_reader_on_the_way:I = 0x7f121119

.field public static final onboarding_retry_message:I = 0x7f12111a

.field public static final onboarding_retry_title:I = 0x7f12111b

.field public static final onboarding_send_reader:I = 0x7f12111c

.field public static final onboarding_shipping_confirm_have_reader_button:I = 0x7f12111d

.field public static final onboarding_shipping_confirm_have_reader_cancel_button:I = 0x7f12111e

.field public static final onboarding_shipping_confirm_have_reader_message:I = 0x7f12111f

.field public static final onboarding_shipping_confirm_have_reader_title:I = 0x7f121120

.field public static final onboarding_shipping_confirm_later:I = 0x7f121121

.field public static final onboarding_status_failure_title:I = 0x7f121122

.field public static final onboarding_status_progress_title:I = 0x7f121123

.field public static final one_day:I = 0x7f121124

.field public static final one_month:I = 0x7f121125

.field public static final one_time:I = 0x7f121126

.field public static final one_week:I = 0x7f121127

.field public static final one_year:I = 0x7f121128

.field public static final online_bank_linking:I = 0x7f121129

.field public static final online_checkout_buylink_url:I = 0x7f12112a

.field public static final online_checkout_buylink_url_staging:I = 0x7f12112b

.field public static final online_checkout_checkout_link:I = 0x7f12112c

.field public static final online_checkout_checkout_link_help_text:I = 0x7f12112d

.field public static final online_checkout_checkout_link_help_text_no_qr_code:I = 0x7f12112e

.field public static final online_checkout_checkout_link_label:I = 0x7f12112f

.field public static final online_checkout_checkout_link_share_link:I = 0x7f121130

.field public static final online_checkout_clipboard_label:I = 0x7f121131

.field public static final online_checkout_copied_toast_message:I = 0x7f121132

.field public static final online_checkout_create_link:I = 0x7f121133

.field public static final online_checkout_edit_link:I = 0x7f121134

.field public static final online_checkout_error_screen_message:I = 0x7f121135

.field public static final online_checkout_error_screen_retry_text:I = 0x7f121136

.field public static final online_checkout_error_screen_title:I = 0x7f121137

.field public static final online_checkout_feature_not_available_msg:I = 0x7f121138

.field public static final online_checkout_get_link_name_is_required:I = 0x7f121139

.field public static final online_checkout_get_link_name_over_255:I = 0x7f12113a

.field public static final online_checkout_link_qr_code_content_desc:I = 0x7f12113b

.field public static final online_checkout_links:I = 0x7f12113c

.field public static final online_checkout_new_sale:I = 0x7f12113d

.field public static final online_checkout_pay_link_already_exists_error_msg:I = 0x7f12113e

.field public static final online_checkout_pay_link_get_link_label:I = 0x7f12113f

.field public static final online_checkout_pay_link_name_hint:I = 0x7f121140

.field public static final online_checkout_pay_link_name_label:I = 0x7f121141

.field public static final online_checkout_paylink_url:I = 0x7f121142

.field public static final online_checkout_paylink_url_staging:I = 0x7f121143

.field public static final online_checkout_settings_action_save:I = 0x7f121144

.field public static final online_checkout_settings_checkout_link_actions:I = 0x7f121145

.field public static final online_checkout_settings_checkout_link_help_text:I = 0x7f121146

.field public static final online_checkout_settings_checkout_link_help_text_no_qr_code:I = 0x7f121147

.field public static final online_checkout_settings_checkout_link_label:I = 0x7f121148

.field public static final online_checkout_settings_checkout_link_qr_code_content_desc:I = 0x7f121149

.field public static final online_checkout_settings_checkout_link_share_link:I = 0x7f12114a

.field public static final online_checkout_settings_could_not_enable:I = 0x7f12114b

.field public static final online_checkout_settings_could_not_enable_help_text:I = 0x7f12114c

.field public static final online_checkout_settings_donation_checkout_link_help_text:I = 0x7f12114d

.field public static final online_checkout_settings_donation_checkout_link_help_text_no_qr_code:I = 0x7f12114e

.field public static final online_checkout_settings_enable_checkout_links:I = 0x7f12114f

.field public static final online_checkout_settings_enabled:I = 0x7f121150

.field public static final online_checkout_settings_enabled_help_text:I = 0x7f121151

.field public static final online_checkout_settings_enabling_checkout_links_help_text:I = 0x7f121152

.field public static final online_checkout_settings_enabling_checkout_links_title:I = 0x7f121153

.field public static final online_checkout_settings_feature_not_available_msg:I = 0x7f121154

.field public static final online_checkout_settings_network_error_title:I = 0x7f121155

.field public static final online_checkout_settings_retry:I = 0x7f121156

.field public static final online_checkout_settings_start_selling_online:I = 0x7f121157

.field public static final online_checkout_settings_start_selling_online_help_text:I = 0x7f121158

.field public static final online_checkout_settings_title:I = 0x7f121159

.field public static final online_checkout_share_sheet_subject:I = 0x7f12115a

.field public static final online_checkout_share_sheet_title:I = 0x7f12115b

.field public static final online_checkout_title:I = 0x7f12115c

.field public static final online_checkout_title_item_unavailable:I = 0x7f12115d

.field public static final online_checkout_title_items_only:I = 0x7f12115e

.field public static final online_checkout_title_no_split_payments:I = 0x7f12115f

.field public static final online_checkout_title_online_only:I = 0x7f121160

.field public static final online_checkout_title_single_items_only:I = 0x7f121161

.field public static final online_checkout_title_with_money:I = 0x7f121162

.field public static final online_only_reason:I = 0x7f121163

.field public static final open_file:I = 0x7f121164

.field public static final open_in_browser_dialog_dismiss_button:I = 0x7f121165

.field public static final open_in_browser_dialog_unhandled_body:I = 0x7f121166

.field public static final open_in_browser_dialog_view_button:I = 0x7f121167

.field public static final open_menu:I = 0x7f121168

.field public static final open_tickets:I = 0x7f121169

.field public static final open_tickets_add_customer:I = 0x7f12116a

.field public static final open_tickets_as_home_screen_toggle_hint:I = 0x7f12116b

.field public static final open_tickets_as_home_screen_toggle_text:I = 0x7f12116c

.field public static final open_tickets_as_home_screen_toggle_text_short:I = 0x7f12116d

.field public static final open_tickets_available_tickets:I = 0x7f12116e

.field public static final open_tickets_card_not_stored:I = 0x7f12116f

.field public static final open_tickets_cart_total:I = 0x7f121170

.field public static final open_tickets_charge:I = 0x7f121171

.field public static final open_tickets_checking:I = 0x7f121172

.field public static final open_tickets_clear_new_items:I = 0x7f121173

.field public static final open_tickets_delete:I = 0x7f121174

.field public static final open_tickets_delete_ticket:I = 0x7f121175

.field public static final open_tickets_delete_ticket_confirm:I = 0x7f121176

.field public static final open_tickets_delete_ticket_warning_keep:I = 0x7f121177

.field public static final open_tickets_delete_ticket_warning_many:I = 0x7f121178

.field public static final open_tickets_delete_ticket_warning_one:I = 0x7f121179

.field public static final open_tickets_delete_tickets:I = 0x7f12117a

.field public static final open_tickets_deleted_many:I = 0x7f12117b

.field public static final open_tickets_deleted_one:I = 0x7f12117c

.field public static final open_tickets_dismiss:I = 0x7f12117d

.field public static final open_tickets_edit_ticket:I = 0x7f12117e

.field public static final open_tickets_error_offline_message:I = 0x7f12117f

.field public static final open_tickets_error_offline_title:I = 0x7f121180

.field public static final open_tickets_error_offline_title_short:I = 0x7f121181

.field public static final open_tickets_error_sync_message_many:I = 0x7f121182

.field public static final open_tickets_error_sync_message_one:I = 0x7f121183

.field public static final open_tickets_error_sync_message_update_register:I = 0x7f121184

.field public static final open_tickets_error_sync_title:I = 0x7f121185

.field public static final open_tickets_error_sync_unfixable_message_many:I = 0x7f121186

.field public static final open_tickets_error_sync_unfixable_message_one:I = 0x7f121187

.field public static final open_tickets_loading:I = 0x7f121188

.field public static final open_tickets_merge:I = 0x7f121189

.field public static final open_tickets_merge_canceled_confirmation:I = 0x7f12118a

.field public static final open_tickets_merge_canceled_message:I = 0x7f12118b

.field public static final open_tickets_merge_canceled_title:I = 0x7f12118c

.field public static final open_tickets_merge_in_progress:I = 0x7f12118d

.field public static final open_tickets_merge_ticket:I = 0x7f12118e

.field public static final open_tickets_merge_tickets_prompt:I = 0x7f12118f

.field public static final open_tickets_merge_tickets_success:I = 0x7f121190

.field public static final open_tickets_merge_with:I = 0x7f121191

.field public static final open_tickets_move:I = 0x7f121192

.field public static final open_tickets_move_canceled_title_many:I = 0x7f121193

.field public static final open_tickets_move_canceled_title_one:I = 0x7f121194

.field public static final open_tickets_move_in_progress:I = 0x7f121195

.field public static final open_tickets_move_ticket:I = 0x7f121196

.field public static final open_tickets_move_ticket_list_many:I = 0x7f121197

.field public static final open_tickets_move_ticket_list_one:I = 0x7f121198

.field public static final open_tickets_move_ticket_list_two:I = 0x7f121199

.field public static final open_tickets_move_tickets_many:I = 0x7f12119a

.field public static final open_tickets_move_tickets_one:I = 0x7f12119b

.field public static final open_tickets_move_tickets_success_many:I = 0x7f12119c

.field public static final open_tickets_move_tickets_success_one:I = 0x7f12119d

.field public static final open_tickets_new_items_multiple:I = 0x7f12119e

.field public static final open_tickets_new_items_one:I = 0x7f12119f

.field public static final open_tickets_no_tickets:I = 0x7f1211a0

.field public static final open_tickets_off:I = 0x7f1211a1

.field public static final open_tickets_on:I = 0x7f1211a2

.field public static final open_tickets_open_tickets:I = 0x7f1211a3

.field public static final open_tickets_other_tickets:I = 0x7f1211a4

.field public static final open_tickets_print_bill:I = 0x7f1211a5

.field public static final open_tickets_reprint_ticket:I = 0x7f1211a6

.field public static final open_tickets_save:I = 0x7f1211a7

.field public static final open_tickets_save_items_to:I = 0x7f1211a8

.field public static final open_tickets_saving:I = 0x7f1211a9

.field public static final open_tickets_search_people:I = 0x7f1211aa

.field public static final open_tickets_search_tickets:I = 0x7f1211ab

.field public static final open_tickets_sort_by_employee:I = 0x7f1211ac

.field public static final open_tickets_sort_by_name:I = 0x7f1211ad

.field public static final open_tickets_sort_by_recent:I = 0x7f1211ae

.field public static final open_tickets_sort_by_total:I = 0x7f1211af

.field public static final open_tickets_split_ticket:I = 0x7f1211b0

.field public static final open_tickets_syncing:I = 0x7f1211b1

.field public static final open_tickets_syncing_message:I = 0x7f1211b2

.field public static final open_tickets_syncing_message_plural:I = 0x7f1211b3

.field public static final open_tickets_ticket_name_hint:I = 0x7f1211b4

.field public static final open_tickets_ticket_name_hint_swipe_allowed:I = 0x7f1211b5

.field public static final open_tickets_ticket_new_ticket:I = 0x7f1211b6

.field public static final open_tickets_ticket_saved:I = 0x7f1211b7

.field public static final open_tickets_tickets:I = 0x7f1211b8

.field public static final open_tickets_title:I = 0x7f1211b9

.field public static final open_tickets_title_with_number:I = 0x7f1211ba

.field public static final open_tickets_toggle_hint:I = 0x7f1211bb

.field public static final open_tickets_toggle_text:I = 0x7f1211bc

.field public static final open_tickets_toggle_text_short:I = 0x7f1211bd

.field public static final open_tickets_too_many_error_dialog_body:I = 0x7f1211be

.field public static final open_tickets_too_many_error_dialog_title:I = 0x7f1211bf

.field public static final open_tickets_total:I = 0x7f1211c0

.field public static final open_tickets_total_amount:I = 0x7f1211c1

.field public static final open_tickets_transfer:I = 0x7f1211c2

.field public static final open_tickets_transfer_ticket:I = 0x7f1211c3

.field public static final open_tickets_transfer_tickets_many:I = 0x7f1211c4

.field public static final open_tickets_transfer_tickets_one:I = 0x7f1211c5

.field public static final open_tickets_transferred_many:I = 0x7f1211c6

.field public static final open_tickets_transferred_one:I = 0x7f1211c7

.field public static final open_tickets_unable_to_read:I = 0x7f1211c8

.field public static final open_tickets_uppercase_sort_employee:I = 0x7f1211c9

.field public static final open_tickets_uppercase_sort_name:I = 0x7f1211ca

.field public static final open_tickets_uppercase_sort_recent:I = 0x7f1211cb

.field public static final open_tickets_uppercase_sort_total:I = 0x7f1211cc

.field public static final open_tickets_url:I = 0x7f1211cd

.field public static final open_tickets_view_all_tickets:I = 0x7f1211ce

.field public static final open_tickets_void_in_progress:I = 0x7f1211cf

.field public static final open_tickets_void_ticket_warning_many:I = 0x7f1211d0

.field public static final open_tickets_void_ticket_warning_one:I = 0x7f1211d1

.field public static final open_tickets_voided_many:I = 0x7f1211d2

.field public static final open_tickets_voided_one:I = 0x7f1211d3

.field public static final open_tickets_your_tickets:I = 0x7f1211d4

.field public static final option_value_number_limit_help_text:I = 0x7f1211d5

.field public static final optional_note:I = 0x7f1211d6

.field public static final order_a_reader:I = 0x7f1211d7

.field public static final order_address_recommended_label:I = 0x7f1211d8

.field public static final order_card_account_owner_row_title:I = 0x7f1211d9

.field public static final order_card_business_info_action_bar:I = 0x7f1211da

.field public static final order_card_business_info_continue_button:I = 0x7f1211db

.field public static final order_card_business_info_help:I = 0x7f1211dc

.field public static final order_card_business_info_help_location:I = 0x7f1211dd

.field public static final order_card_business_info_help_truncation:I = 0x7f1211de

.field public static final order_card_business_info_message:I = 0x7f1211df

.field public static final order_card_business_name_row_title:I = 0x7f1211e0

.field public static final order_card_button_text:I = 0x7f1211e1

.field public static final order_card_customization_error_message:I = 0x7f1211e2

.field public static final order_card_customization_error_title:I = 0x7f1211e3

.field public static final order_card_customization_idv_error_message:I = 0x7f1211e4

.field public static final order_card_customization_idv_error_title:I = 0x7f1211e5

.field public static final order_card_customization_settings_fetch_error_message:I = 0x7f1211e6

.field public static final order_card_customization_settings_fetch_error_title:I = 0x7f1211e7

.field public static final order_card_customization_settings_no_owner_name_error_message:I = 0x7f1211e8

.field public static final order_card_customization_settings_no_owner_name_error_title:I = 0x7f1211e9

.field public static final order_card_customization_too_little_ink_error_message:I = 0x7f1211ea

.field public static final order_card_customization_too_much_ink_error_message:I = 0x7f1211eb

.field public static final order_card_customizing_message:I = 0x7f1211ec

.field public static final order_card_fetching_info_error_message:I = 0x7f1211ed

.field public static final order_card_fetching_info_error_title:I = 0x7f1211ee

.field public static final order_card_generic_error_message:I = 0x7f1211ef

.field public static final order_card_generic_error_title:I = 0x7f1211f0

.field public static final order_card_label:I = 0x7f1211f1

.field public static final order_card_subtitle:I = 0x7f1211f2

.field public static final order_card_terms_of_service_hint:I = 0x7f1211f3

.field public static final order_card_terms_of_service_link:I = 0x7f1211f4

.field public static final order_card_terms_of_service_url:I = 0x7f1211f5

.field public static final order_card_title:I = 0x7f1211f6

.field public static final order_confirmed:I = 0x7f1211f7

.field public static final order_email_confirmation:I = 0x7f1211f8

.field public static final order_history:I = 0x7f1211f9

.field public static final order_history_url:I = 0x7f1211fa

.field public static final order_items_none:I = 0x7f1211fb

.field public static final order_items_one:I = 0x7f1211fc

.field public static final order_items_some:I = 0x7f1211fd

.field public static final order_missing_info_message:I = 0x7f1211fe

.field public static final order_missing_info_title:I = 0x7f1211ff

.field public static final order_not_sent:I = 0x7f121200

.field public static final order_number:I = 0x7f121201

.field public static final order_reader:I = 0x7f121202

.field public static final order_reader_all_hardware:I = 0x7f121203

.field public static final order_reader_contactless:I = 0x7f121204

.field public static final order_reader_magstripe:I = 0x7f121205

.field public static final order_reader_magstripe_validation_system_error_text:I = 0x7f121206

.field public static final order_reader_magstripe_validation_system_error_title:I = 0x7f121207

.field public static final order_reader_order_now:I = 0x7f121208

.field public static final order_status_awaiting_return:I = 0x7f121209

.field public static final order_status_canceled:I = 0x7f12120a

.field public static final order_status_declined:I = 0x7f12120b

.field public static final order_status_declined_and_refunded:I = 0x7f12120c

.field public static final order_status_declined_refund_pending:I = 0x7f12120d

.field public static final order_status_delivered:I = 0x7f12120e

.field public static final order_status_out_for_delivery:I = 0x7f12120f

.field public static final order_status_partially_delivered:I = 0x7f121210

.field public static final order_status_partially_delivered_with_errors:I = 0x7f121211

.field public static final order_status_processing:I = 0x7f121212

.field public static final order_status_returned:I = 0x7f121213

.field public static final order_status_returned_and_refunded:I = 0x7f121214

.field public static final order_status_returned_to_sender:I = 0x7f121215

.field public static final order_status_shipped:I = 0x7f121216

.field public static final order_status_shipped_with_errors:I = 0x7f121217

.field public static final order_status_shipping_error:I = 0x7f121218

.field public static final order_status_unknown:I = 0x7f121219

.field public static final order_validation_modified_text:I = 0x7f12121a

.field public static final order_validation_modified_title:I = 0x7f12121b

.field public static final order_validation_uncorrected_text:I = 0x7f12121c

.field public static final order_validation_uncorrected_title:I = 0x7f12121d

.field public static final orderhub_alert_new_order_message_new_plural:I = 0x7f12121e

.field public static final orderhub_alert_new_order_message_new_singular:I = 0x7f12121f

.field public static final orderhub_alert_new_order_message_upcoming_and_new_singular:I = 0x7f121220

.field public static final orderhub_alert_new_order_message_upcoming_plural:I = 0x7f121221

.field public static final orderhub_alert_new_order_message_upcoming_plural_and_new_plural:I = 0x7f121222

.field public static final orderhub_alert_new_order_message_upcoming_plural_and_new_singular:I = 0x7f121223

.field public static final orderhub_alert_new_order_message_upcoming_singular:I = 0x7f121224

.field public static final orderhub_alert_new_order_message_upcoming_singular_and_new_plural:I = 0x7f121225

.field public static final orderhub_alert_new_order_title:I = 0x7f121226

.field public static final orderhub_alert_new_orders_primary_button:I = 0x7f121227

.field public static final orderhub_alert_new_orders_secondary_button:I = 0x7f121228

.field public static final orderhub_alert_new_orders_title:I = 0x7f121229

.field public static final orderhub_alert_order_update_failed_message:I = 0x7f12122a

.field public static final orderhub_alert_order_update_failed_title:I = 0x7f12122b

.field public static final orderhub_alert_order_update_network_failure_message:I = 0x7f12122c

.field public static final orderhub_alert_order_update_network_failure_title:I = 0x7f12122d

.field public static final orderhub_alert_order_version_mismatch_message:I = 0x7f12122e

.field public static final orderhub_alert_order_version_mismatch_title:I = 0x7f12122f

.field public static final orderhub_alerts_explanation:I = 0x7f121230

.field public static final orderhub_alerts_frequency_1:I = 0x7f121231

.field public static final orderhub_alerts_frequency_2:I = 0x7f121232

.field public static final orderhub_alerts_frequency_3:I = 0x7f121233

.field public static final orderhub_alerts_frequency_4:I = 0x7f121234

.field public static final orderhub_alerts_frequency_header_uppercase:I = 0x7f121235

.field public static final orderhub_alerts_settings_section_label:I = 0x7f121236

.field public static final orderhub_allow_alerts:I = 0x7f121237

.field public static final orderhub_applet_name:I = 0x7f121238

.field public static final orderhub_connection_error:I = 0x7f121239

.field public static final orderhub_connection_error_message:I = 0x7f12123a

.field public static final orderhub_date_format_hour_minute_time:I = 0x7f12123b

.field public static final orderhub_date_format_month_day_and_time:I = 0x7f12123c

.field public static final orderhub_detail_order_missing_recipient:I = 0x7f12123d

.field public static final orderhub_detail_order_no_courier_assigned:I = 0x7f12123e

.field public static final orderhub_detail_order_title_format:I = 0x7f12123f

.field public static final orderhub_detail_orders_not_syncing_message:I = 0x7f121240

.field public static final orderhub_detail_orders_not_syncing_message_last_sync_date:I = 0x7f121241

.field public static final orderhub_detail_orders_not_syncing_title:I = 0x7f121242

.field public static final orderhub_fulfillment_type_name_short_custom:I = 0x7f121243

.field public static final orderhub_fulfillment_type_name_short_delivery:I = 0x7f121244

.field public static final orderhub_fulfillment_type_name_short_digital:I = 0x7f121245

.field public static final orderhub_fulfillment_type_name_short_managed_delivery:I = 0x7f121246

.field public static final orderhub_fulfillment_type_name_short_pickup:I = 0x7f121247

.field public static final orderhub_fulfillment_type_name_short_shipment:I = 0x7f121248

.field public static final orderhub_main_section_name:I = 0x7f121249

.field public static final orderhub_message_no_active_sources_button:I = 0x7f12124a

.field public static final orderhub_message_no_active_sources_subtitle:I = 0x7f12124b

.field public static final orderhub_message_no_active_sources_title:I = 0x7f12124c

.field public static final orderhub_message_no_orders_found_for_search:I = 0x7f12124d

.field public static final orderhub_message_no_orders_subtitle:I = 0x7f12124e

.field public static final orderhub_message_no_orders_title:I = 0x7f12124f

.field public static final orderhub_message_search_error:I = 0x7f121250

.field public static final orderhub_message_search_error_retry_cta:I = 0x7f121251

.field public static final orderhub_message_search_instructions:I = 0x7f121252

.field public static final orderhub_message_transactions_link_text:I = 0x7f121253

.field public static final orderhub_new_orders_notification_content_plural:I = 0x7f121254

.field public static final orderhub_new_orders_notification_content_singular:I = 0x7f121255

.field public static final orderhub_new_orders_notification_title_plural:I = 0x7f121256

.field public static final orderhub_new_orders_notification_title_singular:I = 0x7f121257

.field public static final orderhub_note_action_bar_title:I = 0x7f121258

.field public static final orderhub_note_action_button:I = 0x7f121259

.field public static final orderhub_note_characters_remaining:I = 0x7f12125a

.field public static final orderhub_note_delete_note:I = 0x7f12125b

.field public static final orderhub_order_action_bar_title:I = 0x7f12125c

.field public static final orderhub_order_add_note:I = 0x7f12125d

.field public static final orderhub_order_add_tracking:I = 0x7f12125e

.field public static final orderhub_order_adjust:I = 0x7f12125f

.field public static final orderhub_order_adjust_pickup_time:I = 0x7f121260

.field public static final orderhub_order_cancelation_bill_retrieval_failed_message:I = 0x7f121261

.field public static final orderhub_order_cancelation_bill_retrieval_failed_title:I = 0x7f121262

.field public static final orderhub_order_cancelation_cancel_items_header:I = 0x7f121263

.field public static final orderhub_order_cancelation_inventory_adjustment_failed_message:I = 0x7f121264

.field public static final orderhub_order_cancelation_inventory_adjustment_failed_title:I = 0x7f121265

.field public static final orderhub_order_cancelation_inventory_lost:I = 0x7f121266

.field public static final orderhub_order_cancelation_inventory_restocked:I = 0x7f121267

.field public static final orderhub_order_cancelation_order_already_refunded_message:I = 0x7f121268

.field public static final orderhub_order_cancelation_order_already_refunded_ok:I = 0x7f121269

.field public static final orderhub_order_cancelation_order_already_refunded_title:I = 0x7f12126a

.field public static final orderhub_order_cancelation_reason_customer_request:I = 0x7f12126b

.field public static final orderhub_order_cancelation_reason_items_out_of_stock:I = 0x7f12126c

.field public static final orderhub_order_cancelation_reason_other:I = 0x7f12126d

.field public static final orderhub_order_cancelation_refund_selection_canceling:I = 0x7f12126e

.field public static final orderhub_order_cancelation_refund_selection_continue_to_refund:I = 0x7f12126f

.field public static final orderhub_order_cancelation_refund_selection_item_cancelation_failed:I = 0x7f121270

.field public static final orderhub_order_cancelation_refund_selection_item_cancelation_failed_cancel:I = 0x7f121271

.field public static final orderhub_order_cancelation_refund_selection_item_cancelation_failed_retry:I = 0x7f121272

.field public static final orderhub_order_cancelation_refund_selection_item_cancelation_items_canceled:I = 0x7f121273

.field public static final orderhub_order_cancelation_refund_selection_skip_refund:I = 0x7f121274

.field public static final orderhub_order_cancelation_select_a_reason:I = 0x7f121275

.field public static final orderhub_order_cancelation_select_items_to_cancel:I = 0x7f121276

.field public static final orderhub_order_courier_header:I = 0x7f121277

.field public static final orderhub_order_customer_header:I = 0x7f121278

.field public static final orderhub_order_delivery_deliver_to_header:I = 0x7f121279

.field public static final orderhub_order_delivery_deliver_to_header_for_print:I = 0x7f12127a

.field public static final orderhub_order_delivery_time_header:I = 0x7f12127b

.field public static final orderhub_order_display_state_canceled:I = 0x7f12127c

.field public static final orderhub_order_display_state_completed:I = 0x7f12127d

.field public static final orderhub_order_display_state_failed:I = 0x7f12127e

.field public static final orderhub_order_display_state_in_progress:I = 0x7f12127f

.field public static final orderhub_order_display_state_new:I = 0x7f121280

.field public static final orderhub_order_display_state_ready:I = 0x7f121281

.field public static final orderhub_order_display_state_rejected:I = 0x7f121282

.field public static final orderhub_order_display_state_unknown:I = 0x7f121283

.field public static final orderhub_order_display_state_upcoming:I = 0x7f121284

.field public static final orderhub_order_edit_note:I = 0x7f121285

.field public static final orderhub_order_edit_tracking:I = 0x7f121286

.field public static final orderhub_order_external_source_manage:I = 0x7f121287

.field public static final orderhub_order_external_source_view:I = 0x7f121288

.field public static final orderhub_order_filter_header_source_uppercase:I = 0x7f121289

.field public static final orderhub_order_filter_header_status_uppercase:I = 0x7f12128a

.field public static final orderhub_order_filter_header_type_uppercase:I = 0x7f12128b

.field public static final orderhub_order_filter_value_status_active:I = 0x7f12128c

.field public static final orderhub_order_filter_value_status_active_shortened:I = 0x7f12128d

.field public static final orderhub_order_filter_value_status_completed:I = 0x7f12128e

.field public static final orderhub_order_filter_value_status_upcoming:I = 0x7f12128f

.field public static final orderhub_order_filter_value_type_custom:I = 0x7f121290

.field public static final orderhub_order_filter_value_type_delivery:I = 0x7f121291

.field public static final orderhub_order_filter_value_type_digital:I = 0x7f121292

.field public static final orderhub_order_filter_value_type_managed_delivery:I = 0x7f121293

.field public static final orderhub_order_filter_value_type_pickup:I = 0x7f121294

.field public static final orderhub_order_filter_value_type_shipment:I = 0x7f121295

.field public static final orderhub_order_filter_value_type_unknown:I = 0x7f121296

.field public static final orderhub_order_fulfillment_action_accept:I = 0x7f121297

.field public static final orderhub_order_fulfillment_action_cancel_items:I = 0x7f121298

.field public static final orderhub_order_fulfillment_action_complete:I = 0x7f121299

.field public static final orderhub_order_fulfillment_action_mark_in_progress:I = 0x7f12129a

.field public static final orderhub_order_fulfillment_action_mark_picked_up:I = 0x7f12129b

.field public static final orderhub_order_fulfillment_action_mark_ready:I = 0x7f12129c

.field public static final orderhub_order_fulfillment_action_mark_shipped:I = 0x7f12129d

.field public static final orderhub_order_id_header:I = 0x7f12129e

.field public static final orderhub_order_item_name_custom_amount:I = 0x7f12129f

.field public static final orderhub_order_item_name_unknown:I = 0x7f1212a0

.field public static final orderhub_order_item_quantity:I = 0x7f1212a1

.field public static final orderhub_order_item_selection_next:I = 0x7f1212a2

.field public static final orderhub_order_item_selection_select_all:I = 0x7f1212a3

.field public static final orderhub_order_items_header_canceled_uppercase:I = 0x7f1212a4

.field public static final orderhub_order_items_header_completed_uppercase:I = 0x7f1212a5

.field public static final orderhub_order_items_header_uppercase:I = 0x7f1212a6

.field public static final orderhub_order_pickup_time_header:I = 0x7f1212a7

.field public static final orderhub_order_price_discount:I = 0x7f1212a8

.field public static final orderhub_order_price_subtotal:I = 0x7f1212a9

.field public static final orderhub_order_price_tax:I = 0x7f1212aa

.field public static final orderhub_order_price_tip:I = 0x7f1212ab

.field public static final orderhub_order_price_total:I = 0x7f1212ac

.field public static final orderhub_order_print:I = 0x7f1212ad

.field public static final orderhub_order_relative_date_ago:I = 0x7f1212ae

.field public static final orderhub_order_relative_date_days:I = 0x7f1212af

.field public static final orderhub_order_relative_date_format:I = 0x7f1212b0

.field public static final orderhub_order_relative_date_hours_minutes:I = 0x7f1212b1

.field public static final orderhub_order_relative_date_hours_minutes_ago:I = 0x7f1212b2

.field public static final orderhub_order_relative_date_months:I = 0x7f1212b3

.field public static final orderhub_order_relative_date_one_day:I = 0x7f1212b4

.field public static final orderhub_order_relative_date_one_month:I = 0x7f1212b5

.field public static final orderhub_order_relative_date_one_week:I = 0x7f1212b6

.field public static final orderhub_order_relative_date_one_year:I = 0x7f1212b7

.field public static final orderhub_order_relative_date_time_format:I = 0x7f1212b8

.field public static final orderhub_order_relative_date_today:I = 0x7f1212b9

.field public static final orderhub_order_relative_date_tomorrow:I = 0x7f1212ba

.field public static final orderhub_order_relative_date_weeks:I = 0x7f1212bb

.field public static final orderhub_order_relative_date_years:I = 0x7f1212bc

.field public static final orderhub_order_relative_date_yesterday:I = 0x7f1212bd

.field public static final orderhub_order_reprint:I = 0x7f1212be

.field public static final orderhub_order_shipment_method_header:I = 0x7f1212bf

.field public static final orderhub_order_shipment_select_items_to_ship:I = 0x7f1212c0

.field public static final orderhub_order_shipment_ship_items_header:I = 0x7f1212c1

.field public static final orderhub_order_shipment_to_header:I = 0x7f1212c2

.field public static final orderhub_order_shipment_to_header_for_print:I = 0x7f1212c3

.field public static final orderhub_order_source_id:I = 0x7f1212c4

.field public static final orderhub_order_status_canceled_at:I = 0x7f1212c5

.field public static final orderhub_order_status_canceled_at_with_reason:I = 0x7f1212c6

.field public static final orderhub_order_status_canceled_with_reason:I = 0x7f1212c7

.field public static final orderhub_order_status_completed_at:I = 0x7f1212c8

.field public static final orderhub_order_status_picked_up_at:I = 0x7f1212c9

.field public static final orderhub_order_status_placed_at:I = 0x7f1212ca

.field public static final orderhub_order_status_rejected_at:I = 0x7f1212cb

.field public static final orderhub_order_status_shipped_at:I = 0x7f1212cc

.field public static final orderhub_order_tracking_info:I = 0x7f1212cd

.field public static final orderhub_order_tracking_label:I = 0x7f1212ce

.field public static final orderhub_order_txn_header:I = 0x7f1212cf

.field public static final orderhub_order_view_transaction_detail:I = 0x7f1212d0

.field public static final orderhub_print_delivery:I = 0x7f1212d1

.field public static final orderhub_print_digital:I = 0x7f1212d2

.field public static final orderhub_print_fulfillment_name_with_pickup_at:I = 0x7f1212d3

.field public static final orderhub_print_pickup:I = 0x7f1212d4

.field public static final orderhub_print_shipment:I = 0x7f1212d5

.field public static final orderhub_printing_explanation:I = 0x7f1212d6

.field public static final orderhub_printing_new_orders:I = 0x7f1212d7

.field public static final orderhub_printing_settings_section_label:I = 0x7f1212d8

.field public static final orderhub_quick_actions_awaiting_courier:I = 0x7f1212d9

.field public static final orderhub_quick_actions_explanation:I = 0x7f1212da

.field public static final orderhub_quick_actions_failed_action:I = 0x7f1212db

.field public static final orderhub_quick_actions_retry:I = 0x7f1212dc

.field public static final orderhub_quick_actions_settings_section_label:I = 0x7f1212dd

.field public static final orderhub_quick_actions_use_in_order_manager:I = 0x7f1212de

.field public static final orderhub_quick_actions_view_to_ship:I = 0x7f1212df

.field public static final orderhub_search_hint:I = 0x7f1212e0

.field public static final orderhub_success_cancellation_message:I = 0x7f1212e1

.field public static final orderhub_success_cancellation_title:I = 0x7f1212e2

.field public static final orderhub_tracking_action_bar_title:I = 0x7f1212e3

.field public static final orderhub_tracking_action_button_done:I = 0x7f1212e4

.field public static final orderhub_tracking_action_button_skip_tracking:I = 0x7f1212e5

.field public static final orderhub_tracking_carrier_hint:I = 0x7f1212e6

.field public static final orderhub_tracking_carrier_title:I = 0x7f1212e7

.field public static final orderhub_tracking_hint:I = 0x7f1212e8

.field public static final orderhub_tracking_other:I = 0x7f1212e9

.field public static final orderhub_tracking_remove_tracking_button_text:I = 0x7f1212ea

.field public static final orderhub_tracking_tracking_number:I = 0x7f1212eb

.field public static final orders:I = 0x7f1212ec

.field public static final orders_error_message:I = 0x7f1212ed

.field public static final orders_error_title:I = 0x7f1212ee

.field public static final orders_none_message:I = 0x7f1212ef

.field public static final orders_none_title:I = 0x7f1212f0

.field public static final orders_reload:I = 0x7f1212f1

.field public static final orders_track:I = 0x7f1212f2

.field public static final orders_view_full_history:I = 0x7f1212f3

.field public static final organization:I = 0x7f1212f4

.field public static final other_gift_card:I = 0x7f1212f5

.field public static final other_tender:I = 0x7f1212f6

.field public static final out_of_disk_space_content:I = 0x7f1212f7

.field public static final out_of_disk_space_title:I = 0x7f1212f8

.field public static final outstanding:I = 0x7f1212f9

.field public static final page_label_edit_hint:I = 0x7f1212fa

.field public static final page_label_edit_title:I = 0x7f1212fb

.field public static final page_label_help_text:I = 0x7f1212fc

.field public static final paid_in_out:I = 0x7f1212fd

.field public static final paid_in_out_amount_hint:I = 0x7f1212fe

.field public static final paid_in_out_amount_uppercase:I = 0x7f1212ff

.field public static final paid_in_out_description_hint:I = 0x7f121300

.field public static final paid_in_out_description_uppercase:I = 0x7f121301

.field public static final paid_in_out_employee_full_name:I = 0x7f121302

.field public static final paid_in_out_employee_name:I = 0x7f121303

.field public static final paid_in_out_paid_in_button:I = 0x7f121304

.field public static final paid_in_out_paid_in_button_confirm:I = 0x7f121305

.field public static final paid_in_out_paid_out_button:I = 0x7f121306

.field public static final paid_in_out_paid_out_button_confirm:I = 0x7f121307

.field public static final paid_in_out_total:I = 0x7f121308

.field public static final pairing_confirmation_message:I = 0x7f121309

.field public static final pairing_confirmation_title:I = 0x7f12130a

.field public static final pairing_confirmation_wrong_reader:I = 0x7f12130b

.field public static final pairing_fallback_help:I = 0x7f12130c

.field public static final pairing_fallback_help_url:I = 0x7f12130d

.field public static final pairing_fallback_no_readers_found:I = 0x7f12130e

.field public static final pairing_help:I = 0x7f12130f

.field public static final pairing_help_devices:I = 0x7f121310

.field public static final pairing_help_support:I = 0x7f121311

.field public static final pairing_help_video:I = 0x7f121312

.field public static final pairing_list_row_text:I = 0x7f121313

.field public static final pairing_progress_title:I = 0x7f121314

.field public static final pairing_screen_bluetooth_enable_action:I = 0x7f121315

.field public static final pairing_screen_bluetooth_required_message:I = 0x7f121316

.field public static final pairing_screen_bluetooth_required_title:I = 0x7f121317

.field public static final pairing_screen_help:I = 0x7f121318

.field public static final pairing_screen_help_verbose:I = 0x7f121319

.field public static final pairing_screen_instructions:I = 0x7f12131a

.field public static final pairing_screen_title:I = 0x7f12131b

.field public static final paper_signature_additional_auth_slip:I = 0x7f12131c

.field public static final paper_signature_additional_auth_slip_hint:I = 0x7f12131d

.field public static final paper_signature_always_print_receipt:I = 0x7f12131e

.field public static final paper_signature_always_skip_signature_warning_dialog_content:I = 0x7f12131f

.field public static final paper_signature_always_skip_signature_warning_dialog_title:I = 0x7f121320

.field public static final paper_signature_automatically_print_receipt:I = 0x7f121321

.field public static final paper_signature_card_issuer_agreement:I = 0x7f121322

.field public static final paper_signature_help_url:I = 0x7f121323

.field public static final paper_signature_quick_tip_calculated_amount_and_total_pattern:I = 0x7f121324

.field public static final paper_signature_quick_tip_calculated_total_pattern:I = 0x7f121325

.field public static final paper_signature_quick_tip_hint:I = 0x7f121326

.field public static final paper_signature_quick_tip_no_tip_label:I = 0x7f121327

.field public static final paper_signature_quick_tip_percentage_pattern:I = 0x7f121328

.field public static final paper_signature_quick_tip_setting_hint:I = 0x7f121329

.field public static final paper_signature_quick_tip_setting_label:I = 0x7f12132a

.field public static final paper_signature_quick_tip_tip_label:I = 0x7f12132b

.field public static final paper_signature_quick_tip_title_label:I = 0x7f12132c

.field public static final paper_signature_quick_tip_total_label:I = 0x7f12132d

.field public static final paper_signature_settle_failed_title:I = 0x7f12132e

.field public static final paper_signature_sign_and_tip_on_printed_receipts_without_printer_dialog_content:I = 0x7f12132f

.field public static final paper_signature_sign_on_device:I = 0x7f121330

.field public static final paper_signature_sign_on_device_hint:I = 0x7f121331

.field public static final paper_signature_sign_on_device_hint_au:I = 0x7f121332

.field public static final paper_signature_sign_on_device_short:I = 0x7f121333

.field public static final paper_signature_sign_on_printed_receipt:I = 0x7f121334

.field public static final paper_signature_sign_on_printed_receipt_description:I = 0x7f121335

.field public static final paper_signature_sign_on_printed_receipt_short:I = 0x7f121336

.field public static final paper_signature_sign_on_printed_receipts_warning_dialog_content:I = 0x7f121337

.field public static final paper_signature_sign_on_printed_receipts_warning_dialog_title:I = 0x7f121338

.field public static final paper_signature_sign_on_printed_receipts_without_printer_dialog_content:I = 0x7f121339

.field public static final paper_signature_sign_on_printed_receipts_without_printer_dialog_title:I = 0x7f12133a

.field public static final paper_signature_skip_signature_warning_dialog_content:I = 0x7f12133b

.field public static final paper_signature_skip_signature_warning_dialog_title:I = 0x7f12133c

.field public static final paper_signature_traditional_additional_tip_label:I = 0x7f12133d

.field public static final paper_signature_traditional_receipt:I = 0x7f12133e

.field public static final paper_signature_traditional_tip_label:I = 0x7f12133f

.field public static final paper_signature_traditional_total_label:I = 0x7f121340

.field public static final parenthesis:I = 0x7f121341

.field public static final partial_auth_charge_and_continue:I = 0x7f121342

.field public static final partial_auth_charge_and_continue_with_amount:I = 0x7f121343

.field public static final partial_auth_insufficient_balance:I = 0x7f121344

.field public static final partial_auth_insufficient_funds:I = 0x7f121345

.field public static final partial_auth_message2:I = 0x7f121346

.field public static final partial_auth_message3:I = 0x7f121347

.field public static final partial_payments_amount_overdue:I = 0x7f121348

.field public static final partial_payments_balance:I = 0x7f121349

.field public static final partial_payments_card_payment_status:I = 0x7f12134a

.field public static final partial_payments_card_payment_status_no_last_four:I = 0x7f12134b

.field public static final partial_payments_cash_check_payment_status:I = 0x7f12134c

.field public static final partial_payments_deposit:I = 0x7f12134d

.field public static final partial_payments_due_date:I = 0x7f12134e

.field public static final partial_payments_due_today:I = 0x7f12134f

.field public static final partial_payments_due_upon_receipt:I = 0x7f121350

.field public static final partial_payments_due_within_future_date_plural:I = 0x7f121351

.field public static final partial_payments_due_within_future_date_singular:I = 0x7f121352

.field public static final partial_payments_due_within_past_date_plural:I = 0x7f121353

.field public static final partial_payments_due_within_past_date_singular:I = 0x7f121354

.field public static final partial_payments_installment:I = 0x7f121355

.field public static final partial_payments_other_payment_status:I = 0x7f121356

.field public static final partial_payments_overdue:I = 0x7f121357

.field public static final partial_payments_paid:I = 0x7f121358

.field public static final partial_payments_partially_paid:I = 0x7f121359

.field public static final partial_payments_unknown:I = 0x7f12135a

.field public static final partial_payments_unpaid:I = 0x7f12135b

.field public static final partial_refund_help_text:I = 0x7f12135c

.field public static final partial_refund_help_url:I = 0x7f12135d

.field public static final pass:I = 0x7f12135e

.field public static final passcode_settings_after_each_sale_check:I = 0x7f12135f

.field public static final passcode_settings_after_logout_check:I = 0x7f121360

.field public static final passcode_settings_after_logout_description:I = 0x7f121361

.field public static final passcode_settings_after_timeout:I = 0x7f121362

.field public static final passcode_settings_after_timeout_1_minute:I = 0x7f121363

.field public static final passcode_settings_after_timeout_30_seconds:I = 0x7f121364

.field public static final passcode_settings_after_timeout_5_minutes:I = 0x7f121365

.field public static final passcode_settings_after_timeout_never:I = 0x7f121366

.field public static final passcode_settings_back_out_of_sale_check:I = 0x7f121367

.field public static final passcode_settings_confirm_passcode:I = 0x7f121368

.field public static final passcode_settings_create_owner_passcode_description:I = 0x7f121369

.field public static final passcode_settings_create_owner_passcode_success_description:I = 0x7f12136a

.field public static final passcode_settings_create_owner_passcode_title:I = 0x7f12136b

.field public static final passcode_settings_create_passcode_progress_title:I = 0x7f12136c

.field public static final passcode_settings_create_team_passcode:I = 0x7f12136d

.field public static final passcode_settings_create_team_passcode_description:I = 0x7f12136e

.field public static final passcode_settings_create_team_passcode_dialog_message:I = 0x7f12136f

.field public static final passcode_settings_create_team_passcode_dialog_negative_button_text:I = 0x7f121370

.field public static final passcode_settings_create_team_passcode_dialog_positive_button_text:I = 0x7f121371

.field public static final passcode_settings_create_team_passcode_dialog_title:I = 0x7f121372

.field public static final passcode_settings_create_team_passcode_success_continue_button:I = 0x7f121373

.field public static final passcode_settings_create_team_passcode_success_description:I = 0x7f121374

.field public static final passcode_settings_create_team_passcode_title:I = 0x7f121375

.field public static final passcode_settings_edit_team_passcode_title:I = 0x7f121376

.field public static final passcode_settings_enable_switch:I = 0x7f121377

.field public static final passcode_settings_enable_switch_description:I = 0x7f121378

.field public static final passcode_settings_enter_new_passcode:I = 0x7f121379

.field public static final passcode_settings_enter_passcode:I = 0x7f12137a

.field public static final passcode_settings_error:I = 0x7f12137b

.field public static final passcode_settings_incomplete_passcode:I = 0x7f12137c

.field public static final passcode_settings_not_available_dialog_button_text:I = 0x7f12137d

.field public static final passcode_settings_not_available_dialog_message:I = 0x7f12137e

.field public static final passcode_settings_passcode_in_use:I = 0x7f12137f

.field public static final passcode_settings_passcodes_do_not_match_error:I = 0x7f121380

.field public static final passcode_settings_section_title:I = 0x7f121381

.field public static final passcode_settings_share_team_passcode_dialog_button_text:I = 0x7f121382

.field public static final passcode_settings_share_team_passcode_dialog_message:I = 0x7f121383

.field public static final passcode_settings_share_team_passcode_dialog_title:I = 0x7f121384

.field public static final passcode_settings_team_passcode:I = 0x7f121385

.field public static final passcode_settings_team_passcode_description:I = 0x7f121386

.field public static final passcode_settings_team_passcode_switch:I = 0x7f121387

.field public static final passcode_settings_team_permissions:I = 0x7f121388

.field public static final passcode_settings_team_permissions_edit_link:I = 0x7f121389

.field public static final passcode_settings_team_permissions_enable_link:I = 0x7f12138a

.field public static final passcode_settings_timeout_description:I = 0x7f12138b

.field public static final passcode_settings_timeout_save_button:I = 0x7f12138c

.field public static final passcode_settings_timeout_title:I = 0x7f12138d

.field public static final password_helper_hint:I = 0x7f12138e

.field public static final password_helper_text:I = 0x7f12138f

.field public static final password_hint:I = 0x7f121390

.field public static final password_toggle_content_description:I = 0x7f121391

.field public static final password_too_short:I = 0x7f121392

.field public static final password_too_short_message:I = 0x7f121393

.field public static final path_password_eye:I = 0x7f121394

.field public static final path_password_eye_mask_strike_through:I = 0x7f121395

.field public static final path_password_eye_mask_visible:I = 0x7f121396

.field public static final path_password_strike_through:I = 0x7f121397

.field public static final pay_card_card_on_file_hint:I = 0x7f121398

.field public static final pay_card_card_on_file_hint_with_rate:I = 0x7f121399

.field public static final pay_card_card_on_file_hint_with_rate_higher:I = 0x7f12139a

.field public static final pay_card_card_on_file_url:I = 0x7f12139b

.field public static final pay_card_charge_button:I = 0x7f12139c

.field public static final pay_card_cnp_hint:I = 0x7f12139d

.field public static final pay_card_cnp_hint_with_rate:I = 0x7f12139e

.field public static final pay_card_cnp_hint_with_rate_higher:I = 0x7f12139f

.field public static final pay_card_cnp_url:I = 0x7f1213a0

.field public static final pay_card_on_file_title:I = 0x7f1213a1

.field public static final pay_card_swipe_title:I = 0x7f1213a2

.field public static final pay_card_title:I = 0x7f1213a3

.field public static final pay_cash_custom_button:I = 0x7f1213a4

.field public static final pay_cash_tender_button:I = 0x7f1213a5

.field public static final pay_cash_title:I = 0x7f1213a6

.field public static final pay_gift_card_cnp_hint2:I = 0x7f1213a7

.field public static final pay_gift_card_hint:I = 0x7f1213a8

.field public static final pay_gift_card_hint_no_swipe:I = 0x7f1213a9

.field public static final pay_gift_card_purchase_hint:I = 0x7f1213aa

.field public static final pay_gift_card_title:I = 0x7f1213ab

.field public static final pay_other_tender_button:I = 0x7f1213ac

.field public static final paylink_url:I = 0x7f1213ad

.field public static final paylink_url_staging:I = 0x7f1213ae

.field public static final payment_amount_option_next_payment:I = 0x7f1213af

.field public static final payment_amount_option_other_amount:I = 0x7f1213b0

.field public static final payment_amount_option_remaining_amount:I = 0x7f1213b1

.field public static final payment_caption:I = 0x7f1213b2

.field public static final payment_caption_on_date_time_format:I = 0x7f1213b3

.field public static final payment_devices_connect:I = 0x7f1213b4

.field public static final payment_devices_connect_reader:I = 0x7f1213b5

.field public static final payment_devices_learn_more_check_compatibility:I = 0x7f1213b6

.field public static final payment_devices_learn_more_connect_wirelessly:I = 0x7f1213b7

.field public static final payment_devices_learn_more_contactless_message:I = 0x7f1213b8

.field public static final payment_devices_learn_more_magstripe_message:I = 0x7f1213b9

.field public static final payment_devices_learn_more_order_reader:I = 0x7f1213ba

.field public static final payment_expiring:I = 0x7f1213bb

.field public static final payment_expiring_message:I = 0x7f1213bc

.field public static final payment_failed:I = 0x7f1213bd

.field public static final payment_failed_above_maximum:I = 0x7f1213be

.field public static final payment_failed_below_minimum:I = 0x7f1213bf

.field public static final payment_failed_card_not_charged:I = 0x7f1213c0

.field public static final payment_failed_gift_card_not_charged:I = 0x7f1213c1

.field public static final payment_failed_message:I = 0x7f1213c2

.field public static final payment_methods_prompt_nfc_enabled:I = 0x7f1213c3

.field public static final payment_methods_prompt_nfc_not_enabled:I = 0x7f1213c4

.field public static final payment_methods_prompt_offline_mode:I = 0x7f1213c5

.field public static final payment_methods_prompt_offline_mode_help_text:I = 0x7f1213c6

.field public static final payment_methods_prompt_swipe_or_dip:I = 0x7f1213c7

.field public static final payment_note_item_name_quantity:I = 0x7f1213c8

.field public static final payment_note_item_name_quantity_unit:I = 0x7f1213c9

.field public static final payment_note_tip:I = 0x7f1213ca

.field public static final payment_note_tip_itemized:I = 0x7f1213cb

.field public static final payment_number:I = 0x7f1213cc

.field public static final payment_prompt_all_options:I = 0x7f1213cd

.field public static final payment_prompt_all_options_single_message:I = 0x7f1213ce

.field public static final payment_prompt_charge_text:I = 0x7f1213cf

.field public static final payment_prompt_choose_option:I = 0x7f1213d0

.field public static final payment_prompt_choose_option_x2:I = 0x7f1213d1

.field public static final payment_prompt_insert:I = 0x7f1213d2

.field public static final payment_prompt_insert_and_tap:I = 0x7f1213d3

.field public static final payment_prompt_insert_single_message:I = 0x7f1213d4

.field public static final payment_prompt_no_payment_options:I = 0x7f1213d5

.field public static final payment_prompt_swipe:I = 0x7f1213d6

.field public static final payment_prompt_swipe_and_insert:I = 0x7f1213d7

.field public static final payment_prompt_transaction_type_purchase:I = 0x7f1213d8

.field public static final payment_recorded:I = 0x7f1213d9

.field public static final payment_recorded_failed:I = 0x7f1213da

.field public static final payment_recorded_tender_type_on_date:I = 0x7f1213db

.field public static final payment_request_balance_due_date:I = 0x7f1213dc

.field public static final payment_request_deposit_due_date:I = 0x7f1213dd

.field public static final payment_request_due_in:I = 0x7f1213de

.field public static final payment_request_due_in_no_due_text:I = 0x7f1213df

.field public static final payment_request_due_over:I = 0x7f1213e0

.field public static final payment_request_due_upon_receipt:I = 0x7f1213e1

.field public static final payment_request_due_upon_receipt_no_due_text:I = 0x7f1213e2

.field public static final payment_request_in_ninety_days:I = 0x7f1213e3

.field public static final payment_request_in_one_hundred_twenty_days:I = 0x7f1213e4

.field public static final payment_request_in_sixty_days:I = 0x7f1213e5

.field public static final payment_request_in_thirty_days:I = 0x7f1213e6

.field public static final payment_request_number:I = 0x7f1213e7

.field public static final payment_request_payment_due_date:I = 0x7f1213e8

.field public static final payment_request_summary:I = 0x7f1213e9

.field public static final payment_schedule:I = 0x7f1213ea

.field public static final payment_schedule_all_caps:I = 0x7f1213eb

.field public static final payment_schedule_balance_section_money:I = 0x7f1213ec

.field public static final payment_schedule_balance_section_percentage:I = 0x7f1213ed

.field public static final payment_schedule_deposit_section:I = 0x7f1213ee

.field public static final payment_schedule_null_state:I = 0x7f1213ef

.field public static final payment_schedule_validation_balance_high:I = 0x7f1213f0

.field public static final payment_schedule_validation_balance_high_money:I = 0x7f1213f1

.field public static final payment_schedule_validation_balance_low:I = 0x7f1213f2

.field public static final payment_schedule_validation_balance_low_body:I = 0x7f1213f3

.field public static final payment_schedule_validation_balance_one_hundred_percent:I = 0x7f1213f4

.field public static final payment_schedule_validation_conflicting_due_date:I = 0x7f1213f5

.field public static final payment_schedule_validation_conflicting_due_dates_body:I = 0x7f1213f6

.field public static final payment_schedule_validation_deposit_after_balance:I = 0x7f1213f7

.field public static final payment_schedule_validation_invalid_amount:I = 0x7f1213f8

.field public static final payment_schedule_validation_invalid_amount_body:I = 0x7f1213f9

.field public static final payment_type:I = 0x7f1213fa

.field public static final payment_type_above_maximum:I = 0x7f1213fb

.field public static final payment_type_above_maximum_card:I = 0x7f1213fc

.field public static final payment_type_above_maximum_card_offline:I = 0x7f1213fd

.field public static final payment_type_above_maximum_invoice:I = 0x7f1213fe

.field public static final payment_type_below_minimum:I = 0x7f1213ff

.field public static final payment_type_below_minimum_card:I = 0x7f121400

.field public static final payment_type_below_minimum_gift_card:I = 0x7f121401

.field public static final payment_type_below_minimum_invoice:I = 0x7f121402

.field public static final payment_type_contactless_row_phone:I = 0x7f121403

.field public static final payment_type_need_to_charge_card:I = 0x7f121404

.field public static final payment_type_need_to_charge_card_subtitle:I = 0x7f121405

.field public static final payment_type_other:I = 0x7f121406

.field public static final payment_type_other_tablet:I = 0x7f121407

.field public static final payment_type_other_uppercase:I = 0x7f121408

.field public static final payment_type_share_payment_link:I = 0x7f121409

.field public static final payment_type_zero_amount:I = 0x7f12140a

.field public static final payment_type_zero_amount_uppercase:I = 0x7f12140b

.field public static final payment_types_active:I = 0x7f12140c

.field public static final payment_types_settings_empty:I = 0x7f12140d

.field public static final payment_types_settings_empty_disabled:I = 0x7f12140e

.field public static final payment_types_settings_instructions:I = 0x7f12140f

.field public static final payment_types_settings_label_card:I = 0x7f121410

.field public static final payment_types_settings_label_card_on_file:I = 0x7f121411

.field public static final payment_types_settings_label_cash:I = 0x7f121412

.field public static final payment_types_settings_label_emoney:I = 0x7f121413

.field public static final payment_types_settings_label_gift_card:I = 0x7f121414

.field public static final payment_types_settings_label_installments:I = 0x7f121415

.field public static final payment_types_settings_label_invoice:I = 0x7f121416

.field public static final payment_types_settings_label_online_checkout:I = 0x7f121417

.field public static final payment_types_settings_label_record_card_payment:I = 0x7f121418

.field public static final payment_types_settings_minimum_1_payment_type_required:I = 0x7f121419

.field public static final payment_types_settings_preview:I = 0x7f12141a

.field public static final payment_types_skip_itemized_cart_hint:I = 0x7f12141b

.field public static final payment_types_skip_itemzied_cart:I = 0x7f12141c

.field public static final payment_types_skip_payment_type_selection:I = 0x7f12141d

.field public static final payment_types_skip_payment_type_selection_hint:I = 0x7f12141e

.field public static final payment_types_title:I = 0x7f12141f

.field public static final payments:I = 0x7f121420

.field public static final payroll_learn_more:I = 0x7f121421

.field public static final payroll_upsell_description:I = 0x7f121422

.field public static final payroll_upsell_headline:I = 0x7f121423

.field public static final payroll_upsell_link:I = 0x7f121424

.field public static final payroll_upsell_title:I = 0x7f121425

.field public static final payroll_url:I = 0x7f121426

.field public static final pdf_attachment:I = 0x7f121427

.field public static final pending:I = 0x7f121428

.field public static final pending_bank_account_uppercase:I = 0x7f121429

.field public static final pending_payments_notification_content_plural:I = 0x7f12142a

.field public static final pending_payments_notification_content_singular:I = 0x7f12142b

.field public static final pending_payments_notification_title_plural:I = 0x7f12142c

.field public static final pending_payments_notification_title_singular:I = 0x7f12142d

.field public static final percent:I = 0x7f12142e

.field public static final percent_character_pattern:I = 0x7f12142f

.field public static final percent_character_postfix:I = 0x7f121430

.field public static final percentage_of_invoice_balance:I = 0x7f121431

.field public static final percentage_of_invoice_total:I = 0x7f121432

.field public static final peripheral_configured_many:I = 0x7f121433

.field public static final peripheral_configured_one:I = 0x7f121434

.field public static final peripheral_connected_many:I = 0x7f121435

.field public static final peripheral_connected_one:I = 0x7f121436

.field public static final personal_info_heading:I = 0x7f121437

.field public static final personal_info_subheading:I = 0x7f121438

.field public static final phone_number_content_description:I = 0x7f121439

.field public static final photo_canceled:I = 0x7f12143a

.field public static final photo_error:I = 0x7f12143b

.field public static final play_store_intent_uri:I = 0x7f12143c

.field public static final play_store_link:I = 0x7f12143d

.field public static final play_store_package_name_for_review:I = 0x7f12143e

.field public static final please_check_your_network_connection:I = 0x7f12143f

.field public static final please_check_your_network_connection_try_again:I = 0x7f121440

.field public static final please_contact_support:I = 0x7f121441

.field public static final please_remove_card:I = 0x7f121442

.field public static final please_remove_card_title:I = 0x7f121443

.field public static final please_restore_internet_connectivity:I = 0x7f121444

.field public static final please_try_again:I = 0x7f121445

.field public static final please_try_again_or_contact_support:I = 0x7f121446

.field public static final please_visit_url:I = 0x7f121447

.field public static final please_wait:I = 0x7f121448

.field public static final plural_days:I = 0x7f121449

.field public static final plural_months:I = 0x7f12144a

.field public static final plural_weeks:I = 0x7f12144b

.field public static final plural_years:I = 0x7f12144c

.field public static final pm:I = 0x7f12144d

.field public static final points_empty_state_title:I = 0x7f12144e

.field public static final points_empty_state_unenrolled:I = 0x7f12144f

.field public static final points_empty_state_unsubscribed:I = 0x7f121450

.field public static final points_redeem_button:I = 0x7f121451

.field public static final points_redeem_button_applied:I = 0x7f121452

.field public static final points_redeem_button_disabled:I = 0x7f121453

.field public static final points_redemption_error_generic:I = 0x7f121454

.field public static final points_total_amount:I = 0x7f121455

.field public static final popup_actions_view_dismiss:I = 0x7f121456

.field public static final postal_country_prompt:I = 0x7f121457

.field public static final postal_hint:I = 0x7f121458

.field public static final postal_keyboard_switch:I = 0x7f121459

.field public static final postal_other:I = 0x7f12145a

.field public static final postal_pick:I = 0x7f12145b

.field public static final postcode_hint:I = 0x7f12145c

.field public static final pre_charge_card_inserted:I = 0x7f12145d

.field public static final pre_charge_card_must_be_inserted:I = 0x7f12145e

.field public static final pre_charge_card_swiped:I = 0x7f12145f

.field public static final precision_display_value_0:I = 0x7f121460

.field public static final precision_display_value_1:I = 0x7f121461

.field public static final precision_display_value_2:I = 0x7f121462

.field public static final precision_display_value_3:I = 0x7f121463

.field public static final precision_display_value_4:I = 0x7f121464

.field public static final precision_display_value_5:I = 0x7f121465

.field public static final precision_label:I = 0x7f121466

.field public static final predefined_ticket_group_many_tickets:I = 0x7f121467

.field public static final predefined_ticket_group_one_ticket:I = 0x7f121468

.field public static final predefined_tickets_all_tickets:I = 0x7f121469

.field public static final predefined_tickets_automatic_ticket_names:I = 0x7f12146a

.field public static final predefined_tickets_change_ticket_naming:I = 0x7f12146b

.field public static final predefined_tickets_change_ticket_naming_message:I = 0x7f12146c

.field public static final predefined_tickets_confirm_delete_ticket_group:I = 0x7f12146d

.field public static final predefined_tickets_convert_to_custom_ticket:I = 0x7f12146e

.field public static final predefined_tickets_count:I = 0x7f12146f

.field public static final predefined_tickets_custom:I = 0x7f121470

.field public static final predefined_tickets_custom_ticket:I = 0x7f121471

.field public static final predefined_tickets_custom_ticket_names:I = 0x7f121472

.field public static final predefined_tickets_custom_ticket_names_hint:I = 0x7f121473

.field public static final predefined_tickets_delete_ticket_group:I = 0x7f121474

.field public static final predefined_tickets_disabled_dialog_confirmation:I = 0x7f121475

.field public static final predefined_tickets_disabled_dialog_message:I = 0x7f121476

.field public static final predefined_tickets_disabled_dialog_title:I = 0x7f121477

.field public static final predefined_tickets_edit_custom_ticket:I = 0x7f121478

.field public static final predefined_tickets_group:I = 0x7f121479

.field public static final predefined_tickets_log_out:I = 0x7f12147a

.field public static final predefined_tickets_new_group_ticket:I = 0x7f12147b

.field public static final predefined_tickets_no_group_tickets_available_title:I = 0x7f12147c

.field public static final predefined_tickets_no_group_tickets_message:I = 0x7f12147d

.field public static final predefined_tickets_no_group_tickets_title:I = 0x7f12147e

.field public static final predefined_tickets_no_predefined_tickets_message:I = 0x7f12147f

.field public static final predefined_tickets_no_predefined_tickets_title:I = 0x7f121480

.field public static final predefined_tickets_no_tickets_message:I = 0x7f121481

.field public static final predefined_tickets_no_tickets_title:I = 0x7f121482

.field public static final predefined_tickets_none:I = 0x7f121483

.field public static final predefined_tickets_opt_in_button_text:I = 0x7f121484

.field public static final predefined_tickets_opt_in_hint:I = 0x7f121485

.field public static final predefined_tickets_opt_in_message:I = 0x7f121486

.field public static final predefined_tickets_opt_in_title:I = 0x7f121487

.field public static final predefined_tickets_preview:I = 0x7f121488

.field public static final predefined_tickets_search_all_tickets:I = 0x7f121489

.field public static final predefined_tickets_select_ticket_group:I = 0x7f12148a

.field public static final predefined_tickets_show_in_ticket_group:I = 0x7f12148b

.field public static final predefined_tickets_template_name_hint:I = 0x7f12148c

.field public static final predefined_tickets_ticket_group_limit_reached:I = 0x7f12148d

.field public static final predefined_tickets_ticket_group_limit_reached_message:I = 0x7f12148e

.field public static final predefined_tickets_ticket_group_name_hint:I = 0x7f12148f

.field public static final predefined_tickets_ticket_template_limit_message:I = 0x7f121490

.field public static final predefined_tickets_ticket_template_limit_reached:I = 0x7f121491

.field public static final predefined_tickets_ticket_template_limit_reached_message:I = 0x7f121492

.field public static final predefined_tickets_tickets:I = 0x7f121493

.field public static final predefined_tickets_toggle_hint:I = 0x7f121494

.field public static final predefined_tickets_toggle_text:I = 0x7f121495

.field public static final predefined_tickets_toggle_text_short:I = 0x7f121496

.field public static final preview_and_send:I = 0x7f121497

.field public static final price:I = 0x7f121498

.field public static final price_change_message:I = 0x7f121499

.field public static final price_change_title:I = 0x7f12149a

.field public static final pricing_rule_description_long:I = 0x7f12149b

.field public static final pricing_rule_description_medium_with_items:I = 0x7f12149c

.field public static final pricing_rule_description_medium_without_items:I = 0x7f12149d

.field public static final primary_setup:I = 0x7f12149e

.field public static final print_a_ticket_for_each_item_label:I = 0x7f12149f

.field public static final print_allow_printing:I = 0x7f1214a0

.field public static final print_allow_printing_hint:I = 0x7f1214a1

.field public static final print_compact_tickets_label:I = 0x7f1214a2

.field public static final print_gift_receipt:I = 0x7f1214a3

.field public static final print_job_title:I = 0x7f1214a4

.field public static final print_order_ticket_stubs_value:I = 0x7f1214a5

.field public static final print_order_tickets_autonumber_label:I = 0x7f1214a6

.field public static final print_order_tickets_confirm_naming_change:I = 0x7f1214a7

.field public static final print_order_tickets_custom_name_label:I = 0x7f1214a8

.field public static final print_order_tickets_help_message:I = 0x7f1214a9

.field public static final print_order_tickets_no_categories_message:I = 0x7f1214aa

.field public static final print_order_tickets_value:I = 0x7f1214ab

.field public static final print_receipts:I = 0x7f1214ac

.field public static final print_receipts_value:I = 0x7f1214ad

.field public static final print_receipts_value_open_tickets:I = 0x7f1214ae

.field public static final print_receipts_value_reports:I = 0x7f1214af

.field public static final print_receipts_value_reports_open_tickets:I = 0x7f1214b0

.field public static final print_report_header:I = 0x7f1214b1

.field public static final print_report_include_items:I = 0x7f1214b2

.field public static final print_report_print_label:I = 0x7f1214b3

.field public static final print_report_report_printed_title:I = 0x7f1214b4

.field public static final print_uncategorized_items:I = 0x7f1214b5

.field public static final printed_ticket_void:I = 0x7f1214b6

.field public static final printer_connected:I = 0x7f1214b7

.field public static final printer_connected_by_name:I = 0x7f1214b8

.field public static final printer_disconnected:I = 0x7f1214b9

.field public static final printer_disconnected_by_name:I = 0x7f1214ba

.field public static final printer_nickname:I = 0x7f1214bb

.field public static final printer_settings_label:I = 0x7f1214bc

.field public static final printer_station_create_printer_station:I = 0x7f1214bd

.field public static final printer_station_edit_printer_station:I = 0x7f1214be

.field public static final printer_station_no_options_selected:I = 0x7f1214bf

.field public static final printer_station_no_printer_selected:I = 0x7f1214c0

.field public static final printer_stations_cannot_sign_on_printed_receipt_content:I = 0x7f1214c1

.field public static final printer_stations_cannot_sign_on_printed_receipt_title:I = 0x7f1214c2

.field public static final printer_stations_change_settings:I = 0x7f1214c3

.field public static final printer_stations_confirm_change_settings:I = 0x7f1214c4

.field public static final printer_stations_disabled_uppercase:I = 0x7f1214c5

.field public static final printer_stations_edit_printer_stations:I = 0x7f1214c6

.field public static final printer_stations_enabled_uppercase:I = 0x7f1214c7

.field public static final printer_stations_list_title_phrase:I = 0x7f1214c8

.field public static final printer_stations_name_hint:I = 0x7f1214c9

.field public static final printer_stations_name_required_error_message:I = 0x7f1214ca

.field public static final printer_stations_new_station_hint:I = 0x7f1214cb

.field public static final printer_stations_no_hardware_printer:I = 0x7f1214cc

.field public static final printer_stations_no_hardware_printer_selected:I = 0x7f1214cd

.field public static final printer_stations_no_hardware_printers_available_text:I = 0x7f1214ce

.field public static final printer_stations_no_hardware_printers_available_title:I = 0x7f1214cf

.field public static final printer_stations_remove_button_confirmation:I = 0x7f1214d0

.field public static final printer_stations_remove_button_text:I = 0x7f1214d1

.field public static final printer_stations_role_not_supported:I = 0x7f1214d2

.field public static final printer_stations_role_receipt:I = 0x7f1214d3

.field public static final printer_stations_role_receipts:I = 0x7f1214d4

.field public static final printer_stations_role_stub:I = 0x7f1214d5

.field public static final printer_stations_role_stubs:I = 0x7f1214d6

.field public static final printer_stations_role_ticket:I = 0x7f1214d7

.field public static final printer_stations_role_tickets:I = 0x7f1214d8

.field public static final printer_stations_select_hardware_printer:I = 0x7f1214d9

.field public static final printer_stations_select_hardware_printer_title:I = 0x7f1214da

.field public static final printer_stations_set_up_printers_url:I = 0x7f1214db

.field public static final printer_stations_test_print_job_name:I = 0x7f1214dc

.field public static final printer_stations_test_print_ticket_name:I = 0x7f1214dd

.field public static final printer_test_print:I = 0x7f1214de

.field public static final printer_test_print_item_example:I = 0x7f1214df

.field public static final printer_test_print_printer_disconnected:I = 0x7f1214e0

.field public static final printer_test_print_uppercase_dining_option_for_here:I = 0x7f1214e1

.field public static final printer_title:I = 0x7f1214e2

.field public static final printer_troubleshooting:I = 0x7f1214e3

.field public static final printer_uppercase_from_this_device:I = 0x7f1214e4

.field public static final printer_uppercase_include_on_tickets:I = 0x7f1214e5

.field public static final printer_uppercase_order_tickets:I = 0x7f1214e6

.field public static final printing_hud:I = 0x7f1214e7

.field public static final printout_type_auth_slip:I = 0x7f1214e8

.field public static final printout_type_auth_slip_copy:I = 0x7f1214e9

.field public static final printout_type_order_ticket:I = 0x7f1214ea

.field public static final printout_type_order_ticket_stub:I = 0x7f1214eb

.field public static final printout_type_receipt:I = 0x7f1214ec

.field public static final printout_type_void_ticket:I = 0x7f1214ed

.field public static final privacy_policy:I = 0x7f1214ee

.field public static final problem_with_card:I = 0x7f1214ef

.field public static final processing_payments_expiring:I = 0x7f1214f0

.field public static final processing_payments_expiring_by_name:I = 0x7f1214f1

.field public static final processing_payments_expiring_many:I = 0x7f1214f2

.field public static final processing_payments_expiring_one:I = 0x7f1214f3

.field public static final processing_payments_expiring_text:I = 0x7f1214f4

.field public static final processing_payments_many:I = 0x7f1214f5

.field public static final processing_payments_one:I = 0x7f1214f6

.field public static final processing_payments_text:I = 0x7f1214f7

.field public static final processing_time:I = 0x7f1214f8

.field public static final prod_base_url:I = 0x7f1214f9

.field public static final province_hint:I = 0x7f1214fa

.field public static final quantity_entry_manual_with_connected_scale:I = 0x7f1214fb

.field public static final quantity_footer_adjust_weight_on_scale:I = 0x7f1214fc

.field public static final quantity_footer_error:I = 0x7f1214fd

.field public static final quantity_footer_incompatible_unit:I = 0x7f1214fe

.field public static final quantity_footer_manual_weight_entry:I = 0x7f1214ff

.field public static final quantity_footer_no_weight_reading:I = 0x7f121500

.field public static final quick_amounts_manual_tutorial_button_next:I = 0x7f121501

.field public static final quick_amounts_manual_tutorial_message1:I = 0x7f121502

.field public static final quick_amounts_manual_tutorial_message2:I = 0x7f121503

.field public static final quick_amounts_settings_add:I = 0x7f121504

.field public static final quick_amounts_settings_clear_value_accessibility:I = 0x7f121505

.field public static final quick_amounts_settings_description:I = 0x7f121506

.field public static final quick_amounts_settings_items_prompt_create:I = 0x7f121507

.field public static final quick_amounts_settings_items_prompt_description:I = 0x7f121508

.field public static final quick_amounts_settings_items_prompt_no_thanks:I = 0x7f121509

.field public static final quick_amounts_settings_items_prompt_title:I = 0x7f12150a

.field public static final quick_amounts_settings_preview_heading:I = 0x7f12150b

.field public static final quick_amounts_settings_radio_auto:I = 0x7f12150c

.field public static final quick_amounts_settings_radio_off:I = 0x7f12150d

.field public static final quick_amounts_settings_radio_set:I = 0x7f12150e

.field public static final quick_amounts_settings_status_auto:I = 0x7f12150f

.field public static final quick_amounts_settings_status_off:I = 0x7f121510

.field public static final quick_amounts_settings_status_set:I = 0x7f121511

.field public static final quick_amounts_settings_status_use_quick_amounts:I = 0x7f121512

.field public static final quick_amounts_settings_switch:I = 0x7f121513

.field public static final quick_amounts_settings_title:I = 0x7f121514

.field public static final quick_amounts_status_auto:I = 0x7f121515

.field public static final quick_amounts_status_off:I = 0x7f121516

.field public static final quick_amounts_status_set:I = 0x7f121517

.field public static final quick_amounts_tutorial_button_accept:I = 0x7f121518

.field public static final quick_amounts_tutorial_button_deny:I = 0x7f121519

.field public static final quick_amounts_tutorial_message:I = 0x7f12151a

.field public static final quick_amounts_tutorial_update_button:I = 0x7f12151b

.field public static final quick_amounts_tutorial_update_message:I = 0x7f12151c

.field public static final quickamounts_walkthrough:I = 0x7f12151d

.field public static final quickamounts_walkthrough_subtext:I = 0x7f12151e

.field public static final r12_education_charge_message:I = 0x7f12151f

.field public static final r12_education_charge_title:I = 0x7f121520

.field public static final r12_education_dip_message:I = 0x7f121521

.field public static final r12_education_dip_title:I = 0x7f121522

.field public static final r12_education_start_button:I = 0x7f121523

.field public static final r12_education_start_message:I = 0x7f121524

.field public static final r12_education_start_title:I = 0x7f121525

.field public static final r12_education_swipe_message:I = 0x7f121526

.field public static final r12_education_swipe_message_au:I = 0x7f121527

.field public static final r12_education_swipe_title:I = 0x7f121528

.field public static final r12_education_swipe_title_au:I = 0x7f121529

.field public static final r12_education_tap_message:I = 0x7f12152a

.field public static final r12_education_tap_title:I = 0x7f12152b

.field public static final r12_education_video_message:I = 0x7f12152c

.field public static final r12_education_video_title:I = 0x7f12152d

.field public static final r12_education_welcome_button:I = 0x7f12152e

.field public static final r12_education_welcome_message_standard:I = 0x7f12152f

.field public static final r12_education_welcome_message_updating:I = 0x7f121530

.field public static final r12_education_welcome_title_standard:I = 0x7f121531

.field public static final r12_education_welcome_title_updating:I = 0x7f121532

.field public static final r12_video_option_connect:I = 0x7f121533

.field public static final r12_video_option_message:I = 0x7f121534

.field public static final r12_video_option_title:I = 0x7f121535

.field public static final r12_video_option_watch:I = 0x7f121536

.field public static final r6_first_time_video_subtitle:I = 0x7f121537

.field public static final r6_first_time_video_title:I = 0x7f121538

.field public static final rate_on_play_store_prompt:I = 0x7f121539

.field public static final rate_on_the_play_store:I = 0x7f12153a

.field public static final rate_tour_cnp_heading:I = 0x7f12153b

.field public static final rate_tour_cnp_heading_credit:I = 0x7f12153c

.field public static final rate_tour_cnp_heading_type:I = 0x7f12153d

.field public static final rate_tour_cnp_message:I = 0x7f12153e

.field public static final rate_tour_cnp_message_higher:I = 0x7f12153f

.field public static final rate_tour_cnp_message_rate:I = 0x7f121540

.field public static final rate_tour_cnp_subtitle:I = 0x7f121541

.field public static final rate_tour_cnp_subtitle_credit:I = 0x7f121542

.field public static final rate_tour_cnp_subtitle_type:I = 0x7f121543

.field public static final rate_tour_cp_heading:I = 0x7f121544

.field public static final rate_tour_cp_heading_credit:I = 0x7f121545

.field public static final rate_tour_cp_message:I = 0x7f121546

.field public static final rate_tour_cp_subtitle:I = 0x7f121547

.field public static final rate_tour_cp_subtitle_credit:I = 0x7f121548

.field public static final rate_tour_dip_jcb_heading:I = 0x7f121549

.field public static final rate_tour_dip_jcb_message:I = 0x7f12154a

.field public static final rate_tour_dipswipe_heading:I = 0x7f12154b

.field public static final rate_tour_dipswipe_message:I = 0x7f12154c

.field public static final rate_tour_dipswipe_message_brands:I = 0x7f12154d

.field public static final rate_tour_dipswipe_subtitle:I = 0x7f12154e

.field public static final rate_tour_diptap_heading:I = 0x7f12154f

.field public static final rate_tour_diptap_heading_credit:I = 0x7f121550

.field public static final rate_tour_diptap_heading_insert:I = 0x7f121551

.field public static final rate_tour_diptap_message:I = 0x7f121552

.field public static final rate_tour_diptap_message_all:I = 0x7f121553

.field public static final rate_tour_diptap_message_credit:I = 0x7f121554

.field public static final rate_tour_diptap_subtitle:I = 0x7f121555

.field public static final rate_tour_diptap_subtitle_credit:I = 0x7f121556

.field public static final rate_tour_diptap_subtitle_insert:I = 0x7f121557

.field public static final rate_tour_help_button_description:I = 0x7f121558

.field public static final rate_tour_help_url:I = 0x7f121559

.field public static final rate_tour_interac_heading:I = 0x7f12155a

.field public static final rate_tour_interac_heading_with_limit:I = 0x7f12155b

.field public static final rate_tour_interac_message:I = 0x7f12155c

.field public static final rate_tour_interac_message_with_limit:I = 0x7f12155d

.field public static final rate_tour_interac_subtitle:I = 0x7f12155e

.field public static final rate_tour_interac_subtitle_with_limit:I = 0x7f12155f

.field public static final rate_tour_tapdip_heading:I = 0x7f121560

.field public static final rate_tour_tapdip_message:I = 0x7f121561

.field public static final rate_tour_tapdip_subtitle:I = 0x7f121562

.field public static final re_enter_address_button_label:I = 0x7f121563

.field public static final read_notification_dot_content_description:I = 0x7f121564

.field public static final read_only_storage_content:I = 0x7f121565

.field public static final read_only_storage_title:I = 0x7f121566

.field public static final read_the_direct_debit_guarantee:I = 0x7f121567

.field public static final reader_battery_very_low:I = 0x7f121568

.field public static final reader_detail_accepts_legacy:I = 0x7f121569

.field public static final reader_detail_accepts_name:I = 0x7f12156a

.field public static final reader_detail_accepts_r12_value:I = 0x7f12156b

.field public static final reader_detail_accepts_r6_value:I = 0x7f12156c

.field public static final reader_detail_battery_name:I = 0x7f12156d

.field public static final reader_detail_battery_percentage:I = 0x7f12156e

.field public static final reader_detail_connection_bluetooth:I = 0x7f12156f

.field public static final reader_detail_connection_headset:I = 0x7f121570

.field public static final reader_detail_connection_name:I = 0x7f121571

.field public static final reader_detail_firmware_name:I = 0x7f121572

.field public static final reader_detail_forget_reader:I = 0x7f121573

.field public static final reader_detail_identify_reader:I = 0x7f121574

.field public static final reader_detail_nickname_message:I = 0x7f121575

.field public static final reader_detail_serial_number_name:I = 0x7f121576

.field public static final reader_detail_serial_number_name_short:I = 0x7f121577

.field public static final reader_detail_status_connecting_value:I = 0x7f121578

.field public static final reader_detail_status_failed_value:I = 0x7f121579

.field public static final reader_detail_status_name:I = 0x7f12157a

.field public static final reader_detail_status_permission_needed:I = 0x7f12157b

.field public static final reader_detail_status_ready_value:I = 0x7f12157c

.field public static final reader_detail_status_unavailable_value:I = 0x7f12157d

.field public static final reader_detail_status_updating_value:I = 0x7f12157e

.field public static final reader_failed_after_rebooting_fwup_message:I = 0x7f12157f

.field public static final reader_failed_to_connect:I = 0x7f121580

.field public static final reader_message_warning_declined_card:I = 0x7f121581

.field public static final reader_message_warning_declined_card_contactless:I = 0x7f121582

.field public static final reader_message_warning_reinsertion:I = 0x7f121583

.field public static final reader_sdk_invalid_amount_too_high:I = 0x7f121584

.field public static final reader_sdk_invalid_amount_too_low:I = 0x7f121585

.field public static final reader_sdk_swipe_card:I = 0x7f121586

.field public static final reader_sdk_swipe_dip_or_tap_card:I = 0x7f121587

.field public static final reader_sdk_swipe_or_dip_card:I = 0x7f121588

.field public static final reader_status_battery_advice:I = 0x7f121589

.field public static final reader_status_battery_dead_headline:I = 0x7f12158a

.field public static final reader_status_battery_low_headline:I = 0x7f12158b

.field public static final reader_status_battery_percent_confirmation:I = 0x7f12158c

.field public static final reader_status_battery_percent_r12:I = 0x7f12158d

.field public static final reader_status_connecting_advice:I = 0x7f12158e

.field public static final reader_status_connecting_advice_r6_taking_forever:I = 0x7f12158f

.field public static final reader_status_connecting_headline:I = 0x7f121590

.field public static final reader_status_fwup_advice:I = 0x7f121591

.field public static final reader_status_fwup_checking_for_updates_advice:I = 0x7f121592

.field public static final reader_status_fwup_checking_for_updates_headline:I = 0x7f121593

.field public static final reader_status_fwup_failed_headline:I = 0x7f121594

.field public static final reader_status_fwup_inprogress_headline:I = 0x7f121595

.field public static final reader_status_fwup_rebooting_headline:I = 0x7f121596

.field public static final reader_status_offline_r12_advice:I = 0x7f121597

.field public static final reader_status_offline_r12_headline:I = 0x7f121598

.field public static final reader_status_offline_r6_advice:I = 0x7f121599

.field public static final reader_status_offline_r6_headline:I = 0x7f12159a

.field public static final reader_status_ss_connecting_advice:I = 0x7f12159b

.field public static final reader_status_ss_connecting_headline:I = 0x7f12159c

.field public static final reader_status_tamper_advice:I = 0x7f12159d

.field public static final reader_status_tamper_headline:I = 0x7f12159e

.field public static final reader_status_unavailable_advice:I = 0x7f12159f

.field public static final reader_status_unavailable_headline:I = 0x7f1215a0

.field public static final reader_status_wired_connect_failed_advice:I = 0x7f1215a1

.field public static final reader_status_wireless_connect_failed_advice:I = 0x7f1215a2

.field public static final reader_timed_out_waiting_for_card_message:I = 0x7f1215a3

.field public static final reader_timed_out_waiting_for_card_title:I = 0x7f1215a4

.field public static final reader_type_screen_button_r12:I = 0x7f1215a5

.field public static final receipt_detail_card:I = 0x7f1215a6

.field public static final receipt_detail_card_declined_message:I = 0x7f1215a7

.field public static final receipt_detail_card_declined_message_learn_more:I = 0x7f1215a8

.field public static final receipt_detail_card_declined_message_partial:I = 0x7f1215a9

.field public static final receipt_detail_card_declined_title:I = 0x7f1215aa

.field public static final receipt_detail_id:I = 0x7f1215ab

.field public static final receipt_detail_invoice_number:I = 0x7f1215ac

.field public static final receipt_detail_items_dining_option:I = 0x7f1215ad

.field public static final receipt_detail_not_sent:I = 0x7f1215ae

.field public static final receipt_detail_paid_cardcase:I = 0x7f1215af

.field public static final receipt_detail_paid_cardcase_uppercase:I = 0x7f1215b0

.field public static final receipt_detail_payment_forwarded_message:I = 0x7f1215b1

.field public static final receipt_detail_payment_forwarded_title:I = 0x7f1215b2

.field public static final receipt_detail_payment_pending_message:I = 0x7f1215b3

.field public static final receipt_detail_payment_pending_title:I = 0x7f1215b4

.field public static final receipt_detail_refunded:I = 0x7f1215b5

.field public static final receipt_detail_rounding:I = 0x7f1215b6

.field public static final receipt_detail_settle_additional_tip_title:I = 0x7f1215b7

.field public static final receipt_detail_settle_tip_confirm:I = 0x7f1215b8

.field public static final receipt_detail_settle_tip_failed_body:I = 0x7f1215b9

.field public static final receipt_detail_settle_tip_popup_title:I = 0x7f1215ba

.field public static final receipt_detail_settle_tip_title:I = 0x7f1215bb

.field public static final receipt_detail_tax_breakdown_net_uppercase:I = 0x7f1215bc

.field public static final receipt_detail_tax_breakdown_tax_rate_uppercase:I = 0x7f1215bd

.field public static final receipt_detail_tax_breakdown_tax_uppercase:I = 0x7f1215be

.field public static final receipt_detail_tax_breakdown_total_uppercase:I = 0x7f1215bf

.field public static final receipt_detail_tax_multiple_included:I = 0x7f1215c0

.field public static final receipt_detail_total:I = 0x7f1215c1

.field public static final receipt_email:I = 0x7f1215c2

.field public static final receipt_felica_reprint:I = 0x7f1215c3

.field public static final receipt_felica_suica_terminal_id:I = 0x7f1215c4

.field public static final receipt_header_purchase:I = 0x7f1215c5

.field public static final receipt_header_purchase_transaction_type:I = 0x7f1215c6

.field public static final receipt_header_refund_transaction_type:I = 0x7f1215c7

.field public static final receipt_header_return:I = 0x7f1215c8

.field public static final receipt_new:I = 0x7f1215c9

.field public static final receipt_paper:I = 0x7f1215ca

.field public static final receipt_paper_formal:I = 0x7f1215cb

.field public static final receipt_reissue:I = 0x7f1215cc

.field public static final receipt_sms:I = 0x7f1215cd

.field public static final receipt_tender_felica_id:I = 0x7f1215ce

.field public static final receipt_tender_felica_id_purchase_location:I = 0x7f1215cf

.field public static final receipt_tender_felica_id_refund_location:I = 0x7f1215d0

.field public static final receipt_tender_felica_qp:I = 0x7f1215d1

.field public static final receipt_tender_felica_suica:I = 0x7f1215d2

.field public static final receipt_title:I = 0x7f1215d3

.field public static final recent_activity_active_sales:I = 0x7f1215d4

.field public static final recent_activity_balance_title_uppercase:I = 0x7f1215d5

.field public static final recent_activity_card_activity_date_format:I = 0x7f1215d6

.field public static final recent_activity_deposits_title_uppercase:I = 0x7f1215d7

.field public static final recent_activity_empty_message:I = 0x7f1215d8

.field public static final recent_activity_empty_title:I = 0x7f1215d9

.field public static final recent_activity_error_message:I = 0x7f1215da

.field public static final recent_activity_error_title:I = 0x7f1215db

.field public static final recent_activity_pending_deposit:I = 0x7f1215dc

.field public static final recent_activity_sending_date:I = 0x7f1215dd

.field public static final recent_activity_unified_activity_title:I = 0x7f1215de

.field public static final record_card_payment:I = 0x7f1215df

.field public static final record_miscellaneous_forms_of_payment:I = 0x7f1215e0

.field public static final record_other_gift_card:I = 0x7f1215e1

.field public static final record_other_tender_disclaimer:I = 0x7f1215e2

.field public static final record_payment:I = 0x7f1215e3

.field public static final record_payment_action_label:I = 0x7f1215e4

.field public static final record_payment_amount:I = 0x7f1215e5

.field public static final record_payment_date:I = 0x7f1215e6

.field public static final record_payment_description:I = 0x7f1215e7

.field public static final record_payment_method_label:I = 0x7f1215e8

.field public static final record_payment_method_title:I = 0x7f1215e9

.field public static final record_payment_note_hint:I = 0x7f1215ea

.field public static final record_payment_save:I = 0x7f1215eb

.field public static final record_payment_send_receipt:I = 0x7f1215ec

.field public static final record_payment_tender_type_cash:I = 0x7f1215ed

.field public static final record_payment_tender_type_check:I = 0x7f1215ee

.field public static final record_payment_tender_type_other:I = 0x7f1215ef

.field public static final recurring:I = 0x7f1215f0

.field public static final recurring_ending_on_date:I = 0x7f1215f1

.field public static final recurring_frequency:I = 0x7f1215f2

.field public static final recurring_series:I = 0x7f1215f3

.field public static final recurring_series_updated:I = 0x7f1215f4

.field public static final recurring_state_filter_active:I = 0x7f1215f5

.field public static final recurring_state_filter_active_short:I = 0x7f1215f6

.field public static final recurring_state_filter_draft:I = 0x7f1215f7

.field public static final recurring_state_filter_draft_short:I = 0x7f1215f8

.field public static final recurring_state_filter_inactive:I = 0x7f1215f9

.field public static final recurring_state_filter_inactive_short:I = 0x7f1215fa

.field public static final recurring_state_filter_unknown:I = 0x7f1215fb

.field public static final redeem_rewards_add_customer:I = 0x7f1215fc

.field public static final redeem_rewards_cart_points_title:I = 0x7f1215fd

.field public static final redeem_rewards_cart_points_value:I = 0x7f1215fe

.field public static final redeem_rewards_contacts_error_message:I = 0x7f1215ff

.field public static final redeem_rewards_coupons_title:I = 0x7f121600

.field public static final redeem_rewards_customer_points_title:I = 0x7f121601

.field public static final redeem_rewards_failure_redeeming:I = 0x7f121602

.field public static final redeem_rewards_failure_returning:I = 0x7f121603

.field public static final redeem_rewards_item_in_category_title:I = 0x7f121604

.field public static final redeem_rewards_item_loading_title:I = 0x7f121605

.field public static final redeem_rewards_item_popup_body:I = 0x7f121606

.field public static final redeem_rewards_item_popup_body_category:I = 0x7f121607

.field public static final redeem_rewards_item_popup_deleted_body:I = 0x7f121608

.field public static final redeem_rewards_item_popup_deleted_title:I = 0x7f121609

.field public static final redeem_rewards_item_popup_negative_button:I = 0x7f12160a

.field public static final redeem_rewards_item_popup_positive_button:I = 0x7f12160b

.field public static final redeem_rewards_item_popup_title:I = 0x7f12160c

.field public static final redeem_rewards_no_available:I = 0x7f12160d

.field public static final redeem_rewards_points_message_add:I = 0x7f12160e

.field public static final redeem_rewards_points_message_enroll:I = 0x7f12160f

.field public static final redeem_rewards_redeem:I = 0x7f121610

.field public static final redeem_rewards_remove_customer:I = 0x7f121611

.field public static final redeem_rewards_title_coupons:I = 0x7f121612

.field public static final redeem_rewards_title_rewards:I = 0x7f121613

.field public static final redeem_rewards_undo:I = 0x7f121614

.field public static final redeem_rewards_use_code:I = 0x7f121615

.field public static final reference:I = 0x7f121616

.field public static final referral_balance_title:I = 0x7f121617

.field public static final referral_earn_rewards:I = 0x7f121618

.field public static final referral_email_subject:I = 0x7f121619

.field public static final referral_email_text:I = 0x7f12161a

.field public static final referral_get_free_processing:I = 0x7f12161b

.field public static final referral_later:I = 0x7f12161c

.field public static final referral_message:I = 0x7f12161d

.field public static final referral_message_rewards:I = 0x7f12161e

.field public static final referral_process_failure:I = 0x7f12161f

.field public static final referral_send_with:I = 0x7f121620

.field public static final referral_share_link:I = 0x7f121621

.field public static final referral_title:I = 0x7f121622

.field public static final referral_title_rewards:I = 0x7f121623

.field public static final referral_url_template:I = 0x7f121624

.field public static final referral_use_by:I = 0x7f121625

.field public static final referrals_notification_content:I = 0x7f121626

.field public static final referrals_notification_title:I = 0x7f121627

.field public static final refund:I = 0x7f121628

.field public static final refund_amount:I = 0x7f121629

.field public static final refund_amount_help:I = 0x7f12162a

.field public static final refund_amount_max:I = 0x7f12162b

.field public static final refund_caption:I = 0x7f12162c

.field public static final refund_caption_on_date_time_format:I = 0x7f12162d

.field public static final refund_card_presence_required_hint:I = 0x7f12162e

.field public static final refund_complete:I = 0x7f12162f

.field public static final refund_connect_reader_message:I = 0x7f121630

.field public static final refund_connect_reader_title:I = 0x7f121631

.field public static final refund_custom_amount:I = 0x7f121632

.field public static final refund_failed:I = 0x7f121633

.field public static final refund_fees:I = 0x7f121634

.field public static final refund_felica_id:I = 0x7f121635

.field public static final refund_felica_quicpay:I = 0x7f121636

.field public static final refund_gift_card_credit_card_hint:I = 0x7f121637

.field public static final refund_gift_card_help:I = 0x7f121638

.field public static final refund_gift_card_hint:I = 0x7f121639

.field public static final refund_gift_card_line_item:I = 0x7f12163a

.field public static final refund_gift_card_many_line_items:I = 0x7f12163b

.field public static final refund_gift_card_many_line_items_subtext:I = 0x7f12163c

.field public static final refund_gift_card_split_tender_hint:I = 0x7f12163d

.field public static final refund_help_text_url:I = 0x7f12163e

.field public static final refund_help_url:I = 0x7f12163f

.field public static final refund_includes_taxes_and_discounts:I = 0x7f121640

.field public static final refund_issue:I = 0x7f121641

.field public static final refund_issuing:I = 0x7f121642

.field public static final refund_legal:I = 0x7f121643

.field public static final refund_past_deadline_message:I = 0x7f121644

.field public static final refund_past_deadline_message_plural:I = 0x7f121645

.field public static final refund_past_deadline_title:I = 0x7f121646

.field public static final refund_reason_accidental_charge:I = 0x7f121647

.field public static final refund_reason_canceled_order:I = 0x7f121648

.field public static final refund_reason_fraudulent_charge:I = 0x7f121649

.field public static final refund_reason_other_hint:I = 0x7f12164a

.field public static final refund_reason_other_title:I = 0x7f12164b

.field public static final refund_reason_returned_goods:I = 0x7f12164c

.field public static final refund_requested_toast:I = 0x7f12164d

.field public static final refund_select_all_items:I = 0x7f12164e

.field public static final refund_server_4xx_message:I = 0x7f12164f

.field public static final refund_server_4xx_title:I = 0x7f121650

.field public static final refund_string:I = 0x7f121651

.field public static final refund_tap_contactless_card:I = 0x7f121652

.field public static final refund_tap_dip_message:I = 0x7f121653

.field public static final refund_tap_dip_title:I = 0x7f121654

.field public static final refund_tax_help:I = 0x7f121655

.field public static final refund_taxes_and_fees:I = 0x7f121656

.field public static final refund_tender_type_and_amount:I = 0x7f121657

.field public static final refund_tenders_awaiting_tip_message:I = 0x7f121658

.field public static final refund_total_refundable_amount:I = 0x7f121659

.field public static final refund_with_max_amount_help:I = 0x7f12165a

.field public static final refund_with_max_amount_with_tax_info_help:I = 0x7f12165b

.field public static final refunded:I = 0x7f12165c

.field public static final refunded_amount:I = 0x7f12165d

.field public static final refunded_section_header:I = 0x7f12165e

.field public static final refunding:I = 0x7f12165f

.field public static final reload_balance:I = 0x7f121660

.field public static final reload_button_content_description:I = 0x7f121661

.field public static final reminder_save_error_duplicate_days_body:I = 0x7f121662

.field public static final reminder_save_error_duplicate_days_title:I = 0x7f121663

.field public static final reminder_scheduled:I = 0x7f121664

.field public static final reminder_scheduled_description:I = 0x7f121665

.field public static final reminders_off_description:I = 0x7f121666

.field public static final reminders_scheduled_description:I = 0x7f121667

.field public static final remove:I = 0x7f121668

.field public static final remove_attachment:I = 0x7f121669

.field public static final remove_deposit_request:I = 0x7f12166a

.field public static final remove_deposit_request_confirm_message:I = 0x7f12166b

.field public static final remove_deposit_request_confirm_title:I = 0x7f12166c

.field public static final remove_discount_content_description:I = 0x7f12166d

.field public static final remove_invoice_attachment:I = 0x7f12166e

.field public static final remove_invoice_attachment_confirm:I = 0x7f12166f

.field public static final remove_payment_request:I = 0x7f121670

.field public static final remove_reader_heading:I = 0x7f121671

.field public static final remove_reader_message:I = 0x7f121672

.field public static final remove_tax:I = 0x7f121673

.field public static final remove_tax_content_description:I = 0x7f121674

.field public static final repeat_every:I = 0x7f121675

.field public static final replay_tutorial:I = 0x7f121676

.field public static final report_calendar_date_range:I = 0x7f121677

.field public static final report_calendar_date_range_with_times:I = 0x7f121678

.field public static final report_calendar_single_day_time_range:I = 0x7f121679

.field public static final report_config_all_day:I = 0x7f12167a

.field public static final report_config_apply:I = 0x7f12167b

.field public static final report_config_customize:I = 0x7f12167c

.field public static final report_config_device_filter:I = 0x7f12167d

.field public static final report_config_employee_selection:I = 0x7f12167e

.field public static final report_config_employee_selection_all_employees:I = 0x7f12167f

.field public static final report_config_employee_selection_filter:I = 0x7f121680

.field public static final report_config_employee_selection_header_upercase:I = 0x7f121681

.field public static final report_config_employee_selection_loading:I = 0x7f121682

.field public static final report_config_employee_selection_multiple_employees:I = 0x7f121683

.field public static final report_config_employee_selection_name:I = 0x7f121684

.field public static final report_config_employee_selection_one_employee:I = 0x7f121685

.field public static final report_config_end_time:I = 0x7f121686

.field public static final report_config_hour_format:I = 0x7f121687

.field public static final report_config_items_details:I = 0x7f121688

.field public static final report_config_month_format:I = 0x7f121689

.field public static final report_config_month_short_format:I = 0x7f12168a

.field public static final report_config_start_time:I = 0x7f12168b

.field public static final reports_current_drawer:I = 0x7f12168c

.field public static final reports_disputes:I = 0x7f12168d

.field public static final reports_drawer_history:I = 0x7f12168e

.field public static final reports_error:I = 0x7f12168f

.field public static final reports_error_message:I = 0x7f121690

.field public static final reports_error_reload:I = 0x7f121691

.field public static final reports_loyalty:I = 0x7f121692

.field public static final reports_net_total:I = 0x7f121693

.field public static final reports_sales:I = 0x7f121694

.field public static final reprint_ticket:I = 0x7f121695

.field public static final request_deposit:I = 0x7f121696

.field public static final request_initial_deposit:I = 0x7f121697

.field public static final reset_card_pin:I = 0x7f121698

.field public static final reset_square_card_pin_create_code_message:I = 0x7f121699

.field public static final reset_square_card_pin_title:I = 0x7f12169a

.field public static final restart:I = 0x7f12169b

.field public static final restock_items:I = 0x7f12169c

.field public static final restock_recoverable_failure_message:I = 0x7f12169d

.field public static final restock_select_all_items:I = 0x7f12169e

.field public static final restock_skip_restock:I = 0x7f12169f

.field public static final restore_connectivity_to_refund:I = 0x7f1216a0

.field public static final restore_connectivity_to_settle_plural:I = 0x7f1216a1

.field public static final restore_connectivity_to_settle_plural_noperiod:I = 0x7f1216a2

.field public static final restore_connectivity_to_settle_single:I = 0x7f1216a3

.field public static final retry:I = 0x7f1216a4

.field public static final return_or_exchange:I = 0x7f1216a5

.field public static final review_variations_to_delete_header:I = 0x7f1216a6

.field public static final review_variations_to_delete_title:I = 0x7f1216a7

.field public static final riker_version:I = 0x7f1216a8

.field public static final root_activity_title:I = 0x7f1216a9

.field public static final routing_account_number_match_headline:I = 0x7f1216aa

.field public static final routing_account_number_match_prompt:I = 0x7f1216ab

.field public static final routing_number:I = 0x7f1216ac

.field public static final rules_many:I = 0x7f1216ad

.field public static final run_and_grow_your_business:I = 0x7f1216ae

.field public static final sales_report:I = 0x7f1216af

.field public static final sales_report_action_bar_title:I = 0x7f1216b0

.field public static final sales_report_average_sales:I = 0x7f1216b1

.field public static final sales_report_card:I = 0x7f1216b2

.field public static final sales_report_cash:I = 0x7f1216b3

.field public static final sales_report_cash_rounding:I = 0x7f1216b4

.field public static final sales_report_chart_highlight_plural:I = 0x7f1216b5

.field public static final sales_report_chart_highlight_single:I = 0x7f1216b6

.field public static final sales_report_chart_highlight_zero:I = 0x7f1216b7

.field public static final sales_report_chart_scrub_compare_label_weekly_format:I = 0x7f1216b8

.field public static final sales_report_chart_scrub_label_date_format:I = 0x7f1216b9

.field public static final sales_report_chart_scrub_label_hour_format:I = 0x7f1216ba

.field public static final sales_report_chart_timespan_daily_uppercase:I = 0x7f1216bb

.field public static final sales_report_chart_timespan_monthly3_uppercase:I = 0x7f1216bc

.field public static final sales_report_chart_timespan_monthly_uppercase:I = 0x7f1216bd

.field public static final sales_report_chart_timespan_weekly_uppercase:I = 0x7f1216be

.field public static final sales_report_chart_timespan_yearly_uppercase:I = 0x7f1216bf

.field public static final sales_report_chart_title_custom_timespan_format:I = 0x7f1216c0

.field public static final sales_report_chart_title_preset_timespan_format:I = 0x7f1216c1

.field public static final sales_report_chart_type_gross_sales:I = 0x7f1216c2

.field public static final sales_report_chart_type_gross_sales_uppercase:I = 0x7f1216c3

.field public static final sales_report_chart_type_net_sales:I = 0x7f1216c4

.field public static final sales_report_chart_type_net_sales_uppercase:I = 0x7f1216c5

.field public static final sales_report_chart_type_sales_count:I = 0x7f1216c6

.field public static final sales_report_chart_type_sales_count_uppercase:I = 0x7f1216c7

.field public static final sales_report_compare_button_content_description:I = 0x7f1216c8

.field public static final sales_report_comparison_previous_day:I = 0x7f1216c9

.field public static final sales_report_comparison_previous_month:I = 0x7f1216ca

.field public static final sales_report_comparison_previous_three_months:I = 0x7f1216cb

.field public static final sales_report_comparison_previous_week:I = 0x7f1216cc

.field public static final sales_report_comparison_previous_year:I = 0x7f1216cd

.field public static final sales_report_comparison_same_day_previous_week:I = 0x7f1216ce

.field public static final sales_report_comparison_same_day_previous_year:I = 0x7f1216cf

.field public static final sales_report_comparison_same_month_previous_year:I = 0x7f1216d0

.field public static final sales_report_comparison_same_months_previous_year:I = 0x7f1216d1

.field public static final sales_report_comparison_same_week_previous_year:I = 0x7f1216d2

.field public static final sales_report_comparison_yesterday:I = 0x7f1216d3

.field public static final sales_report_count_tab:I = 0x7f1216d4

.field public static final sales_report_cover_count:I = 0x7f1216d5

.field public static final sales_report_customize_button_content_description:I = 0x7f1216d6

.field public static final sales_report_dashboard_category_sales_url:I = 0x7f1216d7

.field public static final sales_report_dashboard_item_sales_url:I = 0x7f1216d8

.field public static final sales_report_date_format_date_only:I = 0x7f1216d9

.field public static final sales_report_date_format_date_range:I = 0x7f1216da

.field public static final sales_report_date_format_date_time_range:I = 0x7f1216db

.field public static final sales_report_date_format_day_of_week:I = 0x7f1216dc

.field public static final sales_report_date_format_day_of_week_letter:I = 0x7f1216dd

.field public static final sales_report_date_format_day_only:I = 0x7f1216de

.field public static final sales_report_date_format_month_day:I = 0x7f1216df

.field public static final sales_report_date_format_month_day_year:I = 0x7f1216e0

.field public static final sales_report_date_format_month_only:I = 0x7f1216e1

.field public static final sales_report_date_format_month_year:I = 0x7f1216e2

.field public static final sales_report_date_format_same_date_time_range:I = 0x7f1216e3

.field public static final sales_report_date_format_year_only:I = 0x7f1216e4

.field public static final sales_report_details_cash_rounding:I = 0x7f1216e5

.field public static final sales_report_details_discounts_and_comps:I = 0x7f1216e6

.field public static final sales_report_details_down_caret_description:I = 0x7f1216e7

.field public static final sales_report_details_gift_cards:I = 0x7f1216e8

.field public static final sales_report_details_gross_sales:I = 0x7f1216e9

.field public static final sales_report_details_label_uppercase:I = 0x7f1216ea

.field public static final sales_report_details_net_sales:I = 0x7f1216eb

.field public static final sales_report_details_refund:I = 0x7f1216ec

.field public static final sales_report_details_refunds_by_amount:I = 0x7f1216ed

.field public static final sales_report_details_right_caret_description:I = 0x7f1216ee

.field public static final sales_report_details_show_overview:I = 0x7f1216ef

.field public static final sales_report_details_tax:I = 0x7f1216f0

.field public static final sales_report_details_tips:I = 0x7f1216f1

.field public static final sales_report_details_total:I = 0x7f1216f2

.field public static final sales_report_discounts:I = 0x7f1216f3

.field public static final sales_report_email_offline:I = 0x7f1216f4

.field public static final sales_report_email_report:I = 0x7f1216f5

.field public static final sales_report_email_sent:I = 0x7f1216f6

.field public static final sales_report_employee_no_employees_found:I = 0x7f1216f7

.field public static final sales_report_employee_selection_search_hint:I = 0x7f1216f8

.field public static final sales_report_empty_button:I = 0x7f1216f9

.field public static final sales_report_empty_message:I = 0x7f1216fa

.field public static final sales_report_empty_title:I = 0x7f1216fb

.field public static final sales_report_fee_details_learn_more:I = 0x7f1216fc

.field public static final sales_report_fee_details_message:I = 0x7f1216fd

.field public static final sales_report_fee_details_title:I = 0x7f1216fe

.field public static final sales_report_fees:I = 0x7f1216ff

.field public static final sales_report_gift_card:I = 0x7f121700

.field public static final sales_report_gift_card_sales:I = 0x7f121701

.field public static final sales_report_gross_sales:I = 0x7f121702

.field public static final sales_report_header_category_uppercase:I = 0x7f121703

.field public static final sales_report_header_count:I = 0x7f121704

.field public static final sales_report_header_count_uppercase:I = 0x7f121705

.field public static final sales_report_header_gross:I = 0x7f121706

.field public static final sales_report_header_gross_uppercase:I = 0x7f121707

.field public static final sales_report_header_item_uppercase:I = 0x7f121708

.field public static final sales_report_hint_customize:I = 0x7f121709

.field public static final sales_report_item_gross_sales:I = 0x7f12170a

.field public static final sales_report_more_options_button:I = 0x7f12170b

.field public static final sales_report_name_with_quantity:I = 0x7f12170c

.field public static final sales_report_net_sales:I = 0x7f12170d

.field public static final sales_report_network_error_button:I = 0x7f12170e

.field public static final sales_report_network_error_message:I = 0x7f12170f

.field public static final sales_report_network_error_title:I = 0x7f121710

.field public static final sales_report_no_transactions_for_time_period:I = 0x7f121711

.field public static final sales_report_no_transactions_message:I = 0x7f121712

.field public static final sales_report_other:I = 0x7f121713

.field public static final sales_report_overflow_options_button_content_description:I = 0x7f121714

.field public static final sales_report_overview_average_sale_label:I = 0x7f121715

.field public static final sales_report_overview_discount_label:I = 0x7f121716

.field public static final sales_report_overview_gross_sales_label:I = 0x7f121717

.field public static final sales_report_overview_header_uppercase:I = 0x7f121718

.field public static final sales_report_overview_net_sales_label:I = 0x7f121719

.field public static final sales_report_overview_refunds_label:I = 0x7f12171a

.field public static final sales_report_overview_sales_label:I = 0x7f12171b

.field public static final sales_report_overview_show_details:I = 0x7f12171c

.field public static final sales_report_partial_refund:I = 0x7f12171d

.field public static final sales_report_payment_method_card_title:I = 0x7f12171e

.field public static final sales_report_payment_method_cash_title:I = 0x7f12171f

.field public static final sales_report_payment_method_emoney_title:I = 0x7f121720

.field public static final sales_report_payment_method_external_title:I = 0x7f121721

.field public static final sales_report_payment_method_fees:I = 0x7f121722

.field public static final sales_report_payment_method_gift_card_title:I = 0x7f121723

.field public static final sales_report_payment_method_installment_title:I = 0x7f121724

.field public static final sales_report_payment_method_net_total:I = 0x7f121725

.field public static final sales_report_payment_method_other_title:I = 0x7f121726

.field public static final sales_report_payment_method_split_tender_title:I = 0x7f121727

.field public static final sales_report_payment_method_third_party_card_title:I = 0x7f121728

.field public static final sales_report_payment_method_total_collected:I = 0x7f121729

.field public static final sales_report_payment_method_zero_amount_title:I = 0x7f12172a

.field public static final sales_report_payment_methods_header_uppercase:I = 0x7f12172b

.field public static final sales_report_print_category_sales_uppercase:I = 0x7f12172c

.field public static final sales_report_print_date_format:I = 0x7f12172d

.field public static final sales_report_print_discounts_applied_uppercase:I = 0x7f12172e

.field public static final sales_report_print_item_sales_uppercase:I = 0x7f12172f

.field public static final sales_report_print_job_title:I = 0x7f121730

.field public static final sales_report_print_payments_uppercase:I = 0x7f121731

.field public static final sales_report_print_report:I = 0x7f121732

.field public static final sales_report_print_report_date_printout_format:I = 0x7f121733

.field public static final sales_report_print_report_date_printout_pattern:I = 0x7f121734

.field public static final sales_report_print_sales_uppercase:I = 0x7f121735

.field public static final sales_report_print_title_uppercase:I = 0x7f121736

.field public static final sales_report_product_sales:I = 0x7f121737

.field public static final sales_report_refund_count:I = 0x7f121738

.field public static final sales_report_refunds:I = 0x7f121739

.field public static final sales_report_returns:I = 0x7f12173a

.field public static final sales_report_sales_chart_overflow_options_button_content_description:I = 0x7f12173b

.field public static final sales_report_sales_count:I = 0x7f12173c

.field public static final sales_report_select_time_frame:I = 0x7f12173d

.field public static final sales_report_service_charges:I = 0x7f12173e

.field public static final sales_report_summary_overflow_options_button_content_description:I = 0x7f12173f

.field public static final sales_report_tax:I = 0x7f121740

.field public static final sales_report_third_party_card:I = 0x7f121741

.field public static final sales_report_time_selection_1_day:I = 0x7f121742

.field public static final sales_report_time_selection_1_month:I = 0x7f121743

.field public static final sales_report_time_selection_1_week:I = 0x7f121744

.field public static final sales_report_time_selection_1_year:I = 0x7f121745

.field public static final sales_report_time_selection_3_months:I = 0x7f121746

.field public static final sales_report_time_selection_custom_1_day:I = 0x7f121747

.field public static final sales_report_time_selection_custom_1_month:I = 0x7f121748

.field public static final sales_report_time_selection_custom_1_week:I = 0x7f121749

.field public static final sales_report_time_selection_custom_3_months:I = 0x7f12174a

.field public static final sales_report_time_selector_1day:I = 0x7f12174b

.field public static final sales_report_time_selector_1month:I = 0x7f12174c

.field public static final sales_report_time_selector_1week:I = 0x7f12174d

.field public static final sales_report_time_selector_1year:I = 0x7f12174e

.field public static final sales_report_time_selector_3month:I = 0x7f12174f

.field public static final sales_report_time_selector_one_day_content_description:I = 0x7f121750

.field public static final sales_report_time_selector_one_month_content_description:I = 0x7f121751

.field public static final sales_report_time_selector_one_week_content_description:I = 0x7f121752

.field public static final sales_report_time_selector_one_year_content_description:I = 0x7f121753

.field public static final sales_report_time_selector_three_months_content_description:I = 0x7f121754

.field public static final sales_report_tip:I = 0x7f121755

.field public static final sales_report_top_bar_subtitle_format:I = 0x7f121756

.field public static final sales_report_top_bar_subtitle_format_without_comparison:I = 0x7f121757

.field public static final sales_report_top_bar_subtitle_format_without_employee:I = 0x7f121758

.field public static final sales_report_top_bar_subtitle_format_without_employee_and_comparison:I = 0x7f121759

.field public static final sales_report_top_bar_title_format:I = 0x7f12175a

.field public static final sales_report_top_categories_header_uppercase:I = 0x7f12175b

.field public static final sales_report_top_categories_overflow_options_button_content_description:I = 0x7f12175c

.field public static final sales_report_top_items_header_uppercase:I = 0x7f12175d

.field public static final sales_report_top_items_overflow_options_button_content_description:I = 0x7f12175e

.field public static final sales_report_total:I = 0x7f12175f

.field public static final sales_report_total_collected:I = 0x7f121760

.field public static final sales_report_unattributed_employee:I = 0x7f121761

.field public static final sales_report_view_collapse_category_details:I = 0x7f121762

.field public static final sales_report_view_collapse_item_details:I = 0x7f121763

.field public static final sales_report_view_count_10:I = 0x7f121764

.field public static final sales_report_view_count_5:I = 0x7f121765

.field public static final sales_report_view_count_all:I = 0x7f121766

.field public static final sales_report_view_expand_category_details:I = 0x7f121767

.field public static final sales_report_view_expand_item_details:I = 0x7f121768

.field public static final sales_report_view_in_dashboard:I = 0x7f121769

.field public static final same_day_deposit:I = 0x7f12176a

.field public static final same_day_deposit_description:I = 0x7f12176b

.field public static final sample_rate_unset_message:I = 0x7f12176c

.field public static final sample_rate_unset_title:I = 0x7f12176d

.field public static final saturdays:I = 0x7f12176e

.field public static final save:I = 0x7f12176f

.field public static final save_item:I = 0x7f121770

.field public static final save_message_as_default:I = 0x7f121771

.field public static final saved_card:I = 0x7f121772

.field public static final savings:I = 0x7f121773

.field public static final scale_connected:I = 0x7f121774

.field public static final scale_disconnected:I = 0x7f121775

.field public static final scale_manufacturer_brecknell:I = 0x7f121776

.field public static final scale_manufacturer_cas:I = 0x7f121777

.field public static final scale_manufacturer_starmicronics:I = 0x7f121778

.field public static final scales_action_bar_title:I = 0x7f121779

.field public static final scales_header_title_uppercase:I = 0x7f12177a

.field public static final scales_settings_title:I = 0x7f12177b

.field public static final scanner_unknown_product:I = 0x7f12177c

.field public static final scanner_unknown_vendor:I = 0x7f12177d

.field public static final scanner_vendor_and_product:I = 0x7f12177e

.field public static final schedule:I = 0x7f12177f

.field public static final schedule_days_of_the_week_daily:I = 0x7f121780

.field public static final schedule_days_of_the_week_many:I = 0x7f121781

.field public static final schedule_days_of_the_week_weekdays:I = 0x7f121782

.field public static final schedule_many:I = 0x7f121783

.field public static final schedule_time_range:I = 0x7f121784

.field public static final schedule_time_range_next_day:I = 0x7f121785

.field public static final scroll_down_to_try_out_new_feature:I = 0x7f121786

.field public static final sdk_suffix:I = 0x7f121787

.field public static final search_hint:I = 0x7f121788

.field public static final search_hint_text:I = 0x7f121789

.field public static final search_history_hint_deprecated:I = 0x7f12178a

.field public static final search_history_hint_rw:I = 0x7f12178b

.field public static final search_history_x2_hint:I = 0x7f12178c

.field public static final search_library_hint_all:I = 0x7f12178d

.field public static final search_library_hint_all_items:I = 0x7f12178e

.field public static final search_library_hint_all_services:I = 0x7f12178f

.field public static final search_menu_title:I = 0x7f121790

.field public static final search_transactions_history_hint:I = 0x7f121791

.field public static final search_transactions_history_hint_offline:I = 0x7f121792

.field public static final seating_seat_format:I = 0x7f121793

.field public static final seating_table_shared:I = 0x7f121794

.field public static final secondary_button_save:I = 0x7f121795

.field public static final secondary_payment_title_amount:I = 0x7f121796

.field public static final secure_session_required_for_dip_title:I = 0x7f121797

.field public static final secure_session_required_for_swipe_message:I = 0x7f121798

.field public static final secure_session_required_for_swipe_title:I = 0x7f121799

.field public static final secure_session_revoked_default_desc:I = 0x7f12179a

.field public static final secure_session_revoked_default_title:I = 0x7f12179b

.field public static final select_a_business:I = 0x7f12179c

.field public static final select_a_business_subtext:I = 0x7f12179d

.field public static final select_a_location:I = 0x7f12179e

.field public static final select_a_location_subtext:I = 0x7f12179f

.field public static final select_an_option:I = 0x7f1217a0

.field public static final select_an_option_detail:I = 0x7f1217a1

.field public static final select_one_uppercase:I = 0x7f1217a2

.field public static final select_option_values_action_bar_primary_button_text:I = 0x7f1217a3

.field public static final select_option_values_add_new_option_value_row_hint:I = 0x7f1217a4

.field public static final select_option_values_all_options:I = 0x7f1217a5

.field public static final select_option_values_capital_section_header:I = 0x7f1217a6

.field public static final select_option_values_create_from_search:I = 0x7f1217a7

.field public static final select_option_values_duplicate_option_value_name_message:I = 0x7f1217a8

.field public static final select_option_values_duplicate_option_value_name_title:I = 0x7f1217a9

.field public static final select_option_values_for_variation_add_custom_item_variation_upper_case:I = 0x7f1217aa

.field public static final select_option_values_for_variation_add_variation:I = 0x7f1217ab

.field public static final select_option_values_for_variation_choose_from_option_values_upper_case:I = 0x7f1217ac

.field public static final select_option_values_for_variation_create_variation_button_text:I = 0x7f1217ad

.field public static final select_option_values_for_variation_item_option_value_placeholder_text:I = 0x7f1217ae

.field public static final select_option_values_remove_option_set:I = 0x7f1217af

.field public static final select_option_values_search_bar_hint_format:I = 0x7f1217b0

.field public static final select_options_create_from_search:I = 0x7f1217b1

.field public static final select_options_null_state_content:I = 0x7f1217b2

.field public static final select_options_null_state_create_button_hint:I = 0x7f1217b3

.field public static final select_options_too_many_option_sets_message:I = 0x7f1217b4

.field public static final select_options_too_many_option_sets_title:I = 0x7f1217b5

.field public static final select_tender_title:I = 0x7f1217b6

.field public static final select_variations_to_create_screen_all_variations_label:I = 0x7f1217b7

.field public static final select_variations_to_create_screen_title:I = 0x7f1217b8

.field public static final select_variations_to_create_screen_update_existing_variations_row_help_text:I = 0x7f1217b9

.field public static final select_variations_to_create_screen_variations_label:I = 0x7f1217ba

.field public static final select_variations_to_create_workflow_update_existing_variations_text:I = 0x7f1217bb

.field public static final seller_agreement:I = 0x7f1217bc

.field public static final seller_community_url:I = 0x7f1217bd

.field public static final send:I = 0x7f1217be

.field public static final send_anyways:I = 0x7f1217bf

.field public static final send_diagnostic_report:I = 0x7f1217c0

.field public static final send_reader_subheading:I = 0x7f1217c1

.field public static final send_square_card_confirmation_subheading:I = 0x7f1217c2

.field public static final send_square_card_subheading:I = 0x7f1217c3

.field public static final sending_to_bank_uppercase:I = 0x7f1217c4

.field public static final series_list_null:I = 0x7f1217c5

.field public static final series_list_title:I = 0x7f1217c6

.field public static final server_error_message:I = 0x7f1217c7

.field public static final server_error_title:I = 0x7f1217c8

.field public static final service_charge_disclosure:I = 0x7f1217c9

.field public static final service_charge_name:I = 0x7f1217ca

.field public static final service_user_number_body:I = 0x7f1217cb

.field public static final service_user_number_title:I = 0x7f1217cc

.field public static final session_expired_message:I = 0x7f1217cd

.field public static final session_expired_title:I = 0x7f1217ce

.field public static final set_price:I = 0x7f1217cf

.field public static final set_up_instant_deposit:I = 0x7f1217d0

.field public static final settings_group_uppercase_account:I = 0x7f1217d1

.field public static final settings_group_uppercase_checkout_options:I = 0x7f1217d2

.field public static final settings_group_uppercase_device:I = 0x7f1217d3

.field public static final settings_group_uppercase_hardware:I = 0x7f1217d4

.field public static final settings_group_uppercase_orders:I = 0x7f1217d5

.field public static final settings_group_uppercase_security:I = 0x7f1217d6

.field public static final settings_group_uppercase_team_management:I = 0x7f1217d7

.field public static final settings_title:I = 0x7f1217d8

.field public static final setup_failed:I = 0x7f1217d9

.field public static final setup_guide:I = 0x7f1217da

.field public static final setup_guide_free_reader_card_message:I = 0x7f1217db

.field public static final setup_guide_free_reader_card_title:I = 0x7f1217dc

.field public static final share:I = 0x7f1217dd

.field public static final share_sheet_subject:I = 0x7f1217de

.field public static final share_sheet_title:I = 0x7f1217df

.field public static final shared_settings_empty_state_dashboard_link:I = 0x7f1217e0

.field public static final shared_settings_empty_state_dashboard_link_text:I = 0x7f1217e1

.field public static final shared_settings_empty_state_message_text:I = 0x7f1217e2

.field public static final shared_settings_empty_state_title_text:I = 0x7f1217e3

.field public static final shared_settings_synced:I = 0x7f1217e4

.field public static final shared_settings_title:I = 0x7f1217e5

.field public static final ship_to:I = 0x7f1217e6

.field public static final shipping_address_confirmation_message:I = 0x7f1217e7

.field public static final shipping_address_confirmation_title:I = 0x7f1217e8

.field public static final shipping_address_unverified_message:I = 0x7f1217e9

.field public static final shipping_address_unverified_title:I = 0x7f1217ea

.field public static final shipping_details:I = 0x7f1217eb

.field public static final short_form_date_time_template:I = 0x7f1217ec

.field public static final short_friday:I = 0x7f1217ed

.field public static final short_monday:I = 0x7f1217ee

.field public static final short_saturday:I = 0x7f1217ef

.field public static final short_sunday:I = 0x7f1217f0

.field public static final short_thursday:I = 0x7f1217f1

.field public static final short_tuesday:I = 0x7f1217f2

.field public static final short_wednesday:I = 0x7f1217f3

.field public static final show_private_data_two_factor_title:I = 0x7f1217f4

.field public static final sign_in:I = 0x7f1217f5

.field public static final sign_in_signing_in:I = 0x7f1217f6

.field public static final sign_in_to_square:I = 0x7f1217f7

.field public static final sign_out:I = 0x7f1217f8

.field public static final sign_out_confirm:I = 0x7f1217f9

.field public static final sign_skip_under_amount:I = 0x7f1217fa

.field public static final sign_under_amount_hint:I = 0x7f1217fb

.field public static final signature_always_collect:I = 0x7f1217fc

.field public static final signature_and_receipt_title:I = 0x7f1217fd

.field public static final signature_clear_signature:I = 0x7f1217fe

.field public static final signature_collect_over_amount:I = 0x7f1217ff

.field public static final signature_confirm_design_action_bar:I = 0x7f121800

.field public static final signature_hint:I = 0x7f121801

.field public static final signature_never_collect:I = 0x7f121802

.field public static final signature_never_collect_hint:I = 0x7f121803

.field public static final signature_optional_off_short:I = 0x7f121804

.field public static final signature_optional_on:I = 0x7f121805

.field public static final signature_optional_on_short:I = 0x7f121806

.field public static final signature_personalize_card_action_bar:I = 0x7f121807

.field public static final signature_title:I = 0x7f121808

.field public static final signing_up:I = 0x7f121809

.field public static final signing_up_fail:I = 0x7f12180a

.field public static final signup_actionbar_sign_up:I = 0x7f12180b

.field public static final signup_lets_get_started_body:I = 0x7f12180c

.field public static final signup_lets_get_started_title:I = 0x7f12180d

.field public static final signup_missing_required_field:I = 0x7f12180e

.field public static final skip:I = 0x7f12180f

.field public static final skip_device_tour_cancel_exit:I = 0x7f121810

.field public static final skip_device_tour_confirm_exit:I = 0x7f121811

.field public static final skip_device_tour_message:I = 0x7f121812

.field public static final skip_device_tour_title:I = 0x7f121813

.field public static final skip_for_now:I = 0x7f121814

.field public static final skip_receipt_screen:I = 0x7f121815

.field public static final skip_receipt_screen_helper_text:I = 0x7f121816

.field public static final skip_receipt_screen_url:I = 0x7f121817

.field public static final skip_receipt_screen_warning_content:I = 0x7f121818

.field public static final skip_receipt_screen_warning_title:I = 0x7f121819

.field public static final skip_walkthrough_cancel_exit:I = 0x7f12181a

.field public static final skip_walkthrough_confirm_exit:I = 0x7f12181b

.field public static final skip_walkthrough_message:I = 0x7f12181c

.field public static final skip_walkthrough_title:I = 0x7f12181d

.field public static final sku_error_message:I = 0x7f12181e

.field public static final sku_error_message_batched:I = 0x7f12181f

.field public static final sku_error_message_local:I = 0x7f121820

.field public static final sku_not_found_actionbar:I = 0x7f121821

.field public static final sku_not_found_message:I = 0x7f121822

.field public static final sku_not_found_title:I = 0x7f121823

.field public static final smart_reader_anonymous_name:I = 0x7f121824

.field public static final smart_reader_chip_name:I = 0x7f121825

.field public static final smart_reader_contactless_and_chip_name:I = 0x7f121826

.field public static final smart_reader_updating:I = 0x7f121827

.field public static final sms_marketing_check_your_texts:I = 0x7f121828

.field public static final sms_marketing_input_detail:I = 0x7f121829

.field public static final sms_marketing_input_enter_phone_number:I = 0x7f12182a

.field public static final sms_marketing_input_is_this_correct:I = 0x7f12182b

.field public static final sms_marketing_input_submit:I = 0x7f12182c

.field public static final sms_marketing_no_thanks:I = 0x7f12182d

.field public static final sms_marketing_receipt_sent_to:I = 0x7f12182e

.field public static final sms_marketing_sign_up:I = 0x7f12182f

.field public static final sms_marketing_transaction_complete:I = 0x7f121830

.field public static final sms_marketing_youre_all_set:I = 0x7f121831

.field public static final sort_code:I = 0x7f121832

.field public static final specified_products:I = 0x7f121833

.field public static final specified_times:I = 0x7f121834

.field public static final speed_test_checking_connection:I = 0x7f121835

.field public static final speed_test_complete_message:I = 0x7f121836

.field public static final speed_test_complete_title:I = 0x7f121837

.field public static final speed_test_failure_message:I = 0x7f121838

.field public static final speed_test_failure_title:I = 0x7f121839

.field public static final speed_test_failure_url:I = 0x7f12183a

.field public static final speed_test_in_progress:I = 0x7f12183b

.field public static final speed_test_success_url:I = 0x7f12183c

.field public static final speed_test_support_link_text:I = 0x7f12183d

.field public static final splash_page_accept_payments:I = 0x7f12183e

.field public static final splash_page_build_trust:I = 0x7f12183f

.field public static final splash_page_get_started:I = 0x7f121840

.field public static final splash_page_reports:I = 0x7f121841

.field public static final splash_page_sell_in_minutes:I = 0x7f121842

.field public static final splash_page_sign_in:I = 0x7f121843

.field public static final split_balance_into_milestones:I = 0x7f121844

.field public static final split_tender:I = 0x7f121845

.field public static final split_tender_again:I = 0x7f121846

.field public static final split_tender_amount_done:I = 0x7f121847

.field public static final split_tender_amount_label:I = 0x7f121848

.field public static final split_tender_amount_remaining:I = 0x7f121849

.field public static final split_tender_amount_remaining_help_text:I = 0x7f12184a

.field public static final split_tender_cancel_payment_cancel:I = 0x7f12184b

.field public static final split_tender_cancel_payment_keep:I = 0x7f12184c

.field public static final split_tender_cancel_payment_message:I = 0x7f12184d

.field public static final split_tender_cancel_payment_title:I = 0x7f12184e

.field public static final split_tender_card_brand_and_digits:I = 0x7f12184f

.field public static final split_tender_card_declined:I = 0x7f121850

.field public static final split_tender_card_declined_no_digits:I = 0x7f121851

.field public static final split_tender_card_tendered:I = 0x7f121852

.field public static final split_tender_card_tendered_plus_tip:I = 0x7f121853

.field public static final split_tender_cash_declined:I = 0x7f121854

.field public static final split_tender_cash_tendered:I = 0x7f121855

.field public static final split_tender_completed_payments:I = 0x7f121856

.field public static final split_tender_completed_payments_cancel:I = 0x7f121857

.field public static final split_tender_edit_split:I = 0x7f121858

.field public static final split_tender_equal_label:I = 0x7f121859

.field public static final split_tender_equal_split_label:I = 0x7f12185a

.field public static final split_tender_equal_split_title:I = 0x7f12185b

.field public static final split_tender_other_declined:I = 0x7f12185c

.field public static final split_tender_other_tendered:I = 0x7f12185d

.field public static final split_tender_payment_amount_label:I = 0x7f12185e

.field public static final split_tender_secondary_button:I = 0x7f12185f

.field public static final split_tender_total_amount:I = 0x7f121860

.field public static final split_tender_total_subtitle:I = 0x7f121861

.field public static final split_tender_unavailable_message:I = 0x7f121862

.field public static final split_tender_unavailable_title:I = 0x7f121863

.field public static final split_ticket_move_item_plural:I = 0x7f121864

.field public static final split_ticket_move_item_singular:I = 0x7f121865

.field public static final split_ticket_name_and_ordinal:I = 0x7f121866

.field public static final split_ticket_new_ticket:I = 0x7f121867

.field public static final split_ticket_null_state:I = 0x7f121868

.field public static final split_ticket_print_all:I = 0x7f121869

.field public static final split_ticket_save_all:I = 0x7f12186a

.field public static final split_ticket_ticket_name_action_bar:I = 0x7f12186b

.field public static final spoc_version:I = 0x7f12186c

.field public static final spos_whats_new_tour_sales_reports_subtitle:I = 0x7f12186d

.field public static final spos_whats_new_tour_sales_reports_title:I = 0x7f12186e

.field public static final square_capital:I = 0x7f12186f

.field public static final square_capital_flex_plan_due_date:I = 0x7f121870

.field public static final square_capital_flex_plan_overdue_since:I = 0x7f121871

.field public static final square_capital_flex_plan_percent_paid:I = 0x7f121872

.field public static final square_capital_flex_status_current_plan:I = 0x7f121873

.field public static final square_capital_flex_status_financing_request:I = 0x7f121874

.field public static final square_capital_flex_status_no_offer:I = 0x7f121875

.field public static final square_capital_flex_status_offer:I = 0x7f121876

.field public static final square_capital_flex_status_previous_plan:I = 0x7f121877

.field public static final square_card:I = 0x7f121878

.field public static final square_card_enter_phone_number_help:I = 0x7f121879

.field public static final square_card_enter_phone_number_hint:I = 0x7f12187a

.field public static final square_card_enter_phone_number_message:I = 0x7f12187b

.field public static final square_card_enter_phone_number_submit_text:I = 0x7f12187c

.field public static final square_card_enter_phone_number_title:I = 0x7f12187d

.field public static final square_card_expiration_and_cvc_phrase:I = 0x7f12187e

.field public static final square_card_reset_pin_loading:I = 0x7f12187f

.field public static final square_card_reset_pin_success_message:I = 0x7f121880

.field public static final square_card_reset_pin_success_title:I = 0x7f121881

.field public static final square_card_submitting_phone_number:I = 0x7f121882

.field public static final square_card_two_factor_auth_enter_code_message:I = 0x7f121883

.field public static final square_card_two_factor_auth_sending_code:I = 0x7f121884

.field public static final square_card_two_factor_auth_verifying_code:I = 0x7f121885

.field public static final square_card_two_factor_error_expired_token_message:I = 0x7f121886

.field public static final square_card_two_factor_error_expired_token_title:I = 0x7f121887

.field public static final square_card_two_factor_error_generic_message:I = 0x7f121888

.field public static final square_card_two_factor_error_generic_title:I = 0x7f121889

.field public static final square_card_two_factor_error_invalid_token_message:I = 0x7f12188a

.field public static final square_card_two_factor_error_invalid_token_title:I = 0x7f12188b

.field public static final square_card_upsell_button_text:I = 0x7f12188c

.field public static final square_card_upsell_message:I = 0x7f12188d

.field public static final square_card_upsell_sample_name:I = 0x7f12188e

.field public static final square_card_upsell_title:I = 0x7f12188f

.field public static final square_dashboard:I = 0x7f121890

.field public static final square_fees:I = 0x7f121891

.field public static final square_fees_url:I = 0x7f121892

.field public static final square_go_dashboard:I = 0x7f121893

.field public static final square_market_url:I = 0x7f121894

.field public static final square_reader_bluetooth:I = 0x7f121895

.field public static final square_reader_bluetooth_info_page_name:I = 0x7f121896

.field public static final square_reader_bluetooth_info_page_name_short:I = 0x7f121897

.field public static final square_reader_chip:I = 0x7f121898

.field public static final square_reader_contactless_chip:I = 0x7f121899

.field public static final square_reader_help:I = 0x7f12189a

.field public static final square_reader_magstripe:I = 0x7f12189b

.field public static final square_reader_magstripe_fullname:I = 0x7f12189c

.field public static final square_readers:I = 0x7f12189d

.field public static final square_relative_url:I = 0x7f12189e

.field public static final square_seller_agreement:I = 0x7f12189f

.field public static final square_seller_agreement_url_au:I = 0x7f1218a0

.field public static final square_server_error_message:I = 0x7f1218a1

.field public static final square_server_error_title:I = 0x7f1218a2

.field public static final square_shop:I = 0x7f1218a3

.field public static final square_support:I = 0x7f1218a4

.field public static final square_unavailable_message:I = 0x7f1218a5

.field public static final square_unavailable_title:I = 0x7f1218a6

.field public static final square_url:I = 0x7f1218a7

.field public static final squareup_europe_limited_body:I = 0x7f1218a8

.field public static final squareup_europe_limited_title:I = 0x7f1218a9

.field public static final ssn_hint:I = 0x7f1218aa

.field public static final staging_base_url:I = 0x7f1218ab

.field public static final standalone_percent_character:I = 0x7f1218ac

.field public static final standard_units_list_create_custom_unit:I = 0x7f1218ad

.field public static final standard_units_list_create_custom_unit_with_name:I = 0x7f1218ae

.field public static final standard_units_list_create_unit:I = 0x7f1218af

.field public static final standard_units_list_family_label_area:I = 0x7f1218b0

.field public static final standard_units_list_family_label_length:I = 0x7f1218b1

.field public static final standard_units_list_family_label_time:I = 0x7f1218b2

.field public static final standard_units_list_family_label_volume:I = 0x7f1218b3

.field public static final standard_units_list_family_label_weight:I = 0x7f1218b4

.field public static final standard_units_list_no_search_results:I = 0x7f1218b5

.field public static final standard_units_list_search_bar_hint_text:I = 0x7f1218b6

.field public static final standard_units_list_unit_label:I = 0x7f1218b7

.field public static final start_accessibility_dialog_enable:I = 0x7f1218b8

.field public static final start_accessibility_dialog_message:I = 0x7f1218b9

.field public static final start_accessibility_dialog_title:I = 0x7f1218ba

.field public static final start_drawer_modal_text:I = 0x7f1218bb

.field public static final start_drawer_modal_title:I = 0x7f1218bc

.field public static final started_at_label:I = 0x7f1218bd

.field public static final starts_at_label:I = 0x7f1218be

.field public static final state_completed:I = 0x7f1218bf

.field public static final state_declined:I = 0x7f1218c0

.field public static final state_dispute_reversed:I = 0x7f1218c1

.field public static final state_dispute_won:I = 0x7f1218c2

.field public static final state_disputed:I = 0x7f1218c3

.field public static final state_hint:I = 0x7f1218c4

.field public static final state_or_territory_hint:I = 0x7f1218c5

.field public static final state_pending:I = 0x7f1218c6

.field public static final state_refunded:I = 0x7f1218c7

.field public static final state_voided:I = 0x7f1218c8

.field public static final status:I = 0x7f1218c9

.field public static final status_bar_notification_info_overflow:I = 0x7f1218ca

.field public static final stock:I = 0x7f1218cb

.field public static final stop_movie:I = 0x7f1218cc

.field public static final street:I = 0x7f1218cd

.field public static final submit:I = 0x7f1218ce

.field public static final submit_feedback:I = 0x7f1218cf

.field public static final submit_feedback_error_message:I = 0x7f1218d0

.field public static final submit_feedback_error_title:I = 0x7f1218d1

.field public static final submit_your_information_body:I = 0x7f1218d2

.field public static final submit_your_information_title:I = 0x7f1218d3

.field public static final submitted_feedback_success_message:I = 0x7f1218d4

.field public static final submitted_feedback_success_message_title:I = 0x7f1218d5

.field public static final submitting_feedback_spinner_messsage:I = 0x7f1218d6

.field public static final subtotal:I = 0x7f1218d7

.field public static final sundays:I = 0x7f1218d8

.field public static final support_center:I = 0x7f1218d9

.field public static final support_center_r12_help_url:I = 0x7f1218da

.field public static final support_center_r6_help_url:I = 0x7f1218db

.field public static final support_center_url:I = 0x7f1218dc

.field public static final support_title:I = 0x7f1218dd

.field public static final support_twitter:I = 0x7f1218de

.field public static final support_version:I = 0x7f1218df

.field public static final supported_hardware_url:I = 0x7f1218e0

.field public static final supported_reader_url:I = 0x7f1218e1

.field public static final swipe_card_to_charge:I = 0x7f1218e2

.field public static final swipe_chip_cards_enable_hint:I = 0x7f1218e3

.field public static final swipe_chip_cards_popup_button:I = 0x7f1218e4

.field public static final swipe_chip_cards_popup_for_more_information:I = 0x7f1218e5

.field public static final swipe_chip_cards_popup_liability_warning:I = 0x7f1218e6

.field public static final swipe_chip_cards_popup_liability_warning_text:I = 0x7f1218e7

.field public static final swipe_chip_cards_title:I = 0x7f1218e8

.field public static final swipe_chip_cards_toggle_off:I = 0x7f1218e9

.field public static final swipe_chip_cards_toggle_on:I = 0x7f1218ea

.field public static final swipe_chip_cards_toggle_text:I = 0x7f1218eb

.field public static final swipe_chip_cards_url:I = 0x7f1218ec

.field public static final swipe_credit_card:I = 0x7f1218ed

.field public static final swipe_failed_bad_swipe_message:I = 0x7f1218ee

.field public static final swipe_failed_bad_swipe_message_swipe_straight:I = 0x7f1218ef

.field public static final swipe_failed_bad_swipe_title_transactions_history:I = 0x7f1218f0

.field public static final swipe_failed_invalid_card_message:I = 0x7f1218f1

.field public static final swipe_failed_invalid_card_title:I = 0x7f1218f2

.field public static final swipe_failed_square_card_confirmation_message:I = 0x7f1218f3

.field public static final swipe_failed_square_card_confirmation_title:I = 0x7f1218f4

.field public static final swipe_only_hint:I = 0x7f1218f5

.field public static final switch_employee_tooltip:I = 0x7f1218f6

.field public static final switch_employee_tooltip_passcode:I = 0x7f1218f7

.field public static final switch_employee_tooltip_restricted:I = 0x7f1218f8

.field public static final switch_employee_tooltip_title:I = 0x7f1218f9

.field public static final switch_off:I = 0x7f1218fa

.field public static final switch_on:I = 0x7f1218fb

.field public static final system_permission_contacts_body:I = 0x7f1218fc

.field public static final system_permission_contacts_body_invoices:I = 0x7f1218fd

.field public static final system_permission_contacts_button:I = 0x7f1218fe

.field public static final system_permission_contacts_title:I = 0x7f1218ff

.field public static final system_permission_location_body:I = 0x7f121900

.field public static final system_permission_location_button:I = 0x7f121901

.field public static final system_permission_location_title:I = 0x7f121902

.field public static final system_permission_mic_body:I = 0x7f121903

.field public static final system_permission_mic_button:I = 0x7f121904

.field public static final system_permission_mic_title:I = 0x7f121905

.field public static final system_permission_phone_body:I = 0x7f121906

.field public static final system_permission_phone_button:I = 0x7f121907

.field public static final system_permission_phone_title:I = 0x7f121908

.field public static final system_permission_settings_button:I = 0x7f121909

.field public static final system_permission_storage_body:I = 0x7f12190a

.field public static final system_permission_storage_button:I = 0x7f12190b

.field public static final system_permission_storage_title:I = 0x7f12190c

.field public static final system_status:I = 0x7f12190d

.field public static final system_status_subtext:I = 0x7f12190e

.field public static final system_status_url:I = 0x7f12190f

.field public static final tag_percent_margin_key:I = 0x7f121910

.field public static final take_photo:I = 0x7f121911

.field public static final talk_back_go_to_accessibility_setting:I = 0x7f121912

.field public static final talk_back_please_close:I = 0x7f121913

.field public static final talk_back_please_close_header:I = 0x7f121914

.field public static final talk_back_warning_header:I = 0x7f121915

.field public static final talk_back_warning_message:I = 0x7f121916

.field public static final tap_get_help_to_view_messaging:I = 0x7f121917

.field public static final tap_insert_swipe:I = 0x7f121918

.field public static final tap_to_edit:I = 0x7f121919

.field public static final tap_to_preview_file:I = 0x7f12191a

.field public static final tap_to_retry:I = 0x7f12191b

.field public static final tap_to_set_color:I = 0x7f12191c

.field public static final tap_to_set_label:I = 0x7f12191d

.field public static final tap_to_view_conversation:I = 0x7f12191e

.field public static final tap_to_view_message_reply:I = 0x7f12191f

.field public static final tax_applicable_items:I = 0x7f121920

.field public static final tax_applicable_items_count_all:I = 0x7f121921

.field public static final tax_applicable_items_count_none:I = 0x7f121922

.field public static final tax_applicable_items_count_one:I = 0x7f121923

.field public static final tax_applicable_items_count_some:I = 0x7f121924

.field public static final tax_applicable_items_custom:I = 0x7f121925

.field public static final tax_applicable_items_select_all:I = 0x7f121926

.field public static final tax_applicable_items_select_none:I = 0x7f121927

.field public static final tax_applicable_services:I = 0x7f121928

.field public static final tax_applicable_services_count_all:I = 0x7f121929

.field public static final tax_applicable_services_count_none:I = 0x7f12192a

.field public static final tax_applicable_services_count_one:I = 0x7f12192b

.field public static final tax_applicable_services_count_some:I = 0x7f12192c

.field public static final tax_basis_help_text:I = 0x7f12192d

.field public static final tax_basis_help_url:I = 0x7f12192e

.field public static final tax_breakdown_table_gross_header:I = 0x7f12192f

.field public static final tax_breakdown_table_net_header:I = 0x7f121930

.field public static final tax_breakdown_table_vat_header:I = 0x7f121931

.field public static final tax_breakdown_table_vat_rate_header:I = 0x7f121932

.field public static final tax_breakdown_table_vat_rate_name_and_percentage:I = 0x7f121933

.field public static final tax_count_one:I = 0x7f121934

.field public static final tax_count_two_or_more:I = 0x7f121935

.field public static final tax_count_zero:I = 0x7f121936

.field public static final tax_create_tax:I = 0x7f121937

.field public static final tax_delete_button_confirm_stage_text:I = 0x7f121938

.field public static final tax_edit:I = 0x7f121939

.field public static final tax_error_missing_name_message:I = 0x7f12193a

.field public static final tax_error_missing_name_title:I = 0x7f12193b

.field public static final tax_error_missing_rate_message:I = 0x7f12193c

.field public static final tax_error_missing_rate_title:I = 0x7f12193d

.field public static final tax_included:I = 0x7f12193e

.field public static final tax_included_format:I = 0x7f12193f

.field public static final tax_included_in_help_text:I = 0x7f121940

.field public static final tax_included_in_multiple_prices:I = 0x7f121941

.field public static final tax_included_in_one_variable_price:I = 0x7f121942

.field public static final tax_inclusion_helper:I = 0x7f121943

.field public static final tax_inclusion_type_excluded:I = 0x7f121944

.field public static final tax_inclusion_type_included:I = 0x7f121945

.field public static final tax_item_pricing:I = 0x7f121946

.field public static final tax_list_help:I = 0x7f121947

.field public static final tax_name:I = 0x7f121948

.field public static final tax_name_default:I = 0x7f121949

.field public static final tax_not_modifiable_help:I = 0x7f12194a

.field public static final tax_off:I = 0x7f12194b

.field public static final tax_percentage_dialog_title:I = 0x7f12194c

.field public static final tax_percentage_row_name:I = 0x7f12194d

.field public static final tax_taxes:I = 0x7f12194e

.field public static final tender:I = 0x7f12194f

.field public static final tender_adjustment:I = 0x7f121950

.field public static final tender_adjustment_uppercase:I = 0x7f121951

.field public static final tender_amount:I = 0x7f121952

.field public static final tender_unknown:I = 0x7f121953

.field public static final tender_unknown_uppercase:I = 0x7f121954

.field public static final terminal_damaged_message:I = 0x7f121955

.field public static final terminal_damaged_title:I = 0x7f121956

.field public static final terms:I = 0x7f121957

.field public static final text_0:I = 0x7f121958

.field public static final text_00:I = 0x7f121959

.field public static final text_1:I = 0x7f12195a

.field public static final text_2:I = 0x7f12195b

.field public static final text_3:I = 0x7f12195c

.field public static final text_4:I = 0x7f12195d

.field public static final text_5:I = 0x7f12195e

.field public static final text_6:I = 0x7f12195f

.field public static final text_7:I = 0x7f121960

.field public static final text_8:I = 0x7f121961

.field public static final text_9:I = 0x7f121962

.field public static final text_backspace:I = 0x7f121963

.field public static final text_cancel:I = 0x7f121964

.field public static final text_clear:I = 0x7f121965

.field public static final text_decimal:I = 0x7f121966

.field public static final text_password_hide:I = 0x7f121967

.field public static final text_password_hint:I = 0x7f121968

.field public static final text_password_show:I = 0x7f121969

.field public static final text_plus:I = 0x7f12196a

.field public static final text_skip:I = 0x7f12196b

.field public static final text_submit:I = 0x7f12196c

.field public static final third_party_card_disclaimer:I = 0x7f12196d

.field public static final thursdays:I = 0x7f12196e

.field public static final ticket_reprint_label:I = 0x7f12196f

.field public static final ticket_template_delete_content_description:I = 0x7f121970

.field public static final time_ago:I = 0x7f121971

.field public static final time_day:I = 0x7f121972

.field public static final time_day_ago:I = 0x7f121973

.field public static final time_days:I = 0x7f121974

.field public static final time_days_ago:I = 0x7f121975

.field public static final time_hour:I = 0x7f121976

.field public static final time_hour_long:I = 0x7f121977

.field public static final time_hour_min:I = 0x7f121978

.field public static final time_hours_long:I = 0x7f121979

.field public static final time_just_now:I = 0x7f12197a

.field public static final time_min:I = 0x7f12197b

.field public static final time_mins:I = 0x7f12197c

.field public static final time_sec:I = 0x7f12197d

.field public static final time_tracking_settings_enable_switch_description:I = 0x7f12197e

.field public static final time_tracking_settings_section_title:I = 0x7f12197f

.field public static final time_week:I = 0x7f121980

.field public static final time_zone_daylight:I = 0x7f121981

.field public static final time_zone_offset:I = 0x7f121982

.field public static final time_zone_standard:I = 0x7f121983

.field public static final timecard_add_notes:I = 0x7f121984

.field public static final timecard_add_or_edit_notes_save_button:I = 0x7f121985

.field public static final timecard_clock_in_and_continue:I = 0x7f121986

.field public static final timecard_clock_in_and_continue_glyph_title:I = 0x7f121987

.field public static final timecard_clock_in_and_continue_success_title:I = 0x7f121988

.field public static final timecard_clock_in_out_button:I = 0x7f121989

.field public static final timecard_clock_in_title:I = 0x7f12198a

.field public static final timecard_clock_out_header:I = 0x7f12198b

.field public static final timecard_clock_out_header_job:I = 0x7f12198c

.field public static final timecard_clock_out_summary_footer_total:I = 0x7f12198d

.field public static final timecard_clock_out_summary_mw_header_0_shift:I = 0x7f12198e

.field public static final timecard_clock_out_summary_mw_header_1_clocked_in:I = 0x7f12198f

.field public static final timecard_clock_out_summary_mw_header_2_clocked_out:I = 0x7f121990

.field public static final timecard_clock_out_summary_mw_header_3_paid_hours:I = 0x7f121991

.field public static final timecard_clock_out_title:I = 0x7f121992

.field public static final timecard_clocked_in_summary:I = 0x7f121993

.field public static final timecard_clocked_in_summary_job:I = 0x7f121994

.field public static final timecard_continue_to_register:I = 0x7f121995

.field public static final timecard_edit_notes:I = 0x7f121996

.field public static final timecard_error_no_internet_connection:I = 0x7f121997

.field public static final timecard_error_title:I = 0x7f121998

.field public static final timecard_message:I = 0x7f121999

.field public static final timecard_no_job_title:I = 0x7f12199a

.field public static final timecard_print_receipt_summary:I = 0x7f12199b

.field public static final timecard_punching_title:I = 0x7f12199c

.field public static final timecard_success_clock_in_out_button:I = 0x7f12199d

.field public static final timecard_total_hours_plural:I = 0x7f12199e

.field public static final timecard_total_hours_singular:I = 0x7f12199f

.field public static final timecard_total_minutes_plural:I = 0x7f1219a0

.field public static final timecard_total_minutes_singular:I = 0x7f1219a1

.field public static final timecard_total_minutes_zero:I = 0x7f1219a2

.field public static final timecard_view_notes:I = 0x7f1219a3

.field public static final timecard_view_notes_save_button:I = 0x7f1219a4

.field public static final timecards_breaks_header:I = 0x7f1219a5

.field public static final timecards_clock_in:I = 0x7f1219a6

.field public static final timecards_clock_out:I = 0x7f1219a7

.field public static final timecards_print_timestamp:I = 0x7f1219a8

.field public static final timecards_receipt_title:I = 0x7f1219a9

.field public static final timecards_shift_default_title:I = 0x7f1219aa

.field public static final timecards_shift_report_header:I = 0x7f1219ab

.field public static final timecards_shifts_header:I = 0x7f1219ac

.field public static final timecards_total_hours:I = 0x7f1219ad

.field public static final tip:I = 0x7f1219ae

.field public static final tip_amount:I = 0x7f1219af

.field public static final tip_amount_first:I = 0x7f1219b0

.field public static final tip_amount_second:I = 0x7f1219b1

.field public static final tip_amount_third:I = 0x7f1219b2

.field public static final tip_for_card:I = 0x7f1219b3

.field public static final tip_hint_collect:I = 0x7f1219b4

.field public static final tip_hint_custom:I = 0x7f1219b5

.field public static final tip_hint_separate_screen:I = 0x7f1219b6

.field public static final tip_hint_smart:I = 0x7f1219b7

.field public static final tip_manual_label:I = 0x7f1219b8

.field public static final tip_manual_label_short:I = 0x7f1219b9

.field public static final tip_message_card_added:I = 0x7f1219ba

.field public static final tip_message_card_inserted:I = 0x7f1219bb

.field public static final tip_message_card_line:I = 0x7f1219bc

.field public static final tip_message_see_cashier:I = 0x7f1219bd

.field public static final tip_off:I = 0x7f1219be

.field public static final tip_on:I = 0x7f1219bf

.field public static final tip_percentage:I = 0x7f1219c0

.field public static final tip_post_tax_label:I = 0x7f1219c1

.field public static final tip_pre_tax_label:I = 0x7f1219c2

.field public static final tip_row_format:I = 0x7f1219c3

.field public static final tip_separate_screen_label:I = 0x7f1219c4

.field public static final tip_separate_screen_label_short:I = 0x7f1219c5

.field public static final tip_set_percentages:I = 0x7f1219c6

.field public static final tip_set_percentages_short:I = 0x7f1219c7

.field public static final tip_smart_amounts:I = 0x7f1219c8

.field public static final tip_smart_amounts_short:I = 0x7f1219c9

.field public static final tip_smart_label:I = 0x7f1219ca

.field public static final tips_collect_label:I = 0x7f1219cb

.field public static final tips_title:I = 0x7f1219cc

.field public static final title:I = 0x7f1219cd

.field public static final title_atm_withdrawal:I = 0x7f1219ce

.field public static final title_card_payment:I = 0x7f1219cf

.field public static final title_card_processing_adjustment:I = 0x7f1219d0

.field public static final title_card_processing_sales:I = 0x7f1219d1

.field public static final title_debit_card_transfer_in:I = 0x7f1219d2

.field public static final title_hint:I = 0x7f1219d3

.field public static final title_instant_transfer_out:I = 0x7f1219d4

.field public static final title_payroll:I = 0x7f1219d5

.field public static final title_promotional_reward:I = 0x7f1219d6

.field public static final title_standard_transfer_out:I = 0x7f1219d7

.field public static final title_unknown:I = 0x7f1219d8

.field public static final titlecase_activity:I = 0x7f1219d9

.field public static final titlecase_balance_applet_name:I = 0x7f1219da

.field public static final titlecase_deposits:I = 0x7f1219db

.field public static final titlecase_invoices:I = 0x7f1219dc

.field public static final titlecase_items:I = 0x7f1219dd

.field public static final titlecase_items_appointments:I = 0x7f1219de

.field public static final titlecase_recurring_series:I = 0x7f1219df

.field public static final titlecase_register:I = 0x7f1219e0

.field public static final titlecase_reports:I = 0x7f1219e1

.field public static final titlecase_settings:I = 0x7f1219e2

.field public static final titlecase_support:I = 0x7f1219e3

.field public static final tms_invalid_contact_support:I = 0x7f1219e4

.field public static final tms_invalid_dismiss:I = 0x7f1219e5

.field public static final tms_invalid_message:I = 0x7f1219e6

.field public static final tms_invalid_title:I = 0x7f1219e7

.field public static final today:I = 0x7f1219e8

.field public static final total:I = 0x7f1219e9

.field public static final tour_learn_more_backoffice_subtitle:I = 0x7f1219ea

.field public static final tour_learn_more_backoffice_title:I = 0x7f1219eb

.field public static final tour_learn_more_pos_subtitle:I = 0x7f1219ec

.field public static final tour_learn_more_pos_title:I = 0x7f1219ed

.field public static final tour_learn_more_reader_subtitle:I = 0x7f1219ee

.field public static final tour_learn_more_reader_title:I = 0x7f1219ef

.field public static final tour_learn_more_simple_subtitle:I = 0x7f1219f0

.field public static final tour_learn_more_simple_title:I = 0x7f1219f1

.field public static final track_time_upsell_description:I = 0x7f1219f2

.field public static final track_time_upsell_headline:I = 0x7f1219f3

.field public static final track_time_upsell_link:I = 0x7f1219f4

.field public static final track_time_upsell_title:I = 0x7f1219f5

.field public static final transactions_history:I = 0x7f1219f6

.field public static final transactions_history_activity:I = 0x7f1219f7

.field public static final transactions_url:I = 0x7f1219f8

.field public static final transfer_arrives_instantly:I = 0x7f1219f9

.field public static final transfer_arrives_one_to_two_business_days:I = 0x7f1219fa

.field public static final transfer_arrives_title:I = 0x7f1219fb

.field public static final transfer_available_balance:I = 0x7f1219fc

.field public static final transfer_breakdown_uppercase:I = 0x7f1219fd

.field public static final transfer_complete:I = 0x7f1219fe

.field public static final transfer_deposit_amount:I = 0x7f1219ff

.field public static final transfer_deposit_summary_uppercase:I = 0x7f121a00

.field public static final transfer_deposit_to:I = 0x7f121a01

.field public static final transfer_ending_balance:I = 0x7f121a02

.field public static final transfer_in_progress:I = 0x7f121a03

.field public static final transfer_instant_deposit_fee:I = 0x7f121a04

.field public static final transfer_reports_help_url:I = 0x7f121a05

.field public static final transfer_result_error_message:I = 0x7f121a06

.field public static final transfer_result_error_title:I = 0x7f121a07

.field public static final transfer_result_instant_message_with_bank:I = 0x7f121a08

.field public static final transfer_result_instant_message_with_card:I = 0x7f121a09

.field public static final transfer_result_instant_title:I = 0x7f121a0a

.field public static final transfer_result_processing:I = 0x7f121a0b

.field public static final transfer_result_standard_message:I = 0x7f121a0c

.field public static final transfer_result_standard_title:I = 0x7f121a0d

.field public static final transfer_speed_hint:I = 0x7f121a0e

.field public static final transfer_speed_instant:I = 0x7f121a0f

.field public static final transfer_speed_instant_fee_amount:I = 0x7f121a10

.field public static final transfer_speed_instant_fee_rate:I = 0x7f121a11

.field public static final transfer_speed_standard:I = 0x7f121a12

.field public static final transfer_speed_uppercase:I = 0x7f121a13

.field public static final transit_number:I = 0x7f121a14

.field public static final translation_type_auto_gratuity:I = 0x7f121a15

.field public static final translation_type_cash_refund:I = 0x7f121a16

.field public static final translation_type_change:I = 0x7f121a17

.field public static final translation_type_credit:I = 0x7f121a18

.field public static final translation_type_custom_amount_item:I = 0x7f121a19

.field public static final translation_type_custom_amount_item_variation:I = 0x7f121a1a

.field public static final translation_type_default_comp_reason:I = 0x7f121a1b

.field public static final translation_type_default_device:I = 0x7f121a1c

.field public static final translation_type_default_discount:I = 0x7f121a1d

.field public static final translation_type_default_item:I = 0x7f121a1e

.field public static final translation_type_default_item_modifier:I = 0x7f121a1f

.field public static final translation_type_default_item_variation:I = 0x7f121a20

.field public static final translation_type_default_measurement_unit:I = 0x7f121a21

.field public static final translation_type_default_modifier_set:I = 0x7f121a22

.field public static final translation_type_default_order_source_name:I = 0x7f121a23

.field public static final translation_type_default_sales_tax:I = 0x7f121a24

.field public static final translation_type_default_surcharge:I = 0x7f121a25

.field public static final translation_type_default_tip:I = 0x7f121a26

.field public static final translation_type_employee_role_account_owner_role:I = 0x7f121a27

.field public static final translation_type_employee_role_mobile_staff_role:I = 0x7f121a28

.field public static final translation_type_employee_role_owner_role:I = 0x7f121a29

.field public static final translation_type_inclusive_tax:I = 0x7f121a2a

.field public static final translation_type_no_category:I = 0x7f121a2b

.field public static final translation_type_no_comp:I = 0x7f121a2c

.field public static final translation_type_no_comp_reason:I = 0x7f121a2d

.field public static final translation_type_no_contact_token_name:I = 0x7f121a2e

.field public static final translation_type_no_conversational_mode:I = 0x7f121a2f

.field public static final translation_type_no_device_credential:I = 0x7f121a30

.field public static final translation_type_no_dining_option:I = 0x7f121a31

.field public static final translation_type_no_discount:I = 0x7f121a32

.field public static final translation_type_no_employee:I = 0x7f121a33

.field public static final translation_type_no_employee_role:I = 0x7f121a34

.field public static final translation_type_no_gift_card:I = 0x7f121a35

.field public static final translation_type_no_menu:I = 0x7f121a36

.field public static final translation_type_no_mobile_staff:I = 0x7f121a37

.field public static final translation_type_no_modifier:I = 0x7f121a38

.field public static final translation_type_no_modifier_set:I = 0x7f121a39

.field public static final translation_type_no_payment_source_name:I = 0x7f121a3a

.field public static final translation_type_no_reporting_group:I = 0x7f121a3b

.field public static final translation_type_no_subunit:I = 0x7f121a3c

.field public static final translation_type_no_surcharge:I = 0x7f121a3d

.field public static final translation_type_no_tax:I = 0x7f121a3e

.field public static final translation_type_no_ticket_group:I = 0x7f121a3f

.field public static final translation_type_no_ticket_template_custom_check:I = 0x7f121a40

.field public static final translation_type_no_ticket_template_new_sales:I = 0x7f121a41

.field public static final translation_type_no_vendor:I = 0x7f121a42

.field public static final translation_type_no_void_reason:I = 0x7f121a43

.field public static final translation_type_order_source_appointments:I = 0x7f121a44

.field public static final translation_type_order_source_billing:I = 0x7f121a45

.field public static final translation_type_order_source_capital:I = 0x7f121a46

.field public static final translation_type_order_source_cash:I = 0x7f121a47

.field public static final translation_type_order_source_egifting:I = 0x7f121a48

.field public static final translation_type_order_source_external_api:I = 0x7f121a49

.field public static final translation_type_order_source_invoices:I = 0x7f121a4a

.field public static final translation_type_order_source_market:I = 0x7f121a4b

.field public static final translation_type_order_source_online_store:I = 0x7f121a4c

.field public static final translation_type_order_source_order:I = 0x7f121a4d

.field public static final translation_type_order_source_payroll:I = 0x7f121a4e

.field public static final translation_type_order_source_register:I = 0x7f121a4f

.field public static final translation_type_order_source_register_hardware:I = 0x7f121a50

.field public static final translation_type_order_source_starbucks:I = 0x7f121a51

.field public static final translation_type_order_source_store:I = 0x7f121a52

.field public static final translation_type_order_source_terminal:I = 0x7f121a53

.field public static final translation_type_order_source_white_label:I = 0x7f121a54

.field public static final translation_type_refund:I = 0x7f121a55

.field public static final translation_type_surcharge:I = 0x7f121a56

.field public static final translation_type_swedish_rounding:I = 0x7f121a57

.field public static final translation_type_ticket_group_name:I = 0x7f121a58

.field public static final translation_type_ticket_name:I = 0x7f121a59

.field public static final translation_type_unitemized_tax:I = 0x7f121a5a

.field public static final translation_type_unknown:I = 0x7f121a5b

.field public static final translation_type_unknown_payment_source_name:I = 0x7f121a5c

.field public static final translation_type_unknown_vendor:I = 0x7f121a5d

.field public static final troubleshoot_pos:I = 0x7f121a5e

.field public static final troubleshoot_pos_url:I = 0x7f121a5f

.field public static final troubleshooting:I = 0x7f121a60

.field public static final troubleshooting_customer_display_connection:I = 0x7f121a61

.field public static final troubleshooting_customer_display_connection_link:I = 0x7f121a62

.field public static final troubleshooting_hardware_connection:I = 0x7f121a63

.field public static final troubleshooting_hardware_connection_link:I = 0x7f121a64

.field public static final troubleshooting_network_connection:I = 0x7f121a65

.field public static final troubleshooting_network_connection_link:I = 0x7f121a66

.field public static final try_again:I = 0x7f121a67

.field public static final try_offline_mode:I = 0x7f121a68

.field public static final try_offline_mode_hint:I = 0x7f121a69

.field public static final tuesdays:I = 0x7f121a6a

.field public static final turn_off_reminders_body:I = 0x7f121a6b

.field public static final turn_off_reminders_title:I = 0x7f121a6c

.field public static final tutorial_button_ok:I = 0x7f121a6d

.field public static final tutorial_button_skip:I = 0x7f121a6e

.field public static final tutorial_content_description:I = 0x7f121a6f

.field public static final tutorial_fi_end_done:I = 0x7f121a70

.field public static final tutorial_fp_content_action_or_choose:I = 0x7f121a71

.field public static final tutorial_fp_content_action_or_enable:I = 0x7f121a72

.field public static final tutorial_fp_content_actions_connect:I = 0x7f121a73

.field public static final tutorial_fp_content_actions_dip_contactless:I = 0x7f121a74

.field public static final tutorial_fp_content_actions_dip_swipe:I = 0x7f121a75

.field public static final tutorial_fp_content_actions_dip_swipe_contactless:I = 0x7f121a76

.field public static final tutorial_fp_content_actions_dip_tap:I = 0x7f121a77

.field public static final tutorial_fp_content_actions_swipe:I = 0x7f121a78

.field public static final tutorial_fp_content_actions_swipe_dip:I = 0x7f121a79

.field public static final tutorial_fp_content_actions_swipe_dip_contactless:I = 0x7f121a7a

.field public static final tutorial_fp_content_actions_swipe_dip_tap:I = 0x7f121a7b

.field public static final tutorial_fp_content_actions_tap_dip:I = 0x7f121a7c

.field public static final tutorial_fp_content_actions_tap_dip_swipe:I = 0x7f121a7d

.field public static final tutorial_fp_content_cash_payment_screen:I = 0x7f121a7e

.field public static final tutorial_fp_content_charge_less:I = 0x7f121a7f

.field public static final tutorial_fp_content_charge_more:I = 0x7f121a80

.field public static final tutorial_fp_content_choose_or_create_ticket:I = 0x7f121a81

.field public static final tutorial_fp_content_cnp_entry:I = 0x7f121a82

.field public static final tutorial_fp_content_continue_payment:I = 0x7f121a83

.field public static final tutorial_fp_content_invoice:I = 0x7f121a84

.field public static final tutorial_fp_content_new_sale:I = 0x7f121a85

.field public static final tutorial_fp_content_receipt:I = 0x7f121a86

.field public static final tutorial_fp_content_sign:I = 0x7f121a87

.field public static final tutorial_fp_content_start:I = 0x7f121a88

.field public static final tutorial_fp_content_start_items_keypad_phone_landscape:I = 0x7f121a89

.field public static final tutorial_fp_content_start_items_keypad_phone_portrait:I = 0x7f121a8a

.field public static final tutorial_fp_content_start_items_keypad_tablet_landscape:I = 0x7f121a8b

.field public static final tutorial_fp_content_start_items_keypad_tablet_portrait:I = 0x7f121a8c

.field public static final tutorial_fp_content_start_items_library_no_items_phone_landscape:I = 0x7f121a8d

.field public static final tutorial_fp_content_start_items_library_no_items_phone_portrait:I = 0x7f121a8e

.field public static final tutorial_fp_content_start_items_library_no_items_tablet_landscape:I = 0x7f121a8f

.field public static final tutorial_fp_content_start_items_library_no_items_tablet_portrait:I = 0x7f121a90

.field public static final tutorial_fp_content_start_items_library_phone_landscape:I = 0x7f121a91

.field public static final tutorial_fp_content_start_items_library_phone_portrait:I = 0x7f121a92

.field public static final tutorial_fp_content_start_items_library_tablet_landscape:I = 0x7f121a93

.field public static final tutorial_fp_content_start_items_library_tablet_portrait:I = 0x7f121a94

.field public static final tutorial_fp_content_tap_cash_phone:I = 0x7f121a95

.field public static final tutorial_fp_content_tap_cash_tablet:I = 0x7f121a96

.field public static final tutorial_fp_content_tap_charge:I = 0x7f121a97

.field public static final tutorial_fp_content_tipping:I = 0x7f121a98

.field public static final tutorial_fp_end_content_failed_verify:I = 0x7f121a99

.field public static final tutorial_fp_end_content_history:I = 0x7f121a9a

.field public static final tutorial_fp_end_content_no_bank:I = 0x7f121a9b

.field public static final tutorial_fp_end_content_pending:I = 0x7f121a9c

.field public static final tutorial_fp_end_content_verified:I = 0x7f121a9d

.field public static final tutorial_fp_end_done:I = 0x7f121a9e

.field public static final tutorial_fp_end_link_bank_account:I = 0x7f121a9f

.field public static final tutorial_fp_end_link_history:I = 0x7f121aa0

.field public static final tutorial_fp_end_title:I = 0x7f121aa1

.field public static final tutorial_fp_exit_content:I = 0x7f121aa2

.field public static final tutorial_fp_exit_continue:I = 0x7f121aa3

.field public static final tutorial_fp_exit_end:I = 0x7f121aa4

.field public static final tutorial_fp_exit_title:I = 0x7f121aa5

.field public static final tutorial_p0_message:I = 0x7f121aa6

.field public static final tutorial_p0_title:I = 0x7f121aa7

.field public static final tutorial_p1_message:I = 0x7f121aa8

.field public static final tutorial_p1_title:I = 0x7f121aa9

.field public static final tutorial_p2_message:I = 0x7f121aaa

.field public static final tutorial_p2_title:I = 0x7f121aab

.field public static final tutorial_p3_message:I = 0x7f121aac

.field public static final tutorial_p3_title:I = 0x7f121aad

.field public static final tutorial_p4_message:I = 0x7f121aae

.field public static final tutorial_p4_title:I = 0x7f121aaf

.field public static final tutorial_p5_message:I = 0x7f121ab0

.field public static final tutorial_p5_title:I = 0x7f121ab1

.field public static final tutorial_p6_message:I = 0x7f121ab2

.field public static final tutorial_p6_title:I = 0x7f121ab3

.field public static final tutorial_progress:I = 0x7f121ab4

.field public static final tutorials:I = 0x7f121ab5

.field public static final two_factor_auth_url:I = 0x7f121ab6

.field public static final two_factor_contact_support_url:I = 0x7f121ab7

.field public static final two_factor_details_picker_authentication_app:I = 0x7f121ab8

.field public static final two_factor_details_picker_sms:I = 0x7f121ab9

.field public static final two_factor_enroll_google_auth_code_hint:I = 0x7f121aba

.field public static final two_factor_enroll_google_auth_code_subtitle:I = 0x7f121abb

.field public static final two_factor_enroll_google_auth_code_title:I = 0x7f121abc

.field public static final two_factor_enroll_google_auth_learn_more:I = 0x7f121abd

.field public static final two_factor_enroll_google_auth_learn_more_text:I = 0x7f121abe

.field public static final two_factor_enroll_google_auth_qr_hint:I = 0x7f121abf

.field public static final two_factor_enroll_google_auth_qr_subtitle:I = 0x7f121ac0

.field public static final two_factor_enroll_hint:I = 0x7f121ac1

.field public static final two_factor_enroll_skip:I = 0x7f121ac2

.field public static final two_factor_enroll_skip_for_now:I = 0x7f121ac3

.field public static final two_factor_enroll_sms_hint:I = 0x7f121ac4

.field public static final two_factor_enroll_subtitle:I = 0x7f121ac5

.field public static final two_factor_enroll_title:I = 0x7f121ac6

.field public static final two_factor_google_auth_cant_scan_barcode:I = 0x7f121ac7

.field public static final two_factor_google_auth_copy_code:I = 0x7f121ac8

.field public static final two_factor_resend_code:I = 0x7f121ac9

.field public static final two_factor_resend_code_pending:I = 0x7f121aca

.field public static final two_factor_resend_code_toast:I = 0x7f121acb

.field public static final two_factor_sms_picker_subtitle:I = 0x7f121acc

.field public static final two_factor_sms_picker_title:I = 0x7f121acd

.field public static final two_factor_spinner_title_enrolling:I = 0x7f121ace

.field public static final two_factor_spinner_title_signing_in:I = 0x7f121acf

.field public static final two_factor_spinner_title_verifying_code:I = 0x7f121ad0

.field public static final two_factor_use_sms:I = 0x7f121ad1

.field public static final two_factor_verification_code:I = 0x7f121ad2

.field public static final two_factor_verification_google_auth_contact_support:I = 0x7f121ad3

.field public static final two_factor_verification_google_auth_hint:I = 0x7f121ad4

.field public static final two_factor_verification_google_auth_subtitle:I = 0x7f121ad5

.field public static final two_factor_verification_google_auth_support:I = 0x7f121ad6

.field public static final two_factor_verification_google_auth_title:I = 0x7f121ad7

.field public static final two_factor_verification_remember_this_device:I = 0x7f121ad8

.field public static final two_factor_verification_sms_hint:I = 0x7f121ad9

.field public static final two_factor_verification_sms_subtitle:I = 0x7f121ada

.field public static final two_factor_verification_sms_title:I = 0x7f121adb

.field public static final two_factor_verify:I = 0x7f121adc

.field public static final type:I = 0x7f121add

.field public static final unable_to_change_expense_body:I = 0x7f121ade

.field public static final unable_to_change_expense_title:I = 0x7f121adf

.field public static final unable_to_load_image:I = 0x7f121ae0

.field public static final unable_to_process_chip_cards:I = 0x7f121ae1

.field public static final unapplied_discount_row_title_choose_item:I = 0x7f121ae2

.field public static final unarchive_invoice:I = 0x7f121ae3

.field public static final understanding_your_register:I = 0x7f121ae4

.field public static final understanding_your_register_subtext:I = 0x7f121ae5

.field public static final underwriting_error_explanation:I = 0x7f121ae6

.field public static final underwriting_error_title:I = 0x7f121ae7

.field public static final unified_activity_section_label:I = 0x7f121ae8

.field public static final unit_detail_searchable_list_custom_label:I = 0x7f121ae9

.field public static final unit_label:I = 0x7f121aea

.field public static final unit_number_limit_help_text:I = 0x7f121aeb

.field public static final unit_selection_create_unit_button_text:I = 0x7f121aec

.field public static final unit_selection_help_text:I = 0x7f121aed

.field public static final unit_type_hint_url:I = 0x7f121aee

.field public static final unread_notification_dot_content_description:I = 0x7f121aef

.field public static final upcoming_deposits_uppercase:I = 0x7f121af0

.field public static final update:I = 0x7f121af1

.field public static final update_image:I = 0x7f121af2

.field public static final update_invoice:I = 0x7f121af3

.field public static final update_square_register_message:I = 0x7f121af4

.field public static final update_square_vertical:I = 0x7f121af5

.field public static final updating_r12_message:I = 0x7f121af6

.field public static final updating_r12_title:I = 0x7f121af7

.field public static final upload:I = 0x7f121af8

.field public static final upload_diagnostics_data_failure:I = 0x7f121af9

.field public static final upload_diagnostics_data_success:I = 0x7f121afa

.field public static final upload_failed_default:I = 0x7f121afb

.field public static final upload_file_invoice_error:I = 0x7f121afc

.field public static final upload_file_invoice_progress:I = 0x7f121afd

.field public static final upload_file_invoice_successful:I = 0x7f121afe

.field public static final upload_success:I = 0x7f121aff

.field public static final upload_support_ledger:I = 0x7f121b00

.field public static final upload_transaction_ledger_empty:I = 0x7f121b01

.field public static final upload_transaction_ledger_failure:I = 0x7f121b02

.field public static final upload_transaction_ledger_success:I = 0x7f121b03

.field public static final uploaded_at_time_stamp:I = 0x7f121b04

.field public static final uppercase_account:I = 0x7f121b05

.field public static final uppercase_amount_to_refund:I = 0x7f121b06

.field public static final uppercase_application:I = 0x7f121b07

.field public static final uppercase_balance_applet_name:I = 0x7f121b08

.field public static final uppercase_bill_to:I = 0x7f121b09

.field public static final uppercase_card:I = 0x7f121b0a

.field public static final uppercase_card_number:I = 0x7f121b0b

.field public static final uppercase_card_on_file:I = 0x7f121b0c

.field public static final uppercase_cards_on_file:I = 0x7f121b0d

.field public static final uppercase_cart_dining_option_header:I = 0x7f121b0e

.field public static final uppercase_cart_discount_header:I = 0x7f121b0f

.field public static final uppercase_cart_item_description_header:I = 0x7f121b10

.field public static final uppercase_cart_notes_header:I = 0x7f121b11

.field public static final uppercase_cart_quantity_header:I = 0x7f121b12

.field public static final uppercase_cart_quantity_header_area:I = 0x7f121b13

.field public static final uppercase_cart_quantity_header_length:I = 0x7f121b14

.field public static final uppercase_cart_quantity_header_volume:I = 0x7f121b15

.field public static final uppercase_cart_quantity_header_weight:I = 0x7f121b16

.field public static final uppercase_cart_taxes_header:I = 0x7f121b17

.field public static final uppercase_cart_variable_price_header:I = 0x7f121b18

.field public static final uppercase_checkout_link:I = 0x7f121b19

.field public static final uppercase_choose_between_counts:I = 0x7f121b1a

.field public static final uppercase_choose_count:I = 0x7f121b1b

.field public static final uppercase_choose_label_color:I = 0x7f121b1c

.field public static final uppercase_choose_many:I = 0x7f121b1d

.field public static final uppercase_choose_one:I = 0x7f121b1e

.field public static final uppercase_choose_up_to_count:I = 0x7f121b1f

.field public static final uppercase_choose_up_to_one:I = 0x7f121b20

.field public static final uppercase_closed_disputes:I = 0x7f121b21

.field public static final uppercase_communication:I = 0x7f121b22

.field public static final uppercase_custom_percentage_amounts:I = 0x7f121b23

.field public static final uppercase_customer_info:I = 0x7f121b24

.field public static final uppercase_drawer_report_summary:I = 0x7f121b25

.field public static final uppercase_edit_invoice_details_title_id:I = 0x7f121b26

.field public static final uppercase_exchange:I = 0x7f121b27

.field public static final uppercase_file_attachments:I = 0x7f121b28

.field public static final uppercase_file_name:I = 0x7f121b29

.field public static final uppercase_frequently_asked_questions:I = 0x7f121b2a

.field public static final uppercase_getting_started:I = 0x7f121b2b

.field public static final uppercase_gift_card:I = 0x7f121b2c

.field public static final uppercase_gift_card_on_file:I = 0x7f121b2d

.field public static final uppercase_header_account_holder_name:I = 0x7f121b2e

.field public static final uppercase_header_account_information:I = 0x7f121b2f

.field public static final uppercase_header_account_type:I = 0x7f121b30

.field public static final uppercase_header_bank_account_information:I = 0x7f121b31

.field public static final uppercase_header_basic_info:I = 0x7f121b32

.field public static final uppercase_header_business_address:I = 0x7f121b33

.field public static final uppercase_header_business_name:I = 0x7f121b34

.field public static final uppercase_header_choose_account_type:I = 0x7f121b35

.field public static final uppercase_header_contact_information:I = 0x7f121b36

.field public static final uppercase_header_country:I = 0x7f121b37

.field public static final uppercase_header_date_of_birth:I = 0x7f121b38

.field public static final uppercase_header_do_you_have_ein:I = 0x7f121b39

.field public static final uppercase_header_estimated_revenue:I = 0x7f121b3a

.field public static final uppercase_header_last_4_ssn:I = 0x7f121b3b

.field public static final uppercase_header_last_4_ssn_with_itin:I = 0x7f121b3c

.field public static final uppercase_header_profile_image:I = 0x7f121b3d

.field public static final uppercase_header_ssn:I = 0x7f121b3e

.field public static final uppercase_header_what_is_your_ein:I = 0x7f121b3f

.field public static final uppercase_header_your_business_address:I = 0x7f121b40

.field public static final uppercase_header_your_home_address:I = 0x7f121b41

.field public static final uppercase_header_your_legal_name:I = 0x7f121b42

.field public static final uppercase_header_your_phone_number:I = 0x7f121b43

.field public static final uppercase_invoice_details:I = 0x7f121b44

.field public static final uppercase_invoice_file_name:I = 0x7f121b45

.field public static final uppercase_invoice_options:I = 0x7f121b46

.field public static final uppercase_items_and_services:I = 0x7f121b47

.field public static final uppercase_items_assigned_to_category:I = 0x7f121b48

.field public static final uppercase_keypad:I = 0x7f121b49

.field public static final uppercase_learn_more:I = 0x7f121b4a

.field public static final uppercase_libraries:I = 0x7f121b4b

.field public static final uppercase_library:I = 0x7f121b4c

.field public static final uppercase_line_items:I = 0x7f121b4d

.field public static final uppercase_message:I = 0x7f121b4e

.field public static final uppercase_message_center:I = 0x7f121b4f

.field public static final uppercase_modifier_sets:I = 0x7f121b50

.field public static final uppercase_more_options:I = 0x7f121b51

.field public static final uppercase_my_readers:I = 0x7f121b52

.field public static final uppercase_nickname:I = 0x7f121b53

.field public static final uppercase_offline_payments_history_header:I = 0x7f121b54

.field public static final uppercase_offline_payments_history_header_one:I = 0x7f121b55

.field public static final uppercase_open_disputes:I = 0x7f121b56

.field public static final uppercase_options:I = 0x7f121b57

.field public static final uppercase_other:I = 0x7f121b58

.field public static final uppercase_overview:I = 0x7f121b59

.field public static final uppercase_paid_in_out:I = 0x7f121b5a

.field public static final uppercase_pairing_fallback_available_readers:I = 0x7f121b5b

.field public static final uppercase_payment_details:I = 0x7f121b5c

.field public static final uppercase_payment_schedule:I = 0x7f121b5d

.field public static final uppercase_payment_types_settings_disabled:I = 0x7f121b5e

.field public static final uppercase_payment_types_settings_primary:I = 0x7f121b5f

.field public static final uppercase_payment_types_settings_secondary:I = 0x7f121b60

.field public static final uppercase_payments:I = 0x7f121b61

.field public static final uppercase_pdf:I = 0x7f121b62

.field public static final uppercase_per_transaction_limit:I = 0x7f121b63

.field public static final uppercase_photo_edit_label:I = 0x7f121b64

.field public static final uppercase_preview:I = 0x7f121b65

.field public static final uppercase_price:I = 0x7f121b66

.field public static final uppercase_price_and_duration:I = 0x7f121b67

.field public static final uppercase_price_and_inventory:I = 0x7f121b68

.field public static final uppercase_reader_information:I = 0x7f121b69

.field public static final uppercase_receipt_customer_copy:I = 0x7f121b6a

.field public static final uppercase_receipt_detail_refund_giver:I = 0x7f121b6b

.field public static final uppercase_receipt_detail_refund_timestamp:I = 0x7f121b6c

.field public static final uppercase_receipt_detail_tender_payment_taker:I = 0x7f121b6d

.field public static final uppercase_receipt_detail_tender_payment_timestamp:I = 0x7f121b6e

.field public static final uppercase_receipt_detail_tender_payment_type:I = 0x7f121b6f

.field public static final uppercase_receipt_merchant_copy:I = 0x7f121b70

.field public static final uppercase_recurring_options:I = 0x7f121b71

.field public static final uppercase_refund:I = 0x7f121b72

.field public static final uppercase_refund_failed:I = 0x7f121b73

.field public static final uppercase_refund_pending:I = 0x7f121b74

.field public static final uppercase_refund_reason:I = 0x7f121b75

.field public static final uppercase_refund_to:I = 0x7f121b76

.field public static final uppercase_sales_report:I = 0x7f121b77

.field public static final uppercase_sales_report_amount:I = 0x7f121b78

.field public static final uppercase_sales_report_category_sales:I = 0x7f121b79

.field public static final uppercase_sales_report_daily_sales:I = 0x7f121b7a

.field public static final uppercase_sales_report_discounts_applied:I = 0x7f121b7b

.field public static final uppercase_sales_report_gross_sales:I = 0x7f121b7c

.field public static final uppercase_sales_report_hourly_sales:I = 0x7f121b7d

.field public static final uppercase_sales_report_items_sales:I = 0x7f121b7e

.field public static final uppercase_sales_report_items_sold:I = 0x7f121b7f

.field public static final uppercase_sales_report_monthly_sales:I = 0x7f121b80

.field public static final uppercase_sales_report_overview:I = 0x7f121b81

.field public static final uppercase_sales_report_quantity:I = 0x7f121b82

.field public static final uppercase_sales_report_sales_summary:I = 0x7f121b83

.field public static final uppercase_sales_report_sales_summary_payments_table:I = 0x7f121b84

.field public static final uppercase_sales_report_sales_summary_sales_table:I = 0x7f121b85

.field public static final uppercase_sales_report_summary:I = 0x7f121b86

.field public static final uppercase_support:I = 0x7f121b87

.field public static final uppercase_support_legal:I = 0x7f121b88

.field public static final uppercase_taxes:I = 0x7f121b89

.field public static final uppercase_ticket_groups:I = 0x7f121b8a

.field public static final uppercase_total:I = 0x7f121b8b

.field public static final uppercase_transaction:I = 0x7f121b8c

.field public static final uppercase_troubleshooting:I = 0x7f121b8d

.field public static final uppercase_variations:I = 0x7f121b8e

.field public static final use_a_device_code:I = 0x7f121b8f

.field public static final user_agent_identifier:I = 0x7f121b90

.field public static final validating_address:I = 0x7f121b91

.field public static final value:I = 0x7f121b92

.field public static final variable_percent_discount_standalone_character:I = 0x7f121b93

.field public static final variable_price:I = 0x7f121b94

.field public static final variation_one:I = 0x7f121b95

.field public static final variations_plural:I = 0x7f121b96

.field public static final verification_canceled:I = 0x7f121b97

.field public static final verification_hint:I = 0x7f121b98

.field public static final verification_hint_ca_au:I = 0x7f121b99

.field public static final verification_hint_jp:I = 0x7f121b9a

.field public static final verification_in_progress:I = 0x7f121b9b

.field public static final verified:I = 0x7f121b9c

.field public static final verify_card_change_failure_message:I = 0x7f121b9d

.field public static final verify_card_change_failure_title:I = 0x7f121b9e

.field public static final verify_card_change_success_message:I = 0x7f121b9f

.field public static final verify_card_change_success_title:I = 0x7f121ba0

.field public static final version:I = 0x7f121ba1

.field public static final version_name_suffix:I = 0x7f121ba2

.field public static final view_billing_address_error_message:I = 0x7f121ba3

.field public static final view_billing_address_error_title:I = 0x7f121ba4

.field public static final view_billing_address_field_label:I = 0x7f121ba5

.field public static final view_billing_address_label:I = 0x7f121ba6

.field public static final view_billing_address_loading:I = 0x7f121ba7

.field public static final view_billing_address_message:I = 0x7f121ba8

.field public static final view_details:I = 0x7f121ba9

.field public static final view_documents_legal_text:I = 0x7f121baa

.field public static final view_invoices:I = 0x7f121bab

.field public static final view_messages:I = 0x7f121bac

.field public static final view_notifications_label:I = 0x7f121bad

.field public static final view_sales_and_fees:I = 0x7f121bae

.field public static final view_sales_and_fees_url:I = 0x7f121baf

.field public static final visit_support:I = 0x7f121bb0

.field public static final visit_support_subtext:I = 0x7f121bb1

.field public static final void_confirm:I = 0x7f121bb2

.field public static final void_initial:I = 0x7f121bb3

.field public static final void_item:I = 0x7f121bb4

.field public static final void_reason_attributed:I = 0x7f121bb5

.field public static final void_reason_default:I = 0x7f121bb6

.field public static final void_ticket:I = 0x7f121bb7

.field public static final void_ticket_help_text:I = 0x7f121bb8

.field public static final void_tickets:I = 0x7f121bb9

.field public static final void_transactions_history:I = 0x7f121bba

.field public static final void_uppercase_reason:I = 0x7f121bbb

.field public static final void_with_reason:I = 0x7f121bbc

.field public static final voided_sale:I = 0x7f121bbd

.field public static final voided_sale_on_date_time_format:I = 0x7f121bbe

.field public static final warn_duplicate_variation_with_same_option_values_message:I = 0x7f121bbf

.field public static final warn_duplicate_variation_with_same_option_values_title:I = 0x7f121bc0

.field public static final warn_reaching_variation_number_limit_message:I = 0x7f121bc1

.field public static final warn_variation_number_limit_message_plural:I = 0x7f121bc2

.field public static final warn_variation_number_limit_message_singular:I = 0x7f121bc3

.field public static final warn_variation_number_limit_title:I = 0x7f121bc4

.field public static final warn_variations_to_delete_confirm_delete_and_create:I = 0x7f121bc5

.field public static final warn_variations_to_delete_confirm_delete_plural:I = 0x7f121bc6

.field public static final warn_variations_to_delete_confirm_delete_singular:I = 0x7f121bc7

.field public static final warn_variations_to_delete_message_plural_delete_plural_add:I = 0x7f121bc8

.field public static final warn_variations_to_delete_message_plural_delete_singular_add:I = 0x7f121bc9

.field public static final warn_variations_to_delete_message_remove_multiple_values_plural_variations:I = 0x7f121bca

.field public static final warn_variations_to_delete_message_remove_option_set_no_display_plural:I = 0x7f121bcb

.field public static final warn_variations_to_delete_message_remove_option_set_no_display_singular:I = 0x7f121bcc

.field public static final warn_variations_to_delete_message_remove_option_set_plural:I = 0x7f121bcd

.field public static final warn_variations_to_delete_message_remove_option_set_singular:I = 0x7f121bce

.field public static final warn_variations_to_delete_message_remove_single_value_plural_variations:I = 0x7f121bcf

.field public static final warn_variations_to_delete_message_remove_single_value_single_variation:I = 0x7f121bd0

.field public static final warn_variations_to_delete_message_singular_delete_plural_add:I = 0x7f121bd1

.field public static final warn_variations_to_delete_message_singular_delete_singular_add:I = 0x7f121bd2

.field public static final warn_variations_to_delete_title_delete:I = 0x7f121bd3

.field public static final warn_variations_to_delete_title_delete_and_create:I = 0x7f121bd4

.field public static final wednesdays:I = 0x7f121bd5

.field public static final week:I = 0x7f121bd6

.field public static final weekend_balance:I = 0x7f121bd7

.field public static final weekend_balance_hint:I = 0x7f121bd8

.field public static final weeks:I = 0x7f121bd9

.field public static final weight_unit_abbreviation_gram_lowercase:I = 0x7f121bda

.field public static final weight_unit_abbreviation_kilogram_lowercase:I = 0x7f121bdb

.field public static final weight_unit_abbreviation_milligram_lowercase:I = 0x7f121bdc

.field public static final weight_unit_abbreviation_ounce_lowercase:I = 0x7f121bdd

.field public static final weight_unit_abbreviation_pound_lowercase:I = 0x7f121bde

.field public static final what_do_you_think_of_app:I = 0x7f121bdf

.field public static final what_is_ein_hint:I = 0x7f121be0

.field public static final whats_new:I = 0x7f121be1

.field public static final whats_new_subtext:I = 0x7f121be2

.field public static final whats_new_tab:I = 0x7f121be3

.field public static final within_hundred_fifty_days:I = 0x7f121be4

.field public static final within_hundred_twenty_days:I = 0x7f121be5

.field public static final within_ninety_days:I = 0x7f121be6

.field public static final within_sixty_days:I = 0x7f121be7

.field public static final within_thirty_days:I = 0x7f121be8

.field public static final world_finish_account_subtitle:I = 0x7f121be9

.field public static final x2_buyer_loyalty_check_in_tier_reward_redeemed:I = 0x7f121bea

.field public static final x2_buyer_loyalty_check_in_tier_reward_redeemed_multiple:I = 0x7f121beb

.field public static final x2_support_center_url:I = 0x7f121bec

.field public static final year:I = 0x7f121bed

.field public static final year_only_format:I = 0x7f121bee

.field public static final years:I = 0x7f121bef

.field public static final yes:I = 0x7f121bf0

.field public static final yesterday:I = 0x7f121bf1

.field public static final you_are_set_up_for_same_day_deposits:I = 0x7f121bf2

.field public static final you_are_set_up_for_same_day_deposits_footer:I = 0x7f121bf3

.field public static final your_information:I = 0x7f121bf4

.field public static final youtube_url_template:I = 0x7f121bf5

.field public static final zero_amount_error:I = 0x7f121bf6

.field public static final zero_amount_error_body:I = 0x7f121bf7

.field public static final zip:I = 0x7f121bf8

.field public static final zip_code_hint:I = 0x7f121bf9


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
