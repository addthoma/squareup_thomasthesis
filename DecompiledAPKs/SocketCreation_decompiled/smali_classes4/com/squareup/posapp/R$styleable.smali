.class public final Lcom/squareup/posapp/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/posapp/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ActionBar:[I

.field public static final ActionBarLayout:[I

.field public static final ActionBarLayout_android_layout_gravity:I = 0x0

.field public static final ActionBar_background:I = 0x0

.field public static final ActionBar_backgroundSplit:I = 0x1

.field public static final ActionBar_backgroundStacked:I = 0x2

.field public static final ActionBar_contentInsetEnd:I = 0x3

.field public static final ActionBar_contentInsetEndWithActions:I = 0x4

.field public static final ActionBar_contentInsetLeft:I = 0x5

.field public static final ActionBar_contentInsetRight:I = 0x6

.field public static final ActionBar_contentInsetStart:I = 0x7

.field public static final ActionBar_contentInsetStartWithNavigation:I = 0x8

.field public static final ActionBar_customNavigationLayout:I = 0x9

.field public static final ActionBar_displayOptions:I = 0xa

.field public static final ActionBar_divider:I = 0xb

.field public static final ActionBar_elevation:I = 0xc

.field public static final ActionBar_height:I = 0xd

.field public static final ActionBar_hideOnContentScroll:I = 0xe

.field public static final ActionBar_homeAsUpIndicator:I = 0xf

.field public static final ActionBar_homeLayout:I = 0x10

.field public static final ActionBar_icon:I = 0x11

.field public static final ActionBar_indeterminateProgressStyle:I = 0x12

.field public static final ActionBar_itemPadding:I = 0x13

.field public static final ActionBar_logo:I = 0x14

.field public static final ActionBar_navigationMode:I = 0x15

.field public static final ActionBar_popupTheme:I = 0x16

.field public static final ActionBar_progressBarPadding:I = 0x17

.field public static final ActionBar_progressBarStyle:I = 0x18

.field public static final ActionBar_subtitle:I = 0x19

.field public static final ActionBar_subtitleTextStyle:I = 0x1a

.field public static final ActionBar_title:I = 0x1b

.field public static final ActionBar_titleTextStyle:I = 0x1c

.field public static final ActionMenuItemView:[I

.field public static final ActionMenuItemView_android_minWidth:I = 0x0

.field public static final ActionMenuView:[I

.field public static final ActionMode:[I

.field public static final ActionMode_background:I = 0x0

.field public static final ActionMode_backgroundSplit:I = 0x1

.field public static final ActionMode_closeItemLayout:I = 0x2

.field public static final ActionMode_height:I = 0x3

.field public static final ActionMode_subtitleTextStyle:I = 0x4

.field public static final ActionMode_titleTextStyle:I = 0x5

.field public static final ActivityChooserView:[I

.field public static final ActivityChooserView_expandActivityOverflowButtonDrawable:I = 0x0

.field public static final ActivityChooserView_initialActivityCount:I = 0x1

.field public static final AlertDialog:[I

.field public static final AlertDialog_android_layout:I = 0x0

.field public static final AlertDialog_buttonIconDimen:I = 0x1

.field public static final AlertDialog_buttonPanelSideLayout:I = 0x2

.field public static final AlertDialog_listItemLayout:I = 0x3

.field public static final AlertDialog_listLayout:I = 0x4

.field public static final AlertDialog_multiChoiceItemLayout:I = 0x5

.field public static final AlertDialog_showTitle:I = 0x6

.field public static final AlertDialog_singleChoiceItemLayout:I = 0x7

.field public static final AmountCell:[I

.field public static final AmountCell_amountCellStyle:I = 0x1

.field public static final AmountCell_android_title:I = 0x0

.field public static final AmountCell_sqMessageText:I = 0x2

.field public static final AnimatedStateListDrawableCompat:[I

.field public static final AnimatedStateListDrawableCompat_android_constantSize:I = 0x3

.field public static final AnimatedStateListDrawableCompat_android_dither:I = 0x0

.field public static final AnimatedStateListDrawableCompat_android_enterFadeDuration:I = 0x4

.field public static final AnimatedStateListDrawableCompat_android_exitFadeDuration:I = 0x5

.field public static final AnimatedStateListDrawableCompat_android_variablePadding:I = 0x2

.field public static final AnimatedStateListDrawableCompat_android_visible:I = 0x1

.field public static final AnimatedStateListDrawableItem:[I

.field public static final AnimatedStateListDrawableItem_android_drawable:I = 0x1

.field public static final AnimatedStateListDrawableItem_android_id:I = 0x0

.field public static final AnimatedStateListDrawableTransition:[I

.field public static final AnimatedStateListDrawableTransition_android_drawable:I = 0x0

.field public static final AnimatedStateListDrawableTransition_android_fromId:I = 0x2

.field public static final AnimatedStateListDrawableTransition_android_reversible:I = 0x3

.field public static final AnimatedStateListDrawableTransition_android_toId:I = 0x1

.field public static final AppBarLayout:[I

.field public static final AppBarLayoutStates:[I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AppBarLayoutStates_state_collapsed:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AppBarLayoutStates_state_collapsible:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AppBarLayoutStates_state_liftable:I = 0x2

.field public static final AppBarLayoutStates_state_lifted:I = 0x3

.field public static final AppBarLayout_Layout:[I

.field public static final AppBarLayout_Layout_layout_scrollFlags:I = 0x0

.field public static final AppBarLayout_Layout_layout_scrollInterpolator:I = 0x1

.field public static final AppBarLayout_android_background:I = 0x0

.field public static final AppBarLayout_android_keyboardNavigationCluster:I = 0x2

.field public static final AppBarLayout_android_touchscreenBlocksFocus:I = 0x1

.field public static final AppBarLayout_elevation:I = 0x3

.field public static final AppBarLayout_expanded:I = 0x4

.field public static final AppBarLayout_liftOnScroll:I = 0x5

.field public static final AppBarLayout_liftOnScrollTargetViewId:I = 0x6

.field public static final AppBarLayout_statusBarForeground:I = 0x7

.field public static final AppCompatImageView:[I

.field public static final AppCompatImageView_android_src:I = 0x0

.field public static final AppCompatImageView_srcCompat:I = 0x1

.field public static final AppCompatImageView_tint:I = 0x2

.field public static final AppCompatImageView_tintMode:I = 0x3

.field public static final AppCompatSeekBar:[I

.field public static final AppCompatSeekBar_android_thumb:I = 0x0

.field public static final AppCompatSeekBar_tickMark:I = 0x1

.field public static final AppCompatSeekBar_tickMarkTint:I = 0x2

.field public static final AppCompatSeekBar_tickMarkTintMode:I = 0x3

.field public static final AppCompatTextHelper:[I

.field public static final AppCompatTextHelper_android_drawableBottom:I = 0x2

.field public static final AppCompatTextHelper_android_drawableEnd:I = 0x6

.field public static final AppCompatTextHelper_android_drawableLeft:I = 0x3

.field public static final AppCompatTextHelper_android_drawableRight:I = 0x4

.field public static final AppCompatTextHelper_android_drawableStart:I = 0x5

.field public static final AppCompatTextHelper_android_drawableTop:I = 0x1

.field public static final AppCompatTextHelper_android_textAppearance:I = 0x0

.field public static final AppCompatTextView:[I

.field public static final AppCompatTextView_android_textAppearance:I = 0x0

.field public static final AppCompatTextView_autoSizeMaxTextSize:I = 0x1

.field public static final AppCompatTextView_autoSizeMinTextSize:I = 0x2

.field public static final AppCompatTextView_autoSizePresetSizes:I = 0x3

.field public static final AppCompatTextView_autoSizeStepGranularity:I = 0x4

.field public static final AppCompatTextView_autoSizeTextType:I = 0x5

.field public static final AppCompatTextView_drawableBottomCompat:I = 0x6

.field public static final AppCompatTextView_drawableEndCompat:I = 0x7

.field public static final AppCompatTextView_drawableLeftCompat:I = 0x8

.field public static final AppCompatTextView_drawableRightCompat:I = 0x9

.field public static final AppCompatTextView_drawableStartCompat:I = 0xa

.field public static final AppCompatTextView_drawableTint:I = 0xb

.field public static final AppCompatTextView_drawableTintMode:I = 0xc

.field public static final AppCompatTextView_drawableTopCompat:I = 0xd

.field public static final AppCompatTextView_firstBaselineToTopHeight:I = 0xe

.field public static final AppCompatTextView_fontFamily:I = 0xf

.field public static final AppCompatTextView_fontVariationSettings:I = 0x10

.field public static final AppCompatTextView_lastBaselineToBottomHeight:I = 0x11

.field public static final AppCompatTextView_lineHeight:I = 0x12

.field public static final AppCompatTextView_textAllCaps:I = 0x13

.field public static final AppCompatTextView_textLocale:I = 0x14

.field public static final AppCompatTheme:[I

.field public static final AppCompatTheme_actionBarDivider:I = 0x2

.field public static final AppCompatTheme_actionBarItemBackground:I = 0x3

.field public static final AppCompatTheme_actionBarPopupTheme:I = 0x4

.field public static final AppCompatTheme_actionBarSize:I = 0x5

.field public static final AppCompatTheme_actionBarSplitStyle:I = 0x6

.field public static final AppCompatTheme_actionBarStyle:I = 0x7

.field public static final AppCompatTheme_actionBarTabBarStyle:I = 0x8

.field public static final AppCompatTheme_actionBarTabStyle:I = 0x9

.field public static final AppCompatTheme_actionBarTabTextStyle:I = 0xa

.field public static final AppCompatTheme_actionBarTheme:I = 0xb

.field public static final AppCompatTheme_actionBarWidgetTheme:I = 0xc

.field public static final AppCompatTheme_actionButtonStyle:I = 0xd

.field public static final AppCompatTheme_actionDropDownStyle:I = 0xe

.field public static final AppCompatTheme_actionMenuTextAppearance:I = 0xf

.field public static final AppCompatTheme_actionMenuTextColor:I = 0x10

.field public static final AppCompatTheme_actionModeBackground:I = 0x11

.field public static final AppCompatTheme_actionModeCloseButtonStyle:I = 0x12

.field public static final AppCompatTheme_actionModeCloseDrawable:I = 0x13

.field public static final AppCompatTheme_actionModeCopyDrawable:I = 0x14

.field public static final AppCompatTheme_actionModeCutDrawable:I = 0x15

.field public static final AppCompatTheme_actionModeFindDrawable:I = 0x16

.field public static final AppCompatTheme_actionModePasteDrawable:I = 0x17

.field public static final AppCompatTheme_actionModePopupWindowStyle:I = 0x18

.field public static final AppCompatTheme_actionModeSelectAllDrawable:I = 0x19

.field public static final AppCompatTheme_actionModeShareDrawable:I = 0x1a

.field public static final AppCompatTheme_actionModeSplitBackground:I = 0x1b

.field public static final AppCompatTheme_actionModeStyle:I = 0x1c

.field public static final AppCompatTheme_actionModeWebSearchDrawable:I = 0x1d

.field public static final AppCompatTheme_actionOverflowButtonStyle:I = 0x1e

.field public static final AppCompatTheme_actionOverflowMenuStyle:I = 0x1f

.field public static final AppCompatTheme_activityChooserViewStyle:I = 0x20

.field public static final AppCompatTheme_alertDialogButtonGroupStyle:I = 0x21

.field public static final AppCompatTheme_alertDialogCenterButtons:I = 0x22

.field public static final AppCompatTheme_alertDialogStyle:I = 0x23

.field public static final AppCompatTheme_alertDialogTheme:I = 0x24

.field public static final AppCompatTheme_android_windowAnimationStyle:I = 0x1

.field public static final AppCompatTheme_android_windowIsFloating:I = 0x0

.field public static final AppCompatTheme_autoCompleteTextViewStyle:I = 0x25

.field public static final AppCompatTheme_borderlessButtonStyle:I = 0x26

.field public static final AppCompatTheme_buttonBarButtonStyle:I = 0x27

.field public static final AppCompatTheme_buttonBarNegativeButtonStyle:I = 0x28

.field public static final AppCompatTheme_buttonBarNeutralButtonStyle:I = 0x29

.field public static final AppCompatTheme_buttonBarPositiveButtonStyle:I = 0x2a

.field public static final AppCompatTheme_buttonBarStyle:I = 0x2b

.field public static final AppCompatTheme_buttonStyle:I = 0x2c

.field public static final AppCompatTheme_buttonStyleSmall:I = 0x2d

.field public static final AppCompatTheme_checkboxStyle:I = 0x2e

.field public static final AppCompatTheme_checkedTextViewStyle:I = 0x2f

.field public static final AppCompatTheme_colorAccent:I = 0x30

.field public static final AppCompatTheme_colorBackgroundFloating:I = 0x31

.field public static final AppCompatTheme_colorButtonNormal:I = 0x32

.field public static final AppCompatTheme_colorControlActivated:I = 0x33

.field public static final AppCompatTheme_colorControlHighlight:I = 0x34

.field public static final AppCompatTheme_colorControlNormal:I = 0x35

.field public static final AppCompatTheme_colorError:I = 0x36

.field public static final AppCompatTheme_colorPrimary:I = 0x37

.field public static final AppCompatTheme_colorPrimaryDark:I = 0x38

.field public static final AppCompatTheme_colorSwitchThumbNormal:I = 0x39

.field public static final AppCompatTheme_controlBackground:I = 0x3a

.field public static final AppCompatTheme_dialogCornerRadius:I = 0x3b

.field public static final AppCompatTheme_dialogPreferredPadding:I = 0x3c

.field public static final AppCompatTheme_dialogTheme:I = 0x3d

.field public static final AppCompatTheme_dividerHorizontal:I = 0x3e

.field public static final AppCompatTheme_dividerVertical:I = 0x3f

.field public static final AppCompatTheme_dropDownListViewStyle:I = 0x40

.field public static final AppCompatTheme_dropdownListPreferredItemHeight:I = 0x41

.field public static final AppCompatTheme_editTextBackground:I = 0x42

.field public static final AppCompatTheme_editTextColor:I = 0x43

.field public static final AppCompatTheme_editTextStyle:I = 0x44

.field public static final AppCompatTheme_homeAsUpIndicator:I = 0x45

.field public static final AppCompatTheme_imageButtonStyle:I = 0x46

.field public static final AppCompatTheme_listChoiceBackgroundIndicator:I = 0x47

.field public static final AppCompatTheme_listChoiceIndicatorMultipleAnimated:I = 0x48

.field public static final AppCompatTheme_listChoiceIndicatorSingleAnimated:I = 0x49

.field public static final AppCompatTheme_listDividerAlertDialog:I = 0x4a

.field public static final AppCompatTheme_listMenuViewStyle:I = 0x4b

.field public static final AppCompatTheme_listPopupWindowStyle:I = 0x4c

.field public static final AppCompatTheme_listPreferredItemHeight:I = 0x4d

.field public static final AppCompatTheme_listPreferredItemHeightLarge:I = 0x4e

.field public static final AppCompatTheme_listPreferredItemHeightSmall:I = 0x4f

.field public static final AppCompatTheme_listPreferredItemPaddingEnd:I = 0x50

.field public static final AppCompatTheme_listPreferredItemPaddingLeft:I = 0x51

.field public static final AppCompatTheme_listPreferredItemPaddingRight:I = 0x52

.field public static final AppCompatTheme_listPreferredItemPaddingStart:I = 0x53

.field public static final AppCompatTheme_panelBackground:I = 0x54

.field public static final AppCompatTheme_panelMenuListTheme:I = 0x55

.field public static final AppCompatTheme_panelMenuListWidth:I = 0x56

.field public static final AppCompatTheme_popupMenuStyle:I = 0x57

.field public static final AppCompatTheme_popupWindowStyle:I = 0x58

.field public static final AppCompatTheme_radioButtonStyle:I = 0x59

.field public static final AppCompatTheme_ratingBarStyle:I = 0x5a

.field public static final AppCompatTheme_ratingBarStyleIndicator:I = 0x5b

.field public static final AppCompatTheme_ratingBarStyleSmall:I = 0x5c

.field public static final AppCompatTheme_searchViewStyle:I = 0x5d

.field public static final AppCompatTheme_seekBarStyle:I = 0x5e

.field public static final AppCompatTheme_selectableItemBackground:I = 0x5f

.field public static final AppCompatTheme_selectableItemBackgroundBorderless:I = 0x60

.field public static final AppCompatTheme_spinnerDropDownItemStyle:I = 0x61

.field public static final AppCompatTheme_spinnerStyle:I = 0x62

.field public static final AppCompatTheme_switchStyle:I = 0x63

.field public static final AppCompatTheme_textAppearanceLargePopupMenu:I = 0x64

.field public static final AppCompatTheme_textAppearanceListItem:I = 0x65

.field public static final AppCompatTheme_textAppearanceListItemSecondary:I = 0x66

.field public static final AppCompatTheme_textAppearanceListItemSmall:I = 0x67

.field public static final AppCompatTheme_textAppearancePopupMenuHeader:I = 0x68

.field public static final AppCompatTheme_textAppearanceSearchResultSubtitle:I = 0x69

.field public static final AppCompatTheme_textAppearanceSearchResultTitle:I = 0x6a

.field public static final AppCompatTheme_textAppearanceSmallPopupMenu:I = 0x6b

.field public static final AppCompatTheme_textColorAlertDialogListItem:I = 0x6c

.field public static final AppCompatTheme_textColorSearchUrl:I = 0x6d

.field public static final AppCompatTheme_toolbarNavigationButtonStyle:I = 0x6e

.field public static final AppCompatTheme_toolbarStyle:I = 0x6f

.field public static final AppCompatTheme_tooltipForegroundColor:I = 0x70

.field public static final AppCompatTheme_tooltipFrameBackground:I = 0x71

.field public static final AppCompatTheme_viewInflaterClass:I = 0x72

.field public static final AppCompatTheme_windowActionBar:I = 0x73

.field public static final AppCompatTheme_windowActionBarOverlay:I = 0x74

.field public static final AppCompatTheme_windowActionModeOverlay:I = 0x75

.field public static final AppCompatTheme_windowFixedHeightMajor:I = 0x76

.field public static final AppCompatTheme_windowFixedHeightMinor:I = 0x77

.field public static final AppCompatTheme_windowFixedWidthMajor:I = 0x78

.field public static final AppCompatTheme_windowFixedWidthMinor:I = 0x79

.field public static final AppCompatTheme_windowMinWidthMajor:I = 0x7a

.field public static final AppCompatTheme_windowMinWidthMinor:I = 0x7b

.field public static final AppCompatTheme_windowNoTitle:I = 0x7c

.field public static final AppletSectionsListView:[I

.field public static final AppletSectionsListView_sq_valueColor:I = 0x0

.field public static final AspectRatioImageView:[I

.field public static final AspectRatioImageView_dominantMeasurement:I = 0x0

.field public static final AspectRatioImageView_sq_aspectRatio:I = 0x1

.field public static final AspectRatioImageView_sq_aspectRatioEnabled:I = 0x2

.field public static final AspectRatioLayout:[I

.field public static final AspectRatioLayout_sq_aspectRatio:I = 0x0

.field public static final AspectRatioLayout_sq_aspectRatioEnabled:I = 0x1

.field public static final AspectRatioPadlock:[I

.field public static final AspectRatioPadlock_sq_aspectRatio:I = 0x0

.field public static final AutoPaddingTextView:[I

.field public static final AutoPaddingTextView_autoPadding:I = 0x0

.field public static final AutoPaddingTextView_autoPaddingTextViewStyle:I = 0x1

.field public static final Badge:[I

.field public static final Badge_backgroundColor:I = 0x0

.field public static final Badge_badgeGravity:I = 0x1

.field public static final Badge_badgeTextColor:I = 0x2

.field public static final Badge_maxCharacterCount:I = 0x3

.field public static final Badge_number:I = 0x4

.field public static final BarChartView:[I

.field public static final BarChartView_barSpacing:I = 0x0

.field public static final BarChartView_stepSpacing:I = 0x1

.field public static final BorderedLinearLayout:[I

.field public static final BorderedLinearLayout_borders:I = 0x0

.field public static final BorderedLinearLayout_marinBorderWidth:I = 0x1

.field public static final BorderedScrollView:[I

.field public static final BorderedScrollView_borderColor:I = 0x0

.field public static final BottomAppBar:[I

.field public static final BottomAppBar_backgroundTint:I = 0x0

.field public static final BottomAppBar_elevation:I = 0x1

.field public static final BottomAppBar_fabAlignmentMode:I = 0x2

.field public static final BottomAppBar_fabAnimationMode:I = 0x3

.field public static final BottomAppBar_fabCradleMargin:I = 0x4

.field public static final BottomAppBar_fabCradleRoundedCornerRadius:I = 0x5

.field public static final BottomAppBar_fabCradleVerticalOffset:I = 0x6

.field public static final BottomAppBar_hideOnScroll:I = 0x7

.field public static final BottomNavigationView:[I

.field public static final BottomNavigationView_backgroundTint:I = 0x0

.field public static final BottomNavigationView_elevation:I = 0x1

.field public static final BottomNavigationView_itemBackground:I = 0x2

.field public static final BottomNavigationView_itemHorizontalTranslationEnabled:I = 0x3

.field public static final BottomNavigationView_itemIconSize:I = 0x4

.field public static final BottomNavigationView_itemIconTint:I = 0x5

.field public static final BottomNavigationView_itemRippleColor:I = 0x6

.field public static final BottomNavigationView_itemTextAppearanceActive:I = 0x7

.field public static final BottomNavigationView_itemTextAppearanceInactive:I = 0x8

.field public static final BottomNavigationView_itemTextColor:I = 0x9

.field public static final BottomNavigationView_labelVisibilityMode:I = 0xa

.field public static final BottomNavigationView_menu:I = 0xb

.field public static final BottomSheetBehavior_Layout:[I

.field public static final BottomSheetBehavior_Layout_android_elevation:I = 0x0

.field public static final BottomSheetBehavior_Layout_backgroundTint:I = 0x1

.field public static final BottomSheetBehavior_Layout_behavior_expandedOffset:I = 0x2

.field public static final BottomSheetBehavior_Layout_behavior_fitToContents:I = 0x3

.field public static final BottomSheetBehavior_Layout_behavior_halfExpandedRatio:I = 0x4

.field public static final BottomSheetBehavior_Layout_behavior_hideable:I = 0x5

.field public static final BottomSheetBehavior_Layout_behavior_peekHeight:I = 0x6

.field public static final BottomSheetBehavior_Layout_behavior_saveFlags:I = 0x7

.field public static final BottomSheetBehavior_Layout_behavior_skipCollapsed:I = 0x8

.field public static final BottomSheetBehavior_Layout_shapeAppearance:I = 0x9

.field public static final BottomSheetBehavior_Layout_shapeAppearanceOverlay:I = 0xa

.field public static final BottomSheetIndicator:[I

.field public static final BottomSheetIndicator_sqIndicatorColor:I = 0x0

.field public static final BottomSheetIndicator_sqShowIndicator:I = 0x1

.field public static final ButtonBarLayout:[I

.field public static final ButtonBarLayout_allowStacking:I = 0x0

.field public static final BuyerActionBar:[I

.field public static final BuyerActionBar_callToAction:I = 0x0

.field public static final BuyerActionBar_showNewSale:I = 0x1

.field public static final CalendarPickerView:[I

.field public static final CalendarPickerView_android_background:I = 0x0

.field public static final CalendarPickerView_tsquare_dayBackground:I = 0x1

.field public static final CalendarPickerView_tsquare_dayTextColor:I = 0x2

.field public static final CalendarPickerView_tsquare_displayAlwaysDigitNumbers:I = 0x3

.field public static final CalendarPickerView_tsquare_displayDayNamesHeaderRow:I = 0x4

.field public static final CalendarPickerView_tsquare_displayHeader:I = 0x5

.field public static final CalendarPickerView_tsquare_dividerColor:I = 0x6

.field public static final CalendarPickerView_tsquare_headerTextColor:I = 0x7

.field public static final CalendarPickerView_tsquare_titleTextStyle:I = 0x8

.field public static final CardBackground:[I

.field public static final CardBackground_marinCardBackground:I = 0x0

.field public static final CardEditor:[I

.field public static final CardEditor_android_textSize:I = 0x0

.field public static final CardEditor_compact:I = 0x1

.field public static final CardEditor_hideGlyph:I = 0x2

.field public static final CardEditor_sqInnerShadowColor:I = 0x3

.field public static final CardEditor_sqInnerShadowHeight:I = 0x4

.field public static final CardScreenPhoneLayout:[I

.field public static final CardScreenPhoneLayout_bodyLayout:I = 0x0

.field public static final CardView:[I

.field public static final CardView_android_minHeight:I = 0x1

.field public static final CardView_android_minWidth:I = 0x0

.field public static final CardView_cardBackgroundColor:I = 0x2

.field public static final CardView_cardCornerRadius:I = 0x3

.field public static final CardView_cardElevation:I = 0x4

.field public static final CardView_cardMaxElevation:I = 0x5

.field public static final CardView_cardPreventCornerOverlap:I = 0x6

.field public static final CardView_cardUseCompatPadding:I = 0x7

.field public static final CardView_contentPadding:I = 0x8

.field public static final CardView_contentPaddingBottom:I = 0x9

.field public static final CardView_contentPaddingLeft:I = 0xa

.field public static final CardView_contentPaddingRight:I = 0xb

.field public static final CardView_contentPaddingTop:I = 0xc

.field public static final CartEntryView:[I

.field public static final CartEntryView_halfGutterBlocks:I = 0x0

.field public static final CenteredFrameLayout_LayoutParams:[I

.field public static final CenteredFrameLayout_LayoutParams_centerOffsetLeft:I = 0x0

.field public static final ChartView:[I

.field public static final ChartView_axisColor:I = 0x0

.field public static final ChartView_axisStrokeWidth:I = 0x1

.field public static final ChartView_dataColors:I = 0x2

.field public static final ChartView_dataSelectedColors:I = 0x3

.field public static final ChartView_hideFirstYLabel:I = 0x4

.field public static final ChartView_textColor:I = 0x5

.field public static final ChartView_tickSize:I = 0x6

.field public static final ChartView_xLabelFrequency:I = 0x7

.field public static final ChartView_xLabelMinGap:I = 0x8

.field public static final ChartView_xLabelSize:I = 0x9

.field public static final ChartView_xLabelTickSpacing:I = 0xa

.field public static final ChartView_yLabelSize:I = 0xb

.field public static final ChartView_yLabelSpacing:I = 0xc

.field public static final CheckableGroup:[I

.field public static final CheckableGroup_android_divider:I = 0x0

.field public static final CheckableGroup_isHorizontal:I = 0x1

.field public static final CheckableGroup_singleChoice:I = 0x2

.field public static final CheckableGroup_sqCheckedButton:I = 0x3

.field public static final CheckableGroup_twoColumns:I = 0x4

.field public static final CheckablePreservedLabelRow:[I

.field public static final CheckablePreservedLabelRow_checkablePreservedLabelRowStyle:I = 0x0

.field public static final CheckablePreservedLabelRow_checkablePreservedLabelViewInternalStyle:I = 0x1

.field public static final Chip:[I

.field public static final ChipGroup:[I

.field public static final ChipGroup_checkedChip:I = 0x0

.field public static final ChipGroup_chipSpacing:I = 0x1

.field public static final ChipGroup_chipSpacingHorizontal:I = 0x2

.field public static final ChipGroup_chipSpacingVertical:I = 0x3

.field public static final ChipGroup_singleLine:I = 0x4

.field public static final ChipGroup_singleSelection:I = 0x5

.field public static final Chip_android_checkable:I = 0x5

.field public static final Chip_android_ellipsize:I = 0x2

.field public static final Chip_android_maxWidth:I = 0x3

.field public static final Chip_android_text:I = 0x4

.field public static final Chip_android_textAppearance:I = 0x0

.field public static final Chip_android_textColor:I = 0x1

.field public static final Chip_checkedIcon:I = 0x6

.field public static final Chip_checkedIconEnabled:I = 0x7

.field public static final Chip_checkedIconVisible:I = 0x8

.field public static final Chip_chipBackgroundColor:I = 0x9

.field public static final Chip_chipCornerRadius:I = 0xa

.field public static final Chip_chipEndPadding:I = 0xb

.field public static final Chip_chipIcon:I = 0xc

.field public static final Chip_chipIconEnabled:I = 0xd

.field public static final Chip_chipIconSize:I = 0xe

.field public static final Chip_chipIconTint:I = 0xf

.field public static final Chip_chipIconVisible:I = 0x10

.field public static final Chip_chipMinHeight:I = 0x11

.field public static final Chip_chipMinTouchTargetSize:I = 0x12

.field public static final Chip_chipStartPadding:I = 0x13

.field public static final Chip_chipStrokeColor:I = 0x14

.field public static final Chip_chipStrokeWidth:I = 0x15

.field public static final Chip_chipSurfaceColor:I = 0x16

.field public static final Chip_closeIcon:I = 0x17

.field public static final Chip_closeIconEnabled:I = 0x18

.field public static final Chip_closeIconEndPadding:I = 0x19

.field public static final Chip_closeIconSize:I = 0x1a

.field public static final Chip_closeIconStartPadding:I = 0x1b

.field public static final Chip_closeIconTint:I = 0x1c

.field public static final Chip_closeIconVisible:I = 0x1d

.field public static final Chip_ensureMinTouchTargetSize:I = 0x1e

.field public static final Chip_hideMotionSpec:I = 0x1f

.field public static final Chip_iconEndPadding:I = 0x20

.field public static final Chip_iconStartPadding:I = 0x21

.field public static final Chip_rippleColor:I = 0x22

.field public static final Chip_shapeAppearance:I = 0x23

.field public static final Chip_shapeAppearanceOverlay:I = 0x24

.field public static final Chip_showMotionSpec:I = 0x25

.field public static final Chip_textEndPadding:I = 0x26

.field public static final Chip_textStartPadding:I = 0x27

.field public static final CollapsingHeaderView:[I

.field public static final CollapsingHeaderView_headerBottomMargin:I = 0x0

.field public static final CollapsingHeaderView_headerTopMargin:I = 0x1

.field public static final CollapsingHeaderView_overlay:I = 0x2

.field public static final CollapsingHeaderView_toolBarHeight:I = 0x3

.field public static final CollapsingToolbarLayout:[I

.field public static final CollapsingToolbarLayout_Layout:[I

.field public static final CollapsingToolbarLayout_Layout_layout_collapseMode:I = 0x0

.field public static final CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier:I = 0x1

.field public static final CollapsingToolbarLayout_collapsedTitleGravity:I = 0x0

.field public static final CollapsingToolbarLayout_collapsedTitleTextAppearance:I = 0x1

.field public static final CollapsingToolbarLayout_contentScrim:I = 0x2

.field public static final CollapsingToolbarLayout_expandedTitleGravity:I = 0x3

.field public static final CollapsingToolbarLayout_expandedTitleMargin:I = 0x4

.field public static final CollapsingToolbarLayout_expandedTitleMarginBottom:I = 0x5

.field public static final CollapsingToolbarLayout_expandedTitleMarginEnd:I = 0x6

.field public static final CollapsingToolbarLayout_expandedTitleMarginStart:I = 0x7

.field public static final CollapsingToolbarLayout_expandedTitleMarginTop:I = 0x8

.field public static final CollapsingToolbarLayout_expandedTitleTextAppearance:I = 0x9

.field public static final CollapsingToolbarLayout_scrimAnimationDuration:I = 0xa

.field public static final CollapsingToolbarLayout_scrimVisibleHeightTrigger:I = 0xb

.field public static final CollapsingToolbarLayout_statusBarScrim:I = 0xc

.field public static final CollapsingToolbarLayout_title:I = 0xd

.field public static final CollapsingToolbarLayout_titleEnabled:I = 0xe

.field public static final CollapsingToolbarLayout_toolbarId:I = 0xf

.field public static final ColorStateListItem:[I

.field public static final ColorStateListItem_alpha:I = 0x2

.field public static final ColorStateListItem_android_alpha:I = 0x1

.field public static final ColorStateListItem_android_color:I = 0x0

.field public static final ColumnLayout:[I

.field public static final ColumnLayout_android_divider:I = 0x0

.field public static final ColumnLayout_sq_marginBetweenColumns:I = 0x1

.field public static final ColumnLayout_twoColumns:I = 0x2

.field public static final CompoundButton:[I

.field public static final CompoundButton_android_button:I = 0x0

.field public static final CompoundButton_buttonCompat:I = 0x1

.field public static final CompoundButton_buttonTint:I = 0x2

.field public static final CompoundButton_buttonTintMode:I = 0x3

.field public static final ConfirmButton:[I

.field public static final ConfirmButton_buttonHeight:I = 0x0

.field public static final ConfirmButton_confirmStageBackground:I = 0x1

.field public static final ConfirmButton_confirmStageText:I = 0x2

.field public static final ConfirmButton_confirmStageTextColor:I = 0x3

.field public static final ConfirmButton_initialStageBackground:I = 0x4

.field public static final ConfirmButton_initialStageText:I = 0x5

.field public static final ConfirmButton_initialStageTextColor:I = 0x6

.field public static final ConfirmButton_weight:I = 0x7

.field public static final ConstraintLayout_Layout:[I

.field public static final ConstraintLayout_Layout_android_maxHeight:I = 0x2

.field public static final ConstraintLayout_Layout_android_maxWidth:I = 0x1

.field public static final ConstraintLayout_Layout_android_minHeight:I = 0x4

.field public static final ConstraintLayout_Layout_android_minWidth:I = 0x3

.field public static final ConstraintLayout_Layout_android_orientation:I = 0x0

.field public static final ConstraintLayout_Layout_barrierAllowsGoneWidgets:I = 0x5

.field public static final ConstraintLayout_Layout_barrierDirection:I = 0x6

.field public static final ConstraintLayout_Layout_chainUseRtl:I = 0x7

.field public static final ConstraintLayout_Layout_constraintSet:I = 0x8

.field public static final ConstraintLayout_Layout_constraint_referenced_ids:I = 0x9

.field public static final ConstraintLayout_Layout_layout_constrainedHeight:I = 0xa

.field public static final ConstraintLayout_Layout_layout_constrainedWidth:I = 0xb

.field public static final ConstraintLayout_Layout_layout_constraintBaseline_creator:I = 0xc

.field public static final ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf:I = 0xd

.field public static final ConstraintLayout_Layout_layout_constraintBottom_creator:I = 0xe

.field public static final ConstraintLayout_Layout_layout_constraintBottom_toBottomOf:I = 0xf

.field public static final ConstraintLayout_Layout_layout_constraintBottom_toTopOf:I = 0x10

.field public static final ConstraintLayout_Layout_layout_constraintCircle:I = 0x11

.field public static final ConstraintLayout_Layout_layout_constraintCircleAngle:I = 0x12

.field public static final ConstraintLayout_Layout_layout_constraintCircleRadius:I = 0x13

.field public static final ConstraintLayout_Layout_layout_constraintDimensionRatio:I = 0x14

.field public static final ConstraintLayout_Layout_layout_constraintEnd_toEndOf:I = 0x15

.field public static final ConstraintLayout_Layout_layout_constraintEnd_toStartOf:I = 0x16

.field public static final ConstraintLayout_Layout_layout_constraintGuide_begin:I = 0x17

.field public static final ConstraintLayout_Layout_layout_constraintGuide_end:I = 0x18

.field public static final ConstraintLayout_Layout_layout_constraintGuide_percent:I = 0x19

.field public static final ConstraintLayout_Layout_layout_constraintHeight_default:I = 0x1a

.field public static final ConstraintLayout_Layout_layout_constraintHeight_max:I = 0x1b

.field public static final ConstraintLayout_Layout_layout_constraintHeight_min:I = 0x1c

.field public static final ConstraintLayout_Layout_layout_constraintHeight_percent:I = 0x1d

.field public static final ConstraintLayout_Layout_layout_constraintHorizontal_bias:I = 0x1e

.field public static final ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle:I = 0x1f

.field public static final ConstraintLayout_Layout_layout_constraintHorizontal_weight:I = 0x20

.field public static final ConstraintLayout_Layout_layout_constraintLeft_creator:I = 0x21

.field public static final ConstraintLayout_Layout_layout_constraintLeft_toLeftOf:I = 0x22

.field public static final ConstraintLayout_Layout_layout_constraintLeft_toRightOf:I = 0x23

.field public static final ConstraintLayout_Layout_layout_constraintRight_creator:I = 0x24

.field public static final ConstraintLayout_Layout_layout_constraintRight_toLeftOf:I = 0x25

.field public static final ConstraintLayout_Layout_layout_constraintRight_toRightOf:I = 0x26

.field public static final ConstraintLayout_Layout_layout_constraintStart_toEndOf:I = 0x27

.field public static final ConstraintLayout_Layout_layout_constraintStart_toStartOf:I = 0x28

.field public static final ConstraintLayout_Layout_layout_constraintTop_creator:I = 0x29

.field public static final ConstraintLayout_Layout_layout_constraintTop_toBottomOf:I = 0x2a

.field public static final ConstraintLayout_Layout_layout_constraintTop_toTopOf:I = 0x2b

.field public static final ConstraintLayout_Layout_layout_constraintVertical_bias:I = 0x2c

.field public static final ConstraintLayout_Layout_layout_constraintVertical_chainStyle:I = 0x2d

.field public static final ConstraintLayout_Layout_layout_constraintVertical_weight:I = 0x2e

.field public static final ConstraintLayout_Layout_layout_constraintWidth_default:I = 0x2f

.field public static final ConstraintLayout_Layout_layout_constraintWidth_max:I = 0x30

.field public static final ConstraintLayout_Layout_layout_constraintWidth_min:I = 0x31

.field public static final ConstraintLayout_Layout_layout_constraintWidth_percent:I = 0x32

.field public static final ConstraintLayout_Layout_layout_editor_absoluteX:I = 0x33

.field public static final ConstraintLayout_Layout_layout_editor_absoluteY:I = 0x34

.field public static final ConstraintLayout_Layout_layout_goneMarginBottom:I = 0x35

.field public static final ConstraintLayout_Layout_layout_goneMarginEnd:I = 0x36

.field public static final ConstraintLayout_Layout_layout_goneMarginLeft:I = 0x37

.field public static final ConstraintLayout_Layout_layout_goneMarginRight:I = 0x38

.field public static final ConstraintLayout_Layout_layout_goneMarginStart:I = 0x39

.field public static final ConstraintLayout_Layout_layout_goneMarginTop:I = 0x3a

.field public static final ConstraintLayout_Layout_layout_optimizationLevel:I = 0x3b

.field public static final ConstraintLayout_placeholder:[I

.field public static final ConstraintLayout_placeholder_content:I = 0x0

.field public static final ConstraintLayout_placeholder_emptyVisibility:I = 0x1

.field public static final ConstraintSet:[I

.field public static final ConstraintSet_android_alpha:I = 0xd

.field public static final ConstraintSet_android_elevation:I = 0x1a

.field public static final ConstraintSet_android_id:I = 0x1

.field public static final ConstraintSet_android_layout_height:I = 0x4

.field public static final ConstraintSet_android_layout_marginBottom:I = 0x8

.field public static final ConstraintSet_android_layout_marginEnd:I = 0x18

.field public static final ConstraintSet_android_layout_marginLeft:I = 0x5

.field public static final ConstraintSet_android_layout_marginRight:I = 0x7

.field public static final ConstraintSet_android_layout_marginStart:I = 0x17

.field public static final ConstraintSet_android_layout_marginTop:I = 0x6

.field public static final ConstraintSet_android_layout_width:I = 0x3

.field public static final ConstraintSet_android_maxHeight:I = 0xa

.field public static final ConstraintSet_android_maxWidth:I = 0x9

.field public static final ConstraintSet_android_minHeight:I = 0xc

.field public static final ConstraintSet_android_minWidth:I = 0xb

.field public static final ConstraintSet_android_orientation:I = 0x0

.field public static final ConstraintSet_android_rotation:I = 0x14

.field public static final ConstraintSet_android_rotationX:I = 0x15

.field public static final ConstraintSet_android_rotationY:I = 0x16

.field public static final ConstraintSet_android_scaleX:I = 0x12

.field public static final ConstraintSet_android_scaleY:I = 0x13

.field public static final ConstraintSet_android_transformPivotX:I = 0xe

.field public static final ConstraintSet_android_transformPivotY:I = 0xf

.field public static final ConstraintSet_android_translationX:I = 0x10

.field public static final ConstraintSet_android_translationY:I = 0x11

.field public static final ConstraintSet_android_translationZ:I = 0x19

.field public static final ConstraintSet_android_visibility:I = 0x2

.field public static final ConstraintSet_barrierAllowsGoneWidgets:I = 0x1b

.field public static final ConstraintSet_barrierDirection:I = 0x1c

.field public static final ConstraintSet_chainUseRtl:I = 0x1d

.field public static final ConstraintSet_constraint_referenced_ids:I = 0x1e

.field public static final ConstraintSet_layout_constrainedHeight:I = 0x1f

.field public static final ConstraintSet_layout_constrainedWidth:I = 0x20

.field public static final ConstraintSet_layout_constraintBaseline_creator:I = 0x21

.field public static final ConstraintSet_layout_constraintBaseline_toBaselineOf:I = 0x22

.field public static final ConstraintSet_layout_constraintBottom_creator:I = 0x23

.field public static final ConstraintSet_layout_constraintBottom_toBottomOf:I = 0x24

.field public static final ConstraintSet_layout_constraintBottom_toTopOf:I = 0x25

.field public static final ConstraintSet_layout_constraintCircle:I = 0x26

.field public static final ConstraintSet_layout_constraintCircleAngle:I = 0x27

.field public static final ConstraintSet_layout_constraintCircleRadius:I = 0x28

.field public static final ConstraintSet_layout_constraintDimensionRatio:I = 0x29

.field public static final ConstraintSet_layout_constraintEnd_toEndOf:I = 0x2a

.field public static final ConstraintSet_layout_constraintEnd_toStartOf:I = 0x2b

.field public static final ConstraintSet_layout_constraintGuide_begin:I = 0x2c

.field public static final ConstraintSet_layout_constraintGuide_end:I = 0x2d

.field public static final ConstraintSet_layout_constraintGuide_percent:I = 0x2e

.field public static final ConstraintSet_layout_constraintHeight_default:I = 0x2f

.field public static final ConstraintSet_layout_constraintHeight_max:I = 0x30

.field public static final ConstraintSet_layout_constraintHeight_min:I = 0x31

.field public static final ConstraintSet_layout_constraintHeight_percent:I = 0x32

.field public static final ConstraintSet_layout_constraintHorizontal_bias:I = 0x33

.field public static final ConstraintSet_layout_constraintHorizontal_chainStyle:I = 0x34

.field public static final ConstraintSet_layout_constraintHorizontal_weight:I = 0x35

.field public static final ConstraintSet_layout_constraintLeft_creator:I = 0x36

.field public static final ConstraintSet_layout_constraintLeft_toLeftOf:I = 0x37

.field public static final ConstraintSet_layout_constraintLeft_toRightOf:I = 0x38

.field public static final ConstraintSet_layout_constraintRight_creator:I = 0x39

.field public static final ConstraintSet_layout_constraintRight_toLeftOf:I = 0x3a

.field public static final ConstraintSet_layout_constraintRight_toRightOf:I = 0x3b

.field public static final ConstraintSet_layout_constraintStart_toEndOf:I = 0x3c

.field public static final ConstraintSet_layout_constraintStart_toStartOf:I = 0x3d

.field public static final ConstraintSet_layout_constraintTop_creator:I = 0x3e

.field public static final ConstraintSet_layout_constraintTop_toBottomOf:I = 0x3f

.field public static final ConstraintSet_layout_constraintTop_toTopOf:I = 0x40

.field public static final ConstraintSet_layout_constraintVertical_bias:I = 0x41

.field public static final ConstraintSet_layout_constraintVertical_chainStyle:I = 0x42

.field public static final ConstraintSet_layout_constraintVertical_weight:I = 0x43

.field public static final ConstraintSet_layout_constraintWidth_default:I = 0x44

.field public static final ConstraintSet_layout_constraintWidth_max:I = 0x45

.field public static final ConstraintSet_layout_constraintWidth_min:I = 0x46

.field public static final ConstraintSet_layout_constraintWidth_percent:I = 0x47

.field public static final ConstraintSet_layout_editor_absoluteX:I = 0x48

.field public static final ConstraintSet_layout_editor_absoluteY:I = 0x49

.field public static final ConstraintSet_layout_goneMarginBottom:I = 0x4a

.field public static final ConstraintSet_layout_goneMarginEnd:I = 0x4b

.field public static final ConstraintSet_layout_goneMarginLeft:I = 0x4c

.field public static final ConstraintSet_layout_goneMarginRight:I = 0x4d

.field public static final ConstraintSet_layout_goneMarginStart:I = 0x4e

.field public static final ConstraintSet_layout_goneMarginTop:I = 0x4f

.field public static final CoordinatorLayout:[I

.field public static final CoordinatorLayout_Layout:[I

.field public static final CoordinatorLayout_Layout_android_layout_gravity:I = 0x0

.field public static final CoordinatorLayout_Layout_layout_anchor:I = 0x1

.field public static final CoordinatorLayout_Layout_layout_anchorGravity:I = 0x2

.field public static final CoordinatorLayout_Layout_layout_behavior:I = 0x3

.field public static final CoordinatorLayout_Layout_layout_dodgeInsetEdges:I = 0x4

.field public static final CoordinatorLayout_Layout_layout_insetEdge:I = 0x5

.field public static final CoordinatorLayout_Layout_layout_keyline:I = 0x6

.field public static final CoordinatorLayout_keylines:I = 0x0

.field public static final CoordinatorLayout_statusBarBackground:I = 0x1

.field public static final DetailConfirmationView:[I

.field public static final DetailConfirmationView_android_title:I = 0x0

.field public static final DetailConfirmationView_buttonText:I = 0x1

.field public static final DetailConfirmationView_equalizeLines:I = 0x2

.field public static final DetailConfirmationView_glyph:I = 0x3

.field public static final DetailConfirmationView_helperText:I = 0x4

.field public static final DetailConfirmationView_message:I = 0x5

.field public static final DialogCardView:[I

.field public static final DialogCardView_landscapePhoneAsCard:I = 0x0

.field public static final DialogContentView:[I

.field public static final DialogContentView_dialogButtonCancelStyle:I = 0x0

.field public static final DialogContentView_dialogButtonConfirmStyle:I = 0x1

.field public static final DialogContentView_dialogMessageStyle:I = 0x2

.field public static final DialogContentView_dialogTitleStyle:I = 0x3

.field public static final DigitInputView:[I

.field public static final DigitInputView_android_textSize:I = 0x0

.field public static final DigitInputView_digitCount:I = 0x1

.field public static final DigitInputView_digitInputViewStyle:I = 0x2

.field public static final DiscountRuleSectionView:[I

.field public static final DiscountRuleSectionView_android_paddingBottom:I = 0x3

.field public static final DiscountRuleSectionView_android_paddingLeft:I = 0x0

.field public static final DiscountRuleSectionView_android_paddingRight:I = 0x2

.field public static final DiscountRuleSectionView_android_paddingTop:I = 0x1

.field public static final DiscountRuleSectionView_sectionTitle:I = 0x4

.field public static final DragSortListView:[I

.field public static final DragSortListView_click_remove_id:I = 0x0

.field public static final DragSortListView_collapsed_height:I = 0x1

.field public static final DragSortListView_drag_enabled:I = 0x2

.field public static final DragSortListView_drag_handle_id:I = 0x3

.field public static final DragSortListView_drag_scroll_start:I = 0x4

.field public static final DragSortListView_drag_start_mode:I = 0x5

.field public static final DragSortListView_drop_animation_duration:I = 0x6

.field public static final DragSortListView_fling_handle_id:I = 0x7

.field public static final DragSortListView_float_alpha:I = 0x8

.field public static final DragSortListView_float_background_color:I = 0x9

.field public static final DragSortListView_max_drag_scroll_speed:I = 0xa

.field public static final DragSortListView_remove_animation_duration:I = 0xb

.field public static final DragSortListView_remove_enabled:I = 0xc

.field public static final DragSortListView_remove_mode:I = 0xd

.field public static final DragSortListView_slide_shuffle_speed:I = 0xe

.field public static final DragSortListView_sort_enabled:I = 0xf

.field public static final DragSortListView_track_drag_sort:I = 0x10

.field public static final DragSortListView_use_default_controller:I = 0x11

.field public static final DrawerArrowToggle:[I

.field public static final DrawerArrowToggle_arrowHeadLength:I = 0x0

.field public static final DrawerArrowToggle_arrowShaftLength:I = 0x1

.field public static final DrawerArrowToggle_barLength:I = 0x2

.field public static final DrawerArrowToggle_color:I = 0x3

.field public static final DrawerArrowToggle_drawableSize:I = 0x4

.field public static final DrawerArrowToggle_gapBetweenBars:I = 0x5

.field public static final DrawerArrowToggle_spinBars:I = 0x6

.field public static final DrawerArrowToggle_thickness:I = 0x7

.field public static final DropDownContainer:[I

.field public static final DropDownContainer_dropDownContainerStyle:I = 0x0

.field public static final DropDownContainer_dropDownDuration:I = 0x1

.field public static final DropDownContainer_glassCancel:I = 0x2

.field public static final DropDownContainer_scrimColor:I = 0x3

.field public static final EditItemItemOptionAndValuesRow:[I

.field public static final EditItemItemOptionAndValuesRow_android_minHeight:I = 0x0

.field public static final EditItemItemOptionAndValuesRow_sqOptionNamePhoneAppearance:I = 0x1

.field public static final EditItemItemOptionAndValuesRow_sqOptionNameTabletAppearance:I = 0x2

.field public static final EditItemItemOptionAndValuesRow_sqOptionValuesPhoneAppearance:I = 0x3

.field public static final EditItemItemOptionAndValuesRow_sqOptionValuesTabletAppearance:I = 0x4

.field public static final EmployeeLockButton:[I

.field public static final EmployeeLockButton_android_textColor:I = 0x0

.field public static final ErrorsBarView:[I

.field public static final ErrorsBarView_android_divider:I = 0x3

.field public static final ErrorsBarView_android_paddingLeft:I = 0x1

.field public static final ErrorsBarView_android_paddingRight:I = 0x2

.field public static final ErrorsBarView_android_showDividers:I = 0x4

.field public static final ErrorsBarView_android_textColor:I = 0x0

.field public static final ErrorsBarView_errorRowHeight:I = 0x5

.field public static final ExtendedFloatingActionButton:[I

.field public static final ExtendedFloatingActionButton_Behavior_Layout:[I

.field public static final ExtendedFloatingActionButton_Behavior_Layout_behavior_autoHide:I = 0x0

.field public static final ExtendedFloatingActionButton_Behavior_Layout_behavior_autoShrink:I = 0x1

.field public static final ExtendedFloatingActionButton_elevation:I = 0x0

.field public static final ExtendedFloatingActionButton_extendMotionSpec:I = 0x1

.field public static final ExtendedFloatingActionButton_hideMotionSpec:I = 0x2

.field public static final ExtendedFloatingActionButton_showMotionSpec:I = 0x3

.field public static final ExtendedFloatingActionButton_shrinkMotionSpec:I = 0x4

.field public static final ExtraTextAppearance:[I

.field public static final ExtraTextAppearance_android_fontFamily:I = 0x4

.field public static final ExtraTextAppearance_android_letterSpacing:I = 0x5

.field public static final ExtraTextAppearance_android_textAllCaps:I = 0x3

.field public static final ExtraTextAppearance_android_textColor:I = 0x2

.field public static final ExtraTextAppearance_android_textSize:I = 0x0

.field public static final ExtraTextAppearance_android_textStyle:I = 0x1

.field public static final ExtraTextAppearance_sqFontSelection:I = 0x6

.field public static final FloatingActionButton:[I

.field public static final FloatingActionButton_Behavior_Layout:[I

.field public static final FloatingActionButton_Behavior_Layout_behavior_autoHide:I = 0x0

.field public static final FloatingActionButton_backgroundTint:I = 0x0

.field public static final FloatingActionButton_backgroundTintMode:I = 0x1

.field public static final FloatingActionButton_borderWidth:I = 0x2

.field public static final FloatingActionButton_elevation:I = 0x3

.field public static final FloatingActionButton_ensureMinTouchTargetSize:I = 0x4

.field public static final FloatingActionButton_fabCustomSize:I = 0x5

.field public static final FloatingActionButton_fabSize:I = 0x6

.field public static final FloatingActionButton_hideMotionSpec:I = 0x7

.field public static final FloatingActionButton_hoveredFocusedTranslationZ:I = 0x8

.field public static final FloatingActionButton_maxImageSize:I = 0x9

.field public static final FloatingActionButton_pressedTranslationZ:I = 0xa

.field public static final FloatingActionButton_rippleColor:I = 0xb

.field public static final FloatingActionButton_shapeAppearance:I = 0xc

.field public static final FloatingActionButton_shapeAppearanceOverlay:I = 0xd

.field public static final FloatingActionButton_showMotionSpec:I = 0xe

.field public static final FloatingActionButton_useCompatPadding:I = 0xf

.field public static final FloatingHeaderBodyRow:[I

.field public static final FloatingHeaderBodyRow_bodyText:I = 0x0

.field public static final FloatingHeaderBodyRow_headerText:I = 0x1

.field public static final FlowLayout:[I

.field public static final FlowLayout_itemSpacing:I = 0x0

.field public static final FlowLayout_lineSpacing:I = 0x1

.field public static final FlyingStarsBackgroundView:[I

.field public static final FlyingStarsBackgroundView_numberOfStars:I = 0x0

.field public static final FontFamily:[I

.field public static final FontFamilyFont:[I

.field public static final FontFamilyFont_android_font:I = 0x0

.field public static final FontFamilyFont_android_fontStyle:I = 0x2

.field public static final FontFamilyFont_android_fontVariationSettings:I = 0x4

.field public static final FontFamilyFont_android_fontWeight:I = 0x1

.field public static final FontFamilyFont_android_ttcIndex:I = 0x3

.field public static final FontFamilyFont_font:I = 0x5

.field public static final FontFamilyFont_fontStyle:I = 0x6

.field public static final FontFamilyFont_fontVariationSettings:I = 0x7

.field public static final FontFamilyFont_fontWeight:I = 0x8

.field public static final FontFamilyFont_ttcIndex:I = 0x9

.field public static final FontFamily_fontProviderAuthority:I = 0x0

.field public static final FontFamily_fontProviderCerts:I = 0x1

.field public static final FontFamily_fontProviderFetchStrategy:I = 0x2

.field public static final FontFamily_fontProviderFetchTimeout:I = 0x3

.field public static final FontFamily_fontProviderPackage:I = 0x4

.field public static final FontFamily_fontProviderQuery:I = 0x5

.field public static final FontSelection:[I

.field public static final FontSelection_android_fontFamily:I = 0x0

.field public static final FontSelection_sqBoldWeight:I = 0x1

.field public static final FontSelection_sqNormalWeight:I = 0x2

.field public static final ForegroundLinearLayout:[I

.field public static final ForegroundLinearLayout_android_foreground:I = 0x0

.field public static final ForegroundLinearLayout_android_foregroundGravity:I = 0x1

.field public static final ForegroundLinearLayout_foregroundInsidePadding:I = 0x2

.field public static final Fragment:[I

.field public static final FragmentContainerView:[I

.field public static final FragmentContainerView_android_name:I = 0x0

.field public static final FragmentContainerView_android_tag:I = 0x1

.field public static final Fragment_android_id:I = 0x1

.field public static final Fragment_android_name:I = 0x0

.field public static final Fragment_android_tag:I = 0x2

.field public static final GlyphButtonEditText:[I

.field public static final GlyphButtonEditText_android_hint:I = 0x1

.field public static final GlyphButtonEditText_android_imeOptions:I = 0x3

.field public static final GlyphButtonEditText_android_inputType:I = 0x2

.field public static final GlyphButtonEditText_android_text:I = 0x0

.field public static final GlyphButtonEditText_buttonGlyph:I = 0x4

.field public static final GlyphButtonEditText_buttonGlyphBackground:I = 0x5

.field public static final GlyphButtonEditText_buttonGlyphColor:I = 0x6

.field public static final GlyphButtonEditText_buttonGlyphHint:I = 0x7

.field public static final GlyphButtonEditText_glyph:I = 0x8

.field public static final GlyphButtonEditText_sq_glyphColor:I = 0x9

.field public static final GlyphRadioRow:[I

.field public static final GlyphRadioRow_android_text:I = 0x0

.field public static final GlyphRadioRow_shortText:I = 0x1

.field public static final GlyphSelectableEditText:[I

.field public static final GlyphSelectableEditText_glyph:I = 0x0

.field public static final GradientColor:[I

.field public static final GradientColorItem:[I

.field public static final GradientColorItem_android_color:I = 0x0

.field public static final GradientColorItem_android_offset:I = 0x1

.field public static final GradientColor_android_centerColor:I = 0x7

.field public static final GradientColor_android_centerX:I = 0x3

.field public static final GradientColor_android_centerY:I = 0x4

.field public static final GradientColor_android_endColor:I = 0x1

.field public static final GradientColor_android_endX:I = 0xa

.field public static final GradientColor_android_endY:I = 0xb

.field public static final GradientColor_android_gradientRadius:I = 0x5

.field public static final GradientColor_android_startColor:I = 0x0

.field public static final GradientColor_android_startX:I = 0x8

.field public static final GradientColor_android_startY:I = 0x9

.field public static final GradientColor_android_tileMode:I = 0x6

.field public static final GradientColor_android_type:I = 0x2

.field public static final HSRoundedImageView:[I

.field public static final HSRoundedImageView_hs__backgroundColor:I = 0x0

.field public static final HSRoundedImageView_hs__borderColor:I = 0x1

.field public static final HSRoundedImageView_hs__borderWidth:I = 0x2

.field public static final HSRoundedImageView_hs__cornerRadius:I = 0x3

.field public static final HSRoundedImageView_hs__placeholder:I = 0x4

.field public static final HSRoundedImageView_hs__roundedBottomLeft:I = 0x5

.field public static final HSRoundedImageView_hs__roundedBottomRight:I = 0x6

.field public static final HSRoundedImageView_hs__roundedTopLeft:I = 0x7

.field public static final HSRoundedImageView_hs__roundedTopRight:I = 0x8

.field public static final HSTypingIndicatorView:[I

.field public static final HSTypingIndicatorView_hs__dotColor:I = 0x0

.field public static final HSTypingIndicatorView_hs__dotDiameter:I = 0x1

.field public static final HSTypingIndicatorView_hs__interDotPadding:I = 0x2

.field public static final HalfAndHalfLayout:[I

.field public static final HalfAndHalfLayout_android_orientation:I = 0x0

.field public static final HelpshiftTheme_Activity:[I

.field public static final HelpshiftTheme_Activity_hs__actionButtonIconColor:I = 0x0

.field public static final HelpshiftTheme_Activity_hs__attachScreenshotActionButtonIcon:I = 0x1

.field public static final HelpshiftTheme_Activity_hs__attachmentBackgroundColor:I = 0x2

.field public static final HelpshiftTheme_Activity_hs__chatBubbleAdminBackgroundColor:I = 0x3

.field public static final HelpshiftTheme_Activity_hs__chatBubbleAdminLink:I = 0x4

.field public static final HelpshiftTheme_Activity_hs__chatBubbleAdminText:I = 0x5

.field public static final HelpshiftTheme_Activity_hs__chatBubbleMediaBackgroundColor:I = 0x6

.field public static final HelpshiftTheme_Activity_hs__chatBubbleMediaBorderColor:I = 0x7

.field public static final HelpshiftTheme_Activity_hs__chatBubbleUserBackgroundColor:I = 0x8

.field public static final HelpshiftTheme_Activity_hs__chatBubbleUserLink:I = 0x9

.field public static final HelpshiftTheme_Activity_hs__chatBubbleUserText:I = 0xa

.field public static final HelpshiftTheme_Activity_hs__collapsedPickerIconColor:I = 0xb

.field public static final HelpshiftTheme_Activity_hs__composeBackgroundColor:I = 0xc

.field public static final HelpshiftTheme_Activity_hs__contactUsButtonStyle:I = 0xd

.field public static final HelpshiftTheme_Activity_hs__contentSeparatorColor:I = 0xe

.field public static final HelpshiftTheme_Activity_hs__conversationActionButtonIcon:I = 0xf

.field public static final HelpshiftTheme_Activity_hs__conversationNotificationActionButtonIcon:I = 0x10

.field public static final HelpshiftTheme_Activity_hs__errorTextColor:I = 0x11

.field public static final HelpshiftTheme_Activity_hs__expandedPickerIconColor:I = 0x12

.field public static final HelpshiftTheme_Activity_hs__faqsListItemStyle:I = 0x13

.field public static final HelpshiftTheme_Activity_hs__faqsPagerTabStripIndicatorColor:I = 0x14

.field public static final HelpshiftTheme_Activity_hs__faqsPagerTabStripStyle:I = 0x15

.field public static final HelpshiftTheme_Activity_hs__footerPromptBackground:I = 0x16

.field public static final HelpshiftTheme_Activity_hs__inboxBodyTextColor:I = 0x17

.field public static final HelpshiftTheme_Activity_hs__inboxIconBackgroundColor:I = 0x18

.field public static final HelpshiftTheme_Activity_hs__inboxSeparatorColor:I = 0x19

.field public static final HelpshiftTheme_Activity_hs__inboxSwipeToDeleteBackgroundColor:I = 0x1a

.field public static final HelpshiftTheme_Activity_hs__inboxSwipeToDeleteIconColor:I = 0x1b

.field public static final HelpshiftTheme_Activity_hs__inboxTimeStampTextColor:I = 0x1c

.field public static final HelpshiftTheme_Activity_hs__inboxTimeStampUnreadTextColor:I = 0x1d

.field public static final HelpshiftTheme_Activity_hs__inboxTitleTextColor:I = 0x1e

.field public static final HelpshiftTheme_Activity_hs__inboxTitleUnreadTextColor:I = 0x1f

.field public static final HelpshiftTheme_Activity_hs__searchActionButtonIcon:I = 0x20

.field public static final HelpshiftTheme_Activity_hs__searchHighlightColor:I = 0x21

.field public static final HelpshiftTheme_Activity_hs__searchOnNewConversationDoneActionButtonIcon:I = 0x22

.field public static final HelpshiftTheme_Activity_hs__selectableOptionColor:I = 0x23

.field public static final HelpshiftTheme_Activity_hs__separatorColor:I = 0x24

.field public static final HelpshiftTheme_Activity_hs__startConversationActionButtonIcon:I = 0x25

.field public static final HelpshiftTheme_Activity_hs__tabSelectedTextColor:I = 0x26

.field public static final HelpshiftTheme_Activity_hs__tabTextColor:I = 0x27

.field public static final HelpshiftTheme_Activity_hs__tabletConversationContainerBackgroundColor:I = 0x28

.field public static final HelpshiftTheme_Activity_hs__textFieldBorderColor:I = 0x29

.field public static final HelpshiftTheme_Activity_hs__toolbarStyle:I = 0x2a

.field public static final HelpshiftTheme_Activity_hs__typingIndicatorColor:I = 0x2b

.field public static final HoloCompat:[I

.field public static final HoloCompat_square_buttonBarButtonStyle:I = 0x0

.field public static final HoloCompat_square_buttonBarStyle:I = 0x1

.field public static final HoloCompat_square_dividerHorizontal:I = 0x2

.field public static final HoloCompat_square_dividerVertical:I = 0x3

.field public static final HorizontalThreeChildLayout:[I

.field public static final HorizontalThreeChildLayout_firstChildPercentage:I = 0x0

.field public static final HorizontalThreeChildLayout_secondChildPercentage:I = 0x1

.field public static final HorizontalThreeChildLayout_thirdChildPercentage:I = 0x2

.field public static final JaggedLineView:[I

.field public static final JaggedLineView_borderColor:I = 0x0

.field public static final JaggedLineView_fillColor:I = 0x1

.field public static final JaggedLineView_triangleCount:I = 0x2

.field public static final LabeledProgressBar:[I

.field public static final LabeledProgressBar_title:I = 0x0

.field public static final LineChartView:[I

.field public static final LineChartView_dotSize:I = 0x0

.field public static final LineChartView_lineSize:I = 0x1

.field public static final LineRow:[I

.field public static final LineRow_chevronVisibility:I = 0x0

.field public static final LineRow_displayValueAsPercent:I = 0x1

.field public static final LineRow_glyph:I = 0x2

.field public static final LineRow_lineRowPadding:I = 0x3

.field public static final LineRow_sq_labelText:I = 0x4

.field public static final LineRow_sq_noteText:I = 0x5

.field public static final LineRow_sq_valueColor:I = 0x6

.field public static final LineRow_sq_valueText:I = 0x7

.field public static final LineRow_titleText:I = 0x8

.field public static final LineRow_titleTextSize:I = 0x9

.field public static final LineRow_valueWeight:I = 0xa

.field public static final LineRow_weight:I = 0xb

.field public static final LinearConstraintLayout:[I

.field public static final LinearConstraintLayout_android_orientation:I = 0x0

.field public static final LinearLayoutCompat:[I

.field public static final LinearLayoutCompat_Layout:[I

.field public static final LinearLayoutCompat_Layout_android_layout_gravity:I = 0x0

.field public static final LinearLayoutCompat_Layout_android_layout_height:I = 0x2

.field public static final LinearLayoutCompat_Layout_android_layout_weight:I = 0x3

.field public static final LinearLayoutCompat_Layout_android_layout_width:I = 0x1

.field public static final LinearLayoutCompat_android_baselineAligned:I = 0x2

.field public static final LinearLayoutCompat_android_baselineAlignedChildIndex:I = 0x3

.field public static final LinearLayoutCompat_android_gravity:I = 0x0

.field public static final LinearLayoutCompat_android_orientation:I = 0x1

.field public static final LinearLayoutCompat_android_weightSum:I = 0x4

.field public static final LinearLayoutCompat_divider:I = 0x5

.field public static final LinearLayoutCompat_dividerPadding:I = 0x6

.field public static final LinearLayoutCompat_measureWithLargestChild:I = 0x7

.field public static final LinearLayoutCompat_showDividers:I = 0x8

.field public static final ListPopupWindow:[I

.field public static final ListPopupWindow_android_dropDownHorizontalOffset:I = 0x0

.field public static final ListPopupWindow_android_dropDownVerticalOffset:I = 0x1

.field public static final LoadingImageView:[I

.field public static final LoadingImageView_circleCrop:I = 0x0

.field public static final LoadingImageView_imageAspectRatio:I = 0x1

.field public static final LoadingImageView_imageAspectRatioAdjust:I = 0x2

.field public static final MarinActionBarView:[I

.field public static final MarinActionBarView_marinActionBarPrimaryButtonStyle:I = 0x0

.field public static final MarinActionBarView_marinActionBarSecondaryButtonStyle:I = 0x1

.field public static final MarinActionBarView_marinActionBarStyle:I = 0x2

.field public static final MarinActionBarView_marinActionBarTheme:I = 0x3

.field public static final MarinActionBarView_marinActionBarUpButtonStyle:I = 0x4

.field public static final MarinAppletTabletLayout:[I

.field public static final MarinAppletTabletLayout_dividerOffsetFromTop:I = 0x0

.field public static final MarinAppletTabletLayout_dividerWidth:I = 0x1

.field public static final MarinAppletTabletLayout_ratio:I = 0x2

.field public static final MarinGlyphMessage:[I

.field public static final MarinGlyphMessage_android_title:I = 0x0

.field public static final MarinGlyphMessage_button_text:I = 0x1

.field public static final MarinGlyphMessage_glyph:I = 0x2

.field public static final MarinGlyphMessage_glyphSaveEnabled:I = 0x3

.field public static final MarinGlyphMessage_glyphVisible:I = 0x4

.field public static final MarinGlyphMessage_marinGlyphMessageTitleColor:I = 0x5

.field public static final MarinGlyphMessage_message:I = 0x6

.field public static final MarinGlyphMessage_messageColor:I = 0x7

.field public static final MarinGlyphMessage_sq_glyphColor:I = 0x8

.field public static final MarinGlyphTextView:[I

.field public static final MarinGlyphTextView_android_textColor:I = 0x0

.field public static final MarinGlyphTextView_chevronColor:I = 0x1

.field public static final MarinGlyphTextView_chevronVisibility:I = 0x2

.field public static final MarinGlyphTextView_glyph:I = 0x3

.field public static final MarinGlyphTextView_glyphFontSizeOverride:I = 0x4

.field public static final MarinGlyphTextView_glyphPosition:I = 0x5

.field public static final MarinGlyphTextView_glyphShadowColor:I = 0x6

.field public static final MarinGlyphTextView_glyphShadowDx:I = 0x7

.field public static final MarinGlyphTextView_glyphShadowDy:I = 0x8

.field public static final MarinGlyphTextView_glyphShadowRadius:I = 0x9

.field public static final MarinPageIndicator:[I

.field public static final MarinPageIndicator_indicatorDiameter:I = 0x0

.field public static final MarinPageIndicator_indicatorHighlightColor:I = 0x1

.field public static final MarinPageIndicator_indicatorMatteColor:I = 0x2

.field public static final MarinPageIndicator_indicatorSpacing:I = 0x3

.field public static final MarinSpinnerGlyph:[I

.field public static final MarinSpinnerGlyph_failureGlyph:I = 0x0

.field public static final MarinSpinnerGlyph_failureGlyphColor:I = 0x1

.field public static final MarinSpinnerGlyph_glyph:I = 0x2

.field public static final MarinSpinnerGlyph_glyphColor:I = 0x3

.field public static final MarinSpinnerGlyph_marinSpinnerGlyphStyle:I = 0x4

.field public static final MarinSpinnerGlyph_transitionDuration:I = 0x5

.field public static final MarinVerticalCaretView:[I

.field public static final MarinVerticalCaretView_pointingDown:I = 0x0

.field public static final MarketAutoCompleteTextView:[I

.field public static final MarketAutoCompleteTextView_weight:I = 0x0

.field public static final MarketButton:[I

.field public static final MarketButton_weight:I = 0x0

.field public static final MarketCheckedTextView:[I

.field public static final MarketCheckedTextView_weight:I = 0x0

.field public static final MarketEditText:[I

.field public static final MarketEditText_weight:I = 0x0

.field public static final MarketRadioButton:[I

.field public static final MarketRadioButton_weight:I = 0x0

.field public static final MarketTextView:[I

.field public static final MarketTextView_weight:I = 0x0

.field public static final MaterialAlertDialog:[I

.field public static final MaterialAlertDialogTheme:[I

.field public static final MaterialAlertDialogTheme_materialAlertDialogBodyTextStyle:I = 0x0

.field public static final MaterialAlertDialogTheme_materialAlertDialogTheme:I = 0x1

.field public static final MaterialAlertDialogTheme_materialAlertDialogTitleIconStyle:I = 0x2

.field public static final MaterialAlertDialogTheme_materialAlertDialogTitlePanelStyle:I = 0x3

.field public static final MaterialAlertDialogTheme_materialAlertDialogTitleTextStyle:I = 0x4

.field public static final MaterialAlertDialog_backgroundInsetBottom:I = 0x0

.field public static final MaterialAlertDialog_backgroundInsetEnd:I = 0x1

.field public static final MaterialAlertDialog_backgroundInsetStart:I = 0x2

.field public static final MaterialAlertDialog_backgroundInsetTop:I = 0x3

.field public static final MaterialButton:[I

.field public static final MaterialButtonToggleGroup:[I

.field public static final MaterialButtonToggleGroup_checkedButton:I = 0x0

.field public static final MaterialButtonToggleGroup_singleSelection:I = 0x1

.field public static final MaterialButton_android_checkable:I = 0x4

.field public static final MaterialButton_android_insetBottom:I = 0x3

.field public static final MaterialButton_android_insetLeft:I = 0x0

.field public static final MaterialButton_android_insetRight:I = 0x1

.field public static final MaterialButton_android_insetTop:I = 0x2

.field public static final MaterialButton_backgroundTint:I = 0x5

.field public static final MaterialButton_backgroundTintMode:I = 0x6

.field public static final MaterialButton_cornerRadius:I = 0x7

.field public static final MaterialButton_elevation:I = 0x8

.field public static final MaterialButton_icon:I = 0x9

.field public static final MaterialButton_iconGravity:I = 0xa

.field public static final MaterialButton_iconPadding:I = 0xb

.field public static final MaterialButton_iconSize:I = 0xc

.field public static final MaterialButton_iconTint:I = 0xd

.field public static final MaterialButton_iconTintMode:I = 0xe

.field public static final MaterialButton_rippleColor:I = 0xf

.field public static final MaterialButton_shapeAppearance:I = 0x10

.field public static final MaterialButton_shapeAppearanceOverlay:I = 0x11

.field public static final MaterialButton_strokeColor:I = 0x12

.field public static final MaterialButton_strokeWidth:I = 0x13

.field public static final MaterialCalendar:[I

.field public static final MaterialCalendarItem:[I

.field public static final MaterialCalendarItem_android_insetBottom:I = 0x3

.field public static final MaterialCalendarItem_android_insetLeft:I = 0x0

.field public static final MaterialCalendarItem_android_insetRight:I = 0x1

.field public static final MaterialCalendarItem_android_insetTop:I = 0x2

.field public static final MaterialCalendarItem_itemFillColor:I = 0x4

.field public static final MaterialCalendarItem_itemShapeAppearance:I = 0x5

.field public static final MaterialCalendarItem_itemShapeAppearanceOverlay:I = 0x6

.field public static final MaterialCalendarItem_itemStrokeColor:I = 0x7

.field public static final MaterialCalendarItem_itemStrokeWidth:I = 0x8

.field public static final MaterialCalendarItem_itemTextColor:I = 0x9

.field public static final MaterialCalendar_android_windowFullscreen:I = 0x0

.field public static final MaterialCalendar_dayInvalidStyle:I = 0x1

.field public static final MaterialCalendar_daySelectedStyle:I = 0x2

.field public static final MaterialCalendar_dayStyle:I = 0x3

.field public static final MaterialCalendar_dayTodayStyle:I = 0x4

.field public static final MaterialCalendar_rangeFillColor:I = 0x5

.field public static final MaterialCalendar_yearSelectedStyle:I = 0x6

.field public static final MaterialCalendar_yearStyle:I = 0x7

.field public static final MaterialCalendar_yearTodayStyle:I = 0x8

.field public static final MaterialCardView:[I

.field public static final MaterialCardView_android_checkable:I = 0x0

.field public static final MaterialCardView_cardForegroundColor:I = 0x1

.field public static final MaterialCardView_checkedIcon:I = 0x2

.field public static final MaterialCardView_checkedIconTint:I = 0x3

.field public static final MaterialCardView_rippleColor:I = 0x4

.field public static final MaterialCardView_shapeAppearance:I = 0x5

.field public static final MaterialCardView_shapeAppearanceOverlay:I = 0x6

.field public static final MaterialCardView_state_dragged:I = 0x7

.field public static final MaterialCardView_strokeColor:I = 0x8

.field public static final MaterialCardView_strokeWidth:I = 0x9

.field public static final MaterialCheckBox:[I

.field public static final MaterialCheckBox_buttonTint:I = 0x0

.field public static final MaterialCheckBox_useMaterialThemeColors:I = 0x1

.field public static final MaterialRadioButton:[I

.field public static final MaterialRadioButton_useMaterialThemeColors:I = 0x0

.field public static final MaterialShape:[I

.field public static final MaterialShape_shapeAppearance:I = 0x0

.field public static final MaterialShape_shapeAppearanceOverlay:I = 0x1

.field public static final MaterialTextAppearance:[I

.field public static final MaterialTextAppearance_android_lineHeight:I = 0x0

.field public static final MaterialTextAppearance_lineHeight:I = 0x1

.field public static final MaterialTextView:[I

.field public static final MaterialTextView_android_lineHeight:I = 0x1

.field public static final MaterialTextView_android_textAppearance:I = 0x0

.field public static final MaterialTextView_lineHeight:I = 0x2

.field public static final MenuGroup:[I

.field public static final MenuGroup_android_checkableBehavior:I = 0x5

.field public static final MenuGroup_android_enabled:I = 0x0

.field public static final MenuGroup_android_id:I = 0x1

.field public static final MenuGroup_android_menuCategory:I = 0x3

.field public static final MenuGroup_android_orderInCategory:I = 0x4

.field public static final MenuGroup_android_visible:I = 0x2

.field public static final MenuItem:[I

.field public static final MenuItem_actionLayout:I = 0xd

.field public static final MenuItem_actionProviderClass:I = 0xe

.field public static final MenuItem_actionViewClass:I = 0xf

.field public static final MenuItem_alphabeticModifiers:I = 0x10

.field public static final MenuItem_android_alphabeticShortcut:I = 0x9

.field public static final MenuItem_android_checkable:I = 0xb

.field public static final MenuItem_android_checked:I = 0x3

.field public static final MenuItem_android_enabled:I = 0x1

.field public static final MenuItem_android_icon:I = 0x0

.field public static final MenuItem_android_id:I = 0x2

.field public static final MenuItem_android_menuCategory:I = 0x5

.field public static final MenuItem_android_numericShortcut:I = 0xa

.field public static final MenuItem_android_onClick:I = 0xc

.field public static final MenuItem_android_orderInCategory:I = 0x6

.field public static final MenuItem_android_title:I = 0x7

.field public static final MenuItem_android_titleCondensed:I = 0x8

.field public static final MenuItem_android_visible:I = 0x4

.field public static final MenuItem_contentDescription:I = 0x11

.field public static final MenuItem_iconTint:I = 0x12

.field public static final MenuItem_iconTintMode:I = 0x13

.field public static final MenuItem_numericModifiers:I = 0x14

.field public static final MenuItem_showAsAction:I = 0x15

.field public static final MenuItem_tooltipText:I = 0x16

.field public static final MenuView:[I

.field public static final MenuView_android_headerBackground:I = 0x4

.field public static final MenuView_android_horizontalDivider:I = 0x2

.field public static final MenuView_android_itemBackground:I = 0x5

.field public static final MenuView_android_itemIconDisabledAlpha:I = 0x6

.field public static final MenuView_android_itemTextAppearance:I = 0x1

.field public static final MenuView_android_verticalDivider:I = 0x3

.field public static final MenuView_android_windowAnimationStyle:I = 0x0

.field public static final MenuView_preserveIconSpacing:I = 0x7

.field public static final MenuView_subMenuArrow:I = 0x8

.field public static final MessageView:[I

.field public static final MessageView_android_ellipsize:I = 0x3

.field public static final MessageView_android_gravity:I = 0x4

.field public static final MessageView_android_includeFontPadding:I = 0x6

.field public static final MessageView_android_shadowColor:I = 0x7

.field public static final MessageView_android_shadowDx:I = 0x8

.field public static final MessageView_android_shadowDy:I = 0x9

.field public static final MessageView_android_shadowRadius:I = 0xa

.field public static final MessageView_android_text:I = 0x5

.field public static final MessageView_android_textColor:I = 0x2

.field public static final MessageView_android_textSize:I = 0x0

.field public static final MessageView_android_textStyle:I = 0x1

.field public static final MessageView_equalizeLines:I = 0xb

.field public static final MessageView_textJustification:I = 0xc

.field public static final MessageView_weight:I = 0xd

.field public static final NameValueImageRow:[I

.field public static final NameValueImageRow_android_src:I = 0x0

.field public static final NameValueImageRow_android_text:I = 0x1

.field public static final NameValueImageRow_nameValueRowNameStyle:I = 0x2

.field public static final NameValueImageRow_nameValueRowStyle:I = 0x3

.field public static final NameValueImageRow_shortText:I = 0x4

.field public static final NameValueRow:[I

.field public static final NameValueRow_android_text:I = 0x0

.field public static final NameValueRow_nameValueRowNameStyle:I = 0x1

.field public static final NameValueRow_nameValueRowStyle:I = 0x2

.field public static final NameValueRow_nameValueRowValueStyle:I = 0x3

.field public static final NameValueRow_shortText:I = 0x4

.field public static final NameValueRow_showPercent:I = 0x5

.field public static final NameValueRow_value:I = 0x6

.field public static final NavigationView:[I

.field public static final NavigationView_android_background:I = 0x0

.field public static final NavigationView_android_fitsSystemWindows:I = 0x1

.field public static final NavigationView_android_maxWidth:I = 0x2

.field public static final NavigationView_elevation:I = 0x3

.field public static final NavigationView_headerLayout:I = 0x4

.field public static final NavigationView_itemBackground:I = 0x5

.field public static final NavigationView_itemHorizontalPadding:I = 0x6

.field public static final NavigationView_itemIconPadding:I = 0x7

.field public static final NavigationView_itemIconSize:I = 0x8

.field public static final NavigationView_itemIconTint:I = 0x9

.field public static final NavigationView_itemMaxLines:I = 0xa

.field public static final NavigationView_itemShapeAppearance:I = 0xb

.field public static final NavigationView_itemShapeAppearanceOverlay:I = 0xc

.field public static final NavigationView_itemShapeFillColor:I = 0xd

.field public static final NavigationView_itemShapeInsetBottom:I = 0xe

.field public static final NavigationView_itemShapeInsetEnd:I = 0xf

.field public static final NavigationView_itemShapeInsetStart:I = 0x10

.field public static final NavigationView_itemShapeInsetTop:I = 0x11

.field public static final NavigationView_itemTextAppearance:I = 0x12

.field public static final NavigationView_itemTextColor:I = 0x13

.field public static final NavigationView_menu:I = 0x14

.field public static final NohoActionBar:[I

.field public static final NohoActionBar_nohoActionBarStyle:I = 0x0

.field public static final NohoActionBar_sqEdgeColor:I = 0x1

.field public static final NohoActionBar_sqEdgeWidth:I = 0x2

.field public static final NohoButton:[I

.field public static final NohoButton_android_background:I = 0x2

.field public static final NohoButton_android_minHeight:I = 0x3

.field public static final NohoButton_android_textAppearance:I = 0x0

.field public static final NohoButton_android_textColor:I = 0x1

.field public static final NohoButton_nohoButtonStyle:I = 0x4

.field public static final NohoButton_sqButtonType:I = 0x5

.field public static final NohoButton_sqEdgeColor:I = 0x6

.field public static final NohoButton_sqEdgeWidth:I = 0x7

.field public static final NohoButton_sqHideBorder:I = 0x8

.field public static final NohoButton_sqIsTwoLines:I = 0x9

.field public static final NohoButton_sqMinHeightTwoLines:I = 0xa

.field public static final NohoButton_sqSubText:I = 0xb

.field public static final NohoButton_sqSubTextAppearance:I = 0xc

.field public static final NohoButton_sqSubTextColor:I = 0xd

.field public static final NohoCheckableGroup:[I

.field public static final NohoCheckableGroup_android_divider:I = 0x0

.field public static final NohoCheckableGroup_sqCheckedButton:I = 0x1

.field public static final NohoCheckableGroup_sqIsHorizontal:I = 0x2

.field public static final NohoCheckableGroup_sqSingleChoice:I = 0x3

.field public static final NohoCheckableGroup_sqTwoColumns:I = 0x4

.field public static final NohoCheckableRow:[I

.field public static final NohoCheckableRow_sqCheckType:I = 0x0

.field public static final NohoConstraintLayout:[I

.field public static final NohoConstraintLayout_layout_edges:I = 0x0

.field public static final NohoConstraintLayout_layout_insetEdges:I = 0x1

.field public static final NohoConstraintLayout_nohoConstraintLayoutStyle:I = 0x2

.field public static final NohoConstraintLayout_sqEdgeColor:I = 0x3

.field public static final NohoConstraintLayout_sqEdgeWidth:I = 0x4

.field public static final NohoDoubleButtonRow:[I

.field public static final NohoDoubleButtonRow_sqLeadingButtonType:I = 0x0

.field public static final NohoDoubleButtonRow_sqLeadingText:I = 0x1

.field public static final NohoDoubleButtonRow_sqTrailingButtonType:I = 0x2

.field public static final NohoDoubleButtonRow_sqTrailingText:I = 0x3

.field public static final NohoDropdown:[I

.field public static final NohoDropdown_sqMenuViewLayoutId:I = 0x0

.field public static final NohoDropdown_sqSelectedViewLayoutId:I = 0x1

.field public static final NohoEdges:[I

.field public static final NohoEdges_sqEdgeColor:I = 0x0

.field public static final NohoEdges_sqEdgeWidth:I = 0x1

.field public static final NohoEdges_sqFocusColor:I = 0x2

.field public static final NohoEdges_sqFocusHeight:I = 0x3

.field public static final NohoEdges_sqHideBorder:I = 0x4

.field public static final NohoEdges_sqHideShadow:I = 0x5

.field public static final NohoEdges_sqInnerShadowColor:I = 0x6

.field public static final NohoEdges_sqInnerShadowHeight:I = 0x7

.field public static final NohoEditRow:[I

.field public static final NohoEditRow_sqClearButton:I = 0x0

.field public static final NohoEditRow_sqFillAvailableArea:I = 0x1

.field public static final NohoEditRow_sqFlexibleLabel:I = 0x2

.field public static final NohoEditRow_sqLabelLayoutWeight:I = 0x3

.field public static final NohoEditRow_sqLabelText:I = 0x4

.field public static final NohoEditRow_sqPositionInList:I = 0x5

.field public static final NohoEditRow_sqSearchIcon:I = 0x6

.field public static final NohoEditRow_sqTextAlignment:I = 0x7

.field public static final NohoEditRow_sqViewPasswordButton:I = 0x8

.field public static final NohoEditText:[I

.field public static final NohoEditText_nohoEditTextStyle:I = 0x0

.field public static final NohoEditText_sqFillAvailableArea:I = 0x1

.field public static final NohoEditText_sqLabelColor:I = 0x2

.field public static final NohoEditText_sqLabelLayoutWeight:I = 0x3

.field public static final NohoEditText_sqLabelText:I = 0x4

.field public static final NohoEditText_sqNoteColor:I = 0x5

.field public static final NohoEditText_sqTextAlignment:I = 0x6

.field public static final NohoFixedSizeDrawable:[I

.field public static final NohoFixedSizeDrawable_android_drawable:I = 0x0

.field public static final NohoInputBox:[I

.field public static final NohoInputBox_sqClearButton:I = 0x0

.field public static final NohoInputBox_sqDetailsAppearance:I = 0x1

.field public static final NohoInputBox_sqErrorAppearance:I = 0x2

.field public static final NohoInputBox_sqLabel:I = 0x3

.field public static final NohoInputBox_sqLabelAppearance:I = 0x4

.field public static final NohoLabel:[I

.field public static final NohoLabel_android_textAppearance:I = 0x0

.field public static final NohoLabel_sqLabelType:I = 0x1

.field public static final NohoLinearLayout:[I

.field public static final NohoLinearLayout_layout_edges:I = 0x0

.field public static final NohoLinearLayout_layout_insetEdges:I = 0x1

.field public static final NohoLinearLayout_sqEdgeColor:I = 0x2

.field public static final NohoLinearLayout_sqEdgeWidth:I = 0x3

.field public static final NohoListView:[I

.field public static final NohoListView_nohoListViewStyle:I = 0x0

.field public static final NohoListView_sqContentPaddingType:I = 0x1

.field public static final NohoListView_sqIsAlert:I = 0x2

.field public static final NohoMessageText:[I

.field public static final NohoMessageText_nohoMessageTextStyle:I = 0x0

.field public static final NohoMessageView:[I

.field public static final NohoMessageView_android_title:I = 0x0

.field public static final NohoMessageView_nohoMessageViewStyle:I = 0x1

.field public static final NohoMessageView_sqDrawable:I = 0x2

.field public static final NohoMessageView_sqHelpText:I = 0x3

.field public static final NohoMessageView_sqLinkText:I = 0x4

.field public static final NohoMessageView_sqMessageText:I = 0x5

.field public static final NohoMessageView_sqPrimaryButtonText:I = 0x6

.field public static final NohoMessageView_sqSecondaryButtonText:I = 0x7

.field public static final NohoNumberPicker:[I

.field public static final NohoNumberPicker_android_divider:I = 0x3

.field public static final NohoNumberPicker_android_dividerHeight:I = 0x4

.field public static final NohoNumberPicker_android_maxWidth:I = 0x2

.field public static final NohoNumberPicker_android_textColor:I = 0x1

.field public static final NohoNumberPicker_android_textSize:I = 0x0

.field public static final NohoNumberPicker_nohoNumberPickerStyle:I = 0x5

.field public static final NohoNumberPicker_sqSelectionDividersDistance:I = 0x6

.field public static final NohoNumberPicker_sqSelectionItemGap:I = 0x7

.field public static final NohoNumberPicker_sqSelectionTextColor:I = 0x8

.field public static final NohoNumberPicker_sqTextGravity:I = 0x9

.field public static final NohoRecyclerEdges:[I

.field public static final NohoRecyclerEdgesRow:[I

.field public static final NohoRecyclerEdgesRow_android_paddingBottom:I = 0x3

.field public static final NohoRecyclerEdgesRow_android_paddingLeft:I = 0x0

.field public static final NohoRecyclerEdgesRow_android_paddingRight:I = 0x2

.field public static final NohoRecyclerEdgesRow_android_paddingTop:I = 0x1

.field public static final NohoRecyclerEdges_android_paddingBottom:I = 0x3

.field public static final NohoRecyclerEdges_android_paddingLeft:I = 0x0

.field public static final NohoRecyclerEdges_android_paddingRight:I = 0x2

.field public static final NohoRecyclerEdges_android_paddingTop:I = 0x1

.field public static final NohoRecyclerEdges_sqEdgeColor:I = 0x4

.field public static final NohoRecyclerEdges_sqEdgeWidth:I = 0x5

.field public static final NohoRecyclerView:[I

.field public static final NohoRecyclerView_nohoRecyclerViewStyle:I = 0x0

.field public static final NohoRecyclerView_sqContentPaddingType:I = 0x1

.field public static final NohoRecyclerView_sqIsAlert:I = 0x2

.field public static final NohoResizeDrawable:[I

.field public static final NohoResizeDrawable_android_drawable:I = 0x2

.field public static final NohoResizeDrawable_android_height:I = 0x0

.field public static final NohoResizeDrawable_android_width:I = 0x1

.field public static final NohoResponseLinearLayout:[I

.field public static final NohoResponseLinearLayout_sqIsResponsive:I = 0x0

.field public static final NohoRow:[I

.field public static final NohoRow_Accessory:[I

.field public static final NohoRow_Accessory_android_height:I = 0x1

.field public static final NohoRow_Accessory_android_tint:I = 0x0

.field public static final NohoRow_Accessory_android_width:I = 0x2

.field public static final NohoRow_ActionIcon:[I

.field public static final NohoRow_ActionIcon_android_height:I = 0x1

.field public static final NohoRow_ActionIcon_android_tint:I = 0x0

.field public static final NohoRow_ActionIcon_android_width:I = 0x2

.field public static final NohoRow_Icon:[I

.field public static final NohoRow_Icon_android_colorForeground:I = 0x0

.field public static final NohoRow_Icon_android_colorForegroundInverse:I = 0x3

.field public static final NohoRow_Icon_android_height:I = 0x1

.field public static final NohoRow_Icon_android_width:I = 0x2

.field public static final NohoRow_android_minHeight:I = 0x3

.field public static final NohoRow_android_padding:I = 0x0

.field public static final NohoRow_android_paddingBottom:I = 0x2

.field public static final NohoRow_android_paddingEnd:I = 0x5

.field public static final NohoRow_android_paddingStart:I = 0x4

.field public static final NohoRow_android_paddingTop:I = 0x1

.field public static final NohoRow_sqAccessoryStyle:I = 0x6

.field public static final NohoRow_sqAccessoryType:I = 0x7

.field public static final NohoRow_sqActionIcon:I = 0x8

.field public static final NohoRow_sqActionIconStyle:I = 0x9

.field public static final NohoRow_sqActionLink:I = 0xa

.field public static final NohoRow_sqActionLinkAppearance:I = 0xb

.field public static final NohoRow_sqAlignToLabel:I = 0xc

.field public static final NohoRow_sqDescription:I = 0xd

.field public static final NohoRow_sqDescriptionAppearance:I = 0xe

.field public static final NohoRow_sqIcon:I = 0xf

.field public static final NohoRow_sqIconBackground:I = 0x10

.field public static final NohoRow_sqIconStyle:I = 0x11

.field public static final NohoRow_sqLabel:I = 0x12

.field public static final NohoRow_sqLabelAppearance:I = 0x13

.field public static final NohoRow_sqRowStyle:I = 0x14

.field public static final NohoRow_sqSubValue:I = 0x15

.field public static final NohoRow_sqSubValueAppearance:I = 0x16

.field public static final NohoRow_sqTextIcon:I = 0x17

.field public static final NohoRow_sqValue:I = 0x18

.field public static final NohoRow_sqValueAppearance:I = 0x19

.field public static final NohoScrollView:[I

.field public static final NohoScrollView_nohoScrollViewStyle:I = 0x0

.field public static final NohoScrollView_sqContentPaddingType:I = 0x1

.field public static final NohoScrollView_sqIsAlert:I = 0x2

.field public static final NohoSelectable:[I

.field public static final NohoSelectable_android_checked:I = 0x1

.field public static final NohoSelectable_android_enabled:I = 0x0

.field public static final NohoSelectable_android_minHeight:I = 0x2

.field public static final NohoSelectable_sqColor:I = 0x3

.field public static final NohoSelectable_sqLabel:I = 0x4

.field public static final NohoSelectable_sqLabelAppearance:I = 0x5

.field public static final NohoSelectable_sqValue:I = 0x6

.field public static final NohoSelectable_sqValueAppearance:I = 0x7

.field public static final NohoSpinner:[I

.field public static final NohoSpinner_nohoSpinnerStyle:I = 0x0

.field public static final NohoTextAppearance:[I

.field public static final NohoTextAppearance_android_fontFamily:I = 0x2

.field public static final NohoTextAppearance_android_textColor:I = 0x1

.field public static final NohoTextAppearance_android_textSize:I = 0x0

.field public static final NohoTitleValueRow:[I

.field public static final NohoTitleValueRow_android_text:I = 0x0

.field public static final NohoTitleValueRow_nohoTitleValueRowStyle:I = 0x1

.field public static final NohoTitleValueRow_sqValueText:I = 0x2

.field public static final NotificationButton:[I

.field public static final NotificationButton_highlightIfImportant:I = 0x0

.field public static final NotificationButton_importantColor:I = 0x1

.field public static final NotificationButton_summaryMessagePlural:I = 0x2

.field public static final NotificationButton_summaryMessageSingular:I = 0x3

.field public static final NotificationDrawable:[I

.field public static final NotificationDrawableItem:[I

.field public static final NotificationDrawableItem_android_drawable:I = 0x2

.field public static final NotificationDrawableItem_android_height:I = 0x0

.field public static final NotificationDrawableItem_android_left:I = 0x3

.field public static final NotificationDrawableItem_android_top:I = 0x4

.field public static final NotificationDrawableItem_android_width:I = 0x1

.field public static final NotificationDrawableText:[I

.field public static final NotificationDrawableText_android_textAppearance:I = 0x0

.field public static final NotificationDrawableText_android_x:I = 0x1

.field public static final NotificationDrawableText_android_y:I = 0x2

.field public static final NotificationDrawable_android_drawable:I = 0x3

.field public static final NotificationDrawable_android_height:I = 0x1

.field public static final NotificationDrawable_android_text:I = 0x0

.field public static final NotificationDrawable_android_width:I = 0x2

.field public static final NotificationDrawable_balloonStyle:I = 0x4

.field public static final NotificationDrawable_baseStyle:I = 0x5

.field public static final NotificationDrawable_filterOutStyle:I = 0x6

.field public static final NotificationDrawable_textStyle:I = 0x7

.field public static final NullStateHorizontalScrollView:[I

.field public static final NullStateHorizontalScrollView_nullStateText:I = 0x0

.field public static final NullStateListView:[I

.field public static final NullStateListView_nullStateNegativeTopMargin:I = 0x0

.field public static final NullStateListView_nullStateText:I = 0x1

.field public static final OrderEntryDrawerButton:[I

.field public static final OrderEntryDrawerButton_customLayout:I = 0x0

.field public static final OrderHardwareItemView:[I

.field public static final OrderHardwareItemView_readerSrc:I = 0x0

.field public static final OrderHardwareItemView_title:I = 0x1

.field public static final Padlock:[I

.field public static final Padlock_android_lineSpacingExtra:I = 0x0

.field public static final Padlock_backspaceColor:I = 0x1

.field public static final Padlock_backspaceDisabledColor:I = 0x2

.field public static final Padlock_backspaceSelector:I = 0x3

.field public static final Padlock_buttonSelector:I = 0x4

.field public static final Padlock_buttonTextSize:I = 0x5

.field public static final Padlock_clearTextColor:I = 0x6

.field public static final Padlock_clearTextDisabledColor:I = 0x7

.field public static final Padlock_deleteType:I = 0x8

.field public static final Padlock_digitColor:I = 0x9

.field public static final Padlock_digitDisabledColor:I = 0xa

.field public static final Padlock_drawDividerLines:I = 0xb

.field public static final Padlock_drawLeftLine:I = 0xc

.field public static final Padlock_drawRightLine:I = 0xd

.field public static final Padlock_drawTopLine:I = 0xe

.field public static final Padlock_horizontalDividerStyle:I = 0xf

.field public static final Padlock_lettersColor:I = 0x10

.field public static final Padlock_lettersDisabledColor:I = 0x11

.field public static final Padlock_lettersTextSize:I = 0x12

.field public static final Padlock_lineColor:I = 0x13

.field public static final Padlock_pinMode:I = 0x14

.field public static final Padlock_pinSubmitColor:I = 0x15

.field public static final Padlock_showDecimal:I = 0x16

.field public static final Padlock_submitColor:I = 0x17

.field public static final Padlock_submitDisabledColor:I = 0x18

.field public static final Padlock_submitSelector:I = 0x19

.field public static final Padlock_submitType:I = 0x1a

.field public static final Padlock_tabletMode:I = 0x1b

.field public static final PairLayout:[I

.field public static final PairLayout_android_orientation:I = 0x0

.field public static final PairLayout_childDimen:I = 0x1

.field public static final PairLayout_childPercentage:I = 0x2

.field public static final PairLayout_primaryChild:I = 0x3

.field public static final PopupWindow:[I

.field public static final PopupWindowBackgroundState:[I

.field public static final PopupWindowBackgroundState_state_above_anchor:I = 0x0

.field public static final PopupWindow_android_popupAnimationStyle:I = 0x1

.field public static final PopupWindow_android_popupBackground:I = 0x0

.field public static final PopupWindow_overlapAnchor:I = 0x2

.field public static final PreservedLabelView:[I

.field public static final PreservedLabelView_allowMultiLine:I = 0x0

.field public static final PreservedLabelView_preservedRowLayoutLabelStyle:I = 0x1

.field public static final PreservedLabelView_preservedRowLayoutTitleStyle:I = 0x2

.field public static final PreservedLabelView_spacing:I = 0x3

.field public static final PreservedLabelView_verticalSpacing:I = 0x4

.field public static final QuickAmountsView:[I

.field public static final QuickAmountsView_amountCount:I = 0x1

.field public static final QuickAmountsView_android_textColor:I = 0x0

.field public static final QuickAmountsView_buttonSelector:I = 0x2

.field public static final QuickAmountsView_drawBottomLine:I = 0x3

.field public static final QuickAmountsView_drawTopLine:I = 0x4

.field public static final QuickAmountsView_lineColor:I = 0x5

.field public static final QuickAmountsView_lineWidth:I = 0x6

.field public static final RectangularGridLayout:[I

.field public static final RectangularGridLayout_columnCount:I = 0x0

.field public static final RectangularGridLayout_gap:I = 0x1

.field public static final RectangularGridLayout_minCellWidth:I = 0x2

.field public static final RectangularGridLayout_rowHeight:I = 0x3

.field public static final RecycleListView:[I

.field public static final RecycleListView_paddingBottomNoButtons:I = 0x0

.field public static final RecycleListView_paddingTopNoTitle:I = 0x1

.field public static final RecyclerView:[I

.field public static final RecyclerView_android_clipToPadding:I = 0x1

.field public static final RecyclerView_android_descendantFocusability:I = 0x2

.field public static final RecyclerView_android_orientation:I = 0x0

.field public static final RecyclerView_fastScrollEnabled:I = 0x3

.field public static final RecyclerView_fastScrollHorizontalThumbDrawable:I = 0x4

.field public static final RecyclerView_fastScrollHorizontalTrackDrawable:I = 0x5

.field public static final RecyclerView_fastScrollVerticalThumbDrawable:I = 0x6

.field public static final RecyclerView_fastScrollVerticalTrackDrawable:I = 0x7

.field public static final RecyclerView_layoutManager:I = 0x8

.field public static final RecyclerView_reverseLayout:I = 0x9

.field public static final RecyclerView_spanCount:I = 0xa

.field public static final RecyclerView_stackFromEnd:I = 0xb

.field public static final ResizingConfirmButton:[I

.field public static final ResizingConfirmButton_android_paddingLeft:I = 0x1

.field public static final ResizingConfirmButton_android_paddingRight:I = 0x2

.field public static final ResizingConfirmButton_android_textColor:I = 0x0

.field public static final ResizingConfirmButton_buttonBackground:I = 0x3

.field public static final ResizingConfirmButton_confirmStageText:I = 0x4

.field public static final ResizingConfirmButton_initialStageText:I = 0x5

.field public static final ResizingConfirmButton_weight:I = 0x6

.field public static final ResponsiveView:[I

.field public static final ResponsiveView_isResponsive:I = 0x0

.field public static final SVGImageView:[I

.field public static final SVGImageView_svg:I = 0x0

.field public static final ScalingRadioButton:[I

.field public static final ScalingRadioButton_android_includeFontPadding:I = 0x0

.field public static final ScalingRadioButton_minTextSize:I = 0x1

.field public static final ScalingRadioButton_weight:I = 0x2

.field public static final ScalingTextView:[I

.field public static final ScalingTextView_android_includeFontPadding:I = 0x0

.field public static final ScalingTextView_android_shadowColor:I = 0x1

.field public static final ScalingTextView_android_shadowDx:I = 0x2

.field public static final ScalingTextView_android_shadowDy:I = 0x3

.field public static final ScalingTextView_android_shadowRadius:I = 0x4

.field public static final ScalingTextView_minTextSize:I = 0x5

.field public static final ScalingTextView_weight:I = 0x6

.field public static final ScrimInsetsFrameLayout:[I

.field public static final ScrimInsetsFrameLayout_insetForeground:I = 0x0

.field public static final ScrollingViewBehavior_Layout:[I

.field public static final ScrollingViewBehavior_Layout_behavior_overlapTop:I = 0x0

.field public static final SearchView:[I

.field public static final SearchView_android_focusable:I = 0x0

.field public static final SearchView_android_imeOptions:I = 0x3

.field public static final SearchView_android_inputType:I = 0x2

.field public static final SearchView_android_maxWidth:I = 0x1

.field public static final SearchView_closeIcon:I = 0x4

.field public static final SearchView_commitIcon:I = 0x5

.field public static final SearchView_defaultQueryHint:I = 0x6

.field public static final SearchView_goIcon:I = 0x7

.field public static final SearchView_iconifiedByDefault:I = 0x8

.field public static final SearchView_layout:I = 0x9

.field public static final SearchView_queryBackground:I = 0xa

.field public static final SearchView_queryHint:I = 0xb

.field public static final SearchView_searchHintIcon:I = 0xc

.field public static final SearchView_searchIcon:I = 0xd

.field public static final SearchView_submitBackground:I = 0xe

.field public static final SearchView_suggestionRowLayout:I = 0xf

.field public static final SearchView_voiceIcon:I = 0x10

.field public static final SectionHeaderRow:[I

.field public static final SectionHeaderRow_sectionHeaderRowStyle:I = 0x0

.field public static final SecureTouchKeyView:[I

.field public static final SecureTouchKeyView_digitText:I = 0x0

.field public static final SecureTouchKeyView_subText:I = 0x1

.field public static final SelectableAddressLayout:[I

.field public static final SelectableAddressLayout_sqSelectable:I = 0x0

.field public static final SelectableAddressLayout_sqSelected:I = 0x1

.field public static final SetupGuideIntroCard:[I

.field public static final SetupGuideIntroCard_subtitle:I = 0x0

.field public static final SetupGuideIntroCard_title:I = 0x1

.field public static final ShapeAppearance:[I

.field public static final ShapeAppearance_cornerFamily:I = 0x0

.field public static final ShapeAppearance_cornerFamilyBottomLeft:I = 0x1

.field public static final ShapeAppearance_cornerFamilyBottomRight:I = 0x2

.field public static final ShapeAppearance_cornerFamilyTopLeft:I = 0x3

.field public static final ShapeAppearance_cornerFamilyTopRight:I = 0x4

.field public static final ShapeAppearance_cornerSize:I = 0x5

.field public static final ShapeAppearance_cornerSizeBottomLeft:I = 0x6

.field public static final ShapeAppearance_cornerSizeBottomRight:I = 0x7

.field public static final ShapeAppearance_cornerSizeTopLeft:I = 0x8

.field public static final ShapeAppearance_cornerSizeTopRight:I = 0x9

.field public static final ShorteningTextView:[I

.field public static final ShorteningTextView_ellipsizeShortText:I = 0x0

.field public static final ShorteningTextView_shortText:I = 0x1

.field public static final SignInButton:[I

.field public static final SignInButton_buttonSize:I = 0x0

.field public static final SignInButton_colorScheme:I = 0x1

.field public static final SignInButton_scopeUris:I = 0x2

.field public static final SlidingTwoTabLayout:[I

.field public static final SlidingTwoTabLayout_indicatorColor:I = 0x0

.field public static final SlidingTwoTabLayout_indicatorHeight:I = 0x1

.field public static final SmartLineRow:[I

.field public static final SmartLineRow_chevronVisibility:I = 0x0

.field public static final SmartLineRow_preserveValueText:I = 0x1

.field public static final SmartLineRow_sq_subtitleTextAppearance:I = 0x2

.field public static final SmartLineRow_sq_titleTextAppearance:I = 0x3

.field public static final SmartLineRow_sq_valueColor:I = 0x4

.field public static final SmartLineRow_sq_valueTextAppearance:I = 0x5

.field public static final SmartLineRow_subtitleText:I = 0x6

.field public static final SmartLineRow_titleText:I = 0x7

.field public static final SmartLineRow_titleTextColor:I = 0x8

.field public static final SmartLineRow_weight:I = 0x9

.field public static final Snackbar:[I

.field public static final SnackbarLayout:[I

.field public static final SnackbarLayout_actionTextColorAlpha:I = 0x1

.field public static final SnackbarLayout_android_maxWidth:I = 0x0

.field public static final SnackbarLayout_animationMode:I = 0x2

.field public static final SnackbarLayout_backgroundOverlayColorAlpha:I = 0x3

.field public static final SnackbarLayout_elevation:I = 0x4

.field public static final SnackbarLayout_maxActionInlineWidth:I = 0x5

.field public static final Snackbar_snackbarButtonStyle:I = 0x0

.field public static final Snackbar_snackbarStyle:I = 0x1

.field public static final Spinner:[I

.field public static final Spinner_android_dropDownWidth:I = 0x3

.field public static final Spinner_android_entries:I = 0x0

.field public static final Spinner_android_popupBackground:I = 0x1

.field public static final Spinner_android_prompt:I = 0x2

.field public static final Spinner_popupTheme:I = 0x4

.field public static final SquareCardPreview:[I

.field public static final SquareCardPreview_sqCardCornerRadius:I = 0x0

.field public static final SquareCardPreview_sqCardOrientation:I = 0x1

.field public static final SquareCardPreview_sqMaxCardWidth:I = 0x2

.field public static final SquareGlyphView:[I

.field public static final SquareGlyphView_android_textColor:I = 0x0

.field public static final SquareGlyphView_glyph:I = 0x1

.field public static final SquareGlyphView_glyphFontSizeOverride:I = 0x2

.field public static final SquareGlyphView_glyphShadowColor:I = 0x3

.field public static final SquareGlyphView_glyphShadowDx:I = 0x4

.field public static final SquareGlyphView_glyphShadowDy:I = 0x5

.field public static final SquareGlyphView_glyphShadowRadius:I = 0x6

.field public static final StarGroup:[I

.field public static final StarGroup_starFailColor:I = 0x0

.field public static final StarGroup_starGap:I = 0x1

.field public static final StarGroup_starRadius:I = 0x2

.field public static final StarGroup_starSuccessColor:I = 0x3

.field public static final StarsView:[I

.field public static final StarsView_starsCount:I = 0x0

.field public static final StateListDrawable:[I

.field public static final StateListDrawableItem:[I

.field public static final StateListDrawableItem_android_drawable:I = 0x0

.field public static final StateListDrawable_android_constantSize:I = 0x3

.field public static final StateListDrawable_android_dither:I = 0x0

.field public static final StateListDrawable_android_enterFadeDuration:I = 0x4

.field public static final StateListDrawable_android_exitFadeDuration:I = 0x5

.field public static final StateListDrawable_android_variablePadding:I = 0x2

.field public static final StateListDrawable_android_visible:I = 0x1

.field public static final StateListVectorDrawableItem:[I

.field public static final StateListVectorDrawableItem_android_drawable:I = 0x0

.field public static final StateListVectorDrawableItem_defaultStyle:I = 0x1

.field public static final StateListVectorDrawableItem_vectorStyle:I = 0x2

.field public static final StickyListHeadersListView:[I

.field public static final StickyListHeadersListView_android_cacheColorHint:I = 0xe

.field public static final StickyListHeadersListView_android_choiceMode:I = 0x11

.field public static final StickyListHeadersListView_android_clipToPadding:I = 0x8

.field public static final StickyListHeadersListView_android_divider:I = 0xf

.field public static final StickyListHeadersListView_android_dividerHeight:I = 0x10

.field public static final StickyListHeadersListView_android_drawSelectorOnTop:I = 0xa

.field public static final StickyListHeadersListView_android_fadingEdgeLength:I = 0x7

.field public static final StickyListHeadersListView_android_fastScrollAlwaysVisible:I = 0x14

.field public static final StickyListHeadersListView_android_fastScrollEnabled:I = 0x12

.field public static final StickyListHeadersListView_android_listSelector:I = 0x9

.field public static final StickyListHeadersListView_android_overScrollMode:I = 0x13

.field public static final StickyListHeadersListView_android_padding:I = 0x1

.field public static final StickyListHeadersListView_android_paddingBottom:I = 0x5

.field public static final StickyListHeadersListView_android_paddingLeft:I = 0x2

.field public static final StickyListHeadersListView_android_paddingRight:I = 0x4

.field public static final StickyListHeadersListView_android_paddingTop:I = 0x3

.field public static final StickyListHeadersListView_android_requiresFadingEdge:I = 0x15

.field public static final StickyListHeadersListView_android_scrollbarStyle:I = 0x0

.field public static final StickyListHeadersListView_android_scrollbars:I = 0x6

.field public static final StickyListHeadersListView_android_scrollingCache:I = 0xc

.field public static final StickyListHeadersListView_android_stackFromBottom:I = 0xb

.field public static final StickyListHeadersListView_android_transcriptMode:I = 0xd

.field public static final StickyListHeadersListView_square_doublePaddingForSelector:I = 0x16

.field public static final StickyListHeadersListView_square_hasStickyHeaders:I = 0x17

.field public static final StickyListHeadersListView_square_isDrawingListUnderStickyHeader:I = 0x18

.field public static final StickyListHeadersListView_square_stickyListHeadersListViewStyle:I = 0x19

.field public static final StockCountRow:[I

.field public static final StockCountRow_labelPaddingLeft:I = 0x0

.field public static final StockCountRow_showLabel:I = 0x1

.field public static final StockCountRow_sqHideBorder:I = 0x2

.field public static final StockCountRow_sqLabelLayoutWeight:I = 0x3

.field public static final StockCountRow_stockFieldAlignment:I = 0x4

.field public static final StockCountRow_stockFieldPaddingLeft:I = 0x5

.field public static final SwitchCompat:[I

.field public static final SwitchCompat_android_textOff:I = 0x1

.field public static final SwitchCompat_android_textOn:I = 0x0

.field public static final SwitchCompat_android_thumb:I = 0x2

.field public static final SwitchCompat_showText:I = 0x3

.field public static final SwitchCompat_splitTrack:I = 0x4

.field public static final SwitchCompat_switchMinWidth:I = 0x5

.field public static final SwitchCompat_switchPadding:I = 0x6

.field public static final SwitchCompat_switchTextAppearance:I = 0x7

.field public static final SwitchCompat_thumbTextPadding:I = 0x8

.field public static final SwitchCompat_thumbTint:I = 0x9

.field public static final SwitchCompat_thumbTintMode:I = 0xa

.field public static final SwitchCompat_track:I = 0xb

.field public static final SwitchCompat_trackTint:I = 0xc

.field public static final SwitchCompat_trackTintMode:I = 0xd

.field public static final SwitchMaterial:[I

.field public static final SwitchMaterial_useMaterialThemeColors:I = 0x0

.field public static final SwitcherButton:[I

.field public static final SwitcherButton_android_text:I = 0x0

.field public static final SwitcherButton_applet:I = 0x1

.field public static final SwitcherButton_glyph:I = 0x2

.field public static final TabItem:[I

.field public static final TabItem_android_icon:I = 0x0

.field public static final TabItem_android_layout:I = 0x1

.field public static final TabItem_android_text:I = 0x2

.field public static final TabLayout:[I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TabLayout_tabBackground:I = 0x0

.field public static final TabLayout_tabContentStart:I = 0x1

.field public static final TabLayout_tabGravity:I = 0x2

.field public static final TabLayout_tabIconTint:I = 0x3

.field public static final TabLayout_tabIconTintMode:I = 0x4

.field public static final TabLayout_tabIndicator:I = 0x5

.field public static final TabLayout_tabIndicatorAnimationDuration:I = 0x6

.field public static final TabLayout_tabIndicatorColor:I = 0x7

.field public static final TabLayout_tabIndicatorFullWidth:I = 0x8

.field public static final TabLayout_tabIndicatorGravity:I = 0x9

.field public static final TabLayout_tabIndicatorHeight:I = 0xa
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TabLayout_tabInlineLabel:I = 0xb

.field public static final TabLayout_tabMaxWidth:I = 0xc

.field public static final TabLayout_tabMinWidth:I = 0xd

.field public static final TabLayout_tabMode:I = 0xe

.field public static final TabLayout_tabPadding:I = 0xf

.field public static final TabLayout_tabPaddingBottom:I = 0x10

.field public static final TabLayout_tabPaddingEnd:I = 0x11

.field public static final TabLayout_tabPaddingStart:I = 0x12

.field public static final TabLayout_tabPaddingTop:I = 0x13

.field public static final TabLayout_tabRippleColor:I = 0x14

.field public static final TabLayout_tabSelectedTextColor:I = 0x15
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TabLayout_tabTextAppearance:I = 0x16

.field public static final TabLayout_tabTextColor:I = 0x17

.field public static final TabLayout_tabUnboundedRipple:I = 0x18

.field public static final TagPercentMarginLinearLayout:[I

.field public static final TagPercentMarginLinearLayout_marginPercentage:I = 0x0

.field public static final TextAppearance:[I

.field public static final TextAppearanceSpanCompat:[I

.field public static final TextAppearanceSpanCompat_android_fontFamily:I = 0x0

.field public static final TextAppearance_android_fontFamily:I = 0xa

.field public static final TextAppearance_android_shadowColor:I = 0x6

.field public static final TextAppearance_android_shadowDx:I = 0x7

.field public static final TextAppearance_android_shadowDy:I = 0x8

.field public static final TextAppearance_android_shadowRadius:I = 0x9

.field public static final TextAppearance_android_textColor:I = 0x3

.field public static final TextAppearance_android_textColorHint:I = 0x4

.field public static final TextAppearance_android_textColorLink:I = 0x5

.field public static final TextAppearance_android_textFontWeight:I = 0xb

.field public static final TextAppearance_android_textSize:I = 0x0

.field public static final TextAppearance_android_textStyle:I = 0x2

.field public static final TextAppearance_android_typeface:I = 0x1

.field public static final TextAppearance_fontFamily:I = 0xc

.field public static final TextAppearance_fontVariationSettings:I = 0xd

.field public static final TextAppearance_textAllCaps:I = 0xe

.field public static final TextAppearance_textLocale:I = 0xf

.field public static final TextInputLayout:[I

.field public static final TextInputLayout_android_hint:I = 0x1

.field public static final TextInputLayout_android_textColorHint:I = 0x0

.field public static final TextInputLayout_boxBackgroundColor:I = 0x2

.field public static final TextInputLayout_boxBackgroundMode:I = 0x3

.field public static final TextInputLayout_boxCollapsedPaddingTop:I = 0x4

.field public static final TextInputLayout_boxCornerRadiusBottomEnd:I = 0x5

.field public static final TextInputLayout_boxCornerRadiusBottomStart:I = 0x6

.field public static final TextInputLayout_boxCornerRadiusTopEnd:I = 0x7

.field public static final TextInputLayout_boxCornerRadiusTopStart:I = 0x8

.field public static final TextInputLayout_boxStrokeColor:I = 0x9

.field public static final TextInputLayout_boxStrokeWidth:I = 0xa

.field public static final TextInputLayout_boxStrokeWidthFocused:I = 0xb

.field public static final TextInputLayout_counterEnabled:I = 0xc

.field public static final TextInputLayout_counterMaxLength:I = 0xd

.field public static final TextInputLayout_counterOverflowTextAppearance:I = 0xe

.field public static final TextInputLayout_counterOverflowTextColor:I = 0xf

.field public static final TextInputLayout_counterTextAppearance:I = 0x10

.field public static final TextInputLayout_counterTextColor:I = 0x11

.field public static final TextInputLayout_endIconCheckable:I = 0x12

.field public static final TextInputLayout_endIconContentDescription:I = 0x13

.field public static final TextInputLayout_endIconDrawable:I = 0x14

.field public static final TextInputLayout_endIconMode:I = 0x15

.field public static final TextInputLayout_endIconTint:I = 0x16

.field public static final TextInputLayout_endIconTintMode:I = 0x17

.field public static final TextInputLayout_errorEnabled:I = 0x18

.field public static final TextInputLayout_errorIconDrawable:I = 0x19

.field public static final TextInputLayout_errorIconTint:I = 0x1a

.field public static final TextInputLayout_errorIconTintMode:I = 0x1b

.field public static final TextInputLayout_errorTextAppearance:I = 0x1c

.field public static final TextInputLayout_errorTextColor:I = 0x1d

.field public static final TextInputLayout_helperText:I = 0x1e

.field public static final TextInputLayout_helperTextEnabled:I = 0x1f

.field public static final TextInputLayout_helperTextTextAppearance:I = 0x20

.field public static final TextInputLayout_helperTextTextColor:I = 0x21

.field public static final TextInputLayout_hintAnimationEnabled:I = 0x22

.field public static final TextInputLayout_hintEnabled:I = 0x23

.field public static final TextInputLayout_hintTextAppearance:I = 0x24

.field public static final TextInputLayout_hintTextColor:I = 0x25

.field public static final TextInputLayout_passwordToggleContentDescription:I = 0x26

.field public static final TextInputLayout_passwordToggleDrawable:I = 0x27

.field public static final TextInputLayout_passwordToggleEnabled:I = 0x28

.field public static final TextInputLayout_passwordToggleTint:I = 0x29

.field public static final TextInputLayout_passwordToggleTintMode:I = 0x2a

.field public static final TextInputLayout_shapeAppearance:I = 0x2b

.field public static final TextInputLayout_shapeAppearanceOverlay:I = 0x2c

.field public static final TextInputLayout_startIconCheckable:I = 0x2d

.field public static final TextInputLayout_startIconContentDescription:I = 0x2e

.field public static final TextInputLayout_startIconDrawable:I = 0x2f

.field public static final TextInputLayout_startIconTint:I = 0x30

.field public static final TextInputLayout_startIconTintMode:I = 0x31

.field public static final ThemeEnforcement:[I

.field public static final ThemeEnforcement_android_textAppearance:I = 0x0

.field public static final ThemeEnforcement_enforceMaterialTheme:I = 0x1

.field public static final ThemeEnforcement_enforceTextAppearance:I = 0x2

.field public static final ThemedAlertDialog:[I

.field public static final ThemedAlertDialog_themedAlertDialogTheme:I = 0x0

.field public static final TitlePageIndicator:[I

.field public static final TitlePageIndicator_android_textColor:I = 0x1

.field public static final TitlePageIndicator_android_textSize:I = 0x0

.field public static final TitlePageIndicator_clipPadding:I = 0x2

.field public static final TitlePageIndicator_footerColor:I = 0x3

.field public static final TitlePageIndicator_footerIndicatorHeight:I = 0x4

.field public static final TitlePageIndicator_footerIndicatorStyle:I = 0x5

.field public static final TitlePageIndicator_footerIndicatorUnderlinePadding:I = 0x6

.field public static final TitlePageIndicator_footerLineHeight:I = 0x7

.field public static final TitlePageIndicator_footerPadding:I = 0x8

.field public static final TitlePageIndicator_selectedBold:I = 0x9

.field public static final TitlePageIndicator_selectedColor:I = 0xa

.field public static final TitlePageIndicator_titlePadding:I = 0xb

.field public static final TitlePageIndicator_topPadding:I = 0xc

.field public static final ToggleButtonRow:[I

.field public static final ToggleButtonRow_android_text:I = 0x0

.field public static final ToggleButtonRow_shortText:I = 0x1

.field public static final ToggleButtonRow_toggleButtonRowStyle:I = 0x2

.field public static final ToggleButtonRow_toggleButtonRowTextStyle:I = 0x3

.field public static final ToggleButtonRow_type:I = 0x4

.field public static final TokenView:[I

.field public static final TokenView_android_saveEnabled:I = 0x1

.field public static final TokenView_android_textColor:I = 0x0

.field public static final Toolbar:[I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final Toolbar_android_gravity:I = 0x0

.field public static final Toolbar_android_minHeight:I = 0x1

.field public static final Toolbar_buttonGravity:I = 0x2

.field public static final Toolbar_collapseContentDescription:I = 0x3

.field public static final Toolbar_collapseIcon:I = 0x4

.field public static final Toolbar_contentInsetEnd:I = 0x5

.field public static final Toolbar_contentInsetEndWithActions:I = 0x6

.field public static final Toolbar_contentInsetLeft:I = 0x7

.field public static final Toolbar_contentInsetRight:I = 0x8

.field public static final Toolbar_contentInsetStart:I = 0x9

.field public static final Toolbar_contentInsetStartWithNavigation:I = 0xa

.field public static final Toolbar_logo:I = 0xb

.field public static final Toolbar_logoDescription:I = 0xc

.field public static final Toolbar_maxButtonHeight:I = 0xd

.field public static final Toolbar_menu:I = 0xe

.field public static final Toolbar_navigationContentDescription:I = 0xf

.field public static final Toolbar_navigationIcon:I = 0x10

.field public static final Toolbar_popupTheme:I = 0x11

.field public static final Toolbar_subtitle:I = 0x12

.field public static final Toolbar_subtitleTextAppearance:I = 0x13

.field public static final Toolbar_subtitleTextColor:I = 0x14

.field public static final Toolbar_title:I = 0x15

.field public static final Toolbar_titleMargin:I = 0x16

.field public static final Toolbar_titleMarginBottom:I = 0x17

.field public static final Toolbar_titleMarginEnd:I = 0x18

.field public static final Toolbar_titleMarginStart:I = 0x19

.field public static final Toolbar_titleMarginTop:I = 0x1a

.field public static final Toolbar_titleMargins:I = 0x1b
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final Toolbar_titleTextAppearance:I = 0x1c

.field public static final Toolbar_titleTextColor:I = 0x1d

.field public static final TriangleView:[I

.field public static final TriangleView_triangleColor:I = 0x0

.field public static final UnderlinePageIndicator:[I

.field public static final UnderlinePageIndicator_android_background:I = 0x0

.field public static final UnderlinePageIndicator_fadeDelay:I = 0x1

.field public static final UnderlinePageIndicator_fadeLength:I = 0x2

.field public static final UnderlinePageIndicator_fades:I = 0x3

.field public static final UnderlinePageIndicator_lineWidth:I = 0x4

.field public static final UnderlinePageIndicator_selectedColor:I = 0x5

.field public static final UnderlinePageIndicator_strokeWidth:I = 0x6

.field public static final View:[I

.field public static final ViewBackgroundHelper:[I

.field public static final ViewBackgroundHelper_android_background:I = 0x0

.field public static final ViewBackgroundHelper_backgroundTint:I = 0x1

.field public static final ViewBackgroundHelper_backgroundTintMode:I = 0x2

.field public static final ViewPager2:[I

.field public static final ViewPager2_android_orientation:I = 0x0

.field public static final ViewStubCompat:[I

.field public static final ViewStubCompat_android_id:I = 0x0

.field public static final ViewStubCompat_android_inflatedId:I = 0x2

.field public static final ViewStubCompat_android_layout:I = 0x1

.field public static final View_android_focusable:I = 0x1

.field public static final View_android_theme:I = 0x0

.field public static final View_paddingEnd:I = 0x2

.field public static final View_paddingStart:I = 0x3

.field public static final View_theme:I = 0x4

.field public static final WifiNameRow:[I

.field public static final WifiNameRow_wifiRowBackground:I = 0x0

.field public static final WindowBackground:[I

.field public static final WindowBackground_marinWindowBackground:I = 0x0

.field public static final WrappingNameValueRow:[I

.field public static final WrappingNameValueRow_android_text:I = 0x0

.field public static final WrappingNameValueRow_showPercent:I = 0x1

.field public static final WrappingNameValueRow_value:I = 0x2

.field public static final WrappingNameValueRow_wrappingNameValueRowStyle:I = 0x3

.field public static final X2DatePicker:[I

.field public static final X2DatePicker_cellHeight:I = 0x0

.field public static final X2DatePicker_cellWidth:I = 0x1

.field public static final X2TimePicker:[I

.field public static final X2TimePicker_cellStyle:I = 0x0

.field public static final X2TimePicker_overlayBackground:I = 0x1

.field public static final X2TimePicker_paddingSides:I = 0x2

.field public static final XableEditText:[I

.field public static final XableEditText_android_hint:I = 0x1

.field public static final XableEditText_android_imeOptions:I = 0x4

.field public static final XableEditText_android_inputType:I = 0x3

.field public static final XableEditText_android_maxLength:I = 0x2

.field public static final XableEditText_android_text:I = 0x0

.field public static final XableEditText_dismissOnClick:I = 0x5

.field public static final XableEditText_glyph:I = 0x6

.field public static final XableEditText_glyphSpacing:I = 0x7

.field public static final XableEditText_hideBackground:I = 0x8

.field public static final XableEditText_showXOnFocus:I = 0x9

.field public static final XableEditText_showXOnNonEmptyText:I = 0xa

.field public static final XableEditText_sq_glyphColor:I = 0xb

.field public static final XableEditText_xSelector:I = 0xc

.field public static final calendar_cell:[I

.field public static final calendar_cell_tsquare_state_current_month:I = 0x0

.field public static final calendar_cell_tsquare_state_highlighted:I = 0x1

.field public static final calendar_cell_tsquare_state_range_first:I = 0x2

.field public static final calendar_cell_tsquare_state_range_last:I = 0x3

.field public static final calendar_cell_tsquare_state_range_middle:I = 0x4

.field public static final calendar_cell_tsquare_state_selectable:I = 0x5

.field public static final calendar_cell_tsquare_state_today:I = 0x6

.field public static final telescope_TelescopeLayout:[I

.field public static final telescope_TelescopeLayout_telescope_pointerCount:I = 0x0

.field public static final telescope_TelescopeLayout_telescope_progressColor:I = 0x1

.field public static final telescope_TelescopeLayout_telescope_screenshotChildrenOnly:I = 0x2

.field public static final telescope_TelescopeLayout_telescope_screenshotMode:I = 0x3

.field public static final telescope_TelescopeLayout_telescope_vibrate:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 16

    const/16 v0, 0x1d

    new-array v0, v0, [I

    .line 28842
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/posapp/R$styleable;->ActionBar:[I

    const/4 v0, 0x1

    new-array v1, v0, [I

    const/4 v2, 0x0

    const v3, 0x10100b3

    aput v3, v1, v2

    .line 29274
    sput-object v1, Lcom/squareup/posapp/R$styleable;->ActionBarLayout:[I

    new-array v1, v0, [I

    const v3, 0x101013f

    aput v3, v1, v2

    .line 29317
    sput-object v1, Lcom/squareup/posapp/R$styleable;->ActionMenuItemView:[I

    new-array v1, v2, [I

    .line 29333
    sput-object v1, Lcom/squareup/posapp/R$styleable;->ActionMenuView:[I

    const/4 v1, 0x6

    new-array v3, v1, [I

    .line 29356
    fill-array-data v3, :array_1

    sput-object v3, Lcom/squareup/posapp/R$styleable;->ActionMode:[I

    const/4 v3, 0x2

    new-array v4, v3, [I

    .line 29455
    fill-array-data v4, :array_2

    sput-object v4, Lcom/squareup/posapp/R$styleable;->ActivityChooserView:[I

    const/16 v4, 0x8

    new-array v5, v4, [I

    .line 29511
    fill-array-data v5, :array_3

    sput-object v5, Lcom/squareup/posapp/R$styleable;->AlertDialog:[I

    const/4 v5, 0x3

    new-array v6, v5, [I

    .line 29625
    fill-array-data v6, :array_4

    sput-object v6, Lcom/squareup/posapp/R$styleable;->AmountCell:[I

    new-array v6, v1, [I

    .line 29685
    fill-array-data v6, :array_5

    sput-object v6, Lcom/squareup/posapp/R$styleable;->AnimatedStateListDrawableCompat:[I

    new-array v6, v3, [I

    .line 29776
    fill-array-data v6, :array_6

    sput-object v6, Lcom/squareup/posapp/R$styleable;->AnimatedStateListDrawableItem:[I

    const/4 v6, 0x4

    new-array v7, v6, [I

    .line 29823
    fill-array-data v7, :array_7

    sput-object v7, Lcom/squareup/posapp/R$styleable;->AnimatedStateListDrawableTransition:[I

    new-array v7, v4, [I

    .line 29903
    fill-array-data v7, :array_8

    sput-object v7, Lcom/squareup/posapp/R$styleable;->AppBarLayout:[I

    new-array v7, v6, [I

    .line 30033
    fill-array-data v7, :array_9

    sput-object v7, Lcom/squareup/posapp/R$styleable;->AppBarLayoutStates:[I

    new-array v7, v3, [I

    .line 30097
    fill-array-data v7, :array_a

    sput-object v7, Lcom/squareup/posapp/R$styleable;->AppBarLayout_Layout:[I

    new-array v7, v6, [I

    .line 30164
    fill-array-data v7, :array_b

    sput-object v7, Lcom/squareup/posapp/R$styleable;->AppCompatImageView:[I

    new-array v7, v6, [I

    .line 30252
    fill-array-data v7, :array_c

    sput-object v7, Lcom/squareup/posapp/R$styleable;->AppCompatSeekBar:[I

    const/4 v7, 0x7

    new-array v8, v7, [I

    .line 30342
    fill-array-data v8, :array_d

    sput-object v8, Lcom/squareup/posapp/R$styleable;->AppCompatTextHelper:[I

    const/16 v8, 0x15

    new-array v8, v8, [I

    .line 30503
    fill-array-data v8, :array_e

    sput-object v8, Lcom/squareup/posapp/R$styleable;->AppCompatTextView:[I

    const/16 v8, 0x7d

    new-array v8, v8, [I

    .line 31088
    fill-array-data v8, :array_f

    sput-object v8, Lcom/squareup/posapp/R$styleable;->AppCompatTheme:[I

    new-array v8, v0, [I

    const v9, 0x7f0403ca

    aput v9, v8, v2

    .line 32828
    sput-object v8, Lcom/squareup/posapp/R$styleable;->AppletSectionsListView:[I

    new-array v8, v5, [I

    .line 32861
    fill-array-data v8, :array_10

    sput-object v8, Lcom/squareup/posapp/R$styleable;->AspectRatioImageView:[I

    new-array v8, v3, [I

    .line 32921
    fill-array-data v8, :array_11

    sput-object v8, Lcom/squareup/posapp/R$styleable;->AspectRatioLayout:[I

    new-array v8, v0, [I

    const v9, 0x7f0403c2

    aput v9, v8, v2

    .line 32962
    sput-object v8, Lcom/squareup/posapp/R$styleable;->AspectRatioPadlock:[I

    new-array v8, v3, [I

    .line 32991
    fill-array-data v8, :array_12

    sput-object v8, Lcom/squareup/posapp/R$styleable;->AutoPaddingTextView:[I

    const/4 v8, 0x5

    new-array v9, v8, [I

    .line 33042
    fill-array-data v9, :array_13

    sput-object v9, Lcom/squareup/posapp/R$styleable;->Badge:[I

    new-array v9, v3, [I

    .line 33118
    fill-array-data v9, :array_14

    sput-object v9, Lcom/squareup/posapp/R$styleable;->BarChartView:[I

    new-array v9, v3, [I

    .line 33160
    fill-array-data v9, :array_15

    sput-object v9, Lcom/squareup/posapp/R$styleable;->BorderedLinearLayout:[I

    new-array v9, v0, [I

    const v10, 0x7f040065

    aput v10, v9, v2

    .line 33212
    sput-object v9, Lcom/squareup/posapp/R$styleable;->BorderedScrollView:[I

    new-array v9, v4, [I

    .line 33251
    fill-array-data v9, :array_16

    sput-object v9, Lcom/squareup/posapp/R$styleable;->BottomAppBar:[I

    const/16 v9, 0xc

    new-array v10, v9, [I

    .line 33404
    fill-array-data v10, :array_17

    sput-object v10, Lcom/squareup/posapp/R$styleable;->BottomNavigationView:[I

    const/16 v10, 0xb

    new-array v10, v10, [I

    .line 33615
    fill-array-data v10, :array_18

    sput-object v10, Lcom/squareup/posapp/R$styleable;->BottomSheetBehavior_Layout:[I

    new-array v10, v3, [I

    .line 33800
    fill-array-data v10, :array_19

    sput-object v10, Lcom/squareup/posapp/R$styleable;->BottomSheetIndicator:[I

    new-array v10, v0, [I

    const v11, 0x7f04002e

    aput v11, v10, v2

    .line 33836
    sput-object v10, Lcom/squareup/posapp/R$styleable;->ButtonBarLayout:[I

    new-array v10, v3, [I

    .line 33864
    fill-array-data v10, :array_1a

    sput-object v10, Lcom/squareup/posapp/R$styleable;->BuyerActionBar:[I

    const/16 v10, 0x9

    new-array v11, v10, [I

    .line 33918
    fill-array-data v11, :array_1b

    sput-object v11, Lcom/squareup/posapp/R$styleable;->CalendarPickerView:[I

    new-array v11, v0, [I

    const v12, 0x7f04029d

    aput v12, v11, v2

    .line 34036
    sput-object v11, Lcom/squareup/posapp/R$styleable;->CardBackground:[I

    new-array v11, v8, [I

    .line 34070
    fill-array-data v11, :array_1c

    sput-object v11, Lcom/squareup/posapp/R$styleable;->CardEditor:[I

    new-array v11, v0, [I

    const v12, 0x7f040063

    aput v12, v11, v2

    .line 34142
    sput-object v11, Lcom/squareup/posapp/R$styleable;->CardScreenPhoneLayout:[I

    const/16 v11, 0xd

    new-array v12, v11, [I

    .line 34192
    fill-array-data v12, :array_1d

    sput-object v12, Lcom/squareup/posapp/R$styleable;->CardView:[I

    new-array v12, v0, [I

    const v13, 0x7f0401ad

    aput v13, v12, v2

    .line 34383
    sput-object v12, Lcom/squareup/posapp/R$styleable;->CartEntryView:[I

    new-array v12, v0, [I

    const v13, 0x7f04009c

    aput v13, v12, v2

    .line 34407
    sput-object v12, Lcom/squareup/posapp/R$styleable;->CenteredFrameLayout_LayoutParams:[I

    new-array v12, v11, [I

    .line 34458
    fill-array-data v12, :array_1e

    sput-object v12, Lcom/squareup/posapp/R$styleable;->ChartView:[I

    new-array v12, v8, [I

    .line 34639
    fill-array-data v12, :array_1f

    sput-object v12, Lcom/squareup/posapp/R$styleable;->CheckableGroup:[I

    new-array v12, v3, [I

    .line 34714
    fill-array-data v12, :array_20

    sput-object v12, Lcom/squareup/posapp/R$styleable;->CheckablePreservedLabelRow:[I

    const/16 v12, 0x28

    new-array v12, v12, [I

    .line 34830
    fill-array-data v12, :array_21

    sput-object v12, Lcom/squareup/posapp/R$styleable;->Chip:[I

    new-array v12, v1, [I

    .line 35388
    fill-array-data v12, :array_22

    sput-object v12, Lcom/squareup/posapp/R$styleable;->ChipGroup:[I

    new-array v12, v6, [I

    .line 35490
    fill-array-data v12, :array_23

    sput-object v12, Lcom/squareup/posapp/R$styleable;->CollapsingHeaderView:[I

    const/16 v12, 0x10

    new-array v13, v12, [I

    .line 35592
    fill-array-data v13, :array_24

    sput-object v13, Lcom/squareup/posapp/R$styleable;->CollapsingToolbarLayout:[I

    new-array v13, v3, [I

    .line 35855
    fill-array-data v13, :array_25

    sput-object v13, Lcom/squareup/posapp/R$styleable;->CollapsingToolbarLayout_Layout:[I

    new-array v13, v5, [I

    .line 35903
    fill-array-data v13, :array_26

    sput-object v13, Lcom/squareup/posapp/R$styleable;->ColorStateListItem:[I

    new-array v13, v5, [I

    .line 35952
    fill-array-data v13, :array_27

    sput-object v13, Lcom/squareup/posapp/R$styleable;->ColumnLayout:[I

    new-array v13, v6, [I

    .line 36010
    fill-array-data v13, :array_28

    sput-object v13, Lcom/squareup/posapp/R$styleable;->CompoundButton:[I

    new-array v13, v4, [I

    .line 36102
    fill-array-data v13, :array_29

    sput-object v13, Lcom/squareup/posapp/R$styleable;->ConfirmButton:[I

    const/16 v13, 0x3c

    new-array v13, v13, [I

    .line 36337
    fill-array-data v13, :array_2a

    sput-object v13, Lcom/squareup/posapp/R$styleable;->ConstraintLayout_Layout:[I

    new-array v13, v3, [I

    .line 37239
    fill-array-data v13, :array_2b

    sput-object v13, Lcom/squareup/posapp/R$styleable;->ConstraintLayout_placeholder:[I

    const/16 v13, 0x50

    new-array v13, v13, [I

    .line 37440
    fill-array-data v13, :array_2c

    sput-object v13, Lcom/squareup/posapp/R$styleable;->ConstraintSet:[I

    new-array v13, v3, [I

    .line 38600
    fill-array-data v13, :array_2d

    sput-object v13, Lcom/squareup/posapp/R$styleable;->CoordinatorLayout:[I

    new-array v13, v7, [I

    .line 38662
    fill-array-data v13, :array_2e

    sput-object v13, Lcom/squareup/posapp/R$styleable;->CoordinatorLayout_Layout:[I

    new-array v13, v1, [I

    .line 38838
    fill-array-data v13, :array_2f

    sput-object v13, Lcom/squareup/posapp/R$styleable;->DetailConfirmationView:[I

    new-array v13, v0, [I

    const v14, 0x7f040236

    aput v14, v13, v2

    .line 39144
    sput-object v13, Lcom/squareup/posapp/R$styleable;->DialogCardView:[I

    new-array v13, v6, [I

    .line 39174
    fill-array-data v13, :array_30

    sput-object v13, Lcom/squareup/posapp/R$styleable;->DialogContentView:[I

    new-array v13, v5, [I

    .line 39240
    fill-array-data v13, :array_31

    sput-object v13, Lcom/squareup/posapp/R$styleable;->DigitInputView:[I

    new-array v13, v8, [I

    .line 39296
    fill-array-data v13, :array_32

    sput-object v13, Lcom/squareup/posapp/R$styleable;->DiscountRuleSectionView:[I

    const/16 v13, 0x12

    new-array v13, v13, [I

    .line 39407
    fill-array-data v13, :array_33

    sput-object v13, Lcom/squareup/posapp/R$styleable;->DragSortListView:[I

    new-array v13, v4, [I

    .line 39629
    fill-array-data v13, :array_34

    sput-object v13, Lcom/squareup/posapp/R$styleable;->DrawerArrowToggle:[I

    new-array v13, v6, [I

    .line 39757
    fill-array-data v13, :array_35

    sput-object v13, Lcom/squareup/posapp/R$styleable;->DropDownContainer:[I

    new-array v13, v8, [I

    .line 39829
    fill-array-data v13, :array_36

    sput-object v13, Lcom/squareup/posapp/R$styleable;->EditItemItemOptionAndValuesRow:[I

    new-array v13, v0, [I

    const v14, 0x1010098

    aput v14, v13, v2

    .line 39905
    sput-object v13, Lcom/squareup/posapp/R$styleable;->EmployeeLockButton:[I

    new-array v13, v1, [I

    .line 39944
    fill-array-data v13, :array_37

    sput-object v13, Lcom/squareup/posapp/R$styleable;->ErrorsBarView:[I

    new-array v13, v8, [I

    .line 40055
    fill-array-data v13, :array_38

    sput-object v13, Lcom/squareup/posapp/R$styleable;->ExtendedFloatingActionButton:[I

    new-array v13, v3, [I

    .line 40138
    fill-array-data v13, :array_39

    sput-object v13, Lcom/squareup/posapp/R$styleable;->ExtendedFloatingActionButton_Behavior_Layout:[I

    new-array v13, v7, [I

    .line 40186
    fill-array-data v13, :array_3a

    sput-object v13, Lcom/squareup/posapp/R$styleable;->ExtraTextAppearance:[I

    new-array v13, v12, [I

    .line 40320
    fill-array-data v13, :array_3b

    sput-object v13, Lcom/squareup/posapp/R$styleable;->FloatingActionButton:[I

    new-array v13, v0, [I

    const v14, 0x7f040059

    aput v14, v13, v2

    .line 40567
    sput-object v13, Lcom/squareup/posapp/R$styleable;->FloatingActionButton_Behavior_Layout:[I

    new-array v13, v3, [I

    .line 40594
    fill-array-data v13, :array_3c

    sput-object v13, Lcom/squareup/posapp/R$styleable;->FloatingHeaderBodyRow:[I

    new-array v13, v3, [I

    .line 40630
    fill-array-data v13, :array_3d

    sput-object v13, Lcom/squareup/posapp/R$styleable;->FlowLayout:[I

    new-array v13, v0, [I

    const v14, 0x7f040301

    aput v14, v13, v2

    .line 40672
    sput-object v13, Lcom/squareup/posapp/R$styleable;->FlyingStarsBackgroundView:[I

    new-array v13, v1, [I

    .line 40710
    fill-array-data v13, :array_3e

    sput-object v13, Lcom/squareup/posapp/R$styleable;->FontFamily:[I

    const/16 v13, 0xa

    new-array v14, v13, [I

    .line 40846
    fill-array-data v14, :array_3f

    sput-object v14, Lcom/squareup/posapp/R$styleable;->FontFamilyFont:[I

    new-array v14, v5, [I

    .line 40980
    fill-array-data v14, :array_40

    sput-object v14, Lcom/squareup/posapp/R$styleable;->FontSelection:[I

    new-array v14, v5, [I

    .line 41026
    fill-array-data v14, :array_41

    sput-object v14, Lcom/squareup/posapp/R$styleable;->ForegroundLinearLayout:[I

    new-array v14, v5, [I

    .line 41096
    fill-array-data v14, :array_42

    sput-object v14, Lcom/squareup/posapp/R$styleable;->Fragment:[I

    new-array v14, v3, [I

    .line 41144
    fill-array-data v14, :array_43

    sput-object v14, Lcom/squareup/posapp/R$styleable;->FragmentContainerView:[I

    new-array v14, v13, [I

    .line 41196
    fill-array-data v14, :array_44

    sput-object v14, Lcom/squareup/posapp/R$styleable;->GlyphButtonEditText:[I

    new-array v14, v3, [I

    .line 41841
    fill-array-data v14, :array_45

    sput-object v14, Lcom/squareup/posapp/R$styleable;->GlyphRadioRow:[I

    new-array v14, v0, [I

    const v15, 0x7f0401a0

    aput v15, v14, v2

    .line 41875
    sput-object v14, Lcom/squareup/posapp/R$styleable;->GlyphSelectableEditText:[I

    new-array v14, v9, [I

    .line 42149
    fill-array-data v14, :array_46

    sput-object v14, Lcom/squareup/posapp/R$styleable;->GradientColor:[I

    new-array v14, v3, [I

    .line 42333
    fill-array-data v14, :array_47

    sput-object v14, Lcom/squareup/posapp/R$styleable;->GradientColorItem:[I

    new-array v14, v10, [I

    .line 42386
    fill-array-data v14, :array_48

    sput-object v14, Lcom/squareup/posapp/R$styleable;->HSRoundedImageView:[I

    new-array v14, v5, [I

    .line 42506
    fill-array-data v14, :array_49

    sput-object v14, Lcom/squareup/posapp/R$styleable;->HSTypingIndicatorView:[I

    new-array v14, v0, [I

    const v15, 0x10100c4

    aput v15, v14, v2

    .line 42557
    sput-object v14, Lcom/squareup/posapp/R$styleable;->HalfAndHalfLayout:[I

    const/16 v14, 0x2c

    new-array v14, v14, [I

    .line 42674
    fill-array-data v14, :array_4a

    sput-object v14, Lcom/squareup/posapp/R$styleable;->HelpshiftTheme_Activity:[I

    new-array v14, v6, [I

    .line 43198
    fill-array-data v14, :array_4b

    sput-object v14, Lcom/squareup/posapp/R$styleable;->HoloCompat:[I

    new-array v14, v5, [I

    .line 43268
    fill-array-data v14, :array_4c

    sput-object v14, Lcom/squareup/posapp/R$styleable;->HorizontalThreeChildLayout:[I

    new-array v14, v5, [I

    .line 43313
    fill-array-data v14, :array_4d

    sput-object v14, Lcom/squareup/posapp/R$styleable;->JaggedLineView:[I

    new-array v14, v0, [I

    const v15, 0x7f04045d

    aput v15, v14, v2

    .line 43358
    sput-object v14, Lcom/squareup/posapp/R$styleable;->LabeledProgressBar:[I

    new-array v14, v3, [I

    .line 43384
    fill-array-data v14, :array_4e

    sput-object v14, Lcom/squareup/posapp/R$styleable;->LineChartView:[I

    new-array v14, v9, [I

    .line 43446
    fill-array-data v14, :array_4f

    sput-object v14, Lcom/squareup/posapp/R$styleable;->LineRow:[I

    new-array v14, v0, [I

    const v15, 0x10100c4

    aput v15, v14, v2

    .line 43862
    sput-object v14, Lcom/squareup/posapp/R$styleable;->LinearConstraintLayout:[I

    new-array v14, v10, [I

    .line 43910
    fill-array-data v14, :array_50

    sput-object v14, Lcom/squareup/posapp/R$styleable;->LinearLayoutCompat:[I

    new-array v14, v6, [I

    .line 44080
    fill-array-data v14, :array_51

    sput-object v14, Lcom/squareup/posapp/R$styleable;->LinearLayoutCompat_Layout:[I

    new-array v14, v3, [I

    .line 44180
    fill-array-data v14, :array_52

    sput-object v14, Lcom/squareup/posapp/R$styleable;->ListPopupWindow:[I

    new-array v14, v5, [I

    .line 44226
    fill-array-data v14, :array_53

    sput-object v14, Lcom/squareup/posapp/R$styleable;->LoadingImageView:[I

    new-array v14, v8, [I

    .line 44285
    fill-array-data v14, :array_54

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarinActionBarView:[I

    new-array v14, v5, [I

    .line 44364
    fill-array-data v14, :array_55

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarinAppletTabletLayout:[I

    new-array v14, v10, [I

    .line 44429
    fill-array-data v14, :array_56

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarinGlyphMessage:[I

    new-array v14, v13, [I

    .line 44803
    fill-array-data v14, :array_57

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarinGlyphTextView:[I

    new-array v14, v6, [I

    .line 45209
    fill-array-data v14, :array_58

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarinPageIndicator:[I

    new-array v14, v1, [I

    .line 45281
    fill-array-data v14, :array_59

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarinSpinnerGlyph:[I

    new-array v14, v0, [I

    const v15, 0x7f040315

    aput v15, v14, v2

    .line 45827
    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarinVerticalCaretView:[I

    new-array v14, v0, [I

    const v15, 0x7f040497

    aput v15, v14, v2

    .line 45851
    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarketAutoCompleteTextView:[I

    new-array v14, v0, [I

    aput v15, v14, v2

    .line 45883
    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarketButton:[I

    new-array v14, v0, [I

    aput v15, v14, v2

    .line 45915
    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarketCheckedTextView:[I

    new-array v14, v0, [I

    aput v15, v14, v2

    .line 45947
    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarketEditText:[I

    new-array v14, v0, [I

    aput v15, v14, v2

    .line 45979
    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarketRadioButton:[I

    new-array v14, v0, [I

    aput v15, v14, v2

    .line 46011
    sput-object v14, Lcom/squareup/posapp/R$styleable;->MarketTextView:[I

    new-array v14, v6, [I

    .line 46049
    fill-array-data v14, :array_5a

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MaterialAlertDialog:[I

    new-array v14, v8, [I

    .line 46123
    fill-array-data v14, :array_5b

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MaterialAlertDialogTheme:[I

    const/16 v14, 0x14

    new-array v14, v14, [I

    .line 46236
    fill-array-data v14, :array_5c

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MaterialButton:[I

    new-array v14, v3, [I

    .line 46573
    fill-array-data v14, :array_5d

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MaterialButtonToggleGroup:[I

    new-array v14, v10, [I

    .line 46629
    fill-array-data v14, :array_5e

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MaterialCalendar:[I

    new-array v14, v13, [I

    .line 46768
    fill-array-data v14, :array_5f

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MaterialCalendarItem:[I

    new-array v14, v13, [I

    .line 46940
    fill-array-data v14, :array_60

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MaterialCardView:[I

    new-array v14, v3, [I

    .line 47085
    fill-array-data v14, :array_61

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MaterialCheckBox:[I

    new-array v14, v0, [I

    const v15, 0x7f04048f

    aput v15, v14, v2

    .line 47125
    sput-object v14, Lcom/squareup/posapp/R$styleable;->MaterialRadioButton:[I

    new-array v14, v3, [I

    .line 47155
    fill-array-data v14, :array_62

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MaterialShape:[I

    new-array v14, v3, [I

    .line 47197
    fill-array-data v14, :array_63

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MaterialTextAppearance:[I

    new-array v14, v5, [I

    .line 47245
    fill-array-data v14, :array_64

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MaterialTextView:[I

    new-array v14, v1, [I

    .line 47309
    fill-array-data v14, :array_65

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MenuGroup:[I

    const/16 v14, 0x17

    new-array v14, v14, [I

    .line 47456
    fill-array-data v14, :array_66

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MenuItem:[I

    new-array v14, v10, [I

    .line 47833
    fill-array-data v14, :array_67

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MenuView:[I

    const/16 v14, 0xe

    new-array v14, v14, [I

    .line 47993
    fill-array-data v14, :array_68

    sput-object v14, Lcom/squareup/posapp/R$styleable;->MessageView:[I

    new-array v14, v8, [I

    .line 48221
    fill-array-data v14, :array_69

    sput-object v14, Lcom/squareup/posapp/R$styleable;->NameValueImageRow:[I

    new-array v14, v7, [I

    .line 48325
    fill-array-data v14, :array_6a

    sput-object v14, Lcom/squareup/posapp/R$styleable;->NameValueRow:[I

    const/16 v14, 0x15

    new-array v14, v14, [I

    .line 48496
    fill-array-data v14, :array_6b

    sput-object v14, Lcom/squareup/posapp/R$styleable;->NavigationView:[I

    new-array v14, v5, [I

    .line 48801
    fill-array-data v14, :array_6c

    sput-object v14, Lcom/squareup/posapp/R$styleable;->NohoActionBar:[I

    const/16 v14, 0xe

    new-array v14, v14, [I

    .line 48877
    fill-array-data v14, :array_6d

    sput-object v14, Lcom/squareup/posapp/R$styleable;->NohoButton:[I

    new-array v14, v8, [I

    .line 49091
    fill-array-data v14, :array_6e

    sput-object v14, Lcom/squareup/posapp/R$styleable;->NohoCheckableGroup:[I

    new-array v14, v0, [I

    const v15, 0x7f04036e

    aput v15, v14, v2

    .line 49160
    sput-object v14, Lcom/squareup/posapp/R$styleable;->NohoCheckableRow:[I

    new-array v14, v8, [I

    .line 49200
    fill-array-data v14, :array_6f

    sput-object v14, Lcom/squareup/posapp/R$styleable;->NohoConstraintLayout:[I

    new-array v14, v6, [I

    .line 49288
    fill-array-data v14, :array_70

    sput-object v14, Lcom/squareup/posapp/R$styleable;->NohoDoubleButtonRow:[I

    new-array v14, v3, [I

    .line 49364
    fill-array-data v14, :array_71

    sput-object v14, Lcom/squareup/posapp/R$styleable;->NohoDropdown:[I

    new-array v14, v4, [I

    .line 49416
    fill-array-data v14, :array_72

    sput-object v14, Lcom/squareup/posapp/R$styleable;->NohoEdges:[I

    new-array v10, v10, [I

    .line 49550
    fill-array-data v10, :array_73

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoEditRow:[I

    new-array v10, v7, [I

    .line 49688
    fill-array-data v10, :array_74

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoEditText:[I

    new-array v10, v0, [I

    const v14, 0x1010199

    aput v14, v10, v2

    .line 49789
    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoFixedSizeDrawable:[I

    new-array v10, v8, [I

    .line 49823
    fill-array-data v10, :array_75

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoInputBox:[I

    new-array v10, v3, [I

    .line 49896
    fill-array-data v10, :array_76

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoLabel:[I

    new-array v10, v6, [I

    .line 49952
    fill-array-data v10, :array_77

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoLinearLayout:[I

    new-array v10, v5, [I

    .line 50025
    fill-array-data v10, :array_78

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoListView:[I

    new-array v10, v0, [I

    const v14, 0x7f0402e6

    aput v14, v10, v2

    .line 50081
    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoMessageText:[I

    new-array v10, v4, [I

    .line 50121
    fill-array-data v10, :array_79

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoMessageView:[I

    new-array v10, v13, [I

    .line 50242
    fill-array-data v10, :array_7a

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoNumberPicker:[I

    new-array v10, v1, [I

    .line 50404
    fill-array-data v10, :array_7b

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoRecyclerEdges:[I

    new-array v10, v6, [I

    .line 50501
    fill-array-data v10, :array_7c

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoRecyclerEdgesRow:[I

    new-array v10, v5, [I

    .line 50572
    fill-array-data v10, :array_7d

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoRecyclerView:[I

    new-array v10, v5, [I

    .line 50632
    fill-array-data v10, :array_7e

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoResizeDrawable:[I

    new-array v10, v0, [I

    const v14, 0x7f04038d

    aput v14, v10, v2

    .line 50684
    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoResponseLinearLayout:[I

    const/16 v10, 0x1a

    new-array v10, v10, [I

    .line 50758
    fill-array-data v10, :array_7f

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoRow:[I

    new-array v10, v5, [I

    .line 51097
    fill-array-data v10, :array_80

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoRow_Accessory:[I

    new-array v10, v5, [I

    .line 51152
    fill-array-data v10, :array_81

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoRow_ActionIcon:[I

    new-array v10, v6, [I

    .line 51209
    fill-array-data v10, :array_82

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoRow_Icon:[I

    new-array v10, v5, [I

    .line 51277
    fill-array-data v10, :array_83

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoScrollView:[I

    new-array v10, v4, [I

    .line 51347
    fill-array-data v10, :array_84

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoSelectable:[I

    new-array v10, v0, [I

    const v14, 0x7f0402f7

    aput v14, v10, v2

    .line 51450
    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoSpinner:[I

    new-array v10, v5, [I

    .line 51480
    fill-array-data v10, :array_85

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoTextAppearance:[I

    new-array v10, v5, [I

    .line 51536
    fill-array-data v10, :array_86

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NohoTitleValueRow:[I

    new-array v10, v6, [I

    .line 51592
    fill-array-data v10, :array_87

    sput-object v10, Lcom/squareup/posapp/R$styleable;->NotificationButton:[I

    new-array v4, v4, [I

    .line 51661
    fill-array-data v4, :array_88

    sput-object v4, Lcom/squareup/posapp/R$styleable;->NotificationDrawable:[I

    new-array v4, v8, [I

    .line 51780
    fill-array-data v4, :array_89

    sput-object v4, Lcom/squareup/posapp/R$styleable;->NotificationDrawableItem:[I

    new-array v4, v5, [I

    .line 51863
    fill-array-data v4, :array_8a

    sput-object v4, Lcom/squareup/posapp/R$styleable;->NotificationDrawableText:[I

    new-array v4, v0, [I

    const v10, 0x7f0402ff

    aput v10, v4, v2

    .line 51915
    sput-object v4, Lcom/squareup/posapp/R$styleable;->NullStateHorizontalScrollView:[I

    new-array v4, v3, [I

    .line 51941
    fill-array-data v4, :array_8b

    sput-object v4, Lcom/squareup/posapp/R$styleable;->NullStateListView:[I

    new-array v4, v0, [I

    const v10, 0x7f040112

    aput v10, v4, v2

    .line 51978
    sput-object v4, Lcom/squareup/posapp/R$styleable;->OrderEntryDrawerButton:[I

    new-array v4, v3, [I

    .line 52006
    fill-array-data v4, :array_8c

    sput-object v4, Lcom/squareup/posapp/R$styleable;->OrderHardwareItemView:[I

    const/16 v4, 0x1c

    new-array v4, v4, [I

    .line 52096
    fill-array-data v4, :array_8d

    sput-object v4, Lcom/squareup/posapp/R$styleable;->Padlock:[I

    new-array v4, v6, [I

    .line 52498
    fill-array-data v4, :array_8e

    sput-object v4, Lcom/squareup/posapp/R$styleable;->PairLayout:[I

    new-array v4, v5, [I

    .line 52572
    fill-array-data v4, :array_8f

    sput-object v4, Lcom/squareup/posapp/R$styleable;->PopupWindow:[I

    new-array v4, v0, [I

    const v10, 0x7f0403e4

    aput v10, v4, v2

    .line 52624
    sput-object v4, Lcom/squareup/posapp/R$styleable;->PopupWindowBackgroundState:[I

    new-array v4, v8, [I

    .line 52657
    fill-array-data v4, :array_90

    sput-object v4, Lcom/squareup/posapp/R$styleable;->PreservedLabelView:[I

    new-array v4, v7, [I

    .line 52752
    fill-array-data v4, :array_91

    sput-object v4, Lcom/squareup/posapp/R$styleable;->QuickAmountsView:[I

    new-array v4, v6, [I

    .line 52854
    fill-array-data v4, :array_92

    sput-object v4, Lcom/squareup/posapp/R$styleable;->RectangularGridLayout:[I

    new-array v4, v3, [I

    .line 52918
    fill-array-data v4, :array_93

    sput-object v4, Lcom/squareup/posapp/R$styleable;->RecycleListView:[I

    new-array v4, v9, [I

    .line 52982
    fill-array-data v4, :array_94

    sput-object v4, Lcom/squareup/posapp/R$styleable;->RecyclerView:[I

    new-array v4, v7, [I

    .line 53161
    fill-array-data v4, :array_95

    sput-object v4, Lcom/squareup/posapp/R$styleable;->ResizingConfirmButton:[I

    new-array v4, v0, [I

    const v9, 0x7f04021a

    aput v9, v4, v2

    .line 53271
    sput-object v4, Lcom/squareup/posapp/R$styleable;->ResponsiveView:[I

    new-array v4, v0, [I

    const v9, 0x7f04040a

    aput v9, v4, v2

    .line 53299
    sput-object v4, Lcom/squareup/posapp/R$styleable;->SVGImageView:[I

    new-array v4, v5, [I

    .line 53332
    fill-array-data v4, :array_96

    sput-object v4, Lcom/squareup/posapp/R$styleable;->ScalingRadioButton:[I

    new-array v4, v7, [I

    .line 53401
    fill-array-data v4, :array_97

    sput-object v4, Lcom/squareup/posapp/R$styleable;->ScalingTextView:[I

    new-array v4, v0, [I

    const v9, 0x7f040216

    aput v9, v4, v2

    .line 53497
    sput-object v4, Lcom/squareup/posapp/R$styleable;->ScrimInsetsFrameLayout:[I

    new-array v4, v0, [I

    const v9, 0x7f04005f

    aput v9, v4, v2

    .line 53526
    sput-object v4, Lcom/squareup/posapp/R$styleable;->ScrollingViewBehavior_Layout:[I

    const/16 v4, 0x11

    new-array v4, v4, [I

    .line 53587
    fill-array-data v4, :array_98

    sput-object v4, Lcom/squareup/posapp/R$styleable;->SearchView:[I

    new-array v4, v0, [I

    const v9, 0x7f04033b

    aput v9, v4, v2

    .line 53883
    sput-object v4, Lcom/squareup/posapp/R$styleable;->SectionHeaderRow:[I

    new-array v4, v3, [I

    .line 53911
    fill-array-data v4, :array_99

    sput-object v4, Lcom/squareup/posapp/R$styleable;->SecureTouchKeyView:[I

    new-array v4, v3, [I

    .line 53951
    fill-array-data v4, :array_9a

    sput-object v4, Lcom/squareup/posapp/R$styleable;->SelectableAddressLayout:[I

    new-array v4, v3, [I

    .line 53987
    fill-array-data v4, :array_9b

    sput-object v4, Lcom/squareup/posapp/R$styleable;->SetupGuideIntroCard:[I

    new-array v4, v13, [I

    .line 54039
    fill-array-data v4, :array_9c

    sput-object v4, Lcom/squareup/posapp/R$styleable;->ShapeAppearance:[I

    new-array v4, v3, [I

    .line 54237
    fill-array-data v4, :array_9d

    sput-object v4, Lcom/squareup/posapp/R$styleable;->ShorteningTextView:[I

    new-array v4, v5, [I

    .line 54275
    fill-array-data v4, :array_9e

    sput-object v4, Lcom/squareup/posapp/R$styleable;->SignInButton:[I

    new-array v4, v3, [I

    .line 54349
    fill-array-data v4, :array_9f

    sput-object v4, Lcom/squareup/posapp/R$styleable;->SlidingTwoTabLayout:[I

    new-array v4, v13, [I

    .line 54413
    fill-array-data v4, :array_a0

    sput-object v4, Lcom/squareup/posapp/R$styleable;->SmartLineRow:[I

    new-array v4, v3, [I

    .line 54572
    fill-array-data v4, :array_a1

    sput-object v4, Lcom/squareup/posapp/R$styleable;->Snackbar:[I

    new-array v4, v1, [I

    .line 54623
    fill-array-data v4, :array_a2

    sput-object v4, Lcom/squareup/posapp/R$styleable;->SnackbarLayout:[I

    new-array v4, v8, [I

    .line 54726
    fill-array-data v4, :array_a3

    sput-object v4, Lcom/squareup/posapp/R$styleable;->Spinner:[I

    new-array v4, v5, [I

    .line 54824
    fill-array-data v4, :array_a4

    sput-object v4, Lcom/squareup/posapp/R$styleable;->SquareCardPreview:[I

    new-array v4, v7, [I

    .line 54897
    fill-array-data v4, :array_a5

    sput-object v4, Lcom/squareup/posapp/R$styleable;->SquareGlyphView:[I

    new-array v4, v6, [I

    .line 55250
    fill-array-data v4, :array_a6

    sput-object v4, Lcom/squareup/posapp/R$styleable;->StarGroup:[I

    new-array v4, v0, [I

    const v9, 0x7f0403de

    aput v9, v4, v2

    .line 55328
    sput-object v4, Lcom/squareup/posapp/R$styleable;->StarsView:[I

    new-array v4, v1, [I

    .line 55361
    fill-array-data v4, :array_a7

    sput-object v4, Lcom/squareup/posapp/R$styleable;->StateListDrawable:[I

    new-array v4, v0, [I

    const v9, 0x1010199

    aput v9, v4, v2

    .line 55450
    sput-object v4, Lcom/squareup/posapp/R$styleable;->StateListDrawableItem:[I

    new-array v4, v5, [I

    .line 55482
    fill-array-data v4, :array_a8

    sput-object v4, Lcom/squareup/posapp/R$styleable;->StateListVectorDrawableItem:[I

    const/16 v4, 0x1a

    new-array v4, v4, [I

    .line 55582
    fill-array-data v4, :array_a9

    sput-object v4, Lcom/squareup/posapp/R$styleable;->StickyListHeadersListView:[I

    new-array v1, v1, [I

    .line 55956
    fill-array-data v1, :array_aa

    sput-object v1, Lcom/squareup/posapp/R$styleable;->StockCountRow:[I

    const/16 v1, 0xe

    new-array v1, v1, [I

    .line 56080
    fill-array-data v1, :array_ab

    sput-object v1, Lcom/squareup/posapp/R$styleable;->SwitchCompat:[I

    new-array v1, v0, [I

    const v4, 0x7f04048f

    aput v4, v1, v2

    .line 56300
    sput-object v1, Lcom/squareup/posapp/R$styleable;->SwitchMaterial:[I

    new-array v1, v5, [I

    .line 56332
    fill-array-data v1, :array_ac

    sput-object v1, Lcom/squareup/posapp/R$styleable;->SwitcherButton:[I

    new-array v1, v5, [I

    .line 56621
    fill-array-data v1, :array_ad

    sput-object v1, Lcom/squareup/posapp/R$styleable;->TabItem:[I

    const/16 v1, 0x19

    new-array v1, v1, [I

    .line 56726
    fill-array-data v1, :array_ae

    sput-object v1, Lcom/squareup/posapp/R$styleable;->TabLayout:[I

    new-array v1, v0, [I

    const v4, 0x7f040294

    aput v4, v1, v2

    .line 57104
    sput-object v1, Lcom/squareup/posapp/R$styleable;->TagPercentMarginLinearLayout:[I

    new-array v1, v12, [I

    .line 57162
    fill-array-data v1, :array_af

    sput-object v1, Lcom/squareup/posapp/R$styleable;->TextAppearance:[I

    new-array v1, v0, [I

    const v4, 0x10103ac

    aput v4, v1, v2

    .line 57383
    sput-object v1, Lcom/squareup/posapp/R$styleable;->TextAppearanceSpanCompat:[I

    const/16 v1, 0x32

    new-array v1, v1, [I

    .line 57508
    fill-array-data v1, :array_b0

    sput-object v1, Lcom/squareup/posapp/R$styleable;->TextInputLayout:[I

    new-array v1, v5, [I

    .line 58242
    fill-array-data v1, :array_b1

    sput-object v1, Lcom/squareup/posapp/R$styleable;->ThemeEnforcement:[I

    new-array v1, v0, [I

    const v4, 0x7f040451

    aput v4, v1, v2

    .line 58298
    sput-object v1, Lcom/squareup/posapp/R$styleable;->ThemedAlertDialog:[I

    new-array v1, v11, [I

    .line 58348
    fill-array-data v1, :array_b2

    sput-object v1, Lcom/squareup/posapp/R$styleable;->TitlePageIndicator:[I

    new-array v1, v8, [I

    .line 58555
    fill-array-data v1, :array_b3

    sput-object v1, Lcom/squareup/posapp/R$styleable;->ToggleButtonRow:[I

    new-array v1, v3, [I

    .line 58634
    fill-array-data v1, :array_b4

    sput-object v1, Lcom/squareup/posapp/R$styleable;->TokenView:[I

    const/16 v1, 0x1e

    new-array v1, v1, [I

    .line 58739
    fill-array-data v1, :array_b5

    sput-object v1, Lcom/squareup/posapp/R$styleable;->Toolbar:[I

    new-array v1, v0, [I

    const v4, 0x7f04047a

    aput v4, v1, v2

    .line 59188
    sput-object v1, Lcom/squareup/posapp/R$styleable;->TriangleView:[I

    new-array v1, v7, [I

    .line 59225
    fill-array-data v1, :array_b6

    sput-object v1, Lcom/squareup/posapp/R$styleable;->UnderlinePageIndicator:[I

    new-array v1, v8, [I

    .line 59333
    fill-array-data v1, :array_b7

    sput-object v1, Lcom/squareup/posapp/R$styleable;->View:[I

    new-array v1, v5, [I

    .line 59432
    fill-array-data v1, :array_b8

    sput-object v1, Lcom/squareup/posapp/R$styleable;->ViewBackgroundHelper:[I

    new-array v1, v0, [I

    const v4, 0x10100c4

    aput v4, v1, v2

    .line 59500
    sput-object v1, Lcom/squareup/posapp/R$styleable;->ViewPager2:[I

    new-array v1, v5, [I

    .line 59535
    fill-array-data v1, :array_b9

    sput-object v1, Lcom/squareup/posapp/R$styleable;->ViewStubCompat:[I

    new-array v1, v0, [I

    const v4, 0x7f040498

    aput v4, v1, v2

    .line 59589
    sput-object v1, Lcom/squareup/posapp/R$styleable;->WifiNameRow:[I

    new-array v0, v0, [I

    const v1, 0x7f0402a2

    aput v1, v0, v2

    .line 59615
    sput-object v0, Lcom/squareup/posapp/R$styleable;->WindowBackground:[I

    new-array v0, v6, [I

    .line 59647
    fill-array-data v0, :array_ba

    sput-object v0, Lcom/squareup/posapp/R$styleable;->WrappingNameValueRow:[I

    new-array v0, v3, [I

    .line 59709
    fill-array-data v0, :array_bb

    sput-object v0, Lcom/squareup/posapp/R$styleable;->X2DatePicker:[I

    new-array v0, v5, [I

    .line 59753
    fill-array-data v0, :array_bc

    sput-object v0, Lcom/squareup/posapp/R$styleable;->X2TimePicker:[I

    new-array v0, v11, [I

    .line 59827
    fill-array-data v0, :array_bd

    sput-object v0, Lcom/squareup/posapp/R$styleable;->XableEditText:[I

    new-array v0, v7, [I

    .line 60300
    fill-array-data v0, :array_be

    sput-object v0, Lcom/squareup/posapp/R$styleable;->calendar_cell:[I

    new-array v0, v8, [I

    .line 60393
    fill-array-data v0, :array_bf

    sput-object v0, Lcom/squareup/posapp/R$styleable;->telescope_TelescopeLayout:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f040042
        0x7f040049
        0x7f04004a
        0x7f0400f3
        0x7f0400f4
        0x7f0400f5
        0x7f0400f6
        0x7f0400f7
        0x7f0400f8
        0x7f040113
        0x7f04012a
        0x7f04012c
        0x7f040151
        0x7f0401b2
        0x7f0401bb
        0x7f0401c2
        0x7f0401c3
        0x7f0401fe
        0x7f04020b
        0x7f040223
        0x7f040292
        0x7f0402ca
        0x7f040318
        0x7f040320
        0x7f040321
        0x7f040402
        0x7f040406
        0x7f04045d
        0x7f04046a
    .end array-data

    :array_1
    .array-data 4
        0x7f040042
        0x7f040049
        0x7f0400cc
        0x7f0401b2
        0x7f040406
        0x7f04046a
    .end array-data

    :array_2
    .array-data 4
        0x7f040167
        0x7f040212
    .end array-data

    :array_3
    .array-data 4
        0x10100f2
        0x7f040084
        0x7f040085
        0x7f040287
        0x7f040288
        0x7f0402c4
        0x7f040350
        0x7f040355
    .end array-data

    :array_4
    .array-data 4
        0x10101e1
        0x7f040031
        0x7f04039b
    .end array-data

    :array_5
    .array-data 4
        0x101011c
        0x1010194
        0x1010195
        0x1010196
        0x101030c
        0x101030d
    .end array-data

    :array_6
    .array-data 4
        0x10100d0
        0x1010199
    .end array-data

    :array_7
    .array-data 4
        0x1010199
        0x1010449
        0x101044a
        0x101044b
    .end array-data

    :array_8
    .array-data 4
        0x10100d4
        0x101048f
        0x1010540
        0x7f040151
        0x7f040168
        0x7f04027b
        0x7f04027c
        0x7f0403ed
    .end array-data

    :array_9
    .array-data 4
        0x7f0403e6
        0x7f0403e7
        0x7f0403e9
        0x7f0403ea
    .end array-data

    :array_a
    .array-data 4
        0x7f040276
        0x7f040277
    .end array-data

    :array_b
    .array-data 4
        0x1010119
        0x7f0403d8
        0x7f04045b
        0x7f04045c
    .end array-data

    :array_c
    .array-data 4
        0x1010142
        0x7f040457
        0x7f040458
        0x7f040459
    .end array-data

    :array_d
    .array-data 4
        0x1010034
        0x101016d
        0x101016e
        0x101016f
        0x1010170
        0x1010392
        0x1010393
    .end array-data

    :array_e
    .array-data 4
        0x1010034
        0x7f04003b
        0x7f04003c
        0x7f04003d
        0x7f04003e
        0x7f04003f
        0x7f04013d
        0x7f04013e
        0x7f04013f
        0x7f040140
        0x7f040142
        0x7f040143
        0x7f040144
        0x7f040145
        0x7f040185
        0x7f04018c
        0x7f040194
        0x7f040237
        0x7f04027e
        0x7f04042f
        0x7f04044c
    .end array-data

    :array_f
    .array-data 4
        0x1010057
        0x10100ae
        0x7f040000
        0x7f040001
        0x7f040002
        0x7f040003
        0x7f040004
        0x7f040005
        0x7f040006
        0x7f040007
        0x7f040008
        0x7f040009
        0x7f04000a
        0x7f04000b
        0x7f04000c
        0x7f04000e
        0x7f04000f
        0x7f040010
        0x7f040011
        0x7f040012
        0x7f040013
        0x7f040014
        0x7f040015
        0x7f040016
        0x7f040017
        0x7f040018
        0x7f040019
        0x7f04001a
        0x7f04001b
        0x7f04001c
        0x7f04001d
        0x7f04001e
        0x7f040022
        0x7f040025
        0x7f040027
        0x7f04002a
        0x7f04002b
        0x7f040038
        0x7f040067
        0x7f040078
        0x7f040079
        0x7f04007a
        0x7f04007b
        0x7f04007c
        0x7f040088
        0x7f040089
        0x7f0400a0
        0x7f0400a7
        0x7f0400d3
        0x7f0400d4
        0x7f0400d5
        0x7f0400d6
        0x7f0400d7
        0x7f0400d8
        0x7f0400d9
        0x7f0400e0
        0x7f0400e1
        0x7f0400e8
        0x7f0400ff
        0x7f04011f
        0x7f040121
        0x7f040122
        0x7f04012d
        0x7f040130
        0x7f040149
        0x7f04014b
        0x7f04014d
        0x7f04014e
        0x7f040150
        0x7f0401c2
        0x7f040209
        0x7f040283
        0x7f040284
        0x7f040285
        0x7f040286
        0x7f040289
        0x7f04028a
        0x7f04028b
        0x7f04028c
        0x7f04028d
        0x7f04028e
        0x7f04028f
        0x7f040290
        0x7f040291
        0x7f04030b
        0x7f04030c
        0x7f04030d
        0x7f040317
        0x7f040319
        0x7f040324
        0x7f040326
        0x7f040327
        0x7f040328
        0x7f040339
        0x7f04033d
        0x7f04033e
        0x7f04033f
        0x7f04035f
        0x7f040360
        0x7f04040d
        0x7f04043a
        0x7f04043c
        0x7f04043d
        0x7f04043e
        0x7f040440
        0x7f040441
        0x7f040442
        0x7f040443
        0x7f040447
        0x7f040448
        0x7f04046f
        0x7f040470
        0x7f040471
        0x7f040472
        0x7f040495
        0x7f040499
        0x7f04049a
        0x7f04049b
        0x7f04049c
        0x7f04049d
        0x7f04049e
        0x7f04049f
        0x7f0404a0
        0x7f0404a1
        0x7f0404a2
    .end array-data

    :array_10
    .array-data 4
        0x7f040132
        0x7f0403c2
        0x7f0403c3
    .end array-data

    :array_11
    .array-data 4
        0x7f0403c2
        0x7f0403c3
    .end array-data

    :array_12
    .array-data 4
        0x7f040039
        0x7f04003a
    .end array-data

    :array_13
    .array-data 4
        0x7f040043
        0x7f040050
        0x7f040052
        0x7f0402ba
        0x7f040300
    .end array-data

    :array_14
    .array-data 4
        0x7f040055
        0x7f0403f6
    .end array-data

    :array_15
    .array-data 4
        0x7f040068
        0x7f04029a
    .end array-data

    :array_16
    .array-data 4
        0x7f04004b
        0x7f040151
        0x7f040172
        0x7f040173
        0x7f040174
        0x7f040175
        0x7f040176
        0x7f0401bc
    .end array-data

    :array_17
    .array-data 4
        0x7f04004b
        0x7f040151
        0x7f04021b
        0x7f04021e
        0x7f040220
        0x7f040221
        0x7f040224
        0x7f040230
        0x7f040231
        0x7f040232
        0x7f040235
        0x7f0402be
    .end array-data

    :array_18
    .array-data 4
        0x1010440
        0x7f04004b
        0x7f04005b
        0x7f04005c
        0x7f04005d
        0x7f04005e
        0x7f040060
        0x7f040061
        0x7f040062
        0x7f040342
        0x7f040345
    .end array-data

    :array_19
    .array-data 4
        0x7f040387
        0x7f0403b0
    .end array-data

    :array_1a
    .array-data 4
        0x7f04008f
        0x7f04034d
    .end array-data

    :array_1b
    .array-data 4
        0x10100d4
        0x7f04047c
        0x7f04047d
        0x7f04047e
        0x7f04047f
        0x7f040480
        0x7f040481
        0x7f040482
        0x7f04048a
    .end array-data

    :array_1c
    .array-data 4
        0x1010095
        0x7f0400eb
        0x7f0401b9
        0x7f040388
        0x7f040389
    .end array-data

    :array_1d
    .array-data 4
        0x101013f
        0x1010140
        0x7f040090
        0x7f040091
        0x7f040092
        0x7f040094
        0x7f040095
        0x7f040096
        0x7f0400f9
        0x7f0400fa
        0x7f0400fb
        0x7f0400fc
        0x7f0400fd
    .end array-data

    :array_1e
    .array-data 4
        0x7f040040
        0x7f040041
        0x7f040114
        0x7f040115
        0x7f0401b8
        0x7f040446
        0x7f04045a
        0x7f0404a4
        0x7f0404a5
        0x7f0404a6
        0x7f0404a7
        0x7f0404a9
        0x7f0404aa
    .end array-data

    :array_1f
    .array-data 4
        0x1010129
        0x7f040217
        0x7f040354
        0x7f040370
        0x7f04048c
    .end array-data

    :array_20
    .array-data 4
        0x7f04009e
        0x7f04009f
    .end array-data

    :array_21
    .array-data 4
        0x1010034
        0x1010098
        0x10100ab
        0x101011f
        0x101014f
        0x10101e5
        0x7f0400a3
        0x7f0400a4
        0x7f0400a6
        0x7f0400ac
        0x7f0400ad
        0x7f0400ae
        0x7f0400b0
        0x7f0400b1
        0x7f0400b2
        0x7f0400b3
        0x7f0400b4
        0x7f0400b5
        0x7f0400b6
        0x7f0400bb
        0x7f0400bc
        0x7f0400bd
        0x7f0400bf
        0x7f0400c5
        0x7f0400c6
        0x7f0400c7
        0x7f0400c8
        0x7f0400c9
        0x7f0400ca
        0x7f0400cb
        0x7f04015e
        0x7f0401ba
        0x7f0401ff
        0x7f040203
        0x7f040330
        0x7f040342
        0x7f040345
        0x7f04034c
        0x7f040449
        0x7f04044d
    .end array-data

    :array_22
    .array-data 4
        0x7f0400a2
        0x7f0400b7
        0x7f0400b8
        0x7f0400b9
        0x7f040356
        0x7f040357
    .end array-data

    :array_23
    .array-data 4
        0x7f0401ae
        0x7f0401b1
        0x7f040304
        0x7f04046d
    .end array-data

    :array_24
    .array-data 4
        0x7f0400cf
        0x7f0400d0
        0x7f0400fe
        0x7f040169
        0x7f04016a
        0x7f04016b
        0x7f04016c
        0x7f04016d
        0x7f04016e
        0x7f04016f
        0x7f040333
        0x7f040336
        0x7f0403f5
        0x7f04045d
        0x7f04045e
        0x7f04046e
    .end array-data

    :array_25
    .array-data 4
        0x7f04023d
        0x7f04023e
    .end array-data

    :array_26
    .array-data 4
        0x10101a5
        0x101031f
        0x7f04002f
    .end array-data

    :array_27
    .array-data 4
        0x1010129
        0x7f0403c6
        0x7f04048c
    .end array-data

    :array_28
    .array-data 4
        0x1010107
        0x7f04007d
        0x7f04008c
        0x7f04008d
    .end array-data

    :array_29
    .array-data 4
        0x7f040083
        0x7f0400ec
        0x7f0400ed
        0x7f0400ee
        0x7f040213
        0x7f040214
        0x7f040215
        0x7f040497
    .end array-data

    :array_2a
    .array-data 4
        0x10100c4
        0x101011f
        0x1010120
        0x101013f
        0x1010140
        0x7f040056
        0x7f040057
        0x7f04009d
        0x7f0400ef
        0x7f0400f0
        0x7f04023f
        0x7f040240
        0x7f040241
        0x7f040242
        0x7f040243
        0x7f040244
        0x7f040245
        0x7f040246
        0x7f040247
        0x7f040248
        0x7f040249
        0x7f04024a
        0x7f04024b
        0x7f04024c
        0x7f04024d
        0x7f04024e
        0x7f04024f
        0x7f040250
        0x7f040251
        0x7f040252
        0x7f040253
        0x7f040254
        0x7f040255
        0x7f040256
        0x7f040257
        0x7f040258
        0x7f040259
        0x7f04025a
        0x7f04025b
        0x7f04025c
        0x7f04025d
        0x7f04025e
        0x7f04025f
        0x7f040260
        0x7f040261
        0x7f040262
        0x7f040263
        0x7f040264
        0x7f040265
        0x7f040266
        0x7f040267
        0x7f04026a
        0x7f04026b
        0x7f04026c
        0x7f04026d
        0x7f04026e
        0x7f04026f
        0x7f040270
        0x7f040271
        0x7f040275
    .end array-data

    :array_2b
    .array-data 4
        0x7f0400f1
        0x7f040155
    .end array-data

    :array_2c
    .array-data 4
        0x10100c4
        0x10100d0
        0x10100dc
        0x10100f4
        0x10100f5
        0x10100f7
        0x10100f8
        0x10100f9
        0x10100fa
        0x101011f
        0x1010120
        0x101013f
        0x1010140
        0x101031f
        0x1010320
        0x1010321
        0x1010322
        0x1010323
        0x1010324
        0x1010325
        0x1010326
        0x1010327
        0x1010328
        0x10103b5
        0x10103b6
        0x10103fa
        0x1010440
        0x7f040056
        0x7f040057
        0x7f04009d
        0x7f0400f0
        0x7f04023f
        0x7f040240
        0x7f040241
        0x7f040242
        0x7f040243
        0x7f040244
        0x7f040245
        0x7f040246
        0x7f040247
        0x7f040248
        0x7f040249
        0x7f04024a
        0x7f04024b
        0x7f04024c
        0x7f04024d
        0x7f04024e
        0x7f04024f
        0x7f040250
        0x7f040251
        0x7f040252
        0x7f040253
        0x7f040254
        0x7f040255
        0x7f040256
        0x7f040257
        0x7f040258
        0x7f040259
        0x7f04025a
        0x7f04025b
        0x7f04025c
        0x7f04025d
        0x7f04025e
        0x7f04025f
        0x7f040260
        0x7f040261
        0x7f040262
        0x7f040263
        0x7f040264
        0x7f040265
        0x7f040266
        0x7f040267
        0x7f04026a
        0x7f04026b
        0x7f04026c
        0x7f04026d
        0x7f04026e
        0x7f04026f
        0x7f040270
        0x7f040271
    .end array-data

    :array_2d
    .array-data 4
        0x7f040233
        0x7f0403eb
    .end array-data

    :array_2e
    .array-data 4
        0x10100b3
        0x7f04023a
        0x7f04023b
        0x7f04023c
        0x7f040268
        0x7f040272
        0x7f040274
    .end array-data

    :array_2f
    .array-data 4
        0x10101e1
        0x7f04008a
        0x7f04015f
        0x7f0401a0
        0x7f0401b3
        0x7f0402bf
    .end array-data

    :array_30
    .array-data 4
        0x7f04011d
        0x7f04011e
        0x7f040120
        0x7f040123
    .end array-data

    :array_31
    .array-data 4
        0x1010095
        0x7f040125
        0x7f040127
    .end array-data

    :array_32
    .array-data 4
        0x10100d6
        0x10100d7
        0x10100d8
        0x10100d9
        0x7f04033c
    .end array-data

    :array_33
    .array-data 4
        0x7f0400c3
        0x7f0400d1
        0x7f040134
        0x7f040135
        0x7f040136
        0x7f040137
        0x7f04014a
        0x7f040187
        0x7f040188
        0x7f040189
        0x7f0402bc
        0x7f04032c
        0x7f04032d
        0x7f04032e
        0x7f040358
        0x7f04035b
        0x7f040478
        0x7f040490
    .end array-data

    :array_34
    .array-data 4
        0x7f040036
        0x7f040037
        0x7f040054
        0x7f0400d2
        0x7f040141
        0x7f04019e
        0x7f04035e
        0x7f040452
    .end array-data

    :array_35
    .array-data 4
        0x7f040147
        0x7f040148
        0x7f04019f
        0x7f040335
    .end array-data

    :array_36
    .array-data 4
        0x1010140
        0x7f04039f
        0x7f0403a0
        0x7f0403a1
        0x7f0403a2
    .end array-data

    :array_37
    .array-data 4
        0x1010098
        0x10100d6
        0x10100d8
        0x1010129
        0x1010329
        0x7f040164
    .end array-data

    :array_38
    .array-data 4
        0x7f040151
        0x7f040170
        0x7f0401ba
        0x7f04034c
        0x7f040353
    .end array-data

    :array_39
    .array-data 4
        0x7f040059
        0x7f04005a
    .end array-data

    :array_3a
    .array-data 4
        0x1010095
        0x1010097
        0x1010098
        0x101038c
        0x10103ac
        0x10104b6
        0x7f040380
    .end array-data

    :array_3b
    .array-data 4
        0x7f04004b
        0x7f04004c
        0x7f040066
        0x7f040151
        0x7f04015e
        0x7f040177
        0x7f040178
        0x7f0401ba
        0x7f0401c5
        0x7f0402bb
        0x7f04031e
        0x7f040330
        0x7f040342
        0x7f040345
        0x7f04034c
        0x7f04048e
    .end array-data

    :array_3c
    .array-data 4
        0x7f040064
        0x7f0401b0
    .end array-data

    :array_3d
    .array-data 4
        0x7f04022c
        0x7f040281
    .end array-data

    :array_3e
    .array-data 4
        0x7f04018d
        0x7f04018e
        0x7f04018f
        0x7f040190
        0x7f040191
        0x7f040192
    .end array-data

    :array_3f
    .array-data 4
        0x1010532
        0x1010533
        0x101053f
        0x101056f
        0x1010570
        0x7f04018b
        0x7f040193
        0x7f040194
        0x7f040195
        0x7f04048b
    .end array-data

    :array_40
    .array-data 4
        0x10103ac
        0x7f04036a
        0x7f04039d
    .end array-data

    :array_41
    .array-data 4
        0x1010109
        0x1010200
        0x7f04019c
    .end array-data

    :array_42
    .array-data 4
        0x1010003
        0x10100d0
        0x10100d1
    .end array-data

    :array_43
    .array-data 4
        0x1010003
        0x10100d1
    .end array-data

    :array_44
    .array-data 4
        0x101014f
        0x1010150
        0x1010220
        0x1010264
        0x7f04007e
        0x7f04007f
        0x7f040080
        0x7f040081
        0x7f0401a0
        0x7f0403c4
    .end array-data

    :array_45
    .array-data 4
        0x101014f
        0x7f040347
    .end array-data

    :array_46
    .array-data 4
        0x101019d
        0x101019e
        0x10101a1
        0x10101a2
        0x10101a3
        0x10101a4
        0x1010201
        0x101020b
        0x1010510
        0x1010511
        0x1010512
        0x1010513
    .end array-data

    :array_47
    .array-data 4
        0x10101a5
        0x1010514
    .end array-data

    :array_48
    .array-data 4
        0x7f0401c9
        0x7f0401ca
        0x7f0401cb
        0x7f0401da
        0x7f0401ed
        0x7f0401ee
        0x7f0401ef
        0x7f0401f0
        0x7f0401f1
    .end array-data

    :array_49
    .array-data 4
        0x7f0401db
        0x7f0401dc
        0x7f0401ec
    .end array-data

    :array_4a
    .array-data 4
        0x7f0401c6
        0x7f0401c7
        0x7f0401c8
        0x7f0401cc
        0x7f0401cd
        0x7f0401ce
        0x7f0401cf
        0x7f0401d0
        0x7f0401d1
        0x7f0401d2
        0x7f0401d3
        0x7f0401d4
        0x7f0401d5
        0x7f0401d6
        0x7f0401d7
        0x7f0401d8
        0x7f0401d9
        0x7f0401dd
        0x7f0401de
        0x7f0401df
        0x7f0401e0
        0x7f0401e1
        0x7f0401e2
        0x7f0401e3
        0x7f0401e4
        0x7f0401e5
        0x7f0401e6
        0x7f0401e7
        0x7f0401e8
        0x7f0401e9
        0x7f0401ea
        0x7f0401eb
        0x7f0401f2
        0x7f0401f3
        0x7f0401f4
        0x7f0401f5
        0x7f0401f6
        0x7f0401f7
        0x7f0401f8
        0x7f0401f9
        0x7f0401fa
        0x7f0401fb
        0x7f0401fc
        0x7f0401fd
    .end array-data

    :array_4b
    .array-data 4
        0x7f0403cd
        0x7f0403ce
        0x7f0403cf
        0x7f0403d0
    .end array-data

    :array_4c
    .array-data 4
        0x7f040186
        0x7f04033a
        0x7f040453
    .end array-data

    :array_4d
    .array-data 4
        0x7f040065
        0x7f040183
        0x7f04047b
    .end array-data

    :array_4e
    .array-data 4
        0x7f040133
        0x7f040280
    .end array-data

    :array_4f
    .array-data 4
        0x7f0400a9
        0x7f04012b
        0x7f0401a0
        0x7f04027f
        0x7f0403c5
        0x7f0403c7
        0x7f0403ca
        0x7f0403cb
        0x7f040466
        0x7f040469
        0x7f040492
        0x7f040497
    .end array-data

    :array_50
    .array-data 4
        0x10100af
        0x10100c4
        0x1010126
        0x1010127
        0x1010128
        0x7f04012c
        0x7f04012f
        0x7f0402bd
        0x7f04034a
    .end array-data

    :array_51
    .array-data 4
        0x10100b3
        0x10100f4
        0x10100f5
        0x1010181
    .end array-data

    :array_52
    .array-data 4
        0x10102ac
        0x10102ad
    .end array-data

    :array_53
    .array-data 4
        0x7f0400c0
        0x7f040207
        0x7f040208
    .end array-data

    :array_54
    .array-data 4
        0x7f040295
        0x7f040296
        0x7f040297
        0x7f040298
        0x7f040299
    .end array-data

    :array_55
    .array-data 4
        0x7f04012e
        0x7f040131
        0x7f040329
    .end array-data

    :array_56
    .array-data 4
        0x10101e1
        0x7f04008e
        0x7f0401a0
        0x7f0401a4
        0x7f0401ab
        0x7f04029e
        0x7f0402bf
        0x7f0402c0
        0x7f0403c4
    .end array-data

    :array_57
    .array-data 4
        0x1010098
        0x7f0400a8
        0x7f0400a9
        0x7f0401a0
        0x7f0401a2
        0x7f0401a3
        0x7f0401a5
        0x7f0401a6
        0x7f0401a7
        0x7f0401a8
    .end array-data

    :array_58
    .array-data 4
        0x7f04020d
        0x7f04020f
        0x7f040210
        0x7f040211
    .end array-data

    :array_59
    .array-data 4
        0x7f04017c
        0x7f04017d
        0x7f0401a0
        0x7f0401a1
        0x7f0402a1
        0x7f040479
    .end array-data

    :array_5a
    .array-data 4
        0x7f040044
        0x7f040045
        0x7f040046
        0x7f040047
    .end array-data

    :array_5b
    .array-data 4
        0x7f0402a4
        0x7f0402a5
        0x7f0402a6
        0x7f0402a7
        0x7f0402a8
    .end array-data

    :array_5c
    .array-data 4
        0x10101b7
        0x10101b8
        0x10101b9
        0x10101ba
        0x10101e5
        0x7f04004b
        0x7f04004c
        0x7f040106
        0x7f040151
        0x7f0401fe
        0x7f040200
        0x7f040201
        0x7f040202
        0x7f040204
        0x7f040205
        0x7f040330
        0x7f040342
        0x7f040345
        0x7f0403f9
        0x7f0403fa
    .end array-data

    :array_5d
    .array-data 4
        0x7f0400a1
        0x7f040357
    .end array-data

    :array_5e
    .array-data 4
        0x101020d
        0x7f040116
        0x7f040117
        0x7f040118
        0x7f040119
        0x7f040325
        0x7f0404ab
        0x7f0404ac
        0x7f0404ad
    .end array-data

    :array_5f
    .array-data 4
        0x10101b7
        0x10101b8
        0x10101b9
        0x10101ba
        0x7f04021c
        0x7f040225
        0x7f040226
        0x7f04022d
        0x7f04022e
        0x7f040232
    .end array-data

    :array_60
    .array-data 4
        0x10101e5
        0x7f040093
        0x7f0400a3
        0x7f0400a5
        0x7f040330
        0x7f040342
        0x7f040345
        0x7f0403e8
        0x7f0403f9
        0x7f0403fa
    .end array-data

    :array_61
    .array-data 4
        0x7f04008c
        0x7f04048f
    .end array-data

    :array_62
    .array-data 4
        0x7f040342
        0x7f040345
    .end array-data

    :array_63
    .array-data 4
        0x101057f
        0x7f04027e
    .end array-data

    :array_64
    .array-data 4
        0x1010034
        0x101057f
        0x7f04027e
    .end array-data

    :array_65
    .array-data 4
        0x101000e
        0x10100d0
        0x1010194
        0x10101de
        0x10101df
        0x10101e0
    .end array-data

    :array_66
    .array-data 4
        0x1010002
        0x101000e
        0x10100d0
        0x1010106
        0x1010194
        0x10101de
        0x10101df
        0x10101e1
        0x10101e2
        0x10101e3
        0x10101e4
        0x10101e5
        0x101026f
        0x7f04000d
        0x7f04001f
        0x7f040021
        0x7f040030
        0x7f0400f2
        0x7f040204
        0x7f040205
        0x7f040302
        0x7f040348
        0x7f040473
    .end array-data

    :array_67
    .array-data 4
        0x10100ae
        0x101012c
        0x101012d
        0x101012e
        0x101012f
        0x1010130
        0x1010131
        0x7f04031a
        0x7f0403fb
    .end array-data

    :array_68
    .array-data 4
        0x1010095
        0x1010097
        0x1010098
        0x10100ab
        0x10100af
        0x101014f
        0x101015f
        0x1010161
        0x1010162
        0x1010163
        0x1010164
        0x7f04015f
        0x7f04044b
        0x7f040497
    .end array-data

    :array_69
    .array-data 4
        0x1010119
        0x101014f
        0x7f0402c5
        0x7f0402c6
        0x7f040347
    .end array-data

    :array_6a
    .array-data 4
        0x101014f
        0x7f0402c5
        0x7f0402c6
        0x7f0402c7
        0x7f040347
        0x7f04034e
        0x7f040491
    .end array-data

    :array_6b
    .array-data 4
        0x10100d4
        0x10100dd
        0x101011f
        0x7f040151
        0x7f0401af
        0x7f04021b
        0x7f04021d
        0x7f04021f
        0x7f040220
        0x7f040221
        0x7f040222
        0x7f040225
        0x7f040226
        0x7f040227
        0x7f040228
        0x7f040229
        0x7f04022a
        0x7f04022b
        0x7f04022f
        0x7f040232
        0x7f0402be
    .end array-data

    :array_6c
    .array-data 4
        0x7f0402cc
        0x7f040379
        0x7f04037a
    .end array-data

    :array_6d
    .array-data 4
        0x1010034
        0x1010098
        0x10100d4
        0x1010140
        0x7f0402cd
        0x7f04036b
        0x7f040379
        0x7f04037a
        0x7f040382
        0x7f04038e
        0x7f04039c
        0x7f0403b2
        0x7f0403b3
        0x7f0403b4
    .end array-data

    :array_6e
    .array-data 4
        0x1010129
        0x7f040370
        0x7f04038c
        0x7f0403b1
        0x7f0403bd
    .end array-data

    :array_6f
    .array-data 4
        0x7f040269
        0x7f040273
        0x7f0402d5
        0x7f040379
        0x7f04037a
    .end array-data

    :array_70
    .array-data 4
        0x7f040396
        0x7f040397
        0x7f0403bb
        0x7f0403bc
    .end array-data

    :array_71
    .array-data 4
        0x7f04039a
        0x7f0403ac
    .end array-data

    :array_72
    .array-data 4
        0x7f040379
        0x7f04037a
        0x7f04037e
        0x7f04037f
        0x7f040382
        0x7f040383
        0x7f040388
        0x7f040389
    .end array-data

    :array_73
    .array-data 4
        0x7f040371
        0x7f04037c
        0x7f04037d
        0x7f040392
        0x7f040394
        0x7f0403a3
        0x7f0403a7
        0x7f0403b8
        0x7f0403c1
    .end array-data

    :array_74
    .array-data 4
        0x7f0402d8
        0x7f04037c
        0x7f040391
        0x7f040392
        0x7f040394
        0x7f04039e
        0x7f0403b8
    .end array-data

    :array_75
    .array-data 4
        0x7f040371
        0x7f040376
        0x7f04037b
        0x7f04038f
        0x7f040390
    .end array-data

    :array_76
    .array-data 4
        0x1010034
        0x7f040395
    .end array-data

    :array_77
    .array-data 4
        0x7f040269
        0x7f040273
        0x7f040379
        0x7f04037a
    .end array-data

    :array_78
    .array-data 4
        0x7f0402e5
        0x7f040373
        0x7f04038b
    .end array-data

    :array_79
    .array-data 4
        0x10101e1
        0x7f0402e7
        0x7f040377
        0x7f040381
        0x7f040398
        0x7f04039b
        0x7f0403a4
        0x7f0403a8
    .end array-data

    :array_7a
    .array-data 4
        0x1010095
        0x1010098
        0x101011f
        0x1010129
        0x101012a
        0x7f0402e8
        0x7f0403ad
        0x7f0403ae
        0x7f0403af
        0x7f0403b9
    .end array-data

    :array_7b
    .array-data 4
        0x10100d6
        0x10100d7
        0x10100d8
        0x10100d9
        0x7f040379
        0x7f04037a
    .end array-data

    :array_7c
    .array-data 4
        0x10100d6
        0x10100d7
        0x10100d8
        0x10100d9
    .end array-data

    :array_7d
    .array-data 4
        0x7f0402f3
        0x7f040373
        0x7f04038b
    .end array-data

    :array_7e
    .array-data 4
        0x1010155
        0x1010159
        0x1010199
    .end array-data

    :array_7f
    .array-data 4
        0x10100d5
        0x10100d7
        0x10100d9
        0x1010140
        0x10103b3
        0x10103b4
        0x7f040362
        0x7f040363
        0x7f040364
        0x7f040365
        0x7f040366
        0x7f040367
        0x7f040368
        0x7f040374
        0x7f040375
        0x7f040384
        0x7f040385
        0x7f040386
        0x7f04038f
        0x7f040390
        0x7f0403a6
        0x7f0403b5
        0x7f0403b6
        0x7f0403ba
        0x7f0403be
        0x7f0403bf
    .end array-data

    :array_80
    .array-data 4
        0x1010121
        0x1010155
        0x1010159
    .end array-data

    :array_81
    .array-data 4
        0x1010121
        0x1010155
        0x1010159
    .end array-data

    :array_82
    .array-data 4
        0x1010030
        0x1010155
        0x1010159
        0x1010206
    .end array-data

    :array_83
    .array-data 4
        0x7f0402f5
        0x7f040373
        0x7f04038b
    .end array-data

    :array_84
    .array-data 4
        0x101000e
        0x1010106
        0x1010140
        0x7f040372
        0x7f04038f
        0x7f040390
        0x7f0403be
        0x7f0403bf
    .end array-data

    :array_85
    .array-data 4
        0x1010095
        0x1010098
        0x10103ac
    .end array-data

    :array_86
    .array-data 4
        0x101014f
        0x7f0402fa
        0x7f0403c0
    .end array-data

    :array_87
    .array-data 4
        0x7f0401bd
        0x7f04020a
        0x7f040408
        0x7f040409
    .end array-data

    :array_88
    .array-data 4
        0x101014f
        0x1010155
        0x1010159
        0x1010199
        0x7f040053
        0x7f040058
        0x7f040184
        0x7f04044e
    .end array-data

    :array_89
    .array-data 4
        0x1010155
        0x1010159
        0x1010199
        0x10101ad
        0x10101ae
    .end array-data

    :array_8a
    .array-data 4
        0x1010034
        0x10100ac
        0x10100ad
    .end array-data

    :array_8b
    .array-data 4
        0x7f0402fe
        0x7f0402ff
    .end array-data

    :array_8c
    .array-data 4
        0x7f04032a
        0x7f04045d
    .end array-data

    :array_8d
    .array-data 4
        0x1010217
        0x7f04004d
        0x7f04004e
        0x7f04004f
        0x7f040086
        0x7f04008b
        0x7f0400c1
        0x7f0400c2
        0x7f04011c
        0x7f040124
        0x7f040126
        0x7f040139
        0x7f04013a
        0x7f04013b
        0x7f04013c
        0x7f0401c4
        0x7f040278
        0x7f040279
        0x7f04027a
        0x7f04027d
        0x7f040313
        0x7f040314
        0x7f040349
        0x7f0403fe
        0x7f0403ff
        0x7f040400
        0x7f040401
        0x7f040429
    .end array-data

    :array_8e
    .array-data 4
        0x10100c4
        0x7f0400aa
        0x7f0400ab
        0x7f04031f
    .end array-data

    :array_8f
    .array-data 4
        0x1010176
        0x10102c9
        0x7f040303
    .end array-data

    :array_90
    .array-data 4
        0x7f04002d
        0x7f04031c
        0x7f04031d
        0x7f04035c
        0x7f040494
    .end array-data

    :array_91
    .array-data 4
        0x1010098
        0x7f040032
        0x7f040086
        0x7f040138
        0x7f04013c
        0x7f04027d
        0x7f040282
    .end array-data

    :array_92
    .array-data 4
        0x7f0400e9
        0x7f04019d
        0x7f0402c1
        0x7f040331
    .end array-data

    :array_93
    .array-data 4
        0x7f040306
        0x7f04030a
    .end array-data

    :array_94
    .array-data 4
        0x10100c4
        0x10100eb
        0x10100f1
        0x7f04017e
        0x7f04017f
        0x7f040180
        0x7f040181
        0x7f040182
        0x7f040239
        0x7f04032f
        0x7f04035d
        0x7f0403d9
    .end array-data

    :array_95
    .array-data 4
        0x1010098
        0x10100d6
        0x10100d8
        0x7f040077
        0x7f0400ed
        0x7f040214
        0x7f040497
    .end array-data

    :array_96
    .array-data 4
        0x101015f
        0x7f0402c2
        0x7f040497
    .end array-data

    :array_97
    .array-data 4
        0x101015f
        0x1010161
        0x1010162
        0x1010163
        0x1010164
        0x7f0402c2
        0x7f040497
    .end array-data

    :array_98
    .array-data 4
        0x10100da
        0x101011f
        0x1010220
        0x1010264
        0x7f0400c5
        0x7f0400ea
        0x7f04011a
        0x7f0401ac
        0x7f040206
        0x7f040238
        0x7f040322
        0x7f040323
        0x7f040337
        0x7f040338
        0x7f0403fd
        0x7f040407
        0x7f040496
    .end array-data

    :array_99
    .array-data 4
        0x7f040128
        0x7f0403fc
    .end array-data

    :array_9a
    .array-data 4
        0x7f0403a9
        0x7f0403ab
    .end array-data

    :array_9b
    .array-data 4
        0x7f040402
        0x7f04045d
    .end array-data

    :array_9c
    .array-data 4
        0x7f040101
        0x7f040102
        0x7f040103
        0x7f040104
        0x7f040105
        0x7f040107
        0x7f040108
        0x7f040109
        0x7f04010a
        0x7f04010b
    .end array-data

    :array_9d
    .array-data 4
        0x7f040154
        0x7f040347
    .end array-data

    :array_9e
    .array-data 4
        0x7f040087
        0x7f0400e4
        0x7f040332
    .end array-data

    :array_9f
    .array-data 4
        0x7f04020c
        0x7f04020e
    .end array-data

    :array_a0
    .array-data 4
        0x7f0400a9
        0x7f04031b
        0x7f0403c8
        0x7f0403c9
        0x7f0403ca
        0x7f0403cc
        0x7f040403
        0x7f040466
        0x7f040468
        0x7f040497
    .end array-data

    :array_a1
    .array-data 4
        0x7f040359
        0x7f04035a
    .end array-data

    :array_a2
    .array-data 4
        0x101011f
        0x7f040020
        0x7f040033
        0x7f040048
        0x7f040151
        0x7f0402b8
    .end array-data

    :array_a3
    .array-data 4
        0x10100b2
        0x1010176
        0x101017b
        0x1010262
        0x7f040318
    .end array-data

    :array_a4
    .array-data 4
        0x7f04036c
        0x7f04036d
        0x7f040399
    .end array-data

    :array_a5
    .array-data 4
        0x1010098
        0x7f0401a0
        0x7f0401a2
        0x7f0401a5
        0x7f0401a6
        0x7f0401a7
        0x7f0401a8
    .end array-data

    :array_a6
    .array-data 4
        0x7f0403da
        0x7f0403db
        0x7f0403dc
        0x7f0403dd
    .end array-data

    :array_a7
    .array-data 4
        0x101011c
        0x1010194
        0x1010195
        0x1010196
        0x101030c
        0x101030d
    .end array-data

    :array_a8
    .array-data 4
        0x1010199
        0x7f04011b
        0x7f040493
    .end array-data

    :array_a9
    .array-data 4
        0x101007f
        0x10100d5
        0x10100d6
        0x10100d7
        0x10100d8
        0x10100d9
        0x10100de
        0x10100e0
        0x10100eb
        0x10100fb
        0x10100fc
        0x10100fd
        0x10100fe
        0x1010100
        0x1010101
        0x1010129
        0x101012a
        0x101012b
        0x1010226
        0x10102c1
        0x1010335
        0x10103a5
        0x7f0403d1
        0x7f0403d2
        0x7f0403d3
        0x7f0403d6
    .end array-data

    :array_aa
    .array-data 4
        0x7f040234
        0x7f04034b
        0x7f040382
        0x7f040392
        0x7f0403f7
        0x7f0403f8
    .end array-data

    :array_ab
    .array-data 4
        0x1010124
        0x1010125
        0x1010142
        0x7f04034f
        0x7f040361
        0x7f04040b
        0x7f04040c
        0x7f04040e
        0x7f040454
        0x7f040455
        0x7f040456
        0x7f040475
        0x7f040476
        0x7f040477
    .end array-data

    :array_ac
    .array-data 4
        0x101014f
        0x7f040035
        0x7f0401a0
    .end array-data

    :array_ad
    .array-data 4
        0x1010002
        0x10100f2
        0x101014f
    .end array-data

    :array_ae
    .array-data 4
        0x7f04040f
        0x7f040410
        0x7f040411
        0x7f040412
        0x7f040413
        0x7f040414
        0x7f040415
        0x7f040416
        0x7f040417
        0x7f040418
        0x7f040419
        0x7f04041a
        0x7f04041b
        0x7f04041c
        0x7f04041d
        0x7f04041e
        0x7f04041f
        0x7f040420
        0x7f040421
        0x7f040422
        0x7f040423
        0x7f040424
        0x7f040426
        0x7f040427
        0x7f040428
    .end array-data

    :array_af
    .array-data 4
        0x1010095
        0x1010096
        0x1010097
        0x1010098
        0x101009a
        0x101009b
        0x1010161
        0x1010162
        0x1010163
        0x1010164
        0x10103ac
        0x1010585
        0x7f04018c
        0x7f040194
        0x7f04042f
        0x7f04044c
    .end array-data

    :array_b0
    .array-data 4
        0x101009a
        0x1010150
        0x7f04006d
        0x7f04006e
        0x7f04006f
        0x7f040070
        0x7f040071
        0x7f040072
        0x7f040073
        0x7f040074
        0x7f040075
        0x7f040076
        0x7f04010c
        0x7f04010d
        0x7f04010e
        0x7f04010f
        0x7f040110
        0x7f040111
        0x7f040156
        0x7f040157
        0x7f040158
        0x7f040159
        0x7f04015a
        0x7f04015b
        0x7f040160
        0x7f040161
        0x7f040162
        0x7f040163
        0x7f040165
        0x7f040166
        0x7f0401b3
        0x7f0401b4
        0x7f0401b5
        0x7f0401b6
        0x7f0401be
        0x7f0401bf
        0x7f0401c0
        0x7f0401c1
        0x7f04030e
        0x7f04030f
        0x7f040310
        0x7f040311
        0x7f040312
        0x7f040342
        0x7f040345
        0x7f0403df
        0x7f0403e0
        0x7f0403e1
        0x7f0403e2
        0x7f0403e3
    .end array-data

    :array_b1
    .array-data 4
        0x1010034
        0x7f04015c
        0x7f04015d
    .end array-data

    :array_b2
    .array-data 4
        0x1010095
        0x1010098
        0x7f0400c4
        0x7f040196
        0x7f040197
        0x7f040198
        0x7f040199
        0x7f04019a
        0x7f04019b
        0x7f040340
        0x7f040341
        0x7f040465
        0x7f040474
    .end array-data

    :array_b3
    .array-data 4
        0x101014f
        0x7f040347
        0x7f04046b
        0x7f04046c
        0x7f04048d
    .end array-data

    :array_b4
    .array-data 4
        0x1010098
        0x10100e7
    .end array-data

    :array_b5
    .array-data 4
        0x10100af
        0x1010140
        0x7f040082
        0x7f0400cd
        0x7f0400ce
        0x7f0400f3
        0x7f0400f4
        0x7f0400f5
        0x7f0400f6
        0x7f0400f7
        0x7f0400f8
        0x7f040292
        0x7f040293
        0x7f0402b9
        0x7f0402be
        0x7f0402c8
        0x7f0402c9
        0x7f040318
        0x7f040402
        0x7f040404
        0x7f040405
        0x7f04045d
        0x7f04045f
        0x7f040460
        0x7f040461
        0x7f040462
        0x7f040463
        0x7f040464
        0x7f040467
        0x7f040468
    .end array-data

    :array_b6
    .array-data 4
        0x10100d4
        0x7f040179
        0x7f04017a
        0x7f04017b
        0x7f040282
        0x7f040341
        0x7f0403fa
    .end array-data

    :array_b7
    .array-data 4
        0x1010000
        0x10100da
        0x7f040307
        0x7f040309
        0x7f04044f
    .end array-data

    :array_b8
    .array-data 4
        0x10100d4
        0x7f04004b
        0x7f04004c
    .end array-data

    :array_b9
    .array-data 4
        0x10100d0
        0x10100f2
        0x10100f3
    .end array-data

    :array_ba
    .array-data 4
        0x101014f
        0x7f04034e
        0x7f040491
        0x7f0404a3
    .end array-data

    :array_bb
    .array-data 4
        0x7f040099
        0x7f04009b
    .end array-data

    :array_bc
    .array-data 4
        0x7f04009a
        0x7f040305
        0x7f040308
    .end array-data

    :array_bd
    .array-data 4
        0x101014f
        0x1010150
        0x1010160
        0x1010220
        0x1010264
        0x7f040129
        0x7f0401a0
        0x7f0401a9
        0x7f0401b7
        0x7f040351
        0x7f040352
        0x7f0403c4
        0x7f0404a8
    .end array-data

    :array_be
    .array-data 4
        0x7f040483
        0x7f040484
        0x7f040485
        0x7f040486
        0x7f040487
        0x7f040488
        0x7f040489
    .end array-data

    :array_bf
    .array-data 4
        0x7f04042a
        0x7f04042b
        0x7f04042c
        0x7f04042d
        0x7f04042e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 28770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
