.class final Lcom/squareup/salesreport/util/ChartSalesSelectionsKt$rangeLabelFormatter$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ChartSalesSelections.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/util/ChartSalesSelectionsKt;->rangeLabelFormatter(Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)Lkotlin/jvm/functions/Function1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Double;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0006\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "range",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $numberFormatter:Lcom/squareup/text/Formatter;


# direct methods
.method constructor <init>(Lcom/squareup/text/Formatter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/util/ChartSalesSelectionsKt$rangeLabelFormatter$1;->$numberFormatter:Lcom/squareup/text/Formatter;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/salesreport/util/ChartSalesSelectionsKt$rangeLabelFormatter$1;->invoke(D)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(D)Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/salesreport/util/ChartSalesSelectionsKt$rangeLabelFormatter$1;->$numberFormatter:Lcom/squareup/text/Formatter;

    double-to-long p1, p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 34
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
