.class public final Lcom/squareup/salesreport/util/ComparisonRangesKt;
.super Ljava/lang/Object;
.source "ComparisonRanges.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "description",
        "",
        "Lcom/squareup/customreport/data/ComparisonRange;",
        "res",
        "Lcom/squareup/util/Res;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final description(Lcom/squareup/customreport/data/ComparisonRange;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$description"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_comparison_same_day_previous_week:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 29
    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_comparison_same_day_previous_year:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 30
    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousDay;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_comparison_previous_day:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 31
    :cond_2
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$Yesterday;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$Yesterday;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_comparison_yesterday:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    .line 32
    :cond_3
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousWeek;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_comparison_previous_week:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 33
    :cond_4
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameWeekPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameWeekPreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_comparison_same_week_previous_year:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 34
    :cond_5
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousMonth;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_comparison_previous_month:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 35
    :cond_6
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameMonthPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameMonthPreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_comparison_same_month_previous_year:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 36
    :cond_7
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_comparison_previous_three_months:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 37
    :cond_8
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameThreeMonthsPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameThreeMonthsPreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 38
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_comparison_same_months_previous_year:I

    .line 37
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 40
    :cond_9
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_comparison_previous_year:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 41
    :cond_a
    sget-object p1, Lcom/squareup/customreport/data/ComparisonRange$NoComparison;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$NoComparison;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    const-string p0, ""

    :goto_0
    return-object p0

    :cond_b
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
