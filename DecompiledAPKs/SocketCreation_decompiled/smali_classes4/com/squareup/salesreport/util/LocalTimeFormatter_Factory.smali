.class public final Lcom/squareup/salesreport/util/LocalTimeFormatter_Factory;
.super Ljava/lang/Object;
.source "LocalTimeFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/salesreport/util/LocalTimeFormatter_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/salesreport/util/LocalTimeFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter_Factory;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/salesreport/util/LocalTimeFormatter_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/salesreport/util/LocalTimeFormatter_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;)Lcom/squareup/salesreport/util/LocalTimeFormatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/salesreport/util/LocalTimeFormatter;

    invoke-direct {v0, p0}, Lcom/squareup/salesreport/util/LocalTimeFormatter;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/salesreport/util/LocalTimeFormatter;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/salesreport/util/LocalTimeFormatter_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/salesreport/util/LocalTimeFormatter_Factory;->newInstance(Ljavax/inject/Provider;)Lcom/squareup/salesreport/util/LocalTimeFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/LocalTimeFormatter_Factory;->get()Lcom/squareup/salesreport/util/LocalTimeFormatter;

    move-result-object v0

    return-object v0
.end method
