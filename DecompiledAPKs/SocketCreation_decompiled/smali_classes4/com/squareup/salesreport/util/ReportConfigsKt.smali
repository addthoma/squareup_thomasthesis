.class public final Lcom/squareup/salesreport/util/ReportConfigsKt;
.super Ljava/lang/Object;
.source "ReportConfigs.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReportConfigs.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReportConfigs.kt\ncom/squareup/salesreport/util/ReportConfigsKt\n*L\n1#1,267:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0000\u001a\u0014\u0010\u0007\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004H\u0000\u001a0\u0010\u0008\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f\u001a(\u0010\u0010\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u001a\u0012\u0010\u0011\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u001a\u0010\u0012\u001a\u00020\u0013*\u00020\u00022\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0000\u001a\u000c\u0010\u0014\u001a\u00020\u000f*\u00020\u0002H\u0000\u001a\u001a\u0010\u0015\u001a\u00020\u000f*\u00020\u00022\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0000\u001a\u000c\u0010\u0016\u001a\u00020\u000f*\u00020\u0002H\u0000\u001a\u0014\u0010\u0017\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0019H\u0000\u001a\u0014\u0010\u001a\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u0019H\u0000\u001a(\u0010\u001b\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u00042\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a8\u0006\u001c"
    }
    d2 = {
        "chartTitle",
        "",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "res",
        "Lcom/squareup/util/Res;",
        "chartSalesSelection",
        "Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;",
        "chartTitleSpan",
        "configDescription",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "localTimeFormatter",
        "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
        "showEmployeeFilter",
        "",
        "dateRangeDescription",
        "employeeSelectionDescription",
        "getCustomRangeSelection",
        "Lcom/squareup/customreport/data/RangeSelection;",
        "isCustomOneMonth",
        "isCustomOneWeek",
        "isCustomThreeMonths",
        "iso8601EndTime",
        "timeZone",
        "Lorg/threeten/bp/ZoneId;",
        "iso8601StartTime",
        "title",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final chartTitle(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$chartTitle"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chartSalesSelection"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 230
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v0

    sget-object v1, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    sget v0, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_title_custom_timespan_format:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    goto :goto_0

    .line 233
    :cond_0
    sget v0, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_title_preset_timespan_format:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 237
    :goto_0
    invoke-static {p0, p1}, Lcom/squareup/salesreport/util/ReportConfigsKt;->chartTitleSpan(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    const-string v1, "time_span"

    invoke-virtual {v0, v1, p0}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 238
    invoke-static {p2, p1}, Lcom/squareup/salesreport/util/ChartSalesSelectionsKt;->chartTitleType(Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string p2, "sales_type"

    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 239
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 240
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final chartTitleSpan(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$chartTitleSpan"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object p0

    .line 247
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_timespan_daily_uppercase:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_4

    .line 248
    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_1
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_timespan_weekly_uppercase:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_4

    .line 249
    :cond_3
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :goto_2
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_timespan_monthly_uppercase:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_4

    .line 250
    :cond_5
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_3

    :cond_6
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 251
    :goto_3
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_timespan_monthly3_uppercase:I

    .line 250
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_4

    .line 252
    :cond_7
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_chart_timespan_yearly_uppercase:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_4

    .line 253
    :cond_8
    sget-object p1, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    const-string p0, ""

    :goto_4
    return-object p0

    :cond_9
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final configDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/LocalTimeFormatter;Z)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Z)",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "$this$configDescription"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localTimeFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v0

    sget-object v1, Lcom/squareup/customreport/data/ComparisonRange$NoComparison;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$NoComparison;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    .line 55
    sget p4, Lcom/squareup/salesreport/impl/R$string;->sales_report_top_bar_subtitle_format:I

    goto :goto_0

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v0

    sget-object v1, Lcom/squareup/customreport/data/ComparisonRange$NoComparison;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$NoComparison;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p4, :cond_1

    .line 58
    sget p4, Lcom/squareup/salesreport/impl/R$string;->sales_report_top_bar_subtitle_format_without_comparison:I

    goto :goto_0

    .line 60
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v0

    sget-object v1, Lcom/squareup/customreport/data/ComparisonRange$NoComparison;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$NoComparison;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    if-nez p4, :cond_2

    .line 61
    sget p4, Lcom/squareup/salesreport/impl/R$string;->sales_report_top_bar_subtitle_format_without_employee:I

    goto :goto_0

    .line 63
    :cond_2
    sget p4, Lcom/squareup/salesreport/impl/R$string;->sales_report_top_bar_subtitle_format_without_employee_and_comparison:I

    .line 52
    :goto_0
    invoke-interface {p1, p4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p4

    .line 66
    invoke-static {p0, p1}, Lcom/squareup/salesreport/util/ReportConfigsKt;->employeeSelectionDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "employee_filter"

    invoke-virtual {p4, v1, v0}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p4

    .line 70
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getThisDeviceOnly()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 71
    sget v0, Lcom/squareup/salesreport/impl/R$string;->customize_report_device_filter:I

    goto :goto_1

    .line 73
    :cond_3
    sget v0, Lcom/squareup/salesreport/impl/R$string;->customize_report_all_devices:I

    .line 69
    :goto_1
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "device_option"

    .line 67
    invoke-virtual {p4, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p4

    .line 79
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v0

    .line 80
    instance-of v0, v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousThreeMonths;

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->comparisonConfig(Lcom/squareup/customreport/data/ReportConfig;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p0

    invoke-static {p0, p3, p1, p2}, Lcom/squareup/salesreport/util/ReportConfigsKt;->dateRangeDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;

    move-result-object p0

    goto :goto_2

    .line 85
    :cond_4
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/salesreport/util/ComparisonRangesKt;->description(Lcom/squareup/customreport/data/ComparisonRange;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    .line 79
    :goto_2
    check-cast p0, Ljava/lang/CharSequence;

    const-string p1, "comparison"

    .line 77
    invoke-virtual {p4, p1, p0}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 88
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 89
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final dateRangeDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "$this$dateRangeDescription"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localTimeFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getAllDay()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/squareup/salesreport/util/RangeSelectionsKt;->dateRangeFormatRes(Lcom/squareup/customreport/data/RangeSelection;ZZ)I

    move-result v0

    .line 127
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 132
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v1

    .line 134
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    invoke-virtual {v3}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 133
    :goto_0
    invoke-static {v1, v2, p2, p3}, Lcom/squareup/salesreport/util/RangeSelectionsKt;->startDateFormatter(Lcom/squareup/customreport/data/RangeSelection;ZLcom/squareup/util/Res;Ljavax/inject/Provider;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v1

    .line 138
    invoke-interface {p3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/util/Locale;

    invoke-virtual {v1, p3}, Lorg/threeten/bp/format/DateTimeFormatter;->withLocale(Ljava/util/Locale;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p3

    .line 139
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    check-cast v1, Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {p3, v1}, Lorg/threeten/bp/format/DateTimeFormatter;->format(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    const-string v1, "start_date"

    .line 130
    invoke-virtual {v0, v1, p3}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    .line 141
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/squareup/salesreport/util/RangeSelectionsKt;->endDateFormatter(Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/util/Res;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p2

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {p2, v0}, Lorg/threeten/bp/format/DateTimeFormatter;->format(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string v0, "end_date"

    invoke-virtual {p3, v0, p2}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 142
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartTime()Lorg/threeten/bp/LocalTime;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/salesreport/util/LocalTimeFormatter;->format(Lorg/threeten/bp/LocalTime;)Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    const-string v0, "start_time"

    invoke-virtual {p2, v0, p3}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 143
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndTime()Lorg/threeten/bp/LocalTime;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/salesreport/util/LocalTimeFormatter;->format(Lorg/threeten/bp/LocalTime;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    const-string p1, "end_time"

    invoke-virtual {p2, p1, p0}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 144
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 145
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final employeeSelectionDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$employeeSelectionDescription"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEmployeeFiltersSelection()Lcom/squareup/customreport/data/EmployeeFiltersSelection;

    move-result-object p0

    .line 205
    sget-object v0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$NoEmployeesSelection;->INSTANCE:Lcom/squareup/customreport/data/EmployeeFiltersSelection$NoEmployeesSelection;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$AllEmployeesSelection;->INSTANCE:Lcom/squareup/customreport/data/EmployeeFiltersSelection$AllEmployeesSelection;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 206
    :goto_0
    sget p0, Lcom/squareup/salesreport/impl/R$string;->customize_report_all_employees:I

    .line 205
    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 208
    :cond_1
    instance-of v0, p0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    if-eqz v0, :cond_3

    .line 209
    check-cast p0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    invoke-virtual {p0}, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->getEmployeeFilters()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 210
    invoke-virtual {p0}, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->getEmployeeFilters()Ljava/util/Set;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->first(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/customreport/data/EmployeeFilter;

    .line 211
    invoke-static {p0, p1}, Lcom/squareup/salesreport/util/EmployeeFiltersKt;->displayValue(Lcom/squareup/customreport/data/EmployeeFilter;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    .line 213
    :cond_2
    sget v0, Lcom/squareup/salesreport/impl/R$string;->customize_report_multiple_employee_filter_description:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 214
    invoke-virtual {p0}, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->getEmployeeFilters()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result p0

    const-string v0, "num_members"

    invoke-virtual {p1, v0, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 215
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 216
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_1
    return-object p0

    .line 209
    :cond_3
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final getCustomRangeSelection(Lcom/squareup/customreport/data/ReportConfig;Ljavax/inject/Provider;)Lcom/squareup/customreport/data/RangeSelection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/customreport/data/RangeSelection;"
        }
    .end annotation

    const-string v0, "$this$getCustomRangeSelection"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection;

    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object p0

    goto :goto_0

    .line 157
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    sget-object p0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;

    check-cast p0, Lcom/squareup/customreport/data/RangeSelection;

    goto :goto_0

    .line 160
    :cond_1
    invoke-static {p0, p1}, Lcom/squareup/salesreport/util/ReportConfigsKt;->isCustomOneWeek(Lcom/squareup/customreport/data/ReportConfig;Ljavax/inject/Provider;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 161
    sget-object p0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;

    check-cast p0, Lcom/squareup/customreport/data/RangeSelection;

    goto :goto_0

    .line 163
    :cond_2
    invoke-static {p0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->isCustomOneMonth(Lcom/squareup/customreport/data/ReportConfig;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 164
    sget-object p0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;

    check-cast p0, Lcom/squareup/customreport/data/RangeSelection;

    goto :goto_0

    .line 166
    :cond_3
    invoke-static {p0}, Lcom/squareup/salesreport/util/ReportConfigsKt;->isCustomThreeMonths(Lcom/squareup/customreport/data/ReportConfig;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 167
    sget-object p0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;

    check-cast p0, Lcom/squareup/customreport/data/RangeSelection;

    goto :goto_0

    .line 170
    :cond_4
    sget-object p0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;

    check-cast p0, Lcom/squareup/customreport/data/RangeSelection;

    :goto_0
    return-object p0
.end method

.method public static final isCustomOneMonth(Lcom/squareup/customreport/data/ReportConfig;)Z
    .locals 2

    const-string v0, "$this$isCustomOneMonth"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 186
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->getMonth()Lorg/threeten/bp/Month;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->getMonth()Lorg/threeten/bp/Month;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->isFirstDayOfMonth(Lorg/threeten/bp/LocalDate;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->isLastDayOfMonth(Lorg/threeten/bp/LocalDate;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isCustomOneWeek(Lcom/squareup/customreport/data/ReportConfig;Ljavax/inject/Provider;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "$this$isCustomOneWeek"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 178
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-wide/16 v1, 0x6

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "localeProvider.get()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/util/Locale;

    invoke-static {v0, v1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->isFirstDayOfWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object p0

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/Locale;

    invoke-static {p0, p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->isLastDayOfWeek(Lorg/threeten/bp/LocalDate;Ljava/util/Locale;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isCustomThreeMonths(Lcom/squareup/customreport/data/ReportConfig;)Z
    .locals 4

    const-string v0, "$this$isCustomThreeMonths"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->getMonth()Lorg/threeten/bp/Month;

    move-result-object v0

    .line 190
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->getMonth()Lorg/threeten/bp/Month;

    move-result-object v1

    const-wide/16 v2, 0x2

    invoke-virtual {v1, v2, v3}, Lorg/threeten/bp/Month;->minus(J)Lorg/threeten/bp/Month;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->isFirstDayOfMonth(Lorg/threeten/bp/LocalDate;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/utilities/threeten/LocalDatesKt;->isLastDayOfMonth(Lorg/threeten/bp/LocalDate;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final iso8601EndTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$iso8601EndTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timeZone"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 266
    invoke-static {p0}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->getEndDateTime(Lcom/squareup/customreport/data/ReportConfig;)Lorg/threeten/bp/LocalDateTime;

    move-result-object p0

    invoke-static {p0, p1}, Lorg/threeten/bp/ZonedDateTime;->of(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p0

    invoke-virtual {p0}, Lorg/threeten/bp/ZonedDateTime;->toInstant()Lorg/threeten/bp/Instant;

    move-result-object p0

    const-string p1, "ZonedDateTime.of(endDate\u2026me, timeZone).toInstant()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/utilities/threeten/InstantsKt;->asIso8601(Lorg/threeten/bp/Instant;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final iso8601StartTime(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/ZoneId;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$iso8601StartTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timeZone"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    invoke-static {p0}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->getStartDateTime(Lcom/squareup/customreport/data/ReportConfig;)Lorg/threeten/bp/LocalDateTime;

    move-result-object p0

    invoke-static {p0, p1}, Lorg/threeten/bp/ZonedDateTime;->of(Lorg/threeten/bp/LocalDateTime;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p0

    invoke-virtual {p0}, Lorg/threeten/bp/ZonedDateTime;->toInstant()Lorg/threeten/bp/Instant;

    move-result-object p0

    const-string p1, "ZonedDateTime.of(startDa\u2026me, timeZone).toInstant()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/utilities/threeten/InstantsKt;->asIso8601(Lorg/threeten/bp/Instant;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final title(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            "Lcom/squareup/salesreport/util/LocalTimeFormatter;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "$this$title"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localTimeFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object v0

    .line 105
    instance-of v1, v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;

    if-eqz v1, :cond_0

    invoke-static {p0, p1, p2, p3}, Lcom/squareup/salesreport/util/ReportConfigsKt;->dateRangeDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 106
    :cond_0
    sget v1, Lcom/squareup/salesreport/impl/R$string;->sales_report_top_bar_title_format:I

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 107
    invoke-static {v0}, Lcom/squareup/salesreport/util/RangeSelectionsKt;->getDescription(Lcom/squareup/customreport/data/RangeSelection;)I

    move-result v0

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v2, "period"

    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 108
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/salesreport/util/ReportConfigsKt;->dateRangeDescription(Lcom/squareup/customreport/data/ReportConfig;Lcom/squareup/salesreport/util/LocalTimeFormatter;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    const-string p1, "date_range"

    invoke-virtual {v0, p1, p0}, Lcom/squareup/phrase/Phrase;->putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 109
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    .line 110
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    :goto_0
    return-object p0
.end method
