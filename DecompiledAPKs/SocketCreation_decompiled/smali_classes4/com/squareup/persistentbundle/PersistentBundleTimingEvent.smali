.class public Lcom/squareup/persistentbundle/PersistentBundleTimingEvent;
.super Lcom/squareup/analytics/event/v1/TimingEvent;
.source "PersistentBundleTimingEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;
    }
.end annotation


# instance fields
.field public final bundleSize:I

.field public final durationFromStart:J

.field public final eventName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;JI)V
    .locals 1

    .line 18
    sget-object v0, Lcom/squareup/analytics/RegisterTimingName;->BUNDLE_PERSISTENCE_TIME:Lcom/squareup/analytics/RegisterTimingName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/TimingEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;)V

    .line 19
    invoke-virtual {p1}, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent$EventType;->name()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent;->eventName:Ljava/lang/String;

    .line 20
    iput-wide p2, p0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent;->durationFromStart:J

    .line 21
    iput p4, p0, Lcom/squareup/persistentbundle/PersistentBundleTimingEvent;->bundleSize:I

    return-void
.end method
