.class public Lcom/squareup/persistentbundle/PersistentBundleManager;
.super Ljava/lang/Object;
.source "PersistentBundleManager.java"


# static fields
.field private static final KEY_PERSISTENT_BUNDLE:Ljava/lang/String; = "persistent-bundle"


# instance fields
.field private final persistentBundleStore:Lcom/squareup/persistentbundle/PersistentBundleStore;


# direct methods
.method constructor <init>(Lcom/squareup/persistentbundle/SharedPreferencesBundleStore;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/persistentbundle/PersistentBundleManager;->persistentBundleStore:Lcom/squareup/persistentbundle/PersistentBundleStore;

    return-void
.end method

.method public static canRestoreFromPersistentBundle(Landroid/os/Bundle;)Z
    .locals 1

    const-string v0, "persistent-bundle"

    .line 30
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public restoreFromPersistentBundle(Landroid/os/Bundle;)V
    .locals 4

    const-string v0, "persistent-bundle"

    .line 61
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/squareup/persistentbundle/PersistentBundleHolder;

    .line 62
    invoke-virtual {v1}, Lcom/squareup/persistentbundle/PersistentBundleHolder;->getUuid()Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-virtual {v1}, Lcom/squareup/persistentbundle/PersistentBundleHolder;->getSavedState()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_0

    .line 66
    iget-object v1, p0, Lcom/squareup/persistentbundle/PersistentBundleManager;->persistentBundleStore:Lcom/squareup/persistentbundle/PersistentBundleStore;

    invoke-interface {v1, v2}, Lcom/squareup/persistentbundle/PersistentBundleStore;->getAndRemove(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    goto :goto_0

    .line 68
    :cond_0
    iget-object v3, p0, Lcom/squareup/persistentbundle/PersistentBundleManager;->persistentBundleStore:Lcom/squareup/persistentbundle/PersistentBundleStore;

    invoke-interface {v3, v2}, Lcom/squareup/persistentbundle/PersistentBundleStore;->remove(Ljava/lang/String;)V

    .line 71
    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    return-void
.end method

.method public saveToPersistentBundle(Landroid/os/Bundle;)V
    .locals 4

    .line 45
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 46
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 47
    invoke-virtual {v1, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 48
    new-instance v2, Lcom/squareup/persistentbundle/PersistentBundleHolder;

    iget-object v3, p0, Lcom/squareup/persistentbundle/PersistentBundleManager;->persistentBundleStore:Lcom/squareup/persistentbundle/PersistentBundleStore;

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/persistentbundle/PersistentBundleHolder;-><init>(Ljava/lang/String;Landroid/os/Bundle;Lcom/squareup/persistentbundle/PersistentBundleStore;)V

    .line 51
    invoke-virtual {p1}, Landroid/os/Bundle;->clear()V

    const-string v0, "persistent-bundle"

    .line 52
    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
