.class final Lcom/squareup/pos/tour/SposWhatsNewTour$SPOS_WHATS_NEW_SALES_REPORTS;
.super Lcom/squareup/pos/tour/SposWhatsNewTour;
.source "SposWhatsNewTour.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pos/tour/SposWhatsNewTour;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SPOS_WHATS_NEW_SALES_REPORTS"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0001\u0018\u00002\u00020\u0001J\u0016\u0010\u0002\u001a\u00020\u00032\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/pos/tour/SposWhatsNewTour$SPOS_WHATS_NEW_SALES_REPORTS;",
        "Lcom/squareup/pos/tour/SposWhatsNewTour;",
        "addPages",
        "",
        "pages",
        "",
        "Lcom/squareup/tour/TourPage;",
        "tour_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 12
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/pos/tour/SposWhatsNewTour;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public addPages(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/tour/TourPage;",
            ">;)V"
        }
    .end annotation

    const-string v0, "pages"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget v0, Lcom/squareup/pos/tour/R$drawable;->spos_whats_new_tour_sales_reports:I

    .line 17
    sget v1, Lcom/squareup/pos/tour/R$string;->spos_whats_new_tour_sales_reports_title:I

    .line 18
    sget v2, Lcom/squareup/pos/tour/R$string;->spos_whats_new_tour_sales_reports_subtitle:I

    .line 15
    invoke-static {v0, v1, v2}, Lcom/squareup/tour/TourPage;->pageBottom(III)Lcom/squareup/tour/TourPage;

    move-result-object v0

    const-string v1, "pageBottom(\n            \u2026orts_subtitle\n          )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
