.class public final Lcom/squareup/pos/tour/SposWhatsNewTourProvider_Factory;
.super Ljava/lang/Object;
.source "SposWhatsNewTourProvider_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/pos/tour/SposWhatsNewTourProvider;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/pos/tour/SposWhatsNewTourProvider_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/pos/tour/SposWhatsNewTourProvider_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/pos/tour/SposWhatsNewTourProvider_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/util/Locale;Lcom/squareup/settings/server/Features;)Lcom/squareup/pos/tour/SposWhatsNewTourProvider;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider;

    invoke-direct {v0, p0, p1}, Lcom/squareup/pos/tour/SposWhatsNewTourProvider;-><init>(Ljava/util/Locale;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/pos/tour/SposWhatsNewTourProvider;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    iget-object v1, p0, Lcom/squareup/pos/tour/SposWhatsNewTourProvider_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1}, Lcom/squareup/pos/tour/SposWhatsNewTourProvider_Factory;->newInstance(Ljava/util/Locale;Lcom/squareup/settings/server/Features;)Lcom/squareup/pos/tour/SposWhatsNewTourProvider;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/pos/tour/SposWhatsNewTourProvider_Factory;->get()Lcom/squareup/pos/tour/SposWhatsNewTourProvider;

    move-result-object v0

    return-object v0
.end method
