.class public final Lcom/squareup/registerlib/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/registerlib/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final activity_applet_sticky_header_padding:I = 0x7f07005d

.field public static final applet_text_size:I = 0x7f070070

.field public static final calendar_month_topmargin:I = 0x7f0700a8

.field public static final cart_header_height:I = 0x7f0700b5

.field public static final cart_header_phone_horizontal_margin:I = 0x7f0700b6

.field public static final cart_modifiers_gap:I = 0x7f0700bd

.field public static final checkout_jumbo_button_height:I = 0x7f0700c9

.field public static final checkout_tablet_row_height:I = 0x7f0700ca

.field public static final crm_cardonfile_unlink_card_button_margin:I = 0x7f0700db

.field public static final crm_cardonfile_unlink_card_glyph_margin:I = 0x7f0700dc

.field public static final crm_customer_input_width:I = 0x7f0700e4

.field public static final dialog_border:I = 0x7f070136

.field public static final discount_switch_item_width:I = 0x7f07013d

.field public static final edit_entry_label_height:I = 0x7f070145

.field public static final edit_entry_label_text_height:I = 0x7f070146

.field public static final edit_entry_label_width:I = 0x7f070147

.field public static final edit_envelope_button_height:I = 0x7f070148

.field public static final edit_stroke_width:I = 0x7f070149

.field public static final emv_title_margin:I = 0x7f07015c

.field public static final emv_title_negative_margin:I = 0x7f07015d

.field public static final emv_total_margin:I = 0x7f07015e

.field public static final gift_card_name_margin_top_tablet_only:I = 0x7f070167

.field public static final glyph_button_padding:I = 0x7f070168

.field public static final grid_delete_button_padding:I = 0x7f070181

.field public static final grid_delete_button_size_with_padding:I = 0x7f070182

.field public static final grid_label_text_size:I = 0x7f070184

.field public static final grid_placeholder_text_max_size:I = 0x7f070185

.field public static final home_item_badge_horizontal_spacing:I = 0x7f070199

.field public static final home_item_badge_vertical_spacing:I = 0x7f07019a

.field public static final invoice_timeline_left_pad:I = 0x7f0701d4

.field public static final item_library_empty_icon_height:I = 0x7f0701d5

.field public static final loyalty_contents_welcome_width:I = 0x7f0701f1

.field public static final match_parent:I = 0x7f0702b5

.field public static final max_hint_drag:I = 0x7f0702bc

.field public static final merchant_featured_image_min_height:I = 0x7f0702bd

.field public static final merchant_logo_height:I = 0x7f0702be

.field public static final merchant_logo_width:I = 0x7f0702bf

.field public static final merchant_map_image_height:I = 0x7f0702c0

.field public static final nav_column_item_height:I = 0x7f070355

.field public static final open_ticket_row_amount_width:I = 0x7f0703f5

.field public static final open_ticket_row_detail_width:I = 0x7f0703f6

.field public static final pairing_select_a_reader_top_margin:I = 0x7f070402

.field public static final pairing_step_width:I = 0x7f070403

.field public static final quantity_editext_margin:I = 0x7f070443

.field public static final r6_first_time_video_text_margin_top:I = 0x7f070449

.field public static final r6_first_time_video_text_size:I = 0x7f07044a

.field public static final receipt_item_image_padding:I = 0x7f070454

.field public static final receipt_item_image_size:I = 0x7f070455

.field public static final sample_image_tile_height:I = 0x7f0704a1

.field public static final sample_text_tile_height:I = 0x7f0704a2

.field public static final sample_text_tile_width:I = 0x7f0704a3

.field public static final sample_tile_container_height:I = 0x7f0704a4

.field public static final sample_tile_gap:I = 0x7f0704a5

.field public static final smart_line_row_badge_margin_bottom:I = 0x7f0704be

.field public static final swipe_failed_background_corner_radius:I = 0x7f07050a

.field public static final swipe_failed_card_corner_radius:I = 0x7f07050b

.field public static final swipe_failed_hgap:I = 0x7f07050c

.field public static final swipe_failed_message:I = 0x7f07050d

.field public static final swipe_failed_title:I = 0x7f07050e

.field public static final text_receipt_banner_body:I = 0x7f070530

.field public static final text_receipt_banner_title:I = 0x7f070531

.field public static final tip_option_width:I = 0x7f070533

.field public static final tutorial_height:I = 0x7f070564

.field public static final underline_page_indicator_height:I = 0x7f070567

.field public static final upsell_medium_gap:I = 0x7f070568

.field public static final wrap_content:I = 0x7f070572


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
