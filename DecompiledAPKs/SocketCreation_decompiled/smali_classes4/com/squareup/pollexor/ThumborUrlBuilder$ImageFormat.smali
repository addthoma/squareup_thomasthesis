.class public final enum Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;
.super Ljava/lang/Enum;
.source "ThumborUrlBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/pollexor/ThumborUrlBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ImageFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

.field public static final enum GIF:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

.field public static final enum JPEG:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

.field public static final enum PNG:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

.field public static final enum WEBP:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;


# instance fields
.field final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 79
    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    const/4 v1, 0x0

    const-string v2, "GIF"

    const-string v3, "gif"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->GIF:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    const/4 v2, 0x1

    const-string v3, "JPEG"

    const-string v4, "jpeg"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->JPEG:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    const/4 v3, 0x2

    const-string v4, "PNG"

    const-string v5, "png"

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->PNG:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    new-instance v0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    const/4 v4, 0x3

    const-string v5, "WEBP"

    const-string/jumbo v6, "webp"

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->WEBP:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    .line 78
    sget-object v5, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->GIF:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->JPEG:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->PNG:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->WEBP:Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->$VALUES:[Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 83
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 84
    iput-object p3, p0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;
    .locals 1

    .line 78
    const-class v0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    return-object p0
.end method

.method public static values()[Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->$VALUES:[Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    invoke-virtual {v0}, [Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/pollexor/ThumborUrlBuilder$ImageFormat;

    return-object v0
.end method
