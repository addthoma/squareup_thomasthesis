.class public Lcom/squareup/register/widgets/Animations;
.super Ljava/lang/Object;
.source "Animations.java"


# static fields
.field public static final PULSE_DURATION:I = 0x14d


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static buildPulseAnimation()Landroid/view/animation/Animation;
    .locals 10

    .line 19
    new-instance v9, Landroid/view/animation/ScaleAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 27
    new-instance v0, Lcom/squareup/register/widgets/Animations$1;

    invoke-direct {v0}, Lcom/squareup/register/widgets/Animations$1;-><init>()V

    invoke-virtual {v9, v0}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    const-wide/16 v0, 0x14d

    .line 50
    invoke-virtual {v9, v0, v1}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    return-object v9
.end method

.method public static expandVertically(Landroid/view/View;J)V
    .locals 3

    .line 55
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 57
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v2, -0x2

    invoke-virtual {p0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 58
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    const/4 v2, 0x0

    .line 60
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 61
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 63
    new-instance v2, Lcom/squareup/register/widgets/Animations$2;

    invoke-direct {v2, v0, v1, p0}, Lcom/squareup/register/widgets/Animations$2;-><init>(Landroid/view/ViewGroup$LayoutParams;ILandroid/view/View;)V

    .line 74
    invoke-virtual {v2, p1, p2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 75
    invoke-virtual {p0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method
