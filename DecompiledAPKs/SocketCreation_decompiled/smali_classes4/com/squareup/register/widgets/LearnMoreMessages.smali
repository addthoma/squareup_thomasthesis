.class public Lcom/squareup/register/widgets/LearnMoreMessages;
.super Ljava/lang/Object;
.source "LearnMoreMessages.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/register/widgets/LearnMoreMessages;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final bodyResId:I

.field public final cancelResId:I

.field public final learnMoreResId:I

.field public final titleResId:I

.field public final url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/register/widgets/LearnMoreMessages$1;

    invoke-direct {v0}, Lcom/squareup/register/widgets/LearnMoreMessages$1;-><init>()V

    sput-object v0, Lcom/squareup/register/widgets/LearnMoreMessages;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIII)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->url:Ljava/lang/String;

    .line 27
    iput p2, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->titleResId:I

    .line 28
    iput p3, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->bodyResId:I

    .line 29
    iput p4, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->learnMoreResId:I

    .line 30
    iput p5, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->cancelResId:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_7

    .line 40
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 42
    :cond_1
    check-cast p1, Lcom/squareup/register/widgets/LearnMoreMessages;

    .line 44
    iget-object v2, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/register/widgets/LearnMoreMessages;->url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v1

    .line 45
    :cond_2
    iget v2, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->titleResId:I

    iget v3, p1, Lcom/squareup/register/widgets/LearnMoreMessages;->titleResId:I

    if-eq v2, v3, :cond_3

    return v1

    .line 46
    :cond_3
    iget v2, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->bodyResId:I

    iget v3, p1, Lcom/squareup/register/widgets/LearnMoreMessages;->bodyResId:I

    if-eq v2, v3, :cond_4

    return v1

    .line 47
    :cond_4
    iget v2, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->learnMoreResId:I

    iget v3, p1, Lcom/squareup/register/widgets/LearnMoreMessages;->learnMoreResId:I

    if-eq v2, v3, :cond_5

    return v1

    .line 48
    :cond_5
    iget v2, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->cancelResId:I

    iget p1, p1, Lcom/squareup/register/widgets/LearnMoreMessages;->cancelResId:I

    if-ne v2, p1, :cond_6

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_7
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->url:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 54
    iget v1, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->titleResId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 55
    iget v1, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->bodyResId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 56
    iget v1, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->learnMoreResId:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 57
    iget v1, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->cancelResId:I

    add-int/2addr v0, v1

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 62
    iget-object p2, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->url:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget p2, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->titleResId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    iget p2, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->bodyResId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 65
    iget p2, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->learnMoreResId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 66
    iget p2, p0, Lcom/squareup/register/widgets/LearnMoreMessages;->cancelResId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
