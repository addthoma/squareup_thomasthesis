.class public final Lcom/squareup/register/widgets/GridSizer$Rect;
.super Ljava/lang/Object;
.source "GridSizer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/GridSizer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Rect"
.end annotation


# instance fields
.field bottom:I

.field left:I

.field right:I

.field top:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput p1, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->left:I

    .line 153
    iput p2, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->top:I

    .line 154
    iput p3, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->right:I

    .line 155
    iput p4, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->bottom:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    .line 165
    :cond_0
    instance-of v1, p1, Lcom/squareup/register/widgets/GridSizer$Rect;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 167
    :cond_1
    check-cast p1, Lcom/squareup/register/widgets/GridSizer$Rect;

    .line 169
    iget v1, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->bottom:I

    iget v3, p1, Lcom/squareup/register/widgets/GridSizer$Rect;->bottom:I

    if-eq v1, v3, :cond_2

    return v2

    .line 170
    :cond_2
    iget v1, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->left:I

    iget v3, p1, Lcom/squareup/register/widgets/GridSizer$Rect;->left:I

    if-eq v1, v3, :cond_3

    return v2

    .line 171
    :cond_3
    iget v1, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->right:I

    iget v3, p1, Lcom/squareup/register/widgets/GridSizer$Rect;->right:I

    if-eq v1, v3, :cond_4

    return v2

    .line 172
    :cond_4
    iget v1, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->top:I

    iget p1, p1, Lcom/squareup/register/widgets/GridSizer$Rect;->top:I

    if-ne v1, p1, :cond_5

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 177
    iget v0, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->left:I

    mul-int/lit8 v0, v0, 0x1f

    .line 178
    iget v1, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->top:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 179
    iget v1, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->right:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 180
    iget v1, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->bottom:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 159
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->left:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget v2, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->top:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget v2, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->right:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/squareup/register/widgets/GridSizer$Rect;->bottom:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const-string v2, "(%d,%d,%d,%d)"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
