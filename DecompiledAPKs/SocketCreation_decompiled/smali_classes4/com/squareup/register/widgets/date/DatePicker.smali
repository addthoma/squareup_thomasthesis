.class public final Lcom/squareup/register/widgets/date/DatePicker;
.super Landroid/widget/LinearLayout;
.source "DatePicker.java"


# static fields
.field private static final END:Ljava/util/Calendar;

.field private static final FOREVER:I = 0x1388

.field private static final START:Ljava/util/Calendar;


# instance fields
.field private currentDate:Ljava/util/Calendar;

.field private dataAdapter:Lcom/squareup/register/widgets/date/MonthPagerAdapter;

.field private dateRange:Lcom/squareup/register/widgets/date/DateRange;

.field private isAttachedToWindow:Z

.field private listener:Lcom/squareup/register/widgets/date/DatePickerListener;

.field private final monthFormat:Ljava/text/DateFormat;

.field private monthPager:Landroidx/viewpager/widget/ViewPager;

.field private monthTitleView:Landroid/widget/TextView;

.field private nextMonthView:Lcom/squareup/glyph/SquareGlyphView;

.field private previousMonthView:Lcom/squareup/glyph/SquareGlyphView;

.field private selectedRange:Lcom/squareup/register/widgets/date/DateRange;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 28
    new-instance v0, Ljava/util/GregorianCalendar;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x7b2

    invoke-direct {v0, v3, v2, v1}, Ljava/util/GregorianCalendar;-><init>(III)V

    sput-object v0, Lcom/squareup/register/widgets/date/DatePicker;->START:Ljava/util/Calendar;

    .line 29
    new-instance v0, Ljava/util/GregorianCalendar;

    const/16 v3, 0x1b3a

    invoke-direct {v0, v3, v2, v1}, Ljava/util/GregorianCalendar;-><init>(III)V

    sput-object v0, Lcom/squareup/register/widgets/date/DatePicker;->END:Ljava/util/Calendar;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0, p1, v0}, Lcom/squareup/register/widgets/date/DatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 49
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/register/widgets/date/DatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/register/widgets/date/DatePicker;->currentDate:Ljava/util/Calendar;

    .line 54
    sget p2, Lcom/squareup/widgets/pos/R$layout;->date_picker_layout:I

    invoke-static {p1, p2, p0}, Lcom/squareup/register/widgets/date/DatePicker;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 55
    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/date/DatePicker;->setOrientation(I)V

    .line 57
    new-instance p1, Ljava/text/SimpleDateFormat;

    .line 58
    invoke-virtual {p0}, Lcom/squareup/register/widgets/date/DatePicker;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget p3, Lcom/squareup/widgets/pos/R$string;->date_picker_month_format:I

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 59
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p3

    invoke-direct {p1, p2, p3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->monthFormat:Ljava/text/DateFormat;

    .line 61
    sget p1, Lcom/squareup/widgets/pos/R$id;->date_picker_month_pager:I

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/date/DatePicker;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/viewpager/widget/ViewPager;

    iput-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->monthPager:Landroidx/viewpager/widget/ViewPager;

    .line 62
    sget p1, Lcom/squareup/widgets/pos/R$id;->date_picker_prev_month:I

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/date/DatePicker;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->previousMonthView:Lcom/squareup/glyph/SquareGlyphView;

    .line 63
    sget p1, Lcom/squareup/widgets/pos/R$id;->date_picker_next_month:I

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/date/DatePicker;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->nextMonthView:Lcom/squareup/glyph/SquareGlyphView;

    .line 64
    sget p1, Lcom/squareup/widgets/pos/R$id;->date_picker_month_title:I

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/date/DatePicker;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->monthTitleView:Landroid/widget/TextView;

    .line 66
    iget-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->nextMonthView:Lcom/squareup/glyph/SquareGlyphView;

    new-instance p2, Lcom/squareup/register/widgets/date/DatePicker$1;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/date/DatePicker$1;-><init>(Lcom/squareup/register/widgets/date/DatePicker;)V

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->previousMonthView:Lcom/squareup/glyph/SquareGlyphView;

    new-instance p2, Lcom/squareup/register/widgets/date/DatePicker$2;

    invoke-direct {p2, p0}, Lcom/squareup/register/widgets/date/DatePicker$2;-><init>(Lcom/squareup/register/widgets/date/DatePicker;)V

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->currentDate:Ljava/util/Calendar;

    invoke-virtual {p0, p1}, Lcom/squareup/register/widgets/date/DatePicker;->setDate(Ljava/util/Calendar;)V

    .line 89
    sget-object p1, Lcom/squareup/register/widgets/date/DatePicker;->START:Ljava/util/Calendar;

    sget-object p2, Lcom/squareup/register/widgets/date/DatePicker;->END:Ljava/util/Calendar;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/widgets/date/DatePicker;->setDateRange(Ljava/util/Calendar;Ljava/util/Calendar;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->currentDate:Ljava/util/Calendar;

    invoke-virtual {p0, p1, p1}, Lcom/squareup/register/widgets/date/DatePicker;->setSelectedRange(Ljava/util/Calendar;Ljava/util/Calendar;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/register/widgets/date/DatePicker;)Landroidx/viewpager/widget/ViewPager;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/register/widgets/date/DatePicker;->monthPager:Landroidx/viewpager/widget/ViewPager;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/register/widgets/date/DatePicker;)Lcom/squareup/register/widgets/date/MonthPagerAdapter;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/register/widgets/date/DatePicker;->dataAdapter:Lcom/squareup/register/widgets/date/MonthPagerAdapter;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/register/widgets/date/DatePicker;)Ljava/util/Calendar;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/register/widgets/date/DatePicker;->currentDate:Ljava/util/Calendar;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/register/widgets/date/DatePicker;)Lcom/squareup/register/widgets/date/DatePickerListener;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/register/widgets/date/DatePicker;->listener:Lcom/squareup/register/widgets/date/DatePickerListener;

    return-object p0
.end method

.method private setupDataSource()V
    .locals 5

    .line 230
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->listener:Lcom/squareup/register/widgets/date/DatePickerListener;

    const-string v1, "listener must be set"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 232
    new-instance v0, Lcom/squareup/register/widgets/date/MonthPagerAdapter;

    new-instance v1, Lcom/squareup/register/widgets/date/-$$Lambda$DatePicker$fD7_6apZt8-BeJOjq9I5kvI3IdU;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/date/-$$Lambda$DatePicker$fD7_6apZt8-BeJOjq9I5kvI3IdU;-><init>(Lcom/squareup/register/widgets/date/DatePicker;)V

    new-instance v2, Lcom/squareup/register/widgets/date/DateRange;

    iget-object v3, p0, Lcom/squareup/register/widgets/date/DatePicker;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    .line 234
    invoke-virtual {v3}, Lcom/squareup/register/widgets/date/DateRange;->getStartCalendar()Ljava/util/Calendar;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/register/widgets/date/DatePicker;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {v4}, Lcom/squareup/register/widgets/date/DateRange;->getEndCalendar()Ljava/util/Calendar;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/squareup/register/widgets/date/DateRange;-><init>(Ljava/util/Calendar;Ljava/util/Calendar;)V

    invoke-direct {v0, v1, v2}, Lcom/squareup/register/widgets/date/MonthPagerAdapter;-><init>(Lcom/squareup/register/widgets/date/MonthView$SelectionListener;Lcom/squareup/register/widgets/date/DateRange;)V

    iput-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->dataAdapter:Lcom/squareup/register/widgets/date/MonthPagerAdapter;

    .line 235
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->dataAdapter:Lcom/squareup/register/widgets/date/MonthPagerAdapter;

    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker;->selectedRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->setSelectedRange(Lcom/squareup/register/widgets/date/DateRange;)V

    .line 237
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->monthPager:Landroidx/viewpager/widget/ViewPager;

    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker;->dataAdapter:Lcom/squareup/register/widgets/date/MonthPagerAdapter;

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 238
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->monthPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->clearOnPageChangeListeners()V

    .line 239
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->monthPager:Landroidx/viewpager/widget/ViewPager;

    new-instance v1, Lcom/squareup/register/widgets/date/DatePicker$3;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/date/DatePicker$3;-><init>(Lcom/squareup/register/widgets/date/DatePicker;)V

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    return-void
.end method

.method private updateViews(Z)V
    .locals 5

    .line 257
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->monthTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker;->monthFormat:Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/register/widgets/date/DatePicker;->currentDate:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->dataAdapter:Lcom/squareup/register/widgets/date/MonthPagerAdapter;

    if-eqz v0, :cond_2

    .line 260
    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker;->currentDate:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->positionForDate(Ljava/util/Calendar;)I

    move-result v0

    .line 261
    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker;->monthPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v1, v0}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 263
    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker;->monthPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 265
    invoke-virtual {v1}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 266
    :goto_0
    iget-object v4, p0, Lcom/squareup/register/widgets/date/DatePicker;->nextMonthView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-static {v4, v1, p1}, Lcom/squareup/register/widgets/date/DatePicker;->updateVisibility(Landroid/view/View;ZZ)V

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 268
    :goto_1
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->previousMonthView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-static {v0, v2, p1}, Lcom/squareup/register/widgets/date/DatePicker;->updateVisibility(Landroid/view/View;ZZ)V

    :cond_2
    return-void
.end method

.method private static updateVisibility(Landroid/view/View;ZZ)V
    .locals 1

    if-eqz p2, :cond_0

    .line 276
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p2

    int-to-float v0, p1

    invoke-virtual {p2, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0

    :cond_0
    int-to-float p2, p1

    .line 278
    invoke-virtual {p0, p2}, Landroid/view/View;->setAlpha(F)V

    .line 280
    :goto_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$setupDataSource$0$DatePicker(III)V
    .locals 1

    .line 233
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->listener:Lcom/squareup/register/widgets/date/DatePickerListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/register/widgets/date/DatePickerListener;->onDaySelected(III)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 94
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    const/4 v0, 0x1

    .line 96
    iput-boolean v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->isAttachedToWindow:Z

    .line 98
    invoke-direct {p0}, Lcom/squareup/register/widgets/date/DatePicker;->setupDataSource()V

    const/4 v0, 0x0

    .line 99
    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/date/DatePicker;->updateViews(Z)V

    return-void
.end method

.method public setDate(III)V
    .locals 1

    .line 175
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 176
    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    .line 177
    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/date/DatePicker;->setDate(Ljava/util/Calendar;)V

    return-void
.end method

.method public setDate(Ljava/util/Calendar;)V
    .locals 1

    const/4 v0, 0x0

    .line 157
    invoke-virtual {p0, p1, v0}, Lcom/squareup/register/widgets/date/DatePicker;->setDate(Ljava/util/Calendar;Z)V

    return-void
.end method

.method public setDate(Ljava/util/Calendar;Z)V
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->currentDate:Ljava/util/Calendar;

    .line 166
    iget-boolean p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->isAttachedToWindow:Z

    if-eqz p1, :cond_0

    .line 167
    invoke-direct {p0, p2}, Lcom/squareup/register/widgets/date/DatePicker;->updateViews(Z)V

    :cond_0
    return-void
.end method

.method public setDateRange(Ljava/util/Calendar;Ljava/util/Calendar;)V
    .locals 1

    .line 120
    new-instance v0, Lcom/squareup/register/widgets/date/DateRange;

    invoke-direct {v0, p1, p2}, Lcom/squareup/register/widgets/date/DateRange;-><init>(Ljava/util/Calendar;Ljava/util/Calendar;)V

    iput-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    .line 122
    iget-boolean p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->isAttachedToWindow:Z

    if-eqz p1, :cond_0

    .line 123
    invoke-direct {p0}, Lcom/squareup/register/widgets/date/DatePicker;->setupDataSource()V

    :cond_0
    return-void
.end method

.method public setDateRangeEnd(Ljava/util/Calendar;)V
    .locals 2

    .line 133
    new-instance v0, Lcom/squareup/register/widgets/date/DateRange;

    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/date/DateRange;->getStartCalendar()Ljava/util/Calendar;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/register/widgets/date/DateRange;-><init>(Ljava/util/Calendar;Ljava/util/Calendar;)V

    iput-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    .line 135
    iget-boolean p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->isAttachedToWindow:Z

    if-eqz p1, :cond_0

    .line 136
    invoke-direct {p0}, Lcom/squareup/register/widgets/date/DatePicker;->setupDataSource()V

    :cond_0
    return-void
.end method

.method public setDateRangeStart(Ljava/util/Calendar;)V
    .locals 2

    .line 146
    new-instance v0, Lcom/squareup/register/widgets/date/DateRange;

    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {v1}, Lcom/squareup/register/widgets/date/DateRange;->getEndCalendar()Ljava/util/Calendar;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/register/widgets/date/DateRange;-><init>(Ljava/util/Calendar;Ljava/util/Calendar;)V

    iput-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->dateRange:Lcom/squareup/register/widgets/date/DateRange;

    .line 148
    iget-boolean p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->isAttachedToWindow:Z

    if-eqz p1, :cond_0

    .line 149
    invoke-direct {p0}, Lcom/squareup/register/widgets/date/DatePicker;->setupDataSource()V

    :cond_0
    return-void
.end method

.method public setListener(Lcom/squareup/register/widgets/date/DatePickerListener;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->listener:Lcom/squareup/register/widgets/date/DatePickerListener;

    return-void
.end method

.method public setPageMargin(I)V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->monthPager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setPageMargin(I)V

    return-void
.end method

.method public setSelectedDate(III)V
    .locals 1

    .line 215
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 216
    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    .line 217
    invoke-virtual {p0, v0, v0}, Lcom/squareup/register/widgets/date/DatePicker;->setSelectedRange(Ljava/util/Calendar;Ljava/util/Calendar;)V

    return-void
.end method

.method public setSelectedRange(Ljava/util/Calendar;Ljava/util/Calendar;)V
    .locals 1

    .line 205
    new-instance v0, Lcom/squareup/register/widgets/date/DateRange;

    invoke-direct {v0, p1, p2}, Lcom/squareup/register/widgets/date/DateRange;-><init>(Ljava/util/Calendar;Ljava/util/Calendar;)V

    iput-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker;->selectedRange:Lcom/squareup/register/widgets/date/DateRange;

    .line 206
    iget-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker;->dataAdapter:Lcom/squareup/register/widgets/date/MonthPagerAdapter;

    if-eqz p1, :cond_0

    .line 207
    iget-object p2, p0, Lcom/squareup/register/widgets/date/DatePicker;->selectedRange:Lcom/squareup/register/widgets/date/DateRange;

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->setSelectedRange(Lcom/squareup/register/widgets/date/DateRange;)V

    :cond_0
    return-void
.end method

.method public setSelectedRange(Ljava/util/Date;Ljava/util/Date;)V
    .locals 1

    .line 188
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 189
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 191
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    .line 192
    invoke-virtual {p1, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 194
    invoke-virtual {p0, v0, p1}, Lcom/squareup/register/widgets/date/DatePicker;->setSelectedRange(Ljava/util/Calendar;Ljava/util/Calendar;)V

    return-void
.end method
