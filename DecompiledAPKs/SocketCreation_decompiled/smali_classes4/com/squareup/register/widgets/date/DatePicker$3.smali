.class Lcom/squareup/register/widgets/date/DatePicker$3;
.super Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;
.source "DatePicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/date/DatePicker;->setupDataSource()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/date/DatePicker;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/date/DatePicker;)V
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/squareup/register/widgets/date/DatePicker$3;->this$0:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-direct {p0}, Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 4

    .line 241
    iget-object v0, p0, Lcom/squareup/register/widgets/date/DatePicker$3;->this$0:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-static {v0}, Lcom/squareup/register/widgets/date/DatePicker;->access$100(Lcom/squareup/register/widgets/date/DatePicker;)Lcom/squareup/register/widgets/date/MonthPagerAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->getYearForPosition(I)I

    move-result v0

    .line 242
    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker$3;->this$0:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-static {v1}, Lcom/squareup/register/widgets/date/DatePicker;->access$100(Lcom/squareup/register/widgets/date/DatePicker;)Lcom/squareup/register/widgets/date/MonthPagerAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/register/widgets/date/MonthPagerAdapter;->getMonthForPosition(I)I

    move-result p1

    .line 244
    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker$3;->this$0:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-static {v1}, Lcom/squareup/register/widgets/date/DatePicker;->access$200(Lcom/squareup/register/widgets/date/DatePicker;)Ljava/util/Calendar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker$3;->this$0:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-static {v1}, Lcom/squareup/register/widgets/date/DatePicker;->access$200(Lcom/squareup/register/widgets/date/DatePicker;)Ljava/util/Calendar;

    move-result-object v1

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-eq p1, v1, :cond_1

    .line 245
    :cond_0
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1, v0, p1, v2}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 246
    iget-object v3, p0, Lcom/squareup/register/widgets/date/DatePicker$3;->this$0:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-virtual {v3, v1, v2}, Lcom/squareup/register/widgets/date/DatePicker;->setDate(Ljava/util/Calendar;Z)V

    .line 248
    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker$3;->this$0:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-static {v1}, Lcom/squareup/register/widgets/date/DatePicker;->access$300(Lcom/squareup/register/widgets/date/DatePicker;)Lcom/squareup/register/widgets/date/DatePickerListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 249
    iget-object v1, p0, Lcom/squareup/register/widgets/date/DatePicker$3;->this$0:Lcom/squareup/register/widgets/date/DatePicker;

    invoke-static {v1}, Lcom/squareup/register/widgets/date/DatePicker;->access$300(Lcom/squareup/register/widgets/date/DatePicker;)Lcom/squareup/register/widgets/date/DatePickerListener;

    move-result-object v1

    invoke-interface {v1, v0, p1}, Lcom/squareup/register/widgets/date/DatePickerListener;->onMonthChange(II)V

    :cond_1
    return-void
.end method
