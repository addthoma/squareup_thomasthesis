.class public Lcom/squareup/register/widgets/NohoDurationPickerRunner;
.super Ljava/lang/Object;
.source "NohoDurationPickerRunner.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoDurationPickerRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoDurationPickerRunner.kt\ncom/squareup/register/widgets/NohoDurationPickerRunner\n+ 2 Flows.kt\ncom/squareup/container/Flows\n*L\n1#1,77:1\n75#2:78\n*E\n*S KotlinDebug\n*F\n+ 1 NohoDurationPickerRunner.kt\ncom/squareup/register/widgets/NohoDurationPickerRunner\n*L\n54#1:78\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\u0008\u0017\u0018\u00002\u00020\u0001:\u0001&B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u001eH\u0016J\r\u0010\u001f\u001a\u00020 H\u0000\u00a2\u0006\u0002\u0008!J\r\u0010\"\u001a\u00020 H\u0000\u00a2\u0006\u0002\u0008#J\"\u0010$\u001a\u00020 2\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000c2\u0006\u0010%\u001a\u00020\u00132\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006R\u001a\u0010\u0005\u001a\u00020\u0006X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\nR\u001e\u0010\u000b\u001a\u00020\u000c8\u0000@\u0000X\u0081\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u0010\u0012\u000c\u0012\n \u0014*\u0004\u0018\u00010\u00130\u00130\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u00020\u000cX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0016\u0010\u000e\"\u0004\u0008\u0017\u0010\u0010R\u001a\u0010\u0018\u001a\u00020\u000cX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0019\u0010\u000e\"\u0004\u0008\u001a\u0010\u0010R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/register/widgets/NohoDurationPickerRunner;",
        "",
        "flow",
        "Lflow/Flow;",
        "(Lflow/Flow;)V",
        "allowZeroDuration",
        "",
        "getAllowZeroDuration$widgets_pos_release",
        "()Z",
        "setAllowZeroDuration$widgets_pos_release",
        "(Z)V",
        "dialogTitle",
        "",
        "getDialogTitle$widgets_pos_release",
        "()I",
        "setDialogTitle$widgets_pos_release",
        "(I)V",
        "durationPicked",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;",
        "kotlin.jvm.PlatformType",
        "hourIndex",
        "getHourIndex$widgets_pos_release",
        "setHourIndex$widgets_pos_release",
        "minuteIndex",
        "getMinuteIndex$widgets_pos_release",
        "setMinuteIndex$widgets_pos_release",
        "token",
        "",
        "datePicked",
        "Lrx/Observable;",
        "onDialogCancelled",
        "",
        "onDialogCancelled$widgets_pos_release",
        "onDurationPickedFromDialog",
        "onDurationPickedFromDialog$widgets_pos_release",
        "showDurationPickerDialog",
        "durationAndToken",
        "DurationAndToken",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private allowZeroDuration:Z

.field private dialogTitle:I

.field private final durationPicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private hourIndex:I

.field private minuteIndex:I

.field private token:J


# direct methods
.method public constructor <init>(Lflow/Flow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->flow:Lflow/Flow;

    const/4 p1, 0x1

    .line 27
    iput-boolean p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->allowZeroDuration:Z

    .line 28
    sget p1, Lcom/squareup/widgets/pos/R$string;->duration_title:I

    iput p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->dialogTitle:I

    .line 29
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string v0, "PublishRelay.create<DurationAndToken>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->durationPicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static synthetic showDurationPickerDialog$default(Lcom/squareup/register/widgets/NohoDurationPickerRunner;ILcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;ZILjava/lang/Object;)V
    .locals 0

    if-nez p5, :cond_1

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x1

    .line 40
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->showDurationPickerDialog(ILcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;Z)V

    return-void

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: showDurationPickerDialog"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public datePicked()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;",
            ">;"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->durationPicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/ObservableSource;

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public final getAllowZeroDuration$widgets_pos_release()Z
    .locals 1

    .line 27
    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->allowZeroDuration:Z

    return v0
.end method

.method public final getDialogTitle$widgets_pos_release()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->dialogTitle:I

    return v0
.end method

.method public final getHourIndex$widgets_pos_release()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->hourIndex:I

    return v0
.end method

.method public final getMinuteIndex$widgets_pos_release()I
    .locals 1

    .line 32
    iget v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->minuteIndex:I

    return v0
.end method

.method public final onDialogCancelled$widgets_pos_release()V
    .locals 4

    .line 54
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    .line 78
    const-class v2, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public final onDurationPickedFromDialog$widgets_pos_release()V
    .locals 5

    .line 58
    iget v0, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->hourIndex:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofHours(J)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 59
    iget v1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->minuteIndex:I

    int-to-long v1, v1

    const/4 v3, 0x5

    int-to-long v3, v3

    mul-long v1, v1, v3

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/Duration;->plusMinutes(J)Lorg/threeten/bp/Duration;

    move-result-object v0

    .line 60
    iget-object v1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->durationPicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v2, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;

    const-string v3, "newDuration"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-wide v3, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->token:J

    invoke-direct {v2, v0, v3, v4}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;-><init>(Lorg/threeten/bp/Duration;J)V

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public final setAllowZeroDuration$widgets_pos_release(Z)V
    .locals 0

    .line 27
    iput-boolean p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->allowZeroDuration:Z

    return-void
.end method

.method public final setDialogTitle$widgets_pos_release(I)V
    .locals 0

    .line 28
    iput p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->dialogTitle:I

    return-void
.end method

.method public final setHourIndex$widgets_pos_release(I)V
    .locals 0

    .line 31
    iput p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->hourIndex:I

    return-void
.end method

.method public final setMinuteIndex$widgets_pos_release(I)V
    .locals 0

    .line 32
    iput p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->minuteIndex:I

    return-void
.end method

.method public final showDurationPickerDialog(ILcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;Z)V
    .locals 4

    const-string v0, "durationAndToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iput p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->dialogTitle:I

    .line 43
    iput-boolean p3, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->allowZeroDuration:Z

    .line 44
    invoke-virtual {p2}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;->getDuration()Lorg/threeten/bp/Duration;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toHours()J

    move-result-wide v0

    long-to-int p3, v0

    .line 46
    iput p3, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->hourIndex:I

    .line 47
    invoke-virtual {p1}, Lorg/threeten/bp/Duration;->toMinutes()J

    move-result-wide v0

    const/16 p1, 0x3c

    int-to-long v2, p1

    rem-long/2addr v0, v2

    const/4 p1, 0x5

    int-to-long v2, p1

    div-long/2addr v0, v2

    long-to-int p1, v0

    iput p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->minuteIndex:I

    .line 48
    invoke-virtual {p2}, Lcom/squareup/register/widgets/NohoDurationPickerRunner$DurationAndToken;->getToken()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->token:J

    .line 50
    iget-object p1, p0, Lcom/squareup/register/widgets/NohoDurationPickerRunner;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen;

    invoke-direct {p2}, Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen;-><init>()V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
