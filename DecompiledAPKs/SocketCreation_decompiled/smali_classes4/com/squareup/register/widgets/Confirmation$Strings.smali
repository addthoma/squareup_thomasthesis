.class public Lcom/squareup/register/widgets/Confirmation$Strings;
.super Ljava/lang/Object;
.source "Confirmation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/Confirmation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Strings"
.end annotation


# instance fields
.field public final body:Ljava/lang/String;

.field public final cancel:Ljava/lang/String;

.field public final confirm:Ljava/lang/String;

.field public final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/register/widgets/Confirmation$Strings;->title:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/squareup/register/widgets/Confirmation$Strings;->body:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/squareup/register/widgets/Confirmation$Strings;->confirm:Ljava/lang/String;

    .line 24
    iput-object p4, p0, Lcom/squareup/register/widgets/Confirmation$Strings;->cancel:Ljava/lang/String;

    return-void
.end method
