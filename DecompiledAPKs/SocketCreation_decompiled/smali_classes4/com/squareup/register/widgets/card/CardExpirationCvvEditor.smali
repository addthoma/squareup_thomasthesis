.class public final Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;
.super Landroid/widget/FrameLayout;
.source "CardExpirationCvvEditor.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCardExpirationCvvEditor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CardExpirationCvvEditor.kt\ncom/squareup/register/widgets/card/CardExpirationCvvEditor\n*L\n1#1,121:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\'\u001a\u00020(H\u0002J\u0008\u0010)\u001a\u00020(H\u0002J\u0008\u0010*\u001a\u00020$H\u0002J\u0008\u0010+\u001a\u00020(H\u0002J\u0008\u0010,\u001a\u00020(H\u0002J\u0008\u0010-\u001a\u00020(H\u0002J\u0008\u0010.\u001a\u00020$H\u0002J\u0008\u0010/\u001a\u00020(H\u0002J\u000c\u00100\u001a\u0008\u0012\u0004\u0012\u00020$01R$\u0010\t\u001a\u00020\u00082\u0006\u0010\u0007\u001a\u00020\u0008@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR$\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0007\u001a\u00020\u000e@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R$\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u00158F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0017\u0010\u0018\"\u0004\u0008\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u001d\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u00158F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u001e\u0010\u0018\"\u0004\u0008\u001f\u0010\u001aR\u000e\u0010 \u001a\u00020!X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020$0#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020&X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;",
        "Landroid/widget/FrameLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "brand",
        "Lcom/squareup/Card$Brand;",
        "cardBrand",
        "getCardBrand",
        "()Lcom/squareup/Card$Brand;",
        "setCardBrand",
        "(Lcom/squareup/Card$Brand;)V",
        "Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        "cardTenderBrand",
        "getCardTenderBrand",
        "()Lcom/squareup/protos/client/bills/CardTender$Card$Brand;",
        "setCardTenderBrand",
        "(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V",
        "value",
        "",
        "cvv",
        "getCvv",
        "()Ljava/lang/String;",
        "setCvv",
        "(Ljava/lang/String;)V",
        "cvvEditor",
        "Lcom/squareup/register/widgets/card/CvvEditor;",
        "expiration",
        "getExpiration",
        "setExpiration",
        "expirationEditor",
        "Lcom/squareup/register/widgets/card/ExpirationEditor;",
        "onInputValidityChange",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "",
        "panValidationStrategy",
        "Lcom/squareup/register/widgets/card/CardPanValidationStrategy;",
        "bindViews",
        "",
        "checkIfAllInputsValid",
        "cvvValid",
        "disableSaveEnabledOnChildren",
        "enableAutoNextForExpiration",
        "enableAutoPreviousForCvv",
        "expirationValid",
        "listenForValidInput",
        "onInputsValidityChange",
        "Lrx/Observable;",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private cardBrand:Lcom/squareup/Card$Brand;

.field private cardTenderBrand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

.field private cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

.field private expirationEditor:Lcom/squareup/register/widgets/card/ExpirationEditor;

.field private final onInputValidityChange:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final panValidationStrategy:Lcom/squareup/register/widgets/card/CardPanValidationStrategy;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-instance p2, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {p2}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    iput-object p2, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->panValidationStrategy:Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    .line 29
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    const-string v0, "PublishRelay.create()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->onInputValidityChange:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 35
    sget p2, Lcom/squareup/widgets/pos/R$layout;->card_expiration_cvv_editor:I

    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, p2, v0}, Landroid/widget/FrameLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 36
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->bindViews()V

    .line 37
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->expirationEditor:Lcom/squareup/register/widgets/card/ExpirationEditor;

    if-nez p1, :cond_0

    const-string p2, "expirationEditor"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object p2, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->panValidationStrategy:Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    check-cast p2, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/card/ExpirationEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 38
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->disableSaveEnabledOnChildren()V

    .line 39
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->enableAutoNextForExpiration()V

    .line 40
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->enableAutoPreviousForCvv()V

    .line 41
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->listenForValidInput()V

    .line 53
    sget-object p1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->UNKNOWN_BRAND:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cardTenderBrand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 60
    sget-object p1, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cardBrand:Lcom/squareup/Card$Brand;

    return-void
.end method

.method public static final synthetic access$checkIfAllInputsValid(Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->checkIfAllInputsValid()V

    return-void
.end method

.method public static final synthetic access$getCvvEditor$p(Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;)Lcom/squareup/register/widgets/card/CvvEditor;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

    if-nez p0, :cond_0

    const-string v0, "cvvEditor"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getExpirationEditor$p(Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;)Lcom/squareup/register/widgets/card/ExpirationEditor;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->expirationEditor:Lcom/squareup/register/widgets/card/ExpirationEditor;

    if-nez p0, :cond_0

    const-string v0, "expirationEditor"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setCvvEditor$p(Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;Lcom/squareup/register/widgets/card/CvvEditor;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

    return-void
.end method

.method public static final synthetic access$setExpirationEditor$p(Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;Lcom/squareup/register/widgets/card/ExpirationEditor;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->expirationEditor:Lcom/squareup/register/widgets/card/ExpirationEditor;

    return-void
.end method

.method private final bindViews()V
    .locals 1

    .line 117
    sget v0, Lcom/squareup/widgets/pos/R$id;->expiration_editor:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/card/ExpirationEditor;

    iput-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->expirationEditor:Lcom/squareup/register/widgets/card/ExpirationEditor;

    .line 118
    sget v0, Lcom/squareup/widgets/pos/R$id;->cvv_editor:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/card/CvvEditor;

    iput-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

    return-void
.end method

.method private final checkIfAllInputsValid()V
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->onInputValidityChange:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvValid()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->expirationValid()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private final cvvValid()Z
    .locals 3

    .line 109
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->panValidationStrategy:Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

    if-nez v1, :cond_0

    const-string v2, "cvvEditor"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/register/widgets/card/CvvEditor;->extractCvv()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cardBrand:Lcom/squareup/Card$Brand;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;->cvvValid(Ljava/lang/String;Lcom/squareup/Card$Brand;)Z

    move-result v0

    return v0
.end method

.method private final disableSaveEnabledOnChildren()V
    .locals 3

    .line 76
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->expirationEditor:Lcom/squareup/register/widgets/card/ExpirationEditor;

    if-nez v0, :cond_0

    const-string v1, "expirationEditor"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/ExpirationEditor;->setSaveEnabled(Z)V

    .line 77
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

    if-nez v0, :cond_1

    const-string v2, "cvvEditor"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CvvEditor;->setSaveEnabled(Z)V

    return-void
.end method

.method private final enableAutoNextForExpiration()V
    .locals 6

    .line 81
    new-instance v0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor$enableAutoNextForExpiration$1;

    invoke-direct {v0, p0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor$enableAutoNextForExpiration$1;-><init>(Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->expirationEditor:Lcom/squareup/register/widgets/card/ExpirationEditor;

    const-string v1, "expirationEditor"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 86
    :cond_0
    new-instance v2, Lcom/squareup/text/AutoAdvanceTextWatcher;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->expirationEditor:Lcom/squareup/register/widgets/card/ExpirationEditor;

    if-nez v4, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v4, Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

    if-nez v1, :cond_2

    const-string v5, "cvvEditor"

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v1, Landroid/view/View;

    const/4 v5, 0x5

    invoke-direct {v2, v3, v4, v1, v5}, Lcom/squareup/text/AutoAdvanceTextWatcher;-><init>(ZLandroid/view/View;Landroid/view/View;I)V

    check-cast v2, Landroid/text/TextWatcher;

    .line 85
    invoke-virtual {v0, v2}, Lcom/squareup/register/widgets/card/ExpirationEditor;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private final enableAutoPreviousForCvv()V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

    if-nez v0, :cond_0

    const-string v1, "cvvEditor"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor$enableAutoPreviousForCvv$1;

    invoke-direct {v1, p0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor$enableAutoPreviousForCvv$1;-><init>(Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;)V

    check-cast v1, Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CvvEditor;->setOnDeleteKeyListener(Lcom/squareup/widgets/OnDeleteEditText$OnDeleteKeyListener;)V

    return-void
.end method

.method private final expirationValid()Z
    .locals 3

    .line 113
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->panValidationStrategy:Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->expirationEditor:Lcom/squareup/register/widgets/card/ExpirationEditor;

    if-nez v1, :cond_0

    const-string v2, "expirationEditor"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/register/widgets/card/ExpirationEditor;->extractCardExpiration()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;->expirationValid(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private final listenForValidInput()V
    .locals 3

    .line 95
    new-instance v0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor$listenForValidInput$emptyTextWatcher$1;

    invoke-direct {v0, p0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor$listenForValidInput$emptyTextWatcher$1;-><init>(Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;)V

    .line 100
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->expirationEditor:Lcom/squareup/register/widgets/card/ExpirationEditor;

    if-nez v1, :cond_0

    const-string v2, "expirationEditor"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {v1, v0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 101
    iget-object v1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

    if-nez v1, :cond_1

    const-string v2, "cvvEditor"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1, v0}, Lcom/squareup/register/widgets/card/CvvEditor;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method


# virtual methods
.method public final getCardBrand()Lcom/squareup/Card$Brand;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cardBrand:Lcom/squareup/Card$Brand;

    return-object v0
.end method

.method public final getCardTenderBrand()Lcom/squareup/protos/client/bills/CardTender$Card$Brand;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cardTenderBrand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-object v0
.end method

.method public final getCvv()Ljava/lang/String;
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

    if-nez v0, :cond_0

    const-string v1, "cvvEditor"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/CvvEditor;->extractCvv()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public final getExpiration()Ljava/lang/String;
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->expirationEditor:Lcom/squareup/register/widgets/card/ExpirationEditor;

    if-nez v0, :cond_0

    const-string v1, "expirationEditor"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/register/widgets/card/ExpirationEditor;->extractCardExpiration()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public final onInputsValidityChange()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->onInputValidityChange:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/PublishRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    const-string v1, "onInputValidityChange.distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final setCardBrand(Lcom/squareup/Card$Brand;)V
    .locals 4

    const-string v0, "brand"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

    const-string v1, "cvvEditor"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->panValidationStrategy:Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    check-cast v2, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    new-instance v3, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor$cardBrand$1;

    invoke-direct {v3, p1}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor$cardBrand$1;-><init>(Lcom/squareup/Card$Brand;)V

    check-cast v3, Lrx/functions/Func0;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/register/widgets/card/CvvEditor;->setStrategyAndBrandGuesser(Lcom/squareup/register/widgets/card/PanValidationStrategy;Lrx/functions/Func0;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const-string v1, "0"

    check-cast v1, Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cardBrand:Lcom/squareup/Card$Brand;

    invoke-virtual {v2}, Lcom/squareup/Card$Brand;->cvvLength()I

    move-result v2

    invoke-static {v1, v2}, Lkotlin/text/StringsKt;->repeat(Ljava/lang/CharSequence;I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CvvEditor;->setHint(Ljava/lang/CharSequence;)V

    .line 64
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cardBrand:Lcom/squareup/Card$Brand;

    return-void
.end method

.method public final setCardTenderBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)V
    .locals 1

    const-string v0, "brand"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-static {p1}, Lcom/squareup/card/CardBrands;->toCardBrand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->setCardBrand(Lcom/squareup/Card$Brand;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cardTenderBrand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    return-void
.end method

.method public final setCvv(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->cvvEditor:Lcom/squareup/register/widgets/card/CvvEditor;

    if-nez v0, :cond_0

    const-string v1, "cvvEditor"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CvvEditor;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setExpiration(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/register/widgets/card/CardExpirationCvvEditor;->expirationEditor:Lcom/squareup/register/widgets/card/ExpirationEditor;

    if-nez v0, :cond_0

    const-string v1, "expirationEditor"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/ExpirationEditor;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
