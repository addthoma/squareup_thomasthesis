.class Lcom/squareup/register/widgets/card/CvvEditor$1;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "CvvEditor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/register/widgets/card/CvvEditor;->addFocusAndInputListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/register/widgets/card/CvvEditor;


# direct methods
.method constructor <init>(Lcom/squareup/register/widgets/card/CvvEditor;)V
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor$1;->this$0:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x5

    if-ne p2, p1, :cond_1

    .line 140
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor$1;->this$0:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CvvEditor;->access$000(Lcom/squareup/register/widgets/card/CvvEditor;)V

    .line 141
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor$1;->this$0:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CvvEditor;->access$200(Lcom/squareup/register/widgets/card/CvvEditor;)Lcom/squareup/register/widgets/card/PanValidationStrategy;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/register/widgets/card/CvvEditor$1;->this$0:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-virtual {p2}, Lcom/squareup/register/widgets/card/CvvEditor;->extractCvv()Ljava/lang/String;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/register/widgets/card/CvvEditor$1;->this$0:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-static {p3}, Lcom/squareup/register/widgets/card/CvvEditor;->access$100(Lcom/squareup/register/widgets/card/CvvEditor;)Lrx/functions/Func0;

    move-result-object p3

    invoke-interface {p3}, Lrx/functions/Func0;->call()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/Card$Brand;

    invoke-interface {p1, p2, p3}, Lcom/squareup/register/widgets/card/PanValidationStrategy;->cvvValid(Ljava/lang/String;Lcom/squareup/Card$Brand;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 142
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor$1;->this$0:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CvvEditor;->access$300(Lcom/squareup/register/widgets/card/CvvEditor;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 144
    :cond_0
    iget-object p1, p0, Lcom/squareup/register/widgets/card/CvvEditor$1;->this$0:Lcom/squareup/register/widgets/card/CvvEditor;

    invoke-static {p1}, Lcom/squareup/register/widgets/card/CvvEditor;->access$400(Lcom/squareup/register/widgets/card/CvvEditor;)Lcom/squareup/register/widgets/card/CardEditorEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/register/widgets/card/CardEditorEditText;->onInvalidContent()V

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method
