.class public final Lcom/squareup/register/widgets/NohoDatePickerDialogOutput$DateChanged;
.super Lcom/squareup/register/widgets/NohoDatePickerDialogOutput;
.source "NohoDatePickerDialogOutput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/NohoDatePickerDialogOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DateChanged"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/register/widgets/NohoDatePickerDialogOutput$DateChanged;",
        "Lcom/squareup/register/widgets/NohoDatePickerDialogOutput;",
        "newDate",
        "Lorg/threeten/bp/LocalDate;",
        "yearEnabled",
        "",
        "(Lorg/threeten/bp/LocalDate;Z)V",
        "getNewDate",
        "()Lorg/threeten/bp/LocalDate;",
        "getYearEnabled",
        "()Z",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final newDate:Lorg/threeten/bp/LocalDate;

.field private final yearEnabled:Z


# direct methods
.method public constructor <init>(Lorg/threeten/bp/LocalDate;Z)V
    .locals 1

    const-string v0, "newDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 16
    invoke-direct {p0, v0}, Lcom/squareup/register/widgets/NohoDatePickerDialogOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogOutput$DateChanged;->newDate:Lorg/threeten/bp/LocalDate;

    iput-boolean p2, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogOutput$DateChanged;->yearEnabled:Z

    return-void
.end method


# virtual methods
.method public final getNewDate()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogOutput$DateChanged;->newDate:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getYearEnabled()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/register/widgets/NohoDatePickerDialogOutput$DateChanged;->yearEnabled:Z

    return v0
.end method
