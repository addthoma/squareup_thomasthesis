.class public interface abstract Lcom/squareup/register/widgets/SmartScrollListView$SmartScrollHeaderCallback;
.super Ljava/lang/Object;
.source "SmartScrollListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/widgets/SmartScrollListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SmartScrollHeaderCallback"
.end annotation


# virtual methods
.method public abstract getHeaderHeight()I
.end method

.method public abstract onHide()V
.end method

.method public abstract onMoved(I)V
.end method

.method public abstract onShown()V
.end method
