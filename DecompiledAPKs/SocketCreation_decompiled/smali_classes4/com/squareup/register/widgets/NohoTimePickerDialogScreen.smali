.class public final Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;
.super Ljava/lang/Object;
.source "NohoTimePickerDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/widgets/NohoTimePickerDialogScreen$Event;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001\u0018B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0002\u0010\u0008J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\u0015\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\u00c6\u0003J)\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0014\u0008\u0002\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\u00c6\u0001J\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "state",
        "Lcom/squareup/register/widgets/NohoTimePickerDialogState;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/register/widgets/NohoTimePickerDialogScreen$Event;",
        "",
        "(Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lkotlin/jvm/functions/Function1;)V",
        "getOnEvent",
        "()Lkotlin/jvm/functions/Function1;",
        "getState",
        "()Lcom/squareup/register/widgets/NohoTimePickerDialogState;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "Event",
        "widgets-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onEvent:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final state:Lcom/squareup/register/widgets/NohoTimePickerDialogState;


# direct methods
.method public constructor <init>(Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogState;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->state:Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    iput-object p2, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->state:Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->copy(Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lkotlin/jvm/functions/Function1;)Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/register/widgets/NohoTimePickerDialogState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->state:Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lkotlin/jvm/functions/Function1;)Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogState;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogScreen$Event;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onEvent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;

    invoke-direct {v0, p1, p2}, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;-><init>(Lcom/squareup/register/widgets/NohoTimePickerDialogState;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->state:Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    iget-object v1, p1, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->state:Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getOnEvent()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/register/widgets/NohoTimePickerDialogScreen$Event;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getState()Lcom/squareup/register/widgets/NohoTimePickerDialogState;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->state:Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->state:Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NohoTimePickerDialogScreen(state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->state:Lcom/squareup/register/widgets/NohoTimePickerDialogState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onEvent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/register/widgets/NohoTimePickerDialogScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
