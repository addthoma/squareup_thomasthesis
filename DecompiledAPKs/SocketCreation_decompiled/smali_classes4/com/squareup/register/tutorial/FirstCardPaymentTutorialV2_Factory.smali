.class public final Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;
.super Ljava/lang/Object;
.source "FirstCardPaymentTutorialV2_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final eventLockerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialEventLocker;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final feeTutorialProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;"
        }
    .end annotation
.end field

.field private final lockScreenMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final loggingHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final oracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryPagesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/CardTutorialRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final textRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/CardTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialEventLocker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->loggingHelperProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->textRendererProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->oracleProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->orderEntryPagesProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->resourcesProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->lockScreenMonitorProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p11, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->eventLockerProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p12, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->feeTutorialProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/CardTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/pages/OrderEntryPages;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialEventLocker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feetutorial/FeeTutorial;",
            ">;)",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;"
        }
    .end annotation

    .line 85
    new-instance v13, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/register/tutorial/CardTutorialRunner;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/util/Device;Landroid/content/res/Resources;Lcom/squareup/permissions/ui/LockScreenMonitor;Lcom/squareup/register/tutorial/TutorialEventLocker;Lcom/squareup/feetutorial/FeeTutorial;)Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;
    .locals 14

    .line 94
    new-instance v13, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;-><init>(Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/register/tutorial/CardTutorialRunner;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/util/Device;Landroid/content/res/Resources;Lcom/squareup/permissions/ui/LockScreenMonitor;Lcom/squareup/register/tutorial/TutorialEventLocker;Lcom/squareup/feetutorial/FeeTutorial;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;
    .locals 13

    .line 73
    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->loggingHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->textRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/register/tutorial/CardTutorialRunner;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->oracleProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->orderEntryPagesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/orderentry/pages/OrderEntryPages;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->lockScreenMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/permissions/ui/LockScreenMonitor;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->eventLockerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/register/tutorial/TutorialEventLocker;

    iget-object v0, p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->feeTutorialProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/feetutorial/FeeTutorial;

    invoke-static/range {v1 .. v12}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->newInstance(Lcom/squareup/register/tutorial/FirstCardPaymentLoggingHelper;Lcom/squareup/register/tutorial/FirstPaymentTutorialTextRenderer;Lcom/squareup/register/tutorial/CardTutorialRunner;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lio/reactivex/Scheduler;Lcom/squareup/settings/server/Features;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/util/Device;Landroid/content/res/Resources;Lcom/squareup/permissions/ui/LockScreenMonitor;Lcom/squareup/register/tutorial/TutorialEventLocker;Lcom/squareup/feetutorial/FeeTutorial;)Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2_Factory;->get()Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;

    move-result-object v0

    return-object v0
.end method
