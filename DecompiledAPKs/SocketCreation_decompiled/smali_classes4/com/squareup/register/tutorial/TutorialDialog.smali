.class public Lcom/squareup/register/tutorial/TutorialDialog;
.super Lcom/squareup/dialog/GlassDialog;
.source "TutorialDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/register/tutorial/TutorialDialog$Prompt;,
        Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .line 24
    sget v0, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    .line 25
    const-class v0, Lcom/squareup/register/tutorial/TutorialsComponent;

    .line 26
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/TutorialsComponent;

    invoke-interface {v0}, Lcom/squareup/register/tutorial/TutorialsComponent;->tutorialPresenter()Lcom/squareup/register/tutorial/TutorialPresenter;

    move-result-object v0

    .line 27
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object p1

    check-cast p1, Lcom/squareup/register/tutorial/TutorialDialogScreen;

    iget-object p1, p1, Lcom/squareup/register/tutorial/TutorialDialogScreen;->prompt:Lcom/squareup/register/tutorial/TutorialDialog$Prompt;

    .line 29
    invoke-interface {p1}, Lcom/squareup/register/tutorial/TutorialDialog$Prompt;->getLayoutResId()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/squareup/register/tutorial/TutorialDialog;->setContentView(I)V

    const/4 v1, 0x0

    .line 30
    invoke-virtual {p0, v1}, Lcom/squareup/register/tutorial/TutorialDialog;->setCancelable(Z)V

    .line 31
    invoke-virtual {p0, v1}, Lcom/squareup/register/tutorial/TutorialDialog;->setCanceledOnTouchOutside(Z)V

    .line 32
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/4 v2, -0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 34
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 36
    sget v3, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_content:I

    invoke-static {v1, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/widgets/MessageView;

    .line 37
    sget v4, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_primary:I

    invoke-static {v1, v4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 38
    sget v5, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_secondary:I

    invoke-static {v1, v5}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 40
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialDialog;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {p1, v6}, Lcom/squareup/register/tutorial/TutorialDialog$Prompt;->getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 42
    sget v7, Lcom/squareup/common/tutorial/R$id;->tutorial_dialog_title:I

    invoke-static {v1, v7}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/MessageView;

    .line 43
    invoke-virtual {v1, v6}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/squareup/register/tutorial/TutorialDialog$Prompt;->getContent(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 48
    invoke-virtual {v3, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    :cond_1
    invoke-interface {p1}, Lcom/squareup/register/tutorial/TutorialDialog$Prompt;->getPrimaryButton()I

    move-result v1

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setText(I)V

    .line 52
    new-instance v1, Lcom/squareup/register/tutorial/TutorialDialog$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/register/tutorial/TutorialDialog$1;-><init>(Lcom/squareup/register/tutorial/TutorialDialog;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/register/tutorial/TutorialDialog$Prompt;)V

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    invoke-interface {p1}, Lcom/squareup/register/tutorial/TutorialDialog$Prompt;->getSecondaryButton()I

    move-result v1

    if-ne v1, v2, :cond_2

    const/16 p1, 0x8

    .line 61
    invoke-virtual {v5, p1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 63
    :cond_2
    invoke-virtual {v5, v1}, Landroid/widget/Button;->setText(I)V

    .line 64
    new-instance v1, Lcom/squareup/register/tutorial/TutorialDialog$2;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/register/tutorial/TutorialDialog$2;-><init>(Lcom/squareup/register/tutorial/TutorialDialog;Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/register/tutorial/TutorialDialog$Prompt;)V

    invoke-virtual {v5, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method
