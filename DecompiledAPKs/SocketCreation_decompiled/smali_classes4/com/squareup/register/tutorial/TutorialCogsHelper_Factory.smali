.class public final Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;
.super Ljava/lang/Object;
.source "TutorialCogsHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/register/tutorial/TutorialCogsHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final jailKeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;->jailKeeperProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;->busProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;)",
            "Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/badbus/BadBus;)Lcom/squareup/register/tutorial/TutorialCogsHelper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            "Lcom/squareup/badbus/BadBus;",
            ")",
            "Lcom/squareup/register/tutorial/TutorialCogsHelper;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/register/tutorial/TutorialCogsHelper;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/register/tutorial/TutorialCogsHelper;-><init>(Ljavax/inject/Provider;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/badbus/BadBus;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/register/tutorial/TutorialCogsHelper;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;->cogsProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;->jailKeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/jailkeeper/JailKeeper;

    iget-object v2, p0, Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/badbus/BadBus;

    invoke-static {v0, v1, v2}, Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/badbus/BadBus;)Lcom/squareup/register/tutorial/TutorialCogsHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/register/tutorial/TutorialCogsHelper_Factory;->get()Lcom/squareup/register/tutorial/TutorialCogsHelper;

    move-result-object v0

    return-object v0
.end method
