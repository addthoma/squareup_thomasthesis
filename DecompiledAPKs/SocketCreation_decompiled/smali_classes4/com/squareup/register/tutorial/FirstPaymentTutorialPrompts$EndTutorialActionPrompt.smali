.class Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;
.super Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$FirstPaymentTutorialPrompt;
.source "FirstPaymentTutorialPrompts.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EndTutorialActionPrompt"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final contentId:I

.field private final primaryButtonTextId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 191
    new-instance v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt$1;

    invoke-direct {v0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt$1;-><init>()V

    sput-object v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(II)V
    .locals 0

    .line 144
    invoke-direct {p0}, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$FirstPaymentTutorialPrompt;-><init>()V

    .line 145
    iput p1, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;->contentId:I

    .line 146
    iput p2, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;->primaryButtonTextId:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-nez p1, :cond_1

    return v1

    .line 183
    :cond_1
    const-class v2, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    if-ne v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getContent(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .line 154
    iget v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;->contentId:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getPrimaryButton()I
    .locals 1

    .line 158
    iget v0, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;->primaryButtonTextId:I

    return v0
.end method

.method public getSecondaryButton()I
    .locals 1

    .line 162
    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_done:I

    return v0
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .line 150
    sget v0, Lcom/squareup/pos/tutorials/R$string;->tutorial_fp_end_title:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method handleTap(Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;)V
    .locals 1

    const/4 v0, 0x0

    .line 168
    invoke-virtual {p2, v0}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->exitQuietly(Z)V

    .line 170
    sget-object v0, Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;->PRIMARY:Lcom/squareup/register/tutorial/TutorialDialog$ButtonTap;

    if-ne p1, v0, :cond_0

    .line 171
    invoke-virtual {p2}, Lcom/squareup/register/tutorial/AbstractFirstPaymentTutorial;->completeButtonAction()V

    :cond_0
    return-void
.end method

.method public hashCode()I
    .locals 1

    .line 176
    const-class v0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 187
    iget p2, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;->contentId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 188
    iget p2, p0, Lcom/squareup/register/tutorial/FirstPaymentTutorialPrompts$EndTutorialActionPrompt;->primaryButtonTextId:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
