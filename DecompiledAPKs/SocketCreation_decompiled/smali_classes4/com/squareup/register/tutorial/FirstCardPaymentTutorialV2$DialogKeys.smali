.class final enum Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;
.super Ljava/lang/Enum;
.source "FirstCardPaymentTutorialV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "DialogKeys"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

.field public static final enum EARLY_EXIT:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

.field public static final enum FINISHED:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

.field public static final enum FINISHED_LINK_BANK:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 527
    new-instance v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    const/4 v1, 0x0

    const-string v2, "FINISHED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->FINISHED:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    new-instance v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    const/4 v2, 0x1

    const-string v3, "FINISHED_LINK_BANK"

    invoke-direct {v0, v3, v2}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->FINISHED_LINK_BANK:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    new-instance v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    const/4 v3, 0x2

    const-string v4, "EARLY_EXIT"

    invoke-direct {v0, v4, v3}, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->EARLY_EXIT:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    .line 526
    sget-object v4, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->FINISHED:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->FINISHED_LINK_BANK:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->EARLY_EXIT:Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->$VALUES:[Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 526
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;
    .locals 1

    .line 526
    const-class v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    return-object p0
.end method

.method public static values()[Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;
    .locals 1

    .line 526
    sget-object v0, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->$VALUES:[Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    invoke-virtual {v0}, [Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$DialogKeys;

    return-object v0
.end method
