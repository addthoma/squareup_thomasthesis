.class public Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;
.super Lcom/squareup/backgroundjob/ExecutorBackgroundJob;
.source "StoreAndForwardJobCreator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/StoreAndForwardJobCreator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EnqueueStoreAndForwardTaskJob"
.end annotation


# static fields
.field private static final DEBUG_NOTIFICATION_TEXT_FORMAT_STRING:Ljava/lang/String; = "Store-and-forward task job scheduled to be run in ~%d hrs"

.field private static final DEBUG_NOTIFICATION_TITLE:Ljava/lang/String; = "Debug: Store-and-Forward"

.field public static final ENQUEUE_TAG:Ljava/lang/String;

.field private static final MERCHANT_NAME_KEY:Ljava/lang/String;

.field public static final QUALIFIED_NAME:Ljava/lang/String;

.field private static final USER_ID_KEY:Ljava/lang/String;

.field private static final WAKE_LOCK_TAG:Ljava/lang/String;


# instance fields
.field private final crossSessionStoreAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 64
    const-class v0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->QUALIFIED_NAME:Ljava/lang/String;

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":enqueue.tag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->ENQUEUE_TAG:Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":user.id.key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->USER_ID_KEY:Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":merchant.name.key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->MERCHANT_NAME_KEY:Ljava/lang/String;

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->QUALIFIED_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":wake.lock.tag"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->WAKE_LOCK_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/queue/retrofit/RetrofitQueue;)V
    .locals 1

    .line 79
    sget-object v0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->WAKE_LOCK_TAG:Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/backgroundjob/ExecutorBackgroundJob;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Ljava/lang/String;)V

    .line 80
    iput-object p4, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->crossSessionStoreAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-void
.end method

.method public static enqueueTaskRequest(JLjava/lang/String;Ljava/lang/String;)Lcom/evernote/android/job/JobRequest;
    .locals 3

    .line 91
    new-instance v0, Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    invoke-direct {v0}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;-><init>()V

    .line 92
    sget-object v1, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->USER_ID_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    sget-object p2, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->MERCHANT_NAME_KEY:Ljava/lang/String;

    invoke-virtual {v0, p2, p3}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    new-instance p2, Lcom/evernote/android/job/JobRequest$Builder;

    sget-object p3, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->ENQUEUE_TAG:Ljava/lang/String;

    invoke-direct {p2, p3}, Lcom/evernote/android/job/JobRequest$Builder;-><init>(Ljava/lang/String;)V

    sget-object p3, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    .line 101
    invoke-virtual {p3, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    add-long/2addr v1, p0

    invoke-virtual {p2, p0, p1, v1, v2}, Lcom/evernote/android/job/JobRequest$Builder;->setExecutionWindow(JJ)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p2

    .line 102
    invoke-virtual {p2, v0}, Lcom/evernote/android/job/JobRequest$Builder;->setExtras(Lcom/evernote/android/job/util/support/PersistableBundleCompat;)Lcom/evernote/android/job/JobRequest$Builder;

    move-result-object p2

    .line 103
    invoke-virtual {p2}, Lcom/evernote/android/job/JobRequest$Builder;->build()Lcom/evernote/android/job/JobRequest;

    move-result-object p2

    .line 104
    sget-object p3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 105
    invoke-virtual {v1, p0, p1}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide p0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    const/4 p1, 0x0

    aput-object p0, v0, p1

    const-string p0, "Store-and-forward task job scheduled to be run in ~%d hrs"

    invoke-static {p3, p0, v0}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string p1, "Debug: Store-and-Forward"

    .line 106
    invoke-static {p2, p1, p0}, Lcom/squareup/backgroundjob/notification/BackgroundJobNotifications;->applyExtrasForNotification(Lcom/evernote/android/job/JobRequest;Ljava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method


# virtual methods
.method public runJob(Lcom/squareup/backgroundjob/JobParams;)Lcom/evernote/android/job/Job$Result;
    .locals 3

    .line 111
    invoke-interface {p1}, Lcom/squareup/backgroundjob/JobParams;->getExtras()Lcom/evernote/android/job/util/support/PersistableBundleCompat;

    move-result-object p1

    .line 112
    sget-object v0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->USER_ID_KEY:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    sget-object v2, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->MERCHANT_NAME_KEY:Ljava/lang/String;

    invoke-virtual {p1, v2, v1}, Lcom/evernote/android/job/util/support/PersistableBundleCompat;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 115
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 116
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "userId cannot be blank"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 117
    sget-object p1, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;

    return-object p1

    .line 119
    :cond_0
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 120
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "merchantName should not be blank"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    const-string v2, "Attempt to enqueue StoreAndForwardLoggedOutTask without a merchantName"

    invoke-static {v1, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator$EnqueueStoreAndForwardTaskJob;->crossSessionStoreAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v2, Lcom/squareup/payment/offline/StoreAndForwardTask;

    invoke-direct {v2, v0, p1}, Lcom/squareup/payment/offline/StoreAndForwardTask;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    .line 124
    sget-object p1, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;

    return-object p1
.end method
