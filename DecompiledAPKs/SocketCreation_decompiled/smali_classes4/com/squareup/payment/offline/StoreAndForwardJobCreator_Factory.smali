.class public final Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;
.super Ljava/lang/Object;
.source "StoreAndForwardJobCreator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/offline/StoreAndForwardJobCreator;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final crossSessionStoreAndForwardQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final jobManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;"
        }
    .end annotation
.end field

.field private final jobNotificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->jobManagerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->jobNotificationManagerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->crossSessionStoreAndForwardQueueProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;)",
            "Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/queue/retrofit/RetrofitQueue;)Lcom/squareup/payment/offline/StoreAndForwardJobCreator;
    .locals 7

    .line 58
    new-instance v6, Lcom/squareup/payment/offline/StoreAndForwardJobCreator;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/payment/offline/StoreAndForwardJobCreator;-><init>(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/queue/retrofit/RetrofitQueue;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/payment/offline/StoreAndForwardJobCreator;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/thread/executor/MainThread;

    iget-object v2, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->jobManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/backgroundjob/BackgroundJobManager;

    iget-object v3, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->jobNotificationManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    iget-object v4, p0, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->crossSessionStoreAndForwardQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/queue/retrofit/RetrofitQueue;)Lcom/squareup/payment/offline/StoreAndForwardJobCreator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/payment/offline/StoreAndForwardJobCreator_Factory;->get()Lcom/squareup/payment/offline/StoreAndForwardJobCreator;

    move-result-object v0

    return-object v0
.end method
