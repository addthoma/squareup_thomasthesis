.class public final Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;
.super Ljava/lang/Object;
.source "EnqueueStoredPaymentRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final storedPaymentsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field

.field private final userIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->badBusProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->fileThreadProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->userIdProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->storedPaymentsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->storeAndForwardQueueProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p8, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;)",
            "Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;"
        }
    .end annotation

    .line 66
    new-instance v9, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/badbus/BadBus;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/AccountStatusSettings;Ljava/lang/String;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/ledger/TransactionLedgerManager;)Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;
    .locals 10

    .line 73
    new-instance v9, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;-><init>(Lcom/squareup/badbus/BadBus;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/AccountStatusSettings;Ljava/lang/String;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;
    .locals 9

    .line 57
    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->fileThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/concurrent/Executor;

    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->userIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->storedPaymentsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/queue/StoredPaymentsQueue;

    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->storeAndForwardQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v0, p0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-static/range {v1 .. v8}, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->newInstance(Lcom/squareup/badbus/BadBus;Ljava/util/concurrent/Executor;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/AccountStatusSettings;Ljava/lang/String;Lcom/squareup/queue/StoredPaymentsQueue;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/payment/ledger/TransactionLedgerManager;)Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner_Factory;->get()Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;

    move-result-object v0

    return-object v0
.end method
