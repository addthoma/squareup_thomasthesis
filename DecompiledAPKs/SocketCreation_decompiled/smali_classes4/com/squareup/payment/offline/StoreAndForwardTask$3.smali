.class Lcom/squareup/payment/offline/StoreAndForwardTask$3;
.super Ljava/lang/Object;
.source "StoreAndForwardTask.java"

# interfaces
.implements Lcom/squareup/tape/ObjectQueue$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/offline/StoreAndForwardTask;->getNextBatch(Lcom/squareup/queue/StoredPaymentsQueue;)Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/tape/ObjectQueue$Listener<",
        "Lcom/squareup/payment/offline/StoredPayment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

.field final synthetic val$batch:Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;


# direct methods
.method constructor <init>(Lcom/squareup/payment/offline/StoreAndForwardTask;Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;)V
    .locals 0

    .line 195
    iput-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$3;->this$0:Lcom/squareup/payment/offline/StoreAndForwardTask;

    iput-object p2, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$3;->val$batch:Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdd(Lcom/squareup/tape/ObjectQueue;Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ")V"
        }
    .end annotation

    if-eqz p2, :cond_0

    .line 199
    iget-object p1, p0, Lcom/squareup/payment/offline/StoreAndForwardTask$3;->val$batch:Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;

    invoke-virtual {p1, p2}, Lcom/squareup/payment/offline/StoreAndForwardTask$Batch;->addIfNotFull(Lcom/squareup/payment/offline/StoredPayment;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onAdd(Lcom/squareup/tape/ObjectQueue;Ljava/lang/Object;)V
    .locals 0

    .line 195
    check-cast p2, Lcom/squareup/payment/offline/StoredPayment;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/payment/offline/StoreAndForwardTask$3;->onAdd(Lcom/squareup/tape/ObjectQueue;Lcom/squareup/payment/offline/StoredPayment;)V

    return-void
.end method

.method public onRemove(Lcom/squareup/tape/ObjectQueue;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
