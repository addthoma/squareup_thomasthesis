.class final Lcom/squareup/payment/offline/ForwardedPaymentsKt$maybeForwardedPaymentWithId$1;
.super Ljava/lang/Object;
.source "ForwardedPayments.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/offline/ForwardedPaymentsKt;->maybeForwardedPaymentWithId(Lio/reactivex/Single;Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nForwardedPayments.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ForwardedPayments.kt\ncom/squareup/payment/offline/ForwardedPaymentsKt$maybeForwardedPaymentWithId$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,18:1\n250#2,2:19\n*E\n*S KotlinDebug\n*F\n+ 1 ForwardedPayments.kt\ncom/squareup/payment/offline/ForwardedPaymentsKt$maybeForwardedPaymentWithId$1\n*L\n16#1,2:19\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/payment/offline/ForwardedPayment;",
        "list",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $transactionId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentsKt$maybeForwardedPaymentWithId$1;->$transactionId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/Collection;)Lcom/squareup/util/Optional;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;)",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/payment/offline/ForwardedPayment;",
            ">;"
        }
    .end annotation

    const-string v0, "list"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v0, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    check-cast p1, Ljava/lang/Iterable;

    .line 19
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/payment/offline/ForwardedPayment;

    .line 16
    invoke-virtual {v2}, Lcom/squareup/payment/offline/ForwardedPayment;->getUniqueKey()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/offline/ForwardedPaymentsKt$maybeForwardedPaymentWithId$1;->$transactionId:Ljava/lang/String;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/util/Optional$Companion;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/util/Collection;

    invoke-virtual {p0, p1}, Lcom/squareup/payment/offline/ForwardedPaymentsKt$maybeForwardedPaymentWithId$1;->apply(Ljava/util/Collection;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method
