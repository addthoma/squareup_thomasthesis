.class Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;
.super Ljava/lang/Object;
.source "ForwardedPaymentManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/offline/ForwardedPaymentManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Result"
.end annotation


# instance fields
.field final billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

.field final paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)V
    .locals 1

    .line 357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 358
    iput-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    .line 359
    iput-object p1, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    return-void
.end method


# virtual methods
.method addTo(Lcom/squareup/payment/offline/ForwardedPayment;)Lcom/squareup/payment/offline/ForwardedPayment;
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    if-eqz v0, :cond_0

    .line 369
    invoke-virtual {p1, v0}, Lcom/squareup/payment/offline/ForwardedPayment;->withResult(Lcom/squareup/protos/queuebert/model/PaymentResult;)Lcom/squareup/payment/offline/ForwardedPayment;

    move-result-object p1

    return-object p1

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    invoke-virtual {p1, v0}, Lcom/squareup/payment/offline/ForwardedPayment;->withResult(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;)Lcom/squareup/payment/offline/ForwardedPayment;

    move-result-object p1

    return-object p1
.end method

.method getUniqueKey()Ljava/lang/String;
    .locals 1

    .line 363
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/queuebert/model/PaymentResult;->unique_key:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    iget-object v0, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 376
    iget-object v0, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    const-string v1, "]"

    if-eqz v0, :cond_0

    .line 377
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Result[paymentResult="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;->paymentResult:Lcom/squareup/protos/queuebert/model/PaymentResult;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 379
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Result[billProcessingResult="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/payment/offline/ForwardedPaymentManager$Result;->billProcessingResult:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
