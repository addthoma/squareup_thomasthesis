.class public final Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;
.super Ljava/lang/Object;
.source "AccountStatusStoreAndForwardEnabledSetting.kt"

# interfaces
.implements Lcom/squareup/payment/StoreAndForwardEnabledSetting;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0007H\u0016R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;",
        "Lcom/squareup/payment/StoreAndForwardEnabledSetting;",
        "statusProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "(Ljavax/inject/Provider;)V",
        "enabled",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final statusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "statusProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;->statusProvider:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public enabled()Z
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/payment/AccountStatusStoreAndForwardEnabledSetting;->statusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
