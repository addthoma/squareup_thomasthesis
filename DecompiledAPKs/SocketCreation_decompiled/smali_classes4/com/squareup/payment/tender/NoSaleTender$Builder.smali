.class public Lcom/squareup/payment/tender/NoSaleTender$Builder;
.super Lcom/squareup/payment/tender/BaseLocalTender$Builder;
.source "NoSaleTender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/NoSaleTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;)V
    .locals 2

    .line 53
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;->NO_SALE:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    sget-object v1, Lcom/squareup/protos/client/bills/Tender$Type;->NO_SALE:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/payment/tender/BaseLocalTender$Builder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Lcom/squareup/protos/client/bills/Tender$Type;)V

    .line 54
    iget-object p1, p1, Lcom/squareup/payment/tender/TenderFactory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/payment/tender/NoSaleTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/payment/tender/NoSaleTender$Builder;->build()Lcom/squareup/payment/tender/NoSaleTender;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/squareup/payment/tender/NoSaleTender;
    .locals 2

    .line 62
    new-instance v0, Lcom/squareup/payment/tender/NoSaleTender;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/tender/NoSaleTender;-><init>(Lcom/squareup/payment/tender/NoSaleTender$Builder;Lcom/squareup/payment/tender/NoSaleTender$1;)V

    return-object v0
.end method

.method public canBeZeroAmount()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
