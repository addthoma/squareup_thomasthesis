.class public Lcom/squareup/payment/tender/MagStripeTenderBuilder$CreditCardTenderBuilder;
.super Lcom/squareup/payment/tender/MagStripeTenderBuilder;
.source "MagStripeTenderBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/tender/MagStripeTenderBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CreditCardTenderBuilder"
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/settings/server/SwipeChipCardsSettings;)V
    .locals 1

    const/4 v0, 0x0

    .line 87
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/payment/tender/MagStripeTenderBuilder;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/settings/server/SwipeChipCardsSettings;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/payment/tender/BaseTender;
    .locals 1

    .line 85
    invoke-virtual {p0}, Lcom/squareup/payment/tender/MagStripeTenderBuilder$CreditCardTenderBuilder;->build()Lcom/squareup/payment/tender/MagStripeTender;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/squareup/payment/tender/MagStripeTender;
    .locals 1

    .line 95
    new-instance v0, Lcom/squareup/payment/tender/MagStripeTender;

    invoke-direct {v0, p0}, Lcom/squareup/payment/tender/MagStripeTender;-><init>(Lcom/squareup/payment/tender/MagStripeTenderBuilder;)V

    return-object v0
.end method

.method public isGiftCard()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
