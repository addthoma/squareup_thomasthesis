.class public final Lcom/squareup/payment/PaymentFlowTaskProvider;
.super Ljava/lang/Object;
.source "PaymentFlowTaskProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentFlowTaskProvider.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentFlowTaskProvider.kt\ncom/squareup/payment/PaymentFlowTaskProvider\n*L\n1#1,383:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a8\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0010#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001<B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJH\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u001e2\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u001f0!2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001f0!J\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eJJ\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\'2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010,\u001a\u00020\u001c2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0-JD\u0010.\u001a\u00020/2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\'2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010,\u001a\u00020\u001c2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0-H\u0002JJ\u00100\u001a\u00020\u000e2\u0006\u0010%\u001a\u00020&2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\'2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010,\u001a\u00020\u001c2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0-J,\u00101\u001a\u0002022\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001f0!2\u0006\u0010*\u001a\u00020+2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u001eH\u0002JR\u00103\u001a\u0002042\u0006\u00105\u001a\u0002062\u0006\u0010%\u001a\u00020&2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\'2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010,\u001a\u00020\u001c2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0-J\u001e\u00107\u001a\u00020\u00102\u0006\u0010*\u001a\u00020+2\u000c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u001eH\u0002J\u0010\u00108\u001a\u0002092\u0006\u0010:\u001a\u00020)H\u0002J\u0008\u0010;\u001a\u00020&H\u0002R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u00108BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u0012R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u00020\u00108BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0012\u00a8\u0006="
    }
    d2 = {
        "Lcom/squareup/payment/PaymentFlowTaskProvider;",
        "",
        "paymentConfig",
        "Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "serverClock",
        "Lcom/squareup/account/ServerClock;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;)V",
        "completeBill",
        "Lcom/squareup/queue/bills/CompleteBill;",
        "paymentConfigPreviousPayments",
        "Lcom/squareup/protos/common/Money;",
        "getPaymentConfigPreviousPayments",
        "()Lcom/squareup/protos/common/Money;",
        "zeroMoney",
        "getZeroMoney",
        "cancelTenders",
        "Lcom/squareup/queue/CancelTask;",
        "billId",
        "Lcom/squareup/protos/client/IdPair;",
        "reason",
        "Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;",
        "merchantToken",
        "",
        "capturedTenders",
        "",
        "Lcom/squareup/payment/tender/BaseTender;",
        "canceledTenders",
        "",
        "flushedTenders",
        "createCaptureTask",
        "Lcom/squareup/queue/CaptureTask;",
        "userInitiated",
        "",
        "",
        "createdAt",
        "Ljava/util/Date;",
        "order",
        "Lcom/squareup/payment/OrderSnapshot;",
        "apiClientId",
        "",
        "createCaptureTendersTask",
        "Lcom/squareup/queue/bills/CaptureTendersTask;",
        "createCompleteBill",
        "getCompleteBillCart",
        "Lcom/squareup/protos/client/bills/Cart;",
        "getLocalQueueTask",
        "Lcom/squareup/queue/LocalPaymentsQueueTask;",
        "addTendersRequest",
        "Lcom/squareup/protos/client/bills/AddTendersRequest;",
        "getUnboundedRemainingAmountDue",
        "iso8601Date",
        "Lcom/squareup/protos/client/ISO8601Date;",
        "date",
        "useDefaultTasks",
        "Factory",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private completeBill:Lcom/squareup/queue/bills/CompleteBill;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

.field private final serverClock:Lcom/squareup/account/ServerClock;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;)V
    .locals 1

    const-string v0, "paymentConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serverClock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    iput-object p2, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p3, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->serverClock:Lcom/squareup/account/ServerClock;

    iput-object p4, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p5, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private final createCaptureTendersTask(Ljava/util/Set;Ljava/util/Date;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/queue/bills/CaptureTendersTask;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Ljava/util/Date;",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)",
            "Lcom/squareup/queue/bills/CaptureTendersTask;"
        }
    .end annotation

    .line 191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 192
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 194
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/payment/tender/BaseTender;

    .line 195
    invoke-virtual {v3}, Lcom/squareup/payment/tender/BaseTender;->requireTenderForHistory()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v4

    .line 196
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    new-instance v4, Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    invoke-direct {v4}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;-><init>()V

    .line 199
    invoke-virtual {v3}, Lcom/squareup/payment/tender/BaseTender;->requireTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v4

    .line 201
    new-instance v5, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    invoke-direct {v5}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;-><init>()V

    .line 202
    invoke-virtual {v3}, Lcom/squareup/payment/tender/BaseTender;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    move-result-object v5

    .line 203
    invoke-virtual {v3}, Lcom/squareup/payment/tender/BaseTender;->getTipPercentage()Lcom/squareup/util/Percentage;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    :goto_1
    invoke-virtual {v5, v6}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    move-result-object v5

    .line 204
    invoke-virtual {v3}, Lcom/squareup/payment/tender/BaseTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->total_charged_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    move-result-object v5

    .line 205
    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    move-result-object v5

    .line 200
    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->amounts(Lcom/squareup/protos/client/bills/CompleteTender$Amounts;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v4

    .line 207
    invoke-virtual {v3}, Lcom/squareup/payment/tender/BaseTender;->getCompleteDetails()Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->complete_details(Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v4

    .line 208
    invoke-virtual {v3}, Lcom/squareup/payment/tender/BaseTender;->getCompleteTenderDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->extra_tender_details(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v3

    .line 209
    invoke-virtual {v3}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender;

    move-result-object v3

    .line 210
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 212
    :cond_1
    invoke-direct {p0, p1, p3, p6}, Lcom/squareup/payment/PaymentFlowTaskProvider;->getCompleteBillCart(Ljava/util/Set;Lcom/squareup/payment/OrderSnapshot;Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    .line 214
    new-instance p6, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    invoke-direct {p6}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;-><init>()V

    .line 215
    invoke-direct {p0, p2}, Lcom/squareup/payment/PaymentFlowTaskProvider;->iso8601Date(Ljava/util/Date;)Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p2

    invoke-virtual {p6, p2}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    move-result-object p2

    .line 216
    iget-object p6, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->serverClock:Lcom/squareup/account/ServerClock;

    invoke-virtual {p6}, Lcom/squareup/account/ServerClock;->getCurrentTime()Ljava/util/Date;

    move-result-object p6

    const-string v2, "serverClock.currentTime"

    invoke-static {p6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p6}, Lcom/squareup/payment/PaymentFlowTaskProvider;->iso8601Date(Ljava/util/Date;)Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p6

    invoke-virtual {p2, p6}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->completed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    move-result-object p2

    .line 217
    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->build()Lcom/squareup/protos/client/bills/Bill$Dates;

    move-result-object p2

    .line 219
    new-instance p6, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    invoke-direct {p6}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;-><init>()V

    .line 220
    invoke-virtual {p6, p4}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    move-result-object p4

    .line 221
    check-cast v1, Ljava/util/List;

    invoke-virtual {p4, v1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->tenders(Ljava/util/List;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    move-result-object p4

    .line 222
    invoke-virtual {p4, p1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    move-result-object p1

    .line 223
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->dates(Lcom/squareup/protos/client/bills/Bill$Dates;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    move-result-object p1

    .line 225
    new-instance p2, Lcom/squareup/protos/client/Merchant$Builder;

    invoke-direct {p2}, Lcom/squareup/protos/client/Merchant$Builder;-><init>()V

    .line 226
    iget-object p4, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p4}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p4

    const-string p6, "settings.userSettings"

    invoke-static {p4, p6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p4}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Lcom/squareup/protos/client/Merchant$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/Merchant$Builder;

    move-result-object p2

    .line 227
    invoke-virtual {p2}, Lcom/squareup/protos/client/Merchant$Builder;->build()Lcom/squareup/protos/client/Merchant;

    move-result-object p2

    .line 224
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->merchant(Lcom/squareup/protos/client/Merchant;)Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;

    move-result-object p1

    .line 229
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CaptureTendersRequest$Builder;->build()Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    move-result-object v2

    .line 231
    invoke-virtual {p3}, Lcom/squareup/payment/OrderSnapshot;->getTicketId()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    const-string p1, ""

    :goto_2
    move-object v4, p1

    .line 233
    new-instance p1, Lcom/squareup/queue/bills/CaptureTendersTask;

    const-string p2, "request"

    .line 234
    invoke-static {v2, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v0

    check-cast v5, Ljava/util/List;

    .line 235
    invoke-virtual {p3}, Lcom/squareup/payment/OrderSnapshot;->getAdjustmentsForRequest()Ljava/util/List;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/server/payment/AdjusterMessages;->asAdjustmentMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    const-string p2, "asAdjustmentMessages(order.adjustmentsForRequest)"

    invoke-static {v6, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    invoke-virtual {p3}, Lcom/squareup/payment/OrderSnapshot;->getItemizationsForRequest()Ljava/util/List;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/server/payment/AdjusterMessages;->asItemizationMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    const-string p2, "asItemizationMessages(or\u2026r.itemizationsForRequest)"

    invoke-static {v7, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, p1

    move-object v3, p5

    .line 233
    invoke-direct/range {v1 .. v7}, Lcom/squareup/queue/bills/CaptureTendersTask;-><init>(Lcom/squareup/protos/client/bills/CaptureTendersRequest;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object p1
.end method

.method private final getCompleteBillCart(Ljava/util/Set;Lcom/squareup/payment/OrderSnapshot;Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)",
            "Lcom/squareup/protos/client/bills/Cart;"
        }
    .end annotation

    .line 245
    invoke-direct {p0}, Lcom/squareup/payment/PaymentFlowTaskProvider;->getZeroMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    const/4 v1, 0x0

    .line 246
    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 249
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v2, 0x1

    move-object v3, v1

    move-object v1, v0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/payment/tender/BaseTender;

    .line 250
    invoke-virtual {v4}, Lcom/squareup/payment/tender/BaseTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    const-string v5, "MoneyMath.sum(totalCollectedMoney, tender.total)"

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    invoke-virtual {v4}, Lcom/squareup/payment/tender/BaseTender;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/squareup/money/MoneyMath;->sumNullSafe(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 253
    invoke-virtual {v4}, Lcom/squareup/payment/tender/BaseTender;->supportsPaperSigAndTip()Z

    move-result v4

    xor-int/2addr v4, v2

    and-int/2addr v0, v4

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    if-nez v3, :cond_1

    .line 259
    invoke-direct {p0}, Lcom/squareup/payment/PaymentFlowTaskProvider;->getZeroMoney()Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 264
    :cond_1
    invoke-direct {p0}, Lcom/squareup/payment/PaymentFlowTaskProvider;->getPaymentConfigPreviousPayments()Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 265
    invoke-static {v1, p1}, Lcom/squareup/money/MoneyMathOperatorsKt;->plus(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 270
    :cond_2
    invoke-direct {p0, p2, p3}, Lcom/squareup/payment/PaymentFlowTaskProvider;->getUnboundedRemainingAmountDue(Lcom/squareup/payment/OrderSnapshot;Ljava/util/List;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/payment/SwedishRounding;->getDifference(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 268
    invoke-virtual {p2, v1, v3, p1}, Lcom/squareup/payment/OrderSnapshot;->getCartProtoForBill(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    const-string p2, "order.getCartProtoForBil\u2026, capturedTenders))\n    )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getPaymentConfigPreviousPayments()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 356
    iget-object v0, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PAYMENT_FLOW_USE_PAYMENT_CONFIG:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getAmountPaidOnBill()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private final getUnboundedRemainingAmountDue(Lcom/squareup/payment/OrderSnapshot;Ljava/util/List;)Lcom/squareup/protos/common/Money;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    .line 289
    invoke-virtual {p1}, Lcom/squareup/payment/OrderSnapshot;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 290
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/tender/BaseTender;

    .line 291
    invoke-virtual {v0}, Lcom/squareup/payment/tender/BaseTender;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p2, "amountDue"

    .line 293
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getZeroMoney()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 362
    iget-object v0, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method private final iso8601Date(Ljava/util/Date;)Lcom/squareup/protos/client/ISO8601Date;
    .locals 1

    .line 275
    new-instance v0, Lcom/squareup/protos/client/ISO8601Date$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ISO8601Date$Builder;-><init>()V

    .line 276
    invoke-static {p1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/ISO8601Date$Builder;->date_string(Ljava/lang/String;)Lcom/squareup/protos/client/ISO8601Date$Builder;

    move-result-object p1

    .line 277
    invoke-virtual {p1}, Lcom/squareup/protos/client/ISO8601Date$Builder;->build()Lcom/squareup/protos/client/ISO8601Date;

    move-result-object p1

    const-string v0, "ISO8601Date.Builder()\n  \u20261(date))\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final useDefaultTasks()Z
    .locals 2

    .line 351
    iget-object v0, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PAYMENT_FLOW_USE_PAYMENT_CONFIG:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public final cancelTenders(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;)Lcom/squareup/queue/CancelTask;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Ljava/util/Set<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Ljava/util/Set<",
            "+",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)",
            "Lcom/squareup/queue/CancelTask;"
        }
    .end annotation

    const-string v0, "billId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reason"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "merchantToken"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capturedTenders"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "canceledTenders"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flushedTenders"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 312
    invoke-direct {p0}, Lcom/squareup/payment/PaymentFlowTaskProvider;->useDefaultTasks()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->hasCapturedTenders()Z

    move-result v0

    if-nez v0, :cond_0

    goto/16 :goto_3

    .line 315
    :cond_0
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    check-cast p2, Ljava/util/List;

    .line 316
    invoke-interface {p5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p5

    const-string v0, "RemoveTender.Builder()\n \u2026\n                .build()"

    if-eqz p5, :cond_1

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p5

    check-cast p5, Lcom/squareup/payment/tender/BaseTender;

    .line 318
    new-instance v1, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;-><init>()V

    .line 319
    invoke-virtual {p5}, Lcom/squareup/payment/tender/BaseTender;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    move-result-object v1

    .line 320
    invoke-virtual {p5}, Lcom/squareup/payment/tender/BaseTender;->getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;

    move-result-object p5

    invoke-virtual {v1, p5}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    move-result-object p5

    .line 321
    invoke-virtual {p5}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->build()Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    move-result-object p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 317
    invoke-interface {p2, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 324
    :cond_1
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/payment/tender/BaseTender;

    .line 326
    new-instance p5, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    invoke-direct {p5}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;-><init>()V

    .line 327
    invoke-virtual {p4}, Lcom/squareup/payment/tender/BaseTender;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {p5, v1}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    move-result-object p5

    .line 328
    invoke-virtual {p4}, Lcom/squareup/payment/tender/BaseTender;->getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;

    move-result-object p4

    invoke-virtual {p5, p4}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    move-result-object p4

    .line 329
    invoke-virtual {p4}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->build()Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    move-result-object p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 325
    invoke-interface {p2, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 332
    :cond_2
    invoke-interface {p6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_2
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p4

    if-eqz p4, :cond_3

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/payment/tender/BaseTender;

    .line 334
    new-instance p5, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    invoke-direct {p5}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;-><init>()V

    .line 335
    invoke-virtual {p4}, Lcom/squareup/payment/tender/BaseTender;->getIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object p6

    invoke-virtual {p5, p6}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    move-result-object p5

    .line 336
    invoke-virtual {p4}, Lcom/squareup/payment/tender/BaseTender;->getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;

    move-result-object p4

    invoke-virtual {p5, p4}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->tender_type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;

    move-result-object p4

    .line 337
    invoke-virtual {p4}, Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender$Builder;->build()Lcom/squareup/protos/client/bills/RemoveTendersRequest$RemoveTender;

    move-result-object p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 333
    invoke-interface {p2, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 340
    :cond_3
    new-instance p3, Lcom/squareup/queue/bills/RemoveTendersTask;

    .line 342
    new-instance p4, Lcom/squareup/protos/client/Merchant$Builder;

    invoke-direct {p4}, Lcom/squareup/protos/client/Merchant$Builder;-><init>()V

    .line 343
    iget-object p5, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p5}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p5

    const-string p6, "settings.userSettings"

    invoke-static {p5, p6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p5}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p4, p5}, Lcom/squareup/protos/client/Merchant$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/Merchant$Builder;

    move-result-object p4

    .line 344
    invoke-virtual {p4}, Lcom/squareup/protos/client/Merchant$Builder;->build()Lcom/squareup/protos/client/Merchant;

    move-result-object p4

    const-string p5, "Merchant.Builder()\n     \u2026n)\n              .build()"

    invoke-static {p4, p5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 340
    invoke-direct {p3, p1, p4, p2}, Lcom/squareup/queue/bills/RemoveTendersTask;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/Merchant;Ljava/util/List;)V

    check-cast p3, Lcom/squareup/queue/CancelTask;

    goto :goto_4

    .line 313
    :cond_4
    :goto_3
    new-instance p4, Lcom/squareup/queue/bills/CancelBill;

    invoke-direct {p4, p1, p2, p3}, Lcom/squareup/queue/bills/CancelBill;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;)V

    move-object p3, p4

    check-cast p3, Lcom/squareup/queue/CancelTask;

    :goto_4
    return-object p3
.end method

.method public final completeBill()Lcom/squareup/queue/bills/CompleteBill;
    .locals 1

    .line 297
    iget-object v0, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->completeBill:Lcom/squareup/queue/bills/CompleteBill;

    return-object v0
.end method

.method public final createCaptureTask(ZLjava/util/Set;Ljava/util/Date;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/queue/CaptureTask;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Set<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Ljava/util/Date;",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)",
            "Lcom/squareup/queue/CaptureTask;"
        }
    .end annotation

    const-string v0, "flushedTenders"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createdAt"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "order"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billId"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiClientId"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capturedTenders"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-direct {p0}, Lcom/squareup/payment/PaymentFlowTaskProvider;->useDefaultTasks()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/payment/PaymentFlowTaskProvider;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->isLastPaymentOnBill()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    .line 106
    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/PaymentFlowTaskProvider;->createCaptureTendersTask(Ljava/util/Set;Ljava/util/Date;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/queue/bills/CaptureTendersTask;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/CaptureTask;

    goto :goto_1

    .line 96
    :cond_1
    :goto_0
    invoke-virtual/range {p0 .. p7}, Lcom/squareup/payment/PaymentFlowTaskProvider;->createCompleteBill(ZLjava/util/Set;Ljava/util/Date;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/queue/bills/CompleteBill;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/CaptureTask;

    :goto_1
    return-object p1
.end method

.method public final createCompleteBill(ZLjava/util/Set;Ljava/util/Date;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/queue/bills/CompleteBill;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Set<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Ljava/util/Date;",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)",
            "Lcom/squareup/queue/bills/CompleteBill;"
        }
    .end annotation

    move-object v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p7

    const-string v6, "flushedTenders"

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "createdAt"

    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "order"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "billId"

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "apiClientId"

    move-object/from16 v13, p6

    invoke-static {v13, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v6, "capturedTenders"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 127
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 129
    invoke-interface/range {p2 .. p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/payment/tender/BaseTender;

    .line 130
    invoke-virtual {v9}, Lcom/squareup/payment/tender/BaseTender;->requireTenderForHistory()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v10

    .line 131
    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    invoke-virtual {v9}, Lcom/squareup/payment/tender/BaseTender;->getTipPercentage()Lcom/squareup/util/Percentage;

    move-result-object v10

    .line 134
    new-instance v11, Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    invoke-direct {v11}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;-><init>()V

    .line 135
    invoke-virtual {v9}, Lcom/squareup/payment/tender/BaseTender;->requireTender()Lcom/squareup/protos/client/bills/Tender;

    move-result-object v12

    iget-object v12, v12, Lcom/squareup/protos/client/bills/Tender;->tender_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v11, v12}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->tender_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v11

    .line 137
    new-instance v12, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    invoke-direct {v12}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;-><init>()V

    .line 138
    invoke-virtual {v9}, Lcom/squareup/payment/tender/BaseTender;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v14

    invoke-virtual {v12, v14}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    move-result-object v12

    if-eqz v10, :cond_0

    .line 139
    invoke-virtual {v10}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_1

    :cond_0
    const/4 v10, 0x0

    :goto_1
    invoke-virtual {v12, v10}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->tip_percentage(Ljava/lang/String;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    move-result-object v10

    .line 140
    invoke-virtual {v9}, Lcom/squareup/payment/tender/BaseTender;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v12

    invoke-virtual {v10, v12}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->total_charged_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;

    move-result-object v10

    .line 141
    invoke-virtual {v10}, Lcom/squareup/protos/client/bills/CompleteTender$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender$Amounts;

    move-result-object v10

    .line 136
    invoke-virtual {v11, v10}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->amounts(Lcom/squareup/protos/client/bills/CompleteTender$Amounts;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v10

    .line 143
    invoke-virtual {v9}, Lcom/squareup/payment/tender/BaseTender;->getCompleteDetails()Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->complete_details(Lcom/squareup/protos/client/bills/CompleteTender$CompleteDetails;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v10

    .line 144
    invoke-virtual {v9}, Lcom/squareup/payment/tender/BaseTender;->getCompleteTenderDetails()Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;

    move-result-object v9

    invoke-virtual {v10, v9}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->extra_tender_details(Lcom/squareup/protos/client/bills/Tender$CompleteTenderDetails;)Lcom/squareup/protos/client/bills/CompleteTender$Builder;

    move-result-object v9

    .line 145
    invoke-virtual {v9}, Lcom/squareup/protos/client/bills/CompleteTender$Builder;->build()Lcom/squareup/protos/client/bills/CompleteTender;

    move-result-object v9

    .line 146
    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 149
    :cond_1
    new-instance v8, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    invoke-direct {v8}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;-><init>()V

    .line 150
    invoke-direct {p0, v2}, Lcom/squareup/payment/PaymentFlowTaskProvider;->iso8601Date(Ljava/util/Date;)Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    move-result-object v2

    .line 151
    iget-object v8, v0, Lcom/squareup/payment/PaymentFlowTaskProvider;->serverClock:Lcom/squareup/account/ServerClock;

    invoke-virtual {v8}, Lcom/squareup/account/ServerClock;->getCurrentTime()Ljava/util/Date;

    move-result-object v8

    const-string v9, "serverClock.currentTime"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v8}, Lcom/squareup/payment/PaymentFlowTaskProvider;->iso8601Date(Ljava/util/Date;)Lcom/squareup/protos/client/ISO8601Date;

    move-result-object v8

    invoke-virtual {v2, v8}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->completed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Bill$Dates$Builder;

    move-result-object v2

    .line 152
    invoke-virtual {v2}, Lcom/squareup/protos/client/bills/Bill$Dates$Builder;->build()Lcom/squareup/protos/client/bills/Bill$Dates;

    move-result-object v2

    .line 153
    invoke-direct {p0, v1, v3, v5}, Lcom/squareup/payment/PaymentFlowTaskProvider;->getCompleteBillCart(Ljava/util/Set;Lcom/squareup/payment/OrderSnapshot;Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v1

    .line 154
    new-instance v5, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    invoke-direct {v5}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;-><init>()V

    .line 155
    invoke-virtual {v5, v4}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object v4

    .line 157
    new-instance v5, Lcom/squareup/protos/client/Merchant$Builder;

    invoke-direct {v5}, Lcom/squareup/protos/client/Merchant$Builder;-><init>()V

    .line 158
    iget-object v8, v0, Lcom/squareup/payment/PaymentFlowTaskProvider;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v8}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v8

    const-string v9, "settings.userSettings"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/squareup/protos/client/Merchant$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/Merchant$Builder;

    move-result-object v5

    .line 159
    invoke-virtual {v5}, Lcom/squareup/protos/client/Merchant$Builder;->build()Lcom/squareup/protos/client/Merchant;

    move-result-object v5

    .line 156
    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->merchant(Lcom/squareup/protos/client/Merchant;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object v4

    .line 161
    check-cast v7, Ljava/util/List;

    invoke-virtual {v4, v7}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->tender(Ljava/util/List;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object v4

    .line 162
    invoke-virtual {v4, v1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object v1

    .line 163
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->dates(Lcom/squareup/protos/client/bills/Bill$Dates;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object v1

    .line 164
    iget-object v2, v0, Lcom/squareup/payment/PaymentFlowTaskProvider;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldUseBillAmendments()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->is_amendable(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object v1

    if-eqz p1, :cond_2

    .line 167
    sget-object v2, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->COMPLETE_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    goto :goto_2

    .line 169
    :cond_2
    sget-object v2, Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;->COMPLETE_BILL_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;

    .line 165
    :goto_2
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->complete_bill_type(Lcom/squareup/protos/client/bills/CompleteBillRequest$CompleteBillType;)Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;

    move-result-object v1

    .line 172
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CompleteBillRequest$Builder;->build()Lcom/squareup/protos/client/bills/CompleteBillRequest;

    move-result-object v8

    .line 174
    new-instance v1, Lcom/squareup/queue/bills/CompleteBill;

    .line 176
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/payment/OrderSnapshot;->getItemizationsForRequest()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/server/payment/AdjusterMessages;->asItemizationMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    .line 177
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/payment/OrderSnapshot;->getAdjustmentsForRequest()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/server/payment/AdjusterMessages;->asAdjustmentMessages(Ljava/util/List;)Ljava/util/List;

    move-result-object v10

    move-object v11, v6

    check-cast v11, Ljava/util/List;

    invoke-virtual/range {p4 .. p4}, Lcom/squareup/payment/OrderSnapshot;->getTicketId()Ljava/lang/String;

    move-result-object v12

    .line 178
    invoke-direct {p0}, Lcom/squareup/payment/PaymentFlowTaskProvider;->getPaymentConfigPreviousPayments()Lcom/squareup/protos/common/Money;

    move-result-object v14

    move-object v7, v1

    move-object/from16 v13, p6

    .line 174
    invoke-direct/range {v7 .. v14}, Lcom/squareup/queue/bills/CompleteBill;-><init>(Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    iput-object v1, v0, Lcom/squareup/payment/PaymentFlowTaskProvider;->completeBill:Lcom/squareup/queue/bills/CompleteBill;

    .line 180
    iget-object v1, v0, Lcom/squareup/payment/PaymentFlowTaskProvider;->completeBill:Lcom/squareup/queue/bills/CompleteBill;

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    return-object v1
.end method

.method public final getLocalQueueTask(Lcom/squareup/protos/client/bills/AddTendersRequest;ZLjava/util/Set;Ljava/util/Date;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/queue/LocalPaymentsQueueTask;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/AddTendersRequest;",
            "Z",
            "Ljava/util/Set<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;",
            "Ljava/util/Date;",
            "Lcom/squareup/payment/OrderSnapshot;",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/tender/BaseTender;",
            ">;)",
            "Lcom/squareup/queue/LocalPaymentsQueueTask;"
        }
    .end annotation

    const-string v0, "addTendersRequest"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flushedTenders"

    move-object v1, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createdAt"

    move-object v6, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "order"

    move-object/from16 v7, p5

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billId"

    move-object/from16 v8, p6

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiClientId"

    move-object/from16 v9, p7

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "capturedTenders"

    move-object/from16 v10, p8

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-direct {p0}, Lcom/squareup/payment/PaymentFlowTaskProvider;->useDefaultTasks()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, p0

    iget-object v3, v0, Lcom/squareup/payment/PaymentFlowTaskProvider;->paymentConfig:Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->isLastPaymentOnBill()Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    move-object v3, p0

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    .line 74
    invoke-direct/range {v3 .. v9}, Lcom/squareup/payment/PaymentFlowTaskProvider;->createCaptureTendersTask(Ljava/util/Set;Ljava/util/Date;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/queue/bills/CaptureTendersTask;

    move-result-object v1

    .line 78
    new-instance v9, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;

    .line 79
    invoke-virtual {v1}, Lcom/squareup/queue/bills/CaptureTendersTask;->getRequest()Lcom/squareup/protos/client/bills/CaptureTendersRequest;

    move-result-object v3

    invoke-virtual {v1}, Lcom/squareup/queue/bills/CaptureTendersTask;->getCaptureTicketId()Ljava/lang/String;

    move-result-object v4

    .line 80
    invoke-virtual {v1}, Lcom/squareup/queue/bills/CaptureTendersTask;->getClientId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/squareup/queue/bills/CaptureTendersTask;->getTenders()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v1}, Lcom/squareup/queue/bills/CaptureTendersTask;->getAdjustments()Ljava/util/List;

    move-result-object v7

    .line 81
    invoke-virtual {v1}, Lcom/squareup/queue/bills/CaptureTendersTask;->getItemizations()Ljava/util/List;

    move-result-object v8

    move-object v1, v9

    move-object v2, p1

    .line 78
    invoke-direct/range {v1 .. v8}, Lcom/squareup/queue/bills/AddAndCaptureTendersTask;-><init>(Lcom/squareup/protos/client/bills/AddTendersRequest;Lcom/squareup/protos/client/bills/CaptureTendersRequest;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    check-cast v9, Lcom/squareup/queue/LocalPaymentsQueueTask;

    return-object v9

    :cond_1
    move-object v0, p0

    :goto_0
    move-object v3, p0

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    .line 64
    invoke-virtual/range {v3 .. v10}, Lcom/squareup/payment/PaymentFlowTaskProvider;->createCompleteBill(ZLjava/util/Set;Ljava/util/Date;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/queue/bills/CompleteBill;

    move-result-object v1

    .line 67
    new-instance v10, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;

    .line 68
    invoke-virtual {v1}, Lcom/squareup/queue/bills/CompleteBill;->getCompleteBillRequest()Lcom/squareup/protos/client/bills/CompleteBillRequest;

    move-result-object v3

    .line 69
    invoke-virtual {v1}, Lcom/squareup/queue/bills/CompleteBill;->getItemizations()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v1}, Lcom/squareup/queue/bills/CompleteBill;->getAdjustments()Ljava/util/List;

    move-result-object v5

    .line 70
    invoke-virtual {v1}, Lcom/squareup/queue/bills/CompleteBill;->getTenders()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v1}, Lcom/squareup/queue/bills/CompleteBill;->getTicketId()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, Lcom/squareup/queue/bills/CompleteBill;->clientId:Ljava/lang/String;

    .line 71
    invoke-direct {p0}, Lcom/squareup/payment/PaymentFlowTaskProvider;->getPaymentConfigPreviousPayments()Lcom/squareup/protos/common/Money;

    move-result-object v9

    move-object v1, v10

    move-object v2, p1

    .line 67
    invoke-direct/range {v1 .. v9}, Lcom/squareup/queue/bills/AddTendersAndCompleteBillTask;-><init>(Lcom/squareup/protos/client/bills/AddTendersRequest;Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    check-cast v10, Lcom/squareup/queue/LocalPaymentsQueueTask;

    return-object v10
.end method
