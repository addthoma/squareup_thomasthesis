.class public Lcom/squareup/payment/PaymentHudToaster;
.super Ljava/lang/Object;
.source "PaymentHudToaster.java"


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final resources:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/hudtoaster/HudToaster;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/payment/PaymentHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 27
    iput-object p2, p0, Lcom/squareup/payment/PaymentHudToaster;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 28
    iput-object p3, p0, Lcom/squareup/payment/PaymentHudToaster;->resources:Lcom/squareup/util/Res;

    .line 29
    iput-object p4, p0, Lcom/squareup/payment/PaymentHudToaster;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method static getOutOfRangeAmount(Lcom/squareup/payment/Transaction;)J
    .locals 2

    .line 33
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->paymentIsAboveMaximum()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getTransactionMaximum()J

    move-result-wide v0

    goto :goto_0

    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->getTransactionMinimum()J

    move-result-wide v0

    :goto_0
    return-wide v0
.end method

.method static getOutOfRangeMessage(Lcom/squareup/payment/Transaction;)I
    .locals 0

    .line 38
    invoke-virtual {p0}, Lcom/squareup/payment/Transaction;->paymentIsAboveMaximum()Z

    move-result p0

    if-eqz p0, :cond_0

    sget p0, Lcom/squareup/cardreader/ui/R$string;->payment_failed_above_maximum:I

    goto :goto_0

    :cond_0
    sget p0, Lcom/squareup/cardreader/ui/R$string;->payment_failed_below_minimum:I

    :goto_0
    return p0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/payment/PaymentHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    invoke-interface {v0}, Lcom/squareup/hudtoaster/HudToaster;->cancel()V

    return-void
.end method

.method public toastInvalidCard()V
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/payment/PaymentHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->INVALID_CARD:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public toastOutOfRangeGiftCard(Ljava/lang/Long;)Z
    .locals 4

    .line 61
    sget v0, Lcom/squareup/cardreader/ui/R$string;->payment_failed_gift_card_not_charged:I

    sget v1, Lcom/squareup/cardreader/ui/R$string;->payment_failed_below_minimum:I

    .line 62
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 61
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/squareup/payment/PaymentHudToaster;->toastOutOfRangeHud(IIJ)Z

    move-result p1

    return p1
.end method

.method public toastOutOfRangeHud(IIJ)Z
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/payment/PaymentHudToaster;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/payment/PaymentHudToaster;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p3, p4, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-interface {v0, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p3

    .line 67
    iget-object p4, p0, Lcom/squareup/payment/PaymentHudToaster;->resources:Lcom/squareup/util/Res;

    .line 68
    invoke-interface {p4, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    const-string p4, "amount"

    invoke-virtual {p2, p4, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 70
    iget-object p3, p0, Lcom/squareup/payment/PaymentHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object p4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_CARD:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v0, p0, Lcom/squareup/payment/PaymentHudToaster;->resources:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {p3, p4, p1, p2}, Lcom/squareup/hudtoaster/HudToaster;->toastLongHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method

.method public toastPaymentOutOfRange(Lcom/squareup/payment/Transaction;)Z
    .locals 4

    .line 56
    sget v0, Lcom/squareup/common/strings/R$string;->payment_failed_card_not_charged:I

    .line 57
    invoke-static {p1}, Lcom/squareup/payment/PaymentHudToaster;->getOutOfRangeMessage(Lcom/squareup/payment/Transaction;)I

    move-result v1

    invoke-static {p1}, Lcom/squareup/payment/PaymentHudToaster;->getOutOfRangeAmount(Lcom/squareup/payment/Transaction;)J

    move-result-wide v2

    .line 56
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/squareup/payment/PaymentHudToaster;->toastOutOfRangeHud(IIJ)Z

    move-result p1

    return p1
.end method

.method public toastSwipeStraight()V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/payment/PaymentHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method

.method public toastTryAgain()V
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/payment/PaymentHudToaster;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_TRY_AGAIN:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method
