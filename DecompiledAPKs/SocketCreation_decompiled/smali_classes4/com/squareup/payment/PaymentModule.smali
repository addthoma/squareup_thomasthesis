.class public abstract Lcom/squareup/payment/PaymentModule;
.super Ljava/lang/Object;
.source "PaymentModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/PaymentModule$Prod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCatalogDiscountBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;>;"
        }
    .end annotation

    .line 57
    new-instance v0, Lcom/squareup/payment/PaymentModule$3;

    invoke-direct {v0}, Lcom/squareup/payment/PaymentModule$3;-><init>()V

    .line 58
    invoke-virtual {v0}, Lcom/squareup/payment/PaymentModule$3;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "list-of-discounts"

    .line 57
    invoke-static {p0, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/reflect/Type;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method

.method static provideFeeListBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;>;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/payment/PaymentModule$1;

    invoke-direct {v0}, Lcom/squareup/payment/PaymentModule$1;-><init>()V

    .line 48
    invoke-virtual {v0}, Lcom/squareup/payment/PaymentModule$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "map-of-fees"

    .line 47
    invoke-static {p0, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/reflect/Type;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method

.method static provideOrderBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/payment/Order;",
            ">;"
        }
    .end annotation

    .line 39
    const-class v0, Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/squareup/payment/Order;

    invoke-static {p0, v0, v1}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method

.method static provideOrderItemKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 72
    const-class v0, Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/squareup/checkout/CartItem;

    invoke-static {p0, v0, v1}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method

.method static provideTaxRuleListBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;>;"
        }
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/payment/PaymentModule$2;

    invoke-direct {v0}, Lcom/squareup/payment/PaymentModule$2;-><init>()V

    .line 53
    invoke-virtual {v0}, Lcom/squareup/payment/PaymentModule$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-string v1, "list-of-tax-rules"

    .line 52
    invoke-static {p0, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/reflect/Type;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method

.method static provideTipSettingsBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/settings/server/TipSettings;",
            ">;"
        }
    .end annotation

    .line 43
    const-class v0, Lcom/squareup/settings/server/TipSettings;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/squareup/settings/server/TipSettings;

    invoke-static {p0, v0, v1}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract provideDanglingAuth(Lcom/squareup/payment/RealDanglingAuth;)Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;
    .annotation runtime Lcom/squareup/payment/DanglingPayment;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideDanglingMiryo(Lcom/squareup/checkoutflow/datamodels/payment/MiryoDanglingAuth;)Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;
    .annotation runtime Lcom/squareup/checkoutflow/datamodels/payment/DanglingMiryo;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideOfflineModeMonitor(Lcom/squareup/payment/RealOfflineModeMonitor;)Lcom/squareup/payment/OfflineModeMonitor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePendingTransactionsStore(Lcom/squareup/payment/pending/RealPendingTransactionsStore;)Lcom/squareup/payment/pending/PendingTransactionsStore;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideReceiptfactory(Lcom/squareup/payment/PaymentReceipt$RealFactory;)Lcom/squareup/payment/PaymentReceipt$Factory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
