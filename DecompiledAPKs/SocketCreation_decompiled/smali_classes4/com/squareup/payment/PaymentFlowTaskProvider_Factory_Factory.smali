.class public final Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;
.super Ljava/lang/Object;
.source "PaymentFlowTaskProvider_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final serverClockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;->serverClockProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ServerClock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;)Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;->serverClockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/account/ServerClock;

    iget-object v2, p0, Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v3, p0, Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;)Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/payment/PaymentFlowTaskProvider_Factory_Factory;->get()Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;

    move-result-object v0

    return-object v0
.end method
