.class public final synthetic Lcom/squareup/payment/-$$Lambda$99taOo5jtkzGPo8GffGnCUZnDwg;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/payment/Transaction;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/payment/Transaction;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/-$$Lambda$99taOo5jtkzGPo8GffGnCUZnDwg;->f$0:Lcom/squareup/payment/Transaction;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/payment/-$$Lambda$99taOo5jtkzGPo8GffGnCUZnDwg;->f$0:Lcom/squareup/payment/Transaction;

    check-cast p1, Lcom/squareup/settings/server/FeesUpdate;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Transaction;->onFeesUpdated(Lcom/squareup/settings/server/FeesUpdate;)V

    return-void
.end method
