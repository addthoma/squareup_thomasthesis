.class Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;
.super Ljava/lang/Object;
.source "RealOfflineModeMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/RealOfflineModeMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RecordAvailabilityTask"
.end annotation


# instance fields
.field private final available:Z

.field private final initiatingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

.field final synthetic this$0:Lcom/squareup/payment/RealOfflineModeMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/payment/RealOfflineModeMonitor;Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;Z)V
    .locals 0

    .line 416
    iput-object p1, p0, Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 417
    iput-object p2, p0, Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;->initiatingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    .line 418
    iput-boolean p3, p0, Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;->available:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 423
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;->initiatingTask:Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    iget-object v1, p0, Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    invoke-static {v1}, Lcom/squareup/payment/RealOfflineModeMonitor;->access$1200(Lcom/squareup/payment/RealOfflineModeMonitor;)Lcom/squareup/payment/RealOfflineModeMonitor$PingTask;

    move-result-object v1

    if-eq v0, v1, :cond_0

    return-void

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    invoke-static {v0}, Lcom/squareup/payment/RealOfflineModeMonitor;->access$1300(Lcom/squareup/payment/RealOfflineModeMonitor;)V

    .line 429
    iget-boolean v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;->available:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 430
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    invoke-static {v0, v1}, Lcom/squareup/payment/RealOfflineModeMonitor;->access$1000(Lcom/squareup/payment/RealOfflineModeMonitor;Z)V

    goto :goto_0

    .line 432
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/RealOfflineModeMonitor$RecordAvailabilityTask;->this$0:Lcom/squareup/payment/RealOfflineModeMonitor;

    invoke-static {v0, v1}, Lcom/squareup/payment/RealOfflineModeMonitor;->access$1400(Lcom/squareup/payment/RealOfflineModeMonitor;Z)V

    :goto_0
    return-void
.end method
