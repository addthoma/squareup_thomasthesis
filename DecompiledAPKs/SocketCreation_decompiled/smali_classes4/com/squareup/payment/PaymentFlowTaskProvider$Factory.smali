.class public final Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;
.super Ljava/lang/Object;
.source "PaymentFlowTaskProvider.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/PaymentFlowTaskProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u000e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;",
        "",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "serverClock",
        "Lcom/squareup/account/ServerClock;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;)V",
        "create",
        "Lcom/squareup/payment/PaymentFlowTaskProvider;",
        "paymentConfig",
        "Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final serverClock:Lcom/squareup/account/ServerClock;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "serverClock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;->serverClock:Lcom/squareup/account/ServerClock;

    iput-object p3, p0, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p4, p0, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public final create(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;)Lcom/squareup/payment/PaymentFlowTaskProvider;
    .locals 7

    const-string v0, "paymentConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    new-instance v0, Lcom/squareup/payment/PaymentFlowTaskProvider;

    .line 375
    iget-object v3, p0, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 376
    iget-object v4, p0, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;->serverClock:Lcom/squareup/account/ServerClock;

    .line 377
    iget-object v5, p0, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 378
    iget-object v6, p0, Lcom/squareup/payment/PaymentFlowTaskProvider$Factory;->features:Lcom/squareup/settings/server/Features;

    move-object v1, v0

    move-object v2, p1

    .line 373
    invoke-direct/range {v1 .. v6}, Lcom/squareup/payment/PaymentFlowTaskProvider;-><init>(Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/account/ServerClock;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method
