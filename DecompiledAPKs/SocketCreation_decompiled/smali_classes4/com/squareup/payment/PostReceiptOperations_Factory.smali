.class public final Lcom/squareup/payment/PostReceiptOperations_Factory;
.super Ljava/lang/Object;
.source "PostReceiptOperations_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/PostReceiptOperations;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final adAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialApiProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialApi;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialApi;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/payment/PostReceiptOperations_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/payment/PostReceiptOperations_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/payment/PostReceiptOperations_Factory;->tutorialApiProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/payment/PostReceiptOperations_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/payment/PostReceiptOperations_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/payment/PostReceiptOperations_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/PostReceiptOperations_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialApi;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Lcom/squareup/payment/PostReceiptOperations_Factory;"
        }
    .end annotation

    .line 54
    new-instance v7, Lcom/squareup/payment/PostReceiptOperations_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/PostReceiptOperations_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/payment/PostReceiptOperations;
    .locals 8

    .line 60
    new-instance v7, Lcom/squareup/payment/PostReceiptOperations;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/payment/PostReceiptOperations;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/payment/PostReceiptOperations;
    .locals 7

    .line 47
    iget-object v0, p0, Lcom/squareup/payment/PostReceiptOperations_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/payment/PostReceiptOperations_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/payment/PostReceiptOperations_Factory;->tutorialApiProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/register/tutorial/TutorialApi;

    iget-object v0, p0, Lcom/squareup/payment/PostReceiptOperations_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v0, p0, Lcom/squareup/payment/PostReceiptOperations_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/adanalytics/AdAnalytics;

    iget-object v0, p0, Lcom/squareup/payment/PostReceiptOperations_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static/range {v1 .. v6}, Lcom/squareup/payment/PostReceiptOperations_Factory;->newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/payment/PostReceiptOperations;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/payment/PostReceiptOperations_Factory;->get()Lcom/squareup/payment/PostReceiptOperations;

    move-result-object v0

    return-object v0
.end method
