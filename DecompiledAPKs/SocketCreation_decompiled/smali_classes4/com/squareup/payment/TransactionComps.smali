.class public final Lcom/squareup/payment/TransactionComps;
.super Ljava/lang/Object;
.source "TransactionComps.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransactionComps.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransactionComps.kt\ncom/squareup/payment/TransactionComps\n*L\n1#1,74:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0018\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010J*\u0010\u0011\u001a\u00020\u000c2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u000e2\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0010J\u0006\u0010\u0018\u001a\u00020\u000cJ\u000e\u0010\u0019\u001a\u00020\u000c2\u0006\u0010\u0012\u001a\u00020\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/payment/TransactionComps;",
        "",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "res",
        "Lcom/squareup/util/Res;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "voidCompSettings",
        "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
        "(Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)V",
        "compAllItems",
        "",
        "compReason",
        "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
        "authorizingEmployee",
        "Lcom/squareup/protos/client/Employee;",
        "compItem",
        "index",
        "",
        "compDiscount",
        "orderItem",
        "Lcom/squareup/checkout/CartItem;",
        "employee",
        "uncompAllCompedItems",
        "uncompItem",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final res:Lcom/squareup/util/Res;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/tickets/voidcomp/VoidCompSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "employeeManagement"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "voidCompSettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/payment/TransactionComps;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    iput-object p2, p0, Lcom/squareup/payment/TransactionComps;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/payment/TransactionComps;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p4, p0, Lcom/squareup/payment/TransactionComps;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    return-void
.end method


# virtual methods
.method public final compAllItems(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/client/Employee;)V
    .locals 5

    const-string v0, "compReason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/payment/TransactionComps;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 41
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->isComp()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    iget-object v0, p0, Lcom/squareup/payment/TransactionComps;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    const-string v2, "items"

    .line 43
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_1

    .line 44
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 45
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->canBeComped()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 46
    invoke-virtual {p0, v1, p1, v3, p2}, Lcom/squareup/payment/TransactionComps;->compItem(ILcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void

    .line 41
    :cond_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Attempting to comp all items with "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 40
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Comping is disabled."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final compItem(ILcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)V
    .locals 1

    const-string v0, "compDiscount"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/payment/TransactionComps;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->isComp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/squareup/payment/TransactionComps;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0, p1, p4, p2, p3}, Lcom/squareup/payment/Order;->compItem(ILcom/squareup/protos/client/Employee;Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;)V

    .line 30
    iget-object p1, p0, Lcom/squareup/payment/TransactionComps;->transaction:Lcom/squareup/payment/Transaction;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Transaction;->postItemsChanged(Z)V

    return-void

    .line 28
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Attempting to comp all items with "

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2

    .line 27
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Comping is disabled."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final uncompAllCompedItems()V
    .locals 4

    .line 65
    iget-object v0, p0, Lcom/squareup/payment/TransactionComps;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getItems()Ljava/util/List;

    move-result-object v0

    const-string v1, "items"

    .line 66
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 67
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 68
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->canBeUncomped()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 69
    invoke-virtual {p0, v2}, Lcom/squareup/payment/TransactionComps;->uncompItem(I)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final uncompItem(I)V
    .locals 3

    .line 55
    iget-object v0, p0, Lcom/squareup/payment/TransactionComps;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/squareup/payment/TransactionComps;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/squareup/payment/TransactionComps;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/TransactionComps;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v2}, Lcom/squareup/permissions/EmployeeInfo;->createEmployeeProto(Lcom/squareup/util/Res;)Lcom/squareup/protos/client/Employee;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/squareup/payment/Order;->uncompItem(ILcom/squareup/protos/client/Employee;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/payment/TransactionComps;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/payment/Transaction;->postItemsChanged(Z)V

    return-void

    .line 55
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Uncomping is disabled."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
