.class public Lcom/squareup/payment/TipDeterminerFactory;
.super Ljava/lang/Object;
.source "TipDeterminerFactory.java"


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/payment/Transaction;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/payment/TipDeterminerFactory;->features:Lcom/squareup/settings/server/Features;

    .line 24
    iput-object p2, p0, Lcom/squareup/payment/TipDeterminerFactory;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 25
    iput-object p3, p0, Lcom/squareup/payment/TipDeterminerFactory;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 26
    iput-object p4, p0, Lcom/squareup/payment/TipDeterminerFactory;->transaction:Lcom/squareup/payment/Transaction;

    return-void
.end method

.method private createFor(Lcom/squareup/payment/TenderInEdit;)Lcom/squareup/payment/TipDeterminer;
    .locals 2

    .line 48
    invoke-interface {p1}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    const-string v1, "TipDeterminerFactory::createFor tenderInEdit is not editing tender"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 50
    invoke-interface {p1}, Lcom/squareup/payment/TenderInEdit;->requireBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/payment/TipDeterminerFactory;->createFor(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/TipDeterminer;

    move-result-object p1

    return-object p1
.end method

.method private createFor(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/payment/TipDeterminer;
    .locals 4

    .line 55
    instance-of v0, p1, Lcom/squareup/payment/AcceptsTips;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 56
    move-object v0, p1

    check-cast v0, Lcom/squareup/payment/AcceptsTips;

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->askForTip()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 60
    :goto_0
    instance-of v2, p1, Lcom/squareup/payment/tender/MagStripeTender;

    if-eqz v2, :cond_1

    move-object v2, p1

    check-cast v2, Lcom/squareup/payment/tender/MagStripeTender;

    .line 61
    invoke-virtual {v2}, Lcom/squareup/payment/tender/MagStripeTender;->getCard()Lcom/squareup/Card;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v2

    sget-object v3, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-eq v2, v3, :cond_1

    const/4 v1, 0x1

    .line 64
    :cond_1
    instance-of v2, p1, Lcom/squareup/payment/tender/SmartCardTender;

    if-eqz v2, :cond_2

    .line 65
    check-cast p1, Lcom/squareup/payment/tender/SmartCardTender;

    .line 66
    invoke-virtual {p1}, Lcom/squareup/payment/tender/SmartCardTender;->isContactless()Z

    move-result p1

    if-nez p1, :cond_2

    .line 67
    iget-object p1, p0, Lcom/squareup/payment/TipDeterminerFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->startedWithChargeButton()Z

    move-result v1

    .line 71
    :cond_2
    invoke-direct {p0, v0, v1}, Lcom/squareup/payment/TipDeterminerFactory;->createTipDeterminer(ZZ)Lcom/squareup/payment/TipDeterminer;

    move-result-object p1

    return-object p1
.end method

.method private createTipDeterminer(ZZ)Lcom/squareup/payment/TipDeterminer;
    .locals 9

    .line 77
    iget-object v0, p0, Lcom/squareup/payment/TipDeterminerFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v7

    .line 78
    iget-object v0, p0, Lcom/squareup/payment/TipDeterminerFactory;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureSettings;->shouldPrintReceiptToSign()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/TipDeterminerFactory;->transaction:Lcom/squareup/payment/Transaction;

    .line 79
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    const/4 v8, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 81
    :goto_0
    new-instance v0, Lcom/squareup/payment/TipDeterminer;

    iget-object v1, p0, Lcom/squareup/payment/TipDeterminerFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_PREFERRED:Lcom/squareup/settings/server/Features$Feature;

    .line 82
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    iget-object v1, p0, Lcom/squareup/payment/TipDeterminerFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    .line 83
    invoke-interface {v1, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    iget-object v1, p0, Lcom/squareup/payment/TipDeterminerFactory;->transaction:Lcom/squareup/payment/Transaction;

    .line 84
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->isUsingSeparateTippingScreen()Z

    move-result v4

    move-object v1, v0

    move v5, p1

    move v6, p2

    invoke-direct/range {v1 .. v8}, Lcom/squareup/payment/TipDeterminer;-><init>(ZZZZZZZ)V

    return-object v0
.end method


# virtual methods
.method public createFor(Lcom/squareup/payment/tender/BaseTender$Builder;)Lcom/squareup/payment/TipDeterminer;
    .locals 0

    .line 44
    invoke-virtual {p1}, Lcom/squareup/payment/tender/BaseTender$Builder;->build()Lcom/squareup/payment/tender/BaseTender;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/payment/TipDeterminerFactory;->createFor(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/payment/TipDeterminer;

    move-result-object p1

    return-object p1
.end method

.method public createForCurrentTender()Lcom/squareup/payment/TipDeterminer;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/payment/TipDeterminerFactory;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/squareup/payment/TipDeterminerFactory;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-direct {p0, v0}, Lcom/squareup/payment/TipDeterminerFactory;->createFor(Lcom/squareup/payment/TenderInEdit;)Lcom/squareup/payment/TipDeterminer;

    move-result-object v0

    return-object v0

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/TipDeterminerFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasBillPayment()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/payment/TipDeterminerFactory;->transaction:Lcom/squareup/payment/Transaction;

    .line 35
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasLastAddedTender()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    iget-object v0, p0, Lcom/squareup/payment/TipDeterminerFactory;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->requireLastAddedTender()Lcom/squareup/payment/tender/BaseTender;

    move-result-object v0

    .line 37
    invoke-direct {p0, v0}, Lcom/squareup/payment/TipDeterminerFactory;->createFor(Lcom/squareup/payment/tender/BaseTender;)Lcom/squareup/payment/TipDeterminer;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, v0, v0}, Lcom/squareup/payment/TipDeterminerFactory;->createTipDeterminer(ZZ)Lcom/squareup/payment/TipDeterminer;

    move-result-object v0

    return-object v0
.end method

.method public usePreAuthTipping()Z
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/payment/TipDeterminerFactory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_TIP_BEFORE_AUTH_REQUIRED:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
