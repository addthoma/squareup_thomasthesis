.class public Lcom/squareup/payment/AutoVoid;
.super Ljava/lang/Object;
.source "AutoVoid.java"


# instance fields
.field private final autoVoidNotifier:Lcom/squareup/notifications/AutoVoidNotifier;

.field private final danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

.field private final danglingMiryo:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/log/OhSnapLogger;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;Lcom/squareup/notifications/AutoVoidNotifier;Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;)V
    .locals 0
    .param p2    # Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;
        .annotation runtime Lcom/squareup/payment/DanglingPayment;
        .end annotation
    .end param
    .param p4    # Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;
        .annotation runtime Lcom/squareup/checkoutflow/datamodels/payment/DanglingMiryo;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/payment/AutoVoid;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 25
    iput-object p2, p0, Lcom/squareup/payment/AutoVoid;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    .line 26
    iput-object p3, p0, Lcom/squareup/payment/AutoVoid;->autoVoidNotifier:Lcom/squareup/notifications/AutoVoidNotifier;

    .line 27
    iput-object p4, p0, Lcom/squareup/payment/AutoVoid;->danglingMiryo:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    return-void
.end method


# virtual methods
.method public reportVoidOnTimeout(Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/payment/AutoVoid;->autoVoidNotifier:Lcom/squareup/notifications/AutoVoidNotifier;

    invoke-interface {v0, p1}, Lcom/squareup/notifications/AutoVoidNotifier;->notifyVoidOnTimeout(Lcom/squareup/protos/common/Money;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/payment/AutoVoid;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->AUTO_VOID:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v1, "timeout"

    invoke-interface {p1, v0, v1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method

.method public voidDanglingAuthAfterCrash(Ljava/lang/String;)V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/payment/AutoVoid;->danglingAuth:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 38
    invoke-interface {v0, v1, p1}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->voidLastAuth(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 41
    iget-object v0, p0, Lcom/squareup/payment/AutoVoid;->autoVoidNotifier:Lcom/squareup/notifications/AutoVoidNotifier;

    invoke-interface {v0, p1}, Lcom/squareup/notifications/AutoVoidNotifier;->notifyDanglingAuthVoidedAfterCrash(Lcom/squareup/protos/common/Money;)V

    .line 43
    :cond_0
    iget-object p1, p0, Lcom/squareup/payment/AutoVoid;->danglingMiryo:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    invoke-interface {p1}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->hasDanglingAuth()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 44
    iget-object p1, p0, Lcom/squareup/payment/AutoVoid;->autoVoidNotifier:Lcom/squareup/notifications/AutoVoidNotifier;

    invoke-interface {p1}, Lcom/squareup/notifications/AutoVoidNotifier;->notifyDanglingMiryo()V

    .line 45
    iget-object p1, p0, Lcom/squareup/payment/AutoVoid;->danglingMiryo:Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;

    invoke-interface {p1}, Lcom/squareup/checkoutflow/datamodels/payment/DanglingAuth;->clearLastAuth()V

    :cond_1
    return-void
.end method
