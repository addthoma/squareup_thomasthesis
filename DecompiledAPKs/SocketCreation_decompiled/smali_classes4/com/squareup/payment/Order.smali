.class public Lcom/squareup/payment/Order;
.super Ljava/lang/Object;
.source "Order.java"

# interfaces
.implements Lcom/squareup/payment/crm/HoldsCustomer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/Order$SplitResult;,
        Lcom/squareup/payment/Order$SensitiveValues;,
        Lcom/squareup/payment/Order$Builder;
    }
.end annotation


# instance fields
.field private final addedCouponsAndCartScopeDiscountEventsById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;"
        }
    .end annotation
.end field

.field private final addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end field

.field private transient adjuster:Lcom/squareup/calc/Adjuster;

.field private appointmentsDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

.field private final availableTaxRules:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;"
        }
    .end annotation
.end field

.field protected final availableTaxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private final availableTaxesById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private transient billPrintState:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

.field private final blacklistedDiscounts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private cart:Lcom/squareup/checkout/Cart$Builder;

.field private final coursingHandler:Lcom/squareup/payment/OrderCoursingHandler;

.field private customerContact:Lcom/squareup/protos/client/rolodex/Contact;

.field private customerInstruments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation
.end field

.field private customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

.field private final deletedTaxItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final deletedTaxSurcharges:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final deletedTaxesById:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end field

.field private diningOption:Lcom/squareup/checkout/DiningOption;

.field private discountApplicationIdEnabled:Z

.field private employeeFirstName:Ljava/lang/String;

.field private employeeLastName:Ljava/lang/String;

.field private employeeToken:Ljava/lang/String;

.field private invoiceFeatureDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

.field private transient onCustomerChanged:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private openTicketName:Ljava/lang/String;

.field private openTicketNote:Ljava/lang/String;

.field private transient orderAdjustments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;"
        }
    .end annotation
.end field

.field private orderTicketName:Ljava/lang/String;

.field private predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

.field private productsOpenTicketOpenedOn:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$OpenTicketProductAssociation;",
            ">;"
        }
    .end annotation
.end field

.field private returnCart:Lcom/squareup/checkout/ReturnCart;

.field protected roundingType:Lcom/squareup/calc/constants/RoundingType;

.field private final seatingHandler:Lcom/squareup/payment/OrderSeatingHandler;

.field private transient sensitiveValues:Lcom/squareup/payment/Order$SensitiveValues;

.field private final surcharges:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Surcharge;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketIdPair:Lcom/squareup/protos/client/IdPair;

.field private timestampForReprint:Ljava/util/Date;

.field private final userId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/Order$Builder;)V
    .locals 2

    .line 602
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 523
    iput-boolean v0, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    .line 532
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    .line 541
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountEventsById:Ljava/util/Map;

    .line 547
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->blacklistedDiscounts:Ljava/util/Set;

    .line 554
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->availableTaxesById:Ljava/util/Map;

    .line 558
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    .line 561
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxItems:Ljava/util/Map;

    .line 564
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxSurcharges:Ljava/util/Map;

    .line 603
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$2300(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->userId:Ljava/lang/String;

    .line 604
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$2400(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->ticketIdPair:Lcom/squareup/protos/client/IdPair;

    .line 605
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$2500(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->employeeToken:Ljava/lang/String;

    .line 606
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$2600(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->employeeFirstName:Ljava/lang/String;

    .line 607
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$2700(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->employeeLastName:Ljava/lang/String;

    .line 608
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$2800(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/calc/constants/RoundingType;

    move-result-object v0

    const-string v1, "roundingType"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/calc/constants/RoundingType;

    iput-object v0, p0, Lcom/squareup/payment/Order;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    .line 610
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$2900(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->orderTicketName:Ljava/lang/String;

    .line 611
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$3000(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->openTicketName:Ljava/lang/String;

    .line 612
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$3100(Lcom/squareup/payment/Order$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->openTicketNote:Ljava/lang/String;

    .line 613
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$3200(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    .line 614
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$3300(Lcom/squareup/payment/Order$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->productsOpenTicketOpenedOn:Ljava/util/List;

    .line 616
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$3400(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    .line 618
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$3500(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 619
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$3600(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 620
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$3700(Lcom/squareup/payment/Order$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->customerInstruments:Ljava/util/List;

    .line 621
    new-instance v0, Lcom/squareup/payment/OrderSeatingHandler;

    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$3800(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderSeatingHandler;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;)V

    iput-object v0, p0, Lcom/squareup/payment/Order;->seatingHandler:Lcom/squareup/payment/OrderSeatingHandler;

    .line 622
    new-instance v0, Lcom/squareup/payment/OrderCoursingHandler;

    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$3900(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/payment/OrderCoursingHandler;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;)V

    iput-object v0, p0, Lcom/squareup/payment/Order;->coursingHandler:Lcom/squareup/payment/OrderCoursingHandler;

    .line 624
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$4000(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->appointmentsDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    .line 626
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$4100(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/checkout/Cart$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    .line 627
    iget-object v0, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$4200(Lcom/squareup/payment/Order$Builder;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 628
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$4300(Lcom/squareup/payment/Order$Builder;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    .line 629
    iget-object v0, p0, Lcom/squareup/payment/Order;->blacklistedDiscounts:Ljava/util/Set;

    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$4400(Lcom/squareup/payment/Order$Builder;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 633
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$4500(Lcom/squareup/payment/Order$Builder;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->availableTaxes:Ljava/util/List;

    .line 634
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$4600(Lcom/squareup/payment/Order$Builder;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->availableTaxRules:Ljava/util/List;

    .line 635
    iget-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$4700(Lcom/squareup/payment/Order$Builder;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 636
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$4800(Lcom/squareup/payment/Order$Builder;)Lcom/squareup/checkout/ReturnCart;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->returnCart:Lcom/squareup/checkout/ReturnCart;

    .line 637
    invoke-static {p1}, Lcom/squareup/payment/Order$Builder;->access$4900(Lcom/squareup/payment/Order$Builder;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    .line 639
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->init()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/Order$Builder;Lcom/squareup/payment/Order$1;)V
    .locals 0

    .line 164
    invoke-direct {p0, p1}, Lcom/squareup/payment/Order;-><init>(Lcom/squareup/payment/Order$Builder;)V

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/payment/Order;)V
    .locals 2

    .line 646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 523
    iput-boolean v0, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    .line 532
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    .line 541
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountEventsById:Ljava/util/Map;

    .line 547
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->blacklistedDiscounts:Ljava/util/Set;

    .line 554
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->availableTaxesById:Ljava/util/Map;

    .line 558
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    .line 561
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxItems:Ljava/util/Map;

    .line 564
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxSurcharges:Ljava/util/Map;

    .line 647
    iget-object v0, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    .line 648
    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 649
    iget-object v0, p1, Lcom/squareup/payment/Order;->availableTaxes:Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/payment/Order;->availableTaxes:Ljava/util/List;

    .line 650
    iget-object v0, p0, Lcom/squareup/payment/Order;->availableTaxesById:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/payment/Order;->availableTaxesById:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 651
    iget-object v0, p0, Lcom/squareup/payment/Order;->blacklistedDiscounts:Ljava/util/Set;

    iget-object v1, p1, Lcom/squareup/payment/Order;->blacklistedDiscounts:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 652
    iget-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 653
    iget-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxItems:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/payment/Order;->deletedTaxItems:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 654
    iget-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxSurcharges:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/payment/Order;->deletedTaxSurcharges:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 655
    iget-object v0, p1, Lcom/squareup/payment/Order;->availableTaxRules:Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/payment/Order;->availableTaxRules:Ljava/util/List;

    .line 656
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCart()Lcom/squareup/checkout/Cart;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart;->toBuilder()Lcom/squareup/checkout/Cart$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    .line 657
    iget-object v0, p1, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    iput-object v0, p0, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    .line 658
    iget-object v0, p1, Lcom/squareup/payment/Order;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    iput-object v0, p0, Lcom/squareup/payment/Order;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 659
    iget-object v0, p1, Lcom/squareup/payment/Order;->customerInstruments:Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/payment/Order;->customerInstruments:Ljava/util/List;

    .line 660
    iget-object v0, p1, Lcom/squareup/payment/Order;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    iput-object v0, p0, Lcom/squareup/payment/Order;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 661
    iget-object v0, p1, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    iput-object v0, p0, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    .line 662
    iget-object v0, p1, Lcom/squareup/payment/Order;->employeeToken:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/payment/Order;->employeeToken:Ljava/lang/String;

    .line 663
    iget-object v0, p1, Lcom/squareup/payment/Order;->employeeFirstName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/payment/Order;->employeeFirstName:Ljava/lang/String;

    .line 664
    iget-object v0, p1, Lcom/squareup/payment/Order;->employeeLastName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/payment/Order;->employeeLastName:Ljava/lang/String;

    .line 665
    iget-object v0, p1, Lcom/squareup/payment/Order;->openTicketName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/payment/Order;->openTicketName:Ljava/lang/String;

    .line 666
    iget-object v0, p1, Lcom/squareup/payment/Order;->openTicketNote:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/payment/Order;->openTicketNote:Ljava/lang/String;

    .line 667
    iget-object v0, p1, Lcom/squareup/payment/Order;->predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    iput-object v0, p0, Lcom/squareup/payment/Order;->predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    .line 668
    iget-object v0, p1, Lcom/squareup/payment/Order;->productsOpenTicketOpenedOn:Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/payment/Order;->productsOpenTicketOpenedOn:Ljava/util/List;

    .line 669
    iget-object v0, p1, Lcom/squareup/payment/Order;->orderTicketName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/payment/Order;->orderTicketName:Ljava/lang/String;

    .line 670
    iget-object v0, p1, Lcom/squareup/payment/Order;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    iput-object v0, p0, Lcom/squareup/payment/Order;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    .line 671
    iget-object v0, p1, Lcom/squareup/payment/Order;->sensitiveValues:Lcom/squareup/payment/Order$SensitiveValues;

    invoke-virtual {v0}, Lcom/squareup/payment/Order$SensitiveValues;->copy()Lcom/squareup/payment/Order$SensitiveValues;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->sensitiveValues:Lcom/squareup/payment/Order$SensitiveValues;

    .line 672
    iget-object v0, p1, Lcom/squareup/payment/Order;->ticketIdPair:Lcom/squareup/protos/client/IdPair;

    iput-object v0, p0, Lcom/squareup/payment/Order;->ticketIdPair:Lcom/squareup/protos/client/IdPair;

    .line 673
    iget-object v0, p1, Lcom/squareup/payment/Order;->userId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/payment/Order;->userId:Ljava/lang/String;

    .line 674
    iget-object v0, p1, Lcom/squareup/payment/Order;->invoiceFeatureDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    iput-object v0, p0, Lcom/squareup/payment/Order;->invoiceFeatureDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    .line 675
    iget-object v0, p1, Lcom/squareup/payment/Order;->appointmentsDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    iput-object v0, p0, Lcom/squareup/payment/Order;->appointmentsDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    .line 676
    iget-object v0, p1, Lcom/squareup/payment/Order;->timestampForReprint:Ljava/util/Date;

    iput-object v0, p0, Lcom/squareup/payment/Order;->timestampForReprint:Ljava/util/Date;

    .line 677
    iget-object v0, p1, Lcom/squareup/payment/Order;->seatingHandler:Lcom/squareup/payment/OrderSeatingHandler;

    iput-object v0, p0, Lcom/squareup/payment/Order;->seatingHandler:Lcom/squareup/payment/OrderSeatingHandler;

    .line 678
    iget-object v0, p1, Lcom/squareup/payment/Order;->coursingHandler:Lcom/squareup/payment/OrderCoursingHandler;

    iput-object v0, p0, Lcom/squareup/payment/Order;->coursingHandler:Lcom/squareup/payment/OrderCoursingHandler;

    .line 679
    iget-object v0, p1, Lcom/squareup/payment/Order;->returnCart:Lcom/squareup/checkout/ReturnCart;

    iput-object v0, p0, Lcom/squareup/payment/Order;->returnCart:Lcom/squareup/checkout/ReturnCart;

    .line 680
    iget-boolean p1, p1, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    iput-boolean p1, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Ljava/util/List;Lcom/squareup/protos/client/IdPair;Lcom/squareup/checkout/DiningOption;Ljava/util/Map;Lcom/squareup/checkout/ReturnCart;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/checkout/DiningOption;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Surcharge;",
            ">;",
            "Lcom/squareup/checkout/ReturnCart;",
            ")V"
        }
    .end annotation

    .line 688
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 523
    iput-boolean v0, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    .line 532
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    .line 541
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountEventsById:Ljava/util/Map;

    .line 547
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->blacklistedDiscounts:Ljava/util/Set;

    .line 554
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->availableTaxesById:Ljava/util/Map;

    .line 558
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    .line 561
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxItems:Ljava/util/Map;

    .line 564
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxSurcharges:Ljava/util/Map;

    .line 689
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->availableTaxes:Ljava/util/List;

    .line 690
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->availableTaxRules:Ljava/util/List;

    .line 691
    new-instance v0, Lcom/squareup/checkout/Cart$Builder;

    invoke-direct {v0}, Lcom/squareup/checkout/Cart$Builder;-><init>()V

    .line 692
    invoke-virtual {v0, p2}, Lcom/squareup/checkout/Cart$Builder;->addItems(Ljava/lang/Iterable;)Lcom/squareup/checkout/Cart$Builder;

    move-result-object p2

    .line 693
    invoke-virtual {p2, p1}, Lcom/squareup/checkout/Cart$Builder;->currencyCode(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/checkout/Cart$Builder;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    .line 694
    iput-object p4, p0, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    .line 695
    iput-object p5, p0, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    const/4 p1, 0x0

    .line 696
    iput-object p1, p0, Lcom/squareup/payment/Order;->employeeToken:Ljava/lang/String;

    .line 697
    iput-object p1, p0, Lcom/squareup/payment/Order;->employeeFirstName:Ljava/lang/String;

    .line 698
    iput-object p1, p0, Lcom/squareup/payment/Order;->employeeLastName:Ljava/lang/String;

    .line 699
    iput-object p1, p0, Lcom/squareup/payment/Order;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    .line 700
    iput-object p3, p0, Lcom/squareup/payment/Order;->ticketIdPair:Lcom/squareup/protos/client/IdPair;

    .line 701
    iput-object p1, p0, Lcom/squareup/payment/Order;->userId:Ljava/lang/String;

    .line 702
    iput-object p6, p0, Lcom/squareup/payment/Order;->returnCart:Lcom/squareup/checkout/ReturnCart;

    .line 703
    new-instance p2, Lcom/squareup/payment/OrderSeatingHandler;

    invoke-direct {p2, p1}, Lcom/squareup/payment/OrderSeatingHandler;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;)V

    iput-object p2, p0, Lcom/squareup/payment/Order;->seatingHandler:Lcom/squareup/payment/OrderSeatingHandler;

    .line 704
    new-instance p2, Lcom/squareup/payment/OrderCoursingHandler;

    invoke-direct {p2, p1}, Lcom/squareup/payment/OrderCoursingHandler;-><init>(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;)V

    iput-object p2, p0, Lcom/squareup/payment/Order;->coursingHandler:Lcom/squareup/payment/OrderCoursingHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/payment/Order;)Z
    .locals 0

    .line 164
    iget-boolean p0, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    return p0
.end method

.method static synthetic access$100(Lcom/squareup/payment/Order;)Lcom/squareup/checkout/ReturnCart;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->returnCart:Lcom/squareup/checkout/ReturnCart;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/payment/Order;)Ljava/lang/String;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->openTicketName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/payment/Order;)Ljava/lang/String;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->orderTicketName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1200(Lcom/squareup/payment/Order;)Ljava/util/Map;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/payment/Order;)Ljava/util/Set;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->blacklistedDiscounts:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/squareup/payment/Order;)Ljava/util/List;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->availableTaxRules:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$1500(Lcom/squareup/payment/Order;)Ljava/util/Map;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->availableTaxesById:Ljava/util/Map;

    return-object p0
.end method

.method static synthetic access$1600(Lcom/squareup/payment/Order;)Ljava/lang/String;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->employeeLastName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1700(Lcom/squareup/payment/Order;)Ljava/lang/String;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->employeeFirstName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1800(Lcom/squareup/payment/Order;)Ljava/lang/String;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->employeeToken:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1900(Lcom/squareup/payment/Order;)Lcom/squareup/checkout/Cart$Builder;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->appointmentsDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    return-object p0
.end method

.method static synthetic access$2000(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/IdPair;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->ticketIdPair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method static synthetic access$2100(Lcom/squareup/payment/Order;)Ljava/lang/String;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->userId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/payment/Order;)Ljava/util/List;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->customerInstruments:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/payment/Order;)Lcom/squareup/checkout/DiningOption;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/payment/Order;)Ljava/util/List;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->productsOpenTicketOpenedOn:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/payment/Order;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/payment/Order;)Ljava/lang/String;
    .locals 0

    .line 164
    iget-object p0, p0, Lcom/squareup/payment/Order;->openTicketNote:Ljava/lang/String;

    return-object p0
.end method

.method private static applyDiscountDelta(Lcom/squareup/checkout/CartItem;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;Z)Lcom/squareup/checkout/CartItem;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount$Scope;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;",
            "Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;",
            "Z)",
            "Lcom/squareup/checkout/CartItem;"
        }
    .end annotation

    .line 1423
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 1424
    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->pricingRuleAppliedDiscounts:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1425
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1427
    invoke-virtual {p3}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->incrementRemoved()Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;

    .line 1428
    invoke-virtual {v0, v3}, Lcom/squareup/checkout/CartItem$Builder;->removeAppliedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    const/4 v2, 0x1

    goto :goto_0

    .line 1431
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1432
    iget-object v5, p0, Lcom/squareup/checkout/CartItem;->pricingRuleAppliedDiscounts:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1434
    invoke-virtual {p3}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->incrementAdded()Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;

    .line 1435
    invoke-interface {p2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogDiscount;

    .line 1440
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/Discount$Scope;

    .line 1441
    invoke-static {v2, v3, p4}, Lcom/squareup/checkout/Discounts;->toAppliedDiscount(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/Discount$Scope;Z)Lcom/squareup/checkout/Discount;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/checkout/CartItem$Builder;->addPricingEngineAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    if-eqz v2, :cond_4

    .line 1445
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p0

    :cond_4
    return-object p0
.end method

.method private buildCartDiningOption()Lcom/squareup/protos/client/bills/DiningOptionLineItem;
    .locals 2

    .line 2583
    iget-object v0, p0, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2587
    :cond_0
    sget-object v1, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/DiningOption;->copy(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/checkout/DiningOption;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/DiningOption;->buildDiningOptionLineItem()Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    move-result-object v0

    return-object v0
.end method

.method private buildItemizations(ZZ)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation

    .line 2684
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2687
    iget-object v1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v1, p2}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView(Z)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 2689
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->hasDiningOption()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    if-eqz v3, :cond_1

    .line 2690
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    if-eqz p1, :cond_0

    sget-object v5, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL_FIXED:Lcom/squareup/protos/client/bills/ApplicationScope;

    goto :goto_1

    :cond_0
    sget-object v5, Lcom/squareup/protos/client/bills/ApplicationScope;->CART_LEVEL:Lcom/squareup/protos/client/bills/ApplicationScope;

    .line 2691
    :goto_1
    invoke-virtual {v4, v5}, Lcom/squareup/checkout/DiningOption;->copy(Lcom/squareup/protos/client/bills/ApplicationScope;)Lcom/squareup/checkout/DiningOption;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/checkout/CartItem$Builder;->selectedDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    .line 2692
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v3

    goto :goto_2

    :cond_1
    move-object v3, v2

    .line 2694
    :goto_2
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isService()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2695
    iget-object v4, v3, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->appointments_service_details:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    .line 2696
    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object v4

    .line 2697
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;

    move-result-object v4

    .line 2698
    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;

    move-result-object v4

    .line 2700
    iget-object v5, v3, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    .line 2701
    invoke-virtual {v5}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    move-result-object v5

    .line 2702
    invoke-virtual {v5, v4}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->appointments_service_details(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$AppointmentsServiceDetails;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    move-result-object v4

    .line 2703
    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    move-result-object v4

    .line 2705
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    iget-object v5, p0, Lcom/squareup/payment/Order;->seatingHandler:Lcom/squareup/payment/OrderSeatingHandler;

    .line 2706
    invoke-virtual {v5}, Lcom/squareup/payment/OrderSeatingHandler;->getAllSeats()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/squareup/checkout/CartItem$Builder;->featureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    .line 2707
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v3

    add-int/lit8 v1, v1, 0x1

    .line 2711
    :cond_2
    invoke-virtual {p0, v2}, Lcom/squareup/payment/Order;->getAdjustedItemFor(Lcom/squareup/checkout/CartItem;)Lcom/squareup/calc/AdjustedItem;

    move-result-object v2

    iget-boolean v4, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    invoke-virtual {v3, v2, v4}, Lcom/squareup/checkout/CartItem;->getItemizationProto(Lcom/squareup/calc/AdjustedItem;Z)Lcom/squareup/protos/client/bills/Itemization;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method private buildTopLevelDiscountLineItems()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/DiscountLineItem;",
            ">;"
        }
    .end annotation

    .line 2748
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2749
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/calc/Adjuster;->getAppliedDiscounts()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/calc/order/Adjustment;

    .line 2750
    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 2751
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForDiscount(Lcom/squareup/calc/order/Adjustment;)J

    move-result-wide v3

    .line 2752
    iget-object v5, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v5}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 2753
    iget-boolean v4, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    invoke-virtual {v2, v3, v4}, Lcom/squareup/checkout/Discount;->buildDiscountLineItem(Lcom/squareup/protos/common/Money;Z)Lcom/squareup/protos/client/bills/DiscountLineItem;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2757
    :cond_0
    iget-boolean v1, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    if-eqz v1, :cond_1

    .line 2758
    invoke-static {v0}, Lcom/squareup/checkout/Discounts;->toMergedDiscountLineItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private buildVoidedItemizations()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Itemization;",
            ">;"
        }
    .end annotation

    .line 2721
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2722
    iget-object v1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v1}, Lcom/squareup/checkout/Cart$Builder;->voidedItemsView()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 2723
    invoke-virtual {p0, v2}, Lcom/squareup/payment/Order;->getAdjustedItemFor(Lcom/squareup/checkout/CartItem;)Lcom/squareup/calc/AdjustedItem;

    move-result-object v3

    iget-boolean v4, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    invoke-virtual {v2, v3, v4}, Lcom/squareup/checkout/CartItem;->getItemizationProto(Lcom/squareup/calc/AdjustedItem;Z)Lcom/squareup/protos/client/bills/Itemization;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private getAddedCartScopeCouponDiscounts()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 2838
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2839
    iget-object v1, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 2840
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->isCoupon()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2841
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2844
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private getItem(Ljava/lang/String;)Lcom/squareup/checkout/CartItem;
    .locals 3

    .line 1105
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    .line 1106
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 1107
    iget-object v2, v1, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private getItemIndex(Ljava/lang/String;)I
    .locals 3

    .line 1092
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 1094
    iget-object v2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v2}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 1095
    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method private getNotVoidedItemsByStatus(Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 1148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1149
    iget-object v1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v1}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 1150
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, v2, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-ne v3, p1, :cond_0

    .line 1151
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1154
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private getRoundingAdjustment(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;
    .locals 5

    if-eqz p1, :cond_1

    .line 2591
    iget-object v0, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    goto :goto_0

    .line 2594
    :cond_0
    new-instance v0, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;-><init>()V

    sget-object v1, Lcom/squareup/api/items/CalculationPhase;->SWEDISH_ROUNDING_PHASE:Lcom/squareup/api/items/CalculationPhase;

    .line 2595
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts$Builder;-><init>()V

    .line 2597
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts$Builder;

    move-result-object p1

    .line 2598
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    move-result-object p1

    .line 2596
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 2600
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v0

    .line 2601
    invoke-virtual {v0}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    .line 2599
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->rounding_adjustment_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    move-result-object p1

    .line 2602
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->build()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method private getSplitParents()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1573
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    .line 1574
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getDeletedItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 1575
    iget-object v3, v2, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Itemization$Event;

    .line 1576
    iget-object v5, v4, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    sget-object v6, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->SPLIT:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    if-ne v5, v6, :cond_1

    .line 1577
    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Event;->split_event_details:Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;

    .line 1578
    iget-object v4, v4, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails;->child_itemization:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;

    .line 1579
    iget-object v5, v5, Lcom/squareup/protos/client/bills/Itemization$Event$SplitEventDetails$ChildItemization;->child_itemization_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v5, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v6, v2, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v6, v6, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private hasDiscounts(Z)Z
    .locals 5

    .line 1894
    iget-object v0, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lez v0, :cond_0

    return v1

    .line 1896
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 1897
    iget-object v3, v2, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    if-eqz p1, :cond_2

    if-lez v3, :cond_2

    return v1

    .line 1900
    :cond_2
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v4

    if-eqz v4, :cond_3

    if-le v3, v1, :cond_3

    return v1

    .line 1902
    :cond_3
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v2

    if-nez v2, :cond_1

    if-lez v3, :cond_1

    return v1

    :cond_4
    const/4 p1, 0x0

    return p1
.end method

.method private hasExactlyOneDiscount(Z)Z
    .locals 6

    .line 1844
    iget-object v0, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-le v0, v2, :cond_0

    return v1

    .line 1846
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v3, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1849
    iget-object v3, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v3}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/CartItem;

    if-nez p1, :cond_2

    .line 1850
    invoke-virtual {v4}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v5

    if-eqz v5, :cond_2

    goto :goto_0

    .line 1851
    :cond_2
    iget-object v4, v4, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/Discount;

    .line 1852
    iget-object v5, v5, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1853
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v5

    if-le v5, v2, :cond_3

    return v1

    .line 1858
    :cond_4
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result p1

    if-ne p1, v2, :cond_5

    const/4 v1, 0x1

    :cond_5
    return v1
.end method

.method private hasSharedAncestor(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .line 1593
    :goto_0
    invoke-interface {p1, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1594
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    goto :goto_0

    .line 1596
    :cond_0
    :goto_1
    invoke-interface {p1, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1597
    invoke-interface {p1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    goto :goto_1

    .line 1599
    :cond_1
    invoke-virtual {p2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method static synthetic lambda$addDiscount$0(Lcom/squareup/checkout/Discount;Lcom/squareup/checkout/Discount;Lcom/squareup/protos/client/bills/Itemization$Event;Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 1713
    invoke-virtual {p3}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/squareup/checkout/Discount;->isCoupon()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {p3}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    .line 1717
    :cond_0
    invoke-virtual {p3}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->addAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p0

    if-eqz p2, :cond_1

    .line 1719
    invoke-virtual {p0, p2}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    .line 1721
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p0

    return-object p0

    :cond_2
    :goto_0
    return-object p3
.end method

.method static synthetic lambda$removeNonRecalledFromTicketDiscounts$1(Ljava/util/Set;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItem;
    .locals 3

    .line 1782
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1785
    iget-object v2, p2, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Discount;

    if-eqz v2, :cond_0

    .line 1786
    iget-boolean v2, v2, Lcom/squareup/checkout/Discount;->recalledFromTicket:Z

    if-nez v2, :cond_0

    if-nez v0, :cond_1

    .line 1789
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 1791
    :cond_1
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->removeAppliedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    const/4 v1, 0x1

    .line 1792
    invoke-virtual {p1, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    :cond_2
    if-nez v0, :cond_3

    goto :goto_1

    .line 1796
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p2

    :goto_1
    return-object p2
.end method

.method private mergeItems(II)V
    .locals 5

    .line 1609
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 1610
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 1611
    iget-object v2, v0, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    iget-object v3, v1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1612
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->hasTicket()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1613
    iget-boolean v3, v0, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    const/4 v4, 0x0

    if-eqz v3, :cond_0

    .line 1614
    iget-object v3, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v3, v0, v4}, Lcom/squareup/checkout/Cart$Builder;->rememberDeletedItem(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)Lcom/squareup/checkout/Cart$Builder;

    .line 1616
    :cond_0
    iget-boolean v3, v1, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-eqz v3, :cond_1

    .line 1617
    iget-object v3, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v3, v1, v4}, Lcom/squareup/checkout/Cart$Builder;->rememberDeletedItem(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)Lcom/squareup/checkout/Cart$Builder;

    .line 1620
    :cond_1
    invoke-static {v0, v2}, Lcom/squareup/payment/Order;->splitItem(Lcom/squareup/checkout/CartItem;Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem;

    move-result-object v0

    .line 1621
    invoke-virtual {p0, p1, v0}, Lcom/squareup/payment/Order;->replaceItem(ILcom/squareup/checkout/CartItem;)V

    .line 1622
    invoke-virtual {p0, p2}, Lcom/squareup/payment/Order;->removeItem(I)Lcom/squareup/checkout/CartItem;

    return-void
.end method

.method private reapplyDiscountsThatCanBeAppliedToOnlyOneItem()V
    .locals 4

    .line 2849
    iget-object v0, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 2850
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2851
    iget-boolean v3, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    if-eqz v3, :cond_1

    .line 2855
    iget-object v1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-static {v2, p0, v1}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher;->applyItemBasedCouponGreedily(Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;)V

    const/4 v1, 0x1

    goto :goto_0

    .line 2858
    :cond_1
    new-instance v3, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;

    invoke-direct {v3, v2}, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;-><init>(Lcom/squareup/checkout/Discount;)V

    .line 2859
    iget-object v2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v2, v3}, Lcom/squareup/checkout/Cart$Builder;->scanItems(Lrx/functions/Action2;)Lcom/squareup/checkout/Cart$Builder;

    .line 2860
    iget-object v2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v3, p0, v2}, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->apply(Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;)V

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    .line 2865
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->invalidate()V

    :cond_3
    return-void
.end method

.method private removeDiscountFromAllItemsInternal(Ljava/lang/String;ZLcom/squareup/protos/client/Employee;)Z
    .locals 9

    .line 2872
    iget-object v0, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/Discount;

    .line 2873
    iget-object v1, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountEventsById:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2876
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    if-eqz v0, :cond_0

    .line 2878
    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2881
    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2884
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    new-instance v8, Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;

    move-object v2, v8

    move-object v3, p0

    move-object v4, p1

    move-object v5, v1

    move-object v6, p3

    move v7, p2

    invoke-direct/range {v2 .. v7}, Lcom/squareup/payment/-$$Lambda$Order$ZaFnrz3Z83I3BMDRw-KlWph2sY8;-><init>(Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/protos/client/Employee;Z)V

    invoke-virtual {v0, v8}, Lcom/squareup/checkout/Cart$Builder;->replaceItems(Lcom/squareup/checkout/CartItem$Transform;)Lcom/squareup/checkout/Cart$Builder;

    .line 2906
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 2907
    invoke-direct {p0}, Lcom/squareup/payment/Order;->reapplyDiscountsThatCanBeAppliedToOnlyOneItem()V

    .line 2910
    :cond_1
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result p1

    return p1
.end method

.method private removeTaxesFromAllItems(Ljava/util/Map;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Adjustment;",
            ">;)Z"
        }
    .end annotation

    .line 903
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 904
    new-instance v3, Ljava/util/LinkedHashMap;

    iget-object v4, v2, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-direct {v3, v4}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 905
    new-instance v4, Ljava/util/LinkedHashSet;

    iget-object v5, v2, Lcom/squareup/checkout/CartItem;->userEditedTaxIds:Ljava/util/Set;

    invoke-direct {v4, v5}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    .line 907
    iget-object v5, v2, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/checkout/Tax;

    .line 908
    iget-object v7, v6, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {p1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 909
    iget-object v7, v6, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v3, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 910
    iget-object v7, v6, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v4, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 913
    iget-object v7, p0, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    iget-object v8, v6, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v7, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 915
    iget-object v7, p0, Lcom/squareup/payment/Order;->deletedTaxItems:Ljava/util/Map;

    iget-object v8, v6, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    if-nez v7, :cond_2

    .line 917
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 919
    :cond_2
    iget-object v8, v2, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v8, v8, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 921
    iget-object v8, p0, Lcom/squareup/payment/Order;->deletedTaxItems:Ljava/util/Map;

    iget-object v6, v6, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v8, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 925
    :cond_3
    iget-object v5, v2, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v3, v5}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v1, 0x1

    .line 928
    iget-object v5, v2, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v5, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/squareup/payment/Order;->getItemIndex(Ljava/lang/String;)I

    move-result v5

    .line 929
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/squareup/checkout/CartItem$Builder;->userEditedTaxIds(Ljava/util/Set;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v2

    .line 928
    invoke-virtual {p0, v5, v2}, Lcom/squareup/payment/Order;->replaceItem(ILcom/squareup/checkout/CartItem;)V

    goto/16 :goto_0

    :cond_4
    return v1
.end method

.method private removeTaxesFromAllSurcharges(Ljava/util/Map;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Adjustment;",
            ">;)Z"
        }
    .end annotation

    .line 943
    iget-object v0, p0, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Surcharge;

    .line 944
    new-instance v3, Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Lcom/squareup/checkout/Surcharge;->appliedTaxes()Ljava/util/Map;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 946
    invoke-virtual {v2}, Lcom/squareup/checkout/Surcharge;->appliedTaxes()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/calc/order/Adjustment;

    .line 947
    invoke-interface {v5}, Lcom/squareup/calc/order/Adjustment;->id()Ljava/lang/String;

    move-result-object v6

    .line 949
    invoke-interface {p1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 950
    invoke-interface {v3, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 953
    iget-object v7, p0, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    check-cast v5, Lcom/squareup/checkout/Tax;

    invoke-interface {v7, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 955
    iget-object v5, p0, Lcom/squareup/payment/Order;->deletedTaxSurcharges:Ljava/util/Map;

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    if-nez v5, :cond_2

    .line 957
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 959
    :cond_2
    invoke-virtual {v2}, Lcom/squareup/checkout/Surcharge;->id()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 961
    iget-object v7, p0, Lcom/squareup/payment/Order;->deletedTaxSurcharges:Ljava/util/Map;

    invoke-interface {v7, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 965
    :cond_3
    invoke-virtual {v2}, Lcom/squareup/checkout/Surcharge;->appliedTaxes()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x1

    .line 968
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/checkout/Surcharge;->withoutTaxes(Ljava/util/Set;)Lcom/squareup/checkout/Surcharge;

    move-result-object v3

    .line 969
    iget-object v4, p0, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/squareup/checkout/Surcharge;->id()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    return v1
.end method

.method public static splitItem(Lcom/squareup/checkout/CartItem;Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem;
    .locals 4

    .line 2664
    iget-object v0, p0, Lcom/squareup/checkout/CartItem;->featureDetails:Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    if-eqz v0, :cond_0

    .line 2666
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->inherited_itemization_details(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$InheritedItemizationDetails;)Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;

    move-result-object v0

    .line 2669
    :cond_0
    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->destination:Lcom/squareup/checkout/OrderDestination;

    invoke-virtual {v1}, Lcom/squareup/checkout/OrderDestination;->getSeats()Ljava/util/List;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 2670
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p0

    new-instance v2, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    .line 2671
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/squareup/checkout/CartItem$Builder;->idPair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p0

    .line 2672
    invoke-virtual {p0, p1}, Lcom/squareup/checkout/CartItem$Builder;->quantity(Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p0

    .line 2673
    invoke-virtual {p0, v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->featureDetails(Lcom/squareup/protos/client/bills/Itemization$FeatureDetails;Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p0

    .line 2674
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public addCoupon(Lcom/squareup/protos/client/coupons/Coupon;Z)V
    .locals 2

    .line 1819
    new-instance v0, Lcom/squareup/checkout/Discount$Builder;

    invoke-direct {v0, p1, p2}, Lcom/squareup/checkout/Discount$Builder;-><init>(Lcom/squareup/protos/client/coupons/Coupon;Z)V

    invoke-virtual {v0}, Lcom/squareup/checkout/Discount$Builder;->build()Lcom/squareup/checkout/Discount;

    move-result-object p1

    .line 1820
    iget-object p2, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    iget-object v0, p1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 1822
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->getCouponToken()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 1825
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Order;->addDiscount(Lcom/squareup/checkout/Discount;)V

    .line 1826
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->invalidate()V

    return-void

    .line 1823
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Coupon does not have a coupon token: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 1821
    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Coupon already added to order: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public addDiscount(Lcom/squareup/checkout/Discount;)V
    .locals 1

    const/4 v0, 0x0

    .line 1677
    invoke-virtual {p0, p1, v0}, Lcom/squareup/payment/Order;->addDiscount(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/client/Employee;)V

    return-void
.end method

.method public addDiscount(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/client/Employee;)V
    .locals 4

    .line 1690
    new-instance v0, Lcom/squareup/checkout/Discount$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/checkout/Discount$Builder;-><init>(Lcom/squareup/checkout/Discount;)V

    .line 1691
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/checkout/Discount$Scope;->ITEMIZATION:Lcom/squareup/checkout/Discount$Scope;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/checkout/Discount$Scope;->CART:Lcom/squareup/checkout/Discount$Scope;

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Discount$Builder;->scope(Lcom/squareup/checkout/Discount$Scope;)Lcom/squareup/checkout/Discount$Builder;

    move-result-object v0

    .line 1692
    invoke-virtual {v0}, Lcom/squareup/checkout/Discount$Builder;->build()Lcom/squareup/checkout/Discount;

    move-result-object v0

    if-eqz p2, :cond_1

    .line 1695
    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/squareup/checkout/util/ItemizationEvents;->discountAddEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 1697
    :goto_1
    iget-object v2, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    iget-object v3, v0, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_2

    .line 1699
    iget-object v2, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountEventsById:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1703
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1704
    iget-boolean p1, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    if-eqz p1, :cond_3

    .line 1705
    iget-object p1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-static {v0, p0, p1, p2}, Lcom/squareup/payment/transform/GreedyItemBasedCouponMatcher;->applyItemBasedCouponGreedily(Lcom/squareup/checkout/Discount;Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;)V

    goto :goto_2

    .line 1707
    :cond_3
    new-instance p1, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;

    invoke-direct {p1, v0}, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;-><init>(Lcom/squareup/checkout/Discount;)V

    .line 1708
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/Cart$Builder;->scanItems(Lrx/functions/Action2;)Lcom/squareup/checkout/Cart$Builder;

    .line 1709
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {p1, p0, v0, p2}, Lcom/squareup/payment/transform/OnlyOneItemDiscountMatcher;->apply(Lcom/squareup/payment/Order;Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/protos/client/Employee;)V

    goto :goto_2

    .line 1712
    :cond_4
    iget-object p2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    new-instance v2, Lcom/squareup/payment/-$$Lambda$Order$9KuBtrFx51bQ9IJeEvoZKlybnzE;

    invoke-direct {v2, p1, v0, v1}, Lcom/squareup/payment/-$$Lambda$Order$9KuBtrFx51bQ9IJeEvoZKlybnzE;-><init>(Lcom/squareup/checkout/Discount;Lcom/squareup/checkout/Discount;Lcom/squareup/protos/client/bills/Itemization$Event;)V

    invoke-virtual {p2, v2}, Lcom/squareup/checkout/Cart$Builder;->replaceItems(Lcom/squareup/checkout/CartItem$Transform;)Lcom/squareup/checkout/Cart$Builder;

    :goto_2
    return-void
.end method

.method public addSurcharge(Lcom/squareup/checkout/Surcharge;)V
    .locals 2

    .line 1055
    iget-object v0, p0, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/checkout/Surcharge;->id()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 1056
    iget-object v0, p0, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/checkout/Surcharge;->id()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public allItemTaxesDisabled()Z
    .locals 3

    .line 2200
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 2201
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public applyPricingRuleBlocks(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ZZ)Lcom/squareup/payment/ApplyAutomaticDiscountsResult;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogDiscount;",
            ">;",
            "Ljava/util/Map<",
            "Lcom/squareup/shared/pricing/models/ClientServerIds;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;",
            ">;>;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;",
            ">;ZZ)",
            "Lcom/squareup/payment/ApplyAutomaticDiscountsResult;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p4

    .line 1473
    new-instance v4, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;

    invoke-direct {v4}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;-><init>()V

    .line 1481
    iget-object v5, v0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v5}, Lcom/squareup/checkout/Cart$Builder;->itemsListIterator()Ljava/util/ListIterator;

    move-result-object v5

    .line 1482
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1483
    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/checkout/CartItem;

    .line 1485
    new-instance v7, Lcom/squareup/shared/pricing/models/ClientServerIds;

    iget-object v8, v6, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v8, v8, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v9, v6, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v9, v9, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-direct {v7, v8, v9}, Lcom/squareup/shared/pricing/models/ClientServerIds;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1489
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    if-eqz p5, :cond_2

    .line 1493
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;

    .line 1495
    invoke-virtual {v10}, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;->getExclusions()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    goto :goto_1

    .line 1498
    :cond_1
    invoke-virtual {v10}, Lcom/squareup/shared/pricing/engine/rules/ApplicationWholeBlock;->getDiscountId()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Lcom/squareup/checkout/Discount$Scope;->ITEMIZATION:Lcom/squareup/checkout/Discount$Scope;

    invoke-interface {v8, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1502
    :cond_2
    invoke-interface {v2, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 1503
    invoke-static {v6, v8, v1, v4, v3}, Lcom/squareup/payment/Order;->applyDiscountDelta(Lcom/squareup/checkout/CartItem;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;Z)Lcom/squareup/checkout/CartItem;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 1508
    :cond_3
    invoke-interface {v2, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    if-eqz v9, :cond_8

    .line 1509
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_8

    .line 1515
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x1

    sub-int/2addr v10, v11

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;

    .line 1516
    invoke-virtual {v10}, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v12

    invoke-virtual {v10}, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;->getOffset()Ljava/math/BigDecimal;

    move-result-object v10

    invoke-virtual {v12, v10}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v10

    .line 1517
    invoke-virtual {v6}, Lcom/squareup/checkout/CartItem;->quantity()Ljava/math/BigDecimal;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v10

    if-gez v10, :cond_4

    goto :goto_2

    :cond_4
    const/4 v11, 0x0

    .line 1519
    :goto_2
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;

    .line 1520
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12, v8}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 1521
    invoke-virtual {v10}, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;->getDiscountIds()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 1522
    sget-object v15, Lcom/squareup/checkout/Discount$Scope;->ITEMIZATION_PER_QUANTITY:Lcom/squareup/checkout/Discount$Scope;

    invoke-interface {v12, v14, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 1525
    :cond_5
    invoke-virtual {v6}, Lcom/squareup/checkout/CartItem;->quantity()Ljava/math/BigDecimal;

    move-result-object v13

    invoke-virtual {v10}, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v13

    if-lez v13, :cond_6

    .line 1530
    invoke-virtual {v10}, Lcom/squareup/shared/pricing/engine/rules/ApplicationBlock;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v10

    const/4 v13, 0x0

    invoke-virtual {v0, v6, v13, v10}, Lcom/squareup/payment/Order;->splitItemInPlace(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;Ljava/math/BigDecimal;)Lcom/squareup/payment/Order$SplitResult;

    move-result-object v6

    .line 1531
    iget-object v10, v6, Lcom/squareup/payment/Order$SplitResult;->first:Lcom/squareup/checkout/CartItem;

    invoke-static {v10, v12, v1, v4, v3}, Lcom/squareup/payment/Order;->applyDiscountDelta(Lcom/squareup/checkout/CartItem;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;Z)Lcom/squareup/checkout/CartItem;

    move-result-object v10

    .line 1533
    invoke-virtual {v4}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->incrementSplit()Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;

    .line 1534
    invoke-virtual {v7}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getClientId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v4, v12, v10}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->addReplacement(Ljava/lang/String;Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;

    .line 1536
    invoke-interface {v5, v10}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 1537
    iget-object v10, v6, Lcom/squareup/payment/Order$SplitResult;->second:Lcom/squareup/checkout/CartItem;

    invoke-interface {v5, v10}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 1538
    iget-object v10, v0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    iget-object v6, v6, Lcom/squareup/payment/Order$SplitResult;->parent:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v10, v6, v13}, Lcom/squareup/checkout/Cart$Builder;->rememberDeletedItem(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)Lcom/squareup/checkout/Cart$Builder;

    .line 1545
    invoke-interface {v5}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 1546
    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/checkout/CartItem;

    goto :goto_3

    .line 1551
    :cond_6
    invoke-static {v6, v12, v1, v4, v3}, Lcom/squareup/payment/Order;->applyDiscountDelta(Lcom/squareup/checkout/CartItem;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;Z)Lcom/squareup/checkout/CartItem;

    move-result-object v10

    invoke-interface {v5, v10}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto :goto_3

    :cond_7
    if-eqz v11, :cond_0

    .line 1557
    invoke-static {v6, v8, v1, v4, v3}, Lcom/squareup/payment/Order;->applyDiscountDelta(Lcom/squareup/checkout/CartItem;Ljava/util/Map;Ljava/util/Map;Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;Z)Lcom/squareup/checkout/CartItem;

    move-result-object v6

    .line 1559
    invoke-interface {v5, v6}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 1560
    invoke-virtual {v7}, Lcom/squareup/shared/pricing/models/ClientServerIds;->getClientId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7, v6}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->addReplacement(Ljava/lang/String;Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;

    goto/16 :goto_0

    .line 1510
    :cond_8
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "blocks cannot be empty if present for an itemization"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1564
    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/squareup/payment/Order;->reapplyDiscountsThatCanBeAppliedToOnlyOneItem()V

    .line 1565
    invoke-virtual {v4}, Lcom/squareup/payment/ApplyAutomaticDiscountsResult$Builder;->build()Lcom/squareup/payment/ApplyAutomaticDiscountsResult;

    move-result-object v1

    return-object v1
.end method

.method public buildTopLevelFeeLineItems()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/FeeLineItem;",
            ">;"
        }
    .end annotation

    .line 2730
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2731
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/calc/Adjuster;->getAppliedTaxes()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/calc/order/Adjustment;

    .line 2732
    check-cast v2, Lcom/squareup/checkout/Tax;

    .line 2733
    iget-object v3, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v3}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v3

    .line 2735
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForTax(Lcom/squareup/calc/order/Adjustment;)J

    move-result-wide v4

    invoke-static {v4, v5, v3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 2736
    invoke-static {p0, v2}, Lcom/squareup/payment/TaxCalculations;->calculateAppliedToAmount(Lcom/squareup/payment/Order;Lcom/squareup/checkout/Tax;)J

    move-result-wide v5

    invoke-static {v5, v6, v3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 2738
    invoke-static {p0, v2}, Lcom/squareup/payment/TaxCalculations;->calculateAfterApplicationItemizationsTotalAmount(Lcom/squareup/payment/Order;Lcom/squareup/checkout/Tax;)J

    move-result-wide v6

    invoke-static {v6, v7, v3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 2740
    invoke-virtual {v2, v4, v5, v3}, Lcom/squareup/checkout/Tax;->buildFeeLineItem(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/FeeLineItem;

    move-result-object v2

    .line 2742
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public buildTopLevelSurchargeLineItems()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/SurchargeLineItem;",
            ">;"
        }
    .end annotation

    .line 2765
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2766
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v1

    .line 2767
    iget-object v2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v2}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    .line 2769
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getSurcharges()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/Surcharge;

    .line 2770
    invoke-virtual {v1, v4}, Lcom/squareup/calc/Adjuster;->getSurchargeAdjustedItemFor(Lcom/squareup/calc/order/Surcharge;)Lcom/squareup/calc/AdjustedItem;

    move-result-object v5

    .line 2771
    invoke-virtual {v5}, Lcom/squareup/calc/AdjustedItem;->getTotalCollectedPerAdjustment()Ljava/util/Map;

    move-result-object v6

    .line 2772
    new-instance v7, Ljava/util/LinkedHashMap;

    invoke-direct {v7}, Ljava/util/LinkedHashMap;-><init>()V

    .line 2773
    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    .line 2774
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v10, v11, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v8

    invoke-interface {v7, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2777
    :cond_0
    invoke-virtual {v5}, Lcom/squareup/calc/AdjustedItem;->getBaseAmount()J

    move-result-wide v5

    invoke-static {v5, v6, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v5

    .line 2776
    invoke-virtual {v4, v5, v7}, Lcom/squareup/checkout/Surcharge;->toSurchargeLineItem(Lcom/squareup/protos/common/Money;Ljava/util/Map;)Lcom/squareup/protos/client/bills/SurchargeLineItem;

    move-result-object v4

    .line 2779
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public catalogDiscountMayApplyAtCartScope(Ljava/lang/String;)Z
    .locals 4

    .line 2127
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    .line 2133
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 2134
    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-static {v2, p1}, Lcom/squareup/checkout/Discounts;->getCatalogId(Ljava/util/Map;Ljava/lang/String;)Lcom/squareup/checkout/Discount;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2136
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->getScope()Lcom/squareup/checkout/Discount$Scope;

    move-result-object v3

    iget-boolean v3, v3, Lcom/squareup/checkout/Discount$Scope;->atItemScope:Z

    if-eqz v3, :cond_1

    .line 2137
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->isAmountDiscount()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 p1, 0x0

    return p1

    .line 2142
    :cond_2
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Order;->hasDiscountAppliedToAllItems(Ljava/lang/String;)Z

    move-result p1

    xor-int/2addr p1, v1

    return p1
.end method

.method public coalesceAdjacentSplitItems()V
    .locals 7

    .line 1641
    invoke-direct {p0}, Lcom/squareup/payment/Order;->getSplitParents()Ljava/util/Map;

    move-result-object v0

    .line 1642
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    :goto_0
    if-ltz v1, :cond_1

    .line 1643
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 1644
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v3

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 1645
    iget-object v5, v2, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v5, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v6, v3, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v6, v6, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-direct {p0, v0, v5, v6}, Lcom/squareup/payment/Order;->hasSharedAncestor(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1646
    invoke-virtual {v2, v3}, Lcom/squareup/checkout/CartItem;->canCoalesce(Lcom/squareup/checkout/CartItem;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1647
    invoke-direct {p0, v1, v4}, Lcom/squareup/payment/Order;->mergeItems(II)V

    .line 1648
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, v2, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public coalesceNewestUnsavedItems()V
    .locals 5

    .line 1660
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    :goto_0
    if-ltz v0, :cond_1

    .line 1661
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 1662
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 1665
    iget-boolean v4, v1, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-nez v4, :cond_1

    iget-boolean v4, v2, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-nez v4, :cond_1

    invoke-virtual {v1, v2}, Lcom/squareup/checkout/CartItem;->canCoalesce(Lcom/squareup/checkout/CartItem;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 1669
    :cond_0
    invoke-direct {p0, v0, v3}, Lcom/squareup/payment/Order;->mergeItems(II)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method

.method public compItem(ILcom/squareup/protos/client/Employee;Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/checkout/CartItem;)V
    .locals 3

    .line 1190
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 1191
    iget-object v1, p4, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/IdPair;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Comped item must match item in cart."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1194
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0, p1, p4}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;

    .line 1196
    invoke-virtual {p4}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "Cannot comp an already comped item."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1197
    invoke-virtual {p4}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "Cannot comp a voided item."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1198
    invoke-virtual {p3}, Lcom/squareup/shared/catalog/models/CatalogDiscount;->isComp()Z

    move-result v0

    const-string v1, "Requires discount to be a COMP"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1200
    new-instance v0, Lcom/squareup/checkout/Discount$Builder;

    invoke-direct {v0, p3}, Lcom/squareup/checkout/Discount$Builder;-><init>(Lcom/squareup/shared/catalog/models/CatalogDiscount;)V

    sget-object p3, Lcom/squareup/checkout/Discount$Scope;->ITEMIZATION:Lcom/squareup/checkout/Discount$Scope;

    .line 1201
    invoke-virtual {v0, p3}, Lcom/squareup/checkout/Discount$Builder;->scope(Lcom/squareup/checkout/Discount$Scope;)Lcom/squareup/checkout/Discount$Builder;

    move-result-object p3

    .line 1202
    invoke-virtual {p3}, Lcom/squareup/checkout/Discount$Builder;->build()Lcom/squareup/checkout/Discount;

    move-result-object p3

    .line 1205
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1206
    iget-object p4, p4, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {p4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p4

    invoke-interface {p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :cond_0
    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 1207
    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->isCoupon()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1208
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1209
    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1210
    iget-object v2, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    iget-object v1, v1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1216
    :cond_1
    iget-object p4, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {p4, p1, p2, p3, v0}, Lcom/squareup/checkout/Cart$Builder;->compItem(ILcom/squareup/protos/client/Employee;Lcom/squareup/checkout/Discount;Ljava/util/List;)Lcom/squareup/checkout/Cart$Builder;

    return-void
.end method

.method public createTaxRuleAppliedInTransactionEvent()Lcom/squareup/log/cart/TaxRuleAppliedInTransactionEvent;
    .locals 15

    .line 2799
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2803
    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 2806
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->hasDiningOption()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_1

    const/4 v5, 0x0

    goto/16 :goto_2

    .line 2810
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/checkout/CartItem;

    .line 2812
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/squareup/checkout/CartItem;->getDiningOption(Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/DiningOption;

    move-result-object v7

    .line 2813
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v9, v5

    const/4 v5, 0x0

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/squareup/payment/OrderTaxRule;

    .line 2814
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAvailableTaxesAsList()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_3
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/squareup/checkout/Tax;

    .line 2815
    iget-object v13, v6, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/squareup/checkout/CartItem;->isCustomItem()Z

    move-result v14

    invoke-virtual {v10, v13, v14, v12, v7}, Lcom/squareup/payment/OrderTaxRule;->doesTaxRuleApply(Ljava/lang/String;ZLcom/squareup/checkout/Tax;Lcom/squareup/checkout/DiningOption;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 2816
    invoke-interface {v0, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2818
    iget-object v5, v6, Lcom/squareup/checkout/CartItem;->userEditedTaxIds:Ljava/util/Set;

    iget-object v12, v12, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v5, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    const/4 v9, 0x1

    goto :goto_1

    :cond_4
    const/4 v5, 0x1

    goto :goto_1

    :cond_5
    if-nez v5, :cond_6

    const/4 v4, 0x0

    :cond_6
    move v5, v9

    goto :goto_0

    :cond_7
    move v2, v4

    .line 2829
    :goto_2
    new-instance v1, Lcom/squareup/log/cart/TaxRuleAppliedInTransactionEvent;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v1, v0, v2, v5}, Lcom/squareup/log/cart/TaxRuleAppliedInTransactionEvent;-><init>(IZZ)V

    return-object v1
.end method

.method public getAddedCouponsAndCartScopeDiscountEvents()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Itemization$Event;",
            ">;"
        }
    .end annotation

    .line 1051
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountEventsById:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 1047
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public getAdditionalTaxAmountFor(Lcom/squareup/calc/order/Adjustment;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 2338
    invoke-interface {p1}, Lcom/squareup/calc/order/Adjustment;->inclusionType()Lcom/squareup/calc/constants/InclusionType;

    move-result-object v0

    sget-object v1, Lcom/squareup/calc/constants/InclusionType;->ADDITIVE:Lcom/squareup/calc/constants/InclusionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 2339
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForTax(Lcom/squareup/calc/order/Adjustment;)J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {p1}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public getAdditionalTaxes()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 2343
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdditionalTaxesAmount()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v2}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAdditionalTaxesAmount()J
    .locals 2

    .line 2330
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForAdditiveTaxes()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAdditionalTaxesAndAmounts()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/checkout/Tax;",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 2317
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2318
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/calc/Adjuster;->getAppliedTaxes()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/calc/order/Adjustment;

    .line 2319
    invoke-interface {v2}, Lcom/squareup/calc/order/Adjustment;->inclusionType()Lcom/squareup/calc/constants/InclusionType;

    move-result-object v3

    sget-object v4, Lcom/squareup/calc/constants/InclusionType;->ADDITIVE:Lcom/squareup/calc/constants/InclusionType;

    if-ne v3, v4, :cond_0

    .line 2321
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForTax(Lcom/squareup/calc/order/Adjustment;)J

    move-result-wide v3

    iget-object v5, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v5}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 2322
    check-cast v2, Lcom/squareup/checkout/Tax;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getAdditiveTaxOrderAdjustments()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;"
        }
    .end annotation

    .line 2627
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2628
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/calc/Adjuster;->getAppliedTaxes()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/calc/order/Adjustment;

    .line 2629
    check-cast v2, Lcom/squareup/checkout/Tax;

    .line 2630
    iget-object v3, v2, Lcom/squareup/checkout/Tax;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v4, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne v3, v4, :cond_0

    .line 2632
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForTax(Lcom/squareup/calc/order/Adjustment;)J

    move-result-wide v3

    iget-object v5, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v5}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 2633
    invoke-static {v2, v3}, Lcom/squareup/payment/OrderAdjustment;->fromTax(Lcom/squareup/checkout/Tax;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/OrderAdjustment;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getAdjustedItemFor(Lcom/squareup/checkout/CartItem;)Lcom/squareup/calc/AdjustedItem;
    .locals 1

    .line 2356
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/calc/Adjuster;->getAdjustedItemFor(Lcom/squareup/calc/order/Item;)Lcom/squareup/calc/AdjustedItem;

    move-result-object p1

    return-object p1
.end method

.method public getAdjustedTotalForItem(Lcom/squareup/checkout/CartItem;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 2366
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Order;->getAdjustedItemFor(Lcom/squareup/checkout/CartItem;)Lcom/squareup/calc/AdjustedItem;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/calc/AdjustedItem;->getAdjustedTotal()J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {p1}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method protected getAdjuster()Lcom/squareup/calc/Adjuster;
    .locals 4

    .line 767
    iget-object v0, p0, Lcom/squareup/payment/Order;->adjuster:Lcom/squareup/calc/Adjuster;

    if-nez v0, :cond_0

    .line 768
    new-instance v0, Lcom/squareup/calc/TaxAndDiscountAdjuster;

    iget-object v1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v1}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView()Ljava/util/List;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    .line 769
    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v3, p0, Lcom/squareup/payment/Order;->roundingType:Lcom/squareup/calc/constants/RoundingType;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/calc/TaxAndDiscountAdjuster;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/calc/constants/RoundingType;)V

    iput-object v0, p0, Lcom/squareup/payment/Order;->adjuster:Lcom/squareup/calc/Adjuster;

    .line 770
    iget-object v0, p0, Lcom/squareup/payment/Order;->adjuster:Lcom/squareup/calc/Adjuster;

    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 772
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Order;->adjuster:Lcom/squareup/calc/Adjuster;

    return-object v0
.end method

.method public getAdjustmentAmountsByIdForItem(Lcom/squareup/checkout/CartItem;)Ljava/util/Map;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 776
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Order;->getAdjustedItemFor(Lcom/squareup/checkout/CartItem;)Lcom/squareup/calc/AdjustedItem;

    move-result-object p1

    .line 777
    invoke-virtual {p1}, Lcom/squareup/calc/AdjustedItem;->getTotalCollectedPerAdjustment()Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public getAllAppliedDiscounts()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 1950
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1951
    iget-object v1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v1}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 1952
    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getAllAppliedDiscountsInDisplayOrder()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 1943
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAllAppliedDiscounts()Ljava/util/Map;

    move-result-object v0

    .line 1944
    invoke-static {v0}, Lcom/squareup/checkout/Adjustment;->sortToDisplayOrder(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 1945
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getAllComps()Lcom/squareup/protos/common/Money;
    .locals 8

    .line 2291
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    .line 2292
    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->getCollectedAmountPerDiscount()Ljava/util/Map;

    move-result-object v1

    .line 2293
    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->getAppliedDiscounts()Ljava/util/Map;

    move-result-object v0

    .line 2295
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const-wide/16 v2, 0x0

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 2296
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/calc/order/Adjustment;

    .line 2297
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/math/BigDecimal;

    invoke-virtual {v4}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    if-eqz v5, :cond_0

    .line 2298
    check-cast v5, Lcom/squareup/checkout/Discount;

    invoke-virtual {v5}, Lcom/squareup/checkout/Discount;->isComp()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2299
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    add-long/2addr v2, v4

    goto :goto_0

    .line 2303
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAllDiscountsAmount()J
    .locals 2

    .line 2253
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForAllDiscounts()J

    move-result-wide v0

    neg-long v0, v0

    return-wide v0
.end method

.method public getAllSurchargesAmount()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 1064
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForAllSurcharges()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    .line 1065
    invoke-virtual {v2}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    .line 1064
    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAllTaxes()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 2312
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAllTaxesAmount()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v2}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAllTaxesAmount()J
    .locals 2

    .line 2308
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForAllTaxes()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAmountDue()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 1381
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAmountDueLong()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v2}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method protected getAmountDueLong()J
    .locals 4

    .line 1398
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->hasReturn()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1399
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getReturnCart()Lcom/squareup/checkout/ReturnCart;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/ReturnCart;->getTotalReturnAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    .line 1401
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/calc/Adjuster;->getTotal()J

    move-result-wide v2

    sub-long/2addr v2, v0

    return-wide v2
.end method

.method public getAmountForDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 2642
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForDiscount(Lcom/squareup/calc/order/Adjustment;)J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    .line 2643
    invoke-virtual {p1}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    .line 2641
    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public getAmounts(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts;
    .locals 1

    .line 2785
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;-><init>()V

    .line 2786
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->total_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p1

    .line 2787
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAllTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tax_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p1

    .line 2788
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getNegativeAllDiscounts()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->discount_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p1

    .line 2789
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAllSurchargesAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->surcharge_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p1

    .line 2790
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;

    move-result-object p1

    .line 2791
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object p1

    return-object p1
.end method

.method public getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;
    .locals 1

    .line 1074
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getSurcharges()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/checkout/Surcharge;->getAutoGratuity(Ljava/util/List;)Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object v0

    return-object v0
.end method

.method public getAutoGratuityAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 1078
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAutoGratuity()Lcom/squareup/checkout/Surcharge$AutoGratuity;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/payment/Order;->getSurchargeBaseAmount(Lcom/squareup/checkout/Surcharge;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getAvailableTaxRules()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;"
        }
    .end annotation

    .line 1027
    iget-object v0, p0, Lcom/squareup/payment/Order;->availableTaxRules:Ljava/util/List;

    return-object v0
.end method

.method public getAvailableTaxes(Ljava/lang/String;)Lcom/squareup/checkout/Tax;
    .locals 3

    .line 2192
    iget-object v0, p0, Lcom/squareup/payment/Order;->availableTaxes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Tax;

    .line 2193
    iget-object v2, v1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getAvailableTaxes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 882
    iget-object v0, p0, Lcom/squareup/payment/Order;->availableTaxesById:Ljava/util/Map;

    return-object v0
.end method

.method public getAvailableTaxesAsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 878
    iget-object v0, p0, Lcom/squareup/payment/Order;->availableTaxes:Ljava/util/List;

    return-object v0
.end method

.method public getBlacklistedDiscounts()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 550
    iget-object v0, p0, Lcom/squareup/payment/Order;->blacklistedDiscounts:Ljava/util/Set;

    return-object v0
.end method

.method public getCart()Lcom/squareup/checkout/Cart;
    .locals 1

    .line 781
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->build()Lcom/squareup/checkout/Cart;

    move-result-object v0

    return-object v0
.end method

.method public getCartProto(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/protos/client/bills/Cart;
    .locals 1

    .line 2453
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;-><init>()V

    .line 2454
    invoke-direct {p0, p4, p5}, Lcom/squareup/payment/Order;->buildItemizations(ZZ)Ljava/util/List;

    move-result-object p4

    invoke-virtual {v0, p4}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object p4

    .line 2455
    invoke-direct {p0}, Lcom/squareup/payment/Order;->buildVoidedItemizations()Ljava/util/List;

    move-result-object p5

    invoke-virtual {p4, p5}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->void_itemization(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object p4

    .line 2456
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->buildTopLevelFeeLineItems()Ljava/util/List;

    move-result-object p5

    invoke-virtual {p4, p5}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->fee(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object p4

    .line 2457
    invoke-direct {p0}, Lcom/squareup/payment/Order;->buildTopLevelDiscountLineItems()Ljava/util/List;

    move-result-object p5

    invoke-virtual {p4, p5}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->discount(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object p4

    .line 2458
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->buildTopLevelSurchargeLineItems()Ljava/util/List;

    move-result-object p5

    invoke-virtual {p4, p5}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->surcharge(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object p4

    .line 2459
    invoke-direct {p0, p3}, Lcom/squareup/payment/Order;->getRoundingAdjustment(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    move-result-object p3

    invoke-virtual {p4, p3}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->rounding_adjustment(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object p3

    .line 2460
    invoke-direct {p0}, Lcom/squareup/payment/Order;->buildCartDiningOption()Lcom/squareup/protos/client/bills/DiningOptionLineItem;

    move-result-object p4

    invoke-virtual {p3, p4}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->default_dining_option(Lcom/squareup/protos/client/bills/DiningOptionLineItem;)Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;

    move-result-object p3

    .line 2462
    new-instance p4, Lcom/squareup/protos/client/bills/Cart$Builder;

    invoke-direct {p4}, Lcom/squareup/protos/client/bills/Cart$Builder;-><init>()V

    .line 2464
    invoke-virtual {p3}, Lcom/squareup/protos/client/bills/Cart$LineItems$Builder;->build()Lcom/squareup/protos/client/bills/Cart$LineItems;

    move-result-object p3

    invoke-virtual {p4, p3}, Lcom/squareup/protos/client/bills/Cart$Builder;->line_items(Lcom/squareup/protos/client/bills/Cart$LineItems;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p3

    .line 2465
    invoke-virtual {p0, p1, p2}, Lcom/squareup/payment/Order;->getAmounts(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart$Amounts;

    move-result-object p1

    invoke-virtual {p3, p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->amounts(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p1

    .line 2467
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->hasReturn()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 2468
    iget-object p3, p0, Lcom/squareup/payment/Order;->returnCart:Lcom/squareup/checkout/ReturnCart;

    invoke-virtual {p3}, Lcom/squareup/checkout/ReturnCart;->toReturnLineItems()Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    move-result-object p3

    .line 2471
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object p4

    .line 2473
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getReturnCart()Lcom/squareup/checkout/ReturnCart;

    move-result-object p5

    .line 2474
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 2470
    invoke-static {p4, p2, p5, v0}, Lcom/squareup/payment/OrderConversionUtils;->getAmountDetails(Lcom/squareup/calc/Adjuster;Lcom/squareup/protos/common/Money;Lcom/squareup/checkout/ReturnCart;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    move-result-object p2

    .line 2478
    invoke-static {p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/protos/client/bills/Cart$Builder;->return_line_items(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p3

    .line 2479
    invoke-virtual {p3, p2}, Lcom/squareup/protos/client/bills/Cart$Builder;->amount_details(Lcom/squareup/protos/client/bills/Cart$AmountDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p3

    iget-object p2, p2, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->sale:Lcom/squareup/protos/client/bills/Cart$Amounts;

    .line 2482
    invoke-virtual {p3, p2}, Lcom/squareup/protos/client/bills/Cart$Builder;->amounts(Lcom/squareup/protos/client/bills/Cart$Amounts;)Lcom/squareup/protos/client/bills/Cart$Builder;

    .line 2484
    :cond_0
    iget-object p2, p1, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    .line 2485
    invoke-virtual {p0, p2}, Lcom/squareup/payment/Order;->getFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/Cart$Builder;->feature_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$Builder;

    move-result-object p1

    .line 2486
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$Builder;->build()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    return-object p1
.end method

.method public getCartProtoForAppointment()Lcom/squareup/protos/client/bills/Cart;
    .locals 6

    .line 2441
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/common/Money;

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v3}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/payment/Order;->getCartProto(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    return-object v0
.end method

.method public getCartProtoForBill(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart;
    .locals 6

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 2424
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/payment/Order;->getCartProto(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    return-object p1
.end method

.method public getCartProtoForExchange()Lcom/squareup/protos/client/bills/Cart;
    .locals 6

    .line 2445
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/payment/Order;->getCartProto(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    return-object v0
.end method

.method public getCartProtoForInvoice(Lcom/squareup/protos/common/Money;Z)Lcom/squareup/protos/client/bills/Cart;
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v5, p2

    .line 2437
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/payment/Order;->getCartProto(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    return-object p1
.end method

.method public getCartProtoForTicket()Lcom/squareup/protos/client/bills/Cart;
    .locals 6

    .line 2432
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/payment/Order;->getCartProto(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    return-object v0
.end method

.method public getCartProtoForTicket(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/Cart;
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    .line 2428
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/payment/Order;->getCartProto(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    return-object p1
.end method

.method public getCompAmountFromDiscount(Ljava/lang/String;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 2283
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->getCollectedAmountPerDiscount()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    .line 2284
    invoke-virtual {p1}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    .line 2282
    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public getCoursingHandler()Lcom/squareup/payment/OrderCoursingHandler;
    .locals 1

    .line 2419
    iget-object v0, p0, Lcom/squareup/payment/Order;->coursingHandler:Lcom/squareup/payment/OrderCoursingHandler;

    return-object v0
.end method

.method public getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 789
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    return-object v0
.end method

.method public getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 2065
    iget-object v0, p0, Lcom/squareup/payment/Order;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public getCustomerId()Ljava/lang/String;
    .locals 1

    .line 2061
    iget-object v0, p0, Lcom/squareup/payment/Order;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getCustomerInstrumentDetails()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;"
        }
    .end annotation

    .line 2073
    iget-object v0, p0, Lcom/squareup/payment/Order;->customerInstruments:Ljava/util/List;

    return-object v0
.end method

.method public getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;
    .locals 1

    .line 2069
    iget-object v0, p0, Lcom/squareup/payment/Order;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    return-object v0
.end method

.method public getDeletedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 1128
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->deletedItemsView()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDeletedTaxItems()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1019
    iget-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxItems:Ljava/util/Map;

    return-object v0
.end method

.method public getDeletedTaxSurcharges()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 1023
    iget-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxSurcharges:Ljava/util/Map;

    return-object v0
.end method

.method public getDeletedTaxesById()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 1015
    iget-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    return-object v0
.end method

.method public getDiningOption()Lcom/squareup/checkout/DiningOption;
    .locals 1

    .line 847
    iget-object v0, p0, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    return-object v0
.end method

.method public getDiningOptionName()Ljava/lang/String;
    .locals 1

    .line 852
    iget-object v0, p0, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/checkout/DiningOption;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getDiscountApplicationIdEnabled()Z
    .locals 1

    .line 526
    iget-boolean v0, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    return v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .line 1976
    iget-object v0, p0, Lcom/squareup/payment/Order;->openTicketName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1977
    iget-object v0, p0, Lcom/squareup/payment/Order;->openTicketName:Ljava/lang/String;

    return-object v0

    .line 1979
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Order;->orderTicketName:Ljava/lang/String;

    return-object v0
.end method

.method public getEmployeeFirstName()Ljava/lang/String;
    .locals 1

    .line 801
    iget-object v0, p0, Lcom/squareup/payment/Order;->employeeFirstName:Ljava/lang/String;

    return-object v0
.end method

.method public getEmployeeLastName()Ljava/lang/String;
    .locals 1

    .line 809
    iget-object v0, p0, Lcom/squareup/payment/Order;->employeeLastName:Ljava/lang/String;

    return-object v0
.end method

.method public getEmployeeToken()Ljava/lang/String;
    .locals 1

    .line 793
    iget-object v0, p0, Lcom/squareup/payment/Order;->employeeToken:Ljava/lang/String;

    return-object v0
.end method

.method public getFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails;
    .locals 4

    if-nez p1, :cond_0

    .line 2491
    new-instance p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    invoke-direct {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;-><init>()V

    goto :goto_0

    .line 2493
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    move-result-object p1

    .line 2496
    :goto_0
    iget-object v0, p0, Lcom/squareup/payment/Order;->orderTicketName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2497
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;

    iget-object v1, p0, Lcom/squareup/payment/Order;->orderTicketName:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->kitchen_printing(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$KitchenPrinting;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    .line 2502
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/Order;->ticketIdPair:Lcom/squareup/protos/client/IdPair;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/payment/Order;->openTicketName:Ljava/lang/String;

    .line 2503
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/payment/Order;->openTicketNote:Ljava/lang/String;

    .line 2504
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/payment/Order;->productsOpenTicketOpenedOn:Ljava/util/List;

    if-eqz v0, :cond_6

    .line 2506
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    if-nez v0, :cond_3

    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;-><init>()V

    goto :goto_1

    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    .line 2508
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v0

    .line 2510
    :goto_1
    iget-object v1, p0, Lcom/squareup/payment/Order;->ticketIdPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->ticket_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/Order;->openTicketName:Ljava/lang/String;

    .line 2511
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/Order;->openTicketNote:Ljava/lang/String;

    .line 2512
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->note(Ljava/lang/String;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/Order;->predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    .line 2513
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->predefined_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    .line 2515
    iget-object v1, p0, Lcom/squareup/payment/Order;->employeeToken:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2516
    new-instance v1, Lcom/squareup/protos/client/CreatorDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/CreatorDetails$Builder;-><init>()V

    new-instance v2, Lcom/squareup/protos/client/Employee$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/Employee$Builder;-><init>()V

    iget-object v3, p0, Lcom/squareup/payment/Order;->employeeToken:Ljava/lang/String;

    .line 2518
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/Employee$Builder;->employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/Order;->employeeFirstName:Ljava/lang/String;

    .line 2519
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/Employee$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/payment/Order;->employeeLastName:Ljava/lang/String;

    .line 2520
    invoke-virtual {v2, v3}, Lcom/squareup/protos/client/Employee$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/protos/client/Employee$Builder;

    move-result-object v2

    .line 2521
    invoke-virtual {v2}, Lcom/squareup/protos/client/Employee$Builder;->build()Lcom/squareup/protos/client/Employee;

    move-result-object v2

    .line 2517
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;

    move-result-object v1

    .line 2522
    invoke-virtual {v1}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v1

    .line 2516
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->ticket_owner(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    .line 2525
    :cond_4
    iget-object v1, p0, Lcom/squareup/payment/Order;->productsOpenTicketOpenedOn:Ljava/util/List;

    if-eqz v1, :cond_5

    .line 2526
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->products_opened_on(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;

    .line 2529
    :cond_5
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->open_ticket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    .line 2532
    :cond_6
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2534
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->newBuilder()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;

    move-result-object v0

    .line 2535
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/SquareCollections;->emptyIfNull(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;->available_instrument_details(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;

    move-result-object v0

    .line 2536
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v0

    goto :goto_2

    .line 2538
    :cond_7
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 2540
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;-><init>()V

    .line 2541
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;->available_instrument_details(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;

    move-result-object v0

    .line 2542
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v0

    .line 2546
    :cond_8
    :goto_2
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->buyer_info(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    .line 2548
    iget-object v0, p0, Lcom/squareup/payment/Order;->invoiceFeatureDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    if-eqz v0, :cond_9

    .line 2549
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->invoice(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    .line 2552
    :cond_9
    iget-object v0, p0, Lcom/squareup/payment/Order;->appointmentsDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    if-eqz v0, :cond_a

    .line 2553
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->appointments_details(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    .line 2556
    :cond_a
    iget-object v0, p0, Lcom/squareup/payment/Order;->seatingHandler:Lcom/squareup/payment/OrderSeatingHandler;

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSeatingHandler;->getSeating()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 2558
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->seating(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Seating;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    .line 2561
    :cond_b
    iget-object v0, p0, Lcom/squareup/payment/Order;->coursingHandler:Lcom/squareup/payment/OrderCoursingHandler;

    invoke-virtual {v0}, Lcom/squareup/payment/OrderCoursingHandler;->getCoursing()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 2563
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->coursing_options(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$CoursingOptions;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    .line 2566
    :cond_c
    iget-object v0, p0, Lcom/squareup/payment/Order;->billPrintState:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    if-eqz v0, :cond_d

    .line 2567
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->bill_print_state(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    .line 2570
    :cond_d
    iget-object v0, p0, Lcom/squareup/payment/Order;->blacklistedDiscounts:Ljava/util/Set;

    if-eqz v0, :cond_e

    .line 2571
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    goto :goto_3

    :cond_e
    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_f

    .line 2573
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState$Builder;-><init>()V

    .line 2574
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/squareup/payment/Order;->blacklistedDiscounts:Ljava/util/Set;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2575
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState$Builder;->blacklisted_discount_ids(Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState$Builder;

    .line 2576
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->pricing_engine_state(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$PricingEngineState;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;

    .line 2579
    :cond_f
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    move-result-object p1

    return-object p1
.end method

.method public getGiftCardTotal()Lcom/squareup/protos/common/Money;
    .locals 4

    .line 2242
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-static {v1, v2, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 2243
    iget-object v1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v1}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 2244
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2245
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getInclusiveTaxesAmount()J
    .locals 2

    .line 2347
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForInclusiveTaxes()J

    move-result-wide v0

    return-wide v0
.end method

.method public getInclusiveTaxesForItem(Lcom/squareup/checkout/CartItem;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 2351
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/calc/Adjuster;->getAdjustedItemFor(Lcom/squareup/calc/order/Item;)Lcom/squareup/calc/AdjustedItem;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/calc/AdjustedItem;->getTotalCollectedForInclusiveTaxes()J

    move-result-wide v0

    .line 2352
    iget-object p1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {p1}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public getItemQuantities()Ljava/math/BigDecimal;
    .locals 4

    .line 2026
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 2027
    iget-object v1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v1}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 2028
    iget-object v3, v2, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v3}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v2, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    goto :goto_1

    :cond_0
    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    :goto_1
    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getItemizationAmountAfterDiscountsApplied(Lcom/squareup/checkout/CartItem;)J
    .locals 5

    .line 1806
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Order;->getAdjustmentAmountsByIdForItem(Lcom/squareup/checkout/CartItem;)Ljava/util/Map;

    move-result-object v0

    .line 1807
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->baseAmount()J

    move-result-wide v1

    .line 1808
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->appliedDiscounts()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/Discount;

    .line 1809
    iget-object v4, v3, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/squareup/checkout/Discount;->getScope()Lcom/squareup/checkout/Discount$Scope;

    move-result-object v4

    iget-boolean v4, v4, Lcom/squareup/checkout/Discount$Scope;->atItemScope:Z

    if-eqz v4, :cond_0

    .line 1811
    iget-object v3, v3, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    add-long/2addr v1, v3

    goto :goto_0

    :cond_1
    return-wide v1
.end method

.method public getItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 1087
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getItemsWithAppliedDiscount(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 1958
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1959
    iget-object v1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v1}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 1960
    iget-object v3, v2, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/Discount;

    .line 1961
    iget-object v4, v4, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1962
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1968
    :cond_2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getNegativeAllDiscounts()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 2272
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAllDiscountsAmount()J

    move-result-wide v0

    neg-long v0, v0

    iget-object v2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v2}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getNegativeAllNonCompDiscounts()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 2277
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getNegativeAllDiscounts()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAllComps()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getNegativePerItemDiscountsAmount()Lcom/squareup/protos/common/Money;
    .locals 6

    .line 2261
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    .line 2262
    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->getAppliedDiscounts()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const-wide/16 v2, 0x0

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/calc/order/Adjustment;

    .line 2263
    move-object v5, v4

    check-cast v5, Lcom/squareup/checkout/Discount;

    invoke-virtual {v5}, Lcom/squareup/checkout/Discount;->canBeAppliedPerItem()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2264
    invoke-virtual {v0, v4}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForDiscount(Lcom/squareup/calc/order/Adjustment;)J

    move-result-wide v4

    add-long/2addr v2, v4

    goto :goto_0

    .line 2267
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getNotVoidedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 1124
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNotVoidedLockedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 1132
    invoke-direct {p0, v0}, Lcom/squareup/payment/Order;->getNotVoidedItemsByStatus(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getNotVoidedUnlockedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 1136
    invoke-direct {p0, v0}, Lcom/squareup/payment/Order;->getNotVoidedItemsByStatus(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getOpenTicketName()Ljava/lang/String;
    .locals 1

    .line 1991
    iget-object v0, p0, Lcom/squareup/payment/Order;->openTicketName:Ljava/lang/String;

    return-object v0
.end method

.method public getOpenTicketNote()Ljava/lang/String;
    .locals 1

    .line 2004
    iget-object v0, p0, Lcom/squareup/payment/Order;->openTicketNote:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderAdjustments()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderAdjustment;",
            ">;"
        }
    .end annotation

    .line 2606
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 2607
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->orderAdjustments:Ljava/util/List;

    .line 2609
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->getAppliedTaxes()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/calc/order/Adjustment;

    .line 2610
    check-cast v1, Lcom/squareup/checkout/Tax;

    .line 2612
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForTax(Lcom/squareup/calc/order/Adjustment;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v4}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 2613
    iget-object v3, p0, Lcom/squareup/payment/Order;->orderAdjustments:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/squareup/payment/OrderAdjustment;->fromTax(Lcom/squareup/checkout/Tax;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/OrderAdjustment;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2616
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->getAppliedDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/calc/order/Adjustment;

    .line 2617
    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 2618
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/squareup/calc/Adjuster;->getTotalCollectedForDiscount(Lcom/squareup/calc/order/Adjustment;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    .line 2619
    invoke-virtual {v4}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v4

    .line 2618
    invoke-static {v2, v3, v4}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 2620
    iget-object v3, p0, Lcom/squareup/payment/Order;->orderAdjustments:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/squareup/payment/OrderAdjustment;->fromDiscount(Lcom/squareup/checkout/Discount;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/OrderAdjustment;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2623
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/Order;->orderAdjustments:Ljava/util/List;

    return-object v0
.end method

.method public getOrderTicketName()Ljava/lang/String;
    .locals 1

    .line 1983
    iget-object v0, p0, Lcom/squareup/payment/Order;->orderTicketName:Ljava/lang/String;

    return-object v0
.end method

.method public getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;
    .locals 1

    .line 2012
    iget-object v0, p0, Lcom/squareup/payment/Order;->predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    return-object v0
.end method

.method public getReturnCart()Lcom/squareup/checkout/ReturnCart;
    .locals 1

    .line 1116
    iget-object v0, p0, Lcom/squareup/payment/Order;->returnCart:Lcom/squareup/checkout/ReturnCart;

    return-object v0
.end method

.method public getReturnItemCount()I
    .locals 1

    .line 1368
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->hasReturn()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/payment/Order;->returnCart:Lcom/squareup/checkout/ReturnCart;

    invoke-virtual {v0}, Lcom/squareup/checkout/ReturnCart;->getReturnItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getSeatingHandler()Lcom/squareup/payment/OrderSeatingHandler;
    .locals 1

    .line 2415
    iget-object v0, p0, Lcom/squareup/payment/Order;->seatingHandler:Lcom/squareup/payment/OrderSeatingHandler;

    return-object v0
.end method

.method public getSensitiveValues()Lcom/squareup/payment/Order$SensitiveValues;
    .locals 1

    .line 1372
    iget-object v0, p0, Lcom/squareup/payment/Order;->sensitiveValues:Lcom/squareup/payment/Order$SensitiveValues;

    return-object v0
.end method

.method public getSubtotal()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 1406
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/calc/Adjuster;->getSubtotal()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v2}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getSurchargeBaseAmount(Lcom/squareup/checkout/Surcharge;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 1069
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/calc/Adjuster;->getSurchargeAdjustedItemFor(Lcom/squareup/calc/order/Surcharge;)Lcom/squareup/calc/AdjustedItem;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/calc/AdjustedItem;->getBaseAmount()J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    .line 1070
    invoke-virtual {p1}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    .line 1069
    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public getSurcharges()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Surcharge;",
            ">;"
        }
    .end annotation

    .line 1060
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getTaxAmountForItem(Lcom/squareup/checkout/CartItem;)Lcom/squareup/protos/common/Money;
    .locals 2

    .line 2374
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Order;->getAdjustedItemFor(Lcom/squareup/checkout/CartItem;)Lcom/squareup/calc/AdjustedItem;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/calc/AdjustedItem;->getTotalCollectedForAllTaxes()J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    .line 2375
    invoke-virtual {p1}, Lcom/squareup/checkout/Cart$Builder;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    .line 2374
    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    return-object p1
.end method

.method public getTicketId()Ljava/lang/String;
    .locals 1

    .line 819
    iget-object v0, p0, Lcom/squareup/payment/Order;->ticketIdPair:Lcom/squareup/protos/client/IdPair;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public getUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation

    .line 1862
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1864
    iget-object v1, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 1865
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    .line 1867
    iget-object v4, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v4}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/checkout/CartItem;

    .line 1868
    invoke-virtual {v5}, Lcom/squareup/checkout/CartItem;->appliedDiscounts()Ljava/util/Map;

    move-result-object v5

    iget-object v6, v2, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v3, 0x1

    :cond_2
    if-nez v3, :cond_0

    .line 1874
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .line 1376
    iget-object v0, p0, Lcom/squareup/payment/Order;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public getVoidedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 1120
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->voidedItemsView()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasAtLeastOneModifier()Z
    .locals 3

    .line 2089
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 2090
    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    .line 2091
    invoke-interface {v1}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 2092
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/SortedMap;

    .line 2093
    invoke-interface {v2}, Ljava/util/SortedMap;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_2
    const/4 v0, 0x0

    return v0
.end method

.method public hasAtLeastOneUnitPricedItem()Z
    .locals 2

    .line 2081
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 2082
    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->isUnitPriced()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hasAvailableNonInclusiveTaxes()Z
    .locals 6

    .line 2171
    iget-object v0, p0, Lcom/squareup/payment/Order;->availableTaxes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 2174
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Order;->availableTaxes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Tax;

    .line 2175
    iget-boolean v4, v2, Lcom/squareup/checkout/Tax;->enabled:Z

    if-eqz v4, :cond_1

    iget-object v2, v2, Lcom/squareup/checkout/Tax;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v4, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-eq v2, v4, :cond_1

    return v3

    .line 2179
    :cond_2
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 2180
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v2, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 2181
    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/Tax;

    .line 2182
    iget-object v4, v4, Lcom/squareup/checkout/Tax;->inclusionType:Lcom/squareup/api/items/Fee$InclusionType;

    sget-object v5, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-eq v4, v5, :cond_4

    return v3

    :cond_5
    return v1
.end method

.method public hasAvailableTaxes()Z
    .locals 5

    .line 2150
    iget-object v0, p0, Lcom/squareup/payment/Order;->availableTaxes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 2153
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Order;->availableTaxes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Tax;

    .line 2154
    iget-boolean v2, v2, Lcom/squareup/checkout/Tax;->enabled:Z

    if-eqz v2, :cond_1

    return v3

    .line 2158
    :cond_2
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 2159
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v2, v2, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    return v3

    :cond_4
    return v1
.end method

.method public hasCompedItems()Z
    .locals 2

    .line 1932
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 1933
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hasCustomer()Z
    .locals 1

    .line 2034
    iget-object v0, p0, Lcom/squareup/payment/Order;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;->customer_id:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasDiningOption()Z
    .locals 1

    .line 843
    iget-object v0, p0, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasDiscountAppliedToAllItems(Ljava/lang/String;)Z
    .locals 5

    .line 2107
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 2108
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    .line 2110
    iget-object v3, v3, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    return v1

    :cond_1
    return v2
.end method

.method public hasDiscounts()Z
    .locals 1

    const/4 v0, 0x1

    .line 1886
    invoke-direct {p0, v0}, Lcom/squareup/payment/Order;->hasDiscounts(Z)Z

    move-result v0

    return v0
.end method

.method public hasExactlyOneDiscount()Z
    .locals 1

    const/4 v0, 0x1

    .line 1834
    invoke-direct {p0, v0}, Lcom/squareup/payment/Order;->hasExactlyOneDiscount(Z)Z

    move-result v0

    return v0
.end method

.method public hasExactlyOneNonCompDiscount()Z
    .locals 1

    const/4 v0, 0x0

    .line 1830
    invoke-direct {p0, v0}, Lcom/squareup/payment/Order;->hasExactlyOneDiscount(Z)Z

    move-result v0

    return v0
.end method

.method public hasGiftCardItem()Z
    .locals 2

    .line 2223
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 2224
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public hasGiftCardItemWithServerId(Ljava/lang/String;)Z
    .locals 3

    .line 2232
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 2233
    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 2234
    invoke-virtual {v1}, Lcom/squareup/checkout/OrderVariation;->getGiftCardDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;->gift_card_server_id:Ljava/lang/String;

    .line 2233
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public hasInvoice()Z
    .locals 1

    .line 831
    iget-object v0, p0, Lcom/squareup/payment/Order;->invoiceFeatureDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasNonCompDiscounts()Z
    .locals 1

    const/4 v0, 0x0

    .line 1890
    invoke-direct {p0, v0}, Lcom/squareup/payment/Order;->hasDiscounts(Z)Z

    move-result v0

    return v0
.end method

.method public hasPerItemDiscounts(Z)Z
    .locals 4

    .line 1921
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 1922
    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 1923
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->canBeAppliedPerItem()Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez p1, :cond_2

    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->isComp()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_2
    const/4 p1, 0x1

    return p1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method public hasPercentDiscounts(Z)Z
    .locals 4

    .line 1910
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 1911
    iget-object v1, v1, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 1912
    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->isAmountDiscount()Z

    move-result v3

    if-nez v3, :cond_1

    if-nez p1, :cond_2

    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->isComp()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_2
    const/4 p1, 0x1

    return p1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method public hasReturn()Z
    .locals 1

    .line 1361
    iget-object v0, p0, Lcom/squareup/payment/Order;->returnCart:Lcom/squareup/checkout/ReturnCart;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasTicket()Z
    .locals 1

    .line 823
    iget-object v0, p0, Lcom/squareup/payment/Order;->ticketIdPair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Z
    .locals 1

    .line 1882
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public init()V
    .locals 4

    .line 741
    iget-object v0, p0, Lcom/squareup/payment/Order;->availableTaxes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Tax;

    .line 742
    iget-object v2, p0, Lcom/squareup/payment/Order;->availableTaxesById:Ljava/util/Map;

    iget-object v3, v1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 745
    :cond_0
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/Order;->onCustomerChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 747
    new-instance v0, Lcom/squareup/payment/Order$SensitiveValues;

    invoke-direct {v0}, Lcom/squareup/payment/Order$SensitiveValues;-><init>()V

    iput-object v0, p0, Lcom/squareup/payment/Order;->sensitiveValues:Lcom/squareup/payment/Order$SensitiveValues;

    .line 748
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->invalidate()V

    return-void
.end method

.method public invalidate()V
    .locals 2

    .line 752
    iget-object v0, p0, Lcom/squareup/payment/Order;->sensitiveValues:Lcom/squareup/payment/Order$SensitiveValues;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/payment/Order$SensitiveValues;->uniqueClientId:Ljava/lang/String;

    .line 753
    iget-object v0, p0, Lcom/squareup/payment/Order;->sensitiveValues:Lcom/squareup/payment/Order$SensitiveValues;

    invoke-static {}, Lcom/squareup/util/Times;->nowAsIso8601()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/payment/Order$SensitiveValues;->iso8601TimeStamp:Ljava/lang/String;

    const/4 v0, 0x0

    .line 754
    iput-object v0, p0, Lcom/squareup/payment/Order;->adjuster:Lcom/squareup/calc/Adjuster;

    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .line 785
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$removeDiscountFromAllItemsInternal$2$Order(Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/squareup/protos/client/Employee;ZLcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 2886
    iget-object v0, p5, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p5

    :cond_0
    const/4 v0, 0x1

    .line 2888
    invoke-virtual {p2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2890
    invoke-virtual {p5}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/checkout/CartItem$Builder;->removeAppliedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    if-eqz p3, :cond_1

    .line 2893
    invoke-static {p3, p1}, Lcom/squareup/checkout/util/ItemizationEvents;->discountRemoveEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    :cond_1
    if-eqz p4, :cond_3

    .line 2896
    iget-object p3, p5, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/checkout/Discount;

    .line 2897
    invoke-virtual {p3}, Lcom/squareup/checkout/Discount;->isCartScopeDiscount()Z

    move-result p3

    if-eqz p3, :cond_2

    .line 2898
    iget-object p3, p0, Lcom/squareup/payment/Order;->blacklistedDiscounts:Ljava/util/Set;

    invoke-interface {p3, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2900
    :cond_2
    invoke-virtual {p2, p1}, Lcom/squareup/checkout/CartItem$Builder;->addBlacklistedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    .line 2903
    :cond_3
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    return-object p1
.end method

.method public manuallyRemoveDiscountFromAllItems(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    .line 1732
    invoke-virtual {p0, p1, v0}, Lcom/squareup/payment/Order;->manuallyRemoveDiscountFromAllItems(Ljava/lang/String;Lcom/squareup/protos/client/Employee;)Z

    move-result p1

    return p1
.end method

.method public manuallyRemoveDiscountFromAllItems(Ljava/lang/String;Lcom/squareup/protos/client/Employee;)Z
    .locals 1

    const/4 v0, 0x1

    .line 1747
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/payment/Order;->removeDiscountFromAllItemsInternal(Ljava/lang/String;ZLcom/squareup/protos/client/Employee;)Z

    move-result p1

    return p1
.end method

.method public markBillPrinted(Ljava/util/Date;)V
    .locals 2

    .line 1390
    new-instance v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;-><init>()V

    const/4 v1, 0x1

    .line 1391
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_printed(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/client/ISO8601Date;

    .line 1392
    invoke-static {p1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/protos/client/ISO8601Date;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->write_only_client_printed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;

    move-result-object p1

    .line 1393
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState$Builder;->build()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/Order;->billPrintState:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BillPrintState;

    return-void
.end method

.method public onCustomerChanged()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 2077
    iget-object v0, p0, Lcom/squareup/payment/Order;->onCustomerChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public orderIdentifier()Ljava/lang/String;
    .locals 1

    .line 2914
    iget-object v0, p0, Lcom/squareup/payment/Order;->sensitiveValues:Lcom/squareup/payment/Order$SensitiveValues;

    iget-object v0, v0, Lcom/squareup/payment/Order$SensitiveValues;->uniqueClientId:Ljava/lang/String;

    return-object v0
.end method

.method public orderIdentifierOrNull()Ljava/lang/String;
    .locals 1

    .line 2918
    iget-object v0, p0, Lcom/squareup/payment/Order;->sensitiveValues:Lcom/squareup/payment/Order$SensitiveValues;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 2921
    :cond_0
    iget-object v0, v0, Lcom/squareup/payment/Order$SensitiveValues;->uniqueClientId:Ljava/lang/String;

    return-object v0
.end method

.method public peekItem()Lcom/squareup/checkout/CartItem;
    .locals 2

    .line 2379
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    .line 2380
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    return-object v0
.end method

.method public peekUninterestingItem()Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 730
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/payment/Order;->peekItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 733
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->peekItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public popItem()Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 2384
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/squareup/payment/Order;->removeItem(I)Lcom/squareup/checkout/CartItem;

    move-result-object v0

    return-object v0
.end method

.method public popUninterestingItem()Lcom/squareup/checkout/CartItem;
    .locals 1

    .line 723
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/payment/Order;->peekItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 726
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->popItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public pricingEngineRemoveDiscountFromAllItems(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1755
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/payment/Order;->removeDiscountFromAllItemsInternal(Ljava/lang/String;ZLcom/squareup/protos/client/Employee;)Z

    move-result p1

    return p1
.end method

.method public pushItem(Lcom/squareup/checkout/CartItem;)V
    .locals 3

    .line 2389
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 2390
    invoke-virtual {v0}, Lcom/squareup/checkout/OrderVariation;->getGiftCardDetails()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$ItemVariationDetails$GiftCardDetails;->gift_card_server_id:Ljava/lang/String;

    .line 2389
    invoke-virtual {p0, v0}, Lcom/squareup/payment/Order;->hasGiftCardItemWithServerId(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 2391
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Cannot add the same gift card twice"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 2394
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2395
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 2396
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 2397
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    .line 2399
    :cond_2
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/checkout/CartItem;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/Cart$Builder;->addItems([Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;

    .line 2400
    invoke-direct {p0}, Lcom/squareup/payment/Order;->reapplyDiscountsThatCanBeAppliedToOnlyOneItem()V

    return-void
.end method

.method public reapplyTaxToItemsAndSurcharges(Ljava/lang/String;)V
    .locals 6

    .line 980
    iget-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxItems:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 981
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Order;->getAvailableTaxes(Ljava/lang/String;)Lcom/squareup/checkout/Tax;

    move-result-object v1

    .line 983
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 984
    iget-object v4, v3, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v4, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 985
    new-instance v4, Ljava/util/LinkedHashMap;

    iget-object v5, v3, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-direct {v4, v5}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 987
    invoke-interface {v4, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 989
    invoke-interface {v4, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 991
    iget-object v5, v3, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v5, v5, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/squareup/payment/Order;->getItemIndex(Ljava/lang/String;)I

    move-result v5

    .line 992
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v3

    .line 991
    invoke-virtual {p0, v5, v3}, Lcom/squareup/payment/Order;->replaceItem(ILcom/squareup/checkout/CartItem;)V

    goto :goto_0

    .line 996
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxSurcharges:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 997
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getSurcharges()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/Surcharge;

    .line 998
    invoke-virtual {v3}, Lcom/squareup/checkout/Surcharge;->id()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 999
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Lcom/squareup/checkout/Surcharge;->appliedTaxes()Ljava/util/Map;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    .line 1001
    invoke-interface {v4, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    xor-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 1003
    invoke-interface {v4, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1005
    invoke-virtual {v3, p1}, Lcom/squareup/checkout/Surcharge;->withReappliedTax(Ljava/lang/String;)Lcom/squareup/checkout/Surcharge;

    move-result-object v4

    .line 1006
    iget-object v5, p0, Lcom/squareup/payment/Order;->surcharges:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/squareup/checkout/Surcharge;->id()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1010
    :cond_3
    iget-object p1, p0, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    iget-object v0, v1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011
    iget-object p1, p0, Lcom/squareup/payment/Order;->deletedTaxItems:Ljava/util/Map;

    iget-object v0, v1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public removeItem(I)Lcom/squareup/checkout/CartItem;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 1158
    invoke-virtual {p0, p1, v0, v1, v0}, Lcom/squareup/payment/Order;->removeItem(IZLcom/squareup/protos/client/Employee;Z)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    return-object p1
.end method

.method public removeItem(IZLcom/squareup/protos/client/Employee;)Lcom/squareup/checkout/CartItem;
    .locals 1

    const/4 v0, 0x0

    .line 1162
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/squareup/payment/Order;->removeItem(IZLcom/squareup/protos/client/Employee;Z)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    return-object p1
.end method

.method public removeItem(IZLcom/squareup/protos/client/Employee;Z)Lcom/squareup/checkout/CartItem;
    .locals 2

    .line 1167
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 1169
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->hasTicket()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/squareup/checkout/CartItem;->lockConfiguration:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    and-int/2addr p2, v1

    .line 1170
    iget-object v1, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v1, p1, p2, p3}, Lcom/squareup/checkout/Cart$Builder;->removeItem(IZLcom/squareup/protos/client/Employee;)Lcom/squareup/checkout/Cart$Builder;

    if-nez p4, :cond_1

    .line 1175
    invoke-direct {p0}, Lcom/squareup/payment/Order;->reapplyDiscountsThatCanBeAppliedToOnlyOneItem()V

    :cond_1
    return-object v0
.end method

.method public removeItem(Ljava/lang/String;ZLcom/squareup/protos/client/Employee;Z)Lcom/squareup/checkout/CartItem;
    .locals 4

    .line 1182
    invoke-direct {p0, p1}, Lcom/squareup/payment/Order;->getItemIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    const-string p1, "Cannot remove item with client id %s"

    .line 1183
    invoke-static {v3, p1, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1185
    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/squareup/payment/Order;->removeItem(IZLcom/squareup/protos/client/Employee;Z)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    return-object p1
.end method

.method public removeNonRecalledFromTicketDiscounts()Z
    .locals 5

    .line 1765
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1766
    iget-object v1, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountsById:Ljava/util/Map;

    .line 1767
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1768
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1769
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 1770
    iget-boolean v3, v2, Lcom/squareup/checkout/Discount;->recalledFromTicket:Z

    if-nez v3, :cond_0

    .line 1771
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 1772
    iget-object v3, p0, Lcom/squareup/payment/Order;->addedCouponsAndCartScopeDiscountEventsById:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1773
    iget-object v2, v2, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1778
    :cond_1
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 1780
    iget-object v2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    new-instance v3, Lcom/squareup/payment/-$$Lambda$Order$F1UYrsEg6Se40VXrta5nYGMEpso;

    invoke-direct {v3, v0, v1}, Lcom/squareup/payment/-$$Lambda$Order$F1UYrsEg6Se40VXrta5nYGMEpso;-><init>(Ljava/util/Set;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    invoke-virtual {v2, v3}, Lcom/squareup/checkout/Cart$Builder;->replaceItems(Lcom/squareup/checkout/CartItem$Transform;)Lcom/squareup/checkout/Cart$Builder;

    .line 1799
    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public removeTaxesFromAllItemsAndSurcharges(Ljava/util/Map;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Adjustment;",
            ">;)Z"
        }
    .end annotation

    .line 891
    invoke-direct {p0, p1}, Lcom/squareup/payment/Order;->removeTaxesFromAllItems(Ljava/util/Map;)Z

    move-result v0

    .line 892
    invoke-direct {p0, p1}, Lcom/squareup/payment/Order;->removeTaxesFromAllSurcharges(Ljava/util/Map;)Z

    move-result p1

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public repeatItem(Ljava/lang/String;Lcom/squareup/util/Res;)Lcom/squareup/checkout/CartItem;
    .locals 7

    .line 1260
    invoke-direct {p0, p1}, Lcom/squareup/payment/Order;->getItem(Ljava/lang/String;)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Item cannot be null"

    .line 1261
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1263
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Order;->getAdjustedItemFor(Lcom/squareup/checkout/CartItem;)Lcom/squareup/calc/AdjustedItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    invoke-virtual {p1, v0, v1}, Lcom/squareup/checkout/CartItem;->getItemizationProto(Lcom/squareup/calc/AdjustedItem;Z)Lcom/squareup/protos/client/bills/Itemization;

    move-result-object v0

    .line 1267
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration;->selected_options:Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    .line 1268
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1269
    iget-object v3, v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->discount:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/DiscountLineItem;

    .line 1270
    iget-object v5, v4, Lcom/squareup/protos/client/bills/DiscountLineItem;->write_only_backing_details:Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/DiscountLineItem$BackingDetails;->discount:Lcom/squareup/api/items/Discount;

    iget-object v5, v5, Lcom/squareup/api/items/Discount;->application_method:Lcom/squareup/api/items/Discount$ApplicationMethod;

    sget-object v6, Lcom/squareup/api/items/Discount$ApplicationMethod;->COMP:Lcom/squareup/api/items/Discount$ApplicationMethod;

    if-eq v5, v6, :cond_1

    .line 1272
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1279
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v3

    new-instance v4, Lcom/squareup/protos/client/IdPair;

    const/4 v5, 0x0

    .line 1280
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/squareup/protos/client/IdPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/squareup/protos/client/bills/Itemization$Builder;->itemization_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v3

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization;->configuration:Lcom/squareup/protos/client/bills/Itemization$Configuration;

    .line 1282
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    move-result-object v0

    .line 1284
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;->newBuilder()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object v1

    .line 1285
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->discount(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;

    move-result-object v1

    .line 1286
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;

    move-result-object v1

    .line 1283
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->selected_options(Lcom/squareup/protos/client/bills/Itemization$Configuration$SelectedOptions;)Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;

    move-result-object v0

    .line 1288
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$Configuration$Builder;->build()Lcom/squareup/protos/client/bills/Itemization$Configuration;

    move-result-object v0

    .line 1281
    invoke-virtual {v3, v0}, Lcom/squareup/protos/client/bills/Itemization$Builder;->configuration(Lcom/squareup/protos/client/bills/Itemization$Configuration;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v0

    .line 1290
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/Itemization$Builder;->event(Ljava/util/List;)Lcom/squareup/protos/client/bills/Itemization$Builder;

    move-result-object v0

    .line 1291
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Itemization$Builder;->build()Lcom/squareup/protos/client/bills/Itemization;

    move-result-object v2

    .line 1293
    new-instance v1, Lcom/squareup/checkout/CartItem$Builder;

    invoke-direct {v1}, Lcom/squareup/checkout/CartItem$Builder;-><init>()V

    sget-object v4, Lcom/squareup/payment/OrderVariationNames;->INSTANCE:Lcom/squareup/payment/OrderVariationNames;

    iget-boolean v5, p0, Lcom/squareup/payment/Order;->discountApplicationIdEnabled:Z

    iget-object v0, p0, Lcom/squareup/payment/Order;->seatingHandler:Lcom/squareup/payment/OrderSeatingHandler;

    .line 1295
    invoke-virtual {v0}, Lcom/squareup/payment/OrderSeatingHandler;->getAllSeats()Ljava/util/List;

    move-result-object v6

    move-object v3, p2

    .line 1294
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/checkout/CartItem$Builder;->fromCartItemization(Lcom/squareup/protos/client/bills/Itemization;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->itemName:Ljava/lang/String;

    .line 1296
    invoke-virtual {p2, v0}, Lcom/squareup/checkout/CartItem$Builder;->itemName(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->variations:Ljava/util/List;

    .line 1297
    invoke-virtual {p2, v0}, Lcom/squareup/checkout/CartItem$Builder;->variations(Ljava/util/List;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->modifierLists:Ljava/util/SortedMap;

    .line 1298
    invoke-virtual {p2, v0}, Lcom/squareup/checkout/CartItem$Builder;->modifierLists(Ljava/util/SortedMap;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->selectedModifiers:Ljava/util/SortedMap;

    .line 1299
    invoke-virtual {p2, v0}, Lcom/squareup/checkout/CartItem$Builder;->selectedModifiers(Ljava/util/SortedMap;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->selectedVariation:Lcom/squareup/checkout/OrderVariation;

    .line 1300
    invoke-virtual {p2, p1}, Lcom/squareup/checkout/CartItem$Builder;->selectedVariation(Lcom/squareup/checkout/OrderVariation;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p1

    .line 1301
    invoke-virtual {p1}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    return-object p1
.end method

.method public replaceDeletedItems(Lcom/squareup/checkout/CartItem$Transform;)V
    .locals 1

    .line 1350
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/Cart$Builder;->replaceDeletedItems(Lcom/squareup/checkout/CartItem$Transform;)Lcom/squareup/checkout/Cart$Builder;

    return-void
.end method

.method public replaceItem(ILcom/squareup/checkout/CartItem;)V
    .locals 3

    .line 1328
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 1333
    iget-object v1, v0, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    iget-object v2, p2, Lcom/squareup/checkout/CartItem;->idPair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/IdPair;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1334
    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->appliedDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1335
    iget-object v0, p2, Lcom/squareup/checkout/CartItem;->appliedDiscounts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1336
    iget-object v0, p2, Lcom/squareup/checkout/CartItem;->blacklistedDiscounts:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1337
    iget-object v0, p0, Lcom/squareup/payment/Order;->deletedTaxesById:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 1340
    :cond_0
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;

    .line 1341
    invoke-direct {p0}, Lcom/squareup/payment/Order;->reapplyDiscountsThatCanBeAppliedToOnlyOneItem()V

    return-void
.end method

.method public replaceItem(ILrx/functions/Func1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lrx/functions/Func1<",
            "Lcom/squareup/checkout/CartItem$Builder;",
            "Lcom/squareup/checkout/CartItem$Builder;",
            ">;)V"
        }
    .end annotation

    .line 1345
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILrx/functions/Func1;)Lcom/squareup/checkout/Cart$Builder;

    .line 1346
    invoke-direct {p0}, Lcom/squareup/payment/Order;->reapplyDiscountsThatCanBeAppliedToOnlyOneItem()V

    return-void
.end method

.method requireInvoiceId()Ljava/lang/String;
    .locals 1

    .line 835
    iget-object v0, p0, Lcom/squareup/payment/Order;->invoiceFeatureDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;->invoice_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    return-object v0
.end method

.method public requireTimestampForReprint()Ljava/util/Date;
    .locals 2

    .line 762
    iget-object v0, p0, Lcom/squareup/payment/Order;->timestampForReprint:Ljava/util/Date;

    const-string v1, "timestampForReprint"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 763
    iget-object v0, p0, Lcom/squareup/payment/Order;->timestampForReprint:Ljava/util/Date;

    return-object v0
.end method

.method public requiresSynchronousAuthorization()Z
    .locals 1

    .line 2219
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->hasGiftCardItem()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/payment/Order;->hasInvoice()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public resolveTaxDiscrepancies(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newTaxes"

    .line 2410
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 2411
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-static {v0, p1}, Lcom/squareup/checkout/CartItemTaxUpdater;->updateAppliedTaxes(Lcom/squareup/checkout/Cart$Builder;Ljava/util/Map;)V

    return-void
.end method

.method setAppointmentDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;)V
    .locals 0

    .line 839
    iput-object p1, p0, Lcom/squareup/payment/Order;->appointmentsDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$AppointmentsDetails;

    return-void
.end method

.method public setCustomer(Lcom/squareup/payment/crm/HoldsCustomer;)V
    .locals 2

    .line 2039
    invoke-interface {p1}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    .line 2040
    invoke-interface {p1}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerSummary()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object v1

    .line 2041
    invoke-interface {p1}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerInstrumentDetails()Ljava/util/List;

    move-result-object p1

    .line 2038
    invoke-virtual {p0, v0, v1, p1}, Lcom/squareup/payment/Order;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    return-void
.end method

.method public setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$FeatureDetails$InstrumentDetails;",
            ">;)V"
        }
    .end annotation

    .line 2046
    iput-object p1, p0, Lcom/squareup/payment/Order;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 2047
    iput-object p2, p0, Lcom/squareup/payment/Order;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 2048
    invoke-static {p1, p3}, Lcom/squareup/cardonfile/StoredInstrumentHelper;->getInstrumentFromContactUnlessNull(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/Order;->customerInstruments:Ljava/util/List;

    .line 2051
    iget-object p1, p0, Lcom/squareup/payment/Order;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    const/4 p2, 0x1

    const/4 p3, 0x0

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/payment/Order;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 2052
    :goto_0
    iget-object v0, p0, Lcom/squareup/payment/Order;->customerInstruments:Ljava/util/List;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 p2, 0x0

    :goto_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_3

    .line 2054
    :cond_2
    iget-object p1, p0, Lcom/squareup/payment/Order;->customerContact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object p2, p0, Lcom/squareup/payment/Order;->customerInstruments:Ljava/util/List;

    invoke-static {p1, p2}, Lcom/squareup/crm/util/RolodexContactHelper;->toBuyerInfo(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/List;)Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/Order;->customerSummary:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;

    .line 2057
    :cond_3
    iget-object p1, p0, Lcom/squareup/payment/Order;->onCustomerChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setDiningOption(Lcom/squareup/checkout/DiningOption;)V
    .locals 6

    .line 857
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v0

    .line 858
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    invoke-virtual {p1, v1}, Lcom/squareup/checkout/DiningOption;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 861
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v1

    const/4 v3, 0x0

    .line 862
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 863
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkout/CartItem;

    .line 864
    invoke-virtual {v4}, Lcom/squareup/checkout/CartItem;->hasDiningOption()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v4}, Lcom/squareup/checkout/CartItem;->isGiftCard()Z

    move-result v5

    if-nez v5, :cond_0

    .line 865
    invoke-static {v4, v0, p1}, Lcom/squareup/util/ConditionalTaxesHelper;->updateTaxesUpon(Lcom/squareup/checkout/CartItem;Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object v3

    .line 866
    invoke-virtual {p0, v2, v3}, Lcom/squareup/payment/Order;->replaceItem(ILcom/squareup/checkout/CartItem;)V

    const/4 v3, 0x1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v2, v3

    .line 871
    :cond_2
    iput-object p1, p0, Lcom/squareup/payment/Order;->diningOption:Lcom/squareup/checkout/DiningOption;

    if-eqz v2, :cond_3

    .line 873
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->invalidate()V

    :cond_3
    return-void
.end method

.method public setEmployeeFirstName(Ljava/lang/String;)V
    .locals 0

    .line 805
    iput-object p1, p0, Lcom/squareup/payment/Order;->employeeFirstName:Ljava/lang/String;

    return-void
.end method

.method public setEmployeeLastName(Ljava/lang/String;)V
    .locals 0

    .line 813
    iput-object p1, p0, Lcom/squareup/payment/Order;->employeeLastName:Ljava/lang/String;

    return-void
.end method

.method public setEmployeeToken(Ljava/lang/String;)V
    .locals 0

    .line 797
    iput-object p1, p0, Lcom/squareup/payment/Order;->employeeToken:Ljava/lang/String;

    return-void
.end method

.method setInvoiceDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;)V
    .locals 0

    .line 827
    iput-object p1, p0, Lcom/squareup/payment/Order;->invoiceFeatureDetails:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$Invoice;

    return-void
.end method

.method public setOpenTicketName(Ljava/lang/String;)V
    .locals 0

    .line 1995
    iput-object p1, p0, Lcom/squareup/payment/Order;->openTicketName:Ljava/lang/String;

    .line 2000
    invoke-virtual {p0, p1}, Lcom/squareup/payment/Order;->setOrderTicketName(Ljava/lang/String;)V

    return-void
.end method

.method public setOpenTicketNote(Ljava/lang/String;)V
    .locals 0

    .line 2008
    iput-object p1, p0, Lcom/squareup/payment/Order;->openTicketNote:Ljava/lang/String;

    return-void
.end method

.method public setOrderTicketName(Ljava/lang/String;)V
    .locals 0

    .line 1987
    iput-object p1, p0, Lcom/squareup/payment/Order;->orderTicketName:Ljava/lang/String;

    return-void
.end method

.method public setPredefinedTicket(Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)V
    .locals 0

    .line 2016
    iput-object p1, p0, Lcom/squareup/payment/Order;->predefinedTicket:Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    return-void
.end method

.method public setReturnCart(Lcom/squareup/checkout/ReturnCart;)V
    .locals 0

    .line 1354
    iput-object p1, p0, Lcom/squareup/payment/Order;->returnCart:Lcom/squareup/checkout/ReturnCart;

    return-void
.end method

.method public setTimestampForReprint(Ljava/util/Date;)V
    .locals 0

    .line 758
    iput-object p1, p0, Lcom/squareup/payment/Order;->timestampForReprint:Ljava/util/Date;

    return-void
.end method

.method public snapshot()Lcom/squareup/payment/OrderSnapshot;
    .locals 2

    .line 709
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0}, Lcom/squareup/checkout/Cart$Builder;->itemsView()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/payment/Order;->popUninterestingItem()Lcom/squareup/checkout/CartItem;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 711
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->invalidate()V

    .line 713
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getAdjuster()Lcom/squareup/calc/Adjuster;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/calc/Adjuster;->calculate()V

    .line 714
    new-instance v1, Lcom/squareup/payment/OrderSnapshot;

    invoke-direct {v1, p0}, Lcom/squareup/payment/OrderSnapshot;-><init>(Lcom/squareup/payment/Order;)V

    if-eqz v0, :cond_2

    .line 716
    invoke-virtual {p0, v0}, Lcom/squareup/payment/Order;->pushItem(Lcom/squareup/checkout/CartItem;)V

    .line 717
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->invalidate()V

    :cond_2
    return-object v1
.end method

.method public splitItem(ILcom/squareup/protos/client/Employee;Ljava/math/BigDecimal;)V
    .locals 2

    .line 1245
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 1247
    invoke-virtual {p0, v0, p2, p3}, Lcom/squareup/payment/Order;->splitItemInPlace(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;Ljava/math/BigDecimal;)Lcom/squareup/payment/Order$SplitResult;

    move-result-object p3

    .line 1249
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    iget-object v1, p3, Lcom/squareup/payment/Order$SplitResult;->parent:Lcom/squareup/checkout/CartItem;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;

    .line 1252
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1, p2}, Lcom/squareup/checkout/Cart$Builder;->removeItem(IZLcom/squareup/protos/client/Employee;)Lcom/squareup/checkout/Cart$Builder;

    .line 1255
    iget-object p2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    iget-object v0, p3, Lcom/squareup/payment/Order$SplitResult;->first:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p2, p1, v0}, Lcom/squareup/checkout/Cart$Builder;->addItem(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;

    .line 1256
    iget-object p2, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    add-int/2addr p1, v1

    iget-object p3, p3, Lcom/squareup/payment/Order$SplitResult;->second:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p2, p1, p3}, Lcom/squareup/checkout/Cart$Builder;->addItem(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;

    return-void
.end method

.method public splitItemInPlace(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;Ljava/math/BigDecimal;)Lcom/squareup/payment/Order$SplitResult;
    .locals 3

    const-wide/16 v0, 0x0

    .line 1312
    invoke-static {p3, v0, v1}, Lcom/squareup/util/BigDecimals;->greaterThan(Ljava/math/BigDecimal;J)Z

    move-result v0

    const-string v1, "Cannot split zero quantity."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1313
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-static {p3, v0}, Lcom/squareup/util/BigDecimals;->lessThan(Ljava/math/BigDecimal;Ljava/math/BigDecimal;)Z

    move-result v0

    const-string v1, "Cannot split more than item\'s quantity."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1315
    new-instance v0, Lcom/squareup/payment/Order$SplitResult;

    invoke-direct {v0}, Lcom/squareup/payment/Order$SplitResult;-><init>()V

    .line 1317
    invoke-static {p1, p3}, Lcom/squareup/payment/Order;->splitItem(Lcom/squareup/checkout/CartItem;Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/payment/Order$SplitResult;->first:Lcom/squareup/checkout/CartItem;

    .line 1318
    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->quantity:Ljava/math/BigDecimal;

    invoke-virtual {v1, p3}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object p3

    invoke-static {p1, p3}, Lcom/squareup/payment/Order;->splitItem(Lcom/squareup/checkout/CartItem;Ljava/math/BigDecimal;)Lcom/squareup/checkout/CartItem;

    move-result-object p3

    iput-object p3, v0, Lcom/squareup/payment/Order$SplitResult;->second:Lcom/squareup/checkout/CartItem;

    .line 1321
    iget-object p3, v0, Lcom/squareup/payment/Order$SplitResult;->first:Lcom/squareup/checkout/CartItem;

    iget-object v1, v0, Lcom/squareup/payment/Order$SplitResult;->second:Lcom/squareup/checkout/CartItem;

    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getTicketId()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, p3, v1, v2}, Lcom/squareup/checkout/util/ItemizationEvents;->splitEvent(Lcom/squareup/protos/client/Employee;Lcom/squareup/checkout/CartItem;Lcom/squareup/checkout/CartItem;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p2

    .line 1322
    invoke-static {p1, p2}, Lcom/squareup/checkout/util/ItemizationEvents;->itemWithEvent(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    iput-object p1, v0, Lcom/squareup/payment/Order$SplitResult;->parent:Lcom/squareup/checkout/CartItem;

    return-object v0
.end method

.method public timestampAsDate()Ljava/util/Date;
    .locals 2

    .line 2649
    iget-object v0, p0, Lcom/squareup/payment/Order;->sensitiveValues:Lcom/squareup/payment/Order$SensitiveValues;

    if-eqz v0, :cond_0

    .line 2653
    :try_start_0
    iget-object v0, v0, Lcom/squareup/payment/Order$SensitiveValues;->iso8601TimeStamp:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Times;->parseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 2655
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 2650
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "No sensitive values, can\'t parse timestamp."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public uncompItem(ILcom/squareup/protos/client/Employee;)V
    .locals 2

    .line 1220
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 1221
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isComped()Z

    move-result v0

    const-string v1, "Cannot uncomp a not-comped item."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1223
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-direct {p0}, Lcom/squareup/payment/Order;->getAddedCartScopeCouponDiscounts()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/squareup/checkout/Cart$Builder;->uncompItem(ILcom/squareup/protos/client/Employee;Ljava/util/List;)Lcom/squareup/checkout/Cart$Builder;

    return-void
.end method

.method public voidItem(ILcom/squareup/protos/client/Employee;Ljava/lang/String;)V
    .locals 2

    .line 1227
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->getItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 1229
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "Cannot void an already voided item."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1230
    invoke-virtual {p0}, Lcom/squareup/payment/Order;->hasTicket()Z

    move-result v0

    const-string v1, "Cannot void an item unless it is in a Ticket."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 1237
    iget-object v0, p0, Lcom/squareup/payment/Order;->cart:Lcom/squareup/checkout/Cart$Builder;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/checkout/Cart$Builder;->voidItem(ILcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/checkout/Cart$Builder;

    return-void
.end method
