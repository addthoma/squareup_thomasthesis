.class public interface abstract Lcom/squareup/payment/TenderInEdit;
.super Ljava/lang/Object;
.source "TenderInEdit.java"


# virtual methods
.method public abstract asAcceptsTips()Lcom/squareup/payment/AcceptsTips;
.end method

.method public abstract assertNoTenderInEdit()V
.end method

.method public abstract clearBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;
.end method

.method public abstract clearCashTender()Lcom/squareup/payment/tender/CashTender$Builder;
.end method

.method public abstract clearEmoney()Lcom/squareup/payment/tender/EmoneyTender$Builder;
.end method

.method public abstract clearInstrumentTender()Lcom/squareup/payment/tender/InstrumentTender$Builder;
.end method

.method public abstract clearMagStripeTender()Lcom/squareup/payment/tender/MagStripeTenderBuilder;
.end method

.method public abstract clearOtherTender()Lcom/squareup/payment/tender/OtherTender$Builder;
.end method

.method public abstract clearSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;
.end method

.method public abstract editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V
.end method

.method public abstract getAmount()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getContact()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract isEditingTender()Z
.end method

.method public abstract isInstrumentTender()Z
.end method

.method public abstract isMagStripeTender()Z
.end method

.method public abstract isSmartCardTender()Z
.end method

.method public abstract requireBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;
.end method

.method public abstract requireCashTender()Lcom/squareup/payment/tender/CashTender$Builder;
.end method

.method public abstract requireEmoney()Lcom/squareup/payment/tender/EmoneyTender$Builder;
.end method

.method public abstract requireInstrumentTender()Lcom/squareup/payment/tender/InstrumentTender$Builder;
.end method

.method public abstract requireMagStripeTender()Lcom/squareup/payment/tender/MagStripeTenderBuilder;
.end method

.method public abstract requireOtherTender()Lcom/squareup/payment/tender/OtherTender$Builder;
.end method

.method public abstract requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;
.end method

.method public abstract reset()V
.end method

.method public abstract setContact(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method
