.class public final Lcom/squareup/payment/BillPaymentOnlineStrategy_Factory_Factory;
.super Ljava/lang/Object;
.source "BillPaymentOnlineStrategy_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final backgroundCaptorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BackgroundCaptor;",
            ">;"
        }
    .end annotation
.end field

.field private final billCreationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BackgroundCaptor;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy_Factory_Factory;->billCreationServiceProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy_Factory_Factory;->backgroundCaptorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/payment/BillPaymentOnlineStrategy_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillCreationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/BackgroundCaptor;",
            ">;)",
            "Lcom/squareup/payment/BillPaymentOnlineStrategy_Factory_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/payment/BillPaymentOnlineStrategy_Factory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/payment/BillPaymentOnlineStrategy_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/bills/BillCreationService;Ljava/lang/Object;)Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    check-cast p1, Lcom/squareup/payment/BackgroundCaptor;

    invoke-direct {v0, p0, p1}, Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;-><init>(Lcom/squareup/server/bills/BillCreationService;Lcom/squareup/payment/BackgroundCaptor;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy_Factory_Factory;->billCreationServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/bills/BillCreationService;

    iget-object v1, p0, Lcom/squareup/payment/BillPaymentOnlineStrategy_Factory_Factory;->backgroundCaptorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/payment/BillPaymentOnlineStrategy_Factory_Factory;->newInstance(Lcom/squareup/server/bills/BillCreationService;Ljava/lang/Object;)Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/payment/BillPaymentOnlineStrategy_Factory_Factory;->get()Lcom/squareup/payment/BillPaymentOnlineStrategy$Factory;

    move-result-object v0

    return-object v0
.end method
