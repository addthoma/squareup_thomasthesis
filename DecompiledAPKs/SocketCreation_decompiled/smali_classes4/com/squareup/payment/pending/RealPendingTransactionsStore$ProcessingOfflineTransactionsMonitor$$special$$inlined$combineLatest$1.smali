.class public final Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor$$special$$inlined$combineLatest$1;
.super Ljava/lang/Object;
.source "RxKotlin.kt"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;-><init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore;Lcom/squareup/connectivity/ConnectivityMonitor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiFunction<",
        "TT1;TT2;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxKotlin.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxKotlin.kt\ncom/squareup/util/rx2/Observables$combineLatest$1\n+ 2 RealPendingTransactionsStore.kt\ncom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor\n*L\n1#1,1655:1\n470#2,18:1656\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0006\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0001*\u00020\u00032\u0006\u0010\u0005\u001a\u0002H\u00022\u0006\u0010\u0006\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "R",
        "T1",
        "",
        "T2",
        "t1",
        "t2",
        "apply",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/util/rx2/Observables$combineLatest$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor$$special$$inlined$combineLatest$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT1;TT2;)TR;"
        }
    .end annotation

    const-string v0, "t1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "t2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    check-cast p1, Lcom/squareup/connectivity/InternetState;

    .line 1656
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor$$special$$inlined$combineLatest$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;

    invoke-virtual {v0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->getProcessingOfflineTransactions()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 1658
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor$$special$$inlined$combineLatest$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;

    invoke-virtual {v0}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->getPriorInternetState()Lcom/squareup/connectivity/InternetState;

    move-result-object v2

    sget-object v3, Lcom/squareup/connectivity/InternetState;->NOT_CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne v2, v3, :cond_0

    sget-object v2, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p1, v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0, v2}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->setProcessingOfflineTransactions(Z)V

    .line 1661
    :cond_1
    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor$$special$$inlined$combineLatest$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->setPriorInternetState(Lcom/squareup/connectivity/InternetState;)V

    .line 1664
    sget-object v0, Lcom/squareup/connectivity/InternetState;->NOT_CONNECTED:Lcom/squareup/connectivity/InternetState;

    if-ne p1, v0, :cond_2

    .line 1665
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor$$special$$inlined$combineLatest$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;

    invoke-virtual {p1, v1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->setProcessingOfflineTransactions(Z)V

    :cond_2
    if-nez p2, :cond_3

    .line 1670
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor$$special$$inlined$combineLatest$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;

    invoke-virtual {p1, v1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->setProcessingOfflineTransactions(Z)V

    .line 1673
    :cond_3
    iget-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor$$special$$inlined$combineLatest$1;->this$0:Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;

    invoke-virtual {p1}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$ProcessingOfflineTransactionsMonitor;->getProcessingOfflineTransactions()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method
