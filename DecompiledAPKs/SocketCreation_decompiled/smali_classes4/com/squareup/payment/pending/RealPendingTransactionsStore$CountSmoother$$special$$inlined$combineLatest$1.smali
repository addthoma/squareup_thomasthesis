.class public final Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother$$special$$inlined$combineLatest$1;
.super Ljava/lang/Object;
.source "RxKotlin.kt"

# interfaces
.implements Lio/reactivex/functions/Function5;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/payment/offline/ForwardedPaymentManager;Lcom/squareup/queue/sqlite/StoredPaymentsMonitor;Lcom/squareup/queue/sqlite/PendingCapturesMonitor;Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "T4:",
        "Ljava/lang/Object;",
        "T5:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function5<",
        "TT1;TT2;TT3;TT4;TT5;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxKotlin.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxKotlin.kt\ncom/squareup/util/rx2/Observables$combineLatest$8\n+ 2 RealPendingTransactionsStore.kt\ncom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother\n*L\n1#1,1655:1\n403#2:1656\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u000b\n\u0002\u0008\u000b\n\u0002\u0008\u000b\n\u0002\u0008\u000b\n\u0002\u0008\u000b\n\u0002\u0008\u000b\n\u0002\u0008\u000b\n\u0002\u0008\u000b\n\u0002\u0008\u000b\n\u0002\u0008\u000b\n\u0002\u0008\u000c\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003\"\u0008\u0008\u0003\u0010\u0006*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0007*\u00020\u0003\"\u0008\u0008\u0005\u0010\u0001*\u00020\u00032\u0006\u0010\u0008\u001a\u0002H\u00022\u0006\u0010\t\u001a\u0002H\u00042\u0006\u0010\n\u001a\u0002H\u00052\u0006\u0010\u000b\u001a\u0002H\u00062\u0006\u0010\u000c\u001a\u0002H\u0007H\n\u00a2\u0006\u0004\u0008\r\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "<anonymous>",
        "R",
        "T1",
        "",
        "T2",
        "T3",
        "T4",
        "T5",
        "t1",
        "t2",
        "t3",
        "t4",
        "t5",
        "apply",
        "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/util/rx2/Observables$combineLatest$8"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic receiver$0$inlined:Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother$$special$$inlined$combineLatest$1;->receiver$0$inlined:Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT1;TT2;TT3;TT4;TT5;)TR;"
        }
    .end annotation

    const-string v0, "t1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "t2"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "t3"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "t4"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "t5"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    check-cast p5, Ljava/lang/Number;

    invoke-virtual {p5}, Ljava/lang/Number;->intValue()I

    move-result v5

    check-cast p4, Ljava/lang/Number;

    invoke-virtual {p4}, Ljava/lang/Number;->intValue()I

    move-result v4

    move-object v3, p3

    check-cast v3, Ljava/util/List;

    move-object v2, p2

    check-cast v2, Ljava/util/Collection;

    move-object v1, p1

    check-cast v1, Ljava/util/Collection;

    iget-object v0, p0, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother$$special$$inlined$combineLatest$1;->receiver$0$inlined:Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;

    .line 1656
    invoke-static/range {v0 .. v5}, Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;->access$smoothedCount(Lcom/squareup/payment/pending/RealPendingTransactionsStore$CountSmoother;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/List;II)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method
