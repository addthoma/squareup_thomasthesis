.class public interface abstract Lcom/squareup/payment/NonForwardedPendingTransactionsCounter;
.super Ljava/lang/Object;
.source "NonForwardedPendingTransactionsCounter.java"


# virtual methods
.method public abstract nonForwardedPendingTransactionsCount()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method
