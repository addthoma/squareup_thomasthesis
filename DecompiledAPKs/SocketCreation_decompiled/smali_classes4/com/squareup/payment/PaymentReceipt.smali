.class public abstract Lcom/squareup/payment/PaymentReceipt;
.super Ljava/lang/Object;
.source "PaymentReceipt.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/payment/PaymentReceipt$TenderReceipt;,
        Lcom/squareup/payment/PaymentReceipt$NoTenderReceipt;,
        Lcom/squareup/payment/PaymentReceipt$VoidReceipt;,
        Lcom/squareup/payment/PaymentReceipt$Builder;,
        Lcom/squareup/payment/PaymentReceipt$RealFactory;,
        Lcom/squareup/payment/PaymentReceipt$Factory;
    }
.end annotation


# instance fields
.field protected final defaultEmail:Lcom/squareup/payment/Obfuscated;

.field protected final defaultSms:Lcom/squareup/payment/Obfuscated;

.field protected orderDisplayName:Ljava/lang/String;

.field protected orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

.field protected final payment:Lcom/squareup/payment/Payment;

.field protected final ticketName:Ljava/lang/CharSequence;


# direct methods
.method private constructor <init>(Lcom/squareup/payment/PaymentReceipt$Builder;)V
    .locals 1

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    invoke-static {p1}, Lcom/squareup/payment/PaymentReceipt$Builder;->access$300(Lcom/squareup/payment/PaymentReceipt$Builder;)Lcom/squareup/payment/Obfuscated;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->defaultEmail:Lcom/squareup/payment/Obfuscated;

    .line 133
    invoke-static {p1}, Lcom/squareup/payment/PaymentReceipt$Builder;->access$400(Lcom/squareup/payment/PaymentReceipt$Builder;)Lcom/squareup/payment/Obfuscated;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->defaultSms:Lcom/squareup/payment/Obfuscated;

    .line 134
    invoke-static {p1}, Lcom/squareup/payment/PaymentReceipt$Builder;->access$500(Lcom/squareup/payment/PaymentReceipt$Builder;)Lcom/squareup/payment/Payment;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->payment:Lcom/squareup/payment/Payment;

    .line 135
    invoke-static {p1}, Lcom/squareup/payment/PaymentReceipt$Builder;->access$600(Lcom/squareup/payment/PaymentReceipt$Builder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->ticketName:Ljava/lang/CharSequence;

    .line 136
    invoke-static {p1}, Lcom/squareup/payment/PaymentReceipt$Builder;->access$700(Lcom/squareup/payment/PaymentReceipt$Builder;)Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    .line 137
    invoke-static {p1}, Lcom/squareup/payment/PaymentReceipt$Builder;->access$800(Lcom/squareup/payment/PaymentReceipt$Builder;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/payment/PaymentReceipt;->orderDisplayName:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/PaymentReceipt$Builder;Lcom/squareup/payment/PaymentReceipt$1;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/payment/PaymentReceipt;-><init>(Lcom/squareup/payment/PaymentReceipt$Builder;)V

    return-void
.end method


# virtual methods
.method public asReturnsChange()Lcom/squareup/payment/tender/BaseTender$ReturnsChange;
    .locals 2

    .line 214
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->payment:Lcom/squareup/payment/Payment;

    instance-of v1, v0, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/payment/tender/BaseTender$ReturnsChange;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public canAutoPrintReceipt()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract decline()V
.end method

.method protected final defaultEmailId()Ljava/lang/String;
    .locals 2

    .line 282
    invoke-virtual {p0}, Lcom/squareup/payment/PaymentReceipt;->getDefaultEmail()Lcom/squareup/payment/Obfuscated;

    move-result-object v0

    const-string v1, "defaultEmail"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Obfuscated;

    invoke-virtual {v0}, Lcom/squareup/payment/Obfuscated;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final defaultSmsId()Ljava/lang/String;
    .locals 2

    .line 286
    invoke-virtual {p0}, Lcom/squareup/payment/PaymentReceipt;->getDefaultSms()Lcom/squareup/payment/Obfuscated;

    move-result-object v0

    const-string v1, "defaultSms"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Obfuscated;

    invoke-virtual {v0}, Lcom/squareup/payment/Obfuscated;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract email(Ljava/lang/String;)V
.end method

.method public abstract emailDefault()V
.end method

.method public getDefaultEmail()Lcom/squareup/payment/Obfuscated;
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->defaultEmail:Lcom/squareup/payment/Obfuscated;

    return-object v0
.end method

.method public getDefaultSms()Lcom/squareup/payment/Obfuscated;
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->defaultSms:Lcom/squareup/payment/Obfuscated;

    return-object v0
.end method

.method public getOrderDisplayName()Ljava/lang/String;
    .locals 1

    .line 258
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->orderDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderSnapshot()Lcom/squareup/payment/OrderSnapshot;
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    return-object v0
.end method

.method public getPayment()Lcom/squareup/payment/Payment;
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->payment:Lcom/squareup/payment/Payment;

    return-object v0
.end method

.method public getRemainingAmountDue()Lcom/squareup/protos/common/Money;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getRemainingBalance()Lcom/squareup/protos/common/Money;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getRemainingBalanceText(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getRemainingBalanceTextForHud()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTenderAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTenderIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTenderType()Lcom/squareup/protos/client/bills/Tender$Type;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTicketName()Ljava/lang/CharSequence;
    .locals 1

    .line 250
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->ticketName:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hasDefaultContact()Z
    .locals 1

    .line 189
    invoke-virtual {p0}, Lcom/squareup/payment/PaymentReceipt;->hasDefaultEmail()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/payment/PaymentReceipt;->hasDefaultSms()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public hasDefaultEmail()Z
    .locals 1

    .line 181
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->defaultEmail:Lcom/squareup/payment/Obfuscated;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasDefaultSms()Z
    .locals 1

    .line 185
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->defaultSms:Lcom/squareup/payment/Obfuscated;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isCard()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isReceiptDeferred()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public paymentComplete()Z
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/squareup/payment/PaymentReceipt;->payment:Lcom/squareup/payment/Payment;

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v0

    return v0
.end method

.method public abstract sms(Ljava/lang/String;)V
.end method

.method public abstract smsDefault()V
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 276
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/payment/PaymentReceipt;->defaultEmail:Lcom/squareup/payment/Obfuscated;

    const-string v2, "null"

    if-nez v1, :cond_0

    move-object v1, v2

    goto :goto_0

    .line 277
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/payment/Obfuscated;->getValue()Ljava/lang/String;

    move-result-object v1

    :goto_0
    const/4 v3, 0x1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v3, p0, Lcom/squareup/payment/PaymentReceipt;->defaultSms:Lcom/squareup/payment/Obfuscated;

    if-nez v3, :cond_1

    goto :goto_1

    .line 278
    :cond_1
    invoke-virtual {v3}, Lcom/squareup/payment/Obfuscated;->getValue()Ljava/lang/String;

    move-result-object v2

    :goto_1
    aput-object v2, v0, v1

    const-string v1, "%s (defaultEmail=%s, defaultSms=%s)"

    .line 276
    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public updateBuyerLanguage(Ljava/util/Locale;)V
    .locals 0

    return-void
.end method
