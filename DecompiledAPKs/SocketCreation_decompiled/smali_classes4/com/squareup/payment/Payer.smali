.class public Lcom/squareup/payment/Payer;
.super Ljava/lang/Object;
.source "Payer.java"


# instance fields
.field private autoSendReceipt:Z

.field private final email:Lcom/squareup/payment/Obfuscated;

.field private final name:Ljava/lang/String;

.field private final phone:Lcom/squareup/payment/Obfuscated;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/squareup/payment/Obfuscated;Lcom/squareup/payment/Obfuscated;Z)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/payment/Payer;->name:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/squareup/payment/Payer;->email:Lcom/squareup/payment/Obfuscated;

    .line 18
    iput-object p3, p0, Lcom/squareup/payment/Payer;->phone:Lcom/squareup/payment/Obfuscated;

    .line 19
    iput-boolean p4, p0, Lcom/squareup/payment/Payer;->autoSendReceipt:Z

    return-void
.end method

.method public static buildFrom(Lcom/squareup/protos/client/Buyer;Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;)Lcom/squareup/payment/Payer;
    .locals 5

    if-eqz p1, :cond_0

    .line 40
    iget-object v0, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->receipt_behavior:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    sget-object v1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;->SKIP_ASKING:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$ReceiptBehavior;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x0

    if-nez p0, :cond_1

    move-object p0, v1

    goto :goto_1

    .line 43
    :cond_1
    iget-object p0, p0, Lcom/squareup/protos/client/Buyer;->full_name:Ljava/lang/String;

    :goto_1
    if-eqz p1, :cond_3

    .line 45
    iget-object v2, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    if-nez v2, :cond_2

    goto :goto_2

    :cond_2
    new-instance v2, Lcom/squareup/payment/Obfuscated;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;->email_id:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->email:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;

    iget-object v4, v4, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Email;->obfuscated_email:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/squareup/payment/Obfuscated;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    :goto_2
    move-object v2, v1

    :goto_3
    if-eqz p1, :cond_5

    .line 48
    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    if-nez v3, :cond_4

    goto :goto_4

    :cond_4
    new-instance v1, Lcom/squareup/payment/Obfuscated;

    iget-object v3, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;->phone_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails;->phone:Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/AddedTender$ReceiptDetails$Phone;->obfuscated_phone:Ljava/lang/String;

    invoke-direct {v1, v3, p1}, Lcom/squareup/payment/Obfuscated;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :cond_5
    :goto_4
    new-instance p1, Lcom/squareup/payment/Payer;

    invoke-direct {p1, p0, v2, v1, v0}, Lcom/squareup/payment/Payer;-><init>(Ljava/lang/String;Lcom/squareup/payment/Obfuscated;Lcom/squareup/payment/Obfuscated;Z)V

    return-object p1
.end method


# virtual methods
.method public getEmail()Lcom/squareup/payment/Obfuscated;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/payment/Payer;->email:Lcom/squareup/payment/Obfuscated;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/payment/Payer;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPhone()Lcom/squareup/payment/Obfuscated;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/payment/Payer;->phone:Lcom/squareup/payment/Obfuscated;

    return-object v0
.end method

.method public shouldAutoSendReceipt()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lcom/squareup/payment/Payer;->autoSendReceipt:Z

    return v0
.end method
