.class Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
.super Ljava/lang/Object;
.source "OrderEntryEvents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/OrderEntryEvents$CartChanged;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Builder"
.end annotation


# instance fields
.field private animate:Z

.field private cardChanged:Z

.field private diningOptionChanged:Z

.field private discountsChanged:Z

.field private itemsChanged:Z

.field private keypadCommitted:Z

.field private paymentChanged:Z

.field private returnChanged:Z

.field private source:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

.field private status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

.field private viaTicket:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 318
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->animate:Z

    .line 319
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->viaTicket:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/payment/OrderEntryEvents$1;)V
    .locals 0

    .line 310
    invoke-direct {p0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z
    .locals 0

    .line 310
    iget-boolean p0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->cardChanged:Z

    return p0
.end method

.method static synthetic access$1000(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Lcom/squareup/payment/OrderEntryEvents$ItemStatus;
    .locals 0

    .line 310
    iget-object p0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Lcom/squareup/payment/OrderEntryEvents$ChangeSource;
    .locals 0

    .line 310
    iget-object p0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z
    .locals 0

    .line 310
    iget-boolean p0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->diningOptionChanged:Z

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z
    .locals 0

    .line 310
    iget-boolean p0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->discountsChanged:Z

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z
    .locals 0

    .line 310
    iget-boolean p0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->itemsChanged:Z

    return p0
.end method

.method static synthetic access$500(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z
    .locals 0

    .line 310
    iget-boolean p0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->keypadCommitted:Z

    return p0
.end method

.method static synthetic access$600(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z
    .locals 0

    .line 310
    iget-boolean p0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->returnChanged:Z

    return p0
.end method

.method static synthetic access$700(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z
    .locals 0

    .line 310
    iget-boolean p0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->paymentChanged:Z

    return p0
.end method

.method static synthetic access$800(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z
    .locals 0

    .line 310
    iget-boolean p0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->animate:Z

    return p0
.end method

.method static synthetic access$900(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;)Z
    .locals 0

    .line 310
    iget-boolean p0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->viaTicket:Z

    return p0
.end method


# virtual methods
.method animate(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 0

    .line 360
    iput-boolean p1, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->animate:Z

    return-object p0
.end method

.method build()Lcom/squareup/payment/OrderEntryEvents$CartChanged;
    .locals 2

    .line 398
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/payment/OrderEntryEvents$CartChanged;-><init>(Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;Lcom/squareup/payment/OrderEntryEvents$1;)V

    return-object v0
.end method

.method cardChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 325
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->cardChanged:Z

    return-object p0
.end method

.method diningOptionChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 330
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->diningOptionChanged:Z

    return-object p0
.end method

.method discountsChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 335
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->discountsChanged:Z

    return-object p0
.end method

.method everythingChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 388
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->paymentChanged:Z

    .line 389
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->cardChanged:Z

    .line 390
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->diningOptionChanged:Z

    .line 391
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->discountsChanged:Z

    .line 392
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->itemsChanged:Z

    .line 393
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->returnChanged:Z

    return-object p0
.end method

.method itemsChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 340
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->itemsChanged:Z

    return-object p0
.end method

.method keypadCommitted()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 345
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->keypadCommitted:Z

    return-object p0
.end method

.method paymentChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 350
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->paymentChanged:Z

    return-object p0
.end method

.method returnChanged()Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 1

    const/4 v0, 0x1

    .line 355
    iput-boolean v0, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->returnChanged:Z

    return-object p0
.end method

.method source(Lcom/squareup/payment/OrderEntryEvents$ChangeSource;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 0

    .line 383
    iput-object p1, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->source:Lcom/squareup/payment/OrderEntryEvents$ChangeSource;

    return-object p0
.end method

.method status(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/checkout/CartItem;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 2

    .line 370
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    sget-object v1, Lcom/squareup/payment/OrderEntryEvents$ItemType;->CART_ITEM:Lcom/squareup/payment/OrderEntryEvents$ItemType;

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;-><init>(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/checkout/CartItem;Lcom/squareup/payment/OrderEntryEvents$ItemType;)V

    invoke-virtual {p0, v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->status(Lcom/squareup/payment/OrderEntryEvents$ItemStatus;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p1

    return-object p1
.end method

.method status(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/checkout/CartItem;Lcom/squareup/payment/OrderEntryEvents$ItemType;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 1

    .line 374
    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/payment/OrderEntryEvents$ItemStatus;-><init>(Lcom/squareup/payment/OrderEntryEvents$ItemChangeType;Lcom/squareup/checkout/CartItem;Lcom/squareup/payment/OrderEntryEvents$ItemType;)V

    invoke-virtual {p0, v0}, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->status(Lcom/squareup/payment/OrderEntryEvents$ItemStatus;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;

    move-result-object p1

    return-object p1
.end method

.method status(Lcom/squareup/payment/OrderEntryEvents$ItemStatus;)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 0

    .line 378
    iput-object p1, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->status:Lcom/squareup/payment/OrderEntryEvents$ItemStatus;

    return-object p0
.end method

.method viaTicket(Z)Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;
    .locals 0

    .line 365
    iput-boolean p1, p0, Lcom/squareup/payment/OrderEntryEvents$CartChanged$Builder;->viaTicket:Z

    return-object p0
.end method
