.class Lcom/squareup/payment/CardConverter$1;
.super Ljava/lang/Object;
.source "CardConverter.java"

# interfaces
.implements Lcom/squareup/Card$InputType$InputTypeHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/payment/CardConverter;->getCardData(Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/Card$InputType$InputTypeHandler<",
        "Lcom/squareup/protos/client/bills/CardData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/payment/CardConverter;

.field final synthetic val$builder:Lcom/squareup/protos/client/bills/CardData$Builder;

.field final synthetic val$card:Lcom/squareup/Card;


# direct methods
.method constructor <init>(Lcom/squareup/payment/CardConverter;Lcom/squareup/protos/client/bills/CardData$Builder;Lcom/squareup/Card;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/payment/CardConverter$1;->this$0:Lcom/squareup/payment/CardConverter;

    iput-object p2, p0, Lcom/squareup/payment/CardConverter$1;->val$builder:Lcom/squareup/protos/client/bills/CardData$Builder;

    iput-object p3, p0, Lcom/squareup/payment/CardConverter$1;->val$card:Lcom/squareup/Card;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleA10(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 2

    .line 60
    iget-object p1, p0, Lcom/squareup/payment/CardConverter$1;->val$builder:Lcom/squareup/protos/client/bills/CardData$Builder;

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->A10:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/payment/CardConverter$1;->val$card:Lcom/squareup/Card;

    .line 62
    invoke-virtual {v1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->encrypted_reader_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$A10Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$A10Card;

    move-result-object v0

    .line 61
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->a10_card(Lcom/squareup/protos/client/bills/CardData$A10Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleA10(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/payment/CardConverter$1;->handleA10(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public handleGen2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 2

    .line 38
    iget-object p1, p0, Lcom/squareup/payment/CardConverter$1;->val$builder:Lcom/squareup/protos/client/bills/CardData$Builder;

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNENCRYPTED:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;

    iget-object v1, p0, Lcom/squareup/payment/CardConverter$1;->val$card:Lcom/squareup/Card;

    .line 39
    invoke-virtual {v1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->unencrypted_card(Lcom/squareup/protos/client/bills/CardData$UnencryptedCard;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleGen2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/payment/CardConverter$1;->handleGen2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public handleManual(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 2

    .line 34
    iget-object p1, p0, Lcom/squareup/payment/CardConverter$1;->this$0:Lcom/squareup/payment/CardConverter;

    iget-object v0, p0, Lcom/squareup/payment/CardConverter$1;->val$builder:Lcom/squareup/protos/client/bills/CardData$Builder;

    iget-object v1, p0, Lcom/squareup/payment/CardConverter$1;->val$card:Lcom/squareup/Card;

    invoke-static {p1, v0, v1}, Lcom/squareup/payment/CardConverter;->access$000(Lcom/squareup/payment/CardConverter;Lcom/squareup/protos/client/bills/CardData$Builder;Lcom/squareup/Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleManual(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/payment/CardConverter$1;->handleManual(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public handleO1(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 2

    .line 43
    iget-object p1, p0, Lcom/squareup/payment/CardConverter$1;->val$builder:Lcom/squareup/protos/client/bills/CardData$Builder;

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->O1:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/client/bills/CardData$O1Card;

    iget-object v1, p0, Lcom/squareup/payment/CardConverter$1;->val$card:Lcom/squareup/Card;

    .line 44
    invoke-virtual {v1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/protos/client/bills/CardData$O1Card;-><init>(Lokio/ByteString;)V

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->o1_card(Lcom/squareup/protos/client/bills/CardData$O1Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleO1(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/payment/CardConverter$1;->handleO1(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public handleR4(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 2

    .line 48
    iget-object p1, p0, Lcom/squareup/payment/CardConverter$1;->val$builder:Lcom/squareup/protos/client/bills/CardData$Builder;

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R4:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/client/bills/CardData$R4Card;

    iget-object v1, p0, Lcom/squareup/payment/CardConverter$1;->val$card:Lcom/squareup/Card;

    .line 49
    invoke-virtual {v1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/protos/client/bills/CardData$R4Card;-><init>(Lokio/ByteString;)V

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->r4_card(Lcom/squareup/protos/client/bills/CardData$R4Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleR4(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/payment/CardConverter$1;->handleR4(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public handleR6(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 2

    .line 53
    iget-object p1, p0, Lcom/squareup/payment/CardConverter$1;->val$builder:Lcom/squareup/protos/client/bills/CardData$Builder;

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/payment/CardConverter$1;->val$card:Lcom/squareup/Card;

    .line 55
    invoke-virtual {v1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;->encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$R6Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$R6Card;

    move-result-object v0

    .line 54
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->r6_card(Lcom/squareup/protos/client/bills/CardData$R6Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleR6(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/payment/CardConverter$1;->handleR6(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public handleT2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 2

    .line 74
    iget-object p1, p0, Lcom/squareup/payment/CardConverter$1;->val$builder:Lcom/squareup/protos/client/bills/CardData$Builder;

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->T2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/payment/CardConverter$1;->val$card:Lcom/squareup/Card;

    .line 76
    invoke-virtual {v1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$T2Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$T2Card;

    move-result-object v0

    .line 75
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->t2_card(Lcom/squareup/protos/client/bills/CardData$T2Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleT2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/payment/CardConverter$1;->handleT2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public handleX2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;
    .locals 2

    .line 67
    iget-object p1, p0, Lcom/squareup/payment/CardConverter$1;->val$builder:Lcom/squareup/protos/client/bills/CardData$Builder;

    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->X2:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->reader_type(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/payment/CardConverter$1;->val$card:Lcom/squareup/Card;

    .line 69
    invoke-virtual {v1}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lokio/ByteString;->decodeBase64(Ljava/lang/String;)Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->encrypted_swipe_data(Lokio/ByteString;)Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/CardData$X2Card$Builder;->build()Lcom/squareup/protos/client/bills/CardData$X2Card;

    move-result-object v0

    .line 68
    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/bills/CardData$Builder;->x2_card(Lcom/squareup/protos/client/bills/CardData$X2Card;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic handleX2(Lcom/squareup/Card$InputType;)Ljava/lang/Object;
    .locals 0

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/payment/CardConverter$1;->handleX2(Lcom/squareup/Card$InputType;)Lcom/squareup/protos/client/bills/CardData$Builder;

    move-result-object p1

    return-object p1
.end method
