.class public interface abstract Lcom/squareup/payment/ledger/DiagnosticsReporter;
.super Ljava/lang/Object;
.source "DiagnosticsReporter.java"


# virtual methods
.method public abstract sendDiagnosticsReport()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation
.end method
