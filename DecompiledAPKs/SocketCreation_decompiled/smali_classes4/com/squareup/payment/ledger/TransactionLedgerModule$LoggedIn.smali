.class public Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedIn;
.super Ljava/lang/Object;
.source "TransactionLedgerModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/payment/ledger/TransactionLedgerModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoggedIn"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideTransactionLedgerManager(Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;Ljava/lang/String;)Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 49
    invoke-interface {p0, p1}, Lcom/squareup/payment/ledger/TransactionLedgerManager$Factory;->create(Ljava/lang/String;)Lcom/squareup/payment/ledger/TransactionLedgerManager;

    move-result-object p0

    return-object p0
.end method
