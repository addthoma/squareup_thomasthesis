.class Lcom/squareup/payment/ledger/NullTransactionLedgerManager;
.super Ljava/lang/Object;
.source "NullTransactionLedgerManager.java"

# interfaces
.implements Lcom/squareup/payment/ledger/MaybeTransactionLedgerManager;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private supportedOperation(Ljava/lang/String;)Ljava/lang/Throwable;
    .locals 3

    .line 133
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No implementation for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public clearLedger()V
    .locals 0

    return-void
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public emailLedger(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public expireOldPayments()V
    .locals 0

    return-void
.end method

.method public logAddTenderBeforeAuth(Lcom/squareup/payment/tender/BaseTender;)V
    .locals 0

    return-void
.end method

.method public logAddTendersRequest(Lcom/squareup/protos/client/bills/AddTendersRequest;)V
    .locals 0

    return-void
.end method

.method public logAddTendersResponse(Lcom/squareup/protos/client/bills/AddTendersResponse;)V
    .locals 0

    return-void
.end method

.method public logCancelBillEnqueued(Lcom/squareup/protos/client/bills/CancelBillRequest;)V
    .locals 0

    return-void
.end method

.method public logCancelBillResponse(Lcom/squareup/protos/client/bills/CancelBillResponse;)V
    .locals 0

    return-void
.end method

.method public logCancelEnqueued(Lcom/squareup/queue/Cancel;)V
    .locals 0

    return-void
.end method

.method public logCaptureEnqueued(Lcom/squareup/queue/Capture;)V
    .locals 0

    return-void
.end method

.method public logCaptureFailed(Lcom/squareup/queue/Capture;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public logCaptureProcessed(Lcom/squareup/queue/Capture;)V
    .locals 0

    return-void
.end method

.method public logCaptureTenderRequest(Lcom/squareup/protos/client/bills/CaptureTendersRequest;)V
    .locals 0

    return-void
.end method

.method public logCaptureTenderResponse(Lcom/squareup/protos/client/bills/CaptureTendersResponse;)V
    .locals 0

    return-void
.end method

.method public logCompleteBillEnqueued(Lcom/squareup/protos/client/bills/CompleteBillRequest;)V
    .locals 0

    return-void
.end method

.method public logCompleteBillResponse(Lcom/squareup/protos/client/bills/CompleteBillResponse;)V
    .locals 0

    return-void
.end method

.method public logIssueRefundsRequest(Lcom/squareup/protos/client/bills/IssueRefundsRequest;)V
    .locals 0

    return-void
.end method

.method public logIssueRefundsResponse(Lcom/squareup/protos/client/bills/IssueRefundsResponse;)V
    .locals 0

    return-void
.end method

.method public logRemoveTendersRequest(Lcom/squareup/protos/client/bills/RemoveTendersRequest;)V
    .locals 0

    return-void
.end method

.method public logRemoveTendersResponse(Lcom/squareup/protos/client/bills/RemoveTendersResponse;)V
    .locals 0

    return-void
.end method

.method public logShowScreen(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public logStoreAndForwardBillFailed(Lcom/squareup/payment/offline/BillInFlight;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public logStoreAndForwardBillReady(Lcom/squareup/payment/offline/BillInFlight;Z)V
    .locals 0

    return-void
.end method

.method public logStoreAndForwardPaymentEnqueued(Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 0

    return-void
.end method

.method public logStoreAndForwardPaymentProcessed(Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 0

    return-void
.end method

.method public logStoreAndForwardTaskStatus(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public logVoidDanglingAuthorization(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public uploadLedger()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "uploadLedger"

    .line 42
    invoke-direct {p0, v0}, Lcom/squareup/payment/ledger/NullTransactionLedgerManager;->supportedOperation(Ljava/lang/String;)Ljava/lang/Throwable;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public uploadLedger(JJ)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation

    const-string p1, "uploadLedger between dates"

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/NullTransactionLedgerManager;->supportedOperation(Ljava/lang/String;)Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public uploadLedgerWithDiagnosticsData(Ljava/lang/String;Ljava/io/File;)Lio/reactivex/Single;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/io/File;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;",
            ">;"
        }
    .end annotation

    const-string p1, "uploadLedgerWithDiagnosticsData"

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/payment/ledger/NullTransactionLedgerManager;->supportedOperation(Ljava/lang/String;)Ljava/lang/Throwable;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->error(Ljava/lang/Throwable;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
