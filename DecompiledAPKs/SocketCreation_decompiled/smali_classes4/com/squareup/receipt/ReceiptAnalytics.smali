.class public Lcom/squareup/receipt/ReceiptAnalytics;
.super Ljava/lang/Object;
.source "ReceiptAnalytics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;
    }
.end annotation


# static fields
.field public static final DEFAULT:Z = true

.field public static final RESEND:Z = true

.field public static final SEND:Z = false

.field public static final TYPED:Z = false

.field private static final VALUE_EMAIL_ACTION:Ljava/lang/String; = "email"

.field private static final VALUE_NO_RECEIPT_ACTION:Ljava/lang/String; = "none"

.field private static final VALUE_PRINT_ACTION:Ljava/lang/String; = "print"

.field private static final VALUE_SMS_ACTION:Ljava/lang/String; = "sms"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/receipt/ReceiptAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public logEmail(ZZ)V
    .locals 3

    .line 27
    iget-object v0, p0, Lcom/squareup/receipt/ReceiptAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;

    const-string v2, "email"

    invoke-direct {v1, v2, p1, p2}, Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;-><init>(Ljava/lang/String;ZZ)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logNone()V
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/receipt/ReceiptAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;

    const/4 v2, 0x0

    const-string v3, "none"

    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;-><init>(Ljava/lang/String;ZZ)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logPrint(Z)V
    .locals 4

    .line 35
    iget-object v0, p0, Lcom/squareup/receipt/ReceiptAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;

    const-string v2, "print"

    const/4 v3, 0x0

    invoke-direct {v1, v2, p1, v3}, Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;-><init>(Ljava/lang/String;ZZ)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logSms(ZZ)V
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/receipt/ReceiptAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;

    const-string v2, "sms"

    invoke-direct {v1, v2, p1, p2}, Lcom/squareup/receipt/ReceiptAnalytics$ReceiptEvent;-><init>(Ljava/lang/String;ZZ)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public logTap(Lcom/squareup/analytics/RegisterTapName;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/receipt/ReceiptAnalytics;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method
