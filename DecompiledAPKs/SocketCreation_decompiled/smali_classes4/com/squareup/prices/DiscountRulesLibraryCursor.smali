.class public Lcom/squareup/prices/DiscountRulesLibraryCursor;
.super Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
.source "DiscountRulesLibraryCursor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/prices/DiscountRulesLibraryCursor$Factory;
    }
.end annotation


# instance fields
.field public final discountBundlesByDiscountIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/utils/DiscountBundle;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    .line 27
    iput-object p2, p0, Lcom/squareup/prices/DiscountRulesLibraryCursor;->discountBundlesByDiscountIds:Ljava/util/Map;

    return-void
.end method

.method private getDiscountBundle()Lcom/squareup/shared/catalog/utils/DiscountBundle;
    .locals 3

    .line 35
    invoke-virtual {p0}, Lcom/squareup/prices/DiscountRulesLibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v0, v1, :cond_1

    .line 39
    iget-object v0, p0, Lcom/squareup/prices/DiscountRulesLibraryCursor;->discountBundlesByDiscountIds:Ljava/util/Map;

    invoke-virtual {p0}, Lcom/squareup/prices/DiscountRulesLibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/utils/DiscountBundle;

    if-eqz v0, :cond_0

    return-object v0

    .line 41
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "discount bundle not found for LibraryEntry with ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    invoke-virtual {p0}, Lcom/squareup/prices/DiscountRulesLibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ".\n discountBundles dump: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/prices/DiscountRulesLibraryCursor;->discountBundlesByDiscountIds:Ljava/util/Map;

    .line 44
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LibraryEntry is not of type == DISCOUNT, LibraryEntry dump: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    invoke-virtual {p0}, Lcom/squareup/prices/DiscountRulesLibraryCursor;->getLibraryEntry()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public discountDescription()Ljava/lang/String;
    .locals 1

    .line 31
    invoke-direct {p0}, Lcom/squareup/prices/DiscountRulesLibraryCursor;->getDiscountBundle()Lcom/squareup/shared/catalog/utils/DiscountBundle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/utils/DiscountBundle;->description()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
