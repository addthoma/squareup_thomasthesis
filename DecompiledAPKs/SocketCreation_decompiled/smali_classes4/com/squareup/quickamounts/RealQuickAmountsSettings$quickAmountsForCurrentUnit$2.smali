.class final Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$2;
.super Ljava/lang/Object;
.source "RealQuickAmountsSettings.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/RealQuickAmountsSettings;->quickAmountsForCurrentUnit(Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0014\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00010\u00010\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$2;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/util/List;)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;",
            ">;)",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$2;->this$0:Lcom/squareup/quickamounts/RealQuickAmountsSettings;

    invoke-static {p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings;->access$createEmptyCatalogObject(Lcom/squareup/quickamounts/RealQuickAmountsSettings;)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/RealQuickAmountsSettings$quickAmountsForCurrentUnit$2;->apply(Ljava/util/List;)Lcom/squareup/shared/catalog/connectv2/models/CatalogQuickAmounts;

    move-result-object p1

    return-object p1
.end method
