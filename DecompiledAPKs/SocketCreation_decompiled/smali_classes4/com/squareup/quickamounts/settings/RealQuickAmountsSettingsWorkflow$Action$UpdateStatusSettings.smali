.class public final Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;
.super Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;
.source "RealQuickAmountsSettingsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdateStatusSettings"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0018\u0010\u0016\u001a\u00020\u0017*\u000e\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u0018H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;",
        "Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;",
        "newStatus",
        "Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/quickamounts/QuickAmountsStatus;Lcom/squareup/analytics/Analytics;)V",
        "getAnalytics",
        "()Lcom/squareup/analytics/Analytics;",
        "getNewStatus",
        "()Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;


# direct methods
.method public constructor <init>(Lcom/squareup/quickamounts/QuickAmountsStatus;Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string v0, "newStatus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 324
    invoke-direct {p0, v0}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    iput-object p2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;Lcom/squareup/quickamounts/QuickAmountsStatus;Lcom/squareup/analytics/Analytics;ILjava/lang/Object;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->analytics:Lcom/squareup/analytics/Analytics;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->copy(Lcom/squareup/quickamounts/QuickAmountsStatus;Lcom/squareup/analytics/Analytics;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;",
            "-",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 326
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState;

    .line 327
    instance-of v1, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$InitialState;

    if-eqz v1, :cond_0

    return-void

    .line 328
    :cond_0
    instance-of v1, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;

    invoke-virtual {v0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;->getAmounts()Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object v0

    goto :goto_0

    .line 329
    :cond_1
    instance-of v1, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$ItemsTutorialPrompt;

    if-eqz v1, :cond_6

    check-cast v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$ItemsTutorialPrompt;

    invoke-virtual {v0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$ItemsTutorialPrompt;->getAmounts()Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object v0

    .line 332
    :goto_0
    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    sget-object v2, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/quickamounts/QuickAmountsStatus;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    const/4 v7, 0x2

    if-eq v1, v2, :cond_4

    if-eq v1, v7, :cond_3

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    goto :goto_1

    .line 335
    :cond_2
    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsAnalyticsKt;->onClickedOff(Lcom/squareup/analytics/Analytics;)V

    goto :goto_1

    .line 334
    :cond_3
    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsAnalyticsKt;->onClickedSetAmounts(Lcom/squareup/analytics/Analytics;)V

    goto :goto_1

    .line 333
    :cond_4
    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsAnalyticsKt;->onClickedAutoAmounts(Lcom/squareup/analytics/Analytics;)V

    .line 338
    :goto_1
    iget-object v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, v0

    invoke-static/range {v1 .. v6}, Lcom/squareup/quickamounts/QuickAmounts;->copy$default(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/Map;ZILjava/lang/Object;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object v1

    .line 340
    invoke-virtual {v0}, Lcom/squareup/quickamounts/QuickAmounts;->getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object v0

    sget-object v2, Lcom/squareup/quickamounts/QuickAmountsStatus;->SET_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    if-ne v0, v2, :cond_5

    .line 341
    new-instance v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateSetAmounts;

    invoke-static {v1}, Lcom/squareup/quickamounts/settings/QuickAmountsFormattingKt;->filterZeroes(Lcom/squareup/quickamounts/QuickAmounts;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateSetAmounts;-><init>(Lcom/squareup/quickamounts/QuickAmounts;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_2

    .line 343
    :cond_5
    new-instance v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateStatus;

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-direct {v0, v2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsOutput$UpdateStatus;-><init>(Lcom/squareup/quickamounts/QuickAmountsStatus;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    .line 346
    :goto_2
    new-instance v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v7, v3}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsState$Loaded;-><init>(Lcom/squareup/quickamounts/QuickAmounts;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void

    .line 329
    :cond_6
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final component1()Lcom/squareup/quickamounts/QuickAmountsStatus;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    return-object v0
.end method

.method public final component2()Lcom/squareup/analytics/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->analytics:Lcom/squareup/analytics/Analytics;

    return-object v0
.end method

.method public final copy(Lcom/squareup/quickamounts/QuickAmountsStatus;Lcom/squareup/analytics/Analytics;)Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;
    .locals 1

    const-string v0, "newStatus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;

    invoke-direct {v0, p1, p2}, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;-><init>(Lcom/squareup/quickamounts/QuickAmountsStatus;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object p1, p1, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAnalytics()Lcom/squareup/analytics/Analytics;
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->analytics:Lcom/squareup/analytics/Analytics;

    return-object v0
.end method

.method public final getNewStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;
    .locals 1

    .line 322
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->analytics:Lcom/squareup/analytics/Analytics;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UpdateStatusSettings(newStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->newStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", analytics="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/RealQuickAmountsSettingsWorkflow$Action$UpdateStatusSettings;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
