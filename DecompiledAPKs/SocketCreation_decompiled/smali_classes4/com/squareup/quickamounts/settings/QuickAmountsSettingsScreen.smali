.class public final Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;
.super Ljava/lang/Object;
.source "QuickAmountsSettingsScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u001f\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0085\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e0\u0011\u0012\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000e0\u0011\u0012\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0002\u0010\u0015J\t\u0010&\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\t\u0010(\u001a\u00020\u0005H\u00c6\u0003J\u000f\u0010)\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0003J\t\u0010*\u001a\u00020\nH\u00c6\u0003J\t\u0010+\u001a\u00020\u0005H\u00c6\u0003J\u000f\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\u000f\u0010-\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\u0015\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e0\u0011H\u00c6\u0003J\u0015\u0010/\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000e0\u0011H\u00c6\u0003J\u009d\u0001\u00100\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00052\u000e\u0008\u0002\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u000e\u0008\u0002\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u0014\u0008\u0002\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e0\u00112\u0014\u0008\u0002\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000e0\u00112\u000e\u0008\u0002\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0001J\u0013\u00101\u001a\u00020\u00052\u0008\u00102\u001a\u0004\u0018\u000103H\u00d6\u0003J\t\u00104\u001a\u00020\u0013H\u00d6\u0001J\t\u00105\u001a\u000206H\u00d6\u0001R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u000b\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0019R\u0017\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u001d\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000e0\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001eR\u0017\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u001cR\u001d\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u000e0\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008 \u0010\u001eR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\"R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u0017\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010\u001c\u00a8\u00067"
    }
    d2 = {
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "quickAmountsStatus",
        "Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "eligibleForAutoAmounts",
        "",
        "amounts",
        "",
        "Lcom/squareup/quickamounts/settings/RecyclerEditableText;",
        "previewUiAmounts",
        "Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;",
        "focusLastRow",
        "onAddPressed",
        "Lkotlin/Function0;",
        "",
        "onBack",
        "onSelectedStatus",
        "Lkotlin/Function1;",
        "onAmountCleared",
        "",
        "updateSetAmountsSettings",
        "(Lcom/squareup/quickamounts/QuickAmountsStatus;ZLjava/util/List;Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V",
        "getAmounts",
        "()Ljava/util/List;",
        "getEligibleForAutoAmounts",
        "()Z",
        "getFocusLastRow",
        "getOnAddPressed",
        "()Lkotlin/jvm/functions/Function0;",
        "getOnAmountCleared",
        "()Lkotlin/jvm/functions/Function1;",
        "getOnBack",
        "getOnSelectedStatus",
        "getPreviewUiAmounts",
        "()Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;",
        "getQuickAmountsStatus",
        "()Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "getUpdateSetAmountsSettings",
        "component1",
        "component10",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/quickamounts/settings/RecyclerEditableText;",
            ">;"
        }
    .end annotation
.end field

.field private final eligibleForAutoAmounts:Z

.field private final focusLastRow:Z

.field private final onAddPressed:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onAmountCleared:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onBack:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onSelectedStatus:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final previewUiAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

.field private final quickAmountsStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

.field private final updateSetAmountsSettings:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/quickamounts/QuickAmountsStatus;ZLjava/util/List;Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/quickamounts/settings/RecyclerEditableText;",
            ">;",
            "Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;",
            "Z",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "quickAmountsStatus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amounts"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previewUiAmounts"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onAddPressed"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSelectedStatus"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onAmountCleared"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "updateSetAmountsSettings"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->quickAmountsStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    iput-boolean p2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->eligibleForAutoAmounts:Z

    iput-object p3, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->amounts:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->previewUiAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    iput-boolean p5, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->focusLastRow:Z

    iput-object p6, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAddPressed:Lkotlin/jvm/functions/Function0;

    iput-object p7, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    iput-object p8, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onSelectedStatus:Lkotlin/jvm/functions/Function1;

    iput-object p9, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAmountCleared:Lkotlin/jvm/functions/Function1;

    iput-object p10, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->updateSetAmountsSettings:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;Lcom/squareup/quickamounts/QuickAmountsStatus;ZLjava/util/List;Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;
    .locals 11

    move-object v0, p0

    move/from16 v1, p11

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->quickAmountsStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->eligibleForAutoAmounts:Z

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->amounts:Ljava/util/List;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->previewUiAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-boolean v6, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->focusLastRow:Z

    goto :goto_4

    :cond_4
    move/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAddPressed:Lkotlin/jvm/functions/Function0;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onSelectedStatus:Lkotlin/jvm/functions/Function1;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAmountCleared:Lkotlin/jvm/functions/Function1;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v1, v1, 0x200

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->updateSetAmountsSettings:Lkotlin/jvm/functions/Function0;

    goto :goto_9

    :cond_9
    move-object/from16 v1, p10

    :goto_9
    move-object p1, v2

    move p2, v3

    move-object p3, v4

    move-object p4, v5

    move/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v1

    invoke-virtual/range {p0 .. p10}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->copy(Lcom/squareup/quickamounts/QuickAmountsStatus;ZLjava/util/List;Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/quickamounts/QuickAmountsStatus;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->quickAmountsStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    return-object v0
.end method

.method public final component10()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->updateSetAmountsSettings:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->eligibleForAutoAmounts:Z

    return v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/quickamounts/settings/RecyclerEditableText;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->amounts:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->previewUiAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->focusLastRow:Z

    return v0
.end method

.method public final component6()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAddPressed:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component7()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final component8()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onSelectedStatus:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component9()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAmountCleared:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Lcom/squareup/quickamounts/QuickAmountsStatus;ZLjava/util/List;Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/quickamounts/settings/RecyclerEditableText;",
            ">;",
            "Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;",
            "Z",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;"
        }
    .end annotation

    const-string v0, "quickAmountsStatus"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amounts"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previewUiAmounts"

    move-object/from16 v5, p4

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onAddPressed"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onBack"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onSelectedStatus"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onAmountCleared"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "updateSetAmountsSettings"

    move-object/from16 v11, p10

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;

    move-object v1, v0

    move v3, p2

    move/from16 v6, p5

    invoke-direct/range {v1 .. v11}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;-><init>(Lcom/squareup/quickamounts/QuickAmountsStatus;ZLjava/util/List;Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->quickAmountsStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->quickAmountsStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->eligibleForAutoAmounts:Z

    iget-boolean v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->eligibleForAutoAmounts:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->amounts:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->amounts:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->previewUiAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->previewUiAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->focusLastRow:Z

    iget-boolean v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->focusLastRow:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAddPressed:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAddPressed:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onSelectedStatus:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onSelectedStatus:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAmountCleared:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAmountCleared:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->updateSetAmountsSettings:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->updateSetAmountsSettings:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmounts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/quickamounts/settings/RecyclerEditableText;",
            ">;"
        }
    .end annotation

    .line 10
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->amounts:Ljava/util/List;

    return-object v0
.end method

.method public final getEligibleForAutoAmounts()Z
    .locals 1

    .line 9
    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->eligibleForAutoAmounts:Z

    return v0
.end method

.method public final getFocusLastRow()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->focusLastRow:Z

    return v0
.end method

.method public final getOnAddPressed()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAddPressed:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnAmountCleared()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/Integer;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAmountCleared:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnBack()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnSelectedStatus()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onSelectedStatus:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getPreviewUiAmounts()Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->previewUiAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    return-object v0
.end method

.method public final getQuickAmountsStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->quickAmountsStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    return-object v0
.end method

.method public final getUpdateSetAmountsSettings()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->updateSetAmountsSettings:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->quickAmountsStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->eligibleForAutoAmounts:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->amounts:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->previewUiAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->focusLastRow:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :cond_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAddPressed:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_5
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_6
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onSelectedStatus:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAmountCleared:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_8
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->updateSetAmountsSettings:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_9
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "QuickAmountsSettingsScreen(quickAmountsStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->quickAmountsStatus:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", eligibleForAutoAmounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->eligibleForAutoAmounts:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->amounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", previewUiAmounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->previewUiAmounts:Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", focusLastRow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->focusLastRow:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", onAddPressed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAddPressed:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onBack="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onBack:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onSelectedStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onSelectedStatus:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onAmountCleared="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->onAmountCleared:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", updateSetAmountsSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsScreen;->updateSetAmountsSettings:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
