.class final Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "QuickAmountsSettingsLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Lcom/squareup/quickamounts/settings/TransactionLimitMoneyScrubber;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/cycler/StandardRowSpec$Creator<",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;",
        "Lcom/squareup/noho/NohoEditRow;",
        ">;",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001*\u0014\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;",
        "Lcom/squareup/noho/NohoEditRow;",
        "context",
        "Landroid/content/Context;",
        "invoke",
        "com/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$1$2$1",
        "com/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$row$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 50
    check-cast p1, Lcom/squareup/cycler/StandardRowSpec$Creator;

    check-cast p2, Landroid/content/Context;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;",
            "Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$RowData;",
            "Lcom/squareup/noho/NohoEditRow;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    sget v0, Lcom/squareup/quickamounts/settings/impl/R$layout;->row_quick_amounts_value:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->inflate(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->setView(Landroid/view/View;)V

    .line 86
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    iget-object v1, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;

    invoke-static {v1}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->access$getMoneyFormatter$p(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;)Lcom/squareup/text/Formatter;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;

    invoke-static {v2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->access$getCurrencyCode$p(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-static {v2, v3, v4}, Lcom/squareup/money/MoneyBuilderKt;->of(Lcom/squareup/protos/common/CurrencyCode;J)Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setHint(Ljava/lang/CharSequence;)V

    .line 87
    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    new-instance v1, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v2, p0, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;

    invoke-static {v2}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;->access$getScrubber$p(Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner;)Lcom/squareup/quickamounts/settings/TransactionLimitMoneyScrubber;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/SelectableTextScrubber;

    invoke-virtual {p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v1, v2, v3}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setScrubber(Lcom/squareup/text/ScrubbingTextWatcher;)V

    .line 88
    new-instance v0, Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {v0, p2, v2, v1, v2}, Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;-><init>(Landroid/content/Context;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 90
    new-instance p2, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;

    invoke-direct {p2, p1, v0}, Lcom/squareup/quickamounts/settings/QuickAmountsSettingsLayoutRunner$$special$$inlined$adopt$lambda$1$1;-><init>(Lcom/squareup/cycler/StandardRowSpec$Creator;Lcom/squareup/quickamounts/settings/ClearQuickAmountPlugin;)V

    check-cast p2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, p2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->bind(Lkotlin/jvm/functions/Function2;)V

    return-void

    .line 85
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.noho.NohoEditRow"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
