.class final Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility$canShow$1;
.super Ljava/lang/Object;
.source "RealQuickAmountsTutorialVisibility.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;->canShow()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealQuickAmountsTutorialVisibility.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealQuickAmountsTutorialVisibility.kt\ncom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility$canShow$1\n*L\n1#1,31:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility$canShow$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility$canShow$1;

    invoke-direct {v0}, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility$canShow$1;-><init>()V

    sput-object v0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility$canShow$1;->INSTANCE:Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility$canShow$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/quickamounts/QuickAmounts;

    invoke-virtual {p0, p1}, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility$canShow$1;->apply(Lcom/squareup/quickamounts/QuickAmounts;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lcom/squareup/quickamounts/QuickAmounts;)Z
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmounts;->getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object v0

    sget-object v1, Lcom/squareup/quickamounts/QuickAmountsStatus;->AUTO_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmounts;->getAmounts()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v2

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method
