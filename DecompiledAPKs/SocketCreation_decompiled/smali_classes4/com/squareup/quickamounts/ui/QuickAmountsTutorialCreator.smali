.class public abstract Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;
.super Lcom/squareup/tutorialv2/TutorialCreator;
.source "QuickAmountsTutorialCreator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008&\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0008\u0010\u0007\u001a\u00020\u0004H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
        "Lcom/squareup/tutorialv2/TutorialCreator;",
        "()V",
        "activate",
        "",
        "startMode",
        "Lcom/squareup/quickamounts/ui/StartMode;",
        "deactivate",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/tutorialv2/TutorialCreator;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract activate(Lcom/squareup/quickamounts/ui/StartMode;)V
.end method

.method public abstract deactivate()V
.end method
