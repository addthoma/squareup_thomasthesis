.class public final Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;
.super Ljava/lang/Object;
.source "RealQuickAmountsTutorialVisibility.kt"

# interfaces
.implements Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealQuickAmountsTutorialVisibility.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealQuickAmountsTutorialVisibility.kt\ncom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,31:1\n57#2,4:32\n*E\n*S KotlinDebug\n*F\n+ 1 RealQuickAmountsTutorialVisibility.kt\ncom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility\n*L\n23#1,4:32\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "quickAmountsSettings",
        "Lcom/squareup/quickamounts/QuickAmountsSettings;",
        "orderEntryApplet",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V",
        "canShow",
        "Lio/reactivex/Observable;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final orderEntryApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quickAmountsSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryApplet"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    iput-object p3, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;->orderEntryApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    return-void
.end method


# virtual methods
.method public canShow()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 19
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;->orderEntryApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->hasOrderEntryApplet()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 20
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.just(false)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 23
    :cond_0
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 24
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    invoke-interface {v0}, Lcom/squareup/quickamounts/QuickAmountsSettings;->amounts()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility$canShow$1;->INSTANCE:Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility$canShow$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "quickAmountsSettings.amo\u2026ts.isNotEmpty()\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v1, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->QUICK_AMOUNTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "features.featureEnabled(QUICK_AMOUNTS)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 34
    new-instance v2, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility$canShow$$inlined$combineLatest$1;

    invoke-direct {v2}, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility$canShow$$inlined$combineLatest$1;-><init>()V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    .line 32
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
