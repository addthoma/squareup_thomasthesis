.class final Lcom/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$$inlined$forEach$lambda$1;
.super Ljava/lang/Object;
.source "QuickAmountsView.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/quickamounts/ui/QuickAmountsView;->setQuickAmounts(Lcom/squareup/quickamounts/ui/QuickAmountsScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick",
        "com/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $amountView:Landroid/widget/TextView;

.field final synthetic $maxTextSize$inlined:I

.field final synthetic $minTextSize$inlined:I

.field final synthetic $typeface$inlined:Landroid/graphics/Typeface;

.field final synthetic $value:Lcom/squareup/protos/connect/v2/common/Money;

.field final synthetic this$0:Lcom/squareup/quickamounts/ui/QuickAmountsView;


# direct methods
.method constructor <init>(Lcom/squareup/protos/connect/v2/common/Money;Landroid/widget/TextView;Lcom/squareup/quickamounts/ui/QuickAmountsView;Landroid/graphics/Typeface;II)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$$inlined$forEach$lambda$1;->$value:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$$inlined$forEach$lambda$1;->$amountView:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$$inlined$forEach$lambda$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsView;

    iput-object p4, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$$inlined$forEach$lambda$1;->$typeface$inlined:Landroid/graphics/Typeface;

    iput p5, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$$inlined$forEach$lambda$1;->$minTextSize$inlined:I

    iput p6, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$$inlined$forEach$lambda$1;->$maxTextSize$inlined:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3

    .line 130
    iget-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$$inlined$forEach$lambda$1;->this$0:Lcom/squareup/quickamounts/ui/QuickAmountsView;

    invoke-static {p1}, Lcom/squareup/quickamounts/ui/QuickAmountsView;->access$getQuickAmountClicked$p(Lcom/squareup/quickamounts/ui/QuickAmountsView;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    new-instance v0, Lcom/squareup/quickamounts/ui/ClickEvent;

    iget-object v1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$$inlined$forEach$lambda$1;->$value:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsView$setQuickAmounts$$inlined$forEach$lambda$1;->$amountView:Landroid/widget/TextView;

    check-cast v2, Landroid/view/View;

    invoke-direct {v0, v1, v2}, Lcom/squareup/quickamounts/ui/ClickEvent;-><init>(Lcom/squareup/protos/connect/v2/common/Money;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
