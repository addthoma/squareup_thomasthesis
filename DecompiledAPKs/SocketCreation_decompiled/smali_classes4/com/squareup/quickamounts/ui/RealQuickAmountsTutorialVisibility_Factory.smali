.class public final Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;
.super Ljava/lang/Object;
.source "RealQuickAmountsTutorialVisibility_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/QuickAmountsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;)",
            "Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/orderentry/OrderEntryAppletGateway;)Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/quickamounts/QuickAmountsSettings;

    iget-object v2, p0, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-static {v0, v1, v2}, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/orderentry/OrderEntryAppletGateway;)Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility_Factory;->get()Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;

    move-result-object v0

    return-object v0
.end method
