.class public abstract Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule;
.super Ljava/lang/Object;
.source "QuickAmountsTutorialModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\'\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH!\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule;",
        "",
        "()V",
        "bindQuickAmountsMonitorAsScoped",
        "Lmortar/Scoped;",
        "quickAmountsTutorialMonitor",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;",
        "bindQuickAmountsTutorialCreator",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
        "realQuickAmountsTutorialCreator",
        "Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;",
        "bindQuickAmountsTutorialVisibility",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;",
        "realQuickAmountsTutorialVisibility",
        "Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;",
        "Companion",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule;->Companion:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindQuickAmountsMonitorAsScoped(Lcom/squareup/quickamounts/ui/QuickAmountsTutorialMonitor;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForMainActivity;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method public abstract bindQuickAmountsTutorialCreator(Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialCreator;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindQuickAmountsTutorialVisibility(Lcom/squareup/quickamounts/ui/RealQuickAmountsTutorialVisibility;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialVisibility;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
