.class public final Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;
.super Ljava/lang/Object;
.source "QuickAmountsTutorialFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u000e\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;",
        "",
        "quickAmountsSettings",
        "Lcom/squareup/quickamounts/QuickAmountsSettings;",
        "appletsDrawerRunner",
        "Lcom/squareup/applet/AppletsDrawerRunner;",
        "appIdling",
        "Lcom/squareup/ui/main/AppIdling;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/analytics/Analytics;)V",
        "forStartMode",
        "Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;",
        "startMode",
        "Lcom/squareup/quickamounts/ui/StartMode;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appIdling:Lcom/squareup/ui/main/AppIdling;

.field private final appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

.field private final quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "quickAmountsSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appletsDrawerRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appIdling"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    iput-object p2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    iput-object p3, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;->appIdling:Lcom/squareup/ui/main/AppIdling;

    iput-object p4, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public final forStartMode(Lcom/squareup/quickamounts/ui/StartMode;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;
    .locals 7

    const-string v0, "startMode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    new-instance v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;

    iget-object v2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;->quickAmountsSettings:Lcom/squareup/quickamounts/QuickAmountsSettings;

    iget-object v3, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    iget-object v5, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;->appIdling:Lcom/squareup/ui/main/AppIdling;

    .line 18
    iget-object v6, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialFactory;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, v0

    move-object v4, p1

    .line 17
    invoke-direct/range {v1 .. v6}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorial;-><init>(Lcom/squareup/quickamounts/QuickAmountsSettings;Lcom/squareup/applet/AppletsDrawerRunner;Lcom/squareup/quickamounts/ui/StartMode;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method
