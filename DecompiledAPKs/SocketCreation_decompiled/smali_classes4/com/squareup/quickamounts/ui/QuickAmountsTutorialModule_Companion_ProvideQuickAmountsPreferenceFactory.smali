.class public final Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule_Companion_ProvideQuickAmountsPreferenceFactory;
.super Ljava/lang/Object;
.source "QuickAmountsTutorialModule_Companion_ProvideQuickAmountsPreferenceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/f2prateek/rx/preferences2/Preference<",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule_Companion_ProvideQuickAmountsPreferenceFactory;->preferencesProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule_Companion_ProvideQuickAmountsPreferenceFactory;->gsonProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule_Companion_ProvideQuickAmountsPreferenceFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule_Companion_ProvideQuickAmountsPreferenceFactory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule_Companion_ProvideQuickAmountsPreferenceFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule_Companion_ProvideQuickAmountsPreferenceFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideQuickAmountsPreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/google/gson/Gson;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ">;"
        }
    .end annotation

    .line 43
    sget-object v0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule;->Companion:Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule$Companion;->provideQuickAmountsPreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/google/gson/Gson;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/f2prateek/rx/preferences2/Preference;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/quickamounts/QuickAmounts;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule_Companion_ProvideQuickAmountsPreferenceFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    iget-object v1, p0, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule_Companion_ProvideQuickAmountsPreferenceFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/gson/Gson;

    invoke-static {v0, v1}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule_Companion_ProvideQuickAmountsPreferenceFactory;->provideQuickAmountsPreference(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/google/gson/Gson;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule_Companion_ProvideQuickAmountsPreferenceFactory;->get()Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method
