.class public final Lcom/squareup/quickamounts/QuickAmounts;
.super Ljava/lang/Object;
.source "QuickAmounts.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/quickamounts/QuickAmounts$Creator;,
        Lcom/squareup/quickamounts/QuickAmounts$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nQuickAmounts.kt\nKotlin\n*S Kotlin\n*F\n+ 1 QuickAmounts.kt\ncom/squareup/quickamounts/QuickAmounts\n*L\n1#1,45:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0012\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0087\u0008\u0018\u0000 )2\u00020\u0001:\u0001)B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0018\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u00020\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u0005\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\u001b\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u00020\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u0005H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\tH\u00c6\u0003J9\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u001a\u0008\u0002\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u00020\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\u0013\u0010\u001d\u001a\u00020\t2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u00d6\u0003J\t\u0010 \u001a\u00020\u001cH\u00d6\u0001J\u001e\u0010!\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006J\t\u0010\"\u001a\u00020#H\u00d6\u0001J\u0019\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020\u001cH\u00d6\u0001R#\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u00020\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0017\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u0017\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0011\u0010\u000fR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0012R\u0017\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "Landroid/os/Parcelable;",
        "status",
        "Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "allAmounts",
        "",
        "",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "isEligibleForAutoAmounts",
        "",
        "(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/Map;Z)V",
        "getAllAmounts",
        "()Ljava/util/Map;",
        "amounts",
        "getAmounts",
        "()Ljava/util/List;",
        "autoAmounts",
        "getAutoAmounts",
        "()Z",
        "setAmounts",
        "getSetAmounts",
        "getStatus",
        "()Lcom/squareup/quickamounts/QuickAmountsStatus;",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "replaceAmounts",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final Companion:Lcom/squareup/quickamounts/QuickAmounts$Companion;

.field private static final EMPTY:Lcom/squareup/quickamounts/QuickAmounts;


# instance fields
.field private final allAmounts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final isEligibleForAutoAmounts:Z

.field private final status:Lcom/squareup/quickamounts/QuickAmountsStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/squareup/quickamounts/QuickAmounts$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/quickamounts/QuickAmounts$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/quickamounts/QuickAmounts;->Companion:Lcom/squareup/quickamounts/QuickAmounts$Companion;

    .line 42
    new-instance v0, Lcom/squareup/quickamounts/QuickAmounts;

    sget-object v1, Lcom/squareup/quickamounts/QuickAmountsStatus;->DISABLED:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/quickamounts/QuickAmounts;-><init>(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/Map;Z)V

    sput-object v0, Lcom/squareup/quickamounts/QuickAmounts;->EMPTY:Lcom/squareup/quickamounts/QuickAmounts;

    new-instance v0, Lcom/squareup/quickamounts/QuickAmounts$Creator;

    invoke-direct {v0}, Lcom/squareup/quickamounts/QuickAmounts$Creator;-><init>()V

    sput-object v0, Lcom/squareup/quickamounts/QuickAmounts;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/Map;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/Map<",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;>;Z)V"
        }
    .end annotation

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allAmounts"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/quickamounts/QuickAmounts;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    iput-object p2, p0, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    iput-boolean p3, p0, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts:Z

    return-void
.end method

.method public static final synthetic access$getEMPTY$cp()Lcom/squareup/quickamounts/QuickAmounts;
    .locals 1

    .line 15
    sget-object v0, Lcom/squareup/quickamounts/QuickAmounts;->EMPTY:Lcom/squareup/quickamounts/QuickAmounts;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/Map;ZILjava/lang/Object;)Lcom/squareup/quickamounts/QuickAmounts;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/quickamounts/QuickAmounts;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/quickamounts/QuickAmounts;->copy(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/Map;Z)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic replaceAmounts$default(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/quickamounts/QuickAmounts;
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    .line 35
    iget-object p1, p0, Lcom/squareup/quickamounts/QuickAmounts;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/quickamounts/QuickAmounts;->replaceAmounts(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/quickamounts/QuickAmountsStatus;
    .locals 1

    iget-object v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    return-object v0
.end method

.method public final component2()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    return-object v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts:Z

    return v0
.end method

.method public final copy(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/Map;Z)Lcom/squareup/quickamounts/QuickAmounts;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/Map<",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;>;Z)",
            "Lcom/squareup/quickamounts/QuickAmounts;"
        }
    .end annotation

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allAmounts"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/quickamounts/QuickAmounts;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/quickamounts/QuickAmounts;-><init>(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/Map;Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/quickamounts/QuickAmounts;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/quickamounts/QuickAmounts;

    iget-object v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    iget-object v1, p1, Lcom/squareup/quickamounts/QuickAmounts;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts:Z

    iget-boolean p1, p1, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAllAmounts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;>;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    return-object v0
.end method

.method public final getAmounts()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/quickamounts/QuickAmounts;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final getAutoAmounts()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    sget-object v1, Lcom/squareup/quickamounts/QuickAmountsStatus;->AUTO_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final getSetAmounts()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    sget-object v1, Lcom/squareup/quickamounts/QuickAmountsStatus;->SET_AMOUNTS:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isEligibleForAutoAmounts()Z
    .locals 1

    .line 18
    iget-boolean v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts:Z

    return v0
.end method

.method public final replaceAmounts(Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/List;)Lcom/squareup/quickamounts/QuickAmounts;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/quickamounts/QuickAmountsStatus;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            ">;)",
            "Lcom/squareup/quickamounts/QuickAmounts;"
        }
    .end annotation

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amounts"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    invoke-static {p1, p2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v3

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-static/range {v1 .. v6}, Lcom/squareup/quickamounts/QuickAmounts;->copy$default(Lcom/squareup/quickamounts/QuickAmounts;Lcom/squareup/quickamounts/QuickAmountsStatus;Ljava/util/Map;ZILjava/lang/Object;)Lcom/squareup/quickamounts/QuickAmounts;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "QuickAmounts(status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/QuickAmounts;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", allAmounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isEligibleForAutoAmounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/quickamounts/QuickAmounts;->status:Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/quickamounts/QuickAmounts;->allAmounts:Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/quickamounts/QuickAmountsStatus;

    invoke-virtual {v1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    goto :goto_0

    :cond_1
    iget-boolean p2, p0, Lcom/squareup/quickamounts/QuickAmounts;->isEligibleForAutoAmounts:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
