.class Lcom/squareup/rootview/RootLayoutTransition;
.super Landroid/animation/LayoutTransition;
.source "RootLayoutTransition.java"


# static fields
.field private static final DISAPPEARING_DURATION:I = 0x1f4


# direct methods
.method constructor <init>()V
    .locals 4

    .line 22
    invoke-direct {p0}, Landroid/animation/LayoutTransition;-><init>()V

    const/4 v0, 0x2

    .line 23
    invoke-virtual {p0, v0}, Lcom/squareup/rootview/RootLayoutTransition;->disableTransitionType(I)V

    const/4 v0, 0x3

    const-wide/16 v1, 0x1f4

    .line 27
    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/rootview/RootLayoutTransition;->setDuration(IJ)V

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    .line 28
    invoke-virtual {p0, v2, v0, v1}, Lcom/squareup/rootview/RootLayoutTransition;->setStartDelay(IJ)V

    const/4 v3, 0x1

    .line 29
    invoke-virtual {p0, v3, v0, v1}, Lcom/squareup/rootview/RootLayoutTransition;->setStartDelay(IJ)V

    .line 30
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p0, v2, v0}, Lcom/squareup/rootview/RootLayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    .line 31
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {p0, v3, v0}, Lcom/squareup/rootview/RootLayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    return-void
.end method
