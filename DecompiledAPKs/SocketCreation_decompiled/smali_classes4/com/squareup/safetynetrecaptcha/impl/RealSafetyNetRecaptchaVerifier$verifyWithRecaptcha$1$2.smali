.class final Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$2;
.super Ljava/lang/Object;
.source "RealSafetyNetRecaptchaVerifier.kt"

# interfaces
.implements Lcom/google/android/gms/tasks/OnFailureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;->subscribe(Lio/reactivex/SingleEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "error",
        "Ljava/lang/Exception;",
        "onFailure"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $emitter:Lio/reactivex/SingleEmitter;

.field final synthetic this$0:Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;


# direct methods
.method constructor <init>(Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;Lio/reactivex/SingleEmitter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$2;->this$0:Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;

    iput-object p2, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$2;->$emitter:Lio/reactivex/SingleEmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Exception;)V
    .locals 4

    const-string v0, "error"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    instance-of v0, p1, Lcom/google/android/gms/common/api/ApiException;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    move-object v0, p1

    :goto_0
    check-cast v0, Lcom/google/android/gms/common/api/ApiException;

    .line 54
    iget-object v2, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$2;->this$0:Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;

    iget-object v2, v2, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1;->this$0:Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;

    invoke-static {v2}, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;->access$getAnalytics$p(Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;)Lcom/squareup/analytics/Analytics;

    move-result-object v2

    new-instance v3, Lcom/squareup/safetynetrecaptcha/impl/CaptchaFailedEvent;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/ApiException;->getStatusCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :cond_1
    invoke-direct {v3, p1, v1}, Lcom/squareup/safetynetrecaptcha/impl/CaptchaFailedEvent;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    check-cast v3, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    if-eqz v0, :cond_2

    .line 56
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/ApiException;->getStatusCode()I

    move-result p1

    const/4 v0, 0x7

    if-ne p1, v0, :cond_2

    .line 57
    iget-object p1, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$2;->$emitter:Lio/reactivex/SingleEmitter;

    sget-object v0, Lcom/squareup/safetynetrecaptcha/CaptchaResult$NetworkError;->INSTANCE:Lcom/squareup/safetynetrecaptcha/CaptchaResult$NetworkError;

    invoke-interface {p1, v0}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void

    .line 61
    :cond_2
    iget-object p1, p0, Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier$verifyWithRecaptcha$1$2;->$emitter:Lio/reactivex/SingleEmitter;

    sget-object v0, Lcom/squareup/safetynetrecaptcha/CaptchaResult$Error;->INSTANCE:Lcom/squareup/safetynetrecaptcha/CaptchaResult$Error;

    invoke-interface {p1, v0}, Lio/reactivex/SingleEmitter;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method
