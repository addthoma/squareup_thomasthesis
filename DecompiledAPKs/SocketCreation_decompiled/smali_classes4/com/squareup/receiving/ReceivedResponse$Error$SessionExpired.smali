.class public final Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;
.super Lcom/squareup/receiving/ReceivedResponse$Error;
.source "ReceivedResponse.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/receiving/ReceivedResponse$Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SessionExpired"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/receiving/ReceivedResponse$Error<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0006\u0008\u0002\u0010\u0001 \u00012\u0008\u0012\u0004\u0012\u0002H\u00010\u0002B\u000f\u0012\u0008\u0010\u0003\u001a\u0004\u0018\u00018\u0002\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0008\u001a\u0004\u0018\u00018\u0002H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0006J \u0010\t\u001a\u0008\u0012\u0004\u0012\u00028\u00020\u00002\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00018\u0002H\u00c6\u0001\u00a2\u0006\u0002\u0010\nJ\u0013\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\r\u001a\u0004\u0018\u00010\u000eH\u00d6\u0003J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J+\u0010\u0011\u001a\u0002H\u0012\"\u0004\u0008\u0003\u0010\u00122\u0016\u0010\u0013\u001a\u0012\u0012\u0006\u0008\u0000\u0012\u00028\u0002\u0012\u0006\u0008\u0001\u0012\u0002H\u00120\u0014H\u0016\u00a2\u0006\u0002\u0010\u0015J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0015\u0010\u0003\u001a\u0004\u0018\u00018\u0002\u00a2\u0006\n\n\u0002\u0010\u0007\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;",
        "T",
        "Lcom/squareup/receiving/ReceivedResponse$Error;",
        "response",
        "(Ljava/lang/Object;)V",
        "getResponse",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "component1",
        "copy",
        "(Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "map",
        "M",
        "javaMapper",
        "Lcom/squareup/receiving/ReceivedMapper;",
        "(Lcom/squareup/receiving/ReceivedMapper;)Ljava/lang/Object;",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final response:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 89
    invoke-direct {p0, v0, v1}, Lcom/squareup/receiving/ReceivedResponse$Error;-><init>(ZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;->response:Ljava/lang/Object;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;->response:Ljava/lang/Object;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;->copy(Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;->response:Ljava/lang/Object;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired<",
            "TT;>;"
        }
    .end annotation

    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    invoke-direct {v0, p1}, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    iget-object v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;->response:Ljava/lang/Object;

    iget-object p1, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;->response:Ljava/lang/Object;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getResponse()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 89
    iget-object v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;->response:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;->response:Ljava/lang/Object;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public map(Lcom/squareup/receiving/ReceivedMapper;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/ReceivedMapper<",
            "-TT;+TM;>;)TM;"
        }
    .end annotation

    const-string v0, "javaMapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-interface {p1}, Lcom/squareup/receiving/ReceivedMapper;->isSessionExpired()Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SessionExpired(response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;->response:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
