.class public final Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;
.super Lcom/squareup/receiving/ReceivedResponse$Error;
.source "ReceivedResponse.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/receiving/ReceivedResponse$Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ServerError"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\t\u0010\u0008\u001a\u00020\u0004H\u00c6\u0003J\u0013\u0010\t\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u0004H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u0004H\u00d6\u0001J+\u0010\u000f\u001a\u0002H\u0010\"\u0004\u0008\u0002\u0010\u00102\u0016\u0010\u0011\u001a\u0012\u0012\u0006\u0008\u0000\u0012\u00020\u0002\u0012\u0006\u0008\u0001\u0012\u0002H\u00100\u0012H\u0016\u00a2\u0006\u0002\u0010\u0013J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;",
        "Lcom/squareup/receiving/ReceivedResponse$Error;",
        "",
        "statusCode",
        "",
        "(I)V",
        "getStatusCode",
        "()I",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "map",
        "M",
        "javaMapper",
        "Lcom/squareup/receiving/ReceivedMapper;",
        "(Lcom/squareup/receiving/ReceivedMapper;)Ljava/lang/Object;",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final statusCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 67
    invoke-direct {p0, v0, v1}, Lcom/squareup/receiving/ReceivedResponse$Error;-><init>(ZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->statusCode:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;IILjava/lang/Object;)Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget p1, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->statusCode:I

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->copy(I)Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->statusCode:I

    return v0
.end method

.method public final copy(I)Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;
    .locals 1

    new-instance v0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    invoke-direct {v0, p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;-><init>(I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    iget v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->statusCode:I

    iget p1, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->statusCode:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getStatusCode()I
    .locals 1

    .line 67
    iget v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->statusCode:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->statusCode:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    return v0
.end method

.method public map(Lcom/squareup/receiving/ReceivedMapper;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/ReceivedMapper;",
            ")TM;"
        }
    .end annotation

    const-string v0, "javaMapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget v0, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->statusCode:I

    invoke-interface {p1, v0}, Lcom/squareup/receiving/ReceivedMapper;->isServerError(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ServerError(statusCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->statusCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
