.class public final synthetic Lcom/squareup/permissions/-$$Lambda$PasscodesSettings$g72bvcl4kzOdBIrIS86UrkKhpOE;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/BiConsumer;


# instance fields
.field private final synthetic f$0:Lcom/squareup/permissions/PasscodesSettings;

.field private final synthetic f$1:Lcom/squareup/permissions/PasscodesSettings$State;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/permissions/PasscodesSettings$State;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/permissions/-$$Lambda$PasscodesSettings$g72bvcl4kzOdBIrIS86UrkKhpOE;->f$0:Lcom/squareup/permissions/PasscodesSettings;

    iput-object p2, p0, Lcom/squareup/permissions/-$$Lambda$PasscodesSettings$g72bvcl4kzOdBIrIS86UrkKhpOE;->f$1:Lcom/squareup/permissions/PasscodesSettings$State;

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    iget-object v0, p0, Lcom/squareup/permissions/-$$Lambda$PasscodesSettings$g72bvcl4kzOdBIrIS86UrkKhpOE;->f$0:Lcom/squareup/permissions/PasscodesSettings;

    iget-object v1, p0, Lcom/squareup/permissions/-$$Lambda$PasscodesSettings$g72bvcl4kzOdBIrIS86UrkKhpOE;->f$1:Lcom/squareup/permissions/PasscodesSettings$State;

    check-cast p1, Lcom/squareup/server/employees/SetPasscodeResponse;

    check-cast p2, Lcom/squareup/permissions/PasscodesSettings$State;

    invoke-virtual {v0, v1, p1, p2}, Lcom/squareup/permissions/PasscodesSettings;->lambda$setTeamPasscode$0$PasscodesSettings(Lcom/squareup/permissions/PasscodesSettings$State;Lcom/squareup/server/employees/SetPasscodeResponse;Lcom/squareup/permissions/PasscodesSettings$State;)V

    return-void
.end method
