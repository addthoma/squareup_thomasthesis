.class public final Lcom/squareup/permissions/EmployeeModel$Factory;
.super Ljava/lang/Object;
.source "EmployeeModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/EmployeeModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/permissions/EmployeeModel;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final creator:Lcom/squareup/permissions/EmployeeModel$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/permissions/EmployeeModel$Creator<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/EmployeeModel$Creator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/permissions/EmployeeModel$Creator<",
            "TT;>;)V"
        }
    .end annotation

    .line 262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 263
    iput-object p1, p0, Lcom/squareup/permissions/EmployeeModel$Factory;->creator:Lcom/squareup/permissions/EmployeeModel$Creator;

    return-void
.end method


# virtual methods
.method public get_all_employees()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 4

    .line 268
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "employees_table"

    const-string v3, "employee_permissions_table"

    filled-new-array {v2, v3}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT token, first_name, last_name, employee_number, hashed_passcode, salt,\n       iterations, timecard_id, clockin_time, active, can_access_register_with_passcode,\n       can_track_time, is_account_owner, is_owner, role_token, group_concat(permission)\nFROM employees_table\nLEFT JOIN employee_permissions_table ON (token = employee_token)\nGROUP BY token"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public get_all_employeesMapper(Lcom/squareup/permissions/EmployeeModel$Get_all_employeesCreator;)Lcom/squareup/permissions/EmployeeModel$Get_all_employeesMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R::",
            "Lcom/squareup/permissions/EmployeeModel$Get_all_employeesModel;",
            ">(",
            "Lcom/squareup/permissions/EmployeeModel$Get_all_employeesCreator<",
            "TR;>;)",
            "Lcom/squareup/permissions/EmployeeModel$Get_all_employeesMapper<",
            "TR;>;"
        }
    .end annotation

    .line 280
    new-instance v0, Lcom/squareup/permissions/EmployeeModel$Get_all_employeesMapper;

    invoke-direct {v0, p1}, Lcom/squareup/permissions/EmployeeModel$Get_all_employeesMapper;-><init>(Lcom/squareup/permissions/EmployeeModel$Get_all_employeesCreator;)V

    return-object v0
.end method
