.class public abstract Lcom/squareup/permissions/PermissionGatekeeper$When;
.super Ljava/lang/Object;
.source "PermissionGatekeeper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/PermissionGatekeeper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "When"
.end annotation


# static fields
.field private static final INVALID_EMPLOYEE:Lcom/squareup/permissions/Employee;

.field private static final INVALID_EMPLOYEE_TOKEN:Ljava/lang/String; = "INVALID_EMPLOYEE_TOKEN"


# instance fields
.field public authorizedEmployee:Lcom/squareup/permissions/Employee;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 53
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;-><init>()V

    const-string v1, "INVALID_EMPLOYEE_TOKEN"

    .line 55
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->token(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 56
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->active(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 57
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_access_register_with_passcode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->can_track_time(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    .line 59
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_account_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    .line 60
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->is_owner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object v0

    .line 54
    invoke-static {v0}, Lcom/squareup/permissions/Employee;->fromEmployeesEntity(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/permissions/Employee;

    move-result-object v0

    sput-object v0, Lcom/squareup/permissions/PermissionGatekeeper$When;->INVALID_EMPLOYEE:Lcom/squareup/permissions/Employee;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    sget-object v0, Lcom/squareup/permissions/PermissionGatekeeper$When;->INVALID_EMPLOYEE:Lcom/squareup/permissions/Employee;

    iput-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$When;->authorizedEmployee:Lcom/squareup/permissions/Employee;

    return-void
.end method


# virtual methods
.method public failure()V
    .locals 0

    return-void
.end method

.method protected getAuthorizedEmployee()Lcom/squareup/permissions/Employee;
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$When;->authorizedEmployee:Lcom/squareup/permissions/Employee;

    sget-object v1, Lcom/squareup/permissions/PermissionGatekeeper$When;->INVALID_EMPLOYEE:Lcom/squareup/permissions/Employee;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Should only use within When.success()."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$When;->authorizedEmployee:Lcom/squareup/permissions/Employee;

    return-object v0
.end method

.method protected getAuthorizedEmployeeToken()Ljava/lang/String;
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$When;->authorizedEmployee:Lcom/squareup/permissions/Employee;

    sget-object v1, Lcom/squareup/permissions/PermissionGatekeeper$When;->INVALID_EMPLOYEE:Lcom/squareup/permissions/Employee;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Should only use within When.success()."

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$When;->authorizedEmployee:Lcom/squareup/permissions/Employee;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/permissions/Employee;->token()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    return-object v0
.end method

.method resetAuthorizedEmployee()V
    .locals 1

    .line 102
    sget-object v0, Lcom/squareup/permissions/PermissionGatekeeper$When;->INVALID_EMPLOYEE:Lcom/squareup/permissions/Employee;

    iput-object v0, p0, Lcom/squareup/permissions/PermissionGatekeeper$When;->authorizedEmployee:Lcom/squareup/permissions/Employee;

    return-void
.end method

.method public abstract success()V
.end method
