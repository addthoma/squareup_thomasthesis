.class public Lcom/squareup/permissions/PasscodeAuthorizationEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "PasscodeAuthorizationEvent.java"


# instance fields
.field public final action_to_authorize:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 12
    iput-object p2, p0, Lcom/squareup/permissions/PasscodeAuthorizationEvent;->action_to_authorize:Ljava/lang/String;

    return-void
.end method
