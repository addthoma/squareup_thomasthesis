.class public Lcom/squareup/permissions/Employees;
.super Ljava/lang/Object;
.source "Employees.java"


# instance fields
.field private final store:Lcom/squareup/permissions/EmployeesStore;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/EmployeesStore;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/permissions/Employees;->store:Lcom/squareup/permissions/EmployeesStore;

    return-void
.end method

.method private filterEmployeesBy(Lio/reactivex/functions/Predicate;)Lio/reactivex/functions/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/functions/Predicate<",
            "Lcom/squareup/permissions/Employee;",
            ">;)",
            "Lio/reactivex/functions/Function<",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation

    .line 109
    new-instance v0, Lcom/squareup/permissions/-$$Lambda$Employees$wZQZWFb6HMR88SBM9FP726aNVko;

    invoke-direct {v0, p1}, Lcom/squareup/permissions/-$$Lambda$Employees$wZQZWFb6HMR88SBM9FP726aNVko;-><init>(Lio/reactivex/functions/Predicate;)V

    return-object v0
.end method

.method static synthetic lambda$activeEmployees$0(Lcom/squareup/permissions/Employee;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 34
    iget-boolean p0, p0, Lcom/squareup/permissions/Employee;->active:Z

    return p0
.end method

.method static synthetic lambda$activeEmployeesWithAnyPermission$2(Ljava/util/Set;Lcom/squareup/permissions/Employee;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 47
    invoke-virtual {p1, p0}, Lcom/squareup/permissions/Employee;->hasAnyPermission(Ljava/util/Set;)Z

    move-result p0

    return p0
.end method

.method static synthetic lambda$activePasscodeEmployees$1(Lcom/squareup/permissions/Employee;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 40
    iget-boolean p0, p0, Lcom/squareup/permissions/Employee;->canAccessRegisterWithPasscode:Z

    return p0
.end method

.method static synthetic lambda$filterEmployeesBy$6(Lio/reactivex/functions/Predicate;Ljava/util/Set;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 110
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 111
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/permissions/Employee;

    .line 112
    invoke-interface {p0, v1}, Lio/reactivex/functions/Predicate;->test(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 113
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$getAccountOwnerEmployee$5(Ljava/util/Set;)Lio/reactivex/MaybeSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 98
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/Employee;

    .line 99
    invoke-virtual {v0}, Lcom/squareup/permissions/Employee;->is_account_owner()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/squareup/permissions/Employee;->active:Z

    if-eqz v1, :cond_0

    .line 100
    invoke-static {v0}, Lio/reactivex/Maybe;->just(Ljava/lang/Object;)Lio/reactivex/Maybe;

    move-result-object p0

    return-object p0

    .line 103
    :cond_1
    invoke-static {}, Lio/reactivex/Maybe;->empty()Lio/reactivex/Maybe;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$getActiveOwnerEmployees$4(Lcom/squareup/permissions/Employee;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 91
    invoke-virtual {p0}, Lcom/squareup/permissions/Employee;->is_owner()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean p0, p0, Lcom/squareup/permissions/Employee;->active:Z

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$maybeOneEmployeeByToken$3(Ljava/lang/String;Ljava/util/Set;)Lio/reactivex/MaybeSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 60
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/Employee;

    .line 61
    iget-object v1, v0, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    invoke-static {v0}, Lio/reactivex/Maybe;->just(Ljava/lang/Object;)Lio/reactivex/Maybe;

    move-result-object p0

    return-object p0

    .line 65
    :cond_1
    invoke-static {}, Lio/reactivex/Maybe;->empty()Lio/reactivex/Maybe;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public activeEmployees()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/permissions/Employees;->store:Lcom/squareup/permissions/EmployeesStore;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeesStore;->allEmployees()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/-$$Lambda$Employees$qBctjwfbynVGZjTBBoxsD2wZPco;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$Employees$qBctjwfbynVGZjTBBoxsD2wZPco;

    .line 34
    invoke-direct {p0, v1}, Lcom/squareup/permissions/Employees;->filterEmployeesBy(Lio/reactivex/functions/Predicate;)Lio/reactivex/functions/Function;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public activeEmployeesWithAnyPermission(Ljava/util/Set;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;)",
            "Lio/reactivex/Observable<",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation

    .line 46
    invoke-virtual {p0}, Lcom/squareup/permissions/Employees;->activeEmployees()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/permissions/-$$Lambda$Employees$lxEaGIcMWvBKiOI-0PInw2ys7X4;

    invoke-direct {v1, p1}, Lcom/squareup/permissions/-$$Lambda$Employees$lxEaGIcMWvBKiOI-0PInw2ys7X4;-><init>(Ljava/util/Set;)V

    invoke-direct {p0, v1}, Lcom/squareup/permissions/Employees;->filterEmployeesBy(Lio/reactivex/functions/Predicate;)Lio/reactivex/functions/Function;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public activePasscodeEmployees()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation

    .line 39
    invoke-virtual {p0}, Lcom/squareup/permissions/Employees;->activeEmployees()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/-$$Lambda$Employees$ZTZZDUCBdSP_caxWde3Q80QNMRc;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$Employees$ZTZZDUCBdSP_caxWde3Q80QNMRc;

    invoke-direct {p0, v1}, Lcom/squareup/permissions/Employees;->filterEmployeesBy(Lio/reactivex/functions/Predicate;)Lio/reactivex/functions/Function;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public allEmployees()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/permissions/Employees;->store:Lcom/squareup/permissions/EmployeesStore;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeesStore;->allEmployees()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getAccountOwnerEmployee()Lio/reactivex/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    .line 95
    invoke-virtual {p0}, Lcom/squareup/permissions/Employees;->activeEmployees()Lio/reactivex/Observable;

    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstElement()Lio/reactivex/Maybe;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/-$$Lambda$Employees$pjsPiKfdD4f6Pwk63tV9O5W00FQ;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$Employees$pjsPiKfdD4f6Pwk63tV9O5W00FQ;

    .line 97
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    return-object v0
.end method

.method public getActiveOwnerEmployees()Lio/reactivex/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Maybe<",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;>;"
        }
    .end annotation

    .line 90
    invoke-virtual {p0}, Lcom/squareup/permissions/Employees;->activeEmployees()Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Observable;->firstElement()Lio/reactivex/Maybe;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/-$$Lambda$Employees$5wUrgFU9QO0DACN1sGdCsahsCOc;->INSTANCE:Lcom/squareup/permissions/-$$Lambda$Employees$5wUrgFU9QO0DACN1sGdCsahsCOc;

    invoke-direct {p0, v1}, Lcom/squareup/permissions/Employees;->filterEmployeesBy(Lio/reactivex/functions/Predicate;)Lio/reactivex/functions/Function;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object v0

    return-object v0
.end method

.method public maybeOneEmployeeByToken(Ljava/lang/String;)Lio/reactivex/Maybe;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 56
    invoke-static {}, Lio/reactivex/Maybe;->empty()Lio/reactivex/Maybe;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/permissions/Employees;->store:Lcom/squareup/permissions/EmployeesStore;

    .line 57
    invoke-interface {v0}, Lcom/squareup/permissions/EmployeesStore;->allEmployees()Lio/reactivex/Observable;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstElement()Lio/reactivex/Maybe;

    move-result-object v0

    new-instance v1, Lcom/squareup/permissions/-$$Lambda$Employees$v8wGiRc3IDycA1FL-dkBH6o602k;

    invoke-direct {v1, p1}, Lcom/squareup/permissions/-$$Lambda$Employees$v8wGiRc3IDycA1FL-dkBH6o602k;-><init>(Ljava/lang/String;)V

    .line 59
    invoke-virtual {v0, v1}, Lio/reactivex/Maybe;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public setTimecardId(Lcom/squareup/permissions/Employee;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Completable;
    .locals 1

    .line 80
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;-><init>()V

    .line 81
    invoke-virtual {v0, p2}, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;->id(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;

    move-result-object p2

    .line 82
    invoke-virtual {p2, p3}, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;->clockin_time(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;

    move-result-object p2

    .line 83
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;

    move-result-object p2

    .line 85
    invoke-virtual {p1}, Lcom/squareup/permissions/Employee;->toEmployeesEntity()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity;->newBuilder()Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->current_timecard(Lcom/squareup/server/account/protos/EmployeesEntity$CurrentTimecard;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p1

    .line 84
    invoke-static {p1}, Lcom/squareup/permissions/Employee;->fromEmployeesEntity(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/permissions/Employee;

    move-result-object p1

    .line 86
    iget-object p2, p0, Lcom/squareup/permissions/Employees;->store:Lcom/squareup/permissions/EmployeesStore;

    invoke-interface {p2, p1}, Lcom/squareup/permissions/EmployeesStore;->update(Lcom/squareup/permissions/Employee;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public updateStore(Ljava/util/Set;)Lio/reactivex/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Employee;",
            ">;)",
            "Lio/reactivex/Completable;"
        }
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/squareup/permissions/Employees;->store:Lcom/squareup/permissions/EmployeesStore;

    invoke-interface {v0, p1}, Lcom/squareup/permissions/EmployeesStore;->update(Ljava/util/Set;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method
