.class public Lcom/squareup/permissions/EmployeeInfo$Builder;
.super Ljava/lang/Object;
.source "EmployeeInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/permissions/EmployeeInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private employeeToken:Ljava/lang/String;

.field private firstName:Ljava/lang/String;

.field private lastName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/permissions/EmployeeInfo;
    .locals 5

    .line 42
    new-instance v0, Lcom/squareup/permissions/EmployeeInfo;

    iget-object v1, p0, Lcom/squareup/permissions/EmployeeInfo$Builder;->firstName:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/permissions/EmployeeInfo$Builder;->lastName:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/permissions/EmployeeInfo$Builder;->employeeToken:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/permissions/EmployeeInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/permissions/EmployeeInfo$1;)V

    return-object v0
.end method

.method public employeeToken(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/squareup/permissions/EmployeeInfo$Builder;->employeeToken:Ljava/lang/String;

    return-object p0
.end method

.method public firstName(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/squareup/permissions/EmployeeInfo$Builder;->firstName:Ljava/lang/String;

    return-object p0
.end method

.method public lastName(Ljava/lang/String;)Lcom/squareup/permissions/EmployeeInfo$Builder;
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/permissions/EmployeeInfo$Builder;->lastName:Ljava/lang/String;

    return-object p0
.end method
