.class Lcom/squareup/http/interceptor/LongCodec;
.super Ljava/lang/Object;
.source "LongCodec.java"


# static fields
.field private static final ENCODED_LENGTH:I = 0xd

.field private static final MASK_5BITS:I = 0x1f

.field private static final NIBBLE_SIZE:I = 0x5

.field private static final base32Alphabet:Ljava/lang/String; = "ABCDEFGHJKMNPQRSTVWXYZabcdefghjk"

.field private static final charToNibbleMap:[B

.field private static final nibbleToCharMap:[C


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v0, 0x20

    new-array v0, v0, [C

    .line 18
    sput-object v0, Lcom/squareup/http/interceptor/LongCodec;->nibbleToCharMap:[C

    const/16 v0, 0x80

    new-array v0, v0, [B

    .line 19
    sput-object v0, Lcom/squareup/http/interceptor/LongCodec;->charToNibbleMap:[B

    const-string v0, "ABCDEFGHJKMNPQRSTVWXYZabcdefghjk"

    .line 27
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-char v4, v0, v2

    .line 28
    sget-object v5, Lcom/squareup/http/interceptor/LongCodec;->nibbleToCharMap:[C

    aput-char v4, v5, v3

    .line 29
    sget-object v5, Lcom/squareup/http/interceptor/LongCodec;->charToNibbleMap:[B

    aput-byte v3, v5, v4

    add-int/lit8 v3, v3, 0x1

    int-to-byte v3, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decodeToLong(Ljava/lang/String;)J
    .locals 7

    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    const/16 v3, 0x3b

    :goto_0
    const/16 v4, 0xd

    if-ge v2, v4, :cond_1

    if-ltz v3, :cond_0

    .line 40
    sget-object v4, Lcom/squareup/http/interceptor/LongCodec;->charToNibbleMap:[B

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    aget-byte v4, v4, v5

    int-to-long v4, v4

    shl-long/2addr v4, v3

    goto :goto_1

    .line 42
    :cond_0
    sget-object v4, Lcom/squareup/http/interceptor/LongCodec;->charToNibbleMap:[B

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v5

    aget-byte v4, v4, v5

    int-to-long v4, v4

    neg-int v6, v3

    shr-long/2addr v4, v6

    :goto_1
    or-long/2addr v0, v4

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v3, -0x5

    goto :goto_0

    :cond_1
    return-wide v0
.end method

.method public static encodeToString(J)Ljava/lang/String;
    .locals 7

    const/16 v0, 0xd

    new-array v1, v0, [C

    const/4 v2, 0x0

    const/16 v3, 0x3b

    :goto_0
    if-ge v2, v0, :cond_1

    if-ltz v3, :cond_0

    .line 54
    sget-object v4, Lcom/squareup/http/interceptor/LongCodec;->nibbleToCharMap:[C

    shr-long v5, p0, v3

    long-to-int v6, v5

    and-int/lit8 v5, v6, 0x1f

    aget-char v4, v4, v5

    aput-char v4, v1, v2

    goto :goto_1

    .line 56
    :cond_0
    sget-object v4, Lcom/squareup/http/interceptor/LongCodec;->nibbleToCharMap:[C

    neg-int v5, v3

    shl-long v5, p0, v5

    long-to-int v6, v5

    and-int/lit8 v5, v6, 0x1f

    aget-char v4, v4, v5

    aput-char v4, v1, v2

    :goto_1
    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v3, -0x5

    goto :goto_0

    .line 59
    :cond_1
    new-instance p0, Ljava/lang/String;

    invoke-direct {p0, v1}, Ljava/lang/String;-><init>([C)V

    return-object p0
.end method
