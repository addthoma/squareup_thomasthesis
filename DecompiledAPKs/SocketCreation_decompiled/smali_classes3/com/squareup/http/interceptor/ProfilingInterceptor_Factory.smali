.class public final Lcom/squareup/http/interceptor/ProfilingInterceptor_Factory;
.super Ljava/lang/Object;
.source "ProfilingInterceptor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/http/interceptor/ProfilingInterceptor;",
        ">;"
    }
.end annotation


# instance fields
.field private final profilerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/HttpProfiler<",
            "*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/HttpProfiler<",
            "*>;>;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/http/interceptor/ProfilingInterceptor_Factory;->profilerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/http/interceptor/ProfilingInterceptor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/HttpProfiler<",
            "*>;>;)",
            "Lcom/squareup/http/interceptor/ProfilingInterceptor_Factory;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/http/interceptor/ProfilingInterceptor_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/http/interceptor/ProfilingInterceptor_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/http/HttpProfiler;)Lcom/squareup/http/interceptor/ProfilingInterceptor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/http/HttpProfiler<",
            "*>;)",
            "Lcom/squareup/http/interceptor/ProfilingInterceptor;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/http/interceptor/ProfilingInterceptor;

    invoke-direct {v0, p0}, Lcom/squareup/http/interceptor/ProfilingInterceptor;-><init>(Lcom/squareup/http/HttpProfiler;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/http/interceptor/ProfilingInterceptor;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/http/interceptor/ProfilingInterceptor_Factory;->profilerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/http/HttpProfiler;

    invoke-static {v0}, Lcom/squareup/http/interceptor/ProfilingInterceptor_Factory;->newInstance(Lcom/squareup/http/HttpProfiler;)Lcom/squareup/http/interceptor/ProfilingInterceptor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/http/interceptor/ProfilingInterceptor_Factory;->get()Lcom/squareup/http/interceptor/ProfilingInterceptor;

    move-result-object v0

    return-object v0
.end method
