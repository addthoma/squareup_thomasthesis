.class public interface abstract Lcom/squareup/http/HttpProfiler;
.super Ljava/lang/Object;
.source "HttpProfiler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/http/HttpProfiler$CallInformation;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract afterCall(Lcom/squareup/http/HttpProfiler$CallInformation;JILjava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/http/HttpProfiler$CallInformation;",
            "JITT;)V"
        }
    .end annotation
.end method

.method public abstract afterError(Lcom/squareup/http/HttpProfiler$CallInformation;Ljava/lang/Exception;JLjava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/http/HttpProfiler$CallInformation;",
            "Ljava/lang/Exception;",
            "JTT;)V"
        }
    .end annotation
.end method

.method public abstract beforeCall()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method
