.class public final Lcom/squareup/http/ServerApiUrls;
.super Ljava/lang/Object;
.source "ServerApiUrls.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0004"
    }
    d2 = {
        "PRODUCTION_API_URL",
        "",
        "PRODUCTION_CONNECT_API_URL",
        "PRODUCTION_WEEBLY_API_URL",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final PRODUCTION_API_URL:Ljava/lang/String; = "https://api-global.squareup.com/"

.field public static final PRODUCTION_CONNECT_API_URL:Ljava/lang/String; = "https://connect.squareup.com"

.field public static final PRODUCTION_WEEBLY_API_URL:Ljava/lang/String; = "https://www.weebly.com"
