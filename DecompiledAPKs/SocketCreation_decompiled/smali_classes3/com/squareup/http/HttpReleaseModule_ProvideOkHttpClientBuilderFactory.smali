.class public final Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;
.super Ljava/lang/Object;
.source "HttpReleaseModule_ProvideOkHttpClientBuilderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lokhttp3/OkHttpClient$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final interceptorsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lokhttp3/Interceptor;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lokhttp3/Interceptor;",
            ">;>;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;->applicationProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;->interceptorsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lokhttp3/Interceptor;",
            ">;>;)",
            "Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideOkHttpClientBuilder(Landroid/app/Application;Ljava/util/Set;)Lokhttp3/OkHttpClient$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Ljava/util/Set<",
            "Lokhttp3/Interceptor;",
            ">;)",
            "Lokhttp3/OkHttpClient$Builder;"
        }
    .end annotation

    .line 43
    invoke-static {p0, p1}, Lcom/squareup/http/HttpReleaseModule;->provideOkHttpClientBuilder(Landroid/app/Application;Ljava/util/Set;)Lokhttp3/OkHttpClient$Builder;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lokhttp3/OkHttpClient$Builder;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;->get()Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    return-object v0
.end method

.method public get()Lokhttp3/OkHttpClient$Builder;
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;->interceptorsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    invoke-static {v0, v1}, Lcom/squareup/http/HttpReleaseModule_ProvideOkHttpClientBuilderFactory;->provideOkHttpClientBuilder(Landroid/app/Application;Ljava/util/Set;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v0

    return-object v0
.end method
