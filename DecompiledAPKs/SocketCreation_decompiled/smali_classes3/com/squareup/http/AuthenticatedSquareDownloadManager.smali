.class public final Lcom/squareup/http/AuthenticatedSquareDownloadManager;
.super Ljava/lang/Object;
.source "AuthenticatedSquareDownloadManager.kt"

# interfaces
.implements Lcom/squareup/http/SquareDownloadManager;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0001\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0006H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/http/AuthenticatedSquareDownloadManager;",
        "Lcom/squareup/http/SquareDownloadManager;",
        "androidDownloadManager",
        "Landroid/app/DownloadManager;",
        "sessionTokenProvider",
        "Ljavax/inject/Provider;",
        "",
        "server",
        "Lcom/squareup/http/Server;",
        "(Landroid/app/DownloadManager;Ljavax/inject/Provider;Lcom/squareup/http/Server;)V",
        "enqueue",
        "",
        "request",
        "Landroid/app/DownloadManager$Request;",
        "encodedPath",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final androidDownloadManager:Landroid/app/DownloadManager;

.field private final server:Lcom/squareup/http/Server;

.field private final sessionTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/DownloadManager;Ljavax/inject/Provider;Lcom/squareup/http/Server;)V
    .locals 1
    .param p2    # Ljavax/inject/Provider;
        .annotation runtime Lcom/squareup/api/SessionIdPII;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/DownloadManager;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/http/Server;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "androidDownloadManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionTokenProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "server"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/http/AuthenticatedSquareDownloadManager;->androidDownloadManager:Landroid/app/DownloadManager;

    iput-object p2, p0, Lcom/squareup/http/AuthenticatedSquareDownloadManager;->sessionTokenProvider:Ljavax/inject/Provider;

    iput-object p3, p0, Lcom/squareup/http/AuthenticatedSquareDownloadManager;->server:Lcom/squareup/http/Server;

    return-void
.end method


# virtual methods
.method public enqueue(Landroid/app/DownloadManager$Request;)J
    .locals 2

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/http/AuthenticatedSquareDownloadManager;->sessionTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 32
    invoke-static {v0}, Lcom/squareup/server/account/AuthorizationHeaders;->authorizationHeaderValueFrom(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Authorization"

    invoke-virtual {p1, v1, v0}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/squareup/http/AuthenticatedSquareDownloadManager;->androidDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v0, p1}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v0

    return-wide v0
.end method

.method public enqueue(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)J
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/app/DownloadManager$Request;",
            "Lkotlin/Unit;",
            ">;)J"
        }
    .end annotation

    const-string v0, "encodedPath"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-static {p0, p1, p2}, Lcom/squareup/http/SquareDownloadManager$DefaultImpls;->enqueue(Lcom/squareup/http/SquareDownloadManager;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)J

    move-result-wide p1

    return-wide p1
.end method

.method public request(Ljava/lang/String;)Landroid/app/DownloadManager$Request;
    .locals 2

    const-string v0, "encodedPath"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    new-instance v0, Landroid/app/DownloadManager$Request;

    .line 39
    iget-object v1, p0, Lcom/squareup/http/AuthenticatedSquareDownloadManager;->server:Lcom/squareup/http/Server;

    invoke-virtual {v1}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object p1

    invoke-virtual {p1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    .line 38
    invoke-direct {v0, p1}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    return-object v0
.end method
