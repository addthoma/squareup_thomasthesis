.class public final Lcom/squareup/ordermagstripe/OrderMagstripeBootstrapScreenScope;
.super Lcom/squareup/container/ContainerTreeKey;
.source "OrderMagstripeBootstrapScreenScope.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderMagstripeBootstrapScreenScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderMagstripeBootstrapScreenScope.kt\ncom/squareup/ordermagstripe/OrderMagstripeBootstrapScreenScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,30:1\n35#2:31\n*E\n*S KotlinDebug\n*F\n+ 1 OrderMagstripeBootstrapScreenScope.kt\ncom/squareup/ordermagstripe/OrderMagstripeBootstrapScreenScope\n*L\n22#1:31\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0001H\u0016R\u000e\u0010\u0003\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ordermagstripe/OrderMagstripeBootstrapScreenScope;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "Lcom/squareup/container/MaybePersistent;",
        "parentKey",
        "(Lcom/squareup/container/ContainerTreeKey;)V",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final parentKey:Lcom/squareup/container/ContainerTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    const-string v0, "parentKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ordermagstripe/OrderMagstripeBootstrapScreenScope;->parentKey:Lcom/squareup/container/ContainerTreeKey;

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerTreeKey;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 31
    const-class v1, Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 22
    check-cast p1, Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner$ParentComponent;

    .line 24
    invoke-interface {p1}, Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner$ParentComponent;->orderMagstripeWorkflowRunner()Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner;

    move-result-object p1

    const-string v1, "scopeBuilder"

    .line 25
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ordermagstripe/OrderMagstripeBootstrapScreenScope;->parentKey:Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ordermagstripe/OrderMagstripeBootstrapScreenScope;->getParentKey()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    .line 15
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method
