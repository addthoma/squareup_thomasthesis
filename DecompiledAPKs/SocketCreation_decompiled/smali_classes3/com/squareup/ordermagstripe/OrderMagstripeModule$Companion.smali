.class public final Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;
.super Ljava/lang/Object;
.source "OrderMagstripeModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ordermagstripe/OrderMagstripeModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0087\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007J\u0008\u0010\u0005\u001a\u00020\u0006H\u0007J\u0008\u0010\u0007\u001a\u00020\u0008H\u0007J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0007J\u0008\u0010\r\u001a\u00020\u000eH\u0007\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;",
        "",
        "()V",
        "provideCorrectedAddressConfiguration",
        "Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;",
        "provideOrderConfirmationConfiguration",
        "Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;",
        "provideOrderCoodinatorConfiguration",
        "Lcom/squareup/mailorder/OrderCoordinator$Configuration;",
        "provideOrderReactorConfiguration",
        "Lcom/squareup/mailorder/OrderReactor$Configuration;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "provideUnverifiedAddressConfiguration",
        "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/ordermagstripe/OrderMagstripeModule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideCorrectedAddressConfiguration()Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 68
    new-instance v0, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;

    .line 69
    sget v1, Lcom/squareup/common/strings/R$string;->continue_label:I

    .line 70
    sget v2, Lcom/squareup/common/strings/R$string;->order_reader:I

    .line 68
    invoke-direct {v0, v1, v2}, Lcom/squareup/mailorder/SelectCorrectedAddressCoordinator$Configuration;-><init>(II)V

    return-object v0
.end method

.method public final provideOrderConfirmationConfiguration()Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 77
    new-instance v0, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v1}, Lcom/squareup/mailorder/OrderConfirmationCoordinator$Configuration;-><init>(II)V

    return-object v0
.end method

.method public final provideOrderCoodinatorConfiguration()Lcom/squareup/mailorder/OrderCoordinator$Configuration;
    .locals 8
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 47
    new-instance v7, Lcom/squareup/mailorder/OrderCoordinator$Configuration;

    .line 48
    sget v1, Lcom/squareup/common/strings/R$string;->continue_label:I

    .line 49
    sget v2, Lcom/squareup/common/strings/R$string;->order_reader:I

    .line 51
    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->READER_MEDIUM:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 52
    sget v5, Lcom/squareup/common/strings/R$string;->shipping_details:I

    .line 53
    sget v6, Lcom/squareup/common/strings/R$string;->send_reader_subheading:I

    const/4 v3, 0x1

    move-object v0, v7

    .line 47
    invoke-direct/range {v0 .. v6}, Lcom/squareup/mailorder/OrderCoordinator$Configuration;-><init>(IIZLcom/squareup/glyph/GlyphTypeface$Glyph;II)V

    return-object v7
.end method

.method public final provideOrderReactorConfiguration(Lcom/squareup/settings/server/Features;)Lcom/squareup/mailorder/OrderReactor$Configuration;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    new-instance v0, Lcom/squareup/mailorder/OrderReactor$Configuration;

    .line 86
    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->VERIFY_ADDRESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    const/4 v1, 0x0

    .line 85
    invoke-direct {v0, p1, v1}, Lcom/squareup/mailorder/OrderReactor$Configuration;-><init>(ZZ)V

    return-object v0
.end method

.method public final provideUnverifiedAddressConfiguration()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 59
    new-instance v0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

    .line 60
    sget v1, Lcom/squareup/common/strings/R$string;->continue_label:I

    .line 61
    sget v2, Lcom/squareup/common/strings/R$string;->order_reader:I

    .line 59
    invoke-direct {v0, v1, v2}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;-><init>(II)V

    return-object v0
.end method
