.class public final Lcom/squareup/giftcard/activation/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/giftcard/activation/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final card_container:I = 0x7f0a02b4

.field public static final cash_out_reason:I = 0x7f0a02ef

.field public static final clear_balance_button:I = 0x7f0a033e

.field public static final gc_add_value_button:I = 0x7f0a0782

.field public static final gc_current_balance_label:I = 0x7f0a0783

.field public static final gc_current_balance_value:I = 0x7f0a0784

.field public static final gc_history_event_container:I = 0x7f0a0785

.field public static final gc_history_row_amount:I = 0x7f0a0786

.field public static final gc_history_row_date:I = 0x7f0a0787

.field public static final gc_history_row_description:I = 0x7f0a0788

.field public static final gift_card_balance_contents:I = 0x7f0a078d

.field public static final gift_card_brand_with_unmasked_digits:I = 0x7f0a078e

.field public static final gift_card_clear_balance_button:I = 0x7f0a078f

.field public static final gift_card_clear_balance_contents:I = 0x7f0a0790

.field public static final gift_card_clear_balance_progress_bar:I = 0x7f0a0791

.field public static final gift_card_clear_reason_other_text:I = 0x7f0a0792

.field public static final gift_card_clear_reason_title:I = 0x7f0a0793

.field public static final gift_card_custom_amount_button:I = 0x7f0a0794

.field public static final gift_card_custom_amount_container:I = 0x7f0a0795

.field public static final gift_card_custom_amount_field:I = 0x7f0a0796

.field public static final gift_card_figure:I = 0x7f0a0797

.field public static final gift_card_history_contents:I = 0x7f0a0798

.field public static final gift_card_number:I = 0x7f0a0799

.field public static final gift_card_order_hint:I = 0x7f0a079c

.field public static final gift_card_preset_amount_container:I = 0x7f0a079d

.field public static final gift_card_progress_bar:I = 0x7f0a079e

.field public static final gift_card_type_check_balance:I = 0x7f0a07a1

.field public static final gift_card_type_container:I = 0x7f0a07a2

.field public static final gift_card_type_egc:I = 0x7f0a07a3

.field public static final gift_card_type_physical:I = 0x7f0a07a4

.field public static final invalid_gift_card:I = 0x7f0a086d

.field public static final lost_card_reason:I = 0x7f0a0963

.field public static final other_reason:I = 0x7f0a0b71

.field public static final reason_checkable_group:I = 0x7f0a0cfa

.field public static final reset_card_reason:I = 0x7f0a0d6f


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
