.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$14;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "screenData",
        "Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$14;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;)V
    .locals 2

    .line 316
    invoke-virtual {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->getState()Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData$State;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 320
    :cond_0
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$14;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getMaybeX2SellerScreenRunner$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-result-object v0

    .line 321
    invoke-virtual {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->getCardBalance()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 322
    invoke-virtual {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;->getCardName()Ljava/lang/String;

    move-result-object p1

    .line 320
    invoke-interface {v0, v1, p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->displayGiftCardBalance(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Z

    goto :goto_0

    .line 319
    :cond_1
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$14;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getMaybeX2SellerScreenRunner$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->errorCheckingGiftCardBalance()Z

    goto :goto_0

    .line 318
    :cond_2
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$14;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getMaybeX2SellerScreenRunner$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->displayGiftCardBalanceCheck()Z

    goto :goto_0

    .line 317
    :cond_3
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$14;->this$0:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->access$getMaybeX2SellerScreenRunner$p(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;)Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->checkingGiftCardBalance()Z

    :goto_0
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 96
    check-cast p1, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$14;->call(Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$ScreenData;)V

    return-void
.end method
