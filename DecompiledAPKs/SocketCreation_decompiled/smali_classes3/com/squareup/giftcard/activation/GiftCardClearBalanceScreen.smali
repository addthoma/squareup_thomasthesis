.class public final Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen;
.super Lcom/squareup/giftcard/activation/InGiftCardLoadingScope;
.source "GiftCardClearBalanceScreen.kt"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardClearBalanceScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardClearBalanceScreen.kt\ncom/squareup/giftcard/activation/GiftCardClearBalanceScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,53:1\n43#2:54\n*E\n*S KotlinDebug\n*F\n+ 1 GiftCardClearBalanceScreen.kt\ncom/squareup/giftcard/activation/GiftCardClearBalanceScreen\n*L\n49#1:54\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0001\u0010B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0008\u0010\u000e\u001a\u00020\u000fH\u0016R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen;",
        "Lcom/squareup/giftcard/activation/InGiftCardLoadingScope;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/container/MaybePersistent;",
        "parentKey",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "(Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "getParentKey",
        "()Lcom/squareup/ui/main/RegisterTreeKey;",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "Runner",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "parentKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/giftcard/activation/InGiftCardLoadingScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    .line 18
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    const-class v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Component;

    .line 50
    invoke-interface {p1}, Lcom/squareup/giftcard/activation/GiftCardLoadingScope$Component;->giftCardClearBalanceCoordinator()Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 45
    sget v0, Lcom/squareup/giftcard/activation/R$layout;->gift_card_clear_balance_view:I

    return v0
.end method
