.class public final Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1;
.super Ljava/lang/Object;
.source "GiftCardLookupCoordinator.kt"

# interfaces
.implements Lcom/squareup/register/widgets/card/OnPanListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0012\u0010\u0006\u001a\u00020\u00032\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008H\u0016J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u0005H\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "com/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1",
        "Lcom/squareup/register/widgets/card/OnPanListener;",
        "onNext",
        "",
        "card",
        "Lcom/squareup/Card;",
        "onPanInvalid",
        "warning",
        "Lcom/squareup/Card$PanWarning;",
        "onPanValid",
        "manualCard",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 66
    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNext(Lcom/squareup/Card;)V
    .locals 1

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getController$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;->continueGiftCardLookupIfValid(Lcom/squareup/Card;)V

    return-void
.end method

.method public onPanInvalid(Lcom/squareup/Card$PanWarning;)V
    .locals 1

    .line 93
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getController$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    move-result-object p1

    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinatorKt;->getEMPTY_CARD()Lcom/squareup/Card;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;->setCard(Lcom/squareup/Card;)V

    return-void
.end method

.method public onPanValid(Lcom/squareup/Card;)V
    .locals 3

    const-string v0, "manualCard"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getGiftCards$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/GiftCards;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v0

    const-string v1, "GiftCardLookupCoordinator"

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getErrorsBar$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    .line 82
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getController$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;->setCard(Lcom/squareup/Card;)V

    goto :goto_0

    .line 84
    :cond_0
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getErrorsBar$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;

    move-result-object p1

    .line 86
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v2, Lcom/squareup/giftcard/activation/R$string;->gift_card_unsupported_message_keyed:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-virtual {p1, v1, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getController$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    move-result-object p1

    invoke-static {}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinatorKt;->getEMPTY_CARD()Lcom/squareup/Card;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;->setCard(Lcom/squareup/Card;)V

    :goto_0
    return-void
.end method
