.class public interface abstract Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;
.super Ljava/lang/Object;
.source "GiftCardClearBalanceScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0008\u0010\u0006\u001a\u00020\u0007H&J\u000e\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0003H&J\u0008\u0010\n\u001a\u00020\u0007H&J\u0008\u0010\u000b\u001a\u00020\u000cH&J\u0010\u0010\r\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u000fH&J\u0010\u0010\u0010\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u0012H&J\u0008\u0010\u0013\u001a\u00020\u0007H&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;",
        "",
        "busy",
        "Lrx/Observable;",
        "",
        "canUseCashOut",
        "cancelClearBalance",
        "",
        "clearBalance",
        "Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;",
        "finishClearBalanceAndCloseActivationFlow",
        "getBalance",
        "Lcom/squareup/protos/common/Money;",
        "setClearReason",
        "newId",
        "",
        "setClearReasonText",
        "reasonText",
        "",
        "triggerClearBalance",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract busy()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract canUseCashOut()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract cancelClearBalance()V
.end method

.method public abstract clearBalance()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract finishClearBalanceAndCloseActivationFlow()V
.end method

.method public abstract getBalance()Lcom/squareup/protos/common/Money;
.end method

.method public abstract setClearReason(I)V
.end method

.method public abstract setClearReasonText(Ljava/lang/String;)V
.end method

.method public abstract triggerClearBalance()V
.end method
