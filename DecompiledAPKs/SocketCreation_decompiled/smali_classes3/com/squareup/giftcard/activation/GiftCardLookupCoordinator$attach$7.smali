.class final Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$7;
.super Ljava/lang/Object;
.source "GiftCardLookupCoordinator.kt"

# interfaces
.implements Lcom/squareup/ui/TokenView$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "onXClicked"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field final synthetic $brandWithUnmaskedDigits:Lcom/squareup/ui/TokenView;

.field final synthetic $panEditor:Lcom/squareup/register/widgets/card/PanEditor;


# direct methods
.method constructor <init>(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/ui/TokenView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$7;->$actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$7;->$panEditor:Lcom/squareup/register/widgets/card/PanEditor;

    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$7;->$brandWithUnmaskedDigits:Lcom/squareup/ui/TokenView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onXClicked()V
    .locals 4

    .line 174
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$7;->$actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 175
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$7;->$panEditor:Lcom/squareup/register/widgets/card/PanEditor;

    const/4 v2, 0x0

    move-object v3, v2

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Lcom/squareup/register/widgets/card/PanEditor;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$7;->$panEditor:Lcom/squareup/register/widgets/card/PanEditor;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/PanEditor;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$7;->$brandWithUnmaskedDigits:Lcom/squareup/ui/TokenView;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/TokenView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$7;->$brandWithUnmaskedDigits:Lcom/squareup/ui/TokenView;

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
