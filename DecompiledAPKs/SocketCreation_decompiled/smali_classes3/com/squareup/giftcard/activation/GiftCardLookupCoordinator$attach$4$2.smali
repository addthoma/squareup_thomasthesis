.class final Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;
.super Ljava/lang/Object;
.source "GiftCardLookupCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/Card;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "card",
        "Lcom/squareup/Card;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/Card;)V
    .locals 4

    .line 124
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getGiftCards$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/GiftCards;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/giftcard/GiftCards;->isThirdPartyGiftCardFeatureEnabled()Z

    move-result v0

    const-string v1, "card"

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getGiftCards$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/GiftCards;

    move-result-object v0

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/giftcard/GiftCards;->isSquareGiftCard(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->$panEditor:Lcom/squareup/register/widgets/card/PanEditor;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/squareup/register/widgets/card/PanEditor;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->$brandWithUnmaskedDigits:Lcom/squareup/ui/TokenView;

    .line 129
    iget-object v2, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;

    iget-object v2, v2, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;

    invoke-static {v2}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/util/Res;

    move-result-object v2

    sget-object v3, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    .line 130
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object p1

    .line 128
    invoke-static {v2, v3, p1}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card$Brand;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 127
    invoke-virtual {v0, p1}, Lcom/squareup/ui/TokenView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->$brandWithUnmaskedDigits:Lcom/squareup/ui/TokenView;

    check-cast p1, Landroid/view/View;

    .line 134
    iget-object v0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;

    iget-object v0, v0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 133
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 136
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;->this$0:Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;

    iget-object p1, p1, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;->$panEditor:Lcom/squareup/register/widgets/card/PanEditor;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/Card;

    invoke-virtual {p0, p1}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4$2;->call(Lcom/squareup/Card;)V

    return-void
.end method
