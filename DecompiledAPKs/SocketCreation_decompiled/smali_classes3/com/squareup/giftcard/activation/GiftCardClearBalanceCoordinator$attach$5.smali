.class final Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$5;
.super Ljava/lang/Object;
.source "GiftCardClearBalanceCoordinator.kt"

# interfaces
.implements Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "checkGroup",
        "Lcom/squareup/widgets/CheckableGroup;",
        "kotlin.jvm.PlatformType",
        "newId",
        "",
        "prevId",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $clearButton:Lcom/squareup/ui/ConfirmButton;

.field final synthetic $otherReasonText:Landroid/widget/EditText;

.field final synthetic this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;Landroid/widget/EditText;Lcom/squareup/ui/ConfirmButton;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$5;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$5;->$otherReasonText:Landroid/widget/EditText;

    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$5;->$clearButton:Lcom/squareup/ui/ConfirmButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 1

    .line 64
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$5;->this$0:Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;

    invoke-static {p1}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;->access$getRunner$p(Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator;)Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;

    move-result-object p1

    invoke-interface {p1, p2}, Lcom/squareup/giftcard/activation/GiftCardClearBalanceScreen$Runner;->setClearReason(I)V

    .line 65
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$5;->$otherReasonText:Landroid/widget/EditText;

    check-cast p1, Landroid/view/View;

    sget p3, Lcom/squareup/giftcard/activation/R$id;->other_reason:I

    const/4 v0, 0x1

    if-ne p2, p3, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 66
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardClearBalanceCoordinator$attach$5;->$clearButton:Lcom/squareup/ui/ConfirmButton;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ConfirmButton;->setEnabled(Z)V

    return-void
.end method
