.class final Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$2;
.super Ljava/lang/Object;
.source "GiftCardLoadingScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "TA;TB;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "swipe",
        "Lcom/squareup/Card;",
        "kotlin.jvm.PlatformType",
        "card",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$2;

    invoke-direct {v0}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$2;-><init>()V

    sput-object v0, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$2;->INSTANCE:Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 96
    check-cast p1, Lcom/squareup/Card;

    check-cast p2, Lcom/squareup/Card;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/giftcard/activation/GiftCardLoadingScopeRunner$onEnterScope$swipedCard$2;->call(Lcom/squareup/Card;Lcom/squareup/Card;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/Card;Lcom/squareup/Card;)Z
    .locals 0

    const-string p1, "card"

    .line 200
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/Card;->getUnmaskedDigits()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method
