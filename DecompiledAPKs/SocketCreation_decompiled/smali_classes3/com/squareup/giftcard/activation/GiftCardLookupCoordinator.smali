.class public final Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "GiftCardLookupCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B=\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "controller",
        "Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;",
        "res",
        "Lcom/squareup/util/Res;",
        "errorsBar",
        "Lcom/squareup/ui/ErrorsBarPresenter;",
        "giftCards",
        "Lcom/squareup/giftcard/GiftCards;",
        "maybeX2SellerScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "thirdPartyGiftCardStrategyProvider",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
        "(Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Ljavax/inject/Provider;)V",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final controller:Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

.field private final errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;

.field private final maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

.field private final res:Lcom/squareup/util/Res;

.field private final thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Ljavax/inject/Provider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/giftcard/GiftCards;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "controller"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorsBar"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "giftCards"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "maybeX2SellerScreenRunner"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "thirdPartyGiftCardStrategyProvider"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->controller:Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    iput-object p2, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    iput-object p4, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->giftCards:Lcom/squareup/giftcard/GiftCards;

    iput-object p5, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iput-object p6, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    .line 51
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    return-void
.end method

.method public static final synthetic access$getController$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->controller:Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    return-object p0
.end method

.method public static final synthetic access$getErrorsBar$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/ui/ErrorsBarPresenter;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    return-object p0
.end method

.method public static final synthetic access$getGiftCards$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/giftcard/GiftCards;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->giftCards:Lcom/squareup/giftcard/GiftCards;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 41
    iget-object p0, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 6

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 56
    sget v1, Lcom/squareup/giftcard/activation/R$id;->gift_card_number:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/register/widgets/card/PanEditor;

    .line 58
    sget v2, Lcom/squareup/giftcard/activation/R$id;->gift_card_brand_with_unmasked_digits:I

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 57
    check-cast v2, Lcom/squareup/ui/TokenView;

    .line 59
    sget v3, Lcom/squareup/giftcard/activation/R$id;->gift_card_order_hint:I

    invoke-static {p1, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/marketfont/MarketTextView;

    .line 61
    iget-object v4, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->controller:Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    invoke-interface {v4}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;->isPlasticCardFeatureEnabled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 62
    sget v4, Lcom/squareup/giftcard/activation/R$string;->gift_card_number_hint_enter_card:I

    invoke-virtual {v1, v4}, Lcom/squareup/register/widgets/card/PanEditor;->setHint(I)V

    const/16 v4, 0x8

    .line 63
    invoke-virtual {v3, v4}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 66
    :cond_0
    new-instance v3, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1;

    invoke-direct {v3, p0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$1;-><init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)V

    check-cast v3, Lcom/squareup/register/widgets/card/OnPanListener;

    invoke-virtual {v1, v3}, Lcom/squareup/register/widgets/card/PanEditor;->setOnPanListener(Lcom/squareup/register/widgets/card/OnPanListener;)V

    .line 97
    iget-object v3, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v3}, Lcom/squareup/giftcard/GiftCards;->isThirdPartyGiftCardFeatureEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 98
    iget-object v3, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    invoke-virtual {v1, v3}, Lcom/squareup/register/widgets/card/PanEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    goto :goto_0

    .line 100
    :cond_1
    new-instance v3, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {v3}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    check-cast v3, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    invoke-virtual {v1, v3}, Lcom/squareup/register/widgets/card/PanEditor;->setStrategy(Lcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 104
    :goto_0
    iget-object v3, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->controller:Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;

    invoke-interface {v3}, Lcom/squareup/giftcard/activation/GiftCardLookupScreen$Controller;->actionBarHasX()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 105
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_1

    .line 107
    :cond_2
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 109
    :goto_1
    iget-object v4, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/librarylist/R$string;->item_library_all_gift_cards:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    .line 103
    invoke-virtual {v0, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 111
    new-instance v3, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$2;

    invoke-direct {v3, p0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$2;-><init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 113
    iget-object v3, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->continue_label:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 112
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x0

    .line 115
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 116
    new-instance v3, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$3;

    invoke-direct {v3, p0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$3;-><init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 119
    new-instance v3, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;

    invoke-direct {v3, p0, v1, v2, p1}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$4;-><init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/ui/TokenView;Landroid/view/View;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 142
    new-instance v3, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$5;

    invoke-direct {v3, p0, v0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$5;-><init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 151
    new-instance v3, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;

    invoke-direct {v3, p0}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$6;-><init>(Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 173
    new-instance p1, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$7;

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator$attach$7;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/register/widgets/card/PanEditor;Lcom/squareup/ui/TokenView;)V

    check-cast p1, Lcom/squareup/ui/TokenView$Listener;

    invoke-virtual {v2, p1}, Lcom/squareup/ui/TokenView;->setListener(Lcom/squareup/ui/TokenView$Listener;)V

    .line 181
    iget-object p1, p0, Lcom/squareup/giftcard/activation/GiftCardLookupCoordinator;->maybeX2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->displayGiftCardBalanceCheck()Z

    return-void
.end method
