.class public final Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;
.super Ljava/lang/Object;
.source "SingleImagePicassoFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/merchantimages/SingleImagePicassoFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 48
    iput-object p8, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg7Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Cache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;",
            ">;)",
            "Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;"
        }
    .end annotation

    .line 62
    new-instance v9, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/picasso/Cache;Ljava/io/File;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/google/gson/Gson;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;)Lcom/squareup/merchantimages/SingleImagePicassoFactory;
    .locals 10

    .line 68
    new-instance v9, Lcom/squareup/merchantimages/SingleImagePicassoFactory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/merchantimages/SingleImagePicassoFactory;-><init>(Landroid/app/Application;Lcom/squareup/picasso/Cache;Ljava/io/File;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/google/gson/Gson;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/merchantimages/SingleImagePicassoFactory;
    .locals 9

    .line 53
    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/picasso/Cache;

    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/io/File;

    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/gson/Gson;

    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;

    iget-object v0, p0, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;

    invoke-static/range {v1 .. v8}, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/picasso/Cache;Ljava/io/File;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Lcom/google/gson/Gson;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/picasso/pollexor/EnsureThumborRequestTransformer;Lcom/squareup/merchantimages/SingleImagePicassoFactory$DownloaderFactory;)Lcom/squareup/merchantimages/SingleImagePicassoFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/merchantimages/SingleImagePicassoFactory_Factory;->get()Lcom/squareup/merchantimages/SingleImagePicassoFactory;

    move-result-object v0

    return-object v0
.end method
