.class public Lcom/squareup/merchantimages/RealCuratedImage;
.super Ljava/lang/Object;
.source "RealCuratedImage.java"

# interfaces
.implements Lcom/squareup/merchantimages/CuratedImage;


# static fields
.field private static final MAX_IMAGE_PIXELS:I = 0xea600


# instance fields
.field private final curatedImageUri:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final imageSize:Landroid/graphics/Point;

.field private picasso:Lcom/squareup/merchantimages/SingleImagePicasso;

.field private final picassoFactory:Lcom/squareup/merchantimages/SingleImagePicassoFactory;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final thumbor:Lcom/squareup/pollexor/Thumbor;


# direct methods
.method public constructor <init>(Lcom/squareup/merchantimages/SingleImagePicassoFactory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/pollexor/Thumbor;Landroid/app/Application;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 47
    invoke-static {p4}, Lcom/squareup/merchantimages/RealCuratedImage;->calculateImageSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object p4

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/merchantimages/RealCuratedImage;-><init>(Lcom/squareup/merchantimages/SingleImagePicassoFactory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/pollexor/Thumbor;Landroid/graphics/Point;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/merchantimages/SingleImagePicassoFactory;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/pollexor/Thumbor;Landroid/graphics/Point;)V
    .locals 1

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/merchantimages/RealCuratedImage;->curatedImageUri:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 52
    iput-object p1, p0, Lcom/squareup/merchantimages/RealCuratedImage;->picassoFactory:Lcom/squareup/merchantimages/SingleImagePicassoFactory;

    .line 53
    iput-object p2, p0, Lcom/squareup/merchantimages/RealCuratedImage;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 54
    iput-object p3, p0, Lcom/squareup/merchantimages/RealCuratedImage;->thumbor:Lcom/squareup/pollexor/Thumbor;

    .line 55
    iput-object p4, p0, Lcom/squareup/merchantimages/RealCuratedImage;->imageSize:Landroid/graphics/Point;

    return-void
.end method

.method private static calculateImageSize(Landroid/content/Context;)Landroid/graphics/Point;
    .locals 6

    .line 118
    invoke-static {p0}, Lcom/squareup/util/Views;->getDisplaySize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object p0

    .line 121
    iget v0, p0, Landroid/graphics/Point;->x:I

    iget v1, p0, Landroid/graphics/Point;->y:I

    mul-int v0, v0, v1

    int-to-double v0, v0

    const-wide v2, 0x412d4c0000000000L    # 960000.0

    cmpl-double v4, v0, v2

    if-lez v4, :cond_0

    div-double/2addr v2, v0

    .line 123
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 124
    new-instance v2, Landroid/graphics/Point;

    iget v3, p0, Landroid/graphics/Point;->x:I

    int-to-double v3, v3

    mul-double v3, v3, v0

    double-to-int v3, v3

    iget p0, p0, Landroid/graphics/Point;->y:I

    int-to-double v4, p0

    mul-double v4, v4, v0

    double-to-int p0, v4

    invoke-direct {v2, v3, p0}, Landroid/graphics/Point;-><init>(II)V

    move-object p0, v2

    :cond_0
    return-object p0
.end method

.method private getThumborUri()Ljava/lang/String;
    .locals 4

    .line 105
    iget-object v0, p0, Lcom/squareup/merchantimages/RealCuratedImage;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCuratedImageUrl()Ljava/lang/String;

    move-result-object v0

    .line 106
    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/squareup/util/Urls;->isLocalUrl(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/squareup/merchantimages/RealCuratedImage;->thumbor:Lcom/squareup/pollexor/Thumbor;

    invoke-virtual {v1, v0}, Lcom/squareup/pollexor/Thumbor;->buildImage(Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/merchantimages/RealCuratedImage;->imageSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/squareup/merchantimages/RealCuratedImage;->imageSize:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    .line 108
    invoke-virtual {v0, v1, v2}, Lcom/squareup/pollexor/ThumborUrlBuilder;->resize(II)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v3, 0x14

    .line 109
    invoke-static {v3}, Lcom/squareup/pollexor/ThumborUrlBuilder;->contrast(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/squareup/pollexor/ThumborUrlBuilder;->filter([Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toUrl()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method static synthetic lambda$load$4(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 87
    sget-object v0, Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$VK4KkVKte5SXbzgeF4WNKd4BbzI;->INSTANCE:Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$VK4KkVKte5SXbzgeF4WNKd4BbzI;

    invoke-virtual {p0, v0}, Lcom/squareup/util/Optional;->filter(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$loadDarkened$2(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 80
    sget-object v0, Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$eFETec5LdZSTpmWFfvFXjlPV8II;->INSTANCE:Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$eFETec5LdZSTpmWFfvFXjlPV8II;

    invoke-virtual {p0, v0}, Lcom/squareup/util/Optional;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$1(Lcom/squareup/picasso/RequestCreator;)Lcom/squareup/picasso/RequestCreator;
    .locals 2

    .line 81
    new-instance v0, Lcom/squareup/ui/OverlayTransformation;

    const/high16 v1, 0x7f000000

    invoke-direct {v0, v1}, Lcom/squareup/ui/OverlayTransformation;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$3(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 0

    .line 87
    invoke-static {p0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private publishUriFromSettings()V
    .locals 2

    .line 97
    invoke-direct {p0}, Lcom/squareup/merchantimages/RealCuratedImage;->getThumborUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v1, p0, Lcom/squareup/merchantimages/RealCuratedImage;->picasso:Lcom/squareup/merchantimages/SingleImagePicasso;

    invoke-virtual {v1, v0}, Lcom/squareup/merchantimages/SingleImagePicasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/picasso/RequestCreator;->fetch()V

    .line 101
    :cond_0
    iget-object v1, p0, Lcom/squareup/merchantimages/RealCuratedImage;->curatedImageUri:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v0}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public cancelRequest(Landroid/widget/ImageView;)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/merchantimages/RealCuratedImage;->picasso:Lcom/squareup/merchantimages/SingleImagePicasso;

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {v0, p1}, Lcom/squareup/merchantimages/SingleImagePicasso;->cancelRequest(Landroid/widget/ImageView;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$load$6$RealCuratedImage(Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 88
    new-instance v0, Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$RSdTqI6OQM5BsEyLQqas5rpjEaA;

    invoke-direct {v0, p0}, Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$RSdTqI6OQM5BsEyLQqas5rpjEaA;-><init>(Lcom/squareup/merchantimages/RealCuratedImage;)V

    invoke-virtual {p1, v0}, Lcom/squareup/util/Optional;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$5$RealCuratedImage(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/merchantimages/RealCuratedImage;->picasso:Lcom/squareup/merchantimages/SingleImagePicasso;

    invoke-virtual {v0, p1}, Lcom/squareup/merchantimages/SingleImagePicasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$0$RealCuratedImage(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 63
    invoke-direct {p0}, Lcom/squareup/merchantimages/RealCuratedImage;->publishUriFromSettings()V

    return-void
.end method

.method public load()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/picasso/RequestCreator;",
            ">;>;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/merchantimages/RealCuratedImage;->curatedImageUri:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$8znRf1SrJflD5pRe11Iz-ENT8MQ;->INSTANCE:Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$8znRf1SrJflD5pRe11Iz-ENT8MQ;

    .line 87
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$HSkhg6KfcvkFQyUvp5fuGSKTXtw;

    invoke-direct {v1, p0}, Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$HSkhg6KfcvkFQyUvp5fuGSKTXtw;-><init>(Lcom/squareup/merchantimages/RealCuratedImage;)V

    .line 88
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public loadDarkened()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/picasso/RequestCreator;",
            ">;>;"
        }
    .end annotation

    .line 78
    invoke-virtual {p0}, Lcom/squareup/merchantimages/RealCuratedImage;->load()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$GLYYrj0v3oVjj4L3U3TmntCiapw;->INSTANCE:Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$GLYYrj0v3oVjj4L3U3TmntCiapw;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/merchantimages/RealCuratedImage;->picassoFactory:Lcom/squareup/merchantimages/SingleImagePicassoFactory;

    const-string v1, "feature"

    invoke-virtual {v0, v1}, Lcom/squareup/merchantimages/SingleImagePicassoFactory;->create(Ljava/lang/String;)Lcom/squareup/merchantimages/SingleImagePicasso;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/merchantimages/RealCuratedImage;->picasso:Lcom/squareup/merchantimages/SingleImagePicasso;

    .line 61
    iget-object v0, p0, Lcom/squareup/merchantimages/RealCuratedImage;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 62
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$NZuaLaXWwDWpfmskQxtGBAnww10;

    invoke-direct {v1, p0}, Lcom/squareup/merchantimages/-$$Lambda$RealCuratedImage$NZuaLaXWwDWpfmskQxtGBAnww10;-><init>(Lcom/squareup/merchantimages/RealCuratedImage;)V

    .line 63
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 61
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/merchantimages/RealCuratedImage;->picasso:Lcom/squareup/merchantimages/SingleImagePicasso;

    invoke-virtual {v0}, Lcom/squareup/merchantimages/SingleImagePicasso;->shutdown()V

    const/4 v0, 0x0

    .line 68
    iput-object v0, p0, Lcom/squareup/merchantimages/RealCuratedImage;->picasso:Lcom/squareup/merchantimages/SingleImagePicasso;

    return-void
.end method

.method public setLocalUriOverride(Ljava/lang/String;)V
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/merchantimages/RealCuratedImage;->picasso:Lcom/squareup/merchantimages/SingleImagePicasso;

    invoke-virtual {v0, p1}, Lcom/squareup/merchantimages/SingleImagePicasso;->setOverride(Ljava/lang/String;)V

    .line 93
    invoke-direct {p0}, Lcom/squareup/merchantimages/RealCuratedImage;->publishUriFromSettings()V

    return-void
.end method
