.class public interface abstract Lcom/squareup/merchantimages/CuratedImage;
.super Ljava/lang/Object;
.source "CuratedImage.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0014\u0010\u0006\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00080\u0007H&J\u0014\u0010\n\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\t0\u00080\u0007H&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH&\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/merchantimages/CuratedImage;",
        "Lmortar/Scoped;",
        "cancelRequest",
        "",
        "view",
        "Landroid/widget/ImageView;",
        "load",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/picasso/RequestCreator;",
        "loadDarkened",
        "setLocalUriOverride",
        "ui",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract cancelRequest(Landroid/widget/ImageView;)V
.end method

.method public abstract load()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/picasso/RequestCreator;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract loadDarkened()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/picasso/RequestCreator;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract setLocalUriOverride(Ljava/lang/String;)V
.end method
