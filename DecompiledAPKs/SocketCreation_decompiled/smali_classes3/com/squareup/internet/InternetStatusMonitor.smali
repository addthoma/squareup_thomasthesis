.class public interface abstract Lcom/squareup/internet/InternetStatusMonitor;
.super Ljava/lang/Object;
.source "InternetStatusMonitor.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0006\u001a\u00020\u0007H&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/internet/InternetStatusMonitor;",
        "",
        "networkConnection",
        "Lcom/squareup/internet/InternetConnection;",
        "getNetworkConnection",
        "()Lcom/squareup/internet/InternetConnection;",
        "logNetworkConnectionStatus",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getNetworkConnection()Lcom/squareup/internet/InternetConnection;
.end method

.method public abstract logNetworkConnectionStatus()V
.end method
