.class public final Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;
.super Lcom/squareup/mailorder/OrderReactor$OrderState;
.source "OrderReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderReactor$OrderState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FetchingMerchantProfile"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0003H\u00d6\u0001J\u0019\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0014H\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "itemToken",
        "",
        "cardCustomizationOption",
        "Lcom/squareup/mailorder/CardCustomizationOption;",
        "contactInfo",
        "Lcom/squareup/mailorder/ContactInfo;",
        "(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;)V",
        "getCardCustomizationOption",
        "()Lcom/squareup/mailorder/CardCustomizationOption;",
        "getContactInfo",
        "()Lcom/squareup/mailorder/ContactInfo;",
        "getItemToken",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

.field private final contactInfo:Lcom/squareup/mailorder/ContactInfo;

.field private final itemToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile$Creator;

    invoke-direct {v0}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile$Creator;-><init>()V

    sput-object v0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;)V
    .locals 1

    const-string v0, "itemToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardCustomizationOption"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 107
    invoke-direct {p0, v0}, Lcom/squareup/mailorder/OrderReactor$OrderState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->itemToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

    iput-object p3, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;ILjava/lang/Object;)Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getItemToken()Ljava/lang/String;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->copy(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;)Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getItemToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/mailorder/CardCustomizationOption;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/mailorder/ContactInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;)Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;
    .locals 1

    const-string v0, "itemToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardCustomizationOption"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactInfo"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;-><init>(Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;Lcom/squareup/mailorder/ContactInfo;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getItemToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getItemToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

    return-object v0
.end method

.method public final getContactInfo()Lcom/squareup/mailorder/ContactInfo;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    return-object v0
.end method

.method public getItemToken()Ljava/lang/String;
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->itemToken:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getItemToken()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FetchingMerchantProfile(itemToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getItemToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cardCustomizationOption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", contactInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->itemToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->cardCustomizationOption:Lcom/squareup/mailorder/CardCustomizationOption;

    invoke-virtual {p2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;->contactInfo:Lcom/squareup/mailorder/ContactInfo;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    return-void
.end method
