.class public final Lcom/squareup/mailorder/OrderWarningDialog;
.super Ljava/lang/Object;
.source "OrderWarningDialogScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J,\u0010\t\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\nj\u0002`\u000b2\u0006\u0010\u000c\u001a\u00020\u00052\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eR\u001d\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderWarningDialog;",
        "",
        "()V",
        "KEY",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/widgets/warning/Warning;",
        "Lcom/squareup/mailorder/OrderEvent$DialogDismissed;",
        "getKEY",
        "()Lcom/squareup/workflow/legacy/Screen$Key;",
        "createScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/mailorder/OrderWarningDialogScreen;",
        "warning",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/mailorder/OrderEvent;",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/mailorder/OrderWarningDialog;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/widgets/warning/Warning;",
            "Lcom/squareup/mailorder/OrderEvent$DialogDismissed;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 11
    new-instance v0, Lcom/squareup/mailorder/OrderWarningDialog;

    invoke-direct {v0}, Lcom/squareup/mailorder/OrderWarningDialog;-><init>()V

    sput-object v0, Lcom/squareup/mailorder/OrderWarningDialog;->INSTANCE:Lcom/squareup/mailorder/OrderWarningDialog;

    .line 12
    new-instance v1, Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-direct {v1, v0}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/squareup/mailorder/OrderWarningDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createScreen(Lcom/squareup/widgets/warning/Warning;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/widgets/warning/Warning;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/mailorder/OrderEvent;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/widgets/warning/Warning;",
            "Lcom/squareup/mailorder/OrderEvent$DialogDismissed;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "warning"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    sget-object v1, Lcom/squareup/mailorder/OrderWarningDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-direct {v0, v1, p1, p2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method public final getKEY()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/widgets/warning/Warning;",
            "Lcom/squareup/mailorder/OrderEvent$DialogDismissed;",
            ">;"
        }
    .end annotation

    .line 12
    sget-object v0, Lcom/squareup/mailorder/OrderWarningDialog;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method
