.class final Lcom/squareup/mailorder/OrderReactor$onReact$3$3;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/OrderReactor$onReact$3;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "it",
        "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/mailorder/OrderReactor$onReact$3;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/OrderReactor$onReact$3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$3$3;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$3;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$onReact$3$3;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$3;

    iget-object v0, v0, Lcom/squareup/mailorder/OrderReactor$onReact$3;->this$0:Lcom/squareup/mailorder/OrderReactor;

    invoke-static {v0}, Lcom/squareup/mailorder/OrderReactor;->access$getAnalytics$p(Lcom/squareup/mailorder/OrderReactor;)Lcom/squareup/mailorder/MailOrderAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingDetailsContinueClick()V

    .line 262
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    .line 263
    iget-object v1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$3$3;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$3;

    iget-object v1, v1, Lcom/squareup/mailorder/OrderReactor$onReact$3;->this$0:Lcom/squareup/mailorder/OrderReactor;

    .line 265
    iget-object v2, p0, Lcom/squareup/mailorder/OrderReactor$onReact$3$3;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$3;

    iget-object v2, v2, Lcom/squareup/mailorder/OrderReactor$onReact$3;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {v2}, Lcom/squareup/mailorder/OrderReactor$OrderState;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v2

    .line 266
    iget-object v3, p0, Lcom/squareup/mailorder/OrderReactor$onReact$3$3;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$3;

    iget-object v3, v3, Lcom/squareup/mailorder/OrderReactor$onReact$3;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {v3}, Lcom/squareup/mailorder/OrderReactor$OrderState;->getItemToken()Ljava/lang/String;

    move-result-object v3

    .line 267
    iget-object v4, p0, Lcom/squareup/mailorder/OrderReactor$onReact$3$3;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$3;

    iget-object v4, v4, Lcom/squareup/mailorder/OrderReactor$onReact$3;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    check-cast v4, Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;

    invoke-virtual {v4}, Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;->getVerifiedAddressToken()Ljava/lang/String;

    move-result-object v4

    .line 263
    invoke-static {v1, p1, v3, v4, v2}, Lcom/squareup/mailorder/OrderReactor;->access$determineOnSendOrderState(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/CardCustomizationOption;)Lcom/squareup/mailorder/OrderReactor$OrderState;

    move-result-object p1

    .line 262
    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/OrderReactor$onReact$3$3;->invoke(Lcom/squareup/mailorder/OrderEvent$OrderScreenEvent$SendOrder;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
