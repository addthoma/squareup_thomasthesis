.class public final Lcom/squareup/mailorder/UnverifiedAddressCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "UnverifiedAddressCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final configurationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;",
            ">;"
        }
    .end annotation
.end field

.field private final spinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator_Factory_Factory;->spinnerProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator_Factory_Factory;->configurationProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/mailorder/UnverifiedAddressCoordinator_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;",
            ">;)",
            "Lcom/squareup/mailorder/UnverifiedAddressCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator_Factory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;)Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;-><init>(Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator_Factory_Factory;->spinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v1, p0, Lcom/squareup/mailorder/UnverifiedAddressCoordinator_Factory_Factory;->configurationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;

    invoke-static {v0, v1}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator_Factory_Factory;->newInstance(Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Configuration;)Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/mailorder/UnverifiedAddressCoordinator_Factory_Factory;->get()Lcom/squareup/mailorder/UnverifiedAddressCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
