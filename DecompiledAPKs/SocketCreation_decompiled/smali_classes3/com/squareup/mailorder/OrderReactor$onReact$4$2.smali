.class final Lcom/squareup/mailorder/OrderReactor$onReact$4$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mailorder/OrderReactor$onReact$4;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$SubmitCorrectedAddress;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$SubmitCorrectedAddress;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;


# direct methods
.method constructor <init>(Lcom/squareup/mailorder/OrderReactor$onReact$4;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$4$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$SubmitCorrectedAddress;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$SubmitCorrectedAddress;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/mailorder/OrderReactor$OrderState;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$SubmitCorrectedAddress;->component1()Lcom/squareup/mailorder/ContactInfo;

    move-result-object v2

    .line 293
    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$4$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$onReact$4;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    check-cast p1, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;

    invoke-virtual {p1}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getCorrectedAddress()Lcom/squareup/address/Address;

    move-result-object p1

    invoke-virtual {v2}, Lcom/squareup/mailorder/ContactInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 294
    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$4$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$onReact$4;->this$0:Lcom/squareup/mailorder/OrderReactor;

    invoke-static {p1}, Lcom/squareup/mailorder/OrderReactor;->access$getAnalytics$p(Lcom/squareup/mailorder/OrderReactor;)Lcom/squareup/mailorder/MailOrderAnalytics;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingDetailsCorrectedAddressRecommendedSubmitted()V

    goto :goto_0

    .line 296
    :cond_0
    iget-object p1, p0, Lcom/squareup/mailorder/OrderReactor$onReact$4$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;

    iget-object p1, p1, Lcom/squareup/mailorder/OrderReactor$onReact$4;->this$0:Lcom/squareup/mailorder/OrderReactor;

    invoke-static {p1}, Lcom/squareup/mailorder/OrderReactor;->access$getAnalytics$p(Lcom/squareup/mailorder/OrderReactor;)Lcom/squareup/mailorder/MailOrderAnalytics;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/mailorder/MailOrderAnalytics;->logShippingDetailsCorrectedAddressOriginalSubmitted()V

    .line 298
    :goto_0
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    .line 299
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$onReact$4$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;

    iget-object v1, v0, Lcom/squareup/mailorder/OrderReactor$onReact$4;->this$0:Lcom/squareup/mailorder/OrderReactor;

    .line 301
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$onReact$4$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;

    iget-object v0, v0, Lcom/squareup/mailorder/OrderReactor$onReact$4;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {v0}, Lcom/squareup/mailorder/OrderReactor$OrderState;->getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;

    move-result-object v7

    .line 302
    invoke-virtual {v2}, Lcom/squareup/mailorder/ContactInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object v3

    .line 303
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$onReact$4$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;

    iget-object v0, v0, Lcom/squareup/mailorder/OrderReactor$onReact$4;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    invoke-virtual {v0}, Lcom/squareup/mailorder/OrderReactor$OrderState;->getItemToken()Ljava/lang/String;

    move-result-object v4

    .line 304
    iget-object v0, p0, Lcom/squareup/mailorder/OrderReactor$onReact$4$2;->this$0:Lcom/squareup/mailorder/OrderReactor$onReact$4;

    iget-object v0, v0, Lcom/squareup/mailorder/OrderReactor$onReact$4;->$state:Lcom/squareup/mailorder/OrderReactor$OrderState;

    check-cast v0, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;

    invoke-virtual {v0}, Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;->getVerifiedAddressToken()Ljava/lang/String;

    move-result-object v5

    .line 305
    sget-object v6, Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;->SELECT_ADDRESS:Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;

    .line 299
    invoke-static/range {v1 .. v7}, Lcom/squareup/mailorder/OrderReactor;->access$sendAddressRequest(Lcom/squareup/mailorder/OrderReactor;Lcom/squareup/mailorder/ContactInfo;Lcom/squareup/address/Address;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder$OriginationFlow;Lcom/squareup/mailorder/CardCustomizationOption;)Lcom/squareup/mailorder/OrderReactor$OrderState;

    move-result-object v0

    .line 298
    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$SubmitCorrectedAddress;

    invoke-virtual {p0, p1}, Lcom/squareup/mailorder/OrderReactor$onReact$4$2;->invoke(Lcom/squareup/mailorder/OrderEvent$CorrectedAddressEvent$SubmitCorrectedAddress;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
