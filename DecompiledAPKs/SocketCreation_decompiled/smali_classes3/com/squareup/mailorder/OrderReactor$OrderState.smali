.class public abstract Lcom/squareup/mailorder/OrderReactor$OrderState;
.super Ljava/lang/Object;
.source "OrderReactor.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/mailorder/OrderReactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "OrderState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;,
        Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;,
        Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;,
        Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;,
        Lcom/squareup/mailorder/OrderReactor$OrderState$OrderSuccess;,
        Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;,
        Lcom/squareup/mailorder/OrderReactor$OrderState$ConfirmUnverifiedAddress;,
        Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0008\u000b\u000c\r\u000e\u000f\u0010\u0011\u0012B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0008\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/mailorder/OrderReactor$OrderState;",
        "Landroid/os/Parcelable;",
        "()V",
        "cardCustomizationOption",
        "Lcom/squareup/mailorder/CardCustomizationOption;",
        "getCardCustomizationOption",
        "()Lcom/squareup/mailorder/CardCustomizationOption;",
        "itemToken",
        "",
        "getItemToken",
        "()Ljava/lang/String;",
        "ConfirmUnverifiedAddress",
        "CorrectedAddressToSubmit",
        "FetchingMerchantProfile",
        "OrderSuccess",
        "ReadyForUser",
        "SendingOrder",
        "VerifyingAddress",
        "Warning",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$FetchingMerchantProfile;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$ReadyForUser;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$VerifyingAddress;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$SendingOrder;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$OrderSuccess;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$CorrectedAddressToSubmit;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$ConfirmUnverifiedAddress;",
        "Lcom/squareup/mailorder/OrderReactor$OrderState$Warning;",
        "mail-order_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 89
    invoke-direct {p0}, Lcom/squareup/mailorder/OrderReactor$OrderState;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getCardCustomizationOption()Lcom/squareup/mailorder/CardCustomizationOption;
.end method

.method public abstract getItemToken()Ljava/lang/String;
.end method
