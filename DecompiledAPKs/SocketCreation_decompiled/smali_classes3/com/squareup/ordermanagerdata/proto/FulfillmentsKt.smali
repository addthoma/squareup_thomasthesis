.class public final Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt;
.super Ljava/lang/Object;
.source "Fulfillments.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0017\u0010\u0005\u001a\u0004\u0018\u00010\u0006*\u00020\u00078F\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "order",
        "",
        "Lcom/squareup/orders/model/Order$Fulfillment$State;",
        "getOrder",
        "(Lcom/squareup/orders/model/Order$Fulfillment$State;)I",
        "placedAt",
        "",
        "Lcom/squareup/orders/model/Order$Fulfillment;",
        "getPlacedAt",
        "(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getOrder(Lcom/squareup/orders/model/Order$Fulfillment$State;)I
    .locals 1

    const-string v0, "$this$order"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    sget-object v0, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 26
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :pswitch_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Should not receive unknown fulfillment state."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    :pswitch_1
    const/4 p0, 0x5

    goto :goto_0

    :pswitch_2
    const/4 p0, 0x4

    goto :goto_0

    :pswitch_3
    const/4 p0, 0x3

    goto :goto_0

    :pswitch_4
    const/4 p0, 0x2

    goto :goto_0

    :pswitch_5
    const/4 p0, 0x1

    goto :goto_0

    :pswitch_6
    const/4 p0, 0x0

    :goto_0
    return p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static final getPlacedAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;
    .locals 3

    const-string v0, "$this$placedAt"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_4

    const/4 v2, 0x3

    if-eq v0, v2, :cond_3

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_1

    goto :goto_0

    .line 41
    :cond_1
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    if-eqz p0, :cond_6

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->placed_at:Ljava/lang/String;

    goto :goto_0

    .line 40
    :cond_2
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    if-eqz p0, :cond_6

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->placed_at:Ljava/lang/String;

    goto :goto_0

    .line 39
    :cond_3
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz p0, :cond_6

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->placed_at:Ljava/lang/String;

    goto :goto_0

    .line 38
    :cond_4
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz p0, :cond_6

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->placed_at:Ljava/lang/String;

    goto :goto_0

    .line 37
    :cond_5
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    if-eqz p0, :cond_6

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->placed_at:Ljava/lang/String;

    :cond_6
    :goto_0
    return-object v1
.end method
