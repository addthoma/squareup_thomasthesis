.class public final Lcom/squareup/ordermanagerdata/OrderRepository$DefaultImpls;
.super Ljava/lang/Object;
.source "OrderRepository.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ordermanagerdata/OrderRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderRepository.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderRepository.kt\ncom/squareup/ordermanagerdata/OrderRepository$DefaultImpls\n*L\n1#1,223:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static synthetic cancelOrder$default(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/List;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    if-nez p5, :cond_1

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 184
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p3

    :cond_0
    invoke-interface {p0, p1, p2, p3}, Lcom/squareup/ordermanagerdata/OrderRepository;->cancelOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0

    .line 0
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: cancelOrder"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static synthetic shipOrder$default(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/util/List;ILjava/lang/Object;)Lio/reactivex/Single;
    .locals 0

    if-nez p5, :cond_2

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 201
    check-cast p2, Lcom/squareup/ordermanagerdata/TrackingInfo;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 202
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p3

    :cond_1
    invoke-interface {p0, p1, p2, p3}, Lcom/squareup/ordermanagerdata/OrderRepository;->shipOrder(Lcom/squareup/orders/model/Order;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/util/List;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0

    .line 0
    :cond_2
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "Super calls with default arguments not supported in this target, function: shipOrder"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
