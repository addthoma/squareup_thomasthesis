.class public final Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;
.super Ljava/lang/Object;
.source "OrderSyncNotifier.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderSyncNotifier.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderSyncNotifier.kt\ncom/squareup/ordermanagerdata/sync/OrderSyncNotifier\n+ 2 PushMessageDelegate.kt\ncom/squareup/pushmessages/PushMessageDelegateKt\n*L\n1#1,72:1\n16#2:73\n*E\n*S KotlinDebug\n*F\n+ 1 OrderSyncNotifier.kt\ncom/squareup/ordermanagerdata/sync/OrderSyncNotifier\n*L\n32#1:73\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008R\u0016\u0010\u0007\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;",
        "",
        "polling",
        "Lcom/squareup/ordermanagerdata/sync/poll/Polling;",
        "pushDelegate",
        "Lcom/squareup/pushmessages/PushMessageDelegate;",
        "(Lcom/squareup/ordermanagerdata/sync/poll/Polling;Lcom/squareup/pushmessages/PushMessageDelegate;)V",
        "pollEvents",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ordermanagerdata/sync/SyncEvent;",
        "pushEvents",
        "events",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final pollEvents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "+",
            "Lcom/squareup/ordermanagerdata/sync/SyncEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final pushEvents:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/sync/SyncEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ordermanagerdata/sync/poll/Polling;Lcom/squareup/pushmessages/PushMessageDelegate;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "polling"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pushDelegate"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const-class v0, Lcom/squareup/pushmessages/PushMessage$OrdersUpdated;

    invoke-interface {p2, v0}, Lcom/squareup/pushmessages/PushMessageDelegate;->observe(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p2

    .line 33
    sget-object v0, Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier$pushEvents$1;->INSTANCE:Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier$pushEvents$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p2

    const-string v0, "pushDelegate\n        .ob\u2026>()\n        .map { Push }"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;->pushEvents:Lio/reactivex/Observable;

    .line 38
    invoke-interface {p1}, Lcom/squareup/ordermanagerdata/sync/poll/Polling;->sample()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;->pollEvents:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getPollEvents$p(Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;)Lio/reactivex/Observable;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;->pollEvents:Lio/reactivex/Observable;

    return-object p0
.end method


# virtual methods
.method public final events()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/sync/SyncEvent;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;->pushEvents:Lio/reactivex/Observable;

    .line 62
    new-instance v1, Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier$events$1;

    invoke-direct {v1, p0}, Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier$events$1;-><init>(Lcom/squareup/ordermanagerdata/sync/OrderSyncNotifier;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->publish(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 70
    sget-object v1, Lcom/squareup/ordermanagerdata/sync/SyncEvent$Manual;->INSTANCE:Lcom/squareup/ordermanagerdata/sync/SyncEvent$Manual;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "pushEvents\n        .publ\u2026       .startWith(Manual)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
