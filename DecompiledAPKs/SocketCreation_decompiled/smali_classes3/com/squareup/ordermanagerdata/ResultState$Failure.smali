.class public abstract Lcom/squareup/ordermanagerdata/ResultState$Failure;
.super Lcom/squareup/ordermanagerdata/ResultState;
.source "OrderRepository.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ordermanagerdata/ResultState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Failure"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;,
        Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;,
        Lcom/squareup/ordermanagerdata/ResultState$Failure$ConnectionError;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u0004\u0005\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003\u0082\u0001\u0003\u0007\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/ResultState$Failure;",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "()V",
        "ConnectionError",
        "GenericError",
        "VersionMismatchError",
        "Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;",
        "Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;",
        "Lcom/squareup/ordermanagerdata/ResultState$Failure$ConnectionError;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 217
    invoke-direct {p0, v0}, Lcom/squareup/ordermanagerdata/ResultState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 217
    invoke-direct {p0}, Lcom/squareup/ordermanagerdata/ResultState$Failure;-><init>()V

    return-void
.end method
