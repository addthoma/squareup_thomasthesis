.class public final Lcom/squareup/ordermanagerdata/RealOrderManagerClientSupport;
.super Ljava/lang/Object;
.source "RealOrderManagerClientSupport.kt"

# interfaces
.implements Lcom/squareup/ordermanagerdata/OrderManagerClientSupport;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ordermanagerdata/RealOrderManagerClientSupport;",
        "Lcom/squareup/ordermanagerdata/OrderManagerClientSupport;",
        "()V",
        "supportedActions",
        "",
        "Lcom/squareup/protos/client/orders/Action$Type;",
        "getSupportedActions",
        "()Ljava/util/List;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ordermanagerdata/RealOrderManagerClientSupport;

.field private static final supportedActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/Action$Type;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5
    new-instance v0, Lcom/squareup/ordermanagerdata/RealOrderManagerClientSupport;

    invoke-direct {v0}, Lcom/squareup/ordermanagerdata/RealOrderManagerClientSupport;-><init>()V

    sput-object v0, Lcom/squareup/ordermanagerdata/RealOrderManagerClientSupport;->INSTANCE:Lcom/squareup/ordermanagerdata/RealOrderManagerClientSupport;

    .line 6
    sget-object v0, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore;->Companion:Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore$Companion;

    invoke-virtual {v0}, Lcom/squareup/ordermanagerdata/remote/RealRemoteOrderStore$Companion;->getSUPPORTED_ACTIONS$impl_release()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ordermanagerdata/RealOrderManagerClientSupport;->supportedActions:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getSupportedActions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/Action$Type;",
            ">;"
        }
    .end annotation

    .line 6
    sget-object v0, Lcom/squareup/ordermanagerdata/RealOrderManagerClientSupport;->supportedActions:Ljava/util/List;

    return-object v0
.end method
