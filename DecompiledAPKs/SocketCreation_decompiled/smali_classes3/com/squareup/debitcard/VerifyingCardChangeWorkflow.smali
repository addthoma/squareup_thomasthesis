.class public final Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "VerifyingCardChangeWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;,
        Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/debitcard/VerifyCardChangeProps;",
        "Lcom/squareup/debitcard/VerifyingCardChangeOutput;",
        "Lcom/squareup/debitcard/VerifyingCardChangeScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nVerifyingCardChangeWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 VerifyingCardChangeWorkflow.kt\ncom/squareup/debitcard/VerifyingCardChangeWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,102:1\n85#2:103\n240#3:104\n276#4:105\n*E\n*S KotlinDebug\n*F\n+ 1 VerifyingCardChangeWorkflow.kt\ncom/squareup/debitcard/VerifyingCardChangeWorkflow\n*L\n62#1:103\n62#1:104\n62#1:105\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0003\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0001:\u0002\u0019\u001aB\u001f\u0008\u0007\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0016\u0010\u000c\u001a\u00020\r2\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u0002J\u0016\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010\u0014\u001a\u00020\u0002H\u0002J$\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u00022\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u00030\u0017H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/debitcard/VerifyCardChangeProps;",
        "Lcom/squareup/debitcard/VerifyingCardChangeOutput;",
        "Lcom/squareup/debitcard/VerifyingCardChangeScreen;",
        "linkDebitCardService",
        "Lcom/squareup/debitcard/LinkDebitCardService;",
        "messageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "resources",
        "Landroid/content/res/Resources;",
        "(Lcom/squareup/debitcard/LinkDebitCardService;Lcom/squareup/receiving/FailureMessageFactory;Landroid/content/res/Resources;)V",
        "confirmCardFailure",
        "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardFailure;",
        "failure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;",
        "confirmCardWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;",
        "props",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "Action",
        "ConfirmCardResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final linkDebitCardService:Lcom/squareup/debitcard/LinkDebitCardService;

.field private final messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/squareup/debitcard/LinkDebitCardService;Lcom/squareup/receiving/FailureMessageFactory;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "linkDebitCardService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messageFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resources"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;->linkDebitCardService:Lcom/squareup/debitcard/LinkDebitCardService;

    iput-object p2, p0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object p3, p0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;->resources:Landroid/content/res/Resources;

    return-void
.end method

.method public static final synthetic access$confirmCardFailure(Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardFailure;
    .locals 0

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;->confirmCardFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardFailure;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getResources$p(Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;)Landroid/content/res/Resources;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;->resources:Landroid/content/res/Resources;

    return-object p0
.end method

.method private final confirmCardFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardFailure;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;",
            ">;)",
            "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardFailure;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/debitcard/impl/R$string;->verify_card_change_failure_title:I

    new-instance v2, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$confirmCardFailure$failureMessage$1;

    invoke-direct {v2, p0}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$confirmCardFailure$failureMessage$1;-><init>(Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 70
    new-instance v0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardFailure;

    .line 71
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    .line 70
    invoke-direct {v0, v1, p1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardFailure;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private final confirmCardWorker(Lcom/squareup/debitcard/VerifyCardChangeProps;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/debitcard/VerifyCardChangeProps;",
            ")",
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;",
            ">;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/protos/ledger/service/VerifyCardChangeRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/ledger/service/VerifyCardChangeRequest$Builder;-><init>()V

    .line 50
    invoke-virtual {p1}, Lcom/squareup/debitcard/VerifyCardChangeProps;->getConfirmationToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/ledger/service/VerifyCardChangeRequest$Builder;->verification_token(Ljava/lang/String;)Lcom/squareup/protos/ledger/service/VerifyCardChangeRequest$Builder;

    move-result-object v0

    .line 51
    invoke-virtual {p1}, Lcom/squareup/debitcard/VerifyCardChangeProps;->getUnitToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/ledger/service/VerifyCardChangeRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/ledger/service/VerifyCardChangeRequest$Builder;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/protos/ledger/service/VerifyCardChangeRequest$Builder;->build()Lcom/squareup/protos/ledger/service/VerifyCardChangeRequest;

    move-result-object p1

    .line 54
    iget-object v0, p0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;->linkDebitCardService:Lcom/squareup/debitcard/LinkDebitCardService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/debitcard/LinkDebitCardService;->confirmCard(Lcom/squareup/protos/ledger/service/VerifyCardChangeRequest;)Lcom/squareup/debitcard/LinkDebitCardService$VerifyCardChangeStandardResponse;

    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/squareup/debitcard/LinkDebitCardService$VerifyCardChangeStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 56
    new-instance v0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$confirmCardWorker$1;

    invoke-direct {v0, p0}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$confirmCardWorker$1;-><init>(Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "linkDebitCardService.con\u2026it)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$confirmCardWorker$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$confirmCardWorker$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 104
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 105
    const-class v0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v1, Lcom/squareup/workflow/Worker;

    return-object v1
.end method


# virtual methods
.method public render(Lcom/squareup/debitcard/VerifyCardChangeProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/debitcard/VerifyingCardChangeScreen;
    .locals 7

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;->confirmCardWorker(Lcom/squareup/debitcard/VerifyCardChangeProps;)Lcom/squareup/workflow/Worker;

    move-result-object v2

    sget-object p1, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$render$1;->INSTANCE:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$render$1;

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p2

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 45
    sget-object p1, Lcom/squareup/debitcard/VerifyingCardChangeScreen;->INSTANCE:Lcom/squareup/debitcard/VerifyingCardChangeScreen;

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/debitcard/VerifyCardChangeProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;->render(Lcom/squareup/debitcard/VerifyCardChangeProps;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/debitcard/VerifyingCardChangeScreen;

    move-result-object p1

    return-object p1
.end method
