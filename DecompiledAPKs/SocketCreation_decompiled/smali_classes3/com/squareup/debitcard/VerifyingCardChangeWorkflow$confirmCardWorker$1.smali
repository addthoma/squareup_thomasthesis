.class final Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$confirmCardWorker$1;
.super Ljava/lang/Object;
.source "VerifyingCardChangeWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;->confirmCardWorker(Lcom/squareup/debitcard/VerifyCardChangeProps;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$confirmCardWorker$1;->this$0:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/ledger/service/VerifyCardChangeResponse;",
            ">;)",
            "Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardSuccess;->INSTANCE:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardSuccess;

    check-cast p1, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;

    goto :goto_0

    .line 59
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$confirmCardWorker$1;->this$0:Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-static {v0, p1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;->access$confirmCardFailure(Lcom/squareup/debitcard/VerifyingCardChangeWorkflow;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult$ConfirmCardFailure;

    move-result-object p1

    check-cast p1, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$confirmCardWorker$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/debitcard/VerifyingCardChangeWorkflow$ConfirmCardResult;

    move-result-object p1

    return-object p1
.end method
