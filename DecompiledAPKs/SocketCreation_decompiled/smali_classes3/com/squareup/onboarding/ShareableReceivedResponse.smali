.class public final Lcom/squareup/onboarding/ShareableReceivedResponse;
.super Ljava/lang/Object;
.source "ShareableReceivedResponse.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "T:",
        "Lcom/squareup/server/SimpleResponse;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u00020\u0002B\u001f\u0012\u0018\u0010\u0005\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u00070\u0006\u00a2\u0006\u0002\u0010\u0008J\u0013\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0014R \u0010\u0005\u001a\u0014\u0012\u0004\u0012\u00028\u0000\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\t\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00010\u000b0\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR(\u0010\u000e\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00028\u0001 \u0010*\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u000b0\u000b0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/onboarding/ShareableReceivedResponse;",
        "I",
        "",
        "T",
        "Lcom/squareup/server/SimpleResponse;",
        "bodyToServiceCall",
        "Lkotlin/Function1;",
        "Lcom/squareup/server/StandardResponse;",
        "(Lkotlin/jvm/functions/Function1;)V",
        "responses",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "getResponses",
        "()Lio/reactivex/Observable;",
        "responsesRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "kotlin.jvm.PlatformType",
        "send",
        "",
        "body",
        "(Ljava/lang/Object;)V",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bodyToServiceCall:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "TI;",
            "Lcom/squareup/server/StandardResponse<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field private final responses:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;"
        }
    .end annotation
.end field

.field private final responsesRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TI;+",
            "Lcom/squareup/server/StandardResponse<",
            "TT;>;>;)V"
        }
    .end annotation

    const-string v0, "bodyToServiceCall"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/ShareableReceivedResponse;->bodyToServiceCall:Lkotlin/jvm/functions/Function1;

    .line 19
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string v0, "PublishRelay.create<SuccessOrFailure<T>>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/onboarding/ShareableReceivedResponse;->responsesRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 25
    iget-object p1, p0, Lcom/squareup/onboarding/ShareableReceivedResponse;->responsesRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast p1, Lio/reactivex/Observable;

    iput-object p1, p0, Lcom/squareup/onboarding/ShareableReceivedResponse;->responses:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method public final getResponses()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TT;>;>;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/onboarding/ShareableReceivedResponse;->responses:Lio/reactivex/Observable;

    return-object v0
.end method

.method public final send(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TI;)V"
        }
    .end annotation

    const-string v0, "body"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/onboarding/ShareableReceivedResponse;->bodyToServiceCall:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/StandardResponse;

    .line 32
    invoke-virtual {p1}, Lcom/squareup/server/StandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 33
    iget-object v0, p0, Lcom/squareup/onboarding/ShareableReceivedResponse;->responsesRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
