.class public Lcom/squareup/onboarding/BannerButtonResolver;
.super Ljava/lang/Object;
.source "BannerButtonResolver.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBannerButtonResolver.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BannerButtonResolver.kt\ncom/squareup/onboarding/BannerButtonResolver\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Observables\n*L\n1#1,54:1\n57#2,4:55\n*E\n*S KotlinDebug\n*F\n+ 1 BannerButtonResolver.kt\ncom/squareup/onboarding/BannerButtonResolver\n*L\n27#1,4:55\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0017\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/onboarding/BannerButtonResolver;",
        "",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "bankLinkingStarter",
        "Lcom/squareup/banklinking/BankLinkingStarter;",
        "(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankLinkingStarter;)V",
        "bannerButton",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/onboarding/BannerButton;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/banklinking/BankLinkingStarter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accountStatusSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankLinkingStarter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/BannerButtonResolver;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p2, p0, Lcom/squareup/onboarding/BannerButtonResolver;->bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

    return-void
.end method

.method public static final synthetic access$getAccountStatusSettings$p(Lcom/squareup/onboarding/BannerButtonResolver;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/onboarding/BannerButtonResolver;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method


# virtual methods
.method public bannerButton()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/onboarding/BannerButton;",
            ">;"
        }
    .end annotation

    .line 27
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 28
    iget-object v0, p0, Lcom/squareup/onboarding/BannerButtonResolver;->bankLinkingStarter:Lcom/squareup/banklinking/BankLinkingStarter;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankLinkingStarter;->variant()Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 29
    iget-object v1, p0, Lcom/squareup/onboarding/BannerButtonResolver;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "accountStatusSettings.settingsAvailable()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    check-cast v0, Lio/reactivex/ObservableSource;

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 57
    new-instance v2, Lcom/squareup/onboarding/BannerButtonResolver$bannerButton$$inlined$combineLatest$1;

    invoke-direct {v2}, Lcom/squareup/onboarding/BannerButtonResolver$bannerButton$$inlined$combineLatest$1;-><init>()V

    check-cast v2, Lio/reactivex/functions/BiFunction;

    .line 55
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.combineLatest\u2026ineFunction(t1, t2) }\n  )"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v1, Lcom/squareup/onboarding/BannerButtonResolver$bannerButton$2;

    invoke-direct {v1, p0}, Lcom/squareup/onboarding/BannerButtonResolver$bannerButton$2;-><init>(Lcom/squareup/onboarding/BannerButtonResolver;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "combineLatest(\n        b\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
