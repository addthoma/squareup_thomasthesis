.class public final Lcom/squareup/onboarding/OnboardingDeepLinkBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "OnboardingDeepLinkBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0010\u0010\t\u001a\n \u000b*\u0004\u0018\u00010\n0\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/onboarding/OnboardingDeepLinkBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "onboardingDiverter",
        "Lcom/squareup/onboarding/OnboardingDiverter;",
        "(Lcom/squareup/onboarding/OnboardingDiverter;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/ui/main/MainActivityScope;",
        "kotlin.jvm.PlatformType",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/OnboardingDiverter;)V
    .locals 1

    const-string v0, "onboardingDiverter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/OnboardingDeepLinkBootstrapScreen;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object p1, p0, Lcom/squareup/onboarding/OnboardingDeepLinkBootstrapScreen;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    sget-object v0, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->START:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    invoke-interface {p1, v0}, Lcom/squareup/onboarding/OnboardingDiverter;->maybeDivertToOnboardingOrBank(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/main/MainActivityScope;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/ui/main/MainActivityScope;->INSTANCE:Lcom/squareup/ui/main/MainActivityScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/onboarding/OnboardingDeepLinkBootstrapScreen;->getParentKey()Lcom/squareup/ui/main/MainActivityScope;

    move-result-object v0

    return-object v0
.end method
