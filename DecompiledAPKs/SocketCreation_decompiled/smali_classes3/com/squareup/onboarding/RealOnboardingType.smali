.class public final Lcom/squareup/onboarding/RealOnboardingType;
.super Ljava/lang/Object;
.source "RealOnboardingType.kt"

# interfaces
.implements Lcom/squareup/onboarding/OnboardingType;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/onboarding/RealOnboardingType;",
        "Lcom/squareup/onboarding/OnboardingType;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "variant",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/onboarding/OnboardingType$Variant;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/RealOnboardingType;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/onboarding/RealOnboardingType;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method public static final synthetic access$getSettings$p(Lcom/squareup/onboarding/RealOnboardingType;)Lcom/squareup/settings/server/AccountStatusSettings;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/squareup/onboarding/RealOnboardingType;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-object p0
.end method


# virtual methods
.method public variant()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/onboarding/OnboardingType$Variant;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/onboarding/RealOnboardingType;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->ONBOARDING_SERVER_DRIVEN_FLOW:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v0

    .line 25
    new-instance v1, Lcom/squareup/onboarding/RealOnboardingType$variant$1;

    invoke-direct {v1, p0}, Lcom/squareup/onboarding/RealOnboardingType$variant$1;-><init>(Lcom/squareup/onboarding/RealOnboardingType;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "features.featureEnabled(\u2026IVE\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
