.class final Lcom/squareup/onboarding/BannerButtonResolver$bannerButton$2;
.super Ljava/lang/Object;
.source "BannerButtonResolver.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/BannerButtonResolver;->bannerButton()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/onboarding/BannerButton;",
        "variant",
        "Lcom/squareup/banklinking/BankLinkingStarter$Variant;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onboarding/BannerButtonResolver;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/BannerButtonResolver;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/BannerButtonResolver$bannerButton$2;->this$0:Lcom/squareup/onboarding/BannerButtonResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/banklinking/BankLinkingStarter$Variant;)Lcom/squareup/onboarding/BannerButton;
    .locals 2

    const-string/jumbo v0, "variant"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/onboarding/BannerButtonResolver$bannerButton$2;->this$0:Lcom/squareup/onboarding/BannerButtonResolver;

    invoke-static {v0}, Lcom/squareup/onboarding/BannerButtonResolver;->access$getAccountStatusSettings$p(Lcom/squareup/onboarding/BannerButtonResolver;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->showInAppActivationPostSignup()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string p1, "activationFeatures"

    .line 34
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->isEligibleForSquareCardPayments()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 35
    sget-object p1, Lcom/squareup/onboarding/BannerButton$Kind;->ACTIVATE_ACCOUNT:Lcom/squareup/onboarding/BannerButton$Kind;

    goto :goto_0

    .line 37
    :cond_0
    sget-object p1, Lcom/squareup/onboarding/BannerButton$Kind;->FINALIZE_ACCOUNT_SETUP:Lcom/squareup/onboarding/BannerButton$Kind;

    goto :goto_0

    .line 39
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 40
    sget-object v0, Lcom/squareup/onboarding/BannerButtonResolver$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/banklinking/BankLinkingStarter$Variant;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 43
    sget-object p1, Lcom/squareup/onboarding/BannerButton$Kind;->LINK_BANK_ACCOUNT_ON_WEB:Lcom/squareup/onboarding/BannerButton$Kind;

    goto :goto_0

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 42
    :cond_3
    sget-object p1, Lcom/squareup/onboarding/BannerButton$Kind;->LINK_BANK_ACCOUNT_IN_APP:Lcom/squareup/onboarding/BannerButton$Kind;

    goto :goto_0

    .line 41
    :cond_4
    sget-object p1, Lcom/squareup/onboarding/BannerButton$Kind;->NONE:Lcom/squareup/onboarding/BannerButton$Kind;

    goto :goto_0

    .line 47
    :cond_5
    sget-object p1, Lcom/squareup/onboarding/BannerButton$Kind;->NONE:Lcom/squareup/onboarding/BannerButton$Kind;

    .line 49
    :goto_0
    new-instance v0, Lcom/squareup/onboarding/BannerButton;

    invoke-direct {v0, p1}, Lcom/squareup/onboarding/BannerButton;-><init>(Lcom/squareup/onboarding/BannerButton$Kind;)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/banklinking/BankLinkingStarter$Variant;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/BannerButtonResolver$bannerButton$2;->apply(Lcom/squareup/banklinking/BankLinkingStarter$Variant;)Lcom/squareup/onboarding/BannerButton;

    move-result-object p1

    return-object p1
.end method
