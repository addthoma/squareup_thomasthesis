.class final Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forPanel$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingRenderer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/analytics/Analytics;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingRenderer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingRenderer.kt\ncom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forPanel$1\n*L\n1#1,120:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/onboarding/flow/OnboardingEvent;",
        "submissionEvent",
        "Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forPanel$1;->$analytics:Lcom/squareup/analytics/Analytics;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;)Lcom/squareup/onboarding/flow/OnboardingEvent;
    .locals 4

    const-string v0, "submissionEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forPanel$1;->$analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/onboarding/flow/ActionEvent;

    invoke-direct {v2, v0}, Lcom/squareup/onboarding/flow/ActionEvent;-><init>(Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 100
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v1, "skip"

    .line 104
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Skip;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Skip;

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;

    goto :goto_1

    :sswitch_1
    const-string v1, "exit"

    .line 102
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Exit;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Exit;

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;

    goto :goto_1

    :sswitch_2
    const-string v1, "back"

    .line 101
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Back;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Back;

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;

    goto :goto_1

    :sswitch_3
    const-string v1, "finish"

    .line 103
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Finish;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Finish;

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;

    goto :goto_1

    .line 105
    :cond_0
    :goto_0
    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;->getOutputs()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;->getFromTimeout()Z

    move-result v3

    invoke-direct {v1, v0, v2, v3}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    move-object v0, v1

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;

    .line 108
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;->getFromTimeout()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_1

    move-object p1, v0

    goto :goto_2

    :cond_1
    const/4 p1, 0x0

    :goto_2
    if-eqz p1, :cond_2

    goto :goto_3

    :cond_2
    new-instance p1, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;

    invoke-direct {p1, v0}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;-><init>(Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;)V

    :goto_3
    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingEvent;

    return-object p1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x4bf6736d -> :sswitch_3
        0x2e04e7 -> :sswitch_2
        0x2fb91e -> :sswitch_1
        0x35e57f -> :sswitch_0
    .end sparse-switch
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 90
    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingRenderer$ScreenInput$forPanel$1;->invoke(Lcom/squareup/onboarding/flow/OnboardingPanel$Submission;)Lcom/squareup/onboarding/flow/OnboardingEvent;

    move-result-object p1

    return-object p1
.end method
