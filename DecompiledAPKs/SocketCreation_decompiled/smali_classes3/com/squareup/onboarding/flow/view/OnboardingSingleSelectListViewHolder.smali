.class public final Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;
.super Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;
.source "OnboardingSingleSelectListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectAdapter;,
        Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;,
        Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;,
        Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$AbstractRowHolder;,
        Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$RadioRowHolder;,
        Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$ClickRowHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder<",
        "Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingSingleSelectListViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingSingleSelectListViewHolder.kt\ncom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder\n*L\n1#1,159:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\t\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0006\u0010\u0011\u0012\u0013\u0014\u0015B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0014J\u0008\u0010\u000f\u001a\u00020\rH\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;",
        "Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;",
        "Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;",
        "inputHandler",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "parent",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V",
        "header",
        "Lcom/squareup/marketfont/MarketTextView;",
        "list",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "onBindComponent",
        "",
        "component",
        "onHighlightError",
        "AbstractRowHolder",
        "ClickRowHolder",
        "RadioRowHolder",
        "SingleSelectAdapter",
        "SingleSelectClickAdapter",
        "SingleSelectRadioAdapter",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final header:Lcom/squareup/marketfont/MarketTextView;

.field private final list:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;)V
    .locals 2

    const-string v0, "inputHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_component_list:I

    .line 26
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/view/OnboardingComponentViewHolder;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;Landroid/view/ViewGroup;I)V

    .line 30
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->itemView:Landroid/view/View;

    const-string p2, "itemView"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_multi_list_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    .line 31
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->itemView:Landroid/view/View;

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Lcom/squareup/onboarding/flow/R$id;->onboarding_multi_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->list:Landroidx/recyclerview/widget/RecyclerView;

    .line 34
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->list:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v0, 0x0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    .line 35
    iget-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->list:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v0, Lcom/squareup/noho/NohoEdgeDecoration;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->itemView:Landroid/view/View;

    invoke-static {v1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const-string v1, "itemView.resources"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p2}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;)V

    return-void
.end method

.method protected onBindComponent(Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;)V
    .locals 3

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->header:Lcom/squareup/marketfont/MarketTextView;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;->label()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 41
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;->clickAction()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->list:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectRadioAdapter;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;Lcom/squareup/onboarding/flow/OnboardingInputHandler;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    goto :goto_1

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->list:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->getInputHandler()Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$SingleSelectClickAdapter;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;Lcom/squareup/onboarding/flow/OnboardingInputHandler;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    :goto_1
    return-void
.end method

.method public onHighlightError()V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;->list:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->requestFocus()Z

    return-void
.end method
