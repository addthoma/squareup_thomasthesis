.class public final Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder_MembersInjector;
.super Ljava/lang/Object;
.source "OnboardingHardwareViewHolder_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private final picassoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder_MembersInjector;->picassoProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPicasso(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;Lcom/squareup/picasso/Picasso;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;->picasso:Lcom/squareup/picasso/Picasso;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder_MembersInjector;->picassoProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/picasso/Picasso;

    invoke-static {p1, v0}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder_MembersInjector;->injectPicasso(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;Lcom/squareup/picasso/Picasso;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder_MembersInjector;->injectMembers(Lcom/squareup/onboarding/flow/view/OnboardingHardwareViewHolder;)V

    return-void
.end method
