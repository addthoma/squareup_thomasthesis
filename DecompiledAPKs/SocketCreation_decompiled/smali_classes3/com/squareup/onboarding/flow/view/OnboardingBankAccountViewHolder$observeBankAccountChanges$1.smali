.class final Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$observeBankAccountChanges$1;
.super Ljava/lang/Object;
.source "OnboardingBankAccountViewHolder.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->observeBankAccountChanges()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$observeBankAccountChanges$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;)V
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$observeBankAccountChanges$1;->this$0:Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;

    invoke-virtual {p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;->getField()Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;->access$onFieldChanged(Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/view/OnboardingBankAccountViewHolder$observeBankAccountChanges$1;->call(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldChange;)V

    return-void
.end method
