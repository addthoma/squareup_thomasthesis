.class Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$AbstractRowHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "OnboardingSingleSelectListViewHolder.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AbstractRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ">",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\t\u0008\u0012\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u00032\u00020\u0004B\r\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0019\u0010\u000b\u001a\u00028\u0000\u00a2\u0006\u0010\n\u0002\u0010\u0010\u0012\u0004\u0008\u000c\u0010\r\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$AbstractRowHolder;",
        "T",
        "Landroid/view/View;",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;",
        "itemView",
        "(Landroid/view/View;)V",
        "edges",
        "",
        "getEdges",
        "()I",
        "textView",
        "textView$annotations",
        "()V",
        "getTextView",
        "()Landroid/view/View;",
        "Landroid/view/View;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final edges:I

.field private final textView:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 150
    iput-object p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$AbstractRowHolder;->textView:Landroid/view/View;

    const/16 p1, 0xa

    .line 152
    iput p1, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$AbstractRowHolder;->edges:I

    return-void
.end method

.method public static synthetic textView$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public getEdges()I
    .locals 1

    .line 152
    iget v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$AbstractRowHolder;->edges:I

    return v0
.end method

.method public getPadding(Landroid/graphics/Rect;)V
    .locals 1

    const-string v0, "outRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges$DefaultImpls;->getPadding(Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;Landroid/graphics/Rect;)V

    return-void
.end method

.method public final getTextView()Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 150
    iget-object v0, p0, Lcom/squareup/onboarding/flow/view/OnboardingSingleSelectListViewHolder$AbstractRowHolder;->textView:Landroid/view/View;

    return-object v0
.end method
