.class public final Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;
.super Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;
.source "OnboardingEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Submit"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u001a\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;",
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;",
        "actionName",
        "",
        "outputs",
        "",
        "Lcom/squareup/protos/client/onboard/Output;",
        "fromTimeout",
        "",
        "(Ljava/lang/String;Ljava/util/List;Z)V",
        "getActionName",
        "()Ljava/lang/String;",
        "getFromTimeout",
        "()Z",
        "getOutputs",
        "()Ljava/util/List;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionName:Ljava/lang/String;

.field private final fromTimeout:Z

.field private final outputs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "actionName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outputs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;-><init>(Ljava/lang/String;Ljava/util/List;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;->actionName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;->outputs:Ljava/util/List;

    iput-boolean p3, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;->fromTimeout:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/util/List;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 38
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    return-void
.end method


# virtual methods
.method public getActionName()Ljava/lang/String;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;->actionName:Ljava/lang/String;

    return-object v0
.end method

.method public final getFromTimeout()Z
    .locals 1

    .line 38
    iget-boolean v0, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;->fromTimeout:Z

    return v0
.end method

.method public getOutputs()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/Output;",
            ">;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Submit;->outputs:Ljava/util/List;

    return-object v0
.end method
