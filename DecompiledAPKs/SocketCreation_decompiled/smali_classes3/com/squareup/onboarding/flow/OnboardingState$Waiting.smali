.class public final Lcom/squareup/onboarding/flow/OnboardingState$Waiting;
.super Lcom/squareup/onboarding/flow/OnboardingState;
.source "OnboardingState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/OnboardingState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Waiting"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingState$Waiting;",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "()V",
        "history",
        "Lcom/squareup/onboarding/flow/PanelHistory;",
        "getHistory",
        "()Lcom/squareup/onboarding/flow/PanelHistory;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/onboarding/flow/OnboardingState$Waiting;

.field private static final history:Lcom/squareup/onboarding/flow/PanelHistory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 65
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingState$Waiting;

    invoke-direct {v0}, Lcom/squareup/onboarding/flow/OnboardingState$Waiting;-><init>()V

    sput-object v0, Lcom/squareup/onboarding/flow/OnboardingState$Waiting;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingState$Waiting;

    .line 66
    sget-object v0, Lcom/squareup/onboarding/flow/PanelHistory;->Companion:Lcom/squareup/onboarding/flow/PanelHistory$Companion;

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelHistory$Companion;->getEMPTY()Lcom/squareup/onboarding/flow/PanelHistory;

    move-result-object v0

    sput-object v0, Lcom/squareup/onboarding/flow/OnboardingState$Waiting;->history:Lcom/squareup/onboarding/flow/PanelHistory;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 65
    invoke-direct {p0, v0}, Lcom/squareup/onboarding/flow/OnboardingState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public getHistory()Lcom/squareup/onboarding/flow/PanelHistory;
    .locals 1

    .line 66
    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingState$Waiting;->history:Lcom/squareup/onboarding/flow/PanelHistory;

    return-object v0
.end method
