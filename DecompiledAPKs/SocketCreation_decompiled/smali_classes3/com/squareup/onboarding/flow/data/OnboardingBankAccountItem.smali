.class public final Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;
.super Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;
.source "OnboardingBankAccountItem.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u000e\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013J\u0014\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00170\u0015H\u0002J\u0016\u0010\u0018\u001a\u00020\u00112\u0006\u0010\u0019\u001a\u00020\t2\u0006\u0010\u001a\u001a\u00020\u0008J\u000e\u0010\u001b\u001a\u00020\u00082\u0006\u0010\u0019\u001a\u00020\tJ\u0010\u0010\u001c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0014J\u0006\u0010\u001d\u001a\u00020\u001eJ\u0008\u0010\u001f\u001a\u00020\rH\u0002J\u0010\u0010 \u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0002J\u0015\u0010!\u001a\u00020\"*\u00020\u000f2\u0006\u0010\u0019\u001a\u00020\tH\u0082\u0002R\u0018\u0010\u0007\u001a\u00020\u0008*\u00020\t8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;",
        "component",
        "Lcom/squareup/protos/client/onboard/Component;",
        "componentData",
        "Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;",
        "(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V",
        "outputName",
        "",
        "Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;",
        "getOutputName",
        "(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Ljava/lang/String;",
        "caBankValidator",
        "Lcom/squareup/onboarding/flow/data/OnboardingValidator;",
        "outputs",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;",
        "countryOutput",
        "Lcom/squareup/protos/client/onboard/Output;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "defaultOtherValidator",
        "Lkotlin/Function1;",
        "Lcom/squareup/protos/client/onboard/Validator;",
        "",
        "fieldOutput",
        "fieldType",
        "number",
        "getField",
        "getValidatorHelper",
        "label",
        "",
        "unknownBankValidator",
        "usBankValidator",
        "get",
        "Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V
    .locals 1

    const-string v0, "component"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "componentData"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;-><init>(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;)V

    return-void
.end method

.method public static final synthetic access$get(Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->get(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;

    move-result-object p0

    return-object p0
.end method

.method private final caBankValidator(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;
    .locals 8

    .line 53
    new-instance v7, Lcom/squareup/onboarding/flow/data/OnboardingValidator;

    .line 54
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$1;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)V

    move-object v2, v0

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 59
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$caBankValidator$2;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)V

    move-object v1, v0

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 70
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->defaultOtherValidator()Lkotlin/jvm/functions/Function1;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, v7

    .line 53
    invoke-direct/range {v0 .. v6}, Lcom/squareup/onboarding/flow/data/OnboardingValidator;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v7
.end method

.method private final defaultOtherValidator()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/protos/client/onboard/Validator;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 89
    sget-object v0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$defaultOtherValidator$1;->INSTANCE:Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$defaultOtherValidator$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method private final get(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;
    .locals 1

    const-string v0, "$this$get"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-direct {p0, p2}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->getOutputName(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;->get(Ljava/lang/String;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;

    move-result-object p1

    return-object p1
.end method

.method private final getOutputName(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Ljava/lang/String;
    .locals 1

    .line 99
    sget-object v0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const-string p1, "account_number"

    goto :goto_0

    .line 103
    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    const-string/jumbo p1, "transit_number"

    goto :goto_0

    :cond_2
    const-string p1, "institution_number"

    goto :goto_0

    :cond_3
    const-string p1, "routing_number"

    :goto_0
    return-object p1
.end method

.method private final unknownBankValidator()Lcom/squareup/onboarding/flow/data/OnboardingValidator;
    .locals 8

    .line 85
    new-instance v7, Lcom/squareup/onboarding/flow/data/OnboardingValidator;

    .line 86
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->defaultOtherValidator()Lkotlin/jvm/functions/Function1;

    move-result-object v4

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v0, v7

    .line 85
    invoke-direct/range {v0 .. v6}, Lcom/squareup/onboarding/flow/data/OnboardingValidator;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v7
.end method

.method private final usBankValidator(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;
    .locals 8

    .line 73
    new-instance v7, Lcom/squareup/onboarding/flow/data/OnboardingValidator;

    .line 74
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$usBankValidator$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$usBankValidator$1;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)V

    move-object v2, v0

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 78
    new-instance v0, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$usBankValidator$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem$usBankValidator$2;-><init>(Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)V

    move-object v1, v0

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 82
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->defaultOtherValidator()Lkotlin/jvm/functions/Function1;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, v7

    .line 73
    invoke-direct/range {v0 .. v6}, Lcom/squareup/onboarding/flow/data/OnboardingValidator;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v7
.end method


# virtual methods
.method public final countryOutput(Lcom/squareup/CountryCode;)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "countryCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object p1

    const-string v0, "_country"

    invoke-virtual {p0, v0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->createOutput$onboarding_release(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    const-string v0, "createOutput(\"_country\", countryCode.name)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final fieldOutput(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;
    .locals 1

    const-string v0, "fieldType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "number"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->getOutputName(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->createOutput$onboarding_release(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Output;

    move-result-object p1

    const-string p2, "createOutput(fieldType.outputName, number)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final getField(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Ljava/lang/String;
    .locals 3

    const-string v0, "fieldType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->getComponentData()Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->getOutputName(Lcom/squareup/banklinking/widgets/BankAccountFieldsView$FieldType;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lcom/squareup/onboarding/flow/PanelMemory$ForComponent$DefaultImpls;->getString$default(Lcom/squareup/onboarding/flow/PanelMemory$ForComponent;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getValidatorHelper(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;
    .locals 2

    const-string v0, "outputs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "_country"

    .line 46
    invoke-virtual {p1, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;->get(Ljava/lang/String;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->getSubject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/onboard/Output;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Output;->string_value:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 47
    :goto_0
    sget-object v1, Lcom/squareup/CountryCode;->CA:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->caBankValidator(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;

    move-result-object p1

    goto :goto_1

    .line 48
    :cond_1
    sget-object v1, Lcom/squareup/CountryCode;->US:Lcom/squareup/CountryCode;

    invoke-virtual {v1}, Lcom/squareup/CountryCode;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->usBankValidator(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;

    move-result-object p1

    goto :goto_1

    .line 49
    :cond_2
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/data/OnboardingBankAccountItem;->unknownBankValidator()Lcom/squareup/onboarding/flow/data/OnboardingValidator;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method public final label()Ljava/lang/CharSequence;
    .locals 3

    const/4 v0, 0x0

    const-string v1, "label"

    const/4 v2, 0x2

    .line 26
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->stringProperty$default(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method
