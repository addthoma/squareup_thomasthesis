.class final Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem$getValidatorHelper$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingSingleSelectListItem.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem;->getValidatorHelper(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)Lcom/squareup/onboarding/flow/data/OnboardingValidator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $outputs:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem$getValidatorHelper$1;->$outputs:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem$getValidatorHelper$1;->invoke()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Z
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/onboarding/flow/data/OnboardingSingleSelectListItem$getValidatorHelper$1;->$outputs:Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;

    const-string v1, "selected_key"

    invoke-virtual {v0, v1}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputMap;->get(Ljava/lang/String;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem$OutputStatement;->isPresent()Z

    move-result v0

    return v0
.end method
