.class final Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;
.super Ljava/lang/Object;
.source "OnboardingReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/OnboardingReactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ResponsePayload"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0002\u0018\u00002\u00020\u0001B!\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;",
        "",
        "sessionToken",
        "",
        "status",
        "Lcom/squareup/protos/client/Status;",
        "step",
        "Lcom/squareup/protos/client/onboard/Step;",
        "(Ljava/lang/String;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/onboard/Step;)V",
        "getSessionToken",
        "()Ljava/lang/String;",
        "getStatus",
        "()Lcom/squareup/protos/client/Status;",
        "getStep",
        "()Lcom/squareup/protos/client/onboard/Step;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final sessionToken:Ljava/lang/String;

.field private final status:Lcom/squareup/protos/client/Status;

.field private final step:Lcom/squareup/protos/client/onboard/Step;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/Status;Lcom/squareup/protos/client/onboard/Step;)V
    .locals 1

    const-string v0, "status"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;->sessionToken:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;->status:Lcom/squareup/protos/client/Status;

    iput-object p3, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;->step:Lcom/squareup/protos/client/onboard/Step;

    return-void
.end method


# virtual methods
.method public final getSessionToken()Ljava/lang/String;
    .locals 1

    .line 354
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;->sessionToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getStatus()Lcom/squareup/protos/client/Status;
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;->status:Lcom/squareup/protos/client/Status;

    return-object v0
.end method

.method public final getStep()Lcom/squareup/protos/client/onboard/Step;
    .locals 1

    .line 356
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;->step:Lcom/squareup/protos/client/onboard/Step;

    return-object v0
.end method
