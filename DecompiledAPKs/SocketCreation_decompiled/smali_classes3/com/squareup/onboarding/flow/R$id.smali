.class public final Lcom/squareup/onboarding/flow/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final account_holder_field:I = 0x7f0a012a

.field public static final account_number_field:I = 0x7f0a012e

.field public static final account_type_field:I = 0x7f0a0130

.field public static final additional_info_view:I = 0x7f0a0177

.field public static final birth_date:I = 0x7f0a0234

.field public static final birth_date_header:I = 0x7f0a0235

.field public static final business_address_different_as_home:I = 0x7f0a0264

.field public static final business_address_label:I = 0x7f0a0267

.field public static final business_address_mobile_business:I = 0x7f0a0268

.field public static final business_address_mobile_business_label:I = 0x7f0a0269

.field public static final business_address_same_as_home:I = 0x7f0a026a

.field public static final business_address_same_as_home_group:I = 0x7f0a026b

.field public static final business_address_view:I = 0x7f0a026d

.field public static final business_info_business_address:I = 0x7f0a026f

.field public static final business_info_container:I = 0x7f0a0270

.field public static final business_name:I = 0x7f0a0271

.field public static final button:I = 0x7f0a0272

.field public static final debit_card_input_editor:I = 0x7f0a055c

.field public static final deposit_methods:I = 0x7f0a056a

.field public static final deposit_speed_next_business_day:I = 0x7f0a0577

.field public static final deposit_speed_same_day:I = 0x7f0a0579

.field public static final ein_form:I = 0x7f0a06af

.field public static final ein_radios:I = 0x7f0a06b0

.field public static final ein_text:I = 0x7f0a06b1

.field public static final first_button:I = 0x7f0a075a

.field public static final footer:I = 0x7f0a076b

.field public static final full_ssn:I = 0x7f0a0781

.field public static final glyph:I = 0x7f0a07ae

.field public static final glyph_message:I = 0x7f0a07b1

.field public static final has_ein:I = 0x7f0a07c7

.field public static final income_radios:I = 0x7f0a082c

.field public static final instructional_text:I = 0x7f0a0860

.field public static final intent_how_custom:I = 0x7f0a0862

.field public static final intent_how_custom_message:I = 0x7f0a0863

.field public static final intent_how_preset:I = 0x7f0a0864

.field public static final intent_how_preset_message:I = 0x7f0a0865

.field public static final itemization_radios:I = 0x7f0a08e7

.field public static final last_name:I = 0x7f0a0913

.field public static final layout_content:I = 0x7f0a0915

.field public static final location_computer:I = 0x7f0a0959

.field public static final location_invoice:I = 0x7f0a095a

.field public static final location_phone:I = 0x7f0a095c

.field public static final location_radios:I = 0x7f0a095d

.field public static final location_tablet:I = 0x7f0a095f

.field public static final location_website:I = 0x7f0a0960

.field public static final merchant_category_container:I = 0x7f0a09c8

.field public static final message:I = 0x7f0a09d3

.field public static final modified_warning:I = 0x7f0a09e3

.field public static final next_business_day_button:I = 0x7f0a0a22

.field public static final no_ein:I = 0x7f0a0a2a

.field public static final onboarding_address:I = 0x7f0a0a87

.field public static final onboarding_address_title:I = 0x7f0a0a88

.field public static final onboarding_bank_account:I = 0x7f0a0a89

.field public static final onboarding_bank_account_title:I = 0x7f0a0a8a

.field public static final onboarding_button_destructive:I = 0x7f0a0a8b

.field public static final onboarding_button_primary:I = 0x7f0a0a8c

.field public static final onboarding_button_root:I = 0x7f0a0a8d

.field public static final onboarding_button_secondary:I = 0x7f0a0a8e

.field public static final onboarding_date_picker:I = 0x7f0a0a8f

.field public static final onboarding_date_title:I = 0x7f0a0a90

.field public static final onboarding_first_name:I = 0x7f0a0a91

.field public static final onboarding_hardware_disclaimer:I = 0x7f0a0a92

.field public static final onboarding_hardware_image:I = 0x7f0a0a93

.field public static final onboarding_hardware_items:I = 0x7f0a0a94

.field public static final onboarding_hardware_subtitle:I = 0x7f0a0a95

.field public static final onboarding_hardware_title:I = 0x7f0a0a96

.field public static final onboarding_image:I = 0x7f0a0a97

.field public static final onboarding_last_name:I = 0x7f0a0a98

.field public static final onboarding_multi_list:I = 0x7f0a0a99

.field public static final onboarding_multi_list_title:I = 0x7f0a0a9a

.field public static final onboarding_person_name_title:I = 0x7f0a0a9b

.field public static final onboarding_prompt_view:I = 0x7f0a0a9c

.field public static final onboarding_spinner:I = 0x7f0a0a9d

.field public static final onboarding_text_field:I = 0x7f0a0a9e

.field public static final onboarding_text_title:I = 0x7f0a0a9f

.field public static final panel_components:I = 0x7f0a0b9b

.field public static final panel_error:I = 0x7f0a0b9c

.field public static final panel_error_button:I = 0x7f0a0b9d

.field public static final panel_error_message:I = 0x7f0a0b9e

.field public static final panel_error_support:I = 0x7f0a0b9f

.field public static final panel_error_title:I = 0x7f0a0ba0

.field public static final panel_message:I = 0x7f0a0ba1

.field public static final panel_primary_buttons:I = 0x7f0a0ba2

.field public static final panel_root:I = 0x7f0a0ba3

.field public static final panel_success:I = 0x7f0a0ba4

.field public static final paragraph_text:I = 0x7f0a0baa

.field public static final paragraph_title:I = 0x7f0a0bab

.field public static final personal_info_birth_date:I = 0x7f0a0c1c

.field public static final personal_info_first_name:I = 0x7f0a0c1d

.field public static final personal_info_last_4_header:I = 0x7f0a0c1e

.field public static final personal_info_last_name:I = 0x7f0a0c1f

.field public static final personal_info_last_ssn:I = 0x7f0a0c20

.field public static final personal_info_personal_address:I = 0x7f0a0c21

.field public static final personal_info_phone_number:I = 0x7f0a0c22

.field public static final personal_info_view:I = 0x7f0a0c23

.field public static final phone_number:I = 0x7f0a0c25

.field public static final question_divider:I = 0x7f0a0ca1

.field public static final question_radios:I = 0x7f0a0ca5

.field public static final question_title:I = 0x7f0a0ca6

.field public static final radios:I = 0x7f0a0cdb

.field public static final routing_number_field:I = 0x7f0a0d9d

.field public static final same_day_button:I = 0x7f0a0dfc

.field public static final same_day_deposit_description:I = 0x7f0a0dfd

.field public static final scroll_content:I = 0x7f0a0e13

.field public static final second_button:I = 0x7f0a0e2a

.field public static final shipping_address:I = 0x7f0a0e7b

.field public static final shipping_name:I = 0x7f0a0e7c

.field public static final spacer:I = 0x7f0a0eb8

.field public static final street:I = 0x7f0a0f41

.field public static final subtitle:I = 0x7f0a0f49

.field public static final title:I = 0x7f0a103f

.field public static final uncorrectable_warning:I = 0x7f0a10ad

.field public static final unsupported_text_view:I = 0x7f0a10bb

.field public static final vector:I = 0x7f0a10e5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
