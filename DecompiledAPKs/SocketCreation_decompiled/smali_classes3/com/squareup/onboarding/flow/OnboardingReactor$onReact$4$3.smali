.class final Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4$3;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "+",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/onboarding/flow/OnboardingState;",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        "it",
        "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4$3;->this$0:Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/onboarding/flow/OnboardingState;",
            "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4$3;->this$0:Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4;

    iget-object v0, v0, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4;->this$0:Lcom/squareup/onboarding/flow/OnboardingReactor;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4$3;->this$0:Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4;

    iget-object v1, v1, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4;->$state:Lcom/squareup/onboarding/flow/OnboardingState;

    check-cast v1, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;

    invoke-virtual {v1}, Lcom/squareup/onboarding/flow/OnboardingState$ShowingExitDialog;->getContext()Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;->getEvent()Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor;->access$handleEventFromPanel(Lcom/squareup/onboarding/flow/OnboardingReactor;Lcom/squareup/onboarding/flow/OnboardingState$ShowingPanel;Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 71
    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$onReact$4$3;->invoke(Lcom/squareup/onboarding/flow/OnboardingEvent$PanelEvent$Timeout;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
