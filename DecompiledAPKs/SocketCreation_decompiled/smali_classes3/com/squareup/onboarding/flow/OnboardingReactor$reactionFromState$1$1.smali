.class final Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OnboardingReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/legacy/Reaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "TT;",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001\"\u0012\u0008\u0000\u0010\u0002*\u000c\u0012\u0004\u0012\u0002H\u0002\u0012\u0002\u0008\u00030\u00032\u0006\u0010\u0004\u001a\u0002H\u0002H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/receiving/FailureMessage$Parts;",
        "T",
        "Lcom/squareup/wire/Message;",
        "it",
        "invoke",
        "(Lcom/squareup/wire/Message;)Lcom/squareup/receiving/FailureMessage$Parts;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/wire/Message;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/squareup/receiving/FailureMessage$Parts;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 223
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1$1;->this$0:Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;

    iget-object v1, v1, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1;->$payloadParser:Lkotlin/jvm/functions/Function1;

    invoke-interface {v1, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$ResponsePayload;->getStatus()Lcom/squareup/protos/client/Status;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 71
    check-cast p1, Lcom/squareup/wire/Message;

    invoke-virtual {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingReactor$reactionFromState$1$1;->invoke(Lcom/squareup/wire/Message;)Lcom/squareup/receiving/FailureMessage$Parts;

    move-result-object p1

    return-object p1
.end method
