.class public final synthetic Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 16

    invoke-static {}, Lcom/squareup/protos/client/onboard/ComponentType;->values()[Lcom/squareup/protos/client/onboard/ComponentType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->BUTTON:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->SINGLE_SELECT_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->MULTIPLE_SELECT_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->PANEL_TITLE:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->TEXT_FIELD:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->PARAGRAPH:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/4 v7, 0x6

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->PERSON_NAME:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/4 v8, 0x7

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->ADDRESS:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/16 v9, 0x8

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->DATE_PICKER:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/16 v10, 0x9

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->PHONE_NUMBER:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/16 v11, 0xa

    aput v11, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->IMAGE:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/16 v12, 0xb

    aput v12, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->DROPDOWN_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/16 v13, 0xc

    aput v13, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->HARDWARE_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/16 v14, 0xd

    aput v14, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->BANK_ACCOUNT:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    const/16 v15, 0xe

    aput v15, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/onboard/ComponentType;->values()[Lcom/squareup/protos/client/onboard/ComponentType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->BUTTON:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->SINGLE_SELECT_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->MULTIPLE_SELECT_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->PANEL_TITLE:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->TEXT_FIELD:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->PARAGRAPH:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v7, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->PERSON_NAME:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v8, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->ADDRESS:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v9, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->DATE_PICKER:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v10, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->PHONE_NUMBER:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v11, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->IMAGE:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v12, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->DROPDOWN_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v13, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->HARDWARE_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v14, v0, v1

    sget-object v0, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->BANK_ACCOUNT:Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/onboard/ComponentType;->ordinal()I

    move-result v1

    aput v15, v0, v1

    return-void
.end method
