.class public final Lcom/squareup/onboarding/flow/OnboardingPanelView;
.super Landroid/widget/FrameLayout;
.source "OnboardingPanelView.kt"

# interfaces
.implements Lcom/squareup/onboarding/flow/OnboardingInputHandler;
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingPanelView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingPanelView.kt\ncom/squareup/onboarding/flow/OnboardingPanelView\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 4 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,364:1\n1103#2,7:365\n1103#2,7:383\n1360#3:372\n1429#3,3:373\n310#3,7:376\n1550#3,3:390\n66#4:393\n*E\n*S KotlinDebug\n*F\n+ 1 OnboardingPanelView.kt\ncom/squareup/onboarding/flow/OnboardingPanelView\n*L\n118#1,7:365\n315#1,7:383\n203#1:372\n203#1,3:373\n214#1,7:376\n328#1,3:390\n103#1:393\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00cc\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0001SB\u0015\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0008\u0010-\u001a\u00020.H\u0002J\u0008\u0010/\u001a\u00020.H\u0002J\u0010\u00100\u001a\u00020.2\u0006\u00101\u001a\u000202H\u0002J\u0010\u00103\u001a\u0002042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u00105\u001a\u00020.2\u0006\u00106\u001a\u000207H\u0002J\u0010\u00108\u001a\u00020.2\u0006\u00109\u001a\u00020#H\u0002J\u0010\u0010:\u001a\u00020.2\u0006\u00109\u001a\u00020#H\u0002J\u0010\u0010;\u001a\u00020.2\u0006\u00109\u001a\u00020#H\u0002J\u0008\u0010<\u001a\u00020.H\u0014J\u0008\u0010=\u001a\u00020>H\u0016J\u0010\u0010?\u001a\u00020.2\u0006\u0010@\u001a\u00020AH\u0016J\u0008\u0010B\u001a\u00020.H\u0014J\u0008\u0010C\u001a\u00020.H\u0014J\u0010\u0010D\u001a\u00020.2\u0006\u0010E\u001a\u00020FH\u0016J\u0012\u0010G\u001a\u00020.2\u0008\u0010H\u001a\u0004\u0018\u00010IH\u0014J\u0008\u0010J\u001a\u00020IH\u0014J\u0010\u0010K\u001a\u00020.2\u0006\u0010@\u001a\u00020AH\u0002J\u0018\u0010L\u001a\u00020.2\u0006\u0010@\u001a\u00020A2\u0006\u0010M\u001a\u00020>H\u0002J\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020)0NJ\u000e\u0010O\u001a\u00020.2\u0006\u00109\u001a\u00020#J\u0014\u0010P\u001a\u00020.*\u00020Q2\u0006\u0010R\u001a\u00020>H\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0018\u001a\u00020\u00198\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001a\u0010\u001b\"\u0004\u0008\u001c\u0010\u001dR\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010!\u001a\u0010\u0012\u000c\u0012\n $*\u0004\u0018\u00010#0#0\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010(\u001a\u0010\u0012\u000c\u0012\n $*\u0004\u0018\u00010)0)0\"X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010+\u001a\u0004\u0018\u00010,X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006T"
    }
    d2 = {
        "Lcom/squareup/onboarding/flow/OnboardingPanelView;",
        "Landroid/widget/FrameLayout;",
        "Lcom/squareup/onboarding/flow/OnboardingInputHandler;",
        "Lcom/squareup/workflow/ui/HandlesBack;",
        "Lcom/squareup/container/spot/HasSpot;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "adapter",
        "Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;",
        "buttonContainer",
        "Landroid/widget/LinearLayout;",
        "errorButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "errorMessage",
        "Lcom/squareup/widgets/MessageView;",
        "errorTitle",
        "Lcom/squareup/marketfont/MarketTextView;",
        "errorViews",
        "Landroid/view/ViewGroup;",
        "mainThread",
        "Lcom/squareup/thread/executor/MainThread;",
        "getMainThread",
        "()Lcom/squareup/thread/executor/MainThread;",
        "setMainThread",
        "(Lcom/squareup/thread/executor/MainThread;)V",
        "memory",
        "Lcom/squareup/onboarding/flow/PanelMemory;",
        "panelMessage",
        "panelState",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
        "kotlin.jvm.PlatformType",
        "panelViews",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "submissions",
        "Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;",
        "supportLink",
        "timeout",
        "Lcom/squareup/onboarding/flow/PanelTimeout;",
        "bindViews",
        "",
        "clearTimeout",
        "configureActionBar",
        "panel",
        "Lcom/squareup/protos/client/onboard/Panel;",
        "getSpot",
        "Lcom/squareup/container/spot/Spot;",
        "highlightFailedComponent",
        "position",
        "",
        "maybeShowError",
        "screenData",
        "maybeShowPanel",
        "maybeUpdateTimeout",
        "onAttachedToWindow",
        "onBackPressed",
        "",
        "onButtonClicked",
        "action",
        "",
        "onDetachedFromWindow",
        "onFinishInflate",
        "onOutput",
        "output",
        "Lcom/squareup/protos/client/onboard/Output;",
        "onRestoreInstanceState",
        "state",
        "Landroid/os/Parcelable;",
        "onSaveInstanceState",
        "onTimeout",
        "sendSubmission",
        "fromTimeout",
        "Lio/reactivex/Observable;",
        "updateView",
        "fadeInOrOut",
        "Landroid/view/View;",
        "fadeIn",
        "SubmissionData",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final adapter:Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;

.field private buttonContainer:Landroid/widget/LinearLayout;

.field private errorButton:Lcom/squareup/marketfont/MarketButton;

.field private errorMessage:Lcom/squareup/widgets/MessageView;

.field private errorTitle:Lcom/squareup/marketfont/MarketTextView;

.field private errorViews:Landroid/view/ViewGroup;

.field public mainThread:Lcom/squareup/thread/executor/MainThread;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private memory:Lcom/squareup/onboarding/flow/PanelMemory;

.field private panelMessage:Lcom/squareup/widgets/MessageView;

.field private final panelState:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/onboarding/flow/OnboardingScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private panelViews:Landroid/view/ViewGroup;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final submissions:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;",
            ">;"
        }
    .end annotation
.end field

.field private supportLink:Lcom/squareup/widgets/MessageView;

.field private timeout:Lcom/squareup/onboarding/flow/PanelTimeout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    const-string v0, "PublishRelay.create<SubmissionData>()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->submissions:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 73
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    const-string v0, "PublishRelay.create<OnboardingScreenData>()"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->panelState:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 74
    new-instance p2, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;

    move-object v0, p0

    check-cast v0, Lcom/squareup/onboarding/flow/OnboardingInputHandler;

    invoke-direct {p2, v0}, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;-><init>(Lcom/squareup/onboarding/flow/OnboardingInputHandler;)V

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->adapter:Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;

    .line 100
    new-instance p2, Lcom/squareup/onboarding/flow/PanelMemory;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p2, v0, v1, v0}, Lcom/squareup/onboarding/flow/PanelMemory;-><init>(Ljava/util/Collection;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->memory:Lcom/squareup/onboarding/flow/PanelMemory;

    .line 393
    const-class p2, Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;

    .line 104
    invoke-interface {p1, p0}, Lcom/squareup/onboarding/flow/OnboardingWorkflowParentComponent;->inject(Lcom/squareup/onboarding/flow/OnboardingPanelView;)V

    return-void
.end method

.method public static final synthetic access$getPanelState$p(Lcom/squareup/onboarding/flow/OnboardingPanelView;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->panelState:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getRecyclerView$p(Lcom/squareup/onboarding/flow/OnboardingPanelView;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    .line 64
    iget-object p0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p0, :cond_0

    const-string v0, "recyclerView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$maybeShowError(Lcom/squareup/onboarding/flow/OnboardingPanelView;Lcom/squareup/onboarding/flow/OnboardingScreenData;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->maybeShowError(Lcom/squareup/onboarding/flow/OnboardingScreenData;)V

    return-void
.end method

.method public static final synthetic access$maybeShowPanel(Lcom/squareup/onboarding/flow/OnboardingPanelView;Lcom/squareup/onboarding/flow/OnboardingScreenData;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->maybeShowPanel(Lcom/squareup/onboarding/flow/OnboardingScreenData;)V

    return-void
.end method

.method public static final synthetic access$maybeUpdateTimeout(Lcom/squareup/onboarding/flow/OnboardingPanelView;Lcom/squareup/onboarding/flow/OnboardingScreenData;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->maybeUpdateTimeout(Lcom/squareup/onboarding/flow/OnboardingScreenData;)V

    return-void
.end method

.method public static final synthetic access$onTimeout(Lcom/squareup/onboarding/flow/OnboardingPanelView;Ljava/lang/String;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->onTimeout(Ljava/lang/String;)V

    return-void
.end method

.method public static final synthetic access$setRecyclerView$p(Lcom/squareup/onboarding/flow/OnboardingPanelView;Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method private final bindViews()V
    .locals 1

    .line 332
    sget v0, Lcom/squareup/onboarding/flow/R$id;->panel_success:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->panelViews:Landroid/view/ViewGroup;

    .line 333
    sget v0, Lcom/squareup/onboarding/flow/R$id;->panel_components:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 334
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 335
    sget v0, Lcom/squareup/onboarding/flow/R$id;->panel_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->panelMessage:Lcom/squareup/widgets/MessageView;

    .line 336
    sget v0, Lcom/squareup/onboarding/flow/R$id;->panel_primary_buttons:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->buttonContainer:Landroid/widget/LinearLayout;

    .line 338
    sget v0, Lcom/squareup/onboarding/flow/R$id;->panel_error:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->errorViews:Landroid/view/ViewGroup;

    .line 339
    sget v0, Lcom/squareup/onboarding/flow/R$id;->panel_error_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->errorTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 340
    sget v0, Lcom/squareup/onboarding/flow/R$id;->panel_error_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->errorMessage:Lcom/squareup/widgets/MessageView;

    .line 341
    sget v0, Lcom/squareup/onboarding/flow/R$id;->panel_error_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->errorButton:Lcom/squareup/marketfont/MarketButton;

    .line 342
    sget v0, Lcom/squareup/onboarding/flow/R$id;->panel_error_support:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->supportLink:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final clearTimeout()V
    .locals 1

    .line 253
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->timeout:Lcom/squareup/onboarding/flow/PanelTimeout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelTimeout;->cancel()V

    :cond_0
    const/4 v0, 0x0

    .line 254
    check-cast v0, Lcom/squareup/onboarding/flow/PanelTimeout;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->timeout:Lcom/squareup/onboarding/flow/PanelTimeout;

    return-void
.end method

.method private final configureActionBar(Lcom/squareup/protos/client/onboard/Panel;)V
    .locals 21

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 274
    iget-object v2, v1, Lcom/squareup/protos/client/onboard/Panel;->title:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v2, ""

    .line 275
    :goto_0
    iget-object v1, v1, Lcom/squareup/protos/client/onboard/Panel;->navigation:Lcom/squareup/protos/client/onboard/Navigation;

    iget-object v1, v1, Lcom/squareup/protos/client/onboard/Navigation;->navigation_buttons:Ljava/util/List;

    const-string v3, "panel.navigation.navigation_buttons"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    new-instance v3, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 279
    iget-object v4, v0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->buttonContainer:Landroid/widget/LinearLayout;

    const-string v11, "buttonContainer"

    if-nez v4, :cond_1

    invoke-static {v11}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 285
    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v12, 0x1

    xor-int/2addr v4, v12

    if-eqz v4, :cond_2

    new-instance v4, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v4, v2}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    check-cast v4, Lcom/squareup/resources/TextModel;

    invoke-virtual {v3, v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 287
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    const/4 v14, 0x1

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/onboard/NavigationButton;

    .line 288
    iget-object v6, v4, Lcom/squareup/protos/client/onboard/NavigationButton;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    if-nez v6, :cond_3

    goto :goto_3

    :cond_3
    sget-object v7, Lcom/squareup/onboarding/flow/OnboardingPanelView$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v6}, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->ordinal()I

    move-result v6

    aget v6, v7, v6

    if-eq v6, v12, :cond_9

    const/4 v7, 0x2

    if-eq v6, v7, :cond_8

    const/4 v7, 0x3

    if-eq v6, v7, :cond_7

    const/4 v7, 0x4

    if-eq v6, v7, :cond_4

    .line 320
    :goto_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown button style: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v4, Lcom/squareup/protos/client/onboard/NavigationButton;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 306
    :cond_4
    new-instance v6, Lcom/squareup/noho/NohoButton;

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->getContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "context"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x4

    const/16 v20, 0x0

    move-object v15, v6

    move-object/from16 v16, v7

    invoke-direct/range {v15 .. v20}, Lcom/squareup/noho/NohoButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    if-eqz v14, :cond_5

    .line 308
    sget-object v7, Lcom/squareup/noho/NohoButtonType;->PRIMARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {v6, v7}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    goto :goto_4

    .line 310
    :cond_5
    sget-object v7, Lcom/squareup/noho/NohoButtonType;->SECONDARY:Lcom/squareup/noho/NohoButtonType;

    invoke-virtual {v6, v7}, Lcom/squareup/noho/NohoButton;->apply(Lcom/squareup/noho/NohoButtonType;)V

    .line 313
    :goto_4
    sget-object v7, Lcom/squareup/onboarding/flow/OnboardingPanelView$configureActionBar$4;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingPanelView$configureActionBar$4;

    check-cast v7, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v6, v7}, Lcom/squareup/noho/NohoButton;->configureEdges(Lkotlin/jvm/functions/Function1;)V

    .line 314
    iget-object v7, v4, Lcom/squareup/protos/client/onboard/NavigationButton;->label:Ljava/lang/String;

    check-cast v7, Ljava/lang/CharSequence;

    invoke-virtual {v6, v7}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 315
    check-cast v6, Landroid/view/View;

    .line 383
    new-instance v7, Lcom/squareup/onboarding/flow/OnboardingPanelView$configureActionBar$$inlined$onClickDebounced$1;

    invoke-direct {v7, v0, v4}, Lcom/squareup/onboarding/flow/OnboardingPanelView$configureActionBar$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingPanelView;Lcom/squareup/protos/client/onboard/NavigationButton;)V

    check-cast v7, Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 316
    iget-object v4, v0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->buttonContainer:Landroid/widget/LinearLayout;

    if-nez v4, :cond_6

    invoke-static {v11}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v4, v6, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    const/4 v14, 0x0

    goto/16 :goto_2

    .line 300
    :cond_7
    sget-object v5, Lcom/squareup/noho/NohoActionButtonStyle;->SECONDARY:Lcom/squareup/noho/NohoActionButtonStyle;

    new-instance v6, Lcom/squareup/util/ViewString$TextString;

    iget-object v7, v4, Lcom/squareup/protos/client/onboard/NavigationButton;->label:Ljava/lang/String;

    const-string v8, "button.label"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v7, Ljava/lang/CharSequence;

    invoke-direct {v6, v7}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v6, Lcom/squareup/resources/TextModel;

    const/4 v7, 0x0

    new-instance v8, Lcom/squareup/onboarding/flow/OnboardingPanelView$configureActionBar$3;

    invoke-direct {v8, v0, v4}, Lcom/squareup/onboarding/flow/OnboardingPanelView$configureActionBar$3;-><init>(Lcom/squareup/onboarding/flow/OnboardingPanelView;Lcom/squareup/protos/client/onboard/NavigationButton;)V

    check-cast v8, Lkotlin/jvm/functions/Function0;

    const/4 v9, 0x4

    const/4 v10, 0x0

    move-object v4, v3

    invoke-static/range {v4 .. v10}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto/16 :goto_2

    .line 295
    :cond_8
    sget-object v5, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v6, Lcom/squareup/onboarding/flow/OnboardingPanelView$configureActionBar$2;

    invoke-direct {v6, v0, v4}, Lcom/squareup/onboarding/flow/OnboardingPanelView$configureActionBar$2;-><init>(Lcom/squareup/onboarding/flow/OnboardingPanelView;Lcom/squareup/protos/client/onboard/NavigationButton;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v3, v5, v6}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto/16 :goto_2

    .line 290
    :cond_9
    sget-object v5, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v6, Lcom/squareup/onboarding/flow/OnboardingPanelView$configureActionBar$1;

    invoke-direct {v6, v0, v4}, Lcom/squareup/onboarding/flow/OnboardingPanelView$configureActionBar$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingPanelView;Lcom/squareup/protos/client/onboard/NavigationButton;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v3, v5, v6}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto/16 :goto_2

    .line 324
    :cond_a
    iget-object v4, v0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v6, "actionBar"

    if-nez v4, :cond_b

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 328
    iget-object v3, v0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v3, :cond_c

    invoke-static {v6}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    check-cast v3, Landroid/view/View;

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/2addr v2, v12

    if-nez v2, :cond_11

    check-cast v1, Ljava/lang/Iterable;

    .line 390
    instance-of v2, v1, Ljava/util/Collection;

    if-eqz v2, :cond_e

    move-object v2, v1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    :cond_d
    const/4 v1, 0x0

    goto :goto_6

    .line 391
    :cond_e
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/onboard/NavigationButton;

    .line 328
    iget-object v2, v2, Lcom/squareup/protos/client/onboard/NavigationButton;->style:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    sget-object v4, Lcom/squareup/protos/client/onboard/NavigationButton$Style;->PRIMARY:Lcom/squareup/protos/client/onboard/NavigationButton$Style;

    if-eq v2, v4, :cond_10

    const/4 v2, 0x1

    goto :goto_5

    :cond_10
    const/4 v2, 0x0

    :goto_5
    if-eqz v2, :cond_f

    const/4 v1, 0x1

    :goto_6
    if-eqz v1, :cond_12

    :cond_11
    const/4 v5, 0x1

    :cond_12
    invoke-static {v3, v5}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final fadeInOrOut(Landroid/view/View;Z)V
    .locals 2

    .line 346
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-eqz p2, :cond_0

    .line 348
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 350
    :cond_0
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method

.method private final highlightFailedComponent(I)V
    .locals 2

    .line 264
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v1, "recyclerView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollToPosition(I)V

    .line 265
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingPanelView$highlightFailedComponent$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onboarding/flow/OnboardingPanelView$highlightFailedComponent$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingPanelView;I)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private final maybeShowError(Lcom/squareup/onboarding/flow/OnboardingScreenData;)V
    .locals 4

    .line 221
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->errorViews:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const-string v1, "errorViews"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    instance-of v1, p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;

    invoke-direct {p0, v0, v1}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->fadeInOrOut(Landroid/view/View;Z)V

    if-eqz v1, :cond_9

    .line 224
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->errorTitle:Lcom/squareup/marketfont/MarketTextView;

    const-string v1, "errorTitle"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->getErrorTitle()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->errorMessage:Lcom/squareup/widgets/MessageView;

    const-string v2, "errorMessage"

    if-nez v0, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;->getErrorMessage()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    iget-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->errorTitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p1, Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->errorTitle:Lcom/squareup/marketfont/MarketTextView;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "errorTitle.text"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    const/4 v3, 0x0

    if-lez v0, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 228
    iget-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->errorMessage:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->errorMessage:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v2, "errorMessage.text"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_8

    goto :goto_1

    :cond_8
    const/4 v1, 0x0

    :goto_1
    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    :cond_9
    return-void
.end method

.method private final maybeShowPanel(Lcom/squareup/onboarding/flow/OnboardingScreenData;)V
    .locals 7

    .line 198
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->panelViews:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const-string v1, "panelViews"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    instance-of v1, p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Failure;

    xor-int/lit8 v2, v1, 0x1

    invoke-direct {p0, v0, v2}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->fadeInOrOut(Landroid/view/View;Z)V

    if-nez v1, :cond_b

    .line 201
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->configureActionBar(Lcom/squareup/protos/client/onboard/Panel;)V

    .line 203
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/onboard/Panel;->components:Ljava/util/List;

    const-string v1, "screenData.panel.components"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 372
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 373
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 374
    check-cast v2, Lcom/squareup/protos/client/onboard/Component;

    .line 204
    sget-object v3, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;->INSTANCE:Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;

    const-string v4, "it"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->memory:Lcom/squareup/onboarding/flow/PanelMemory;

    invoke-virtual {v3, v2, v4}, Lcom/squareup/onboarding/flow/OnboardingComponentsFactory;->createComponentItem(Lcom/squareup/protos/client/onboard/Component;Lcom/squareup/onboarding/flow/PanelMemory;)Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 375
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 206
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->adapter:Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;->update(Ljava/util/List;)V

    .line 209
    instance-of v0, p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Success;

    const/4 v2, 0x0

    if-nez v0, :cond_2

    move-object p1, v2

    :cond_2
    check-cast p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Success;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData$Success;->getError()Lcom/squareup/onboarding/flow/PanelError;

    move-result-object p1

    if-eqz p1, :cond_3

    goto :goto_1

    :cond_3
    sget-object p1, Lcom/squareup/onboarding/flow/PanelError;->Companion:Lcom/squareup/onboarding/flow/PanelError$Companion;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/PanelError$Companion;->getNONE()Lcom/squareup/onboarding/flow/PanelError;

    move-result-object p1

    .line 210
    :goto_1
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->panelMessage:Lcom/squareup/widgets/MessageView;

    const-string v3, "panelMessage"

    if-nez v0, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/PanelError;->getErrorMessage()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->panelMessage:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/PanelError;->getErrorMessage()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-lez v3, :cond_6

    const/4 v3, 0x1

    goto :goto_2

    :cond_6
    const/4 v3, 0x0

    :goto_2
    invoke-static {v0, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 377
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 378
    check-cast v3, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;

    .line 214
    invoke-virtual {v3}, Lcom/squareup/onboarding/flow/data/OnboardingComponentItem;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/PanelError;->getComponentName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    goto :goto_4

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_8
    const/4 v1, -0x1

    .line 382
    :goto_4
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 215
    move-object v0, p1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    if-ltz v0, :cond_9

    const/4 v4, 0x1

    :cond_9
    if-eqz v4, :cond_a

    goto :goto_5

    :cond_a
    move-object p1, v2

    :goto_5
    if-eqz p1, :cond_b

    .line 216
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->highlightFailedComponent(I)V

    :cond_b
    return-void
.end method

.method private final maybeUpdateTimeout(Lcom/squareup/onboarding/flow/OnboardingScreenData;)V
    .locals 9

    .line 234
    instance-of v0, p1, Lcom/squareup/onboarding/flow/OnboardingScreenData$Success;

    if-nez v0, :cond_0

    return-void

    .line 236
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/onboarding/flow/OnboardingScreenData;->getPanel()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Panel;->navigation:Lcom/squareup/protos/client/onboard/Navigation;

    .line 238
    iget-object v0, p1, Lcom/squareup/protos/client/onboard/Navigation;->timeout_duration:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 239
    iget-object v2, p1, Lcom/squareup/protos/client/onboard/Navigation;->timeout_action:Ljava/lang/String;

    const-string/jumbo p1, "timeout_action"

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    iget-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->timeout:Lcom/squareup/onboarding/flow/PanelTimeout;

    if-eqz p1, :cond_3

    goto :goto_2

    :cond_3
    new-instance p1, Lcom/squareup/onboarding/flow/PanelTimeout;

    const-wide/16 v5, 0x0

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v1, p1

    invoke-direct/range {v1 .. v8}, Lcom/squareup/onboarding/flow/PanelTimeout;-><init>(Ljava/lang/String;JJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_2
    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->timeout:Lcom/squareup/onboarding/flow/PanelTimeout;

    .line 244
    iget-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->timeout:Lcom/squareup/onboarding/flow/PanelTimeout;

    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->mainThread:Lcom/squareup/thread/executor/MainThread;

    if-nez v0, :cond_4

    const-string v1, "mainThread"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Lcom/squareup/thread/executor/SerialExecutor;

    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingPanelView$maybeUpdateTimeout$$inlined$with$lambda$1;

    invoke-direct {v1, p0}, Lcom/squareup/onboarding/flow/OnboardingPanelView$maybeUpdateTimeout$$inlined$with$lambda$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingPanelView;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/onboarding/flow/PanelTimeout;->schedule(Lcom/squareup/thread/executor/SerialExecutor;Lkotlin/jvm/functions/Function1;)V

    :cond_5
    return-void
.end method

.method private final onTimeout(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 259
    invoke-direct {p0, p1, v0}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->sendSubmission(Ljava/lang/String;Z)V

    return-void
.end method

.method private final sendSubmission(Ljava/lang/String;Z)V
    .locals 3

    .line 193
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->clearTimeout()V

    .line 194
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->submissions:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;

    iget-object v2, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->memory:Lcom/squareup/onboarding/flow/PanelMemory;

    invoke-virtual {v2}, Lcom/squareup/onboarding/flow/PanelMemory;->outputs()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, p1, v2, p2}, Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getMainThread()Lcom/squareup/thread/executor/MainThread;
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->mainThread:Lcom/squareup/thread/executor/MainThread;

    if-nez v0, :cond_0

    const-string v1, "mainThread"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    sget-object p1, Lcom/squareup/container/spot/Spots;->RIGHT_STABLE_ACTION_BAR:Lcom/squareup/container/spot/Spot;

    const-string v0, "RIGHT_STABLE_ACTION_BAR"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 153
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 155
    new-instance v0, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;

    invoke-direct {v0, p0}, Lcom/squareup/onboarding/flow/OnboardingPanelView$onAttachedToWindow$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingPanelView;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    const-string v0, "back"

    .line 146
    invoke-virtual {p0, v0}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->onButtonClicked(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onButtonClicked(Ljava/lang/String;)V
    .locals 1

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 177
    invoke-direct {p0, p1, v0}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->sendSubmission(Ljava/lang/String;Z)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 168
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 169
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->timeout:Lcom/squareup/onboarding/flow/PanelTimeout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/onboarding/flow/PanelTimeout;->cancel()V

    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 108
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 109
    invoke-direct {p0}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->bindViews()V

    .line 111
    new-instance v0, Lcom/squareup/ui/DividerDecoration;

    .line 112
    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$dimen;->noho_spacing_large:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 111
    invoke-direct {v0, v1}, Lcom/squareup/ui/DividerDecoration;-><init>(I)V

    .line 114
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v2, "recyclerView"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v1, 0x0

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->adapter:Lcom/squareup/onboarding/flow/OnboardingComponentsAdapter;

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->errorButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_3

    const-string v1, "errorButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    .line 365
    new-instance v1, Lcom/squareup/onboarding/flow/OnboardingPanelView$onFinishInflate$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/onboarding/flow/OnboardingPanelView$onFinishInflate$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/onboarding/flow/OnboardingPanelView;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->supportLink:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_4

    const-string v1, "supportLink"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 120
    :cond_4
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/onboarding/flow/OnboardingPanelView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 121
    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_panel_error_support:I

    const-string v3, "support_center"

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 122
    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_help_url:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 123
    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_panel_error_link:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 124
    sget v2, Lcom/squareup/noho/R$color;->noho_text_help_link:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->linkColor(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 125
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onOutput(Lcom/squareup/protos/client/onboard/Output;)V
    .locals 1

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->memory:Lcom/squareup/onboarding/flow/PanelMemory;

    invoke-virtual {v0, p1}, Lcom/squareup/onboarding/flow/PanelMemory;->plusAssign(Lcom/squareup/protos/client/onboard/Output;)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .line 135
    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 136
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void

    .line 140
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "super_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "memory"

    .line 141
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v0, Lcom/squareup/onboarding/flow/PanelMemory;

    iput-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->memory:Lcom/squareup/onboarding/flow/PanelMemory;

    const-string/jumbo v0, "timeout"

    .line 142
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/onboarding/flow/PanelTimeout;

    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->timeout:Lcom/squareup/onboarding/flow/PanelTimeout;

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 128
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 129
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    const-string v2, "super_state"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 130
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->memory:Lcom/squareup/onboarding/flow/PanelMemory;

    check-cast v1, Landroid/os/Parcelable;

    const-string v2, "memory"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 131
    iget-object v1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->timeout:Lcom/squareup/onboarding/flow/PanelTimeout;

    check-cast v1, Landroid/os/Parcelable;

    const-string/jumbo v2, "timeout"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 128
    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public final setMainThread(Lcom/squareup/thread/executor/MainThread;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iput-object p1, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-void
.end method

.method public final submissions()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/onboarding/flow/OnboardingPanelView$SubmissionData;",
            ">;"
        }
    .end annotation

    .line 181
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->submissions:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public final updateView(Lcom/squareup/onboarding/flow/OnboardingScreenData;)V
    .locals 1

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/squareup/onboarding/flow/OnboardingPanelView;->panelState:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
