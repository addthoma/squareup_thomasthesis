.class public final Lcom/squareup/onboarding/flow/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/flow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final activate_via_web_view:I = 0x7f0d0027

.field public static final activate_your_account_view:I = 0x7f0d0028

.field public static final activation_retry_view:I = 0x7f0d0029

.field public static final additional_info_view:I = 0x7f0d004d

.field public static final business_address_view:I = 0x7f0d009c

.field public static final business_info_view:I = 0x7f0d009d

.field public static final card_processing_not_activated_view:I = 0x7f0d00b9

.field public static final category_view_content:I = 0x7f0d00db

.field public static final confirm_identity_question:I = 0x7f0d0102

.field public static final confirm_identity_view:I = 0x7f0d0103

.field public static final confirm_magstripe_address_view:I = 0x7f0d0104

.field public static final deposit_bank_linking_view:I = 0x7f0d01ba

.field public static final deposit_card_linking_view:I = 0x7f0d01bb

.field public static final deposit_linking_result_view:I = 0x7f0d01bc

.field public static final deposit_speed_view:I = 0x7f0d01c0

.field public static final intent_how_view:I = 0x7f0d02d7

.field public static final intent_where_view:I = 0x7f0d02d8

.field public static final loading_view:I = 0x7f0d0337

.field public static final merchant_category_view:I = 0x7f0d0351

.field public static final merchant_subcategory_view:I = 0x7f0d0357

.field public static final onboarding_component_address:I = 0x7f0d03b2

.field public static final onboarding_component_bank_account:I = 0x7f0d03b3

.field public static final onboarding_component_button:I = 0x7f0d03b4

.field public static final onboarding_component_click_list_item:I = 0x7f0d03b5

.field public static final onboarding_component_date_picker:I = 0x7f0d03b6

.field public static final onboarding_component_dropdown:I = 0x7f0d03b7

.field public static final onboarding_component_hardware_list:I = 0x7f0d03b8

.field public static final onboarding_component_image:I = 0x7f0d03b9

.field public static final onboarding_component_list:I = 0x7f0d03ba

.field public static final onboarding_component_multi_list_item:I = 0x7f0d03bb

.field public static final onboarding_component_paragraph:I = 0x7f0d03bc

.field public static final onboarding_component_person_name:I = 0x7f0d03bd

.field public static final onboarding_component_single_list_item:I = 0x7f0d03be

.field public static final onboarding_component_text_field:I = 0x7f0d03bf

.field public static final onboarding_component_title:I = 0x7f0d03c0

.field public static final onboarding_component_unsupported_view:I = 0x7f0d03c1

.field public static final onboarding_container:I = 0x7f0d03c2

.field public static final onboarding_error_view:I = 0x7f0d03c3

.field public static final onboarding_finished_view:I = 0x7f0d03c4

.field public static final onboarding_panel_view:I = 0x7f0d03c5

.field public static final onboarding_prompt_view_content:I = 0x7f0d03c6

.field public static final onboarding_silent_exit:I = 0x7f0d03c7

.field public static final personal_info_view:I = 0x7f0d0446


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
