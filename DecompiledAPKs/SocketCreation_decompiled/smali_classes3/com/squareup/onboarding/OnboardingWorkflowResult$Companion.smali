.class public final Lcom/squareup/onboarding/OnboardingWorkflowResult$Companion;
.super Ljava/lang/Object;
.source "OnboardingWorkflowResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onboarding/OnboardingWorkflowResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/onboarding/OnboardingWorkflowResult$Companion;",
        "",
        "()V",
        "fromTerminalEvent",
        "Lcom/squareup/onboarding/OnboardingWorkflowResult;",
        "event",
        "Lcom/squareup/protos/client/onboard/TerminalEvent;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/onboarding/OnboardingWorkflowResult$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromTerminalEvent(Lcom/squareup/protos/client/onboard/TerminalEvent;)Lcom/squareup/onboarding/OnboardingWorkflowResult;
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    sget-object v0, Lcom/squareup/onboarding/OnboardingWorkflowResult$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/onboard/TerminalEvent;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 30
    sget-object p1, Lcom/squareup/onboarding/OnboardingWorkflowResult$Unsupported;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Unsupported;

    check-cast p1, Lcom/squareup/onboarding/OnboardingWorkflowResult;

    goto :goto_0

    .line 31
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown TerminalEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 29
    :cond_1
    sget-object p1, Lcom/squareup/onboarding/OnboardingWorkflowResult$Cancelled;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Cancelled;

    check-cast p1, Lcom/squareup/onboarding/OnboardingWorkflowResult;

    goto :goto_0

    .line 28
    :cond_2
    sget-object p1, Lcom/squareup/onboarding/OnboardingWorkflowResult$Errored;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Errored;

    check-cast p1, Lcom/squareup/onboarding/OnboardingWorkflowResult;

    goto :goto_0

    .line 27
    :cond_3
    sget-object p1, Lcom/squareup/onboarding/OnboardingWorkflowResult$Finished;->INSTANCE:Lcom/squareup/onboarding/OnboardingWorkflowResult$Finished;

    check-cast p1, Lcom/squareup/onboarding/OnboardingWorkflowResult;

    :goto_0
    return-object p1
.end method
