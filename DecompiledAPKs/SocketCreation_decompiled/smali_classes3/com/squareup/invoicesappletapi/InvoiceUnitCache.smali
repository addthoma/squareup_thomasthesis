.class public interface abstract Lcom/squareup/invoicesappletapi/InvoiceUnitCache;
.super Ljava/lang/Object;
.source "InvoiceUnitCache.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0006H\'J\u0008\u0010\u0007\u001a\u00020\u0008H\'J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\'J\u0008\u0010\u000c\u001a\u00020\rH\'J\u0008\u0010\u000e\u001a\u00020\u000fH\'J\u0008\u0010\u0010\u001a\u00020\u0006H\'J\u0008\u0010\u0011\u001a\u00020\u0012H\'J\u0014\u0010\u0013\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u000f0\n0\u0014H&J\u0008\u0010\u0015\u001a\u00020\u0016H&J\u0014\u0010\u0017\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00180\n0\u0014H&J\u0008\u0010\u0019\u001a\u00020\u0016H&J\u000e\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u0014H&J\u0010\u0010\u001c\u001a\u00020\u00162\u0006\u0010\u001d\u001a\u00020\u0008H&\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
        "Lmortar/Scoped;",
        "forceGetSettings",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;",
        "getAutomaticReminderCountLimit",
        "",
        "getCurrentDefaultMessage",
        "",
        "getDefaultReminderConfigs",
        "",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
        "getFileAttachmentLimits",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;",
        "getInvoiceDefaults",
        "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
        "getReminderCharacterLimit",
        "hasInvoices",
        "",
        "invoiceDefaultsList",
        "Lio/reactivex/Observable;",
        "maybeRefreshFromServer",
        "",
        "metrics",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
        "setHasInvoicesTrue",
        "unitMetadataDisplayDetails",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
        "updateDefaultMessage",
        "defaultMessage",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract forceGetSettings()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoicesappletapi/GetInvoiceSettingsStatus;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAutomaticReminderCountLimit()I
    .annotation runtime Lkotlin/Deprecated;
        message = "Do not use getters! Subscribe to unitMetadataDisplayDetails() instead."
    .end annotation
.end method

.method public abstract getCurrentDefaultMessage()Ljava/lang/String;
    .annotation runtime Lkotlin/Deprecated;
        message = "Do not use getters! Subscribe to invoiceDefaultsList() instead."
    .end annotation
.end method

.method public abstract getDefaultReminderConfigs()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderConfig;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Do not use getters! Subscribe to unitMetadataDisplayDetails() instead."
    .end annotation
.end method

.method public abstract getFileAttachmentLimits()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;
    .annotation runtime Lkotlin/Deprecated;
        message = "Do not use getters! Subscribe to unitMetadataDisplayDetails() instead."
    .end annotation
.end method

.method public abstract getInvoiceDefaults()Lcom/squareup/protos/client/invoice/InvoiceDefaults;
    .annotation runtime Lkotlin/Deprecated;
        message = "Do not use getters! Subscribe to invoiceDefaultsList() instead."
    .end annotation
.end method

.method public abstract getReminderCharacterLimit()I
    .annotation runtime Lkotlin/Deprecated;
        message = "Do not use getters! Subscribe to unitMetadataDisplayDetails() instead."
    .end annotation
.end method

.method public abstract hasInvoices()Z
    .annotation runtime Lkotlin/Deprecated;
        message = "Do not use getters! Subscribe to unitMetadataDisplayDetails() instead."
    .end annotation
.end method

.method public abstract invoiceDefaultsList()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDefaults;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract maybeRefreshFromServer()V
.end method

.method public abstract metrics()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract setHasInvoicesTrue()V
.end method

.method public abstract unitMetadataDisplayDetails()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateDefaultMessage(Ljava/lang/String;)V
.end method
