.class public final Lcom/squareup/firebase/fcm/FcmService;
.super Lcom/google/firebase/messaging/FirebaseMessagingService;
.source "FcmService.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFcmService.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FcmService.kt\ncom/squareup/firebase/fcm/FcmService\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,37:1\n52#2:38\n*E\n*S KotlinDebug\n*F\n+ 1 FcmService.kt\ncom/squareup/firebase/fcm/FcmService\n*L\n17#1:38\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0010\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u001e\u0010\u0003\u001a\u00020\u00048\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/firebase/fcm/FcmService;",
        "Lcom/google/firebase/messaging/FirebaseMessagingService;",
        "()V",
        "fcmPushService",
        "Lcom/squareup/firebase/fcm/FcmPushService;",
        "getFcmPushService$fcm_release",
        "()Lcom/squareup/firebase/fcm/FcmPushService;",
        "setFcmPushService$fcm_release",
        "(Lcom/squareup/firebase/fcm/FcmPushService;)V",
        "onCreate",
        "",
        "onMessageReceived",
        "message",
        "Lcom/google/firebase/messaging/RemoteMessage;",
        "onNewToken",
        "token",
        "",
        "fcm_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public fcmPushService:Lcom/squareup/firebase/fcm/FcmPushService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/google/firebase/messaging/FirebaseMessagingService;-><init>()V

    return-void
.end method


# virtual methods
.method public final getFcmPushService$fcm_release()Lcom/squareup/firebase/fcm/FcmPushService;
    .locals 2

    .line 13
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmService;->fcmPushService:Lcom/squareup/firebase/fcm/FcmPushService;

    if-nez v0, :cond_0

    const-string v1, "fcmPushService"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .line 16
    invoke-super {p0}, Lcom/google/firebase/messaging/FirebaseMessagingService;->onCreate()V

    .line 17
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "appContext()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    const-class v1, Lcom/squareup/firebase/fcm/FcmComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/firebase/fcm/FcmComponent;

    .line 18
    invoke-interface {v0, p0}, Lcom/squareup/firebase/fcm/FcmComponent;->inject(Lcom/squareup/firebase/fcm/FcmService;)V

    return-void
.end method

.method public onMessageReceived(Lcom/google/firebase/messaging/RemoteMessage;)V
    .locals 2

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-super {p0, p1}, Lcom/google/firebase/messaging/FirebaseMessagingService;->onMessageReceived(Lcom/google/firebase/messaging/RemoteMessage;)V

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[PUSH][FCM] Message received: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", data entries: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/RemoteMessage;->getData()Ljava/util/Map;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    new-instance v0, Lcom/squareup/push/PushServiceMessage;

    .line 31
    invoke-virtual {p1}, Lcom/google/firebase/messaging/RemoteMessage;->getMessageId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const-string v1, "-1"

    .line 32
    :goto_1
    invoke-virtual {p1}, Lcom/google/firebase/messaging/RemoteMessage;->getData()Ljava/util/Map;

    move-result-object p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object p1

    .line 30
    :goto_2
    invoke-direct {v0, v1, p1}, Lcom/squareup/push/PushServiceMessage;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    .line 34
    iget-object p1, p0, Lcom/squareup/firebase/fcm/FcmService;->fcmPushService:Lcom/squareup/firebase/fcm/FcmPushService;

    if-nez p1, :cond_3

    const-string v1, "fcmPushService"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1, v0}, Lcom/squareup/firebase/fcm/FcmPushService;->sendMessage$fcm_release(Lcom/squareup/push/PushServiceMessage;)V

    return-void
.end method

.method public onNewToken(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "token"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmService;->fcmPushService:Lcom/squareup/firebase/fcm/FcmPushService;

    if-nez v0, :cond_0

    const-string v1, "fcmPushService"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/firebase/fcm/FcmPushService;->setPushToken$fcm_release(Ljava/lang/String;)V

    return-void
.end method

.method public final setFcmPushService$fcm_release(Lcom/squareup/firebase/fcm/FcmPushService;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmService;->fcmPushService:Lcom/squareup/firebase/fcm/FcmPushService;

    return-void
.end method
