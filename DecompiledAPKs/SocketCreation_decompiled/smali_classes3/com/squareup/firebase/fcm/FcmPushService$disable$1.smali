.class final Lcom/squareup/firebase/fcm/FcmPushService$disable$1;
.super Ljava/lang/Object;
.source "FcmPushService.kt"

# interfaces
.implements Lio/reactivex/functions/Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/firebase/fcm/FcmPushService;->disable()Lio/reactivex/Completable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/firebase/fcm/FcmPushService;


# direct methods
.method constructor <init>(Lcom/squareup/firebase/fcm/FcmPushService;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/firebase/fcm/FcmPushService$disable$1;->this$0:Lcom/squareup/firebase/fcm/FcmPushService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .line 57
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService$disable$1;->this$0:Lcom/squareup/firebase/fcm/FcmPushService;

    invoke-virtual {v0}, Lcom/squareup/firebase/fcm/FcmPushService;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "[PUSH][FCM] Turning auto init off."

    .line 58
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    iget-object v1, p0, Lcom/squareup/firebase/fcm/FcmPushService$disable$1;->this$0:Lcom/squareup/firebase/fcm/FcmPushService;

    invoke-static {v1}, Lcom/squareup/firebase/fcm/FcmPushService;->access$getFirebaseMessaging$p(Lcom/squareup/firebase/fcm/FcmPushService;)Lcom/google/firebase/messaging/FirebaseMessaging;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/firebase/messaging/FirebaseMessaging;->setAutoInitEnabled(Z)V

    .line 60
    iget-object v0, p0, Lcom/squareup/firebase/fcm/FcmPushService$disable$1;->this$0:Lcom/squareup/firebase/fcm/FcmPushService;

    invoke-static {v0}, Lcom/squareup/firebase/fcm/FcmPushService;->access$getFirebaseInstanceId$p(Lcom/squareup/firebase/fcm/FcmPushService;)Lcom/google/firebase/iid/FirebaseInstanceId;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/iid/FirebaseInstanceId;->deleteInstanceId()V

    :cond_0
    return-void
.end method
