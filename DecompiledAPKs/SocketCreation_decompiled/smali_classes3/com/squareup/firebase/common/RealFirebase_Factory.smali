.class public final Lcom/squareup/firebase/common/RealFirebase_Factory;
.super Ljava/lang/Object;
.source "RealFirebase_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/firebase/common/RealFirebase;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final googleApiAvailabilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/common/GoogleApiAvailability;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/common/GoogleApiAvailability;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/firebase/common/RealFirebase_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/firebase/common/RealFirebase_Factory;->googleApiAvailabilityProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/firebase/common/RealFirebase_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/android/gms/common/GoogleApiAvailability;",
            ">;)",
            "Lcom/squareup/firebase/common/RealFirebase_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/firebase/common/RealFirebase_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/firebase/common/RealFirebase_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/app/Application;Lcom/google/android/gms/common/GoogleApiAvailability;)Lcom/squareup/firebase/common/RealFirebase;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/firebase/common/RealFirebase;

    invoke-direct {v0, p0, p1}, Lcom/squareup/firebase/common/RealFirebase;-><init>(Landroid/app/Application;Lcom/google/android/gms/common/GoogleApiAvailability;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/firebase/common/RealFirebase;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/firebase/common/RealFirebase_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/firebase/common/RealFirebase_Factory;->googleApiAvailabilityProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/GoogleApiAvailability;

    invoke-static {v0, v1}, Lcom/squareup/firebase/common/RealFirebase_Factory;->newInstance(Landroid/app/Application;Lcom/google/android/gms/common/GoogleApiAvailability;)Lcom/squareup/firebase/common/RealFirebase;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/firebase/common/RealFirebase_Factory;->get()Lcom/squareup/firebase/common/RealFirebase;

    move-result-object v0

    return-object v0
.end method
