.class final Lcom/squareup/locale/RealLocaleChangedNotifier$locale$1;
.super Ljava/lang/Object;
.source "RealLocaleChangedNotifier.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/locale/RealLocaleChangedNotifier;-><init>(Landroid/app/Application;Lio/reactivex/Scheduler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Ljava/util/Locale;",
        "it",
        "Landroid/content/Intent;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/locale/RealLocaleChangedNotifier;


# direct methods
.method constructor <init>(Lcom/squareup/locale/RealLocaleChangedNotifier;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/locale/RealLocaleChangedNotifier$locale$1;->this$0:Lcom/squareup/locale/RealLocaleChangedNotifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/squareup/locale/RealLocaleChangedNotifier$locale$1;->apply(Landroid/content/Intent;)Ljava/util/Locale;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Landroid/content/Intent;)Ljava/util/Locale;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    iget-object p1, p0, Lcom/squareup/locale/RealLocaleChangedNotifier$locale$1;->this$0:Lcom/squareup/locale/RealLocaleChangedNotifier;

    invoke-static {p1}, Lcom/squareup/locale/RealLocaleChangedNotifier;->access$getLocale(Lcom/squareup/locale/RealLocaleChangedNotifier;)Ljava/util/Locale;

    move-result-object p1

    return-object p1
.end method
