.class public final enum Lcom/squareup/dipper/events/BleErrorType;
.super Ljava/lang/Enum;
.source "BleErrorType.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/dipper/events/BleErrorType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0008\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/dipper/events/BleErrorType;",
        "",
        "(Ljava/lang/String;I)V",
        "UNKNOWN_ERROR_TYPE",
        "OLD_SERVICE_CACHED",
        "SERVICE_VERSION_INCOMPATIBLE",
        "TIMEOUT",
        "TOO_MANY_RECONNECT_ATTEMPTS",
        "UNABLE_TO_CREATE_BOND",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/dipper/events/BleErrorType;

.field public static final enum OLD_SERVICE_CACHED:Lcom/squareup/dipper/events/BleErrorType;

.field public static final enum SERVICE_VERSION_INCOMPATIBLE:Lcom/squareup/dipper/events/BleErrorType;

.field public static final enum TIMEOUT:Lcom/squareup/dipper/events/BleErrorType;

.field public static final enum TOO_MANY_RECONNECT_ATTEMPTS:Lcom/squareup/dipper/events/BleErrorType;

.field public static final enum UNABLE_TO_CREATE_BOND:Lcom/squareup/dipper/events/BleErrorType;

.field public static final enum UNKNOWN_ERROR_TYPE:Lcom/squareup/dipper/events/BleErrorType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/dipper/events/BleErrorType;

    new-instance v1, Lcom/squareup/dipper/events/BleErrorType;

    const/4 v2, 0x0

    const-string v3, "UNKNOWN_ERROR_TYPE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleErrorType;->UNKNOWN_ERROR_TYPE:Lcom/squareup/dipper/events/BleErrorType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleErrorType;

    const/4 v2, 0x1

    const-string v3, "OLD_SERVICE_CACHED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleErrorType;->OLD_SERVICE_CACHED:Lcom/squareup/dipper/events/BleErrorType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleErrorType;

    const/4 v2, 0x2

    const-string v3, "SERVICE_VERSION_INCOMPATIBLE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleErrorType;->SERVICE_VERSION_INCOMPATIBLE:Lcom/squareup/dipper/events/BleErrorType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleErrorType;

    const/4 v2, 0x3

    const-string v3, "TIMEOUT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleErrorType;->TIMEOUT:Lcom/squareup/dipper/events/BleErrorType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleErrorType;

    const/4 v2, 0x4

    const-string v3, "TOO_MANY_RECONNECT_ATTEMPTS"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleErrorType;->TOO_MANY_RECONNECT_ATTEMPTS:Lcom/squareup/dipper/events/BleErrorType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/dipper/events/BleErrorType;

    const/4 v2, 0x5

    const-string v3, "UNABLE_TO_CREATE_BOND"

    invoke-direct {v1, v3, v2}, Lcom/squareup/dipper/events/BleErrorType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/dipper/events/BleErrorType;->UNABLE_TO_CREATE_BOND:Lcom/squareup/dipper/events/BleErrorType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/dipper/events/BleErrorType;->$VALUES:[Lcom/squareup/dipper/events/BleErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/dipper/events/BleErrorType;
    .locals 1

    const-class v0, Lcom/squareup/dipper/events/BleErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/dipper/events/BleErrorType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/dipper/events/BleErrorType;
    .locals 1

    sget-object v0, Lcom/squareup/dipper/events/BleErrorType;->$VALUES:[Lcom/squareup/dipper/events/BleErrorType;

    invoke-virtual {v0}, [Lcom/squareup/dipper/events/BleErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/dipper/events/BleErrorType;

    return-object v0
.end method
