.class public final Lcom/squareup/log/deposits/DebitVerificationEmailEventKt;
.super Ljava/lang/Object;
.source "DebitVerificationEmailEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0003"
    }
    d2 = {
        "CATALOG_NAME",
        "",
        "RESEND_EMAIL_ATTEMPT",
        "feature-analytics_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "deposits_debit_verification_email"

.field private static final RESEND_EMAIL_ATTEMPT:Ljava/lang/String; = "Settings Instant Deposit: Resend Email Attempt"
