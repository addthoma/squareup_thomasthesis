.class public final enum Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
.super Ljava/lang/Enum;
.source "CheckoutInformationEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/fastcheckout/CheckoutInformationEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TenderType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckoutInformationEvent.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckoutInformationEvent.kt\ncom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType\n*L\n1#1,56:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u000e\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u0006\u0010\u0005\u001a\u00020\u0004J\u0006\u0010\u0006\u001a\u00020\u0004j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;",
        "",
        "(Ljava/lang/String;I)V",
        "dashedCardLoggingName",
        "",
        "fullNameForLogging",
        "simpleName",
        "CARD",
        "CARD_NP",
        "CASH",
        "OTHER",
        "NO_SALE",
        "ZERO_AMOUNT",
        "SPLIT",
        "INVOICE",
        "ON_FILE",
        "EMONEY",
        "NONE",
        "feature-analytics_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

.field public static final enum CARD:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

.field public static final enum CARD_NP:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

.field public static final enum CASH:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

.field public static final enum EMONEY:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

.field public static final enum INVOICE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

.field public static final enum NONE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

.field public static final enum NO_SALE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

.field public static final enum ON_FILE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

.field public static final enum OTHER:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

.field public static final enum SPLIT:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

.field public static final enum ZERO_AMOUNT:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    new-instance v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    const/4 v2, 0x0

    const-string v3, "CARD"

    invoke-direct {v1, v3, v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CARD:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    const/4 v2, 0x1

    const-string v3, "CARD_NP"

    invoke-direct {v1, v3, v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CARD_NP:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    const/4 v2, 0x2

    const-string v3, "CASH"

    invoke-direct {v1, v3, v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CASH:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    const/4 v2, 0x3

    const-string v3, "OTHER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->OTHER:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    const/4 v2, 0x4

    const-string v3, "NO_SALE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->NO_SALE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    const/4 v2, 0x5

    const-string v3, "ZERO_AMOUNT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ZERO_AMOUNT:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    const/4 v2, 0x6

    const-string v3, "SPLIT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->SPLIT:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    const/4 v2, 0x7

    const-string v3, "INVOICE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->INVOICE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    const/16 v2, 0x8

    const-string v3, "ON_FILE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ON_FILE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    const/16 v2, 0x9

    const-string v3, "EMONEY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->EMONEY:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    const/16 v2, 0xa

    const-string v3, "NONE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->NONE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->$VALUES:[Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    .locals 1

    const-class v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;
    .locals 1

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->$VALUES:[Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v0}, [Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    return-object v0
.end method


# virtual methods
.method public final dashedCardLoggingName()Ljava/lang/String;
    .locals 3

    .line 22
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Locale.US"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string v0, "card - CNP"

    return-object v0

    :cond_2
    const-string v0, "card - CP"

    return-object v0
.end method

.method public final fullNameForLogging()Ljava/lang/String;
    .locals 3

    .line 35
    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p0}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 46
    invoke-virtual {p0}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->name()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Locale.US"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :pswitch_0
    const-string v0, "Emoney"

    return-object v0

    :pswitch_1
    const-string v0, "Zero Amount"

    return-object v0

    :pswitch_2
    const-string v0, "Split Tender"

    return-object v0

    :pswitch_3
    const-string v0, "Other"

    return-object v0

    :pswitch_4
    const-string v0, "Card On File"

    return-object v0

    :pswitch_5
    const-string v0, "No Sale"

    return-object v0

    :pswitch_6
    const-string v0, "Invoice"

    return-object v0

    :pswitch_7
    const-string v0, "Cash"

    return-object v0

    :pswitch_8
    const-string v0, "Card - NP"

    return-object v0

    :pswitch_9
    const-string v0, "Card - CP"

    return-object v0

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final simpleName()Ljava/lang/String;
    .locals 2

    .line 52
    move-object v0, p0

    check-cast v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    sget-object v1, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CARD_NP:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->CARD:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v0}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method
