.class public Lcom/squareup/log/accuracy/ZeroTenderEvent;
.super Lcom/squareup/log/accuracy/LocalTenderEvent;
.source "ZeroTenderEvent.java"


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/ZeroTender;Lcom/squareup/payment/BillPayment;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/log/accuracy/LocalTenderEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseLocalTender;Lcom/squareup/payment/BillPayment;)V

    return-void
.end method


# virtual methods
.method protected paymentType()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "zero"

    return-object v0
.end method
