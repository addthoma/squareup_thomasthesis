.class public Lcom/squareup/log/accuracy/CashTenderEvent;
.super Lcom/squareup/log/accuracy/LocalTenderEvent;
.source "CashTenderEvent.java"


# direct methods
.method public constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/CashTender;Lcom/squareup/payment/BillPayment;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/log/accuracy/LocalTenderEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterPaymentAccuracyName;Lcom/squareup/payment/tender/BaseLocalTender;Lcom/squareup/payment/BillPayment;)V

    return-void
.end method


# virtual methods
.method protected paymentType()Ljava/lang/String;
    .locals 1

    const-string v0, "cash"

    return-object v0
.end method
