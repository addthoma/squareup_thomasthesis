.class public Lcom/squareup/log/cart/AllTaxesEvent;
.super Lcom/squareup/log/cart/CartInteractionEvent;
.source "AllTaxesEvent.java"


# instance fields
.field private final available_taxes_count:I


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;JLcom/squareup/analytics/RegisterTapName;I)V
    .locals 7

    .line 13
    sget-object v1, Lcom/squareup/analytics/RegisterTimingName;->CART_INTERACTION_ALL_TAXES:Lcom/squareup/analytics/RegisterTimingName;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/log/cart/CartInteractionEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterViewName;JLcom/squareup/analytics/RegisterTapName;)V

    .line 14
    iput p6, p0, Lcom/squareup/log/cart/AllTaxesEvent;->available_taxes_count:I

    return-void
.end method
