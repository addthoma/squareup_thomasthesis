.class public Lcom/squareup/log/RegisterExceptionHandler$Deps;
.super Ljava/lang/Object;
.source "RegisterExceptionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/RegisterExceptionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Deps"
.end annotation


# instance fields
.field final additionalLoggers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/log/CrashAdditionalLogger;",
            ">;"
        }
    .end annotation
.end field

.field final anrChaperone:Lcom/squareup/anrchaperone/AnrChaperone;

.field final featureFlagsForLogs:Lcom/squareup/log/FeatureFlagsForLogs;


# direct methods
.method constructor <init>(Ljava/util/Set;Lcom/squareup/log/FeatureFlagsForLogs;Lcom/squareup/anrchaperone/AnrChaperone;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/log/CrashAdditionalLogger;",
            ">;",
            "Lcom/squareup/log/FeatureFlagsForLogs;",
            "Lcom/squareup/anrchaperone/AnrChaperone;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/squareup/log/RegisterExceptionHandler$Deps;->additionalLoggers:Ljava/util/Set;

    .line 59
    iput-object p2, p0, Lcom/squareup/log/RegisterExceptionHandler$Deps;->featureFlagsForLogs:Lcom/squareup/log/FeatureFlagsForLogs;

    .line 60
    iput-object p3, p0, Lcom/squareup/log/RegisterExceptionHandler$Deps;->anrChaperone:Lcom/squareup/anrchaperone/AnrChaperone;

    return-void
.end method
