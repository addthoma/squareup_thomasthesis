.class public final enum Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;
.super Ljava/lang/Enum;
.source "AppUpgradeDetector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/AppUpgradeDetector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpgradeInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

.field public static final enum FIRST_START_AFTER_FRESH_INSTALL:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

.field public static final enum FIRST_START_AFTER_UPGRADE:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

.field public static final enum NORMAL_START:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 28
    new-instance v0, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    const/4 v1, 0x0

    const-string v2, "FIRST_START_AFTER_FRESH_INSTALL"

    invoke-direct {v0, v2, v1}, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->FIRST_START_AFTER_FRESH_INSTALL:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    .line 29
    new-instance v0, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    const/4 v2, 0x1

    const-string v3, "FIRST_START_AFTER_UPGRADE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->FIRST_START_AFTER_UPGRADE:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    .line 30
    new-instance v0, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    const/4 v3, 0x2

    const-string v4, "NORMAL_START"

    invoke-direct {v0, v4, v3}, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->NORMAL_START:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    .line 27
    sget-object v4, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->FIRST_START_AFTER_FRESH_INSTALL:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->FIRST_START_AFTER_UPGRADE:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->NORMAL_START:Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->$VALUES:[Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;
    .locals 1

    .line 27
    const-class v0, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    return-object p0
.end method

.method public static values()[Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;
    .locals 1

    .line 27
    sget-object v0, Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->$VALUES:[Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    invoke-virtual {v0}, [Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/log/AppUpgradeDetector$UpgradeInfo;

    return-object v0
.end method
