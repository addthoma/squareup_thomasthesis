.class public Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;
.super Lcom/squareup/log/ReaderEvent$Builder;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private firmwareComponentVersions:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1171
    invoke-direct {p0}, Lcom/squareup/log/ReaderEvent$Builder;-><init>()V

    const-string v0, ""

    .line 1172
    iput-object v0, p0, Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;->firmwareComponentVersions:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;)Ljava/lang/String;
    .locals 0

    .line 1171
    iget-object p0, p0, Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;->firmwareComponentVersions:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic buildReaderEvent()Lcom/squareup/log/ReaderEvent;
    .locals 1

    .line 1171
    invoke-virtual {p0}, Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;->buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent;

    move-result-object v0

    return-object v0
.end method

.method public buildReaderEvent()Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent;
    .locals 1

    .line 1187
    new-instance v0, Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent;

    invoke-direct {v0, p0}, Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent;-><init>(Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;)V

    return-object v0
.end method

.method public firmwareComponentVersions([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;
    .locals 5

    .line 1175
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1177
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    .line 1178
    invoke-virtual {v3}, Lcom/squareup/cardreader/FirmwareAssetVersionInfo;->getFirmwareAsset()Lcom/squareup/cardreader/lcr/CrFirmwareAsset;

    move-result-object v4

    invoke-virtual {v3}, Lcom/squareup/cardreader/FirmwareAssetVersionInfo;->getVersion()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1181
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/log/ReaderEventLogger$FwVersionsReaderEvent$Builder;->firmwareComponentVersions:Ljava/lang/String;

    return-object p0
.end method
