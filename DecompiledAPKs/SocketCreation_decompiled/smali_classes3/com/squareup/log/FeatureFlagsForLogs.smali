.class public final Lcom/squareup/log/FeatureFlagsForLogs;
.super Ljava/lang/Object;
.source "FeatureFlagsForLogs.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFeatureFlagsForLogs.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FeatureFlagsForLogs.kt\ncom/squareup/log/FeatureFlagsForLogs\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,70:1\n3595#2:71\n4012#2,2:72\n*E\n*S KotlinDebug\n*F\n+ 1 FeatureFlagsForLogs.kt\ncom/squareup/log/FeatureFlagsForLogs\n*L\n68#1:71\n68#1,2:72\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B!\u0008\u0007\u0012\u0010\u0008\u0001\u0010\u0002\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u001c\u0010\u0008\u001a\u00060\tj\u0002`\n2\u000e\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\nH\u0002J\u001e\u0010\u000c\u001a\u00060\tj\u0002`\n2\u0010\u0008\u0002\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\nH\u0007J\u001c\u0010\r\u001a\u00060\tj\u0002`\n2\u000e\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\nH\u0002J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u0002J\u001e\u0010\u000e\u001a\u00060\tj\u0002`\n2\u0010\u0008\u0002\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\nH\u0007J$\u0010\u0011\u001a\u00060\tj\u0002`\n2\u0006\u0010\u0012\u001a\u00020\u00132\u000e\u0010\u000b\u001a\n\u0018\u00010\tj\u0004\u0018\u0001`\nH\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0002\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/log/FeatureFlagsForLogs;",
        "",
        "userId",
        "Ljavax/inject/Provider;",
        "",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;)V",
        "abbreviatedFeaturesList",
        "Ljava/lang/StringBuilder;",
        "Lkotlin/text/StringBuilder;",
        "stringBuilder",
        "allFeatures",
        "completeFeaturesList",
        "enabledFeatures",
        "",
        "Lcom/squareup/settings/server/Features$Feature;",
        "featuresList",
        "abbreviated",
        "",
        "crash-reporting_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final userId:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .param p1    # Ljavax/inject/Provider;
        .annotation runtime Lcom/squareup/user/MaybeUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "userId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/log/FeatureFlagsForLogs;->userId:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/log/FeatureFlagsForLogs;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private final abbreviatedFeaturesList(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 10

    if-eqz p1, :cond_0

    goto :goto_0

    .line 52
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    :goto_0
    invoke-direct {p0}, Lcom/squareup/log/FeatureFlagsForLogs;->enabledFeatures()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/Iterable;

    const-string v0, ","

    move-object v2, v0

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v0, Lcom/squareup/log/FeatureFlagsForLogs$abbreviatedFeaturesList$1;->INSTANCE:Lcom/squareup/log/FeatureFlagsForLogs$abbreviatedFeaturesList$1;

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/16 v8, 0x1e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "appendTarget.append(enab\u2026rator = \",\") { it.name })"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public static synthetic allFeatures$default(Lcom/squareup/log/FeatureFlagsForLogs;Ljava/lang/StringBuilder;ILjava/lang/Object;)Ljava/lang/StringBuilder;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 37
    check-cast p1, Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/log/FeatureFlagsForLogs;->allFeatures(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object p0

    return-object p0
.end method

.method private final completeFeaturesList(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 5

    if-eqz p1, :cond_0

    goto :goto_0

    .line 57
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    :goto_0
    invoke-static {}, Lcom/squareup/settings/server/Features$Feature;->values()[Lcom/squareup/settings/server/Features$Feature;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    const-string v4, "\n"

    .line 60
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    invoke-virtual {v3}, Lcom/squareup/settings/server/Features$Feature;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ": "

    .line 62
    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    iget-object v4, p0, Lcom/squareup/log/FeatureFlagsForLogs;->features:Lcom/squareup/settings/server/Features;

    invoke-interface {v4, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "enabled"

    goto :goto_2

    :cond_1
    const-string v3, "disabled"

    :goto_2
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    return-object p1
.end method

.method private final enabledFeatures()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/settings/server/Features$Feature;",
            ">;"
        }
    .end annotation

    .line 68
    invoke-static {}, Lcom/squareup/settings/server/Features$Feature;->values()[Lcom/squareup/settings/server/Features$Feature;

    move-result-object v0

    .line 71
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 72
    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    .line 68
    iget-object v5, p0, Lcom/squareup/log/FeatureFlagsForLogs;->features:Lcom/squareup/settings/server/Features;

    invoke-interface {v5, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 73
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public static synthetic enabledFeatures$default(Lcom/squareup/log/FeatureFlagsForLogs;Ljava/lang/StringBuilder;ILjava/lang/Object;)Ljava/lang/StringBuilder;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 25
    check-cast p1, Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/log/FeatureFlagsForLogs;->enabledFeatures(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object p0

    return-object p0
.end method

.method private final featuresList(ZLjava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/log/FeatureFlagsForLogs;->userId:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, ""

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    if-eqz p1, :cond_1

    .line 47
    invoke-direct {p0, p2}, Lcom/squareup/log/FeatureFlagsForLogs;->abbreviatedFeaturesList(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object p1

    goto :goto_0

    .line 48
    :cond_1
    invoke-direct {p0, p2}, Lcom/squareup/log/FeatureFlagsForLogs;->completeFeaturesList(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object p1

    :goto_0
    return-object p1
.end method


# virtual methods
.method public final allFeatures()Ljava/lang/StringBuilder;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, v0}, Lcom/squareup/log/FeatureFlagsForLogs;->allFeatures$default(Lcom/squareup/log/FeatureFlagsForLogs;Ljava/lang/StringBuilder;ILjava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final allFeatures(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 1

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, v0, p1}, Lcom/squareup/log/FeatureFlagsForLogs;->featuresList(ZLjava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object p1

    return-object p1
.end method

.method public final enabledFeatures()Ljava/lang/StringBuilder;
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1, v0}, Lcom/squareup/log/FeatureFlagsForLogs;->enabledFeatures$default(Lcom/squareup/log/FeatureFlagsForLogs;Ljava/lang/StringBuilder;ILjava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public final enabledFeatures(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 1

    const/4 v0, 0x1

    .line 26
    invoke-direct {p0, v0, p1}, Lcom/squareup/log/FeatureFlagsForLogs;->featuresList(ZLjava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object p1

    return-object p1
.end method
