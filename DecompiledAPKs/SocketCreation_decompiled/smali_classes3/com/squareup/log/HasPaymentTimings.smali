.class public interface abstract Lcom/squareup/log/HasPaymentTimings;
.super Ljava/lang/Object;
.source "HasPaymentTimings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/log/HasPaymentTimings$PaymentTimingsTypeAdapter;,
        Lcom/squareup/log/HasPaymentTimings$PaymentTimingsTypeAdapterFactory;
    }
.end annotation


# virtual methods
.method public abstract getPaymentTimings()Lcom/squareup/cardreader/PaymentTimings;
.end method
