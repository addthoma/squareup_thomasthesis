.class public final Lcom/squareup/log/terminal/ReaderEventBusBoy_Factory;
.super Ljava/lang/Object;
.source "ReaderEventBusBoy_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/log/terminal/ReaderEventBusBoy;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/log/terminal/ReaderEventBusBoy_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/log/terminal/ReaderEventBusBoy_Factory;->swipeBusProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/log/terminal/ReaderEventBusBoy_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/SwipeBus;",
            ">;)",
            "Lcom/squareup/log/terminal/ReaderEventBusBoy_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/log/terminal/ReaderEventBusBoy_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/log/terminal/ReaderEventBusBoy_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/wavpool/swipe/SwipeBus;)Lcom/squareup/log/terminal/ReaderEventBusBoy;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/log/terminal/ReaderEventBusBoy;

    invoke-direct {v0, p0, p1}, Lcom/squareup/log/terminal/ReaderEventBusBoy;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/wavpool/swipe/SwipeBus;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/log/terminal/ReaderEventBusBoy;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/log/terminal/ReaderEventBusBoy_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    iget-object v1, p0, Lcom/squareup/log/terminal/ReaderEventBusBoy_Factory;->swipeBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/wavpool/swipe/SwipeBus;

    invoke-static {v0, v1}, Lcom/squareup/log/terminal/ReaderEventBusBoy_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/wavpool/swipe/SwipeBus;)Lcom/squareup/log/terminal/ReaderEventBusBoy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/log/terminal/ReaderEventBusBoy_Factory;->get()Lcom/squareup/log/terminal/ReaderEventBusBoy;

    move-result-object v0

    return-object v0
.end method
