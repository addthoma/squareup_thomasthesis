.class public Lcom/squareup/log/items/EditCatalogObjectEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "EditCatalogObjectEvent.java"


# instance fields
.field private final active_location_count:I

.field private final location_override:Z

.field private final new_item:Z

.field private final object_type:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;ZZ)V
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->ITEMS_APPLET_EDIT_CATALOG_OBJECT:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 18
    iput p1, p0, Lcom/squareup/log/items/EditCatalogObjectEvent;->active_location_count:I

    .line 19
    iput-object p2, p0, Lcom/squareup/log/items/EditCatalogObjectEvent;->object_type:Ljava/lang/String;

    .line 20
    iput-boolean p3, p0, Lcom/squareup/log/items/EditCatalogObjectEvent;->location_override:Z

    .line 21
    iput-boolean p4, p0, Lcom/squareup/log/items/EditCatalogObjectEvent;->new_item:Z

    return-void
.end method
