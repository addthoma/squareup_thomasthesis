.class Lcom/squareup/flowlegacy/EditTextDialogPopup$1;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "EditTextDialogPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/flowlegacy/EditTextDialogPopup;->createDialog(Lcom/squareup/flowlegacy/EditTextDialogPopup$Params;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/flowlegacy/EditTextDialogPopup;

.field final synthetic val$presenter:Lcom/squareup/mortar/PopupPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/flowlegacy/EditTextDialogPopup;Lcom/squareup/mortar/PopupPresenter;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$1;->this$0:Lcom/squareup/flowlegacy/EditTextDialogPopup;

    iput-object p2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$1;->val$presenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x6

    if-ne p1, p2, :cond_0

    .line 49
    iget-object p1, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$1;->val$presenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object p2, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$1;->this$0:Lcom/squareup/flowlegacy/EditTextDialogPopup;

    invoke-static {p2}, Lcom/squareup/flowlegacy/EditTextDialogPopup;->access$400(Lcom/squareup/flowlegacy/EditTextDialogPopup;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    .line 50
    iget-object p1, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$1;->this$0:Lcom/squareup/flowlegacy/EditTextDialogPopup;

    iget-object p1, p1, Lcom/squareup/flowlegacy/EditTextDialogPopup;->editor:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/flowlegacy/EditTextDialogPopup$1;->this$0:Lcom/squareup/flowlegacy/EditTextDialogPopup;

    invoke-virtual {p1}, Lcom/squareup/flowlegacy/EditTextDialogPopup;->dismiss()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
