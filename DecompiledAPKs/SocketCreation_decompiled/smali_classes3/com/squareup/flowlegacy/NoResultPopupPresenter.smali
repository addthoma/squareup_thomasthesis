.class public Lcom/squareup/flowlegacy/NoResultPopupPresenter;
.super Lcom/squareup/mortar/PopupPresenter;
.source "NoResultPopupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Landroid/os/Parcelable;",
        ">",
        "Lcom/squareup/mortar/PopupPresenter<",
        "TD;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/mortar/PopupPresenter;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public extractBundleService(Lcom/squareup/mortar/Popup;)Lmortar/bundler/BundleService;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mortar/Popup<",
            "TD;",
            "Ljava/lang/Void;",
            ">;)",
            "Lmortar/bundler/BundleService;"
        }
    .end annotation

    .line 31
    invoke-super {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->extractBundleService(Lcom/squareup/mortar/Popup;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/mortar/Popup;

    invoke-virtual {p0, p1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->extractBundleService(Lcom/squareup/mortar/Popup;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->onPopupResult(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPopupResult(Ljava/lang/Void;)V
    .locals 0

    return-void
.end method
