.class public final Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;
.super Ljava/lang/Object;
.source "AppFeedbackScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/feedback/AppFeedbackScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final appFeedbackSubmitterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/AppFeedbackSubmitter;",
            ">;"
        }
    .end annotation
.end field

.field private final confirmationScreenDataFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final failureMessageFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final feedbackSubmittedCompleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/FeedbackSubmittedCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final intentAvailabilityManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/IntentAvailabilityManager;",
            ">;"
        }
    .end annotation
.end field

.field private final playStoreIntentCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/PlayStoreIntentCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final standardReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/AppFeedbackSubmitter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/FeedbackSubmittedCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/IntentAvailabilityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/PlayStoreIntentCreator;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->standardReceiverProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->appFeedbackSubmitterProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->feedbackSubmittedCompleterProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->confirmationScreenDataFactoryProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->intentAvailabilityManagerProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p8, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->playStoreIntentCreatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/AppFeedbackSubmitter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/FeedbackSubmittedCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/IntentAvailabilityManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/feedback/PlayStoreIntentCreator;",
            ">;)",
            "Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;"
        }
    .end annotation

    .line 67
    new-instance v9, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/feedback/AppFeedbackSubmitter;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/feedback/FeedbackSubmittedCompleter;Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;Lcom/squareup/util/IntentAvailabilityManager;Lcom/squareup/feedback/PlayStoreIntentCreator;)Lcom/squareup/feedback/AppFeedbackScopeRunner;
    .locals 10

    .line 76
    new-instance v9, Lcom/squareup/feedback/AppFeedbackScopeRunner;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/feedback/AppFeedbackScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/feedback/AppFeedbackSubmitter;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/feedback/FeedbackSubmittedCompleter;Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;Lcom/squareup/util/IntentAvailabilityManager;Lcom/squareup/feedback/PlayStoreIntentCreator;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/feedback/AppFeedbackScopeRunner;
    .locals 9

    .line 56
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->standardReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/receiving/StandardReceiver;

    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->appFeedbackSubmitterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/feedback/AppFeedbackSubmitter;

    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->feedbackSubmittedCompleterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/feedback/FeedbackSubmittedCompleter;

    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->confirmationScreenDataFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;

    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->intentAvailabilityManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/IntentAvailabilityManager;

    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->playStoreIntentCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/feedback/PlayStoreIntentCreator;

    invoke-static/range {v1 .. v8}, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/feedback/AppFeedbackSubmitter;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/feedback/FeedbackSubmittedCompleter;Lcom/squareup/feedback/AppFeedbackConfirmationScreen$ScreenData$Factory;Lcom/squareup/util/IntentAvailabilityManager;Lcom/squareup/feedback/PlayStoreIntentCreator;)Lcom/squareup/feedback/AppFeedbackScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/feedback/AppFeedbackScopeRunner_Factory;->get()Lcom/squareup/feedback/AppFeedbackScopeRunner;

    move-result-object v0

    return-object v0
.end method
