.class public final Lcom/squareup/feedback/AppFeedbackCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AppFeedbackCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0010\u0010\u001d\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0018\u0010\u001e\u001a\u00020\u001a2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u0013H\u0002J\u0010\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u0011\u001a&\u0012\u000c\u0012\n \u0014*\u0004\u0018\u00010\u00130\u0013 \u0014*\u0012\u0012\u000c\u0012\n \u0014*\u0004\u0018\u00010\u00130\u0013\u0018\u00010\u00120\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/feedback/AppFeedbackCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/feedback/AppFeedbackScreen$Runner;",
        "res",
        "Lcom/squareup/util/Res;",
        "device",
        "Lcom/squareup/util/Device;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "appNameFormatter",
        "Lcom/squareup/util/AppNameFormatter;",
        "(Lcom/squareup/feedback/AppFeedbackScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/AppNameFormatter;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "feedbackEditText",
        "Lcom/squareup/widgets/SelectableEditText;",
        "isPhoneOrPortraitLessThan10Inches",
        "Lio/reactivex/Observable;",
        "",
        "kotlin.jvm.PlatformType",
        "starsView",
        "Lcom/squareup/feedback/StarsView;",
        "titleView",
        "Lcom/squareup/widgets/MessageView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureActionBar",
        "starCount",
        "",
        "isSmallScreen",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "screenData",
        "Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;",
        "feedback_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private final appNameFormatter:Lcom/squareup/util/AppNameFormatter;

.field private feedbackEditText:Lcom/squareup/widgets/SelectableEditText;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final isPhoneOrPortraitLessThan10Inches:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/feedback/AppFeedbackScreen$Runner;

.field private starsView:Lcom/squareup/feedback/StarsView;

.field private titleView:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/feedback/AppFeedbackScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/util/AppNameFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appNameFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->runner:Lcom/squareup/feedback/AppFeedbackScreen$Runner;

    iput-object p2, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p5, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    .line 30
    invoke-interface {p3}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object p1

    .line 31
    sget-object p2, Lcom/squareup/feedback/AppFeedbackCoordinator$isPhoneOrPortraitLessThan10Inches$1;->INSTANCE:Lcom/squareup/feedback/AppFeedbackCoordinator$isPhoneOrPortraitLessThan10Inches$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->isPhoneOrPortraitLessThan10Inches:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$configureActionBar(Lcom/squareup/feedback/AppFeedbackCoordinator;IZ)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/feedback/AppFeedbackCoordinator;->configureActionBar(IZ)V

    return-void
.end method

.method public static final synthetic access$getFeedbackEditText$p(Lcom/squareup/feedback/AppFeedbackCoordinator;)Lcom/squareup/widgets/SelectableEditText;
    .locals 1

    .line 21
    iget-object p0, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->feedbackEditText:Lcom/squareup/widgets/SelectableEditText;

    if-nez p0, :cond_0

    const-string v0, "feedbackEditText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/feedback/AppFeedbackCoordinator;)Lcom/squareup/feedback/AppFeedbackScreen$Runner;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->runner:Lcom/squareup/feedback/AppFeedbackScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$setFeedbackEditText$p(Lcom/squareup/feedback/AppFeedbackCoordinator;Lcom/squareup/widgets/SelectableEditText;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->feedbackEditText:Lcom/squareup/widgets/SelectableEditText;

    return-void
.end method

.method public static final synthetic access$spinnerData(Lcom/squareup/feedback/AppFeedbackCoordinator;Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/feedback/AppFeedbackCoordinator;->spinnerData(Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 63
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 64
    sget v0, Lcom/squareup/feedback/R$id;->stars:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/feedback/StarsView;

    iput-object v0, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->starsView:Lcom/squareup/feedback/StarsView;

    .line 65
    sget v0, Lcom/squareup/feedback/R$id;->feedback_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->titleView:Lcom/squareup/widgets/MessageView;

    .line 66
    sget v0, Lcom/squareup/feedback/R$id;->invoices_app_feedback:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->feedbackEditText:Lcom/squareup/widgets/SelectableEditText;

    return-void
.end method

.method private final configureActionBar(IZ)V
    .locals 5

    .line 73
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v2, "actionBar.presenter"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 75
    iget-object v3, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/feedback/R$string;->feedback_and_more:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 76
    new-instance v4, Lcom/squareup/feedback/AppFeedbackCoordinator$configureActionBar$1;

    invoke-direct {v4, p0}, Lcom/squareup/feedback/AppFeedbackCoordinator$configureActionBar$1;-><init>(Lcom/squareup/feedback/AppFeedbackCoordinator;)V

    check-cast v4, Ljava/lang/Runnable;

    .line 74
    invoke-virtual {v0, p2, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(ZLjava/lang/CharSequence;Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 77
    new-instance v3, Lcom/squareup/feedback/AppFeedbackCoordinator$configureActionBar$2;

    invoke-direct {v3, p0, p1}, Lcom/squareup/feedback/AppFeedbackCoordinator$configureActionBar$2;-><init>(Lcom/squareup/feedback/AppFeedbackCoordinator;I)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {p2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 82
    iget-object v3, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/feedback/R$string;->submit_feedback:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {p2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    if-lez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 83
    :goto_0
    invoke-virtual {p2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 84
    iget-object p1, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final spinnerData(Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 4

    .line 59
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    invoke-virtual {p1}, Lcom/squareup/feedback/AppFeedbackScreen$ScreenData;->getShowSpinner()Z

    move-result p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, p1, v1, v2, v3}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/feedback/AppFeedbackCoordinator;->bindViews(Landroid/view/View;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->titleView:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string/jumbo v1, "titleView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v2, Lcom/squareup/feedback/R$string;->what_do_you_think_of_app:I

    invoke-interface {v1, v2}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    sget-object v0, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 45
    iget-object v1, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->starsView:Lcom/squareup/feedback/StarsView;

    if-nez v1, :cond_1

    const-string v2, "starsView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lcom/squareup/feedback/StarsView;->getStarsSelected()Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->isPhoneOrPortraitLessThan10Inches:Lio/reactivex/Observable;

    const-string v3, "isPhoneOrPortraitLessThan10Inches"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {v0, v1, v2}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 46
    new-instance v1, Lcom/squareup/feedback/AppFeedbackCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/feedback/AppFeedbackCoordinator$attach$1;-><init>(Lcom/squareup/feedback/AppFeedbackCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 50
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->runner:Lcom/squareup/feedback/AppFeedbackScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/feedback/AppFeedbackScreen$Runner;->appFeedbackScreenData()Lrx/Observable;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lcom/squareup/feedback/AppFeedbackCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/feedback/AppFeedbackCoordinator$attach$2;

    move-object v3, p0

    check-cast v3, Lcom/squareup/feedback/AppFeedbackCoordinator;

    invoke-direct {v2, v3}, Lcom/squareup/feedback/AppFeedbackCoordinator$attach$2;-><init>(Lcom/squareup/feedback/AppFeedbackCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/feedback/AppFeedbackCoordinator$sam$rx_functions_Func1$0;

    invoke-direct {v3, v2}, Lcom/squareup/feedback/AppFeedbackCoordinator$sam$rx_functions_Func1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {v1, v3}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransform(Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    const-string v1, "runner.appFeedbackScreen\u2026Transform(::spinnerData))"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 55
    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/util/ObservablesKt;->subscribeWith$default(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lrx/Subscription;

    return-void
.end method
