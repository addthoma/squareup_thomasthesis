.class public interface abstract Lcom/squareup/feedback/AppFeedbackSubmitter;
.super Ljava/lang/Object;
.source "AppFeedbackSubmitter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u001e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/feedback/AppFeedbackSubmitter;",
        "",
        "submit",
        "Lrx/Observable;",
        "Lcom/squareup/feedback/AppFeedbackResponse;",
        "rating",
        "",
        "feedback",
        "",
        "feedback_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract submit(ILjava/lang/String;)Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/feedback/AppFeedbackResponse;",
            ">;"
        }
    .end annotation
.end method
