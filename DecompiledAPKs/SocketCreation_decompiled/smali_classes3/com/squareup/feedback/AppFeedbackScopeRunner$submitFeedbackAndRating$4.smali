.class final Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;
.super Ljava/lang/Object;
.source "AppFeedbackScopeRunner.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/feedback/AppFeedbackScopeRunner;->submitFeedbackAndRating(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/feedback/AppFeedbackResponse;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/feedback/AppFeedbackResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $numStars:I

.field final synthetic this$0:Lcom/squareup/feedback/AppFeedbackScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/feedback/AppFeedbackScopeRunner;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;->this$0:Lcom/squareup/feedback/AppFeedbackScopeRunner;

    iput p2, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;->$numStars:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+",
            "Lcom/squareup/feedback/AppFeedbackResponse;",
            ">;)V"
        }
    .end annotation

    .line 70
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_1

    .line 71
    iget-object p1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;->this$0:Lcom/squareup/feedback/AppFeedbackScopeRunner;

    invoke-static {p1}, Lcom/squareup/feedback/AppFeedbackScopeRunner;->access$isMaxRating$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    iget v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;->$numStars:I

    invoke-static {}, Lcom/squareup/feedback/AppFeedbackScopeRunner;->access$Companion()Lcom/squareup/feedback/AppFeedbackScopeRunner$Companion;

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;->this$0:Lcom/squareup/feedback/AppFeedbackScopeRunner;

    invoke-static {p1}, Lcom/squareup/feedback/AppFeedbackScopeRunner;->access$getFlow$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lflow/Flow;

    move-result-object p1

    new-instance v0, Lcom/squareup/feedback/AppFeedbackConfirmationScreen;

    iget-object v1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;->this$0:Lcom/squareup/feedback/AppFeedbackScopeRunner;

    invoke-static {v1}, Lcom/squareup/feedback/AppFeedbackScopeRunner;->access$getAppFeedbackScope$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/squareup/feedback/AppFeedbackScope;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/feedback/AppFeedbackConfirmationScreen;-><init>(Lcom/squareup/feedback/AppFeedbackScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_1

    .line 74
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_2

    .line 76
    iget-object v0, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;->this$0:Lcom/squareup/feedback/AppFeedbackScopeRunner;

    invoke-static {v0}, Lcom/squareup/feedback/AppFeedbackScopeRunner;->access$getFailureMessageFactory$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lcom/squareup/receiving/FailureMessageFactory;

    move-result-object v0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    sget v1, Lcom/squareup/feedback/R$string;->feedback_error:I

    sget-object v2, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4$failureMessage$1;->INSTANCE:Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4$failureMessage$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 77
    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;->this$0:Lcom/squareup/feedback/AppFeedbackScopeRunner;

    invoke-static {p1}, Lcom/squareup/feedback/AppFeedbackScopeRunner;->access$getFlow$p(Lcom/squareup/feedback/AppFeedbackScopeRunner;)Lflow/Flow;

    move-result-object p1

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    invoke-direct {v1, v0}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    invoke-virtual {p1, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/feedback/AppFeedbackScopeRunner$submitFeedbackAndRating$4;->call(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method
