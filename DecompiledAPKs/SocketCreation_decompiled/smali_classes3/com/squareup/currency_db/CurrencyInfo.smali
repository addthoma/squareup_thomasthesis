.class final enum Lcom/squareup/currency_db/CurrencyInfo;
.super Ljava/lang/Enum;
.source "CurrencyInfo.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/currency_db/CurrencyInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum AED:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum ALL:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum AMD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum AOA:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum ARS:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum AUD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum AWG:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum AZN:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BAM:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BBD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BDT:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BGN:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BHD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BMD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BND:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BOB:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BRL:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BSD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BTN:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BWP:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BYR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum BZD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum CAD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum CDF:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum CHF:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum CLP:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum CNY:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum COP:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum CRC:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum CVE:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum CZK:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum DKK:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum DOP:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum DZD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum EGP:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum ETB:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum EUR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum FJD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum GBP:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum GEL:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum GHS:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum GIP:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum GMD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum GTQ:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum GYD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum HKD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum HNL:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum HRK:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum HTG:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum HUF:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum IDR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum ILS:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum INR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum ISK:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum JMD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum JOD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum JPY:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum KES:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum KGS:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum KHR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum KRW:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum KWD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum KYD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum KZT:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum LAK:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum LBP:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum LKR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum LRD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum LTL:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MAD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MDL:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MGA:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MKD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MMK:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MNT:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MOP:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MRO:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MUR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MWK:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MXN:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MYR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum MZN:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum NAD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum NGN:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum NIO:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum NOK:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum NPR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum NZD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum OMR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum PAB:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum PEN:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum PGK:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum PHP:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum PKR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum PLN:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum PYG:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum QAR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum RON:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum RSD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum RUB:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum RWF:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum SAR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum SBD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum SCR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum SEK:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum SGD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum SLL:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum SRD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum STD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum SVC:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum SZL:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum THB:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum TJS:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum TMT:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum TND:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum TRY:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum TTD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum TWD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum TZS:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum UAH:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum UGX:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum USD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum UYU:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum UZS:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum VEF:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum VND:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum XAF:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum XCD:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum XOF:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum YER:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum ZAR:Lcom/squareup/currency_db/CurrencyInfo;

.field public static final enum ZMW:Lcom/squareup/currency_db/CurrencyInfo;


# instance fields
.field final denominations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final fractionalDigits:I

.field final subunit_symbol:Ljava/lang/String;

.field final subunitsPerUnit:I

.field final swedishRoundingInterval:I

.field final symbol:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 46

    .line 8
    new-instance v9, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Integer;

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v10, 0x0

    aput-object v1, v0, v10

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v11, 0x1

    aput-object v1, v0, v11

    const/16 v12, 0x64

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x2

    aput-object v13, v0, v14

    const/16 v1, 0x1f4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    const/4 v8, 0x3

    aput-object v15, v0, v8

    const/16 v1, 0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    const/4 v7, 0x4

    aput-object v16, v0, v7

    const/16 v1, 0x7d0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    const/4 v6, 0x5

    aput-object v17, v0, v6

    const/16 v1, 0x1388

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    const/4 v5, 0x6

    aput-object v18, v0, v5

    const/16 v1, 0x2710

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    const/4 v4, 0x7

    aput-object v19, v0, v4

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const v1, 0xc350

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    const/16 v1, 0x9

    aput-object v20, v0, v1

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xa

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v21

    const-string v1, "AED"

    const/4 v2, 0x0

    const-string v3, "AED"

    const-string v22, "f"

    const/16 v23, 0x2

    const/16 v24, 0x64

    const/16 v25, 0x1

    move-object v0, v9

    const/4 v12, 0x7

    move-object/from16 v4, v22

    const/4 v12, 0x6

    move/from16 v5, v23

    const/4 v12, 0x5

    move/from16 v6, v24

    move/from16 v7, v25

    move-object/from16 v8, v21

    invoke-direct/range {v0 .. v8}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v9, Lcom/squareup/currency_db/CurrencyInfo;->AED:Lcom/squareup/currency_db/CurrencyInfo;

    .line 9
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    aput-object v20, v1, v11

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x4

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v36

    const-string v29, "ALL"

    const/16 v30, 0x1

    const-string v31, "L"

    const-string v32, "q"

    const/16 v33, 0x2

    const/16 v34, 0x64

    const/16 v35, 0x1

    move-object/from16 v28, v0

    invoke-direct/range {v28 .. v36}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->ALL:Lcom/squareup/currency_db/CurrencyInfo;

    .line 10
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    const v1, 0x7a120

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    const v1, 0xf4240

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v14

    const v1, 0x1e8480

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const v1, 0x4c4b40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    const v1, 0x989680

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v45

    const-string v38, "AMD"

    const/16 v39, 0x2

    const-string/jumbo v40, "\u058f"

    const-string v41, ""

    const/16 v42, 0x2

    const/16 v43, 0x64

    const/16 v44, 0x1

    move-object/from16 v37, v0

    invoke-direct/range {v37 .. v45}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->AMD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 11
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v18, v1, v11

    aput-object v19, v1, v14

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v20, v1, v4

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "AOA"

    const/16 v28, 0x3

    const-string v29, "Kz"

    const-string v30, "c"

    const/16 v31, 0x2

    const/16 v32, 0x64

    const/16 v33, 0x1

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->AOA:Lcom/squareup/currency_db/CurrencyInfo;

    .line 12
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v13, v1, v4

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const/4 v2, 0x6

    aput-object v15, v1, v2

    const/4 v2, 0x7

    aput-object v16, v1, v2

    const/16 v2, 0x8

    aput-object v17, v1, v2

    const/16 v2, 0x9

    aput-object v18, v1, v2

    const/16 v2, 0xa

    aput-object v19, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "ARS"

    const/16 v37, 0x4

    const-string v38, "$"

    const-string v39, "c"

    const/16 v40, 0x2

    const/16 v41, 0x64

    const/16 v42, 0x1

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->ARS:Lcom/squareup/currency_db/CurrencyInfo;

    .line 13
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    aput-object v15, v2, v14

    aput-object v16, v2, v3

    aput-object v17, v2, v4

    aput-object v18, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "AUD"

    const/16 v28, 0x5

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    const/16 v33, 0x5

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->AUD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 14
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    const/16 v2, 0x9c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    aput-object v20, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "AWG"

    const/16 v37, 0x6

    const-string/jumbo v38, "\u0192"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->AWG:Lcom/squareup/currency_db/CurrencyInfo;

    .line 15
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "AZN"

    const/16 v28, 0x7

    const-string/jumbo v29, "\u20bc"

    const-string v30, "q"

    const/16 v33, 0x1

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->AZN:Lcom/squareup/currency_db/CurrencyInfo;

    .line 16
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v13, v1, v4

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const/4 v2, 0x6

    aput-object v15, v1, v2

    const/4 v2, 0x7

    aput-object v16, v1, v2

    const/16 v2, 0x8

    aput-object v17, v1, v2

    const/16 v2, 0x9

    aput-object v18, v1, v2

    const/16 v2, 0xa

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xb

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "BAM"

    const/16 v37, 0x8

    const-string/jumbo v38, "\u041a\u041c"

    const-string v39, ""

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BAM:Lcom/squareup/currency_db/CurrencyInfo;

    .line 17
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "BBD"

    const/16 v28, 0x9

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BBD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 18
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    aput-object v15, v1, v11

    aput-object v16, v1, v14

    aput-object v17, v1, v3

    aput-object v18, v1, v4

    aput-object v19, v1, v12

    const/4 v2, 0x6

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "BDT"

    const/16 v37, 0xa

    const-string/jumbo v38, "\u09f3"

    const-string v39, "p"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BDT:Lcom/squareup/currency_db/CurrencyInfo;

    .line 19
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "BGN"

    const/16 v28, 0xb

    const-string/jumbo v29, "\u043b\u0432"

    const-string/jumbo v30, "\u0441\u0442\u043e\u0442\u0438\u043d\u043a\u0438"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BGN:Lcom/squareup/currency_db/CurrencyInfo;

    .line 20
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v13, v1, v4

    aput-object v15, v1, v12

    const/4 v2, 0x6

    aput-object v16, v1, v2

    const/4 v2, 0x7

    aput-object v18, v1, v2

    const/16 v2, 0x8

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x9

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "BHD"

    const/16 v37, 0xc

    const-string v38, "BHD"

    const-string v39, "f"

    const/16 v40, 0x3

    const/16 v41, 0x3e8

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BHD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 21
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "BMD"

    const/16 v28, 0xd

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BMD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 22
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    aput-object v15, v1, v11

    aput-object v16, v1, v14

    aput-object v18, v1, v3

    aput-object v19, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "BND"

    const/16 v37, 0xe

    const-string v38, "$"

    const-string/jumbo v39, "\u00a2"

    const/16 v40, 0x2

    const/16 v41, 0x64

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BND:Lcom/squareup/currency_db/CurrencyInfo;

    .line 23
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v17, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "BOB"

    const/16 v28, 0xf

    const-string v29, "Bs."

    const-string v30, "Cvs."

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BOB:Lcom/squareup/currency_db/CurrencyInfo;

    .line 24
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v13, v1, v4

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const/4 v2, 0x6

    aput-object v15, v1, v2

    const/4 v2, 0x7

    aput-object v16, v1, v2

    const/16 v2, 0x8

    aput-object v17, v1, v2

    const/16 v2, 0x9

    aput-object v18, v1, v2

    const/16 v2, 0xa

    aput-object v19, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "BRL"

    const/16 v37, 0x10

    const-string v38, "R$"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BRL:Lcom/squareup/currency_db/CurrencyInfo;

    .line 25
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "BSD"

    const/16 v28, 0x11

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BSD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 26
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    aput-object v15, v1, v11

    aput-object v16, v1, v14

    aput-object v17, v1, v3

    aput-object v18, v1, v4

    aput-object v19, v1, v12

    const/4 v2, 0x6

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "BTN"

    const/16 v37, 0x12

    const-string v38, "Nu."

    const-string v39, "Ch."

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BTN:Lcom/squareup/currency_db/CurrencyInfo;

    .line 27
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v17, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "BWP"

    const/16 v28, 0x13

    const-string v29, "P"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BWP:Lcom/squareup/currency_db/CurrencyInfo;

    .line 28
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v19, v1, v10

    aput-object v20, v1, v11

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const v2, 0x1e8480

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const v2, 0x4c4b40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const v2, 0x989680

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "BYR"

    const/16 v37, 0x14

    const-string v38, "Br"

    const-string v39, ""

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BYR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 29
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "BZD"

    const/16 v28, 0x15

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->BZD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 30
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v15, v1, v14

    aput-object v16, v1, v3

    aput-object v17, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "CAD"

    const/16 v37, 0x16

    const-string v38, "$"

    const-string/jumbo v39, "\u00a2"

    const/16 v42, 0x5

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->CAD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 31
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v17, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object v20, v1, v12

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x8

    aput-object v2, v1, v5

    const v2, 0x1e8480

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x9

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "CDF"

    const/16 v28, 0x17

    const-string v29, "Fr"

    const-string v30, "c"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->CDF:Lcom/squareup/currency_db/CurrencyInfo;

    .line 32
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v13, v1, v4

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const/4 v2, 0x6

    aput-object v15, v1, v2

    const/4 v2, 0x7

    aput-object v16, v1, v2

    const/16 v2, 0x8

    aput-object v17, v1, v2

    const/16 v2, 0x9

    aput-object v18, v1, v2

    const/16 v2, 0xa

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xb

    aput-object v2, v1, v5

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xc

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "CHF"

    const/16 v37, 0x18

    const-string v38, "CHF"

    const-string v39, "c"

    const/16 v42, 0x1

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->CHF:Lcom/squareup/currency_db/CurrencyInfo;

    .line 33
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v17, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "CLP"

    const/16 v28, 0x19

    const-string v29, "$"

    const-string v30, ""

    const/16 v31, 0x0

    const/16 v32, 0x1

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->CLP:Lcom/squareup/currency_db/CurrencyInfo;

    .line 34
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "CNY"

    const/16 v37, 0x1a

    const-string/jumbo v38, "\u00a5"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->CNY:Lcom/squareup/currency_db/CurrencyInfo;

    .line 35
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    const v1, 0x30d40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    const v1, 0x7a120

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v14

    const v1, 0xf4240

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const v1, 0x1e8480

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    const v1, 0x4c4b40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "COP"

    const/16 v28, 0x1b

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    const/16 v31, 0x2

    const/16 v32, 0x64

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->COP:Lcom/squareup/currency_db/CurrencyInfo;

    .line 36
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    const v1, 0x30d40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    const v1, 0x7a120

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v14

    const v1, 0xf4240

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const v1, 0x1e8480

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    const v1, 0x4c4b40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "CRC"

    const/16 v37, 0x1c

    const-string/jumbo v38, "\u20a1"

    const-string v39, ""

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->CRC:Lcom/squareup/currency_db/CurrencyInfo;

    .line 37
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    aput-object v20, v2, v11

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v14

    const v1, 0x30d40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const v1, 0x3d090

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    const v1, 0x7a120

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "CVE"

    const/16 v28, 0x1d

    const-string v29, "$"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->CVE:Lcom/squareup/currency_db/CurrencyInfo;

    .line 38
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v19, v1, v10

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v20, v1, v14

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "CZK"

    const/16 v37, 0x1e

    const-string v38, "K\u010d"

    const-string v39, "h"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->CZK:Lcom/squareup/currency_db/CurrencyInfo;

    .line 39
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    aput-object v13, v1, v11

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    aput-object v15, v1, v3

    aput-object v16, v1, v4

    aput-object v17, v1, v12

    const/4 v2, 0x6

    aput-object v18, v1, v2

    const/4 v2, 0x7

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x8

    aput-object v2, v1, v5

    const/16 v2, 0x9

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xa

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "DKK"

    const/16 v28, 0x1f

    const-string v29, "kr"

    const-string/jumbo v30, "\u00f8re"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->DKK:Lcom/squareup/currency_db/CurrencyInfo;

    .line 40
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x7

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v1, 0x9c4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    aput-object v18, v2, v11

    aput-object v19, v2, v14

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    aput-object v20, v2, v4

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    const v1, 0x30d40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v5, 0x6

    aput-object v1, v2, v5

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "DOP"

    const/16 v37, 0x20

    const-string v38, "$"

    const-string/jumbo v39, "\u00a2"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->DOP:Lcom/squareup/currency_db/CurrencyInfo;

    .line 41
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v3, [Ljava/lang/Integer;

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    aput-object v20, v1, v11

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "DZD"

    const/16 v28, 0x21

    const-string v29, "DZD"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->DZD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 42
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v13, v1, v14

    aput-object v15, v1, v3

    aput-object v16, v1, v4

    aput-object v17, v1, v12

    const/4 v2, 0x6

    aput-object v18, v1, v2

    const/4 v2, 0x7

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x8

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "EGP"

    const/16 v37, 0x22

    const-string v38, "E\u00a3"

    const-string v39, "pt."

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->EGP:Lcom/squareup/currency_db/CurrencyInfo;

    .line 43
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    aput-object v15, v1, v11

    aput-object v16, v1, v14

    aput-object v18, v1, v3

    aput-object v19, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "ETB"

    const/16 v28, 0x23

    const-string v29, "ETB"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->ETB:Lcom/squareup/currency_db/CurrencyInfo;

    .line 44
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const/4 v2, 0x6

    aput-object v13, v1, v2

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    const/16 v2, 0x8

    aput-object v15, v1, v2

    const/16 v2, 0x9

    aput-object v16, v1, v2

    const/16 v2, 0xa

    aput-object v17, v1, v2

    const/16 v2, 0xb

    aput-object v18, v1, v2

    const/16 v2, 0xc

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xd

    aput-object v2, v1, v5

    const/16 v2, 0xe

    aput-object v20, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "EUR"

    const/16 v37, 0x24

    const-string/jumbo v38, "\u20ac"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->EUR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 45
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "FJD"

    const/16 v28, 0x25

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->FJD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 46
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const/4 v2, 0x6

    aput-object v13, v1, v2

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    const/16 v2, 0x8

    aput-object v15, v1, v2

    const/16 v2, 0x9

    aput-object v16, v1, v2

    const/16 v2, 0xa

    aput-object v17, v1, v2

    const/16 v2, 0xb

    aput-object v18, v1, v2

    const/16 v2, 0xc

    aput-object v19, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "GBP"

    const/16 v37, 0x26

    const-string/jumbo v38, "\u00a3"

    const-string v39, "p"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->GBP:Lcom/squareup/currency_db/CurrencyInfo;

    .line 47
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v15, v1, v14

    aput-object v16, v1, v3

    aput-object v17, v1, v4

    aput-object v18, v1, v12

    const/4 v2, 0x6

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "GEL"

    const/16 v28, 0x27

    const-string/jumbo v29, "\u10da,"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->GEL:Lcom/squareup/currency_db/CurrencyInfo;

    .line 48
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    aput-object v15, v2, v14

    aput-object v16, v2, v3

    aput-object v17, v2, v4

    aput-object v18, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "GHS"

    const/16 v37, 0x28

    const-string/jumbo v38, "\u20b5"

    const-string v39, "Gp"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->GHS:Lcom/squareup/currency_db/CurrencyInfo;

    .line 49
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v15, v1, v10

    aput-object v16, v1, v11

    aput-object v17, v1, v14

    aput-object v18, v1, v3

    aput-object v19, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "GIP"

    const/16 v28, 0x29

    const-string/jumbo v29, "\u00a3"

    const-string v30, "p"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->GIP:Lcom/squareup/currency_db/CurrencyInfo;

    .line 50
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v15, v1, v10

    aput-object v16, v1, v11

    const/16 v2, 0x9c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    aput-object v18, v1, v3

    aput-object v19, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "GMD"

    const/16 v37, 0x2a

    const-string v38, "D"

    const-string v39, "b"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->GMD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 51
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    aput-object v13, v1, v11

    aput-object v15, v1, v14

    aput-object v16, v1, v3

    aput-object v17, v1, v4

    aput-object v18, v1, v12

    const/4 v2, 0x6

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "GTQ"

    const/16 v28, 0x2b

    const-string v29, "Q"

    const-string/jumbo v30, "\u00a2"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->GTQ:Lcom/squareup/currency_db/CurrencyInfo;

    .line 52
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v17, v1, v10

    aput-object v19, v1, v11

    aput-object v20, v1, v14

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "GYD"

    const/16 v37, 0x2c

    const-string v38, "$"

    const-string/jumbo v39, "\u00a2"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->GYD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 53
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    aput-object v13, v1, v3

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object v15, v1, v12

    const/4 v2, 0x6

    aput-object v16, v1, v2

    const/4 v2, 0x7

    aput-object v17, v1, v2

    const/16 v2, 0x8

    aput-object v18, v1, v2

    const/16 v2, 0x9

    aput-object v19, v1, v2

    const/16 v2, 0xa

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xb

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "HKD"

    const/16 v28, 0x2d

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    const/16 v33, 0xa

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->HKD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 54
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v15, v1, v14

    aput-object v16, v1, v3

    aput-object v17, v1, v4

    aput-object v18, v1, v12

    const/4 v2, 0x6

    aput-object v19, v1, v2

    const/4 v2, 0x7

    aput-object v20, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "HNL"

    const/16 v37, 0x2e

    const-string v38, "L"

    const-string/jumbo v39, "\u00a2"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->HNL:Lcom/squareup/currency_db/CurrencyInfo;

    .line 55
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v17, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "HRK"

    const/16 v28, 0x2f

    const-string v29, "kn"

    const-string v30, "lp"

    const/16 v33, 0x1

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->HRK:Lcom/squareup/currency_db/CurrencyInfo;

    .line 56
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v16, v2, v10

    const/16 v1, 0x9c4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    aput-object v18, v2, v14

    aput-object v19, v2, v3

    const/16 v1, 0x61a8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    aput-object v20, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "HTG"

    const/16 v37, 0x30

    const-string v38, "G"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->HTG:Lcom/squareup/currency_db/CurrencyInfo;

    .line 57
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v20, v2, v10

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    const v1, 0x30d40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v14

    const v1, 0x7a120

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const v1, 0xf4240

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    const v1, 0x1e8480

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "HUF"

    const/16 v28, 0x31

    const-string v29, "Ft"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->HUF:Lcom/squareup/currency_db/CurrencyInfo;

    .line 58
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v15, v1, v14

    aput-object v16, v1, v3

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    const v2, 0x1e8480

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x8

    aput-object v2, v1, v5

    const v2, 0x4c4b40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x9

    aput-object v2, v1, v5

    const v2, 0x989680

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xa

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "IDR"

    const/16 v37, 0x32

    const-string v38, "Rp"

    const-string v39, "s"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->IDR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 59
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v13, v1, v14

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v15, v1, v4

    aput-object v16, v1, v12

    const/4 v2, 0x6

    aput-object v17, v1, v2

    const/4 v2, 0x7

    aput-object v18, v1, v2

    const/16 v2, 0x8

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x9

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "ILS"

    const/16 v28, 0x33

    const-string/jumbo v29, "\u20aa"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->ILS:Lcom/squareup/currency_db/CurrencyInfo;

    .line 60
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    aput-object v13, v1, v11

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    aput-object v15, v1, v3

    aput-object v16, v1, v4

    aput-object v17, v1, v12

    const/16 v2, 0x9c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const/4 v2, 0x7

    aput-object v18, v1, v2

    const/16 v2, 0x8

    aput-object v19, v1, v2

    const/16 v2, 0x9

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xa

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "INR"

    const/16 v37, 0x34

    const-string/jumbo v38, "\u20b9"

    const-string v39, "p"

    const/16 v42, 0x64

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->INR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 61
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v13, v1, v4

    aput-object v20, v1, v12

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x8

    aput-object v2, v1, v5

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x9

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "ISK"

    const/16 v28, 0x35

    const-string v29, "kr"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->ISK:Lcom/squareup/currency_db/CurrencyInfo;

    .line 62
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v4, [Ljava/lang/Integer;

    aput-object v18, v1, v10

    aput-object v19, v1, v11

    aput-object v20, v1, v14

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "JMD"

    const/16 v37, 0x36

    const-string v38, "$"

    const-string/jumbo v39, "\u00a2"

    const/16 v42, 0x1

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->JMD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 63
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v18, v1, v11

    aput-object v19, v1, v14

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v20, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "JOD"

    const/16 v28, 0x37

    const-string v29, "JOD"

    const-string v30, ""

    const/16 v31, 0x3

    const/16 v32, 0x3e8

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->JOD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 64
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    aput-object v15, v1, v11

    aput-object v16, v1, v14

    aput-object v18, v1, v3

    aput-object v19, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "JPY"

    const/16 v37, 0x38

    const-string/jumbo v38, "\u00a5"

    const-string v39, "c"

    const/16 v40, 0x0

    const/16 v41, 0x1

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->JPY:Lcom/squareup/currency_db/CurrencyInfo;

    .line 65
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v18, v1, v10

    aput-object v19, v1, v11

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    aput-object v20, v1, v3

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "KES"

    const/16 v28, 0x39

    const-string v29, "KSh"

    const-string v30, "c"

    const/16 v31, 0x2

    const/16 v32, 0x64

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->KES:Lcom/squareup/currency_db/CurrencyInfo;

    .line 66
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v4, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    const/16 v2, 0x12c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v15, v1, v14

    aput-object v16, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "KGS"

    const/16 v37, 0x3a

    const-string v38, "som"

    const-string/jumbo v39, "tyiyn"

    const/16 v40, 0x2

    const/16 v41, 0x64

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->KGS:Lcom/squareup/currency_db/CurrencyInfo;

    .line 67
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v19, v1, v10

    aput-object v20, v1, v11

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const v2, 0x1e8480

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const v2, 0x4c4b40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "KHR"

    const/16 v28, 0x3b

    const-string/jumbo v29, "\u17db"

    const-string v30, "sen"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->KHR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 68
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v13, v1, v4

    aput-object v15, v1, v12

    const/4 v2, 0x6

    aput-object v16, v1, v2

    const/4 v2, 0x7

    aput-object v18, v1, v2

    const/16 v2, 0x8

    aput-object v19, v1, v2

    const/16 v2, 0x9

    aput-object v20, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "KRW"

    const/16 v37, 0x3c

    const-string/jumbo v38, "\u20a9"

    const-string v39, ""

    const/16 v40, 0x0

    const/16 v41, 0x1

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->KRW:Lcom/squareup/currency_db/CurrencyInfo;

    .line 69
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v18, v1, v10

    aput-object v19, v1, v11

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    aput-object v20, v1, v3

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "KWD"

    const/16 v28, 0x3d

    const-string v29, "KWD"

    const-string v30, ""

    const/16 v31, 0x3

    const/16 v32, 0x3e8

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->KWD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 70
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    const/16 v1, 0x9c4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "KYD"

    const/16 v37, 0x3e

    const-string v38, "$"

    const-string/jumbo v39, "\u00a2"

    const/16 v40, 0x2

    const/16 v41, 0x64

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->KYD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 71
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    aput-object v20, v2, v11

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v14

    const v1, 0x30d40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const v1, 0x7a120

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    const v1, 0xf4240

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "KZT"

    const/16 v28, 0x3f

    const-string/jumbo v29, "\u3012"

    const-string/jumbo v30, "t\u00ef\u0131n"

    const/16 v31, 0x2

    const/16 v32, 0x64

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->KZT:Lcom/squareup/currency_db/CurrencyInfo;

    .line 72
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v20, v1, v10

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const v2, 0x1e8480

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const v2, 0x4c4b40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const v2, 0x989680

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "LAK"

    const/16 v37, 0x40

    const-string/jumbo v38, "\u20ad"

    const-string v39, "att"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->LAK:Lcom/squareup/currency_db/CurrencyInfo;

    .line 73
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    const v1, 0x7a120

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    const v1, 0xf4240

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v14

    const v1, 0x1e8480

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const v1, 0x4c4b40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    const v1, 0x989680

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "LBP"

    const/16 v28, 0x41

    const-string v29, "LBP"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->LBP:Lcom/squareup/currency_db/CurrencyInfo;

    .line 74
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v17, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    aput-object v20, v1, v4

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "LKR"

    const/16 v37, 0x42

    const-string/jumbo v38, "\u20a8"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->LKR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 75
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v15, v1, v10

    aput-object v16, v1, v11

    aput-object v17, v1, v14

    aput-object v18, v1, v3

    aput-object v19, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "LRD"

    const/16 v28, 0x43

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->LRD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 76
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v16, v2, v10

    aput-object v17, v2, v11

    aput-object v18, v2, v14

    aput-object v19, v2, v3

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    aput-object v20, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "LTL"

    const/16 v37, 0x44

    const-string v38, "Lt"

    const-string v39, ""

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->LTL:Lcom/squareup/currency_db/CurrencyInfo;

    .line 77
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v4, [Ljava/lang/Integer;

    aput-object v17, v1, v10

    aput-object v18, v1, v11

    aput-object v19, v1, v14

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "MAD"

    const/16 v28, 0x45

    const-string v29, "MAD"

    const-string v30, "s"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MAD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 78
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    aput-object v15, v1, v11

    aput-object v16, v1, v14

    aput-object v17, v1, v3

    aput-object v18, v1, v4

    aput-object v19, v1, v12

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const/4 v2, 0x7

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x8

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "MDL"

    const/16 v37, 0x46

    const-string v38, "L"

    const-string v39, "ban"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MDL:Lcom/squareup/currency_db/CurrencyInfo;

    .line 79
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x7

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    aput-object v15, v2, v14

    aput-object v16, v2, v3

    aput-object v17, v2, v4

    aput-object v18, v2, v12

    const/4 v1, 0x6

    aput-object v19, v2, v1

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "MGA"

    const/16 v28, 0x47

    const-string v29, "Ar"

    const-string v30, ""

    const/16 v31, 0x0

    const/16 v32, 0x1

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MGA:Lcom/squareup/currency_db/CurrencyInfo;

    .line 80
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v16, v2, v10

    aput-object v18, v2, v11

    aput-object v19, v2, v14

    aput-object v20, v2, v3

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    const v1, 0x7a120

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "MKD"

    const/16 v37, 0x48

    const-string/jumbo v38, "\u0434\u0435\u043d"

    const-string v39, "deni"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MKD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 81
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    aput-object v13, v1, v11

    aput-object v15, v1, v14

    aput-object v16, v1, v3

    aput-object v17, v1, v4

    aput-object v18, v1, v12

    const/4 v2, 0x6

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    const/16 v2, 0x8

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x9

    aput-object v2, v1, v5

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xa

    aput-object v2, v1, v5

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xb

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "MMK"

    const/16 v28, 0x49

    const-string v29, "K"

    const-string v30, "pya"

    const/16 v31, 0x2

    const/16 v32, 0x64

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MMK:Lcom/squareup/currency_db/CurrencyInfo;

    .line 82
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v17, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    aput-object v20, v1, v4

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    const v2, 0x1e8480

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x8

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "MNT"

    const/16 v37, 0x4a

    const-string/jumbo v38, "\u20ae"

    const-string v39, ""

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MNT:Lcom/squareup/currency_db/CurrencyInfo;

    .line 83
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v16, v2, v10

    aput-object v17, v2, v11

    aput-object v18, v2, v14

    aput-object v19, v2, v3

    aput-object v20, v2, v4

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "MOP"

    const/16 v28, 0x4b

    const-string v29, "MOP$"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MOP:Lcom/squareup/currency_db/CurrencyInfo;

    .line 84
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    aput-object v15, v2, v14

    aput-object v16, v2, v3

    aput-object v17, v2, v4

    aput-object v18, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "MRO"

    const/16 v37, 0x4c

    const-string v38, "UM"

    const-string v39, ""

    const/16 v40, 0x0

    const/16 v41, 0x1

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MRO:Lcom/squareup/currency_db/CurrencyInfo;

    .line 85
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x7

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v1, 0x9c4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    aput-object v18, v2, v11

    aput-object v19, v2, v14

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    aput-object v20, v2, v4

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    const v1, 0x30d40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v5, 0x6

    aput-object v1, v2, v5

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "MUR"

    const/16 v28, 0x4d

    const-string/jumbo v29, "\u20a8"

    const-string v30, "c"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MUR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 86
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v17, v2, v10

    aput-object v18, v2, v11

    aput-object v19, v2, v14

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    aput-object v20, v2, v4

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "MWK"

    const/16 v37, 0x4e

    const-string v38, "MK"

    const-string v39, "tambala"

    const/16 v40, 0x2

    const/16 v41, 0x64

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MWK:Lcom/squareup/currency_db/CurrencyInfo;

    .line 87
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    aput-object v13, v1, v11

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    aput-object v15, v1, v3

    aput-object v16, v1, v4

    aput-object v17, v1, v12

    const/4 v2, 0x6

    aput-object v18, v1, v2

    const/4 v2, 0x7

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x8

    aput-object v2, v1, v5

    const/16 v2, 0x9

    aput-object v20, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "MXN"

    const/16 v28, 0x4f

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MXN:Lcom/squareup/currency_db/CurrencyInfo;

    .line 88
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "MYR"

    const/16 v37, 0x50

    const-string v38, "RM"

    const-string v39, "sen"

    const/16 v42, 0x5

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MYR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 89
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v17, v1, v10

    aput-object v18, v1, v11

    aput-object v19, v1, v14

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v20, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "MZN"

    const/16 v28, 0x51

    const-string v29, "MTn"

    const-string/jumbo v30, "\u00a2"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->MZN:Lcom/squareup/currency_db/CurrencyInfo;

    .line 90
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v17, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "NAD"

    const/16 v37, 0x52

    const-string v38, "$"

    const-string/jumbo v39, "\u00a2"

    const/16 v42, 0x1

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->NAD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 91
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v15, v1, v10

    aput-object v16, v1, v11

    aput-object v17, v1, v14

    aput-object v18, v1, v3

    aput-object v19, v1, v4

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const/4 v2, 0x6

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "NGN"

    const/16 v28, 0x53

    const-string/jumbo v29, "\u20a6"

    const-string v30, "Kobo"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->NGN:Lcom/squareup/currency_db/CurrencyInfo;

    .line 92
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v16, v2, v10

    aput-object v17, v2, v11

    aput-object v18, v2, v14

    aput-object v19, v2, v3

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    aput-object v20, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "NIO"

    const/16 v37, 0x54

    const-string v38, "C$"

    const-string/jumbo v39, "\u00a2"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->NIO:Lcom/squareup/currency_db/CurrencyInfo;

    .line 93
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v18, v1, v4

    aput-object v19, v1, v12

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const/4 v2, 0x7

    aput-object v20, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "NOK"

    const/16 v28, 0x55

    const-string v29, "kr"

    const-string/jumbo v30, "\u00f8re"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->NOK:Lcom/squareup/currency_db/CurrencyInfo;

    .line 94
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v15, v1, v10

    aput-object v16, v1, v11

    aput-object v17, v1, v14

    const/16 v2, 0x9c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v18, v1, v4

    aput-object v19, v1, v12

    const/4 v2, 0x6

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "NPR"

    const/16 v37, 0x56

    const-string/jumbo v38, "\u20a8"

    const-string v39, "paisa"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->NPR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 95
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    aput-object v13, v1, v3

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object v15, v1, v12

    const/4 v2, 0x6

    aput-object v16, v1, v2

    const/4 v2, 0x7

    aput-object v17, v1, v2

    const/16 v2, 0x8

    aput-object v18, v1, v2

    const/16 v2, 0x9

    aput-object v19, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "NZD"

    const/16 v28, 0x57

    const-string v29, "$"

    const-string v30, "c"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->NZD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 96
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v15, v2, v10

    aput-object v16, v2, v11

    aput-object v18, v2, v14

    aput-object v19, v2, v3

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    aput-object v20, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "OMR"

    const/16 v37, 0x58

    const-string v38, "OMR"

    const-string v39, ""

    const/16 v40, 0x3

    const/16 v41, 0x3e8

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->OMR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 97
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object v13, v1, v12

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x6

    aput-object v2, v1, v5

    const/4 v2, 0x7

    aput-object v15, v1, v2

    const/16 v2, 0x8

    aput-object v16, v1, v2

    const/16 v2, 0x9

    aput-object v17, v1, v2

    const/16 v2, 0xa

    aput-object v18, v1, v2

    const/16 v2, 0xb

    aput-object v19, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "PAB"

    const/16 v28, 0x59

    const-string v29, "B/."

    const-string v30, "c"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->PAB:Lcom/squareup/currency_db/CurrencyInfo;

    .line 98
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v4, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v17, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "PEN"

    const/16 v37, 0x5a

    const-string v38, "S/."

    const-string v39, "c"

    const/16 v40, 0x2

    const/16 v41, 0x64

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->PEN:Lcom/squareup/currency_db/CurrencyInfo;

    .line 99
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "PGK"

    const/16 v28, 0x5b

    const-string v29, "K"

    const-string/jumbo v30, "toea"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->PGK:Lcom/squareup/currency_db/CurrencyInfo;

    .line 100
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v17, v1, v10

    aput-object v18, v1, v11

    aput-object v19, v1, v14

    aput-object v20, v1, v3

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "PHP"

    const/16 v37, 0x5c

    const-string/jumbo v38, "\u20b1"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->PHP:Lcom/squareup/currency_db/CurrencyInfo;

    .line 101
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x7

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v16, v2, v10

    aput-object v17, v2, v11

    aput-object v18, v2, v14

    aput-object v19, v2, v3

    aput-object v20, v2, v4

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    const v1, 0x7a120

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v5, 0x6

    aput-object v1, v2, v5

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "PKR"

    const/16 v28, 0x5d

    const-string/jumbo v29, "\u20a8"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->PKR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 102
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v17, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "PLN"

    const/16 v37, 0x5e

    const-string/jumbo v38, "z\u0142"

    const-string v39, "gr"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->PLN:Lcom/squareup/currency_db/CurrencyInfo;

    .line 103
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const v1, 0x30d40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    const v1, 0x7a120

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    const v1, 0xf4240

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v14

    const v1, 0x1e8480

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const v1, 0x4c4b40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    const v1, 0x989680

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "PYG"

    const/16 v28, 0x5f

    const-string/jumbo v29, "\u20b2"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->PYG:Lcom/squareup/currency_db/CurrencyInfo;

    .line 104
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v18, v2, v3

    aput-object v19, v2, v4

    aput-object v20, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "QAR"

    const/16 v37, 0x60

    const-string v38, "QAR"

    const-string v39, "d"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->QAR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 105
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    aput-object v15, v1, v11

    aput-object v16, v1, v14

    aput-object v18, v1, v3

    aput-object v19, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "RON"

    const/16 v28, 0x61

    const-string v29, "Lei"

    const-string v30, "ban"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->RON:Lcom/squareup/currency_db/CurrencyInfo;

    .line 106
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x7

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v16, v2, v10

    aput-object v17, v2, v11

    aput-object v18, v2, v14

    aput-object v19, v2, v3

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    aput-object v20, v2, v12

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v5, 0x6

    aput-object v1, v2, v5

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "RSD"

    const/16 v37, 0x62

    const-string/jumbo v38, "\u0420\u0421\u0414"

    const-string v39, "para"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->RSD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 107
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v13, v1, v14

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v15, v1, v4

    aput-object v16, v1, v12

    const/4 v2, 0x6

    aput-object v18, v1, v2

    const/4 v2, 0x7

    aput-object v19, v1, v2

    const/16 v2, 0x8

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x9

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "RUB"

    const/16 v28, 0x63

    const-string/jumbo v29, "\u20bd"

    const-string/jumbo v30, "\u043a"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->RUB:Lcom/squareup/currency_db/CurrencyInfo;

    .line 108
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v4, [Ljava/lang/Integer;

    aput-object v20, v1, v10

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "RWF"

    const/16 v37, 0x64

    const-string v38, "FRw"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->RWF:Lcom/squareup/currency_db/CurrencyInfo;

    .line 109
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v13, v1, v4

    aput-object v15, v1, v12

    const/4 v2, 0x6

    aput-object v16, v1, v2

    const/4 v2, 0x7

    aput-object v17, v1, v2

    const/16 v2, 0x8

    aput-object v18, v1, v2

    const/16 v2, 0x9

    aput-object v19, v1, v2

    const/16 v2, 0xa

    aput-object v20, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "SAR"

    const/16 v28, 0x65

    const-string v29, "SAR"

    const-string v30, "h"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->SAR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 110
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v15, v1, v10

    aput-object v16, v1, v11

    aput-object v17, v1, v14

    aput-object v18, v1, v3

    aput-object v19, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "SBD"

    const/16 v37, 0x66

    const-string v38, "$"

    const-string/jumbo v39, "\u00a2"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->SBD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 111
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    const/16 v2, 0x9c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    aput-object v20, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "SCR"

    const/16 v28, 0x67

    const-string/jumbo v29, "\u20a8"

    const-string v30, "c"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->SCR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 112
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x7

    new-array v2, v1, [Ljava/lang/Integer;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    const/4 v1, 0x6

    aput-object v20, v2, v1

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "SEK"

    const/16 v37, 0x68

    const-string v38, "kr"

    const-string/jumbo v39, "\u00f6re"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->SEK:Lcom/squareup/currency_db/CurrencyInfo;

    .line 113
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v13, v1, v4

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    const/4 v2, 0x6

    aput-object v15, v1, v2

    const/4 v2, 0x7

    aput-object v16, v1, v2

    const/16 v2, 0x8

    aput-object v18, v1, v2

    const/16 v2, 0x9

    aput-object v19, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xa

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "SGD"

    const/16 v28, 0x69

    const-string v29, "S$"

    const-string v30, "c"

    const/16 v33, 0x5

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->SGD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 114
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v4, [Ljava/lang/Integer;

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "SLL"

    const/16 v37, 0x6a

    const-string v38, "Le"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->SLL:Lcom/squareup/currency_db/CurrencyInfo;

    .line 115
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x7

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    const/16 v1, 0xfa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    aput-object v15, v2, v14

    aput-object v16, v2, v3

    aput-object v17, v2, v4

    aput-object v18, v2, v12

    const/4 v1, 0x6

    aput-object v19, v2, v1

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "SRD"

    const/16 v28, 0x6b

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    const/16 v33, 0x1

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->SRD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 116
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const v2, 0x1e8480

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const v2, 0x4c4b40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0x989680

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "STD"

    const/16 v37, 0x6c

    const-string v38, "Db"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->STD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 117
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v15, v1, v14

    aput-object v16, v1, v3

    const/16 v2, 0x9c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object v18, v1, v12

    const/4 v2, 0x6

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "SVC"

    const/16 v28, 0x6d

    const-string/jumbo v29, "\u20a1"

    const-string/jumbo v30, "\u00a2"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->SVC:Lcom/squareup/currency_db/CurrencyInfo;

    .line 118
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v16, v1, v10

    aput-object v17, v1, v11

    aput-object v18, v1, v14

    aput-object v19, v1, v3

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "SZL"

    const/16 v37, 0x6e

    const-string v38, "E"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->SZL:Lcom/squareup/currency_db/CurrencyInfo;

    .line 119
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v17, v1, v10

    aput-object v18, v1, v11

    aput-object v19, v1, v14

    aput-object v20, v1, v3

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "THB"

    const/16 v28, 0x6f

    const-string/jumbo v29, "\u0e3f"

    const-string v30, "st"

    const/16 v33, 0x64

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->THB:Lcom/squareup/currency_db/CurrencyInfo;

    .line 120
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v4, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    aput-object v15, v1, v11

    aput-object v17, v1, v14

    aput-object v18, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "TJS"

    const/16 v37, 0x70

    const-string/jumbo v38, "\u0405\u041c"

    const-string v39, "d"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->TJS:Lcom/squareup/currency_db/CurrencyInfo;

    .line 121
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x7

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    const/4 v1, 0x6

    aput-object v20, v2, v1

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "TMT"

    const/16 v28, 0x71

    const-string v29, "m"

    const-string v30, ""

    const/16 v33, 0x1

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->TMT:Lcom/squareup/currency_db/CurrencyInfo;

    .line 122
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v4, [Ljava/lang/Integer;

    aput-object v18, v1, v10

    aput-object v19, v1, v11

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    aput-object v20, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "TND"

    const/16 v37, 0x72

    const-string v38, "TND"

    const-string v39, ""

    const/16 v40, 0x3

    const/16 v41, 0x3e8

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->TND:Lcom/squareup/currency_db/CurrencyInfo;

    .line 123
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Integer;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object v13, v1, v4

    aput-object v15, v1, v12

    const/4 v2, 0x6

    aput-object v16, v1, v2

    const/4 v2, 0x7

    aput-object v17, v1, v2

    const/16 v2, 0x8

    aput-object v18, v1, v2

    const/16 v2, 0x9

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xa

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "TRY"

    const/16 v28, 0x73

    const-string/jumbo v29, "\u20ba"

    const-string v30, "kr"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->TRY:Lcom/squareup/currency_db/CurrencyInfo;

    .line 124
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "TTD"

    const/16 v37, 0x74

    const-string v38, "$"

    const-string/jumbo v39, "\u00a2"

    const/16 v40, 0x2

    const/16 v41, 0x64

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->TTD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 125
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v3, [Ljava/lang/Integer;

    aput-object v19, v1, v10

    aput-object v20, v1, v11

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "TWD"

    const/16 v28, 0x75

    const-string v29, "$"

    const-string v30, "c"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->TWD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 126
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v20, v1, v10

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "TZS"

    const/16 v37, 0x76

    const-string v38, "Sh"

    const-string v39, "/."

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->TZS:Lcom/squareup/currency_db/CurrencyInfo;

    .line 127
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v15, v1, v14

    aput-object v16, v1, v3

    aput-object v17, v1, v4

    aput-object v18, v1, v12

    const/4 v2, 0x6

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    const/16 v2, 0x8

    aput-object v20, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "UAH"

    const/16 v28, 0x77

    const-string/jumbo v29, "\u20b4"

    const-string v30, "k"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->UAH:Lcom/squareup/currency_db/CurrencyInfo;

    .line 128
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    const v1, 0x30d40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    const v1, 0x7a120

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v14

    const v1, 0xf4240

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    const v1, 0x1e8480

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v4

    const v1, 0x4c4b40

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "UGX"

    const/16 v37, 0x78

    const-string v38, "USh"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->UGX:Lcom/squareup/currency_db/CurrencyInfo;

    .line 129
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v4, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    aput-object v15, v1, v11

    aput-object v16, v1, v14

    aput-object v17, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "USD"

    const/16 v28, 0x79

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->USD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 130
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    const/16 v2, 0x12c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v15, v1, v14

    aput-object v16, v1, v3

    const/16 v2, 0x9c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object v18, v1, v12

    const/4 v2, 0x6

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    const/16 v2, 0x8

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x9

    aput-object v2, v1, v5

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xa

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "UYU"

    const/16 v37, 0x7a

    const-string v38, "$"

    const-string/jumbo v39, "\u00a2"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->UYU:Lcom/squareup/currency_db/CurrencyInfo;

    .line 131
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    const/16 v2, 0x12c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v15, v1, v14

    aput-object v16, v1, v3

    const/16 v2, 0x9c4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object v18, v1, v12

    const/4 v2, 0x6

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    const/16 v2, 0x8

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x9

    aput-object v2, v1, v5

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xa

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "UZS"

    const/16 v28, 0x7b

    const-string/jumbo v29, "\u043b\u0432"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->UZS:Lcom/squareup/currency_db/CurrencyInfo;

    .line 132
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x7

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v13, v2, v10

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v11

    aput-object v15, v2, v14

    aput-object v16, v2, v3

    aput-object v17, v2, v4

    aput-object v18, v2, v12

    const/4 v1, 0x6

    aput-object v19, v2, v1

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "VEF"

    const/16 v37, 0x7c

    const-string v38, "Bs F"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->VEF:Lcom/squareup/currency_db/CurrencyInfo;

    .line 133
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Integer;

    aput-object v13, v1, v10

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    aput-object v15, v1, v14

    aput-object v16, v1, v3

    aput-object v17, v1, v4

    aput-object v18, v1, v12

    const/4 v2, 0x6

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v5, 0x7

    aput-object v2, v1, v5

    const/16 v2, 0x8

    aput-object v20, v1, v2

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0x9

    aput-object v2, v1, v5

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xa

    aput-object v2, v1, v5

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xb

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "VND"

    const/16 v28, 0x7d

    const-string/jumbo v29, "\u20ab"

    const-string v30, ""

    const/16 v31, 0x0

    const/16 v32, 0x1

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->VND:Lcom/squareup/currency_db/CurrencyInfo;

    .line 134
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v20, v1, v10

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "XAF"

    const/16 v37, 0x7e

    const-string v38, "Fr"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->XAF:Lcom/squareup/currency_db/CurrencyInfo;

    .line 135
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v15, v1, v10

    aput-object v16, v1, v11

    aput-object v17, v1, v14

    aput-object v18, v1, v3

    aput-object v19, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "XCD"

    const/16 v28, 0x7f

    const-string v29, "$"

    const-string/jumbo v30, "\u00a2"

    const/16 v31, 0x2

    const/16 v32, 0x64

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->XCD:Lcom/squareup/currency_db/CurrencyInfo;

    .line 136
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    new-array v1, v12, [Ljava/lang/Integer;

    aput-object v20, v1, v10

    const v2, 0x186a0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const v2, 0x30d40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    const v2, 0x7a120

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const v2, 0xf4240

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "XOF"

    const/16 v37, 0x80

    const-string v38, "Fr"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->XOF:Lcom/squareup/currency_db/CurrencyInfo;

    .line 137
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    aput-object v18, v2, v10

    aput-object v19, v2, v11

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v14

    const/16 v1, 0x61a8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    aput-object v20, v2, v4

    const v1, 0x186a0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "YER"

    const/16 v28, 0x81

    const-string v29, "YER"

    const-string v30, ""

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->YER:Lcom/squareup/currency_db/CurrencyInfo;

    .line 138
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    aput-object v13, v1, v3

    const/16 v2, 0xc8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    aput-object v15, v1, v12

    const/4 v2, 0x6

    aput-object v16, v1, v2

    const/4 v2, 0x7

    aput-object v17, v1, v2

    const/16 v2, 0x8

    aput-object v18, v1, v2

    const/16 v2, 0x9

    aput-object v19, v1, v2

    const/16 v2, 0x4e20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v5, 0xa

    aput-object v2, v1, v5

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v43

    const-string v36, "ZAR"

    const/16 v37, 0x82

    const-string v38, "R"

    const-string v39, "c"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v43}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->ZAR:Lcom/squareup/currency_db/CurrencyInfo;

    .line 139
    new-instance v0, Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v1, 0x6

    new-array v2, v1, [Ljava/lang/Integer;

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v10

    aput-object v15, v2, v11

    aput-object v16, v2, v14

    aput-object v17, v2, v3

    aput-object v18, v2, v4

    aput-object v19, v2, v12

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v34

    const-string v27, "ZMW"

    const/16 v28, 0x83

    const-string v29, "ZMK"

    const-string v30, "n"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v34}, Lcom/squareup/currency_db/CurrencyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->ZMW:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v0, 0x84

    new-array v0, v0, [Lcom/squareup/currency_db/CurrencyInfo;

    .line 7
    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->AED:Lcom/squareup/currency_db/CurrencyInfo;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->ALL:Lcom/squareup/currency_db/CurrencyInfo;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->AMD:Lcom/squareup/currency_db/CurrencyInfo;

    aput-object v1, v0, v14

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->AOA:Lcom/squareup/currency_db/CurrencyInfo;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->ARS:Lcom/squareup/currency_db/CurrencyInfo;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->AUD:Lcom/squareup/currency_db/CurrencyInfo;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->AWG:Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->AZN:Lcom/squareup/currency_db/CurrencyInfo;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BAM:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BBD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BDT:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BGN:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BHD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BMD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BND:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BOB:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BRL:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BSD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BTN:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BWP:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BYR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->BZD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->CAD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->CDF:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->CHF:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->CLP:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->CNY:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->COP:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->CRC:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->CVE:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->CZK:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->DKK:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->DOP:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->DZD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->EGP:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->ETB:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->EUR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->FJD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->GBP:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->GEL:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->GHS:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->GIP:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->GMD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->GTQ:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x2b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->GYD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x2c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->HKD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x2d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->HNL:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x2e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->HRK:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x2f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->HTG:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x30

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->HUF:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x31

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->IDR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x32

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->ILS:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x33

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->INR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x34

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->ISK:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x35

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->JMD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x36

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->JOD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x37

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->JPY:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x38

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->KES:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x39

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->KGS:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x3a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->KHR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x3b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->KRW:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x3c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->KWD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x3d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->KYD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x3e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->KZT:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x3f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->LAK:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x40

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->LBP:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x41

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->LKR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x42

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->LRD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x43

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->LTL:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x44

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MAD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x45

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MDL:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x46

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MGA:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x47

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MKD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x48

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MMK:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x49

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MNT:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x4a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MOP:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x4b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MRO:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x4c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MUR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x4d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MWK:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x4e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MXN:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x4f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MYR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x50

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->MZN:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x51

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->NAD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x52

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->NGN:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x53

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->NIO:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x54

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->NOK:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x55

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->NPR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x56

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->NZD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x57

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->OMR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x58

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->PAB:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x59

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->PEN:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x5a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->PGK:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x5b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->PHP:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x5c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->PKR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x5d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->PLN:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x5e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->PYG:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x5f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->QAR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x60

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->RON:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x61

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->RSD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x62

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->RUB:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x63

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->RWF:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x64

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->SAR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x65

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->SBD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x66

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->SCR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x67

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->SEK:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x68

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->SGD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x69

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->SLL:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x6a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->SRD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x6b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->STD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x6c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->SVC:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x6d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->SZL:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x6e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->THB:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x6f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->TJS:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x70

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->TMT:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x71

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->TND:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x72

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->TRY:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x73

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->TTD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x74

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->TWD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x75

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->TZS:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x76

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->UAH:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x77

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->UGX:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x78

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->USD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x79

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->UYU:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x7a

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->UZS:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x7b

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->VEF:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x7c

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->VND:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x7d

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->XAF:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x7e

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->XCD:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x7f

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->XOF:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x80

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->YER:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x81

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->ZAR:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x82

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/currency_db/CurrencyInfo;->ZMW:Lcom/squareup/currency_db/CurrencyInfo;

    const/16 v2, 0x83

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/currency_db/CurrencyInfo;->$VALUES:[Lcom/squareup/currency_db/CurrencyInfo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIILjava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "III",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 148
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 149
    iput-object p3, p0, Lcom/squareup/currency_db/CurrencyInfo;->symbol:Ljava/lang/String;

    .line 150
    iput-object p4, p0, Lcom/squareup/currency_db/CurrencyInfo;->subunit_symbol:Ljava/lang/String;

    .line 151
    iput p5, p0, Lcom/squareup/currency_db/CurrencyInfo;->fractionalDigits:I

    .line 152
    iput p6, p0, Lcom/squareup/currency_db/CurrencyInfo;->subunitsPerUnit:I

    .line 153
    iput p7, p0, Lcom/squareup/currency_db/CurrencyInfo;->swedishRoundingInterval:I

    .line 154
    iput-object p8, p0, Lcom/squareup/currency_db/CurrencyInfo;->denominations:Ljava/util/List;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/currency_db/CurrencyInfo;
    .locals 1

    .line 7
    const-class v0, Lcom/squareup/currency_db/CurrencyInfo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/currency_db/CurrencyInfo;

    return-object p0
.end method

.method public static values()[Lcom/squareup/currency_db/CurrencyInfo;
    .locals 1

    .line 7
    sget-object v0, Lcom/squareup/currency_db/CurrencyInfo;->$VALUES:[Lcom/squareup/currency_db/CurrencyInfo;

    invoke-virtual {v0}, [Lcom/squareup/currency_db/CurrencyInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/currency_db/CurrencyInfo;

    return-object v0
.end method
