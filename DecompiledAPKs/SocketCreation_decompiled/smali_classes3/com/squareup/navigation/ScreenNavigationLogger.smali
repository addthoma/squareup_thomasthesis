.class public Lcom/squareup/navigation/ScreenNavigationLogger;
.super Ljava/lang/Object;
.source "ScreenNavigationLogger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/navigation/ScreenNavigationLogger$LegacyViewAppearedEvent;,
        Lcom/squareup/navigation/ScreenNavigationLogger$Worker;
    }
.end annotation


# static fields
.field private static final ENTRY_SCREEN_NAME_SUFFIX:Ljava/lang/String; = " Entry Point"

.field private static final EXIT_SCREEN_NAME_SUFFIX:Ljava/lang/String; = " Exit Point"

.field private static final LAST_SCREEN_NAME_KEY:Ljava/lang/String; = "LAST_SCREEN_NAME"

.field private static final SAVED_SUFFIX:Ljava/lang/String; = " SAVED IN STATE"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/navigation/ScreenNavigationLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/navigation/ScreenNavigationLogger;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/navigation/ScreenNavigationLogger;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method static synthetic lambda$init$0(Lcom/squareup/navigation/ScreenNavigationLogger$Worker;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 50
    invoke-static {p0, p1}, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;->access$100(Lcom/squareup/navigation/ScreenNavigationLogger$Worker;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method


# virtual methods
.method public init(Lmortar/MortarScope;Lio/reactivex/Observable;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmortar/MortarScope;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/container/ContainerTreeKey;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;

    invoke-direct {v0, p0, p3}, Lcom/squareup/navigation/ScreenNavigationLogger$Worker;-><init>(Lcom/squareup/navigation/ScreenNavigationLogger;Ljava/lang/String;)V

    .line 49
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p3

    invoke-virtual {p3, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 50
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance p3, Lcom/squareup/navigation/-$$Lambda$ScreenNavigationLogger$yFO4HNywiSTsCEYl3-C_y-1mEGQ;

    invoke-direct {p3, v0}, Lcom/squareup/navigation/-$$Lambda$ScreenNavigationLogger$yFO4HNywiSTsCEYl3-C_y-1mEGQ;-><init>(Lcom/squareup/navigation/ScreenNavigationLogger$Worker;)V

    invoke-virtual {p2, p3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
