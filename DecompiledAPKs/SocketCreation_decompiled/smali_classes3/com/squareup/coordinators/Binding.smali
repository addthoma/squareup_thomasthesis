.class final Lcom/squareup/coordinators/Binding;
.super Ljava/lang/Object;
.source "Binding.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# instance fields
.field private final coordinator:Lcom/squareup/coordinators/Coordinator;

.field private final view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/coordinators/Coordinator;Landroid/view/View;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/coordinators/Binding;->coordinator:Lcom/squareup/coordinators/Coordinator;

    .line 27
    iput-object p2, p0, Lcom/squareup/coordinators/Binding;->view:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/coordinators/Binding;->view:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 35
    iget-object p1, p0, Lcom/squareup/coordinators/Binding;->coordinator:Lcom/squareup/coordinators/Coordinator;

    invoke-virtual {p1}, Lcom/squareup/coordinators/Coordinator;->isAttached()Z

    move-result p1

    if-nez p1, :cond_0

    .line 39
    iget-object p1, p0, Lcom/squareup/coordinators/Binding;->coordinator:Lcom/squareup/coordinators/Coordinator;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/coordinators/Coordinator;->setAttached(Z)V

    .line 40
    iget-object p1, p0, Lcom/squareup/coordinators/Binding;->coordinator:Lcom/squareup/coordinators/Coordinator;

    iget-object v0, p0, Lcom/squareup/coordinators/Binding;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 41
    iget-object p1, p0, Lcom/squareup/coordinators/Binding;->view:Landroid/view/View;

    sget v0, Lcom/squareup/coordinators/R$id;->coordinator:I

    iget-object v1, p0, Lcom/squareup/coordinators/Binding;->coordinator:Lcom/squareup/coordinators/Coordinator;

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    return-void

    .line 36
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Coordinator "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/coordinators/Binding;->coordinator:Lcom/squareup/coordinators/Coordinator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " is already attached"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 32
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Binding for view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/coordinators/Binding;->view:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " notified of attachment of different view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 3

    .line 45
    iget-object v0, p0, Lcom/squareup/coordinators/Binding;->view:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 49
    iget-object p1, p0, Lcom/squareup/coordinators/Binding;->coordinator:Lcom/squareup/coordinators/Coordinator;

    invoke-virtual {p1}, Lcom/squareup/coordinators/Coordinator;->isAttached()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 54
    :cond_0
    iget-object p1, p0, Lcom/squareup/coordinators/Binding;->coordinator:Lcom/squareup/coordinators/Coordinator;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/coordinators/Coordinator;->setAttached(Z)V

    .line 55
    iget-object p1, p0, Lcom/squareup/coordinators/Binding;->coordinator:Lcom/squareup/coordinators/Coordinator;

    iget-object v0, p0, Lcom/squareup/coordinators/Binding;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    .line 56
    iget-object p1, p0, Lcom/squareup/coordinators/Binding;->view:Landroid/view/View;

    sget v0, Lcom/squareup/coordinators/R$id;->coordinator:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    return-void

    .line 46
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Binding for view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/coordinators/Binding;->view:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " notified of detachment of different view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
