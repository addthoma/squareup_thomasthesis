.class public final Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;
.super Ljava/lang/Object;
.source "BluetoothAclConnectionReceiver_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/logging/BluetoothAclConnectionReceiver;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothDevicesCountInitHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/BluetoothDevicesCountInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/BluetoothDevicesCountInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;->bluetoothDevicesCountInitHelperProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;->applicationProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/BluetoothDevicesCountInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/logging/BluetoothDevicesCountInitializer;Landroid/app/Application;)Lcom/squareup/logging/BluetoothAclConnectionReceiver;
    .locals 1

    .line 48
    new-instance v0, Lcom/squareup/logging/BluetoothAclConnectionReceiver;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/logging/BluetoothAclConnectionReceiver;-><init>(Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/logging/BluetoothDevicesCountInitializer;Landroid/app/Application;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/logging/BluetoothAclConnectionReceiver;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/RealCardReaderListeners;

    iget-object v1, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;->bluetoothDevicesCountInitHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/logging/BluetoothDevicesCountInitializer;

    iget-object v2, p0, Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Application;

    invoke-static {v0, v1, v2}, Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;->newInstance(Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/logging/BluetoothDevicesCountInitializer;Landroid/app/Application;)Lcom/squareup/logging/BluetoothAclConnectionReceiver;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/logging/BluetoothAclConnectionReceiver_Factory;->get()Lcom/squareup/logging/BluetoothAclConnectionReceiver;

    move-result-object v0

    return-object v0
.end method
