.class public final Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;
.super Ljava/lang/Object;
.source "RealBluetoothDevicesCountInitializer.kt"

# interfaces
.implements Lcom/squareup/logging/BluetoothDevicesCountInitializer;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B5\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0016\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014H\u0002J\u0010\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0008\u0010\u0019\u001a\u00020\u001aH\u0002J\u0008\u0010\u001b\u001a\u00020\u0012H\u0016R\u000e\u0010\u000e\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;",
        "Lcom/squareup/logging/BluetoothDevicesCountInitializer;",
        "context",
        "Landroid/content/Context;",
        "bluetoothManager",
        "Landroid/bluetooth/BluetoothManager;",
        "cardReaderListeners",
        "Lcom/squareup/cardreader/RealCardReaderListeners;",
        "bluetoothAdapterProvider",
        "Ljavax/inject/Provider;",
        "Landroid/bluetooth/BluetoothAdapter;",
        "bluetoothUtils",
        "Lcom/squareup/cardreader/BluetoothUtils;",
        "(Landroid/content/Context;Landroid/bluetooth/BluetoothManager;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)V",
        "bluetoothAdapter",
        "shouldNotLog",
        "",
        "broadcastInitialConnectedDevices",
        "",
        "connectedDevices",
        "",
        "Landroid/bluetooth/BluetoothDevice;",
        "logBluetoothProfile",
        "bluetoothProfile",
        "",
        "serviceListener",
        "Landroid/bluetooth/BluetoothProfile$ServiceListener;",
        "setupInitialConnectedDevices",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final bluetoothAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothManager:Landroid/bluetooth/BluetoothManager;

.field private final bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

.field private final cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

.field private final context:Landroid/content/Context;

.field private shouldNotLog:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothManager;Lcom/squareup/cardreader/RealCardReaderListeners;Ljavax/inject/Provider;Lcom/squareup/cardreader/BluetoothUtils;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/bluetooth/BluetoothManager;",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bluetoothManager"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderListeners"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bluetoothAdapterProvider"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bluetoothUtils"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->bluetoothManager:Landroid/bluetooth/BluetoothManager;

    iput-object p3, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    iput-object p4, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    iput-object p5, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    return-void
.end method

.method public static final synthetic access$broadcastInitialConnectedDevices(Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;Ljava/util/List;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->broadcastInitialConnectedDevices(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$getBluetoothAdapter$p(Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;)Landroid/bluetooth/BluetoothAdapter;
    .locals 1

    .line 31
    iget-object p0, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez p0, :cond_0

    const-string v0, "bluetoothAdapter"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setBluetoothAdapter$p(Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;Landroid/bluetooth/BluetoothAdapter;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-void
.end method

.method private final broadcastInitialConnectedDevices(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;)V"
        }
    .end annotation

    .line 92
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 93
    invoke-static {v0}, Lcom/squareup/cardreader/WirelessConnection$Factory;->forBluetoothDevice(Landroid/bluetooth/BluetoothDevice;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    new-instance v2, Lcom/squareup/dipper/events/AclConnectedEvent;

    const-string/jumbo v3, "wirelessConnection"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lcom/squareup/dipper/events/AclConnectedEvent;-><init>(Lcom/squareup/cardreader/WirelessConnection;Z)V

    check-cast v2, Lcom/squareup/dipper/events/CardReaderDataEvent;

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishCardReaderDataEvent(Lcom/squareup/dipper/events/CardReaderDataEvent;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final logBluetoothProfile(I)V
    .locals 3

    .line 63
    iget-boolean v0, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->shouldNotLog:Z

    if-eqz v0, :cond_0

    return-void

    .line 67
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_1

    const-string v1, "bluetoothAdapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->serviceListener()Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 69
    iget-object v0, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    new-instance v1, Lcom/squareup/dipper/events/AclErrorEvent;

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/dipper/events/AclErrorEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/dipper/events/CardReaderDataEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishCardReaderDataEvent(Lcom/squareup/dipper/events/CardReaderDataEvent;)V

    const/4 p1, 0x1

    .line 70
    iput-boolean p1, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->shouldNotLog:Z

    :goto_0
    return-void
.end method

.method private final serviceListener()Landroid/bluetooth/BluetoothProfile$ServiceListener;
    .locals 1

    .line 75
    new-instance v0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer$serviceListener$1;

    invoke-direct {v0, p0}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer$serviceListener$1;-><init>(Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;)V

    check-cast v0, Landroid/bluetooth/BluetoothProfile$ServiceListener;

    return-object v0
.end method


# virtual methods
.method public setupInitialConnectedDevices()V
    .locals 3

    .line 43
    iget-object v0, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->bluetoothUtils:Lcom/squareup/cardreader/BluetoothUtils;

    invoke-interface {v0}, Lcom/squareup/cardreader/BluetoothUtils;->supportsBluetooth()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->cardReaderListeners:Lcom/squareup/cardreader/RealCardReaderListeners;

    new-instance v1, Lcom/squareup/dipper/events/AclErrorEvent;

    const-string v2, "Bluetooth not supported"

    invoke-direct {v1, v2}, Lcom/squareup/dipper/events/AclErrorEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/dipper/events/CardReaderDataEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishCardReaderDataEvent(Lcom/squareup/dipper/events/CardReaderDataEvent;)V

    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "bluetoothAdapterProvider.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/bluetooth/BluetoothAdapter;

    iput-object v0, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->bluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v0, 0x2

    .line 50
    invoke-direct {p0, v0}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->logBluetoothProfile(I)V

    const/4 v0, 0x1

    .line 51
    invoke-direct {p0, v0}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->logBluetoothProfile(I)V

    const/4 v0, 0x3

    .line 52
    invoke-direct {p0, v0}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->logBluetoothProfile(I)V

    .line 54
    iget-object v0, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->bluetoothManager:Landroid/bluetooth/BluetoothManager;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothManager;->getConnectedDevices(I)Ljava/util/List;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->bluetoothManager:Landroid/bluetooth/BluetoothManager;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothManager;->getConnectedDevices(I)Ljava/util/List;

    move-result-object v1

    const-string v2, "connectedGattDevices"

    .line 58
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->broadcastInitialConnectedDevices(Ljava/util/List;)V

    const-string v0, "connectedGattServerDevices"

    .line 59
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->broadcastInitialConnectedDevices(Ljava/util/List;)V

    return-void
.end method
