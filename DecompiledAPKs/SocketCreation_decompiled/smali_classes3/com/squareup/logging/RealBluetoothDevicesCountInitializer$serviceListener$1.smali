.class public final Lcom/squareup/logging/RealBluetoothDevicesCountInitializer$serviceListener$1;
.super Ljava/lang/Object;
.source "RealBluetoothDevicesCountInitializer.kt"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->serviceListener()Landroid/bluetooth/BluetoothProfile$ServiceListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBluetoothDevicesCountInitializer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBluetoothDevicesCountInitializer.kt\ncom/squareup/logging/RealBluetoothDevicesCountInitializer$serviceListener$1\n*L\n1#1,98:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0016J\u0010\u0010\u0008\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/logging/RealBluetoothDevicesCountInitializer$serviceListener$1",
        "Landroid/bluetooth/BluetoothProfile$ServiceListener;",
        "onServiceConnected",
        "",
        "profile",
        "",
        "proxy",
        "Landroid/bluetooth/BluetoothProfile;",
        "onServiceDisconnected",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;


# direct methods
.method constructor <init>(Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 75
    iput-object p1, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer$serviceListener$1;->this$0:Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3

    if-eqz p2, :cond_0

    .line 84
    iget-object v0, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer$serviceListener$1;->this$0:Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;

    invoke-interface {p2}, Landroid/bluetooth/BluetoothProfile;->getConnectedDevices()Ljava/util/List;

    move-result-object v1

    const-string/jumbo v2, "this.connectedDevices"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->access$broadcastInitialConnectedDevices(Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;Ljava/util/List;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer$serviceListener$1;->this$0:Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;

    invoke-static {v0}, Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;->access$getBluetoothAdapter$p(Lcom/squareup/logging/RealBluetoothDevicesCountInitializer;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    :cond_0
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 0

    return-void
.end method
