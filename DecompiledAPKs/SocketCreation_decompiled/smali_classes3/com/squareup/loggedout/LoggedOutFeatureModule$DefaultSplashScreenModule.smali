.class public abstract Lcom/squareup/loggedout/LoggedOutFeatureModule$DefaultSplashScreenModule;
.super Ljava/lang/Object;
.source "LoggedOutFeatureModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loggedout/LoggedOutFeatureModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "DefaultSplashScreenModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getPages()Ljava/util/List;
    .locals 4
    .annotation runtime Lcom/squareup/ui/loggedout/SplashCoordinator$Pages;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;

    .line 116
    new-instance v1, Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;

    sget v2, Lcom/squareup/loggedout/R$layout;->splash_page_accept_payments:I

    sget-object v3, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH_PAGE_1:Lcom/squareup/analytics/RegisterViewName;

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;-><init>(ILcom/squareup/analytics/RegisterViewName;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;

    sget v2, Lcom/squareup/loggedout/R$layout;->splash_page_sell_in_minutes:I

    sget-object v3, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH_PAGE_2:Lcom/squareup/analytics/RegisterViewName;

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;-><init>(ILcom/squareup/analytics/RegisterViewName;)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;

    sget v2, Lcom/squareup/loggedout/R$layout;->splash_page_reports:I

    sget-object v3, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH_PAGE_3:Lcom/squareup/analytics/RegisterViewName;

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;-><init>(ILcom/squareup/analytics/RegisterViewName;)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;

    sget v2, Lcom/squareup/loggedout/R$layout;->splash_page_build_trust:I

    sget-object v3, Lcom/squareup/analytics/RegisterViewName;->WELCOME_SPLASH_PAGE_4:Lcom/squareup/analytics/RegisterViewName;

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;-><init>(ILcom/squareup/analytics/RegisterViewName;)V

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static provideTextAboveImageSplashScreenConfig()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 137
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No TextAboveImageSplashScreenConfig has been set. Did you mean to use SplashScreen?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
