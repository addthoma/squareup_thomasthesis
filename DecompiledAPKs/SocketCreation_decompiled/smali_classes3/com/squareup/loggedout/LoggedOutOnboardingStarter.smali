.class public final Lcom/squareup/loggedout/LoggedOutOnboardingStarter;
.super Ljava/lang/Object;
.source "LoggedOutOnboardingStarter.kt"

# interfaces
.implements Lcom/squareup/onboarding/OnboardingStarter;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/loggedout/LoggedOutActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loggedout/LoggedOutOnboardingStarter$OnboardingMethod;,
        Lcom/squareup/loggedout/LoggedOutOnboardingStarter$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 \u00172\u00020\u0001:\u0002\u0017\u0018B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0008\u0010\u0015\u001a\u00020\u000fH\u0016J\u0010\u0010\u000b\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\rH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/loggedout/LoggedOutOnboardingStarter;",
        "Lcom/squareup/onboarding/OnboardingStarter;",
        "application",
        "Landroid/app/Application;",
        "onboardingType",
        "Lcom/squareup/onboarding/OnboardingType;",
        "onboardingActivityStarter",
        "Lcom/squareup/onboarding/OnboardingActivityStarter;",
        "loggedOutFinisher",
        "Lcom/squareup/ui/loggedout/LoggedOutFinisher;",
        "(Landroid/app/Application;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/onboarding/OnboardingActivityStarter;Lcom/squareup/ui/loggedout/LoggedOutFinisher;)V",
        "startActivation",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;",
        "doStartActivation",
        "",
        "method",
        "Lcom/squareup/loggedout/LoggedOutOnboardingStarter$OnboardingMethod;",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "params",
        "Companion",
        "OnboardingMethod",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ACTIVATION:Landroid/net/Uri;

.field public static final Companion:Lcom/squareup/loggedout/LoggedOutOnboardingStarter$Companion;


# instance fields
.field private final application:Landroid/app/Application;

.field private final loggedOutFinisher:Lcom/squareup/ui/loggedout/LoggedOutFinisher;

.field private final onboardingActivityStarter:Lcom/squareup/onboarding/OnboardingActivityStarter;

.field private final onboardingType:Lcom/squareup/onboarding/OnboardingType;

.field private final startActivation:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->Companion:Lcom/squareup/loggedout/LoggedOutOnboardingStarter$Companion;

    const-string v0, "square-register://root/activate"

    .line 74
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "Uri.parse(\"square-register://root/activate\")"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->ACTIVATION:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/squareup/onboarding/OnboardingType;Lcom/squareup/onboarding/OnboardingActivityStarter;Lcom/squareup/ui/loggedout/LoggedOutFinisher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingActivityStarter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loggedOutFinisher"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->application:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->onboardingType:Lcom/squareup/onboarding/OnboardingType;

    iput-object p3, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->onboardingActivityStarter:Lcom/squareup/onboarding/OnboardingActivityStarter;

    iput-object p4, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->loggedOutFinisher:Lcom/squareup/ui/loggedout/LoggedOutFinisher;

    .line 36
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->startActivation:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method public static final synthetic access$doStartActivation(Lcom/squareup/loggedout/LoggedOutOnboardingStarter;Lcom/squareup/loggedout/LoggedOutOnboardingStarter$OnboardingMethod;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->doStartActivation(Lcom/squareup/loggedout/LoggedOutOnboardingStarter$OnboardingMethod;)V

    return-void
.end method

.method public static final synthetic access$getOnboardingType$p(Lcom/squareup/loggedout/LoggedOutOnboardingStarter;)Lcom/squareup/onboarding/OnboardingType;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->onboardingType:Lcom/squareup/onboarding/OnboardingType;

    return-object p0
.end method

.method private final doStartActivation(Lcom/squareup/loggedout/LoggedOutOnboardingStarter$OnboardingMethod;)V
    .locals 2

    .line 62
    instance-of v0, p1, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$OnboardingMethod$ServerDriven;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->application:Landroid/app/Application;

    move-object v0, p1

    check-cast v0, Landroid/content/Context;

    sget-object v1, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->ACTIVATION:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/squareup/ui/main/MainActivity;->createDeepLinkIntent(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/Application;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 63
    :cond_0
    instance-of v0, p1, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$OnboardingMethod$LegacyMode;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->onboardingActivityStarter:Lcom/squareup/onboarding/OnboardingActivityStarter;

    check-cast p1, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$OnboardingMethod$LegacyMode;

    invoke-virtual {p1}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$OnboardingMethod$LegacyMode;->getParams()Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/onboarding/OnboardingActivityStarter;->startOnboardingActivity(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V

    .line 65
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->loggedOutFinisher:Lcom/squareup/ui/loggedout/LoggedOutFinisher;

    invoke-virtual {p1}, Lcom/squareup/ui/loggedout/LoggedOutFinisher;->finish()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->startActivation:Lcom/jakewharton/rxrelay/PublishRelay;

    check-cast v0, Lrx/Observable;

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 40
    new-instance v1, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$onEnterScope$1;-><init>(Lcom/squareup/loggedout/LoggedOutOnboardingStarter;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 47
    new-instance v1, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$onEnterScope$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;

    invoke-direct {v1, v2}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$onEnterScope$2;-><init>(Lcom/squareup/loggedout/LoggedOutOnboardingStarter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "startActivation.toV2Obse\u2026ribe(::doStartActivation)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public startActivation(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V
    .locals 1

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/loggedout/LoggedOutOnboardingStarter;->startActivation:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
