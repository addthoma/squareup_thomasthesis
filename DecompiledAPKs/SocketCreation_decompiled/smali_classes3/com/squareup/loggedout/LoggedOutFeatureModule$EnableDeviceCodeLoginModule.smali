.class public Lcom/squareup/loggedout/LoggedOutFeatureModule$EnableDeviceCodeLoginModule;
.super Ljava/lang/Object;
.source "LoggedOutFeatureModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loggedout/LoggedOutFeatureModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EnableDeviceCodeLoginModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideSupportsDeviceCodeLogin()Lcom/squareup/ui/login/SupportsDeviceCodeLogin;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 76
    sget-object v0, Lcom/squareup/ui/login/SupportsDeviceCodeLogin$Enabled;->INSTANCE:Lcom/squareup/ui/login/SupportsDeviceCodeLogin$Enabled;

    return-object v0
.end method
