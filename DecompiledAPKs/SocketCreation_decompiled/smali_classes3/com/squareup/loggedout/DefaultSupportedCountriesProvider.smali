.class public final Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;
.super Ljava/lang/Object;
.source "DefaultSupportedCountriesProvider.kt"

# interfaces
.implements Lcom/squareup/loggedout/SupportedCountriesProvider;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0013\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u0016\u00a2\u0006\u0002\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;",
        "Lcom/squareup/loggedout/SupportedCountriesProvider;",
        "resources",
        "Landroid/content/res/Resources;",
        "(Landroid/content/res/Resources;)V",
        "getSupportedCountries",
        "",
        "Lcom/squareup/CountryCode;",
        "()[Lcom/squareup/CountryCode;",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final resources:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;->resources:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public getSupportedCountries()[Lcom/squareup/CountryCode;
    .locals 2

    .line 14
    iget-object v0, p0, Lcom/squareup/loggedout/DefaultSupportedCountriesProvider;->resources:Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/squareup/address/CountryResources;->countryCodesOrderedByNameWithPaymentFirst(Landroid/content/res/Resources;)[Lcom/squareup/CountryCode;

    move-result-object v0

    const-string v1, "countryCodesOrderedByNam\u2026thPaymentFirst(resources)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
