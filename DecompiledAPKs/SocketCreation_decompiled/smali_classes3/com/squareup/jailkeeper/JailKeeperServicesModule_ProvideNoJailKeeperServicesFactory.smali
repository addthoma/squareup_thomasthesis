.class public final Lcom/squareup/jailkeeper/JailKeeperServicesModule_ProvideNoJailKeeperServicesFactory;
.super Ljava/lang/Object;
.source "JailKeeperServicesModule_ProvideNoJailKeeperServicesFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/Set<",
        "Lcom/squareup/jailkeeper/JailKeeperService;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/jailkeeper/JailKeeperServicesModule;


# direct methods
.method public constructor <init>(Lcom/squareup/jailkeeper/JailKeeperServicesModule;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/jailkeeper/JailKeeperServicesModule_ProvideNoJailKeeperServicesFactory;->module:Lcom/squareup/jailkeeper/JailKeeperServicesModule;

    return-void
.end method

.method public static create(Lcom/squareup/jailkeeper/JailKeeperServicesModule;)Lcom/squareup/jailkeeper/JailKeeperServicesModule_ProvideNoJailKeeperServicesFactory;
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/jailkeeper/JailKeeperServicesModule_ProvideNoJailKeeperServicesFactory;

    invoke-direct {v0, p0}, Lcom/squareup/jailkeeper/JailKeeperServicesModule_ProvideNoJailKeeperServicesFactory;-><init>(Lcom/squareup/jailkeeper/JailKeeperServicesModule;)V

    return-object v0
.end method

.method public static provideNoJailKeeperServices(Lcom/squareup/jailkeeper/JailKeeperServicesModule;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/jailkeeper/JailKeeperServicesModule;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/jailkeeper/JailKeeperService;",
            ">;"
        }
    .end annotation

    .line 36
    invoke-virtual {p0}, Lcom/squareup/jailkeeper/JailKeeperServicesModule;->provideNoJailKeeperServices()Ljava/util/Set;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Set;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/jailkeeper/JailKeeperServicesModule_ProvideNoJailKeeperServicesFactory;->get()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/jailkeeper/JailKeeperService;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/jailkeeper/JailKeeperServicesModule_ProvideNoJailKeeperServicesFactory;->module:Lcom/squareup/jailkeeper/JailKeeperServicesModule;

    invoke-static {v0}, Lcom/squareup/jailkeeper/JailKeeperServicesModule_ProvideNoJailKeeperServicesFactory;->provideNoJailKeeperServices(Lcom/squareup/jailkeeper/JailKeeperServicesModule;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
