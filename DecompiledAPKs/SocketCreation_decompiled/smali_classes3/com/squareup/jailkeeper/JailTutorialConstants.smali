.class public final Lcom/squareup/jailkeeper/JailTutorialConstants;
.super Ljava/lang/Object;
.source "JailTutorialConstants.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0002"
    }
    d2 = {
        "SHOWN_JAILSCREEN",
        "",
        "jailkeeper_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final SHOWN_JAILSCREEN:Ljava/lang/String; = "Shown JailScreen"
