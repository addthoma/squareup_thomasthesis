.class public final Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CartShowCartBanner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/CartShowCartBanner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/CartShowCartBanner;",
        "Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000c\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0005J\u000e\u0010\u0008\u001a\u00020\u00002\u0006\u0010\u0008\u001a\u00020\u0005J\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0005J\u0010\u0010\n\u001a\u00020\u00002\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0008\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/CartShowCartBanner;",
        "()V",
        "isChipCardInserted",
        "",
        "Ljava/lang/Boolean;",
        "isChipCardSwiped",
        "isPreviousCardInserted",
        "isSwipeFailure",
        "swipedCard",
        "Lcom/squareup/comms/protos/common/Card;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public isChipCardInserted:Ljava/lang/Boolean;

.field public isChipCardSwiped:Ljava/lang/Boolean;

.field public isPreviousCardInserted:Ljava/lang/Boolean;

.field public isSwipeFailure:Ljava/lang/Boolean;

.field public swipedCard:Lcom/squareup/comms/protos/common/Card;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 120
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/seller/CartShowCartBanner;
    .locals 11

    .line 161
    new-instance v7, Lcom/squareup/comms/protos/seller/CartShowCartBanner;

    .line 162
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->isPreviousCardInserted:Ljava/lang/Boolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 164
    iget-object v5, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->swipedCard:Lcom/squareup/comms/protos/common/Card;

    .line 165
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->isChipCardInserted:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 167
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->isSwipeFailure:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 169
    iget-object v0, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->isChipCardSwiped:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    .line 171
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v7

    move v1, v4

    move-object v2, v5

    move v3, v6

    move v4, v8

    move v5, v9

    move-object v6, v10

    .line 161
    invoke-direct/range {v0 .. v6}, Lcom/squareup/comms/protos/seller/CartShowCartBanner;-><init>(ZLcom/squareup/comms/protos/common/Card;ZZZLokio/ByteString;)V

    return-object v7

    :cond_0
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "isChipCardSwiped"

    aput-object v0, v3, v1

    .line 169
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "isSwipeFailure"

    aput-object v0, v3, v1

    .line 167
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "isChipCardInserted"

    aput-object v0, v3, v1

    .line 165
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "isPreviousCardInserted"

    aput-object v0, v3, v1

    .line 163
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 120
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->build()Lcom/squareup/comms/protos/seller/CartShowCartBanner;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final isChipCardInserted(Z)Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;
    .locals 0

    .line 147
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->isChipCardInserted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final isChipCardSwiped(Z)Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;
    .locals 0

    .line 157
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->isChipCardSwiped:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final isPreviousCardInserted(Z)Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;
    .locals 0

    .line 137
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->isPreviousCardInserted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final isSwipeFailure(Z)Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;
    .locals 0

    .line 152
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->isSwipeFailure:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final swipedCard(Lcom/squareup/comms/protos/common/Card;)Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/CartShowCartBanner$Builder;->swipedCard:Lcom/squareup/comms/protos/common/Card;

    return-object p0
.end method
