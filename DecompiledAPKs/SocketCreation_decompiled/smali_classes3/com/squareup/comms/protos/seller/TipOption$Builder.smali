.class public final Lcom/squareup/comms/protos/seller/TipOption$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TipOption.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/TipOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/TipOption;",
        "Lcom/squareup/comms/protos/seller/TipOption$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u0006\n\u0002\u0008\u0005\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0015\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u000b\u001a\u00020\u0002H\u0016J\u0015\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0002\u0010\u000cR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/TipOption$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/TipOption;",
        "()V",
        "amount",
        "",
        "Ljava/lang/Long;",
        "percentage",
        "",
        "Ljava/lang/Double;",
        "(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/TipOption$Builder;",
        "build",
        "(Ljava/lang/Double;)Lcom/squareup/comms/protos/seller/TipOption$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public amount:Ljava/lang/Long;

.field public percentage:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 84
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final amount(Ljava/lang/Long;)Lcom/squareup/comms/protos/seller/TipOption$Builder;
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/TipOption$Builder;->amount:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/TipOption;
    .locals 4

    .line 107
    new-instance v0, Lcom/squareup/comms/protos/seller/TipOption;

    .line 108
    iget-object v1, p0, Lcom/squareup/comms/protos/seller/TipOption$Builder;->amount:Ljava/lang/Long;

    .line 109
    iget-object v2, p0, Lcom/squareup/comms/protos/seller/TipOption$Builder;->percentage:Ljava/lang/Double;

    .line 110
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/TipOption$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    .line 107
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/comms/protos/seller/TipOption;-><init>(Ljava/lang/Long;Ljava/lang/Double;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/TipOption$Builder;->build()Lcom/squareup/comms/protos/seller/TipOption;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final percentage(Ljava/lang/Double;)Lcom/squareup/comms/protos/seller/TipOption$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/TipOption$Builder;->percentage:Ljava/lang/Double;

    return-object p0
.end method
