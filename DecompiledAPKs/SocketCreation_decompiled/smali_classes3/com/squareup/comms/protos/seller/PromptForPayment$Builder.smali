.class public final Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PromptForPayment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/PromptForPayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/PromptForPayment;",
        "Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0008J\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u0005J\u0008\u0010\u0012\u001a\u00020\u0002H\u0016J\u000e\u0010\u000b\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u0008J\u000e\u0010\u000c\u001a\u00020\u00002\u0006\u0010\u000c\u001a\u00020\u0008J\u000e\u0010\r\u001a\u00020\u00002\u0006\u0010\r\u001a\u00020\u000eJ\u000e\u0010\u000f\u001a\u00020\u00002\u0006\u0010\u000f\u001a\u00020\u0005J\u000e\u0010\u0010\u001a\u00020\u00002\u0006\u0010\u0010\u001a\u00020\u0011R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0016\u0010\n\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0016\u0010\u000c\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00118\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/PromptForPayment;",
        "()V",
        "above_credit_card_maximum",
        "",
        "Ljava/lang/Boolean;",
        "amount",
        "",
        "Ljava/lang/Long;",
        "below_credit_card_minimum",
        "credit_card_maximum",
        "credit_card_minimum",
        "display_variation",
        "Lcom/squareup/comms/protos/seller/PaymentPromptVariation;",
        "offline_mode",
        "time",
        "Lcom/squareup/comms/protos/seller/Time;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public above_credit_card_maximum:Ljava/lang/Boolean;

.field public amount:Ljava/lang/Long;

.field public below_credit_card_minimum:Ljava/lang/Boolean;

.field public credit_card_maximum:Ljava/lang/Long;

.field public credit_card_minimum:Ljava/lang/Long;

.field public display_variation:Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

.field public offline_mode:Ljava/lang/Boolean;

.field public time:Lcom/squareup/comms/protos/seller/Time;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 178
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final above_credit_card_maximum(Z)Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;
    .locals 0

    .line 239
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->above_credit_card_maximum:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final amount(J)Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;
    .locals 0

    .line 207
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->amount:Ljava/lang/Long;

    return-object p0
.end method

.method public final below_credit_card_minimum(Z)Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;
    .locals 0

    .line 231
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->below_credit_card_minimum:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/PromptForPayment;
    .locals 18

    move-object/from16 v0, p0

    .line 268
    new-instance v14, Lcom/squareup/comms/protos/seller/PromptForPayment;

    .line 269
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->amount:Ljava/lang/Long;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 270
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->credit_card_minimum:Ljava/lang/Long;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    .line 272
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->credit_card_maximum:Ljava/lang/Long;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    .line 274
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->below_credit_card_minimum:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    .line 276
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->above_credit_card_maximum:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    .line 278
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->offline_mode:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    .line 279
    iget-object v15, v0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->time:Lcom/squareup/comms/protos/seller/Time;

    if-eqz v15, :cond_1

    .line 280
    iget-object v1, v0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->display_variation:Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    if-eqz v1, :cond_0

    .line 282
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v17, v1

    move-object v1, v14

    move-wide v2, v5

    move-wide v4, v7

    move-wide v6, v9

    move v8, v11

    move v9, v12

    move v10, v13

    move-object v11, v15

    move-object/from16 v12, v17

    move-object/from16 v13, v16

    .line 268
    invoke-direct/range {v1 .. v13}, Lcom/squareup/comms/protos/seller/PromptForPayment;-><init>(JJJZZZLcom/squareup/comms/protos/seller/Time;Lcom/squareup/comms/protos/seller/PaymentPromptVariation;Lokio/ByteString;)V

    return-object v14

    :cond_0
    move-object/from16 v17, v1

    new-array v1, v4, [Ljava/lang/Object;

    aput-object v17, v1, v3

    const-string v3, "display_variation"

    aput-object v3, v1, v2

    .line 280
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_1
    new-array v1, v4, [Ljava/lang/Object;

    aput-object v15, v1, v3

    const-string/jumbo v3, "time"

    aput-object v3, v1, v2

    .line 279
    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_2
    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    const-string v1, "offline_mode"

    aput-object v1, v4, v2

    .line 278
    invoke-static {v4}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_3
    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    const-string v1, "above_credit_card_maximum"

    aput-object v1, v4, v2

    .line 277
    invoke-static {v4}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_4
    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    const-string v1, "below_credit_card_minimum"

    aput-object v1, v4, v2

    .line 275
    invoke-static {v4}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_5
    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    const-string v1, "credit_card_maximum"

    aput-object v1, v4, v2

    .line 272
    invoke-static {v4}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_6
    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    const-string v1, "credit_card_minimum"

    aput-object v1, v4, v2

    .line 270
    invoke-static {v4}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_7
    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v3

    const-string v1, "amount"

    aput-object v1, v4, v2

    .line 269
    invoke-static {v4}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 178
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->build()Lcom/squareup/comms/protos/seller/PromptForPayment;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final credit_card_maximum(J)Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;
    .locals 0

    .line 223
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->credit_card_maximum:Ljava/lang/Long;

    return-object p0
.end method

.method public final credit_card_minimum(J)Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;
    .locals 0

    .line 215
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->credit_card_minimum:Ljava/lang/Long;

    return-object p0
.end method

.method public final display_variation(Lcom/squareup/comms/protos/seller/PaymentPromptVariation;)Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;
    .locals 1

    const-string v0, "display_variation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->display_variation:Lcom/squareup/comms/protos/seller/PaymentPromptVariation;

    return-object p0
.end method

.method public final offline_mode(Z)Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;
    .locals 0

    .line 247
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->offline_mode:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final time(Lcom/squareup/comms/protos/seller/Time;)Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;
    .locals 1

    const-string/jumbo v0, "time"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/PromptForPayment$Builder;->time:Lcom/squareup/comms/protos/seller/Time;

    return-object p0
.end method
