.class public final Lcom/squareup/comms/protos/seller/SellerDevice$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SellerDevice.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/seller/SellerDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/seller/SellerDevice;",
        "Lcom/squareup/comms/protos/seller/SellerDevice$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u0015\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0015J\u0008\u0010\u0016\u001a\u00020\u0002H\u0016J\u000e\u0010\u0008\u001a\u00020\u00002\u0006\u0010\u0008\u001a\u00020\tJ\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\tJ\u000e\u0010\u000b\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u000cJ\u0015\u0010\u000e\u001a\u00020\u00002\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0015J\u0015\u0010\u000f\u001a\u00020\u00002\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0015J\u0015\u0010\u0010\u001a\u00020\u00002\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0015J\u000e\u0010\u0011\u001a\u00020\u00002\u0006\u0010\u0011\u001a\u00020\tJ\u0015\u0010\u0012\u001a\u00020\u00002\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0015J\u000e\u0010\u0013\u001a\u00020\u00002\u0006\u0010\u0013\u001a\u00020\u0014R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u0008\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u000c8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u0013\u001a\u0004\u0018\u00010\u00148\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/comms/protos/seller/SellerDevice$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/seller/SellerDevice;",
        "()V",
        "always_compatible",
        "",
        "Ljava/lang/Boolean;",
        "bran_display_api_x2",
        "country_code",
        "",
        "currency_code",
        "mcc",
        "",
        "Ljava/lang/Integer;",
        "monitor_state_machine",
        "payment_monitor_workflows_x2",
        "quick_chip",
        "sha",
        "spe_fwup_without_matching_tms",
        "time",
        "Lcom/squareup/comms/protos/seller/Time;",
        "(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SellerDevice$Builder;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public always_compatible:Ljava/lang/Boolean;

.field public bran_display_api_x2:Ljava/lang/Boolean;

.field public country_code:Ljava/lang/String;

.field public currency_code:Ljava/lang/String;

.field public mcc:Ljava/lang/Integer;

.field public monitor_state_machine:Ljava/lang/Boolean;

.field public payment_monitor_workflows_x2:Ljava/lang/Boolean;

.field public quick_chip:Ljava/lang/Boolean;

.field public sha:Ljava/lang/String;

.field public spe_fwup_without_matching_tms:Ljava/lang/Boolean;

.field public time:Lcom/squareup/comms/protos/seller/Time;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 215
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final always_compatible(Z)Lcom/squareup/comms/protos/seller/SellerDevice$Builder;
    .locals 0

    .line 263
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->always_compatible:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final bran_display_api_x2(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SellerDevice$Builder;
    .locals 0

    .line 316
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->bran_display_api_x2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/seller/SellerDevice;
    .locals 17

    move-object/from16 v0, p0

    .line 328
    new-instance v14, Lcom/squareup/comms/protos/seller/SellerDevice;

    .line 329
    iget-object v2, v0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->sha:Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    if-eqz v2, :cond_5

    .line 330
    iget-object v5, v0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->currency_code:Ljava/lang/String;

    if-eqz v5, :cond_4

    .line 331
    iget-object v6, v0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->always_compatible:Ljava/lang/Boolean;

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 333
    iget-object v7, v0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->mcc:Ljava/lang/Integer;

    if-eqz v7, :cond_2

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 334
    iget-object v8, v0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->time:Lcom/squareup/comms/protos/seller/Time;

    if-eqz v8, :cond_1

    .line 335
    iget-object v9, v0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->country_code:Ljava/lang/String;

    if-eqz v9, :cond_0

    .line 336
    iget-object v10, v0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->quick_chip:Ljava/lang/Boolean;

    .line 337
    iget-object v11, v0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->monitor_state_machine:Ljava/lang/Boolean;

    .line 338
    iget-object v12, v0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    .line 339
    iget-object v13, v0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->bran_display_api_x2:Ljava/lang/Boolean;

    .line 340
    iget-object v15, v0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->payment_monitor_workflows_x2:Ljava/lang/Boolean;

    .line 341
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object v1, v14

    move-object v3, v5

    move v4, v6

    move v5, v7

    move-object v6, v8

    move-object v7, v9

    move-object v8, v10

    move-object v9, v11

    move-object v10, v12

    move-object v11, v13

    move-object v12, v15

    move-object/from16 v13, v16

    .line 328
    invoke-direct/range {v1 .. v13}, Lcom/squareup/comms/protos/seller/SellerDevice;-><init>(Ljava/lang/String;Ljava/lang/String;ZILcom/squareup/comms/protos/seller/Time;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v14

    :cond_0
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v9, v2, v3

    const-string v3, "country_code"

    aput-object v3, v2, v1

    .line 335
    invoke-static {v2}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_1
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v8, v2, v3

    const-string/jumbo v3, "time"

    aput-object v3, v2, v1

    .line 334
    invoke-static {v2}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_2
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v7, v2, v3

    const-string v3, "mcc"

    aput-object v3, v2, v1

    .line 333
    invoke-static {v2}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_3
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v6, v2, v3

    const-string v3, "always_compatible"

    aput-object v3, v2, v1

    .line 331
    invoke-static {v2}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_4
    new-array v2, v4, [Ljava/lang/Object;

    aput-object v5, v2, v3

    const-string v3, "currency_code"

    aput-object v3, v2, v1

    .line 330
    invoke-static {v2}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    :cond_5
    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v3

    const-string v2, "sha"

    aput-object v2, v4, v1

    .line 329
    invoke-static {v4}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v1

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 215
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->build()Lcom/squareup/comms/protos/seller/SellerDevice;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final country_code(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/SellerDevice$Builder;
    .locals 1

    const-string v0, "country_code"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->country_code:Ljava/lang/String;

    return-object p0
.end method

.method public final currency_code(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/SellerDevice$Builder;
    .locals 1

    const-string v0, "currency_code"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 255
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->currency_code:Ljava/lang/String;

    return-object p0
.end method

.method public final mcc(I)Lcom/squareup/comms/protos/seller/SellerDevice$Builder;
    .locals 0

    .line 268
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->mcc:Ljava/lang/Integer;

    return-object p0
.end method

.method public final monitor_state_machine(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SellerDevice$Builder;
    .locals 0

    .line 300
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->monitor_state_machine:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final payment_monitor_workflows_x2(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SellerDevice$Builder;
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->payment_monitor_workflows_x2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final quick_chip(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SellerDevice$Builder;
    .locals 0

    .line 292
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->quick_chip:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final sha(Ljava/lang/String;)Lcom/squareup/comms/protos/seller/SellerDevice$Builder;
    .locals 1

    const-string v0, "sha"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->sha:Ljava/lang/String;

    return-object p0
.end method

.method public final spe_fwup_without_matching_tms(Ljava/lang/Boolean;)Lcom/squareup/comms/protos/seller/SellerDevice$Builder;
    .locals 0

    .line 308
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final time(Lcom/squareup/comms/protos/seller/Time;)Lcom/squareup/comms/protos/seller/SellerDevice$Builder;
    .locals 1

    const-string/jumbo v0, "time"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 276
    iput-object p1, p0, Lcom/squareup/comms/protos/seller/SellerDevice$Builder;->time:Lcom/squareup/comms/protos/seller/Time;

    return-object p0
.end method
