.class public Lcom/squareup/ms/Ms;
.super Ljava/lang/Object;
.source "Ms.java"

# interfaces
.implements Lcom/squareup/ms/Minesweeper;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ms/Ms$MsNativeMethodDelegate;,
        Lcom/squareup/ms/Ms$PauseTask;,
        Lcom/squareup/ms/Ms$InitializedCallback;,
        Lcom/squareup/ms/Ms$State;
    }
.end annotation


# static fields
.field private static final GRACE_PERIOD_MILLIS:J = 0xbb8L


# instance fields
.field private callback:Lcom/squareup/ms/Minesweeper$DataListener;

.field private final crashnado:Lcom/squareup/crashnado/Crashnado;

.field private final executor:Ljava/util/concurrent/ExecutorService;

.field private initialized:Lcom/squareup/ms/Ms$State;

.field private initializedCallback:Lcom/squareup/ms/Ms$InitializedCallback;

.field private internalFilesDir:Ljava/io/File;

.field private final mainThread:Landroid/os/Handler;

.field private final minesweeperLogger:Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

.field private final msNativeMethodDelegate:Lcom/squareup/ms/Ms$MsNativeMethodDelegate;

.field private timer:Ljava/util/Timer;


# direct methods
.method constructor <init>(Lcom/squareup/crashnado/Crashnado;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;Lcom/squareup/ms/Minesweeper$MinesweeperLogger;Lcom/squareup/ms/Ms$MsNativeMethodDelegate;)V
    .locals 0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p2, p0, Lcom/squareup/ms/Ms;->executor:Ljava/util/concurrent/ExecutorService;

    .line 50
    iput-object p3, p0, Lcom/squareup/ms/Ms;->mainThread:Landroid/os/Handler;

    .line 51
    iput-object p1, p0, Lcom/squareup/ms/Ms;->crashnado:Lcom/squareup/crashnado/Crashnado;

    .line 52
    iput-object p4, p0, Lcom/squareup/ms/Ms;->minesweeperLogger:Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

    .line 53
    iput-object p5, p0, Lcom/squareup/ms/Ms;->msNativeMethodDelegate:Lcom/squareup/ms/Ms$MsNativeMethodDelegate;

    .line 54
    sget-object p1, Lcom/squareup/ms/Ms$State;->NONE:Lcom/squareup/ms/Ms$State;

    iput-object p1, p0, Lcom/squareup/ms/Ms;->initialized:Lcom/squareup/ms/Ms$State;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ms/Ms;)Ljava/util/concurrent/ExecutorService;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/ms/Ms;->executor:Ljava/util/concurrent/ExecutorService;

    return-object p0
.end method

.method static synthetic access$200()I
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/ms/Ms;->ms_pause()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V
    .locals 0

    .line 23
    invoke-static {p0}, Lcom/squareup/ms/Ms;->ms_disabled_status_add_listener(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V

    return-void
.end method

.method static synthetic access$400(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V
    .locals 0

    .line 23
    invoke-static {p0}, Lcom/squareup/ms/Ms;->ms_disabled_status_remove_listener(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V

    return-void
.end method

.method public static synthetic lambda$QdNR-XQp62xL17DmHsNpli_zAw0()I
    .locals 1

    invoke-static {}, Lcom/squareup/ms/Ms;->ms_resume()I

    move-result v0

    return v0
.end method

.method static synthetic lambda$passDataToMinesweeper$1([B)V
    .locals 1

    .line 110
    array-length v0, p0

    if-lez v0, :cond_0

    .line 111
    invoke-static {p0}, Lcom/squareup/ms/Ms;->ms_pass_data([B)V

    :cond_0
    return-void
.end method

.method private static native ms_disabled_status_add_listener(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V
.end method

.method private static native ms_disabled_status_remove_listener(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V
.end method

.method private static native ms_init(Lcom/squareup/ms/Ms;Landroid/content/Context;)I
.end method

.method private static native ms_pass_data([B)V
.end method

.method private static native ms_pause()I
.end method

.method private static native ms_resume()I
.end method

.method private static native ms_update_ticket_async()V
.end method

.method private writeback([B)V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/ms/Ms;->initialized:Lcom/squareup/ms/Ms$State;

    sget-object v1, Lcom/squareup/ms/Ms$State;->CALLED_BACK:Lcom/squareup/ms/Ms$State;

    if-eq v0, v1, :cond_0

    .line 135
    sget-object v0, Lcom/squareup/ms/Ms$State;->CALLED_BACK:Lcom/squareup/ms/Ms$State;

    iput-object v0, p0, Lcom/squareup/ms/Ms;->initialized:Lcom/squareup/ms/Ms$State;

    .line 136
    iget-object v0, p0, Lcom/squareup/ms/Ms;->initializedCallback:Lcom/squareup/ms/Ms$InitializedCallback;

    invoke-interface {v0, p1}, Lcom/squareup/ms/Ms$InitializedCallback;->onInitialized([B)V

    const/4 v0, 0x0

    .line 137
    iput-object v0, p0, Lcom/squareup/ms/Ms;->initializedCallback:Lcom/squareup/ms/Ms$InitializedCallback;

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/squareup/ms/Ms;->mainThread:Landroid/os/Handler;

    new-instance v1, Lcom/squareup/ms/-$$Lambda$Ms$k1u6jltLgKuv-GTKY2yySMZVNLM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ms/-$$Lambda$Ms$k1u6jltLgKuv-GTKY2yySMZVNLM;-><init>(Lcom/squareup/ms/Ms;[B)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public addStatusListener(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/ms/Ms;->msNativeMethodDelegate:Lcom/squareup/ms/Ms$MsNativeMethodDelegate;

    invoke-virtual {v0, p1}, Lcom/squareup/ms/Ms$MsNativeMethodDelegate;->ms_disabled_status_add_listener(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V

    return-void
.end method

.method declared-synchronized initialize(Landroid/app/Application;Lcom/squareup/ms/Minesweeper$DataListener;Lcom/squareup/ms/Ms$InitializedCallback;)V
    .locals 2

    monitor-enter p0

    .line 67
    :try_start_0
    iget-object v0, p0, Lcom/squareup/ms/Ms;->initialized:Lcom/squareup/ms/Ms$State;

    sget-object v1, Lcom/squareup/ms/Ms$State;->NONE:Lcom/squareup/ms/Ms$State;

    if-ne v0, v1, :cond_0

    .line 68
    iput-object p2, p0, Lcom/squareup/ms/Ms;->callback:Lcom/squareup/ms/Minesweeper$DataListener;

    .line 69
    iput-object p3, p0, Lcom/squareup/ms/Ms;->initializedCallback:Lcom/squareup/ms/Ms$InitializedCallback;

    .line 70
    sget-object p2, Lcom/squareup/ms/Ms$State;->STARTING:Lcom/squareup/ms/Ms$State;

    iput-object p2, p0, Lcom/squareup/ms/Ms;->initialized:Lcom/squareup/ms/Ms$State;

    .line 71
    invoke-virtual {p1}, Landroid/app/Application;->getFilesDir()Ljava/io/File;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ms/Ms;->internalFilesDir:Ljava/io/File;

    .line 73
    iget-object p2, p0, Lcom/squareup/ms/Ms;->minesweeperLogger:Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

    const-string p3, "Initializing Minesweeper"

    invoke-interface {p2, p3}, Lcom/squareup/ms/Minesweeper$MinesweeperLogger;->logMinesweeperEvent(Ljava/lang/String;)V

    .line 74
    new-instance p2, Ljava/lang/Thread;

    new-instance p3, Lcom/squareup/ms/-$$Lambda$Ms$81K9C4MHZzLOvy6EkPonSOVaO-0;

    invoke-direct {p3, p0, p1}, Lcom/squareup/ms/-$$Lambda$Ms$81K9C4MHZzLOvy6EkPonSOVaO-0;-><init>(Lcom/squareup/ms/Ms;Landroid/app/Application;)V

    const-string p1, "Sq-ms"

    invoke-direct {p2, p3, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p2}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    monitor-exit p0

    return-void

    .line 67
    :cond_0
    :try_start_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot re-initialize Ms!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public isInitialized()Z
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/ms/Ms;->initialized:Lcom/squareup/ms/Ms$State;

    sget-object v1, Lcom/squareup/ms/Ms$State;->CALLED_BACK:Lcom/squareup/ms/Ms$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$initialize$0$Ms(Landroid/app/Application;)V
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/ms/Ms;->crashnado:Lcom/squareup/crashnado/Crashnado;

    invoke-interface {v0}, Lcom/squareup/crashnado/Crashnado;->prepareStack()V

    .line 78
    iget-object v0, p0, Lcom/squareup/ms/Ms;->minesweeperLogger:Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

    const-string v1, "Starting ms checks"

    invoke-interface {v0, v1}, Lcom/squareup/ms/Minesweeper$MinesweeperLogger;->logMinesweeperEvent(Ljava/lang/String;)V

    .line 79
    invoke-static {p0, p1}, Lcom/squareup/ms/Ms;->ms_init(Lcom/squareup/ms/Ms;Landroid/content/Context;)I

    return-void
.end method

.method public synthetic lambda$writeback$2$Ms([B)V
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/ms/Ms;->callback:Lcom/squareup/ms/Minesweeper$DataListener;

    invoke-interface {v0, p1, p0}, Lcom/squareup/ms/Minesweeper$DataListener;->passDataToServer([BLcom/squareup/ms/Minesweeper;)V

    return-void
.end method

.method public onPause()V
    .locals 4

    .line 98
    invoke-virtual {p0}, Lcom/squareup/ms/Ms;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 100
    :cond_0
    new-instance v0, Ljava/util/Timer;

    const/4 v1, 0x1

    const-string v2, "ms_assassin"

    invoke-direct {v0, v2, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/squareup/ms/Ms;->timer:Ljava/util/Timer;

    .line 101
    iget-object v0, p0, Lcom/squareup/ms/Ms;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/squareup/ms/Ms$PauseTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/squareup/ms/Ms$PauseTask;-><init>(Lcom/squareup/ms/Ms;Lcom/squareup/ms/Ms$1;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void
.end method

.method public onResume()V
    .locals 2

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ms/Ms;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/squareup/ms/Ms;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 91
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/squareup/ms/Ms;->executor:Ljava/util/concurrent/ExecutorService;

    sget-object v1, Lcom/squareup/ms/-$$Lambda$Ms$QdNR-XQp62xL17DmHsNpli_zAw0;->INSTANCE:Lcom/squareup/ms/-$$Lambda$Ms$QdNR-XQp62xL17DmHsNpli_zAw0;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public passDataToMinesweeper([B)V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/ms/Ms;->executor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/ms/-$$Lambda$Ms$7TFULEH5Q5lSBS9UWfnVVJZ3f9o;

    invoke-direct {v1, p1}, Lcom/squareup/ms/-$$Lambda$Ms$7TFULEH5Q5lSBS9UWfnVVJZ3f9o;-><init>([B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public removeStatusListener(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/ms/Ms;->msNativeMethodDelegate:Lcom/squareup/ms/Ms$MsNativeMethodDelegate;

    invoke-virtual {v0, p1}, Lcom/squareup/ms/Ms$MsNativeMethodDelegate;->ms_disabled_status_remove_listener(Lcom/squareup/ms/NativeAppFunctionStatusListener;)V

    return-void
.end method

.method public updateTicketAsync()V
    .locals 0

    .line 105
    invoke-static {}, Lcom/squareup/ms/Ms;->ms_update_ticket_async()V

    return-void
.end method

.method public writeFile(Ljava/lang/String;[BZ)Z
    .locals 3

    const/4 v0, 0x0

    .line 148
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/squareup/ms/Ms;->internalFilesDir:Ljava/io/File;

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 149
    new-instance p1, Ljava/io/FileOutputStream;

    invoke-direct {p1, v1, p3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 150
    :try_start_1
    invoke-virtual {p1, p2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    invoke-static {p1}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    const/4 p1, 0x1

    return p1

    :catchall_0
    move-exception p2

    move-object v0, p1

    goto :goto_1

    :catch_0
    move-object v0, p1

    goto :goto_0

    :catchall_1
    move-exception p2

    goto :goto_1

    .line 152
    :catch_1
    :goto_0
    :try_start_2
    iget-object p1, p0, Lcom/squareup/ms/Ms;->minesweeperLogger:Lcom/squareup/ms/Minesweeper$MinesweeperLogger;

    const-string p2, "failed to open file for minesweeper"

    invoke-interface {p1, p2}, Lcom/squareup/ms/Minesweeper$MinesweeperLogger;->logMinesweeperEvent(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 p1, 0x0

    .line 155
    invoke-static {v0}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    return p1

    :goto_1
    invoke-static {v0}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 156
    throw p2
.end method
