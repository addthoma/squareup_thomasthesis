.class public final Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$3$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardRowSpec.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$3;->invoke(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Integer;",
        "TS;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec$Creator$bind$1\n+ 2 LoyaltyReportRecyclerHelper.kt\ncom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper\n*L\n1#1,87:1\n73#2,4:88\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0004\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0005\u0010\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "<anonymous parameter 0>",
        "",
        "item",
        "invoke",
        "(ILjava/lang/Object;)V",
        "com/squareup/cycler/StandardRowSpec$Creator$bind$1",
        "com/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$bind$3"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $loyaltyReportAverageSpendLabel$inlined:Landroid/widget/TextView;

.field final synthetic $loyaltyReportAverageVisitsCountLabel$inlined:Landroid/widget/TextView;

.field final synthetic $loyaltyReportLoyaltyCustomersCountLabel$inlined:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$3$lambda$1;->$loyaltyReportLoyaltyCustomersCountLabel$inlined:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$3$lambda$1;->$loyaltyReportAverageVisitsCountLabel$inlined:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$3$lambda$1;->$loyaltyReportAverageSpendLabel$inlined:Landroid/widget/TextView;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$3$lambda$1;->invoke(ILjava/lang/Object;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITS;)V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p2, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$Summary;

    .line 88
    iget-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$3$lambda$1;->$loyaltyReportLoyaltyCustomersCountLabel$inlined:Landroid/widget/TextView;

    const-string v0, "loyaltyReportLoyaltyCustomersCountLabel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$Summary;->getLoyaltyCustomers()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$3$lambda$1;->$loyaltyReportAverageVisitsCountLabel$inlined:Landroid/widget/TextView;

    const-string v0, "loyaltyReportAverageVisitsCountLabel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$Summary;->getAverageVisits()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRecyclerHelper$$special$$inlined$create$3$lambda$1;->$loyaltyReportAverageSpendLabel$inlined:Landroid/widget/TextView;

    const-string v0, "loyaltyReportAverageSpendLabel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/loyaltyreport/ui/LoyaltyReportRow$Summary;->getAverageSpend()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
