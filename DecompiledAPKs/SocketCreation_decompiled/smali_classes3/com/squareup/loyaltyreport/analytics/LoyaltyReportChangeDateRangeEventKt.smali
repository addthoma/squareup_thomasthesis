.class public final Lcom/squareup/loyaltyreport/analytics/LoyaltyReportChangeDateRangeEventKt;
.super Ljava/lang/Object;
.source "LoyaltyReportChangeDateRangeEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "analyticsDetail",
        "",
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final analyticsDetail(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$analyticsDetail"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    sget-object v0, Lcom/squareup/loyaltyreport/analytics/LoyaltyReportChangeDateRangeEventKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-ne p0, v0, :cond_0

    const-string/jumbo p0, "this-month"

    goto :goto_0

    .line 22
    :cond_0
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    :cond_1
    const-string p0, "last-month"

    goto :goto_0

    :cond_2
    const-string/jumbo p0, "this-year"

    goto :goto_0

    :cond_3
    const-string p0, "last-year"

    :goto_0
    return-object p0
.end method
