.class final Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealLoyaltyReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->toLayeredScreen(Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/Sink;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$3;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 39
    invoke-virtual {p0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$3;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 186
    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$3;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    invoke-static {v0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->access$getBrowserLauncher$p(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;)Lcom/squareup/util/BrowserLauncher;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$3;->this$0:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;

    invoke-static {v1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->access$getRes$p(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/loyaltyreport/impl/R$string;->loyalty_report_view_dashboard_link:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    return-void
.end method
