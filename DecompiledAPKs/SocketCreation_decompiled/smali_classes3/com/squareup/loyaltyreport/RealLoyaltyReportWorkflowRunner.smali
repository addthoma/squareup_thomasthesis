.class public final Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "RealLoyaltyReportWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        ">;",
        "Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u00012\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\u0002B\'\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0011H\u0014J\u0015\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u0003H\u0016\u00a2\u0006\u0002\u0010\u0014R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;",
        "Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "",
        "viewFactory",
        "Lcom/squareup/loyaltyreport/LoyaltyReportViewFactory;",
        "workflow",
        "Lcom/squareup/loyaltyreport/LoyaltyReportWorkflow;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/loyaltyreport/LoyaltyReportViewFactory;Lcom/squareup/loyaltyreport/LoyaltyReportWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/analytics/Analytics;)V",
        "getWorkflow",
        "()Lcom/squareup/loyaltyreport/LoyaltyReportWorkflow;",
        "onEnterScope",
        "newScope",
        "Lmortar/MortarScope;",
        "startWorkflow",
        "loyaltyReportProps",
        "(Lkotlin/Unit;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final workflow:Lcom/squareup/loyaltyreport/LoyaltyReportWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/loyaltyreport/LoyaltyReportViewFactory;Lcom/squareup/loyaltyreport/LoyaltyReportWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/analytics/Analytics;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "viewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    sget-object v0, Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner;->Companion:Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner$Companion;

    invoke-virtual {v0}, Lcom/squareup/loyaltyreport/LoyaltyReportWorkflowRunner$Companion;->getNAME()Ljava/lang/String;

    move-result-object v2

    .line 22
    invoke-interface {p3}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 23
    move-object v4, p1

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 20
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;->workflow:Lcom/squareup/loyaltyreport/LoyaltyReportWorkflow;

    iput-object p3, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p4, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/loyaltyreport/LoyaltyReportWorkflow;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;->workflow:Lcom/squareup/loyaltyreport/LoyaltyReportWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;->getWorkflow()Lcom/squareup/loyaltyreport/LoyaltyReportWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 29
    invoke-virtual {p0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 32
    invoke-virtual {p0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 33
    new-instance v1, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public startWorkflow(Lkotlin/Unit;)V
    .locals 1

    const-string v0, "loyaltyReportProps"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 40
    iget-object p1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/loyaltyreport/analytics/LoyaltyReportViewEvent;->INSTANCE:Lcom/squareup/loyaltyreport/analytics/LoyaltyReportViewEvent;

    check-cast v0, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 41
    invoke-virtual {p0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
