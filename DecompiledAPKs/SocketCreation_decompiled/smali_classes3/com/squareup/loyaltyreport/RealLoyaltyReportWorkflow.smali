.class public final Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealLoyaltyReportWorkflow.kt"

# interfaces
.implements Lcom/squareup/loyaltyreport/LoyaltyReportWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
        "Lkotlin/Unit;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/loyaltyreport/LoyaltyReportWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealLoyaltyReportWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealLoyaltyReportWorkflow.kt\ncom/squareup/loyaltyreport/RealLoyaltyReportWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 6 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,214:1\n32#2,12:215\n85#3:227\n240#4:228\n276#5:229\n149#6,5:230\n149#6,5:235\n*E\n*S KotlinDebug\n*F\n+ 1 RealLoyaltyReportWorkflow.kt\ncom/squareup/loyaltyreport/RealLoyaltyReportWorkflow\n*L\n56#1,12:215\n72#1:227\n72#1:228\n72#1:229\n163#1,5:230\n189#1,5:235\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 52\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0002:\u00015B7\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016J\u001f\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u00032\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016\u00a2\u0006\u0002\u0010\u001dJ\u001c\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u00182\u0006\u0010\u001f\u001a\u00020 H\u0002J\"\u0010!\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u00180\"2\u0006\u0010#\u001a\u00020 H\u0002JS\u0010$\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u001a\u001a\u00020\u00032\u0006\u0010%\u001a\u00020\u00042\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\'H\u0016\u00a2\u0006\u0002\u0010(JF\u0010)\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0012\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\'2\u0006\u0010%\u001a\u00020\u0004H\u0002JL\u0010*\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010%\u001a\u00020\u00042\u0018\u0010+\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u00180,H\u0002JL\u0010-\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010%\u001a\u00020\u00042\u0018\u0010+\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u00180,H\u0002JT\u0010.\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010/\u001a\u0002002\u0006\u0010%\u001a\u00020\u00042\u0018\u0010+\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u00180,H\u0002J$\u00101\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u00182\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010/\u001a\u000200H\u0002J\u0010\u00102\u001a\u00020\u001c2\u0006\u0010%\u001a\u00020\u0004H\u0016JP\u00103\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t*\u0002042\u0006\u0010%\u001a\u00020\u00042\u0018\u0010+\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u00180,H\u0002R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00030\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;",
        "Lcom/squareup/loyaltyreport/LoyaltyReportWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "loyaltyReportDateRangeService",
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;",
        "loyaltyServiceHelper",
        "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
        "loyaltyReportTransformer",
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;",
        "dateRangeSelectorWorkflow",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;)V",
        "exitAction",
        "Lcom/squareup/workflow/WorkflowAction;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/loyaltyreport/LoyaltyReportState;",
        "loadAction",
        "selectedDateRange",
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
        "loadingAction",
        "Lio/reactivex/Single;",
        "dateRange",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "renderDateRangeSelectorChildWorkflow",
        "renderError",
        "sink",
        "Lcom/squareup/workflow/Sink;",
        "renderLoading",
        "renderLoyaltyReport",
        "loyaltyReport",
        "Lcom/squareup/loyaltyreport/ui/LoyaltyReport;",
        "renderReportAction",
        "snapshotState",
        "toLayeredScreen",
        "Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$Companion;

.field private static final INITIAL_DATE_RANGE:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;


# instance fields
.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final dateRangeSelectorWorkflow:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;

.field private final exitAction:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyReportDateRangeService:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;

.field private final loyaltyReportTransformer:Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;

.field private final loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->Companion:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$Companion;

    .line 211
    sget-object v0, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;->THIS_MONTH:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    sput-object v0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->INITIAL_DATE_RANGE:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "loyaltyReportDateRangeService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyServiceHelper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyReportTransformer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateRangeSelectorWorkflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->loyaltyReportDateRangeService:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;

    iput-object p2, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iput-object p3, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->loyaltyReportTransformer:Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;

    iput-object p4, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->dateRangeSelectorWorkflow:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;

    iput-object p5, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p6, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->res:Lcom/squareup/util/Res;

    .line 193
    sget-object p1, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$exitAction$1;->INSTANCE:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$exitAction$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    const/4 p2, 0x0

    const/4 p3, 0x1

    invoke-static {p0, p2, p1, p3, p2}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->exitAction:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public static final synthetic access$getBrowserLauncher$p(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;)Lcom/squareup/util/BrowserLauncher;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-object p0
.end method

.method public static final synthetic access$getExitAction$p(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->exitAction:Lcom/squareup/workflow/WorkflowAction;

    return-object p0
.end method

.method public static final synthetic access$getINITIAL_DATE_RANGE$cp()Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->INITIAL_DATE_RANGE:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    return-object v0
.end method

.method public static final synthetic access$getLoyaltyReportDateRangeService$p(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;)Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->loyaltyReportDateRangeService:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;

    return-object p0
.end method

.method public static final synthetic access$getLoyaltyReportTransformer$p(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;)Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->loyaltyReportTransformer:Lcom/squareup/loyaltyreport/model/LoyaltyReportTransformer;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;)Lcom/squareup/util/Res;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$loadAction(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->loadAction(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$renderReportAction(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;Lcom/squareup/loyaltyreport/ui/LoyaltyReport;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->renderReportAction(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;Lcom/squareup/loyaltyreport/ui/LoyaltyReport;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method private final loadAction(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 199
    new-instance v0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadAction$1;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadAction$1;-><init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final loadingAction(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->loyaltyServiceHelper:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    .line 98
    iget-object v1, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->loyaltyReportDateRangeService:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;

    invoke-virtual {v1, p1}, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRangeService;->dateRangeNow(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/protos/common/time/DateTimeInterval;

    move-result-object v1

    .line 97
    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->getLoyaltyReport(Lcom/squareup/protos/common/time/DateTimeInterval;)Lio/reactivex/Single;

    move-result-object v0

    .line 100
    new-instance v1, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$loadingAction$1;-><init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "loyaltyServiceHelper\n   \u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final renderDateRangeSelectorChildWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltyreport/LoyaltyReportState;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "-",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 140
    iget-object v0, p0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->dateRangeSelectorWorkflow:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 141
    new-instance v3, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;

    invoke-virtual {p2}, Lcom/squareup/loyaltyreport/LoyaltyReportState;->getSelectedDateRange()Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;-><init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V

    .line 142
    new-instance v0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderDateRangeSelectorChildWorkflow$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderDateRangeSelectorChildWorkflow$1;-><init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/loyaltyreport/LoyaltyReportState;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p1

    .line 139
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1
.end method

.method private final renderError(Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/Sink;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lkotlin/Unit;",
            ">;>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 159
    new-instance v0, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorScreen;

    .line 160
    new-instance v1, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderError$1;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderError$1;-><init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/loyaltyreport/LoyaltyReportState;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 161
    new-instance p1, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderError$2;

    invoke-direct {p1, p0, p2}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderError$2;-><init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    .line 159
    invoke-direct {v0, v1, p1}, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorScreen;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 231
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 232
    const-class p2, Lcom/squareup/loyaltyreport/error/LoyaltyReportErrorScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 233
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 231
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 164
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final renderLoading(Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/Sink;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lkotlin/Unit;",
            ">;>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 124
    sget-object v0, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content$Loading;->INSTANCE:Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content$Loading;

    check-cast v0, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;

    invoke-direct {p0, v0, p1, p2}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->toLayeredScreen(Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/Sink;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final renderLoyaltyReport(Lcom/squareup/loyaltyreport/ui/LoyaltyReport;Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/Sink;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/ui/LoyaltyReport;",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lkotlin/Unit;",
            ">;>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 132
    new-instance v0, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content$Loaded;

    invoke-direct {v0, p1}, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content$Loaded;-><init>(Lcom/squareup/loyaltyreport/ui/LoyaltyReport;)V

    check-cast v0, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;

    invoke-direct {p0, v0, p2, p3}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->toLayeredScreen(Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/Sink;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method private final renderReportAction(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;Lcom/squareup/loyaltyreport/ui/LoyaltyReport;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
            "Lcom/squareup/loyaltyreport/ui/LoyaltyReport;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 206
    new-instance v0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderReportAction$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$renderReportAction$1;-><init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;Lcom/squareup/loyaltyreport/ui/LoyaltyReport;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    const/4 p2, 0x1

    invoke-static {p0, p1, v0, p2, p1}, Lcom/squareup/workflow/StatefulWorkflowKt;->action$default(Lcom/squareup/workflow/StatefulWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method private final toLayeredScreen(Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/Sink;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lcom/squareup/workflow/Sink<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lkotlin/Unit;",
            ">;>;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 172
    instance-of v0, p1, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content$Loaded;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content$Loaded;

    invoke-virtual {v0}, Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content$Loaded;->getLoyaltyReport()Lcom/squareup/loyaltyreport/ui/LoyaltyReport;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 175
    :goto_0
    new-instance v1, Lcom/squareup/loyaltyreport/LoyaltyReportScreen;

    .line 177
    new-instance v2, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$1;

    invoke-direct {v2, p0, p3}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$1;-><init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 178
    new-instance v3, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2;

    invoke-direct {v3, p0, p3, p2, v0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$2;-><init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;Lcom/squareup/workflow/Sink;Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/loyaltyreport/ui/LoyaltyReport;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 185
    new-instance p2, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$3;

    invoke-direct {p2, p0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$toLayeredScreen$3;-><init>(Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    .line 175
    invoke-direct {v1, p1, v2, v3, p2}, Lcom/squareup/loyaltyreport/LoyaltyReportScreen;-><init>(Lcom/squareup/loyaltyreport/LoyaltyReportScreen$Content;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Lcom/squareup/workflow/legacy/V2Screen;

    .line 236
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 237
    const-class p2, Lcom/squareup/loyaltyreport/LoyaltyReportScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string p3, ""

    invoke-static {p2, p3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 238
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    .line 236
    invoke-direct {p1, p2, v1, p3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 190
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/loyaltyreport/LoyaltyReportState;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 215
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 220
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 222
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 223
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 224
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 226
    :cond_3
    check-cast v1, Lcom/squareup/loyaltyreport/LoyaltyReportState;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 56
    :cond_4
    new-instance p1, Lcom/squareup/loyaltyreport/LoyaltyReportState$Loading;

    .line 57
    sget-object p2, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->INITIAL_DATE_RANGE:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    .line 56
    invoke-direct {p1, p2}, Lcom/squareup/loyaltyreport/LoyaltyReportState$Loading;-><init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/loyaltyreport/LoyaltyReportState;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/loyaltyreport/LoyaltyReportState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/loyaltyreport/LoyaltyReportState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->render(Lkotlin/Unit;Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/loyaltyreport/LoyaltyReportState;",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "state"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    .line 70
    instance-of v0, p2, Lcom/squareup/loyaltyreport/LoyaltyReportState$Loading;

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p2}, Lcom/squareup/loyaltyreport/LoyaltyReportState;->getSelectedDateRange()Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->loadingAction(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lio/reactivex/Single;

    move-result-object v0

    .line 227
    sget-object v1, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v1, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$render$$inlined$asWorker$1;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 228
    invoke-static {v1}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 229
    const-class v1, Lcom/squareup/workflow/WorkflowAction;

    sget-object v2, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v3, Lcom/squareup/loyaltyreport/LoyaltyReportState;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v2

    sget-object v3, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v4, Lkotlin/Unit;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v1

    new-instance v2, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v2, v1, v0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 73
    sget-object v0, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$render$1;->INSTANCE:Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow$render$1;

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    .line 71
    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 75
    invoke-direct {p0, p2, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->renderLoading(Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/Sink;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 77
    :cond_0
    instance-of v0, p2, Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderReport;

    if-eqz v0, :cond_1

    .line 79
    move-object p3, p2

    check-cast p3, Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderReport;

    invoke-virtual {p3}, Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderReport;->getLoyaltyReport()Lcom/squareup/loyaltyreport/ui/LoyaltyReport;

    move-result-object p3

    .line 78
    invoke-direct {p0, p3, p2, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->renderLoyaltyReport(Lcom/squareup/loyaltyreport/ui/LoyaltyReport;Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/Sink;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 84
    :cond_1
    instance-of v0, p2, Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderDateSelector;

    if-eqz v0, :cond_2

    .line 85
    invoke-direct {p0, p3, p2}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->renderDateRangeSelectorChildWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/loyaltyreport/LoyaltyReportState;)Ljava/util/Map;

    move-result-object p1

    goto :goto_0

    .line 87
    :cond_2
    instance-of p3, p2, Lcom/squareup/loyaltyreport/LoyaltyReportState$RenderError;

    if-eqz p3, :cond_3

    .line 88
    invoke-direct {p0, p2, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->renderError(Lcom/squareup/loyaltyreport/LoyaltyReportState;Lcom/squareup/workflow/Sink;)Ljava/util/Map;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/loyaltyreport/LoyaltyReportState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/loyaltyreport/LoyaltyReportState;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltyreport/RealLoyaltyReportWorkflow;->snapshotState(Lcom/squareup/loyaltyreport/LoyaltyReportState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
