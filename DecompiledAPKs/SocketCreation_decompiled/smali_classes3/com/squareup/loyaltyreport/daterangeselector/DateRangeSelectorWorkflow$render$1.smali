.class final Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DateRangeSelectorWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;->render(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorProps;Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "selectedRange",
        "Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$render$1;->this$0:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;

    iput-object p2, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$render$1;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$render$1;->invoke(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V
    .locals 3

    const-string v0, "selectedRange"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$render$1;->this$0:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;

    invoke-static {v0}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;->access$getAnalytics$p(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/loyaltyreport/analytics/LoyaltyReportChangeDateRangeEvent;

    invoke-static {p1}, Lcom/squareup/loyaltyreport/analytics/LoyaltyReportChangeDateRangeEventKt;->analyticsDetail(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/loyaltyreport/analytics/LoyaltyReportChangeDateRangeEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$render$1;->$sink:Lcom/squareup/workflow/Sink;

    iget-object v1, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$render$1;->this$0:Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;

    invoke-static {v1, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;->access$exitWithSelection(Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
