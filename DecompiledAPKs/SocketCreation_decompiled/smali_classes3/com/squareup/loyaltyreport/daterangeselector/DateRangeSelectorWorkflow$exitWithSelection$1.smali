.class final Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$exitWithSelection$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DateRangeSelectorWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow;->exitWithSelection(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Mutator<",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;",
        ">;",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $selectedDateRange:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;


# direct methods
.method constructor <init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$exitWithSelection$1;->$selectedDateRange:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorState;",
            ">;)",
            "Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance p1, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;

    iget-object v0, p0, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$exitWithSelection$1;->$selectedDateRange:Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;

    invoke-direct {p1, v0}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;-><init>(Lcom/squareup/loyaltyreport/model/LoyaltyReportDateRange;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Mutator;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorWorkflow$exitWithSelection$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/loyaltyreport/daterangeselector/DateRangeSelectorOutput;

    move-result-object p1

    return-object p1
.end method
