.class public final enum Lcom/squareup/connectivity/InternetState;
.super Ljava/lang/Enum;
.source "InternetState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/connectivity/InternetState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/connectivity/InternetState;

.field public static final enum CONNECTED:Lcom/squareup/connectivity/InternetState;

.field public static final enum NOT_CONNECTED:Lcom/squareup/connectivity/InternetState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 9
    new-instance v0, Lcom/squareup/connectivity/InternetState;

    const/4 v1, 0x0

    const-string v2, "CONNECTED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/connectivity/InternetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    new-instance v0, Lcom/squareup/connectivity/InternetState;

    const/4 v2, 0x1

    const-string v3, "NOT_CONNECTED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/connectivity/InternetState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/connectivity/InternetState;->NOT_CONNECTED:Lcom/squareup/connectivity/InternetState;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/connectivity/InternetState;

    .line 8
    sget-object v3, Lcom/squareup/connectivity/InternetState;->CONNECTED:Lcom/squareup/connectivity/InternetState;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/connectivity/InternetState;->NOT_CONNECTED:Lcom/squareup/connectivity/InternetState;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/connectivity/InternetState;->$VALUES:[Lcom/squareup/connectivity/InternetState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/connectivity/InternetState;
    .locals 1

    .line 8
    const-class v0, Lcom/squareup/connectivity/InternetState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/connectivity/InternetState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/connectivity/InternetState;
    .locals 1

    .line 8
    sget-object v0, Lcom/squareup/connectivity/InternetState;->$VALUES:[Lcom/squareup/connectivity/InternetState;

    invoke-virtual {v0}, [Lcom/squareup/connectivity/InternetState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/connectivity/InternetState;

    return-object v0
.end method
