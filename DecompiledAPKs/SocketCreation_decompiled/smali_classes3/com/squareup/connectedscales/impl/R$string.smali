.class public final Lcom/squareup/connectedscales/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/connectedscales/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final connect_scale_using_usb_footer:I = 0x7f12048a

.field public static final connect_scale_using_usb_to_continue_label:I = 0x7f12048b

.field public static final connected_scales_limit_reached_warning_button_title:I = 0x7f12048c

.field public static final connected_scales_limit_reached_warning_message:I = 0x7f12048d

.field public static final connected_scales_limit_reached_warning_title:I = 0x7f12048e

.field public static final connection_type_and_weight_unit_pattern:I = 0x7f120493

.field public static final connection_type_bluetooth:I = 0x7f120494

.field public static final connection_type_usb:I = 0x7f120495

.field public static final scales_action_bar_title:I = 0x7f121779

.field public static final scales_header_title_uppercase:I = 0x7f12177a

.field public static final weight_unit_abbreviation_gram_lowercase:I = 0x7f121bda

.field public static final weight_unit_abbreviation_kilogram_lowercase:I = 0x7f121bdb

.field public static final weight_unit_abbreviation_milligram_lowercase:I = 0x7f121bdc

.field public static final weight_unit_abbreviation_ounce_lowercase:I = 0x7f121bdd

.field public static final weight_unit_abbreviation_pound_lowercase:I = 0x7f121bde


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
