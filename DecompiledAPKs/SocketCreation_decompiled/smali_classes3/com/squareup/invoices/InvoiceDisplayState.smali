.class public final enum Lcom/squareup/invoices/InvoiceDisplayState;
.super Ljava/lang/Enum;
.source "InvoiceDisplayState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/InvoiceDisplayState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/InvoiceDisplayState;

.field public static final enum CANCELED:Lcom/squareup/invoices/InvoiceDisplayState;

.field public static final enum DRAFT:Lcom/squareup/invoices/InvoiceDisplayState;

.field public static final enum FAILED:Lcom/squareup/invoices/InvoiceDisplayState;

.field public static final enum OVERDUE:Lcom/squareup/invoices/InvoiceDisplayState;

.field public static final enum PAID:Lcom/squareup/invoices/InvoiceDisplayState;

.field public static final enum PARTIALLY_PAID:Lcom/squareup/invoices/InvoiceDisplayState;

.field public static final enum RECURRING:Lcom/squareup/invoices/InvoiceDisplayState;

.field public static final enum REFUNDED:Lcom/squareup/invoices/InvoiceDisplayState;

.field public static final enum SCHEDULED:Lcom/squareup/invoices/InvoiceDisplayState;

.field public static final enum UNDELIVERED:Lcom/squareup/invoices/InvoiceDisplayState;

.field public static final enum UNKNOWN:Lcom/squareup/invoices/InvoiceDisplayState;

.field public static final enum UNPAID:Lcom/squareup/invoices/InvoiceDisplayState;


# instance fields
.field private final colorId:I

.field private final displayState:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field private final isSentOrShared:Z

.field private final isUnpaid:Z

.field private nullStateId:I

.field private titleId:I


# direct methods
.method static constructor <clinit>()V
    .locals 17

    .line 17
    new-instance v8, Lcom/squareup/invoices/InvoiceDisplayState;

    sget v3, Lcom/squareup/common/invoices/R$string;->invoice_display_state_unpaid:I

    sget-object v7, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNPAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const-string v1, "UNPAID"

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/InvoiceDisplayState;-><init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V

    sput-object v8, Lcom/squareup/invoices/InvoiceDisplayState;->UNPAID:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 19
    new-instance v0, Lcom/squareup/invoices/InvoiceDisplayState;

    sget v12, Lcom/squareup/common/invoices/R$string;->invoice_display_state_overdue:I

    sget v13, Lcom/squareup/marin/R$color;->marin_red:I

    sget-object v16, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->OVERDUE:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const-string v10, "OVERDUE"

    const/4 v11, 0x1

    const/4 v14, 0x1

    const/4 v15, 0x1

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/invoices/InvoiceDisplayState;-><init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->OVERDUE:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 21
    new-instance v0, Lcom/squareup/invoices/InvoiceDisplayState;

    sget v4, Lcom/squareup/common/invoices/R$string;->invoice_display_state_paid:I

    sget v5, Lcom/squareup/marin/R$color;->marin_green:I

    sget-object v8, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->PAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const-string v2, "PAID"

    const/4 v3, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/invoices/InvoiceDisplayState;-><init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->PAID:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 23
    new-instance v0, Lcom/squareup/invoices/InvoiceDisplayState;

    sget v12, Lcom/squareup/common/invoices/R$string;->invoice_display_state_draft:I

    sget v13, Lcom/squareup/marin/R$color;->marin_blue:I

    sget-object v16, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->DRAFT:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const-string v10, "DRAFT"

    const/4 v11, 0x3

    const/4 v15, 0x0

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/invoices/InvoiceDisplayState;-><init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->DRAFT:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 25
    new-instance v0, Lcom/squareup/invoices/InvoiceDisplayState;

    sget v4, Lcom/squareup/common/invoices/R$string;->invoice_display_state_scheduled:I

    sget v5, Lcom/squareup/marin/R$color;->marin_blue:I

    sget-object v8, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->SCHEDULED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const-string v2, "SCHEDULED"

    const/4 v3, 0x4

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/invoices/InvoiceDisplayState;-><init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->SCHEDULED:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 28
    new-instance v0, Lcom/squareup/invoices/InvoiceDisplayState;

    sget v12, Lcom/squareup/common/invoices/R$string;->invoice_display_state_recurring:I

    sget v13, Lcom/squareup/marin/R$color;->marin_blue:I

    sget-object v16, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->RECURRING:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const-string v10, "RECURRING"

    const/4 v11, 0x5

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/invoices/InvoiceDisplayState;-><init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->RECURRING:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 31
    new-instance v0, Lcom/squareup/invoices/InvoiceDisplayState;

    sget v4, Lcom/squareup/common/invoices/R$string;->invoice_display_state_refunded:I

    sget-object v8, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->REFUNDED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const-string v2, "REFUNDED"

    const/4 v3, 0x6

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/invoices/InvoiceDisplayState;-><init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->REFUNDED:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 33
    new-instance v0, Lcom/squareup/invoices/InvoiceDisplayState;

    sget v12, Lcom/squareup/common/invoices/R$string;->invoice_display_state_failed:I

    sget v13, Lcom/squareup/marin/R$color;->marin_red:I

    sget-object v16, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->FAILED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const-string v10, "FAILED"

    const/4 v11, 0x7

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/invoices/InvoiceDisplayState;-><init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->FAILED:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 35
    new-instance v0, Lcom/squareup/invoices/InvoiceDisplayState;

    sget v4, Lcom/squareup/common/invoices/R$string;->invoice_display_state_undelivered:I

    sget v5, Lcom/squareup/marin/R$color;->marin_red:I

    sget-object v8, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNDELIVERED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const-string v2, "UNDELIVERED"

    const/16 v3, 0x8

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/invoices/InvoiceDisplayState;-><init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->UNDELIVERED:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 38
    new-instance v0, Lcom/squareup/invoices/InvoiceDisplayState;

    sget v12, Lcom/squareup/common/invoices/R$string;->invoice_display_state_canceled:I

    sget-object v16, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->CANCELLED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const-string v10, "CANCELED"

    const/16 v11, 0x9

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x1

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/invoices/InvoiceDisplayState;-><init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->CANCELED:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 40
    new-instance v0, Lcom/squareup/invoices/InvoiceDisplayState;

    sget v4, Lcom/squareup/common/invoices/R$string;->invoice_display_state_unknown:I

    sget-object v8, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->UNKNOWN:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const-string v2, "UNKNOWN"

    const/16 v3, 0xa

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/invoices/InvoiceDisplayState;-><init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->UNKNOWN:Lcom/squareup/invoices/InvoiceDisplayState;

    .line 41
    new-instance v0, Lcom/squareup/invoices/InvoiceDisplayState;

    sget v12, Lcom/squareup/common/invoices/R$string;->invoice_display_state_partially_paid:I

    sget-object v16, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->PARTIALLY_PAID:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    const-string v10, "PARTIALLY_PAID"

    const/16 v11, 0xb

    move-object v9, v0

    invoke-direct/range {v9 .. v16}, Lcom/squareup/invoices/InvoiceDisplayState;-><init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V

    sput-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->PARTIALLY_PAID:Lcom/squareup/invoices/InvoiceDisplayState;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/squareup/invoices/InvoiceDisplayState;

    .line 16
    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->UNPAID:Lcom/squareup/invoices/InvoiceDisplayState;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->OVERDUE:Lcom/squareup/invoices/InvoiceDisplayState;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->PAID:Lcom/squareup/invoices/InvoiceDisplayState;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->DRAFT:Lcom/squareup/invoices/InvoiceDisplayState;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->SCHEDULED:Lcom/squareup/invoices/InvoiceDisplayState;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->RECURRING:Lcom/squareup/invoices/InvoiceDisplayState;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->REFUNDED:Lcom/squareup/invoices/InvoiceDisplayState;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->FAILED:Lcom/squareup/invoices/InvoiceDisplayState;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->UNDELIVERED:Lcom/squareup/invoices/InvoiceDisplayState;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->CANCELED:Lcom/squareup/invoices/InvoiceDisplayState;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->UNKNOWN:Lcom/squareup/invoices/InvoiceDisplayState;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/InvoiceDisplayState;->PARTIALLY_PAID:Lcom/squareup/invoices/InvoiceDisplayState;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->$VALUES:[Lcom/squareup/invoices/InvoiceDisplayState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIZZLcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZZ",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;",
            ")V"
        }
    .end annotation

    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    iput p3, p0, Lcom/squareup/invoices/InvoiceDisplayState;->titleId:I

    .line 78
    iput-boolean p6, p0, Lcom/squareup/invoices/InvoiceDisplayState;->isSentOrShared:Z

    .line 79
    iput-object p7, p0, Lcom/squareup/invoices/InvoiceDisplayState;->displayState:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    if-nez p4, :cond_0

    .line 81
    sget p4, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    :cond_0
    iput p4, p0, Lcom/squareup/invoices/InvoiceDisplayState;->colorId:I

    .line 82
    iput-boolean p5, p0, Lcom/squareup/invoices/InvoiceDisplayState;->isUnpaid:Z

    return-void
.end method

.method public static forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;
    .locals 1

    .line 86
    sget-object v0, Lcom/squareup/invoices/InvoiceDisplayState$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceDisplayDetails$DisplayState:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->ordinal()I

    move-result p0

    aget p0, v0, p0

    packed-switch p0, :pswitch_data_0

    .line 110
    sget-object p0, Lcom/squareup/invoices/InvoiceDisplayState;->UNKNOWN:Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0

    .line 108
    :pswitch_0
    sget-object p0, Lcom/squareup/invoices/InvoiceDisplayState;->PARTIALLY_PAID:Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0

    .line 106
    :pswitch_1
    sget-object p0, Lcom/squareup/invoices/InvoiceDisplayState;->CANCELED:Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0

    .line 104
    :pswitch_2
    sget-object p0, Lcom/squareup/invoices/InvoiceDisplayState;->UNDELIVERED:Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0

    .line 102
    :pswitch_3
    sget-object p0, Lcom/squareup/invoices/InvoiceDisplayState;->FAILED:Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0

    .line 100
    :pswitch_4
    sget-object p0, Lcom/squareup/invoices/InvoiceDisplayState;->REFUNDED:Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0

    .line 98
    :pswitch_5
    sget-object p0, Lcom/squareup/invoices/InvoiceDisplayState;->RECURRING:Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0

    .line 96
    :pswitch_6
    sget-object p0, Lcom/squareup/invoices/InvoiceDisplayState;->SCHEDULED:Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0

    .line 94
    :pswitch_7
    sget-object p0, Lcom/squareup/invoices/InvoiceDisplayState;->DRAFT:Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0

    .line 92
    :pswitch_8
    sget-object p0, Lcom/squareup/invoices/InvoiceDisplayState;->PAID:Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0

    .line 90
    :pswitch_9
    sget-object p0, Lcom/squareup/invoices/InvoiceDisplayState;->OVERDUE:Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0

    .line 88
    :pswitch_a
    sget-object p0, Lcom/squareup/invoices/InvoiceDisplayState;->UNPAID:Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/InvoiceDisplayState;
    .locals 1

    .line 16
    const-class v0, Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/InvoiceDisplayState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/InvoiceDisplayState;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->$VALUES:[Lcom/squareup/invoices/InvoiceDisplayState;

    invoke-virtual {v0}, [Lcom/squareup/invoices/InvoiceDisplayState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/InvoiceDisplayState;

    return-object v0
.end method


# virtual methods
.method public getDisplayColor()I
    .locals 1

    .line 60
    iget v0, p0, Lcom/squareup/invoices/InvoiceDisplayState;->colorId:I

    return v0
.end method

.method public getDisplayColor(Landroid/content/res/Resources;)I
    .locals 1

    .line 64
    iget v0, p0, Lcom/squareup/invoices/InvoiceDisplayState;->colorId:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    return p1
.end method

.method public getTitle()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/squareup/invoices/InvoiceDisplayState;->titleId:I

    return v0
.end method

.method public getTitle(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    .line 52
    iget v0, p0, Lcom/squareup/invoices/InvoiceDisplayState;->titleId:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public isSentOrShared()Z
    .locals 1

    .line 72
    iget-boolean v0, p0, Lcom/squareup/invoices/InvoiceDisplayState;->isSentOrShared:Z

    return v0
.end method

.method public isUnpaid()Z
    .locals 1

    .line 68
    iget-boolean v0, p0, Lcom/squareup/invoices/InvoiceDisplayState;->isUnpaid:Z

    return v0
.end method
