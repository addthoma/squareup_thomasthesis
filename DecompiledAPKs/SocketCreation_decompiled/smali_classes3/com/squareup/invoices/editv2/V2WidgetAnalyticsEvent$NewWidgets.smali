.class public final Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets;
.super Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent;
.source "EditInvoiceScopeRunnerV2.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NewWidgets"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets;",
        "Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent;",
        "()V",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1746
    new-instance v0, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets;

    invoke-direct {v0}, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets;-><init>()V

    sput-object v0, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets;->INSTANCE:Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .line 1747
    sget-object v0, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets$1;->INSTANCE:Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent$NewWidgets$1;

    check-cast v0, Lcom/squareup/analytics/EventNamedAction;

    const/4 v1, 0x0

    .line 1746
    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/editv2/V2WidgetAnalyticsEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
