.class final Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1;
.super Ljava/lang/Object;
.source "EditInvoiceInstrumentsStore.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->instruments(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0003 \u0004*\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "kotlin.jvm.PlatformType",
        "previousInstrumentsForContact",
        "Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $contactToken:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1;->this$0:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1;->$contactToken:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;"
        }
    .end annotation

    const-string v0, "previousInstrumentsForContact"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1;->$contactToken:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 55
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(emptyList())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 56
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;->getContactToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;->getInstrumentsList()Ljava/util/List;

    move-result-object p1

    .line 56
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(\n           \u2026rumentsList\n            )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1;->this$0:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;->access$getRolodex$p(Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore;)Lcom/squareup/crm/RolodexServiceHelper;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1;->$contactToken:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/crm/RolodexServiceHelper;->getContact(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 60
    sget-object v0, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1$1;->INSTANCE:Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "rolodex.getContact(conta\u2026      }\n                }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/instruments/EditInvoiceInstrumentsStore$instruments$1;->apply(Lcom/squareup/invoices/editv2/instruments/InstrumentsForContact;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
