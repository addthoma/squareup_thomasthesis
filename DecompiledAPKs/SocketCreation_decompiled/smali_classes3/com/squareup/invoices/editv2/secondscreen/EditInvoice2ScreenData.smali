.class public final Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;
.super Ljava/lang/Object;
.source "EditInvoice2ScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;,
        Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0014\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0002!\"B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c6\u0003J\u000f\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\nH\u00c6\u0003J\t\u0010\u001a\u001a\u00020\nH\u00c6\u0003JA\u0010\u001b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\nH\u00c6\u0001J\u0013\u0010\u001c\u001a\u00020\n2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\t\u0010 \u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0014\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;",
        "",
        "actionBarData",
        "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;",
        "bottomButtonText",
        "",
        "invoiceSectionDataList",
        "",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
        "showOverflowMenuButton",
        "",
        "showProgress",
        "(Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;Ljava/lang/String;Ljava/util/List;ZZ)V",
        "getActionBarData",
        "()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;",
        "getBottomButtonText",
        "()Ljava/lang/String;",
        "getInvoiceSectionDataList",
        "()Ljava/util/List;",
        "getShowOverflowMenuButton",
        "()Z",
        "getShowProgress",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "EventKey",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarData:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

.field private final bottomButtonText:Ljava/lang/String;

.field private final invoiceSectionDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
            ">;"
        }
    .end annotation
.end field

.field private final showOverflowMenuButton:Z

.field private final showProgress:Z


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;Ljava/lang/String;Ljava/util/List;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
            ">;ZZ)V"
        }
    .end annotation

    const-string v0, "actionBarData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomButtonText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceSectionDataList"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->actionBarData:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->bottomButtonText:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->invoiceSectionDataList:Ljava/util/List;

    iput-boolean p4, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showOverflowMenuButton:Z

    iput-boolean p5, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showProgress:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;Ljava/lang/String;Ljava/util/List;ZZILjava/lang/Object;)Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->actionBarData:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->bottomButtonText:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->invoiceSectionDataList:Ljava/util/List;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showOverflowMenuButton:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showProgress:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->copy(Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;Ljava/lang/String;Ljava/util/List;ZZ)Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->actionBarData:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->bottomButtonText:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->invoiceSectionDataList:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showOverflowMenuButton:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showProgress:Z

    return v0
.end method

.method public final copy(Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;Ljava/lang/String;Ljava/util/List;ZZ)Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
            ">;ZZ)",
            "Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;"
        }
    .end annotation

    const-string v0, "actionBarData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomButtonText"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceSectionDataList"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;-><init>(Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;Ljava/lang/String;Ljava/util/List;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->actionBarData:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    iget-object v1, p1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->actionBarData:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->bottomButtonText:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->bottomButtonText:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->invoiceSectionDataList:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->invoiceSectionDataList:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showOverflowMenuButton:Z

    iget-boolean v1, p1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showOverflowMenuButton:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showProgress:Z

    iget-boolean p1, p1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showProgress:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActionBarData()Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->actionBarData:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    return-object v0
.end method

.method public final getBottomButtonText()Ljava/lang/String;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->bottomButtonText:Ljava/lang/String;

    return-object v0
.end method

.method public final getInvoiceSectionDataList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
            ">;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->invoiceSectionDataList:Ljava/util/List;

    return-object v0
.end method

.method public final getShowOverflowMenuButton()Z
    .locals 1

    .line 58
    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showOverflowMenuButton:Z

    return v0
.end method

.method public final getShowProgress()Z
    .locals 1

    .line 59
    iget-boolean v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showProgress:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->actionBarData:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->bottomButtonText:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->invoiceSectionDataList:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showOverflowMenuButton:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showProgress:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditInvoice2ScreenData(actionBarData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->actionBarData:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", bottomButtonText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->bottomButtonText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", invoiceSectionDataList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->invoiceSectionDataList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showOverflowMenuButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showOverflowMenuButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showProgress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData;->showProgress:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
