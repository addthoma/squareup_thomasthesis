.class public final Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen;
.super Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;
.source "EditInvoice1Screen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$ParentComponent;,
        Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Runner;,
        Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoice1Screen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoice1Screen.kt\ncom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,66:1\n52#2:67\n24#3,4:68\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoice1Screen.kt\ncom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen\n*L\n28#1:67\n59#1,4:68\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 \u00142\u00020\u00012\u00020\u00022\u00020\u0003:\u0003\u0014\u0015\u0016B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0014J\u0008\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\u000cH\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen;",
        "Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "editInvoiceScopeV2",
        "Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;",
        "(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getAnalyticsName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "Companion",
        "ParentComponent",
        "Runner",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen;->Companion:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$Companion;

    .line 68
    new-instance v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 71
    sput-object v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V
    .locals 1

    const-string v0, "editInvoiceScopeV2"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;-><init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-super {p0, p1, p2}, Lcom/squareup/invoices/editv2/InEditInvoiceScopeV2;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen;->getEditInvoiceScopeV2()Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 45
    invoke-virtual {p0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen;->getEditInvoiceScopeV2()Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/editv2/EditInvoiceScopeV2;->getEditInvoiceContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->INVOICES_CREATION_FORM:Lcom/squareup/analytics/RegisterViewName;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->UNKNOWN:Lcom/squareup/analytics/RegisterViewName;

    :goto_0
    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    const-class v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 28
    check-cast p1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$ParentComponent;

    .line 29
    invoke-interface {p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Screen$ParentComponent;->editInvoice1Coordinator()Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1Coordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 56
    sget v0, Lcom/squareup/features/invoices/R$layout;->edit_invoice_1_view:I

    return v0
.end method
