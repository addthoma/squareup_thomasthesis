.class public final Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;
.super Ljava/lang/Object;
.source "EditInvoice1ScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoice1ScreenData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoice1ScreenData.kt\ncom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,907:1\n1847#2,3:908\n1847#2,3:911\n1550#2,3:914\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoice1ScreenData.kt\ncom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory\n*L\n533#1,3:908\n551#1,3:911\n754#1,3:914\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00f0\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0015\u0018\u00002\u00020\u0001Bi\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0002\u0010\u001aJ\u0008\u0010\u001b\u001a\u00020\u001cH\u0002J\u0008\u0010\u001d\u001a\u00020\u001cH\u0002J\u001c\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0002J\u001c\u0010$\u001a\u0004\u0018\u00010\u001f2\u0006\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0002J\u001a\u0010%\u001a\u0004\u0018\u00010\u001c2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020)H\u0002J\u001a\u0010*\u001a\u0004\u0018\u00010+2\u000e\u0010,\u001a\n\u0012\u0004\u0012\u00020.\u0018\u00010-H\u0002J\"\u0010/\u001a\u0004\u0018\u00010\u001f2\u0006\u0010&\u001a\u00020\'2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u000201H\u0002J\u0008\u00103\u001a\u00020\u001cH\u0002J:\u00104\u001a\u0002052\u0006\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#2\u0008\u00106\u001a\u0004\u0018\u0001072\u0006\u00108\u001a\u0002092\u0006\u0010:\u001a\u0002012\u0006\u0010;\u001a\u000201J\u0018\u0010<\u001a\u00020=2\u0006\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u000201H\u0002J\u0018\u0010A\u001a\u00020B2\u0006\u00108\u001a\u0002092\u0006\u0010 \u001a\u00020!H\u0002J$\u0010C\u001a\u0004\u0018\u00010\u001f2\u0006\u0010 \u001a\u00020!2\u0006\u00108\u001a\u0002092\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0002J\u0010\u0010D\u001a\u00020=2\u0006\u0010 \u001a\u00020!H\u0002J\u0010\u0010E\u001a\u00020B2\u0006\u0010 \u001a\u00020!H\u0002J2\u0010F\u001a\u0004\u0018\u00010=2\u0006\u0010 \u001a\u00020!2\u0006\u00108\u001a\u0002092\u0006\u0010&\u001a\u00020\'2\u0006\u0010G\u001a\u0002012\u0006\u00100\u001a\u000201H\u0002J\u0010\u0010H\u001a\u00020I2\u0006\u00100\u001a\u000201H\u0002J \u0010J\u001a\u00020I2\u0006\u00108\u001a\u0002092\u0006\u0010 \u001a\u00020!2\u0006\u00100\u001a\u000201H\u0002J,\u0010K\u001a\u0004\u0018\u00010\u001f2\u0006\u00108\u001a\u0002092\u0008\u0010\"\u001a\u0004\u0018\u00010#2\u0006\u0010G\u001a\u0002012\u0006\u0010;\u001a\u000201H\u0002J\u0018\u0010L\u001a\u00020B2\u0006\u0010 \u001a\u00020!2\u0006\u0010;\u001a\u000201H\u0002J\"\u0010M\u001a\u00020=2\u0006\u0010&\u001a\u00020\'2\u0008\u00106\u001a\u0004\u0018\u0001072\u0006\u0010@\u001a\u000201H\u0002J4\u0010N\u001a\u00020B2\u0006\u00108\u001a\u0002092\u0006\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#2\u0008\u00106\u001a\u0004\u0018\u0001072\u0006\u0010;\u001a\u000201H\u0002J$\u0010O\u001a\u0004\u0018\u00010\u001f2\u0006\u0010 \u001a\u00020!2\u0006\u00108\u001a\u0002092\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0002J6\u0010P\u001a\u00020Q2\u000c\u0010R\u001a\u0008\u0012\u0004\u0012\u00020S0-2\u000e\u0010T\u001a\n\u0012\u0004\u0012\u00020U\u0018\u00010-2\u0006\u0010V\u001a\u00020W2\u0006\u0010X\u001a\u00020YH\u0002J\u001c\u0010Z\u001a\u0004\u0018\u00010\u001f2\u0006\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0002J$\u0010[\u001a\u0004\u0018\u00010B2\u0006\u00108\u001a\u0002092\u0006\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0002J&\u0010\\\u001a\u0004\u0018\u00010\u001f2\u0008\u0010\"\u001a\u0004\u0018\u00010#2\u0006\u0010\u0002\u001a\u00020\u00032\u0008\u0010]\u001a\u0004\u0018\u00010YH\u0002J\u001a\u0010^\u001a\u0004\u0018\u00010_2\u0006\u0010&\u001a\u00020\'2\u0006\u0010`\u001a\u000201H\u0002J\u0010\u0010a\u001a\u00020_2\u0006\u0010`\u001a\u000201H\u0002J,\u0010b\u001a\u0004\u0018\u00010=2\u0006\u00108\u001a\u0002092\u0006\u0010&\u001a\u00020\'2\u0006\u00100\u001a\u0002012\u0008\u0010c\u001a\u0004\u0018\u00010YH\u0002J \u0010d\u001a\u00020I2\u0006\u00100\u001a\u0002012\u0006\u0010&\u001a\u00020\'2\u0006\u00108\u001a\u000209H\u0002J\u0010\u0010e\u001a\u0002012\u0006\u00108\u001a\u000209H\u0002J\u0018\u0010f\u001a\u0002012\u0006\u0010&\u001a\u00020\'2\u0006\u0010G\u001a\u000201H\u0002J \u0010g\u001a\u0002012\u0006\u00100\u001a\u0002012\u0006\u0010&\u001a\u00020\'2\u0006\u00108\u001a\u000209H\u0002J\u001a\u0010h\u001a\u0002012\u0006\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#H\u0002J\u000c\u0010i\u001a\u00020I*\u00020IH\u0002J\u000c\u0010j\u001a\u00020I*\u00020#H\u0002J\u000c\u0010k\u001a\u00020I*\u00020#H\u0002J\u0014\u0010l\u001a\u00020Y*\u00020!2\u0006\u00108\u001a\u000209H\u0002J\u0014\u0010m\u001a\n\u0012\u0004\u0012\u00020U\u0018\u00010-*\u000209H\u0002J\u0010\u0010n\u001a\u0004\u0018\u00010B*\u0004\u0018\u00010BH\u0002J\u0010\u0010n\u001a\u0004\u0018\u00010\u001f*\u0004\u0018\u00010\u001fH\u0002J\u000c\u0010o\u001a\u000201*\u000209H\u0002J\u000e\u0010p\u001a\u000201*\u0004\u0018\u00010YH\u0002J\u0010\u0010q\u001a\u0004\u0018\u00010B*\u0004\u0018\u00010BH\u0002J\u0010\u0010q\u001a\u0004\u0018\u00010\u001f*\u0004\u0018\u00010\u001fH\u0002J\u0014\u0010r\u001a\n s*\u0004\u0018\u00010I0I*\u00020YH\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006t"
    }
    d2 = {
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "actionBarDataFactory",
        "Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;",
        "bottomButtonTextFactory",
        "Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;",
        "application",
        "Landroid/app/Application;",
        "paymentRequestDataFactory",
        "Lcom/squareup/invoices/PaymentRequestData$Factory;",
        "editPaymentRequestRowDataFactory",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;",
        "v2WidgetsEditPaymentRequestsDataFactory",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "locale",
        "Ljava/util/Locale;",
        "cnpFeesMessageHelper",
        "Lcom/squareup/cnp/CnpFeesMessageHelper;",
        "(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;Landroid/app/Application;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;Lcom/squareup/settings/server/Features;Ljava/util/Locale;Lcom/squareup/cnp/CnpFeesMessageHelper;)V",
        "addAttachmentButton",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;",
        "addLineItemButton",
        "addPaymentScheduleButton",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "invoice",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "recurrenceRule",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "addRequestDepositButton",
        "additionalRecipientsButton",
        "paymentMethod",
        "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
        "additionalRecipientsCount",
        "",
        "attachmentRows",
        "Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;",
        "attachments",
        "",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
        "automaticPaymentsRow",
        "isRecurring",
        "",
        "allowPaymentMethod",
        "billToButton",
        "create",
        "Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;",
        "instrumentSummary",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "editInvoiceContext",
        "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
        "showProgress",
        "inCheckout",
        "customerRow",
        "Lcom/squareup/features/invoices/widgets/SectionElement$RowData;",
        "customer",
        "Lcom/squareup/protos/client/invoice/InvoiceContact;",
        "disable",
        "customerSection",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionData;",
        "depositRemainderSection",
        "detailsRow",
        "detailsSection",
        "dueRow",
        "isFullInvoice",
        "dueRowLabel",
        "",
        "dueRowValue",
        "frequencyRow",
        "lineItemSection",
        "methodRow",
        "paymentDetailSection",
        "paymentPlanSection",
        "paymentRequestsRows",
        "Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;",
        "paymentRequests",
        "Lcom/squareup/protos/client/invoice/PaymentRequest;",
        "paymentRequestStates",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
        "invoiceAmount",
        "Lcom/squareup/protos/common/Money;",
        "invoiceFirstSentDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "paymentScheduleRowV2",
        "paymentScheduleSection",
        "recurringHelperTextRow",
        "scheduled_at",
        "requestShippingAddressRow",
        "Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;",
        "request",
        "requestTipToggle",
        "sendRow",
        "scheduledAt",
        "sendRowLabel",
        "shouldDisableMethodRow",
        "shouldHideDueRow",
        "shouldHideSendRow",
        "shouldShowPaymentScheduleSection",
        "firstInitial",
        "formatEndOnString",
        "formatRepeatEveryString",
        "getDueDate",
        "getPaymentRequestStates",
        "hideIfV2",
        "isEditingScheduledInvoice",
        "isToday",
        "showIfV2",
        "toDateString",
        "kotlin.jvm.PlatformType",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarDataFactory:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;

.field private final application:Landroid/app/Application;

.field private final bottomButtonTextFactory:Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;

.field private final clock:Lcom/squareup/util/Clock;

.field private final cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

.field private final dateFormat:Ljava/text/DateFormat;

.field private final editPaymentRequestRowDataFactory:Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final locale:Ljava/util/Locale;

.field private final paymentRequestDataFactory:Lcom/squareup/invoices/PaymentRequestData$Factory;

.field private final res:Lcom/squareup/util/Res;

.field private final v2WidgetsEditPaymentRequestsDataFactory:Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;Landroid/app/Application;Lcom/squareup/invoices/PaymentRequestData$Factory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;Lcom/squareup/settings/server/Features;Ljava/util/Locale;Lcom/squareup/cnp/CnpFeesMessageHelper;)V
    .locals 1
    .param p2    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionBarDataFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bottomButtonTextFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentRequestDataFactory"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editPaymentRequestRowDataFactory"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "v2WidgetsEditPaymentRequestsDataFactory"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cnpFeesMessageHelper"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->dateFormat:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    iput-object p4, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->actionBarDataFactory:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;

    iput-object p5, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->bottomButtonTextFactory:Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;

    iput-object p6, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->application:Landroid/app/Application;

    iput-object p7, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->paymentRequestDataFactory:Lcom/squareup/invoices/PaymentRequestData$Factory;

    iput-object p8, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->editPaymentRequestRowDataFactory:Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;

    iput-object p9, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->v2WidgetsEditPaymentRequestsDataFactory:Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;

    iput-object p10, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    iput-object p11, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->locale:Ljava/util/Locale;

    iput-object p12, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

    return-void
.end method

.method private final addAttachmentButton()Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;
    .locals 4

    .line 285
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 286
    iget-object v1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->add_attachment:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ADD_ATTACHMENT_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    .line 287
    sget v3, Lcom/squareup/features/invoices/widgets/R$drawable;->add_attachment_row_icon:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 285
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;)V

    return-object v0
.end method

.method private final addLineItemButton()Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;
    .locals 4

    .line 718
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 719
    iget-object v1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->add_line_item:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 720
    sget-object v2, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ADD_LINE_ITEM_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    .line 721
    sget v3, Lcom/squareup/features/invoices/R$drawable;->add_item_row_icon:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 718
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;)V

    return-object v0
.end method

.method private final addPaymentScheduleButton(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 4

    .line 528
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    .line 529
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    .line 532
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_6

    if-eqz p2, :cond_0

    goto :goto_3

    :cond_0
    const-string p2, "paymentRequests"

    .line 533
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 908
    instance-of p2, p1, Ljava/util/Collection;

    const/4 v0, 0x0

    const/4 v2, 0x1

    if-eqz p2, :cond_1

    move-object p2, p1

    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_2

    .line 909
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    const-string v3, "it"

    .line 533
    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {p2}, Lcom/squareup/invoices/PaymentRequestsKt;->isDepositRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result p2

    if-eqz p2, :cond_3

    goto :goto_0

    :cond_3
    const/4 p2, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 p2, 0x1

    :goto_1
    if-eqz p2, :cond_2

    const/4 v2, 0x0

    :cond_5
    :goto_2
    if-eqz v2, :cond_6

    .line 534
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 535
    iget-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->add_payment_schedule:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    sget-object v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ADD_PAYMENT_SCHEDULE_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    .line 536
    sget v1, Lcom/squareup/features/invoices/widgets/R$drawable;->add_row_icon:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 534
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/features/invoices/widgets/SectionElement;

    :cond_6
    :goto_3
    return-object v1
.end method

.method private final addRequestDepositButton(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 4

    .line 546
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    .line 547
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    .line 550
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_6

    if-eqz p2, :cond_0

    goto :goto_3

    :cond_0
    const-string p2, "paymentRequests"

    .line 551
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 911
    instance-of p2, p1, Ljava/util/Collection;

    const/4 v0, 0x0

    const/4 v2, 0x1

    if-eqz p2, :cond_1

    move-object p2, p1

    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_2

    .line 912
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    const-string v3, "it"

    .line 551
    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {p2}, Lcom/squareup/invoices/PaymentRequestsKt;->isDepositRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result p2

    if-eqz p2, :cond_3

    goto :goto_0

    :cond_3
    const/4 p2, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 p2, 0x1

    :goto_1
    if-eqz p2, :cond_2

    const/4 v2, 0x0

    :cond_5
    :goto_2
    if-eqz v2, :cond_6

    .line 552
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 553
    iget-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->request_deposit:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    sget-object v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->REQUEST_DEPOSIT_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    .line 554
    sget v1, Lcom/squareup/features/invoices/widgets/R$drawable;->add_row_icon:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 552
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/features/invoices/widgets/SectionElement;

    :cond_6
    :goto_3
    return-object v1
.end method

.method private final additionalRecipientsButton(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;I)Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;
    .locals 2

    .line 692
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    if-eqz p2, :cond_2

    const/4 p1, 0x1

    if-eq p2, p1, :cond_1

    .line 698
    iget-object p1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->add_additional_recipient_v2_plural:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v0, "count"

    .line 699
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 700
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 697
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/features/invoices/R$string;->add_additional_recipient_v2_single:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_0

    .line 696
    :cond_2
    iget-object p1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/features/invoices/R$string;->add_additional_recipient_v2:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 703
    :goto_0
    new-instance p2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 704
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 705
    sget-object v0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ADDITIONAL_RECIPIENTS_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    .line 706
    sget v1, Lcom/squareup/features/invoices/R$drawable;->add_recipient_row_icon:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 703
    invoke-direct {p2, p1, v0, v1}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;)V

    return-object p2
.end method

.method private final attachmentRows(Ljava/util/List;)Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 280
    invoke-static {p1}, Lcom/squareup/util/CollectionsKt;->nullIfEmpty(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 281
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;

    sget-object v1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ATTACHMENT_ROW_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    invoke-direct {v0, p1, v1}, Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;-><init>(Ljava/util/List;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private final automaticPaymentsRow(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;ZZ)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 9

    if-eqz p3, :cond_0

    .line 463
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_automatic_payment_on:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_automatic_payment_off:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v3, v0

    .line 466
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq p1, v0, :cond_2

    if-eqz p2, :cond_2

    .line 467
    iget-object p1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object p2, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 468
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    .line 469
    iget-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->allow_automatic_payments_label:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 470
    sget-object v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->AUTO_PAYMENTS_TOGGLED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    .line 471
    iget-object v1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->cnpFeesMessageHelper:Lcom/squareup/cnp/CnpFeesMessageHelper;

    invoke-virtual {v1}, Lcom/squareup/cnp/CnpFeesMessageHelper;->invoiceAutomaticPaymentOptInMessageV2()Ljava/lang/CharSequence;

    move-result-object v1

    .line 468
    invoke-direct {p1, p2, p3, v0, v1}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;-><init>(Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;)V

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement;

    goto :goto_1

    .line 474
    :cond_1
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    .line 475
    iget-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/features/invoices/R$string;->invoice_automatic_payments:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 477
    sget-object v4, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->AUTO_PAYMENTS_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p1

    .line 474
    invoke-direct/range {v1 .. v8}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/noho/NohoRow$Icon;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement;

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method private final billToButton()Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;
    .locals 4

    .line 711
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 712
    iget-object v1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->edit_invoice_add_customer:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ADD_CUSTOMER_BUTTON_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    .line 713
    sget v3, Lcom/squareup/features/invoices/R$drawable;->add_customer_row_icon:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 711
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;)V

    return-object v0
.end method

.method private final customerRow(Lcom/squareup/protos/client/invoice/InvoiceContact;Z)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;
    .locals 7

    .line 669
    new-instance v6, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    .line 670
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    .line 671
    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_detail_customer:I

    .line 670
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v1, v0

    .line 673
    iget-object v2, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    if-eqz p2, :cond_1

    .line 674
    sget-object p2, Lcom/squareup/features/invoices/widgets/NoOp;->INSTANCE:Lcom/squareup/features/invoices/widgets/NoOp;

    goto :goto_1

    :cond_1
    sget-object p2, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->VIEW_CUSTOMER_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    :goto_1
    move-object v3, p2

    .line 675
    new-instance p2, Lcom/squareup/noho/NohoRow$Icon$TextIcon;

    .line 676
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    const-string v4, "customer.buyer_name"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->firstInitial(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceContact;->contact_token:Ljava/lang/String;

    const-string v4, "customer.contact_token"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v4}, Lcom/squareup/crm/util/CustomerColorUtilsKt;->getCircleColor(Ljava/lang/String;Lcom/squareup/util/Res;)I

    move-result p1

    .line 677
    sget v4, Lcom/squareup/features/invoices/R$drawable;->customer_circle_background:I

    .line 675
    invoke-direct {p2, v0, p1, v4}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;-><init>(Ljava/lang/String;II)V

    move-object v4, p2

    check-cast v4, Lcom/squareup/noho/NohoRow$Icon;

    .line 679
    sget p1, Lcom/squareup/features/invoices/R$style;->CustomerTextIcon:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v0, v6

    .line 669
    invoke-direct/range {v0 .. v5}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/noho/NohoRow$Icon;Ljava/lang/Integer;)V

    return-object v6
.end method

.method private final customerSection(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 7

    .line 180
    iget-object v0, p2, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->nullIfAllFieldsNull(Lcom/squareup/protos/client/invoice/InvoiceContact;)Lcom/squareup/protos/client/invoice/InvoiceContact;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/squareup/features/invoices/widgets/SectionElement;

    const/4 v2, 0x0

    .line 183
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingNonDraftSentOrSharedSingleInvoice()Z

    move-result p1

    .line 182
    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->customerRow(Lcom/squareup/protos/client/invoice/InvoiceContact;Z)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    move-result-object p1

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object p1, v1, v2

    const/4 p1, 0x1

    .line 187
    invoke-static {p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    iget-object v2, p2, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_shipping_address_enabled:Ljava/lang/Boolean;

    const-string v3, "invoice.buyer_entered_shipping_address_enabled"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 186
    invoke-direct {p0, v0, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->requestShippingAddressRow(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Z)Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v0, v1, p1

    const/4 p1, 0x2

    .line 190
    invoke-static {p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    .line 191
    iget-object p2, p2, Lcom/squareup/protos/client/invoice/Invoice;->additional_recipient_email:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    .line 189
    invoke-direct {p0, v0, p2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->additionalRecipientsButton(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;I)Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    move-result-object p2

    check-cast p2, Lcom/squareup/features/invoices/widgets/SectionElement;

    .line 192
    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->showIfV2(Lcom/squareup/features/invoices/widgets/SectionElement;)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object p2

    aput-object p2, v1, p1

    .line 181
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 194
    :cond_0
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->billToButton()Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    :goto_0
    move-object v2, p1

    .line 196
    new-instance p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    .line 197
    new-instance p2, Lcom/squareup/features/invoices/widgets/Header$Show;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->uppercase_bill_to:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/features/invoices/widgets/Header$Show;-><init>(Ljava/lang/String;)V

    move-object v1, p2

    check-cast v1, Lcom/squareup/features/invoices/widgets/Header;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p1

    .line 196
    invoke-direct/range {v0 .. v6}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;-><init>(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method private final depositRemainderSection(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 10

    .line 608
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    .line 609
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    .line 610
    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->getPaymentRequestStates(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Ljava/util/List;

    move-result-object v2

    .line 611
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    .line 613
    invoke-virtual {p2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object p2

    iget-object v4, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-static {p2, p1, v4}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    .line 616
    iget-object p2, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_2

    if-eqz p3, :cond_0

    goto :goto_0

    .line 617
    :cond_0
    invoke-static {v1}, Lcom/squareup/invoices/PaymentRequestsKt;->hasDepositRequest(Ljava/util/List;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 618
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    iget-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/features/invoices/R$string;->request_deposit:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->REQUEST_DEPOSIT_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p1

    invoke-direct/range {v4 .. v9}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement;

    goto :goto_1

    :cond_1
    const-string p2, "paymentRequests"

    .line 620
    invoke-static {v1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 619
    invoke-direct {p0, v1, v2, v3, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->paymentRequestsRows(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;

    move-result-object p1

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement;

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method private final detailsRow(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;
    .locals 6

    .line 245
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 246
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->no_custom_invoice_id:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 248
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v3, 0x23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 252
    :goto_2
    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    check-cast v3, Ljava/lang/CharSequence;

    if-eqz v3, :cond_4

    invoke-static {v3}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_3

    :cond_3
    const/4 v3, 0x0

    goto :goto_4

    :cond_4
    :goto_3
    const/4 v3, 0x1

    :goto_4
    if-eqz v3, :cond_5

    .line 253
    iget-object v3, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->no_invoice_title:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_5

    .line 255
    :cond_5
    iget-object v3, p1, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    .line 259
    :goto_5
    iget-object v4, p1, Lcom/squareup/protos/client/invoice/Invoice;->description:Ljava/lang/String;

    check-cast v4, Ljava/lang/CharSequence;

    if-eqz v4, :cond_7

    invoke-static {v4}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    goto :goto_6

    :cond_6
    const/4 v4, 0x0

    goto :goto_7

    :cond_7
    :goto_6
    const/4 v4, 0x1

    :goto_7
    if-eqz v4, :cond_8

    const-string v4, ""

    goto :goto_8

    .line 262
    :cond_8
    iget-object v4, p1, Lcom/squareup/protos/client/invoice/Invoice;->description:Ljava/lang/String;

    .line 266
    :goto_8
    iget-object v5, p1, Lcom/squareup/protos/client/invoice/Invoice;->invoice_name:Ljava/lang/String;

    check-cast v5, Ljava/lang/CharSequence;

    if-eqz v5, :cond_a

    invoke-static {v5}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_9

    goto :goto_9

    :cond_9
    const/4 v5, 0x0

    goto :goto_a

    :cond_a
    :goto_9
    const/4 v5, 0x1

    :goto_a
    if-nez v5, :cond_d

    .line 267
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_b

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_c

    :cond_b
    const/4 v1, 0x1

    :cond_c
    if-eqz v1, :cond_d

    goto :goto_b

    .line 268
    :cond_d
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " ("

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x29

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 271
    :goto_b
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    const-string/jumbo v0, "title"

    .line 272
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    sget-object v0, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->EDIT_INVOICE_DETAILS_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    .line 275
    sget v1, Lcom/squareup/features/invoices/R$drawable;->details_row_icon:I

    .line 271
    invoke-direct {p1, v3, v4, v0, v1}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)V

    return-object p1
.end method

.method private final detailsSection(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 8

    .line 231
    new-instance v7, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    .line 232
    new-instance v0, Lcom/squareup/features/invoices/widgets/Header$Show;

    iget-object v1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->details_header:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/features/invoices/widgets/Header$Show;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/features/invoices/widgets/Header;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/features/invoices/widgets/SectionElement;

    .line 234
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->detailsRow(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    move-result-object v2

    check-cast v2, Lcom/squareup/features/invoices/widgets/SectionElement;

    const/4 v3, 0x0

    aput-object v2, v0, v3

    .line 235
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->attachmentRows(Ljava/util/List;)Lcom/squareup/features/invoices/widgets/SectionElement$AttachmentRows;

    move-result-object p1

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement;

    const/4 v2, 0x1

    aput-object p1, v0, v2

    .line 236
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->addAttachmentButton()Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    move-result-object p1

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement;

    const/4 v2, 0x2

    aput-object p1, v0, v2

    .line 233
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, v7

    .line 231
    invoke-direct/range {v0 .. v6}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;-><init>(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v7
.end method

.method private final dueRow(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;ZZ)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;
    .locals 0

    .line 395
    invoke-direct {p0, p3, p4}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->shouldHideDueRow(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Z)Z

    move-result p3

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 397
    :cond_0
    new-instance p3, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    .line 398
    invoke-direct {p0, p5}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->dueRowLabel(Z)Ljava/lang/String;

    move-result-object p4

    invoke-direct {p0, p2, p1, p5}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->dueRowValue(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice;Z)Ljava/lang/String;

    move-result-object p1

    .line 399
    sget-object p2, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->DUE_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    sget p5, Lcom/squareup/features/invoices/widgets/R$drawable;->due_row_icon:I

    .line 397
    invoke-direct {p3, p4, p1, p2, p5}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)V

    return-object p3
.end method

.method private final dueRowLabel(Z)Ljava/lang/String;
    .locals 1

    .line 807
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    if-eqz p1, :cond_0

    .line 809
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_each_invoice_due:I

    goto :goto_0

    .line 811
    :cond_0
    sget p1, Lcom/squareup/common/invoices/R$string;->invoice_detail_due:I

    .line 807
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final dueRowValue(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice;Z)Ljava/lang/String;
    .locals 7

    .line 835
    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->getDueDate(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    .line 838
    iget-object v1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-static {v0, v1}, Lcom/squareup/invoices/InvoiceDateUtility;->beforeOrEqualToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-static {p2}, Lcom/squareup/invoices/InvoiceDateUtility;->getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    :goto_0
    move-object v1, p2

    goto :goto_1

    .line 839
    :cond_0
    iget-object v1, p2, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-static {v1, v2}, Lcom/squareup/invoices/InvoiceDateUtility;->beforeOrEqualToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object p2, p2, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    goto :goto_0

    .line 840
    :cond_1
    iget-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-static {p2}, Lcom/squareup/invoices/InvoiceDateUtility;->getToday(Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    goto :goto_0

    .line 845
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result p2

    const/4 v2, 0x1

    if-nez p2, :cond_3

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDraft()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 p1, 0x1

    .line 848
    :goto_3
    iget-object v3, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    iget-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->application:Landroid/app/Application;

    move-object v4, p2

    check-cast v4, Landroid/content/Context;

    iget-object v5, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->dateFormat:Ljava/text/DateFormat;

    xor-int/lit8 v6, p3, 0x1

    move v2, p1

    .line 847
    invoke-static/range {v0 .. v6}, Lcom/squareup/invoices/InvoiceDateUtility;->formatDueDateValue(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;ZLcom/squareup/util/Res;Landroid/content/Context;Ljava/text/DateFormat;Z)Ljava/lang/CharSequence;

    move-result-object p1

    .line 850
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final firstInitial(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    .line 684
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object p1

    .line 685
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->locale:Ljava/util/Locale;

    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "(this as java.lang.String).toUpperCase(locale)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final formatEndOnString(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Ljava/lang/String;
    .locals 3

    .line 734
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_frequency_end:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 735
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getRecurrenceEnd()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {p1, v1, v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;->prettyString(Lcom/squareup/util/Res;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "count_or_date"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 736
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 737
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final formatRepeatEveryString(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Ljava/lang/String;
    .locals 4

    .line 726
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {p1, v0, v1, v2, v3}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->prettyString$default(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/util/Res;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 727
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_frequency_repeat_every:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 728
    check-cast p1, Ljava/lang/CharSequence;

    const-string v1, "duration"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 729
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 730
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final frequencyRow(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;ZZ)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 1

    if-eqz p3, :cond_4

    .line 409
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingNonDraftSingleInvoice()Z

    move-result p1

    if-nez p1, :cond_4

    if-eqz p4, :cond_0

    goto :goto_2

    .line 412
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object p3, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, p3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_2

    if-eqz p2, :cond_1

    .line 414
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    .line 415
    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->formatRepeatEveryString(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Ljava/lang/String;

    move-result-object p3

    .line 416
    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->formatEndOnString(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Ljava/lang/String;

    move-result-object p2

    .line 417
    sget-object p4, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->FREQUENCY_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    .line 418
    sget v0, Lcom/squareup/features/invoices/widgets/R$drawable;->recurring_row_icon:I

    .line 414
    invoke-direct {p1, p3, p2, p4, v0}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)V

    .line 413
    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement;

    goto :goto_0

    .line 420
    :cond_1
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 421
    iget-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/features/invoices/R$string;->make_this_invoice_recurring:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 422
    sget-object p3, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->FREQUENCY_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    .line 423
    sget p4, Lcom/squareup/features/invoices/widgets/R$drawable;->recurring_row_icon:I

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    .line 420
    invoke-direct {p1, p2, p3, p4}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;)V

    check-cast p1, Lcom/squareup/features/invoices/widgets/SectionElement;

    :goto_0
    return-object p1

    .line 427
    :cond_2
    iget-object p1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/features/invoices/R$string;->invoice_frequency:I

    invoke-interface {p1, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    if-eqz p2, :cond_3

    .line 430
    new-instance p3, Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;

    .line 432
    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->formatRepeatEveryString(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Ljava/lang/String;

    move-result-object p4

    .line 433
    invoke-direct {p0, p2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->formatEndOnString(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Ljava/lang/String;

    move-result-object p2

    .line 434
    sget-object v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->FREQUENCY_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    .line 430
    invoke-direct {p3, p1, p4, p2, v0}, Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 429
    check-cast p3, Lcom/squareup/features/invoices/widgets/SectionElement;

    goto :goto_1

    .line 436
    :cond_3
    new-instance p2, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    .line 438
    iget-object p3, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/features/invoices/R$string;->invoice_frequency_one_time:I

    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 439
    sget-object p4, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->FREQUENCY_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    .line 440
    sget v0, Lcom/squareup/features/invoices/widgets/R$drawable;->recurring_row_icon:I

    .line 436
    invoke-direct {p2, p1, p3, p4, v0}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)V

    move-object p3, p2

    check-cast p3, Lcom/squareup/features/invoices/widgets/SectionElement;

    :goto_1
    return-object p3

    :cond_4
    :goto_2
    const/4 p1, 0x0

    return-object p1
.end method

.method private final getDueDate(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1

    .line 868
    invoke-virtual {p2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object p2

    .line 869
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    .line 867
    invoke-static {p2, p1, v0}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p2

    .line 866
    invoke-static {p1, p2}, Lcom/squareup/invoices/Invoices;->getRemainderDueOn(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    return-object p1
.end method

.method private final getPaymentRequestStates(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
            ">;"
        }
    .end annotation

    .line 862
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object p1

    instance-of v0, p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p1, v1

    :cond_0
    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->payment_request_state:Ljava/util/List;

    :cond_1
    return-object v1
.end method

.method private final hideIfV2(Lcom/squareup/features/invoices/widgets/InvoiceSectionData;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 2

    .line 899
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    return-object p1
.end method

.method private final hideIfV2(Lcom/squareup/features/invoices/widgets/SectionElement;)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 2

    .line 891
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    return-object p1
.end method

.method private final isEditingScheduledInvoice(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z
    .locals 1

    .line 856
    instance-of v0, p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext$EditSingleInvoice;->getInvoiceDisplayDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;->SCHEDULED:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final isToday(Lcom/squareup/protos/common/time/YearMonthDay;)Z
    .locals 1

    .line 853
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/util/Clock;)Z

    move-result p1

    return p1
.end method

.method private final lineItemSection(Lcom/squareup/protos/client/invoice/Invoice;Z)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 7

    .line 206
    iget-object v1, p1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    const-string v0, "cart"

    if-eqz p2, :cond_0

    .line 208
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;-><init>(Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Object;Ljava/lang/Object;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOfNotNull(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    :goto_0
    move-object v2, p1

    goto :goto_2

    .line 210
    :cond_0
    invoke-static {v1}, Lcom/squareup/invoices/InvoiceCartUtils;->isEmpty(Lcom/squareup/protos/client/bills/Cart;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 211
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->addLineItemButton()Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p2, 0x3

    new-array p2, p2, [Lcom/squareup/features/invoices/widgets/SectionElement;

    .line 214
    new-instance v2, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->LINE_ITEM_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    sget-object v3, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ADD_LINE_ITEM_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-direct {v2, v1, v0, v3}, Lcom/squareup/features/invoices/widgets/SectionElement$CartEntryData;-><init>(Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Object;Ljava/lang/Object;)V

    check-cast v2, Lcom/squareup/features/invoices/widgets/SectionElement;

    const/4 v0, 0x0

    aput-object v2, p2, v0

    .line 215
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->isTippingEnabled(Lcom/squareup/protos/client/invoice/Invoice;)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->requestTipToggle(Z)Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    move-result-object v1

    .line 216
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p1

    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    const/4 v3, 0x1

    if-eq p1, v2, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    :goto_1
    check-cast v1, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v1, p2, v3

    const/4 p1, 0x2

    .line 217
    invoke-direct {p0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->addLineItemButton()Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/SectionElement;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->hideIfV2(Lcom/squareup/features/invoices/widgets/SectionElement;)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object v0

    aput-object v0, p2, p1

    .line 213
    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    .line 222
    :goto_2
    new-instance p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    .line 223
    new-instance p2, Lcom/squareup/features/invoices/widgets/Header$Show;

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/invoices/R$string;->uppercase_line_items:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/features/invoices/widgets/Header$Show;-><init>(Ljava/lang/String;)V

    move-object v1, p2

    check-cast v1, Lcom/squareup/features/invoices/widgets/Header;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p1

    .line 222
    invoke-direct/range {v0 .. v6}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;-><init>(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1
.end method

.method private final methodRow(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/protos/client/instruments/InstrumentSummary;Z)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;
    .locals 3

    .line 450
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    .line 451
    iget-object v1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_method:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 452
    iget-object v2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v2, p2}, Lcom/squareup/invoices/edit/PaymentMethodUtility;->getPaymentMethodString(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/util/Res;Lcom/squareup/protos/client/instruments/InstrumentSummary;)Ljava/lang/String;

    move-result-object p1

    if-eqz p3, :cond_0

    .line 453
    sget-object p2, Lcom/squareup/features/invoices/widgets/NoOp;->INSTANCE:Lcom/squareup/features/invoices/widgets/NoOp;

    goto :goto_0

    :cond_0
    sget-object p2, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->INVOICE_METHOD_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    :goto_0
    sget p3, Lcom/squareup/features/invoices/R$drawable;->share_row_icon:I

    .line 450
    invoke-direct {v0, v1, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)V

    return-object v0
.end method

.method private final paymentDetailSection(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/protos/client/instruments/InstrumentSummary;Z)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 19

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move/from16 v10, p5

    .line 299
    iget-object v0, v6, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, v6, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->payment_schedule_all_caps:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 302
    :cond_0
    iget-object v0, v6, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->uppercase_payment_details:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    move-object v11, v0

    .line 306
    iget-object v0, v6, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const-string v12, "invoice.buyer_entered_au\u2026tic_charge_enroll_enabled"

    const/4 v14, 0x4

    const/4 v15, 0x2

    const-string v5, "invoice.payment_request"

    const/16 v16, 0x1

    const/16 v17, 0x0

    if-eqz v0, :cond_3

    new-array v14, v14, [Lcom/squareup/features/invoices/widgets/SectionElement;

    .line 308
    invoke-direct {v6, v8, v9}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->paymentScheduleRowV2(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object v0

    aput-object v0, v14, v17

    .line 310
    invoke-static/range {p2 .. p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v3

    .line 311
    iget-object v0, v8, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isFull(Ljava/util/List;)Z

    move-result v4

    if-eqz v9, :cond_1

    const/16 v18, 0x1

    goto :goto_1

    :cond_1
    const/16 v18, 0x0

    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move-object v13, v5

    move/from16 v5, v18

    .line 309
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->dueRow(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;ZZ)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/SectionElement;

    aput-object v0, v14, v16

    .line 314
    iget-object v0, v8, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {v0, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isFull(Ljava/util/List;)Z

    move-result v0

    .line 313
    invoke-direct {v6, v7, v9, v0, v10}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->frequencyRow(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;ZZ)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object v0

    aput-object v0, v14, v15

    .line 317
    invoke-static/range {p2 .. p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    if-eqz v9, :cond_2

    const/4 v1, 0x1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    .line 318
    :goto_2
    iget-object v2, v8, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    invoke-static {v2, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 316
    invoke-direct {v6, v0, v1, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->automaticPaymentsRow(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;ZZ)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v14, v1

    .line 307
    invoke-static {v14}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_3
    move-object v14, v0

    goto/16 :goto_7

    :cond_3
    move-object v13, v5

    const/4 v0, 0x7

    new-array v5, v0, [Lcom/squareup/features/invoices/widgets/SectionElement;

    .line 324
    invoke-static/range {p2 .. p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    .line 325
    invoke-direct/range {p0 .. p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->shouldDisableMethodRow(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z

    move-result v1

    move-object/from16 v2, p4

    .line 323
    invoke-direct {v6, v0, v2, v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->methodRow(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/protos/client/instruments/InstrumentSummary;Z)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/SectionElement;

    .line 326
    invoke-direct {v6, v0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->hideIfV2(Lcom/squareup/features/invoices/widgets/SectionElement;)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object v0

    aput-object v0, v5, v17

    .line 328
    iget-object v0, v8, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {v0, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isFull(Ljava/util/List;)Z

    move-result v0

    .line 327
    invoke-direct {v6, v7, v9, v0, v10}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->frequencyRow(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;ZZ)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object v0

    aput-object v0, v5, v16

    .line 331
    invoke-static/range {p2 .. p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    if-eqz v9, :cond_4

    const/4 v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    .line 332
    :goto_4
    iget-object v2, v8, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 330
    invoke-direct {v6, v7, v0, v1, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->sendRow(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;ZLcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/SectionElement;

    .line 333
    invoke-direct {v6, v0}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->hideIfV2(Lcom/squareup/features/invoices/widgets/SectionElement;)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object v0

    aput-object v0, v5, v15

    .line 335
    invoke-static/range {p2 .. p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v3

    .line 336
    iget-object v0, v8, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    invoke-static {v0, v13}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsConfigKt;->isFull(Ljava/util/List;)Z

    move-result v4

    if-eqz v9, :cond_5

    const/4 v10, 0x1

    goto :goto_5

    :cond_5
    const/4 v10, 0x0

    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move-object v13, v5

    move v5, v10

    .line 334
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->dueRow(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;ZZ)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/SectionElement;

    const/4 v1, 0x3

    aput-object v0, v13, v1

    .line 339
    invoke-static/range {p2 .. p2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    if-eqz v9, :cond_6

    const/4 v1, 0x1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    .line 340
    :goto_6
    iget-object v2, v8, Lcom/squareup/protos/client/invoice/Invoice;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    invoke-static {v2, v12}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 338
    invoke-direct {v6, v0, v1, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->automaticPaymentsRow(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;ZZ)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object v0

    aput-object v0, v13, v14

    const/4 v0, 0x5

    .line 342
    invoke-direct {v6, v8, v7, v9}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->paymentPlanSection(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object v1

    aput-object v1, v13, v0

    const/4 v0, 0x6

    .line 343
    iget-object v1, v6, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    iget-object v2, v8, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-direct {v6, v9, v1, v2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->recurringHelperTextRow(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/util/Res;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object v1

    aput-object v1, v13, v0

    .line 322
    invoke-static {v13}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_3

    .line 347
    :goto_7
    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    .line 348
    new-instance v1, Lcom/squareup/features/invoices/widgets/Header$Show;

    invoke-direct {v1, v11}, Lcom/squareup/features/invoices/widgets/Header$Show;-><init>(Ljava/lang/String;)V

    move-object v13, v1

    check-cast v13, Lcom/squareup/features/invoices/widgets/Header;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0xc

    const/16 v18, 0x0

    move-object v12, v0

    .line 347
    invoke-direct/range {v12 .. v18}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;-><init>(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method private final paymentPlanSection(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 2

    .line 497
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    invoke-direct {p0, p1, p3}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->addPaymentScheduleButton(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object p1

    goto :goto_0

    .line 500
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->depositRemainderSection(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final paymentRequestsRows(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            ")",
            "Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;"
        }
    .end annotation

    .line 633
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;

    .line 634
    iget-object v1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->paymentRequestDataFactory:Lcom/squareup/invoices/PaymentRequestData$Factory;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/squareup/invoices/PaymentRequestData$Factory;->createForEditing(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;

    move-result-object p1

    .line 640
    sget-object p2, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->PAYMENT_REQUEST_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    .line 633
    invoke-direct {v0, p1, p2}, Lcom/squareup/features/invoices/widgets/SectionElement$PaymentRequestRows;-><init>(Ljava/util/List;Ljava/lang/Object;)V

    return-object v0
.end method

.method private final paymentScheduleRowV2(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 4

    .line 512
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->shouldShowPaymentScheduleSection(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    new-instance p2, Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;

    .line 514
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->v2WidgetsEditPaymentRequestsDataFactory:Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;

    .line 515
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    const-string v3, "invoice.payment_request"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    const-string v3, "invoice.scheduled_at"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 514
    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData$Factory;->create(Lcom/squareup/protos/common/Money;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;

    move-result-object p1

    .line 516
    sget-object v0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->EDIT_PAYMENT_SCHEDULE_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    .line 513
    invoke-direct {p2, p1, v0}, Lcom/squareup/features/invoices/widgets/SectionElement$EditPaymentRequestRows;-><init>(Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionData;Ljava/lang/Object;)V

    check-cast p2, Lcom/squareup/features/invoices/widgets/SectionElement;

    goto :goto_0

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->addPaymentScheduleButton(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object p2

    goto :goto_0

    .line 520
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->addRequestDepositButton(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/SectionElement;

    move-result-object p2

    :goto_0
    return-object p2
.end method

.method private final paymentScheduleSection(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 10

    .line 568
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2, p3}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->shouldShowPaymentScheduleSection(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 572
    invoke-static {p2}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/common/Money;

    move-result-object p3

    .line 574
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-static {p1, p2, v0}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/util/Clock;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    .line 575
    iget-object p2, p2, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    .line 577
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v3, v0

    check-cast v3, Ljava/util/List;

    .line 580
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->editPaymentRequestRowDataFactory:Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;

    const-string v1, "paymentRequests"

    .line 581
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 580
    invoke-interface {v0, p2, p3, p1}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestRowDataFactory;->fromPaymentRequests(Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/List;

    move-result-object p1

    .line 584
    new-instance p2, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;

    .line 585
    iget-object p3, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->edit_payment_schedule:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 586
    sget-object v6, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->EDIT_PAYMENT_SCHEDULE_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v7, 0x0

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object v4, p2

    .line 584
    invoke-direct/range {v4 .. v9}, Lcom/squareup/features/invoices/widgets/SectionElement$ButtonData;-><init>(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 589
    check-cast p1, Ljava/util/Collection;

    invoke-interface {v3, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 590
    invoke-interface {v3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 593
    new-instance p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    .line 594
    new-instance p2, Lcom/squareup/features/invoices/widgets/Header$Show;

    iget-object p3, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->payment_schedule_all_caps:I

    invoke-interface {p3, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/squareup/features/invoices/widgets/Header$Show;-><init>(Ljava/lang/String;)V

    move-object v2, p2

    check-cast v2, Lcom/squareup/features/invoices/widgets/Header;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xc

    move-object v1, p1

    .line 593
    invoke-direct/range {v1 .. v7}, Lcom/squareup/features/invoices/widgets/InvoiceSectionData;-><init>(Lcom/squareup/features/invoices/widgets/Header;Ljava/util/List;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final recurringHelperTextRow(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/util/Res;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 1

    if-eqz p1, :cond_0

    .line 359
    new-instance v0, Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;

    invoke-static {p1, p2, p3}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->getRecurringPeriodLongText(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/util/Res;Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {v0, p1}, Lcom/squareup/features/invoices/widgets/SectionElement$HelperTextData;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 358
    :goto_0
    check-cast v0, Lcom/squareup/features/invoices/widgets/SectionElement;

    return-object v0
.end method

.method private final requestShippingAddressRow(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Z)Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;
    .locals 7

    .line 647
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 650
    :cond_0
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    .line 651
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_request_shipping_address:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 652
    sget-object v3, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->REQUEST_SHIPPING_ADDRESS_TOGGLED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p1

    move v2, p2

    .line 650
    invoke-direct/range {v0 .. v6}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;-><init>(Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    return-object p1
.end method

.method private final requestTipToggle(Z)Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;
    .locals 8

    .line 660
    new-instance v7, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;

    .line 661
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_request_tip:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->REQUEST_A_TIP_TOGGLED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, v7

    move v2, p1

    .line 660
    invoke-direct/range {v0 .. v6}, Lcom/squareup/features/invoices/widgets/SectionElement$ToggleData;-><init>(Ljava/lang/String;ZLjava/lang/Object;Ljava/lang/CharSequence;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v7
.end method

.method private final sendRow(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;ZLcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/features/invoices/widgets/SectionElement$RowData;
    .locals 2

    .line 369
    invoke-direct {p0, p3, p2, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->shouldHideSendRow(ZLcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    .line 373
    :cond_0
    invoke-direct {p0, p3, p2, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->sendRowLabel(ZLcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Ljava/lang/String;

    move-result-object p2

    .line 379
    invoke-direct {p0, p4}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->isEditingScheduledInvoice(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    .line 380
    sget p3, Lcom/squareup/common/invoices/R$string;->invoice_send_immediately:I

    .line 379
    invoke-interface {p1, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    if-eqz p4, :cond_2

    .line 382
    invoke-direct {p0, p4}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->toDateString(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;

    move-result-object v1

    .line 385
    :cond_2
    :goto_0
    new-instance p1, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;

    sget-object p3, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->SEND_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    sget p4, Lcom/squareup/features/invoices/R$drawable;->send_row_icon:I

    invoke-direct {p1, p2, v1, p3, p4}, Lcom/squareup/features/invoices/widgets/SectionElement$RowData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)V

    return-object p1
.end method

.method private final sendRowLabel(ZLcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Ljava/lang/String;
    .locals 1

    .line 782
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->res:Lcom/squareup/util/Res;

    if-eqz p1, :cond_1

    .line 786
    invoke-virtual {p3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingNonDraftSeries()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 787
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_next_invoice:I

    goto :goto_0

    .line 789
    :cond_0
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_start:I

    goto :goto_0

    .line 794
    :cond_1
    sget-object p1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result p2

    aget p1, p1, p2

    const/4 p2, 0x1

    if-eq p1, p2, :cond_3

    const/4 p2, 0x2

    if-eq p1, p2, :cond_2

    .line 799
    sget p1, Lcom/squareup/utilities/R$string;->send:I

    goto :goto_0

    .line 796
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Send row should be hidden for Share link."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 795
    :cond_3
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_charge_cof_action:I

    .line 782
    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final shouldDisableMethodRow(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z
    .locals 1

    .line 488
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingNonDraftSentOrSharedSingleInvoice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 489
    invoke-virtual {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne p1, v0, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method private final shouldHideDueRow(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Z)Z
    .locals 1

    .line 773
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq p1, v0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private final shouldHideSendRow(ZLcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)Z
    .locals 0

    if-nez p1, :cond_0

    .line 766
    sget-object p1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq p2, p1, :cond_2

    .line 765
    :cond_0
    invoke-virtual {p3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingNonDraftSentOrSharedSingleInvoice()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 766
    invoke-virtual {p3}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    sget-object p2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne p1, p2, :cond_3

    :cond_2
    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method private final shouldShowPaymentScheduleSection(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Z
    .locals 3

    .line 749
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->line_items:Lcom/squareup/protos/client/bills/Cart$LineItems;

    .line 750
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->payment_request:Ljava/util/List;

    .line 752
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$LineItems;->itemization:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_5

    if-nez p2, :cond_5

    const-string p2, "paymentRequests"

    .line 754
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 914
    instance-of p2, p1, Ljava/util/Collection;

    if-eqz p2, :cond_1

    move-object p2, p1

    check-cast p2, Ljava/util/Collection;

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_1

    :cond_0
    const/4 p1, 0x0

    goto :goto_2

    .line 915
    :cond_1
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    const-string v0, "it"

    .line 755
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p2}, Lcom/squareup/invoices/PaymentRequestsKt;->isInstallment(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {p2}, Lcom/squareup/invoices/PaymentRequestsKt;->isDepositRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result p2

    if-eqz p2, :cond_3

    goto :goto_0

    :cond_3
    const/4 p2, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 p2, 0x1

    :goto_1
    if-eqz p2, :cond_2

    const/4 p1, 0x1

    :goto_2
    if-eqz p1, :cond_5

    goto :goto_3

    :cond_5
    const/4 v1, 0x0

    :goto_3
    return v1
.end method

.method private final showIfV2(Lcom/squareup/features/invoices/widgets/InvoiceSectionData;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;
    .locals 2

    .line 883
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final showIfV2(Lcom/squareup/features/invoices/widgets/SectionElement;)Lcom/squareup/features/invoices/widgets/SectionElement;
    .locals 2

    .line 875
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_EDIT_FLOW_V2_WIDGETS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final toDateString(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/String;
    .locals 1

    .line 859
    iget-object v0, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->dateFormat:Ljava/text/DateFormat;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDateUtility;->getYMDDateString(Lcom/squareup/protos/common/time/YearMonthDay;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final create(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/protos/client/instruments/InstrumentSummary;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;ZZ)Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;
    .locals 9

    const-string v0, "invoice"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceContext"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    invoke-direct {p0, p4, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->customerSection(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object v0

    .line 144
    invoke-direct {p0, p1, p6}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->lineItemSection(Lcom/squareup/protos/client/invoice/Invoice;Z)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object v1

    move-object v2, p0

    move-object v3, p4

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, p6

    .line 146
    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->paymentDetailSection(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/protos/client/instruments/InstrumentSummary;Z)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object p3

    .line 151
    invoke-direct {p0, p4, p1, p2}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->paymentScheduleSection(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object p6

    .line 152
    invoke-direct {p0, p6}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->hideIfV2(Lcom/squareup/features/invoices/widgets/InvoiceSectionData;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object p6

    .line 154
    new-instance v8, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;

    .line 155
    iget-object v2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->actionBarDataFactory:Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;

    .line 156
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getTotalWithoutTip(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 155
    :goto_0
    invoke-virtual {v2, v3, p4, p2}, Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData$Factory;->create(Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;

    move-result-object v3

    .line 159
    iget-object p2, p0, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->bottomButtonTextFactory:Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;

    invoke-virtual {p2, p1, p4, v5}, Lcom/squareup/invoices/editv2/bottombutton/EditInvoiceV2BottomButtonTextFactory;->create(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;Z)Ljava/lang/String;

    move-result-object p2

    .line 163
    invoke-virtual {p4}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p4}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDraft()Z

    move-result p4

    if-eqz p4, :cond_1

    goto :goto_1

    :cond_1
    const/4 v6, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v6, 0x1

    :goto_2
    const/4 p4, 0x5

    new-array p4, p4, [Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    aput-object v0, p4, v4

    aput-object v1, p4, v5

    const/4 v0, 0x2

    aput-object p3, p4, v0

    const/4 p3, 0x3

    aput-object p6, p4, p3

    const/4 p3, 0x4

    .line 169
    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->detailsSection(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$Factory;->showIfV2(Lcom/squareup/features/invoices/widgets/InvoiceSectionData;)Lcom/squareup/features/invoices/widgets/InvoiceSectionData;

    move-result-object p1

    aput-object p1, p4, p3

    .line 164
    invoke-static {p4}, Lkotlin/collections/CollectionsKt;->listOfNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 170
    invoke-static {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionDataKt;->addDividers(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    move-object v2, v8

    move-object v4, p2

    move v5, p5

    .line 154
    invoke-direct/range {v2 .. v7}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData;-><init>(Lcom/squareup/invoices/editv2/actionbar/EditInvoiceV2ActionBarData;Ljava/lang/String;ZZLjava/util/List;)V

    return-object v8
.end method
