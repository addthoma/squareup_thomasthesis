.class final Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1;
.super Ljava/lang/Object;
.source "WorkingInvoiceEditor.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/payment/Order;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "order",
        "Lcom/squareup/payment/Order;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1;->this$0:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/payment/Order;)V
    .locals 3

    .line 65
    iget-object v0, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1;->this$0:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-static {v0}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->access$getInvoiceRelay$p(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1$1;

    invoke-direct {v2, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1$1;-><init>(Lcom/squareup/payment/Order;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->access$update(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;Lcom/jakewharton/rxrelay2/BehaviorRelay;Lkotlin/jvm/functions/Function1;)V

    const-string v0, "order"

    .line 71
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    if-nez p1, :cond_0

    .line 72
    iget-object p1, p0, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1;->this$0:Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;->access$resetInstruments(Lcom/squareup/invoices/editv2/WorkingInvoiceEditor;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/payment/Order;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/WorkingInvoiceEditor$onEnterScope$1;->accept(Lcom/squareup/payment/Order;)V

    return-void
.end method
