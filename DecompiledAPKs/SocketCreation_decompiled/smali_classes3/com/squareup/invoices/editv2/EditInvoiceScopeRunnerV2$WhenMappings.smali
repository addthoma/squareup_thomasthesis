.class public final synthetic Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I

.field public static final synthetic $EnumSwitchMapping$4:[I

.field public static final synthetic $EnumSwitchMapping$5:[I

.field public static final synthetic $EnumSwitchMapping$6:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 7

    invoke-static {}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->values()[Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ADD_CUSTOMER_BUTTON_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->VIEW_CUSTOMER_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ADD_LINE_ITEM_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->FREQUENCY_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->INVOICE_METHOD_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->REQUEST_DEPOSIT_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    const/4 v6, 0x6

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->SEND_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    const/4 v6, 0x7

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->DUE_ROW_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    const/16 v6, 0x8

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->AUTO_PAYMENTS_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    const/16 v6, 0x9

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ADD_PAYMENT_SCHEDULE_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    const/16 v6, 0xa

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->EDIT_PAYMENT_SCHEDULE_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    const/16 v6, 0xb

    aput v6, v0, v1

    invoke-static {}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->values()[Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->EDIT_INVOICE_DETAILS_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->REMINDERS_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ADDITIONAL_RECIPIENTS_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ADD_ATTACHMENT_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ordinal()I

    move-result v1

    aput v5, v0, v1

    invoke-static {}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;->values()[Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;->SAVE_AS_DRAFT:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;->DELETE_DRAFT:Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/overflow/EditInvoiceOverflowDialogData$EventKey;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->values()[Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->values()[Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ATTACHMENT_ROW_CLICKED:Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/secondscreen/EditInvoice2ScreenData$EventKey;->ordinal()I

    move-result v1

    aput v2, v0, v1

    invoke-static {}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->values()[Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->PAYMENT_REQUEST_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->EDIT_PAYMENT_SCHEDULE_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->values()[Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->LINE_ITEM_CLICKED:Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;

    invoke-virtual {v1}, Lcom/squareup/invoices/editv2/firstscreen/EditInvoice1ScreenData$EventKey;->ordinal()I

    move-result v1

    aput v2, v0, v1

    return-void
.end method
