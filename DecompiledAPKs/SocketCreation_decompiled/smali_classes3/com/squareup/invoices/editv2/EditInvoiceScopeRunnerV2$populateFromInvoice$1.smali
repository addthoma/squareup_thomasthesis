.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$populateFromInvoice$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->populateFromInvoice(Lcom/squareup/payment/InvoicePayment;Lcom/squareup/protos/client/invoice/Invoice;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/util/Optional<",
        "+",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "summary",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_populateFromInvoice:Lcom/squareup/payment/InvoicePayment;


# direct methods
.method constructor <init>(Lcom/squareup/payment/InvoicePayment;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$populateFromInvoice$1;->$this_populateFromInvoice:Lcom/squareup/payment/InvoicePayment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/util/Optional;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;)V"
        }
    .end annotation

    .line 1339
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$populateFromInvoice$1;->$this_populateFromInvoice:Lcom/squareup/payment/InvoicePayment;

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/instruments/InstrumentSummary;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/payment/InvoicePayment;->setChargedInvoice(ZLcom/squareup/protos/client/instruments/InstrumentSummary;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$populateFromInvoice$1;->accept(Lcom/squareup/util/Optional;)V

    return-void
.end method
