.class public final Lcom/squareup/invoices/editv2/service/EditInvoiceFailureUtilsKt;
.super Ljava/lang/Object;
.source "EditInvoiceFailureUtils.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u001b\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u00022\u0006\u0010\u0003\u001a\u0002H\u0002H\u0002\u00a2\u0006\u0002\u0010\u0004\u001a\u001e\u0010\u0005\u001a\u00020\u0006\"\u0004\u0008\u0000\u0010\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t\u00a8\u0006\n"
    }
    d2 = {
        "convertToStatus",
        "Lcom/squareup/protos/client/Status;",
        "T",
        "response",
        "(Ljava/lang/Object;)Lcom/squareup/protos/client/Status;",
        "toFailureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$convertToStatus(Ljava/lang/Object;)Lcom/squareup/protos/client/Status;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/editv2/service/EditInvoiceFailureUtilsKt;->convertToStatus(Ljava/lang/Object;)Lcom/squareup/protos/client/Status;

    move-result-object p0

    return-object p0
.end method

.method private static final convertToStatus(Ljava/lang/Object;)Lcom/squareup/protos/client/Status;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/squareup/protos/client/Status;"
        }
    .end annotation

    .line 31
    instance-of v0, p0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;

    const-string v1, "response.status"

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 32
    :cond_0
    instance-of v0, p0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 33
    :cond_1
    instance-of v0, p0, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 34
    :cond_2
    instance-of v0, p0, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;

    if-eqz v0, :cond_3

    check-cast p0, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 35
    :cond_3
    instance-of v0, p0, Lcom/squareup/protos/client/invoice/DeleteDraftResponse;

    if-eqz v0, :cond_4

    check-cast p0, Lcom/squareup/protos/client/invoice/DeleteDraftResponse;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/DeleteDraftResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 36
    :cond_4
    instance-of v0, p0, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;

    if-eqz v0, :cond_5

    check-cast p0, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p0

    .line 37
    :cond_5
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected response type"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;Lcom/squareup/receiving/FailureMessageFactory;)Lcom/squareup/receiving/FailureMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ")",
            "Lcom/squareup/receiving/FailureMessage;"
        }
    .end annotation

    const-string v0, "$this$toFailureMessage"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget v0, Lcom/squareup/features/invoices/R$string;->network_error_failed:I

    .line 26
    sget-object v1, Lcom/squareup/invoices/editv2/service/EditInvoiceFailureUtilsKt$toFailureMessage$1;->INSTANCE:Lcom/squareup/invoices/editv2/service/EditInvoiceFailureUtilsKt$toFailureMessage$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 24
    invoke-virtual {p1, p0, v0, v1}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p0

    return-object p0
.end method
