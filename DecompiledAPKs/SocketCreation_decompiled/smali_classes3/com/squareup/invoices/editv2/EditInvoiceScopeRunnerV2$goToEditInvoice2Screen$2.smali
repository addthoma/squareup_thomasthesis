.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$2;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->goToEditInvoice2Screen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceScopeRunnerV2.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceScopeRunnerV2.kt\ncom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$2\n*L\n1#1,1749:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/invoices/editv2/SaveDraftResult;",
        "kotlin.jvm.PlatformType",
        "validationResult",
        "Lcom/squareup/invoices/editv2/validation/ValidationResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/invoices/editv2/validation/ValidationResult;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/editv2/validation/ValidationResult;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/invoices/editv2/SaveDraftResult;",
            ">;"
        }
    .end annotation

    const-string/jumbo v0, "validationResult"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 393
    instance-of v0, p1, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/invoices/editv2/SaveDraftResult$ValidationError;

    check-cast p1, Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/editv2/SaveDraftResult$ValidationError;-><init>(Lcom/squareup/invoices/editv2/validation/ValidationResult$InvalidWithMessage;)V

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(ValidationError(validationResult))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 394
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/editv2/validation/ValidationResult$Valid;

    if-eqz v0, :cond_1

    .line 396
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$2;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$saveDraftSingle(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lio/reactivex/Single;

    move-result-object p1

    sget-object v0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$2$1;->INSTANCE:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$2$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "saveDraftSingle().map { DraftSaved(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    .line 394
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Can\'t handle validation error case: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/invoices/editv2/validation/ValidationResult;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$goToEditInvoice2Screen$2;->apply(Lcom/squareup/invoices/editv2/validation/ValidationResult;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
