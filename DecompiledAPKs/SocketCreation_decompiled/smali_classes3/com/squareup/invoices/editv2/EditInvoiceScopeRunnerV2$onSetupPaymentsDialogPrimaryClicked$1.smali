.class final Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSetupPaymentsDialogPrimaryClicked$1;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunnerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->onSetupPaymentsDialogPrimaryClicked(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "response",
        "Lcom/squareup/invoices/editv2/service/SaveDraftResponse;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSetupPaymentsDialogPrimaryClicked$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V
    .locals 2

    .line 529
    invoke-virtual {p1}, Lcom/squareup/invoices/editv2/service/SaveDraftResponse;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    iget-object p1, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSetupPaymentsDialogPrimaryClicked$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    invoke-static {p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$getOnboardingDiverter$p(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;)Lcom/squareup/onboarding/OnboardingDiverter;

    move-result-object p1

    sget-object v0, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->RESTART:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    invoke-interface {p1, v0}, Lcom/squareup/onboarding/OnboardingDiverter;->maybeDivertToOnboardingOrBank(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V

    goto :goto_0

    .line 532
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSetupPaymentsDialogPrimaryClicked$1;->this$0:Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;

    const-string v1, "response"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;->access$goToSaveDraftConfirmation(Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2;Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 236
    check-cast p1, Lcom/squareup/invoices/editv2/service/SaveDraftResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/editv2/EditInvoiceScopeRunnerV2$onSetupPaymentsDialogPrimaryClicked$1;->accept(Lcom/squareup/invoices/editv2/service/SaveDraftResponse;)V

    return-void
.end method
