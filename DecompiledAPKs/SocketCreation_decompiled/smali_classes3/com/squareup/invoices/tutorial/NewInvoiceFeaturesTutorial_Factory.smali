.class public final Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;
.super Ljava/lang/Object;
.source "NewInvoiceFeaturesTutorial_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;",
        ">;"
    }
.end annotation


# instance fields
.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final tipsDismissedSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;>;>;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;->presenterProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;->resProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;->tipsDismissedSettingProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;>;>;)",
            "Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Landroid/app/Application;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/register/tutorial/TutorialPresenter;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/util/Res;",
            "Landroid/app/Application;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;"
        }
    .end annotation

    .line 58
    new-instance v6, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;-><init>(Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Landroid/app/Application;Lcom/squareup/settings/LocalSetting;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/tutorial/TutorialPresenter;

    iget-object v1, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v2, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Application;

    iget-object v4, p0, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;->tipsDismissedSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/settings/LocalSetting;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;->newInstance(Lcom/squareup/register/tutorial/TutorialPresenter;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Landroid/app/Application;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial_Factory;->get()Lcom/squareup/invoices/tutorial/NewInvoiceFeaturesTutorial;

    move-result-object v0

    return-object v0
.end method
