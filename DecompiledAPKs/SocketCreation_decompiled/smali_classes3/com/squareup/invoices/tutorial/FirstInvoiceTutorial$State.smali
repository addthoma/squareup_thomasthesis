.class final enum Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;
.super Ljava/lang/Enum;
.source "FirstInvoiceTutorial.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

.field public static final enum MANUALLY_STARTED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

.field public static final enum NOT_STARTED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

.field public static final enum READY_TO_FINISH_QUIETLY:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

.field public static final enum READY_TO_FINISH_SEND:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

.field public static final enum RUNNING:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

.field public static final enum STOPPED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 31
    new-instance v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    const/4 v1, 0x0

    const-string v2, "NOT_STARTED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->NOT_STARTED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    .line 33
    new-instance v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    const/4 v2, 0x1

    const-string v3, "MANUALLY_STARTED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->MANUALLY_STARTED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    .line 35
    new-instance v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    const/4 v3, 0x2

    const-string v4, "RUNNING"

    invoke-direct {v0, v4, v3}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->RUNNING:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    .line 37
    new-instance v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    const/4 v4, 0x3

    const-string v5, "READY_TO_FINISH_SEND"

    invoke-direct {v0, v5, v4}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->READY_TO_FINISH_SEND:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    .line 39
    new-instance v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    const/4 v5, 0x4

    const-string v6, "READY_TO_FINISH_QUIETLY"

    invoke-direct {v0, v6, v5}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->READY_TO_FINISH_QUIETLY:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    .line 44
    new-instance v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    const/4 v6, 0x5

    const-string v7, "STOPPED"

    invoke-direct {v0, v7, v6}, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->STOPPED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    .line 29
    sget-object v7, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->NOT_STARTED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->MANUALLY_STARTED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->RUNNING:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->READY_TO_FINISH_SEND:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->READY_TO_FINISH_QUIETLY:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->STOPPED:Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->$VALUES:[Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;
    .locals 1

    .line 29
    const-class v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->$VALUES:[Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    invoke-virtual {v0}, [Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;

    return-object v0
.end method


# virtual methods
.method public varargs in([Lcom/squareup/invoices/tutorial/FirstInvoiceTutorial$State;)Z
    .locals 4

    .line 48
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-object v3, p1, v2

    if-ne p0, v3, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method
