.class public Lcom/squareup/invoices/PartialTransactionData$Factory;
.super Ljava/lang/Object;
.source "PartialTransactionData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/PartialTransactionData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPartialTransactionData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PartialTransactionData.kt\ncom/squareup/invoices/PartialTransactionData$Factory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,84:1\n959#2:85\n1360#2:86\n1429#2,3:87\n*E\n*S KotlinDebug\n*F\n+ 1 PartialTransactionData.kt\ncom/squareup/invoices/PartialTransactionData$Factory\n*L\n65#1:85\n72#1:86\n72#1,3:87\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0008\u0003\u0008\u0016\u0018\u00002\u00020\u0001B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011J\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0013J(\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u00142\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00142\u000c\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0014R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/invoices/PartialTransactionData$Factory;",
        "",
        "defaultCurrencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "res",
        "Lcom/squareup/util/Res;",
        "locale",
        "Ljava/util/Locale;",
        "(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Res;Ljava/util/Locale;)V",
        "create",
        "Lcom/squareup/invoices/PartialTransactionData;",
        "refund",
        "Lcom/squareup/protos/client/invoice/InvoiceRefund;",
        "payment",
        "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;",
        "",
        "payments",
        "refunds",
        "invoices_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dateFormat:Ljava/text/DateFormat;

.field private final defaultCurrencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final locale:Ljava/util/Locale;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Res;Ljava/util/Locale;)V
    .locals 1
    .param p3    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumFormNoYear;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/util/Res;",
            "Ljava/util/Locale;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "defaultCurrencyCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->defaultCurrencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p2, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->dateFormat:Ljava/text/DateFormat;

    iput-object p4, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->locale:Ljava/util/Locale;

    return-void
.end method

.method public static final synthetic access$getLocale$p(Lcom/squareup/invoices/PartialTransactionData$Factory;)Ljava/util/Locale;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->locale:Ljava/util/Locale;

    return-object p0
.end method


# virtual methods
.method public final create(Lcom/squareup/protos/client/invoice/InvoiceRefund;)Lcom/squareup/invoices/PartialTransactionData;
    .locals 9

    const-string v0, "refund"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;->refunded_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 52
    new-instance v8, Lcom/squareup/invoices/PartialTransactionData;

    .line 53
    iget-object v1, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->defaultCurrencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v1}, Lcom/squareup/invoices/PartialTransactionsKt;->getGlyph(Lcom/squareup/protos/client/invoice/InvoiceRefund;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    .line 54
    iget-object v1, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v1, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    const-string v0, "moneyFormatter.format(refundedAmount)"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->dateFormat:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v0, v1}, Lcom/squareup/invoices/PartialTransactionsKt;->getStatusText(Lcom/squareup/protos/client/invoice/InvoiceRefund;Ljava/text/DateFormat;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v4

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceRefund;->reason:Ljava/lang/String;

    move-object v5, p1

    check-cast v5, Ljava/lang/CharSequence;

    .line 56
    sget v6, Lcom/squareup/widgets/pos/R$drawable;->badge_refund:I

    const/4 v7, 0x0

    move-object v1, v8

    .line 52
    invoke-direct/range {v1 .. v7}, Lcom/squareup/invoices/PartialTransactionData;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v8
.end method

.method public final create(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;)Lcom/squareup/invoices/PartialTransactionData;
    .locals 9

    const-string v0, "payment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    new-instance v0, Lcom/squareup/invoices/PartialTransactionData;

    .line 45
    iget-object v1, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->defaultCurrencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {p1, v1}, Lcom/squareup/invoices/PartialTransactionsKt;->getGlyph(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    iget-object v1, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->total_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    const-string v1, "moneyFormatter.format(payment.total_amount)"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v1, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->dateFormat:Ljava/text/DateFormat;

    iget-object v4, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->locale:Ljava/util/Locale;

    iget-object v5, p0, Lcom/squareup/invoices/PartialTransactionData$Factory;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v1, v4, v5}, Lcom/squareup/invoices/PartialTransactionsKt;->getStatusText(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v4

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_note:Ljava/lang/String;

    move-object v5, p1

    check-cast v5, Ljava/lang/CharSequence;

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v8, 0x0

    move-object v1, v0

    .line 44
    invoke-direct/range {v1 .. v8}, Lcom/squareup/invoices/PartialTransactionData;-><init>(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final create(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceRefund;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/PartialTransactionData;",
            ">;"
        }
    .end annotation

    const-string v0, "payments"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refunds"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    check-cast p1, Ljava/util/Collection;

    check-cast p2, Ljava/lang/Iterable;

    invoke-static {p1, p2}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 85
    new-instance p2, Lcom/squareup/invoices/PartialTransactionData$Factory$create$$inlined$sortedByDescending$1;

    invoke-direct {p2, p0}, Lcom/squareup/invoices/PartialTransactionData$Factory$create$$inlined$sortedByDescending$1;-><init>(Lcom/squareup/invoices/PartialTransactionData$Factory;)V

    check-cast p2, Ljava/util/Comparator;

    invoke-static {p1, p2}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 86
    new-instance p2, Ljava/util/ArrayList;

    const/16 v0, 0xa

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v0

    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p2, Ljava/util/Collection;

    .line 87
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 88
    check-cast v0, Lcom/squareup/wire/Message;

    .line 74
    instance-of v1, v0, Lcom/squareup/protos/client/invoice/InvoiceRefund;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/protos/client/invoice/InvoiceRefund;

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/PartialTransactionData$Factory;->create(Lcom/squareup/protos/client/invoice/InvoiceRefund;)Lcom/squareup/invoices/PartialTransactionData;

    move-result-object v0

    goto :goto_1

    .line 75
    :cond_0
    instance-of v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/PartialTransactionData$Factory;->create(Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;)Lcom/squareup/invoices/PartialTransactionData;

    move-result-object v0

    .line 79
    :goto_1
    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 76
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 77
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Got an object that isn\'t a refund or payment. Object: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 76
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 89
    :cond_2
    check-cast p2, Ljava/util/List;

    return-object p2
.end method
