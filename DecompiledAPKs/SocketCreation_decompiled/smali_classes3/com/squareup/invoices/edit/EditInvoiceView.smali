.class public Lcom/squareup/invoices/edit/EditInvoiceView;
.super Landroid/widget/LinearLayout;
.source "EditInvoiceView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/edit/EditInvoiceView$AdditionalEmailTextWatcher;,
        Lcom/squareup/invoices/edit/EditInvoiceView$InvoiceTextWatcher;
    }
.end annotation


# static fields
.field public static final ADDITIONAL_EMAIL_RECEPIENT_LIMIT:I = 0x9


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private addItemButton:Landroid/view/View;

.field private addPaymentScheduleButton:Landroid/view/View;

.field private additionalEmailsContainer:Landroid/view/ViewGroup;

.field private allowAutomaticPaymentsRow:Lcom/squareup/ui/account/view/LineRow;

.field private buyerCofRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private cofHelper:Lcom/squareup/widgets/MessageView;

.field private customMessageRow:Lcom/squareup/ui/account/view/SmartLineRow;

.field customerEmailRow:Lcom/squareup/widgets/SelectableEditText;

.field customerNameRow:Lcom/squareup/widgets/SelectableEditText;

.field private customerRow:Lcom/squareup/ui/account/view/SmartLineRow;

.field private deliveryRow:Lcom/squareup/ui/account/view/LineRow;

.field private dueDateRow:Lcom/squareup/ui/account/view/LineRow;

.field private editPaymentScheduleButton:Landroid/view/View;

.field private enableAutomaticRemindersRow:Lcom/squareup/ui/account/view/LineRow;

.field private fileAttachmentButton:Landroid/view/View;

.field private fileAttachmentDivider:Landroid/view/View;

.field private fileAttachmentsContainer:Landroid/view/ViewGroup;

.field private fileAttachmentsTitle:Landroid/widget/TextView;

.field private frequencyRow:Lcom/squareup/ui/account/view/SmartLineRow;

.field invoiceId:Lcom/squareup/widgets/SelectableEditText;

.field private lineItemsContainer:Landroid/view/ViewGroup;

.field private lineItemsDivider:Landroid/view/View;

.field private lineItemsSection:Landroid/widget/LinearLayout;

.field private paymentScheduleContainer:Landroid/view/ViewGroup;

.field private paymentScheduleDivider:Landroid/view/View;

.field private paymentScheduleList:Landroid/view/ViewGroup;

.field presenter:Lcom/squareup/invoices/edit/EditInvoicePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private previewButton:Landroid/view/View;

.field private progressBar:Landroid/view/View;

.field private recurringPeriodHelper:Lcom/squareup/widgets/MessageView;

.field private requestDepositButton:Landroid/view/View;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private saveAsDraftButton:Landroid/widget/TextView;

.field private sendDateRow:Lcom/squareup/ui/account/view/LineRow;

.field private shippingAddressRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private final shortAnimTimeMs:I

.field private tippableRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field title:Lcom/squareup/widgets/SelectableEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 101
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 102
    const-class p2, Lcom/squareup/invoices/edit/EditInvoiceScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/edit/EditInvoiceScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/invoices/edit/EditInvoiceScreen$Component;->inject(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    .line 104
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->shortAnimTimeMs:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/invoices/edit/EditInvoiceView;Ljava/lang/String;Z)V
    .locals 0

    .line 52
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceView;->addAdditionalEmailRowViewIfPossible(Ljava/lang/String;Z)V

    return-void
.end method

.method private addAdditionalEmailRowIfUnderLimit(Ljava/lang/String;Z)V
    .locals 2

    .line 620
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceView;->getAdditionalEmailsTrimmed()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    .line 621
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->additionalEmailsContainer:Landroid/view/ViewGroup;

    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceView;->inflateAdditionalEmailRowView(Ljava/lang/String;Z)Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private addAdditionalEmailRowViewIfPossible(Ljava/lang/String;Z)V
    .locals 1

    if-eqz p2, :cond_0

    .line 611
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p2, 0x0

    .line 612
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceView;->addAdditionalEmailRowIfUnderLimit(Ljava/lang/String;Z)V

    const/4 p1, 0x1

    const-string p2, ""

    .line 613
    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->addAdditionalEmailRowIfUnderLimit(Ljava/lang/String;Z)V

    goto :goto_0

    .line 615
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceView;->addAdditionalEmailRowIfUnderLimit(Ljava/lang/String;Z)V

    :goto_0
    return-void
.end method

.method private createPaymentScheduleRow(ILcom/squareup/invoices/PaymentRequestData;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 5

    .line 583
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleContainer:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v0

    .line 585
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_payment_schedule_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 586
    invoke-virtual {p2}, Lcom/squareup/invoices/PaymentRequestData;->getFormattedAmount()Ljava/lang/CharSequence;

    move-result-object v2

    .line 587
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v3, v4}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v3

    .line 586
    invoke-static {v2, v3}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v2

    const-string v3, "amount"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 589
    invoke-virtual {p2}, Lcom/squareup/invoices/PaymentRequestData;->getRequestType()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v3, v4}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v2

    const-string v3, "request_type"

    .line 588
    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 590
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 591
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 592
    invoke-virtual {p2}, Lcom/squareup/invoices/PaymentRequestData;->getFormattedDueDate()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 593
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->edit:I

    .line 594
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 595
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v2, v3}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v2

    .line 594
    invoke-static {v1, v2}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v1

    .line 593
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 597
    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    const/4 v1, 0x1

    .line 599
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleEnabled(Z)V

    .line 600
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 601
    invoke-virtual {p2}, Lcom/squareup/invoices/PaymentRequestData;->isEditable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 602
    invoke-virtual {p2}, Lcom/squareup/invoices/PaymentRequestData;->isEditable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueSubtitleVisible(Z)V

    .line 604
    invoke-virtual {p2}, Lcom/squareup/invoices/PaymentRequestData;->isEditable()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 605
    new-instance p2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceView$pR_HtpUwsjcq4c3fObZi-7pTdxs;

    invoke-direct {p2, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceView$pR_HtpUwsjcq4c3fObZi-7pTdxs;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;I)V

    invoke-virtual {v0, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-object v0
.end method

.method private createPaymentScheduleRow(Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 2

    .line 573
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleContainer:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v0

    .line 575
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 576
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;->getSubtitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    const/4 p1, 0x1

    .line 577
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleEnabled(Z)V

    .line 578
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    return-object v0
.end method

.method private enableBuyerCofRow(Z)V
    .locals 1

    .line 412
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->buyerCofRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setEnabled(Z)V

    return-void
.end method

.method private hideLineItemsSection()V
    .locals 2

    .line 704
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->lineItemsDivider:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 705
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->lineItemsSection:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method private inflateAdditionalEmailRowView(Ljava/lang/String;Z)Lcom/squareup/widgets/SelectableAutoCompleteEditText;
    .locals 4

    .line 675
    sget v0, Lcom/squareup/features/invoices/R$layout;->pay_invoice_email_row_view_no_plus:I

    .line 676
    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    .line 677
    new-instance v1, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 678
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setText(Ljava/lang/CharSequence;)V

    .line 679
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_hint_recipient_additional_email:I

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setHint(I)V

    .line 680
    new-instance p1, Lcom/squareup/invoices/edit/EditInvoiceView$AdditionalEmailTextWatcher;

    xor-int/lit8 p2, p2, 0x1

    invoke-direct {p1, p0, p2}, Lcom/squareup/invoices/edit/EditInvoiceView$AdditionalEmailTextWatcher;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;Z)V

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-object v0
.end method

.method private lastEmailEmpty()Z
    .locals 2

    .line 287
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->additionalEmailsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 288
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->additionalEmailsContainer:Landroid/view/ViewGroup;

    .line 289
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 293
    :cond_0
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->getValue(Landroid/widget/EditText;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private setAutomaticPaymentValue(Ljava/lang/Boolean;)V
    .locals 1

    .line 395
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->allowAutomaticPaymentsRow:Lcom/squareup/ui/account/view/LineRow;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_automatic_payment_on:I

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_automatic_payment_off:I

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(I)V

    return-void
.end method

.method private setCofHelper(ILjava/lang/CharSequence;)V
    .locals 1

    .line 524
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->cofHelper:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 525
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->cofHelper:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private showAutomaticPaymentsRow(Z)V
    .locals 1

    .line 391
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->allowAutomaticPaymentsRow:Lcom/squareup/ui/account/view/LineRow;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public addAdditionalFileAttachmentRowView(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V
    .locals 3

    .line 627
    new-instance v0, Lcom/squareup/ui/account/view/LineRow$Builder;

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 628
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    .line 629
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lcom/squareup/util/Files;->readableFileSizeFromBytes(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setValue(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    .line 630
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setBackground(I)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 631
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object v0

    .line 632
    invoke-virtual {v0}, Lcom/squareup/ui/account/view/LineRow$Builder;->build()Lcom/squareup/ui/account/view/LineRow;

    move-result-object v0

    .line 634
    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceView$16;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceView$16;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 640
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentsContainer:Landroid/view/ViewGroup;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method addInvoiceTextWatchers()V
    .locals 2

    .line 275
    new-instance v0, Lcom/squareup/invoices/edit/EditInvoiceView$InvoiceTextWatcher;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$InvoiceTextWatcher;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    .line 276
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerEmailRow:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 277
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerNameRow:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 278
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->title:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 279
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->invoiceId:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 281
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceView;->lastEmailEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    const-string v1, ""

    .line 282
    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->addAdditionalEmailRowViewIfPossible(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public addLineItemRow(Lcom/squareup/ui/cart/CartEntryView;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 687
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->lineItemsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 689
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_ultra_light_gray_when_pressed:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 688
    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/CartEntryView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public addLineItemRows(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryView;",
            ">;)V"
        }
    .end annotation

    .line 694
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartEntryView;

    .line 695
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->addLineItemRow(Lcom/squareup/ui/cart/CartEntryView;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method changeCofHelper(ZLjava/lang/Boolean;Z)V
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    .line 509
    new-instance p1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    if-eqz p3, :cond_0

    sget p3, Lcom/squareup/features/invoices/R$string;->invoice_automatic_payment_auto_enable_buyer_cof:I

    goto :goto_0

    :cond_0
    sget p3, Lcom/squareup/features/invoices/R$string;->invoice_save_cof_recurring_helper:I

    :goto_0
    const-string v1, "learn_more"

    .line 510
    invoke-virtual {p1, p3, v1}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget p3, Lcom/squareup/common/invoices/R$string;->invoice_automatic_payment_url:I

    .line 514
    invoke-virtual {p1, p3}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget p3, Lcom/squareup/common/strings/R$string;->learn_more:I

    .line 515
    invoke-virtual {p1, p3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 516
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    .line 517
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v0, 0x8

    :goto_1
    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->setCofHelper(ILjava/lang/CharSequence;)V

    goto :goto_2

    .line 519
    :cond_2
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/features/invoices/R$string;->invoice_save_cof_helper:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->setCofHelper(ILjava/lang/CharSequence;)V

    :goto_2
    return-void
.end method

.method public clearFileAttachmentContainer()V
    .locals 1

    .line 644
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method public disableCustomerRow()V
    .locals 2

    .line 369
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerRow:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setEnabled(Z)V

    .line 370
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerRow:Lcom/squareup/ui/account/view/SmartLineRow;

    sget-object v1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    return-void
.end method

.method public disableDeliveryRow()V
    .locals 2

    .line 322
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->deliveryRow:Lcom/squareup/ui/account/view/LineRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setEnabled(Z)V

    .line 323
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->deliveryRow:Lcom/squareup/ui/account/view/LineRow;

    sget-object v1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    return-void
.end method

.method public disableInvoiceId()V
    .locals 2

    .line 652
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->invoiceId:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setEnabled(Z)V

    return-void
.end method

.method public enableAttachmentButton(Z)V
    .locals 1

    .line 648
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 297
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getAdditionalEmailsTrimmed()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 656
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->additionalEmailsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 657
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    .line 659
    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->additionalEmailsContainer:Landroid/view/ViewGroup;

    .line 660
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    .line 661
    invoke-virtual {p0, v3}, Lcom/squareup/invoices/edit/EditInvoiceView;->getValue(Landroid/widget/EditText;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 662
    invoke-static {v3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 663
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method getValue(Landroid/widget/EditText;)Ljava/lang/String;
    .locals 0

    .line 670
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public hideAdditionalEmailRows()V
    .locals 2

    .line 464
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->additionalEmailsContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public hideCustomerRow()V
    .locals 2

    .line 374
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerRow:Lcom/squareup/ui/account/view/SmartLineRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setVisibility(I)V

    return-void
.end method

.method public hideDraftButton()V
    .locals 2

    .line 452
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->saveAsDraftButton:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public hideDueDateRow()V
    .locals 2

    .line 336
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->dueDateRow:Lcom/squareup/ui/account/view/LineRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public hideFileAttachmentTitle()V
    .locals 2

    .line 428
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentsTitle:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public hideSendDateRow()V
    .locals 2

    .line 360
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->sendDateRow:Lcom/squareup/ui/account/view/LineRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$createPaymentScheduleRow$3$EditInvoiceView(ILandroid/view/View;)V
    .locals 0

    .line 605
    iget-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->presenter:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    invoke-virtual {p2, p1}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->onPaymentRequestClicked(I)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$EditInvoiceView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 234
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->presenter:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->onTippableRowCheckedChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$EditInvoiceView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 251
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->presenter:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->onShippingAddressChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$2$EditInvoiceView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 253
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->presenter:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->onBuyerCofChanged(Z)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 264
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 265
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->presenter:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 259
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->presenter:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->dropView(Ljava/lang/Object;)V

    .line 260
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 109
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 110
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 111
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_customer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerRow:Lcom/squareup/ui/account/view/SmartLineRow;

    .line 112
    sget v0, Lcom/squareup/features/invoices/R$id;->buyer_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerNameRow:Lcom/squareup/widgets/SelectableEditText;

    .line 113
    sget v0, Lcom/squareup/features/invoices/R$id;->buyer_email:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerEmailRow:Lcom/squareup/widgets/SelectableEditText;

    .line 114
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_delivery:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->deliveryRow:Lcom/squareup/ui/account/view/LineRow;

    .line 115
    sget v0, Lcom/squareup/features/invoices/R$id;->edit_invoice_frequency:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->frequencyRow:Lcom/squareup/ui/account/view/SmartLineRow;

    .line 116
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_due_date:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->dueDateRow:Lcom/squareup/ui/account/view/LineRow;

    .line 117
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_send_date:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->sendDateRow:Lcom/squareup/ui/account/view/LineRow;

    .line 118
    sget v0, Lcom/squareup/features/invoices/R$id;->allow_automatic_payment:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->allowAutomaticPaymentsRow:Lcom/squareup/ui/account/view/LineRow;

    .line 119
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->title:Lcom/squareup/widgets/SelectableEditText;

    .line 120
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_number:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->invoiceId:Lcom/squareup/widgets/SelectableEditText;

    .line 121
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_custom_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customMessageRow:Lcom/squareup/ui/account/view/SmartLineRow;

    .line 122
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_request_shipping_address:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->shippingAddressRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 123
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_tippable:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->tippableRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 124
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_allow_buyer_save_cof:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->buyerCofRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 125
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_additional_emails_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->additionalEmailsContainer:Landroid/view/ViewGroup;

    .line 126
    sget v0, Lcom/squareup/features/invoices/R$id;->edit_invoice_file_attachments_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentsContainer:Landroid/view/ViewGroup;

    .line 127
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_preview_action:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->previewButton:Landroid/view/View;

    .line 128
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_edit_action:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->saveAsDraftButton:Landroid/widget/TextView;

    .line 129
    sget v0, Lcom/squareup/features/invoices/R$id;->edit_invoice_file_attachment_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentsTitle:Landroid/widget/TextView;

    .line 130
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_edit_add_line_item:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->addItemButton:Landroid/view/View;

    .line 131
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_edit_request_deposit:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->requestDepositButton:Landroid/view/View;

    .line 132
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_add_payment_schedule_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->addPaymentScheduleButton:Landroid/view/View;

    .line 133
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_edit_line_items_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->lineItemsContainer:Landroid/view/ViewGroup;

    .line 134
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_attach_file:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentButton:Landroid/view/View;

    .line 135
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_attach_file_divider:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentDivider:Landroid/view/View;

    .line 136
    sget v0, Lcom/squareup/features/invoices/R$id;->edit_invoice_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->progressBar:Landroid/view/View;

    .line 137
    sget v0, Lcom/squareup/features/invoices/R$id;->allow_save_cof_helper:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->cofHelper:Lcom/squareup/widgets/MessageView;

    .line 138
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_recurring_period_helper:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->recurringPeriodHelper:Lcom/squareup/widgets/MessageView;

    .line 139
    sget v0, Lcom/squareup/features/invoices/R$id;->enable_automatic_reminders:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->enableAutomaticRemindersRow:Lcom/squareup/ui/account/view/LineRow;

    .line 140
    sget v0, Lcom/squareup/features/invoices/R$id;->line_items_section:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->lineItemsSection:Landroid/widget/LinearLayout;

    .line 141
    sget v0, Lcom/squareup/features/invoices/R$id;->line_items_divider:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->lineItemsDivider:Landroid/view/View;

    .line 142
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_payment_schedule_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleContainer:Landroid/view/ViewGroup;

    .line 143
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_payment_schedule_divider:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleDivider:Landroid/view/View;

    .line 144
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_payment_schedule_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleList:Landroid/view/ViewGroup;

    .line 145
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_edit_payment_schedule_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->editPaymentScheduleButton:Landroid/view/View;

    .line 147
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->presenter:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->inTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceView;->hideLineItemsSection()V

    goto :goto_0

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->addItemButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceView$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$1;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->requestDepositButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceView$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$2;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->addPaymentScheduleButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceView$3;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$3;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->editPaymentScheduleButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceView$4;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$4;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->cofHelper:Lcom/squareup/widgets/MessageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setEqualizeLines(Z)V

    .line 178
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setPreserveValueText(Z)V

    .line 179
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerRow:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 182
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerRow:Lcom/squareup/ui/account/view/SmartLineRow;

    sget v3, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleColor(I)V

    .line 183
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerRow:Lcom/squareup/ui/account/view/SmartLineRow;

    sget v3, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 184
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerRow:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v3, Lcom/squareup/invoices/edit/EditInvoiceView$5;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$5;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->deliveryRow:Lcom/squareup/ui/account/view/LineRow;

    new-instance v3, Lcom/squareup/invoices/edit/EditInvoiceView$6;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$6;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->frequencyRow:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v3, Lcom/squareup/invoices/edit/EditInvoiceView$7;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$7;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->dueDateRow:Lcom/squareup/ui/account/view/LineRow;

    new-instance v3, Lcom/squareup/invoices/edit/EditInvoiceView$8;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$8;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->sendDateRow:Lcom/squareup/ui/account/view/LineRow;

    new-instance v3, Lcom/squareup/invoices/edit/EditInvoiceView$9;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$9;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->allowAutomaticPaymentsRow:Lcom/squareup/ui/account/view/LineRow;

    new-instance v3, Lcom/squareup/invoices/edit/EditInvoiceView$10;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$10;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->enableAutomaticRemindersRow:Lcom/squareup/ui/account/view/LineRow;

    new-instance v3, Lcom/squareup/invoices/edit/EditInvoiceView$11;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$11;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v3}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customMessageRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setPreserveValueText(Z)V

    .line 226
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customMessageRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 227
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customMessageRow:Lcom/squareup/ui/account/view/SmartLineRow;

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 228
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customMessageRow:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceView$12;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$12;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->tippableRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceView$zRDMx-xQsAL7D02uhfV_CS0GklU;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceView$zRDMx-xQsAL7D02uhfV_CS0GklU;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->saveAsDraftButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceView$13;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$13;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->previewButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceView$14;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$14;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceView$15;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/EditInvoiceView$15;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 250
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->shippingAddressRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceView$ckdIIwYzQIi2xuAxomJqpNjmL-M;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceView$ckdIIwYzQIi2xuAxomJqpNjmL-M;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 252
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->buyerCofRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceView$tmF9g9BAD3w4OpBigzwh6VgG8kw;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceView$tmF9g9BAD3w4OpBigzwh6VgG8kw;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 255
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->presenter:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method resetLineItems()V
    .locals 1

    .line 700
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->lineItemsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method restoreAdditionalEmailRows(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 468
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->additionalEmailsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 469
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 470
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 471
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    if-ne v1, v3, :cond_0

    goto :goto_1

    :cond_0
    const/4 v4, 0x0

    .line 470
    :goto_1
    invoke-direct {p0, v2, v4}, Lcom/squareup/invoices/edit/EditInvoiceView;->addAdditionalEmailRowViewIfPossible(Ljava/lang/String;Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method setAutomaticPaymentViews(ZZ)V
    .locals 0

    .line 501
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->showAutomaticPaymentsRow(Z)V

    .line 502
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->setAutomaticPaymentValue(Ljava/lang/Boolean;)V

    xor-int/lit8 p1, p2, 0x1

    .line 503
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceView;->enableBuyerCofRow(Z)V

    return-void
.end method

.method setAutomaticReminderValue(Z)V
    .locals 2

    .line 404
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->enableAutomaticRemindersRow:Lcom/squareup/ui/account/view/LineRow;

    if-eqz p1, :cond_0

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_automatic_reminders_on:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_automatic_reminders_off:I

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setValue(I)V

    .line 408
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->enableAutomaticRemindersRow:Lcom/squareup/ui/account/view/LineRow;

    if-eqz p1, :cond_1

    const-string p1, "on"

    goto :goto_1

    :cond_1
    const-string p1, "off"

    :goto_1
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method public setCustomerInfo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 364
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 365
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setCustomerInfoTextRows(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .line 386
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerNameRow:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 387
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerEmailRow:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDraftText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 448
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->saveAsDraftButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDueDateTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 327
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->dueDateRow:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setDueDateValue(Ljava/lang/CharSequence;)V
    .locals 2

    .line 331
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->dueDateRow:Lcom/squareup/ui/account/view/LineRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    .line 332
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->dueDateRow:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->previewButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 270
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->saveAsDraftButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 271
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method

.method setFrequencyLabel(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Ljava/text/DateFormat;)V
    .locals 4

    .line 480
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->frequencyRow:Lcom/squareup/ui/account/view/SmartLineRow;

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 481
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->frequencyRow:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    if-eqz p1, :cond_0

    .line 483
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->prettyString(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    .line 484
    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_frequency_repeat_every:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "duration"

    .line 485
    invoke-virtual {v2, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 486
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 487
    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->frequencyRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v2, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 489
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->frequencyRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 490
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_frequency_end:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 491
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getRecurrenceEnd()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v2, p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;->prettyString(Lcom/squareup/util/Res;Ljava/text/DateFormat;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "count_or_date"

    invoke-virtual {v0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 492
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 493
    iget-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->frequencyRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueSubtitleText(Ljava/lang/CharSequence;)V

    .line 494
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->frequencyRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueSubtitleVisible(Z)V

    goto :goto_0

    .line 496
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->frequencyRow:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_frequency_one_time:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method public setInvoiceId(Ljava/lang/CharSequence;)V
    .locals 1

    .line 436
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->invoiceId:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setMessageText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 444
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customMessageRow:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setPaymentMethodValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 318
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->deliveryRow:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setRecurringPeriodHelper(ILjava/lang/CharSequence;)V
    .locals 1

    .line 529
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->recurringPeriodHelper:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 530
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->recurringPeriodHelper:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setSendDateLabelToCharge()V
    .locals 3

    .line 344
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->sendDateRow:Lcom/squareup/ui/account/view/LineRow;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_charge_cof_action:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSendDateLabelToNextInvoice()V
    .locals 3

    .line 356
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->sendDateRow:Lcom/squareup/ui/account/view/LineRow;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_next_invoice:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setSendDateLabelToSend()V
    .locals 3

    .line 348
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->sendDateRow:Lcom/squareup/ui/account/view/LineRow;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/utilities/R$string;->send:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSendDateLabelToStart()V
    .locals 3

    .line 352
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->sendDateRow:Lcom/squareup/ui/account/view/LineRow;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_start:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSendDateValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->sendDateRow:Lcom/squareup/ui/account/view/LineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 432
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->title:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showAddPaymentScheduleButton()V
    .locals 2

    .line 544
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleDivider:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 545
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 546
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->addPaymentScheduleButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method showAutomaticReminderRow(Z)V
    .locals 1

    .line 400
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->enableAutomaticRemindersRow:Lcom/squareup/ui/account/view/LineRow;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/LineRow;->setVisibility(I)V

    return-void
.end method

.method public showCustomerEmailRow()V
    .locals 2

    .line 382
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerEmailRow:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setVisibility(I)V

    return-void
.end method

.method public showCustomerNameRow()V
    .locals 2

    .line 378
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customerNameRow:Lcom/squareup/widgets/SelectableEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->setVisibility(I)V

    return-void
.end method

.method showFileAttachmentButton()V
    .locals 2

    .line 416
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method showFileAttachmentDivider()V
    .locals 2

    .line 420
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentDivider:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method showFileAttachmentTitle()V
    .locals 2

    .line 424
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->fileAttachmentsTitle:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method showFrequencyRow(Z)V
    .locals 1

    .line 476
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->frequencyRow:Lcom/squareup/ui/account/view/SmartLineRow;

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setVisibility(I)V

    return-void
.end method

.method public showPaymentRequestList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/PaymentRequestData;",
            ">;)V"
        }
    .end annotation

    .line 562
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleDivider:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 563
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 564
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleList:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 566
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 567
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/PaymentRequestData;

    .line 568
    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleList:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->createPaymentScheduleRow(ILcom/squareup/invoices/PaymentRequestData;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public showPaymentSchedule(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;",
            ">;)V"
        }
    .end annotation

    .line 550
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleDivider:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 551
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 552
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleList:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 553
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->editPaymentScheduleButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 555
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 556
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;

    .line 557
    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleList:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceView;->createPaymentScheduleRow(Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method showPreviewButton(Z)V
    .locals 1

    .line 534
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->previewButton:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 457
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 459
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method

.method showRequestDepositButton()V
    .locals 2

    .line 538
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleDivider:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 539
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->paymentScheduleContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 540
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->requestDepositButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method showViewMessageRow()V
    .locals 2

    .line 440
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->customMessageRow:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setVisibility(I)V

    return-void
.end method

.method public updateBuyerCofRow(ILjava/lang/Boolean;)V
    .locals 1

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    .line 312
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    .line 313
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->buyerCofRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setVisibility(I)V

    .line 314
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->buyerCofRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method public updateShippingAddressRow(ILjava/lang/Boolean;)V
    .locals 1

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    .line 306
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    .line 307
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->shippingAddressRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setVisibility(I)V

    .line 308
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->shippingAddressRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method public updateTippableRow(IZ)V
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->tippableRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setVisibility(I)V

    .line 302
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView;->tippableRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method
