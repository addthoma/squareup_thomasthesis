.class public Lcom/squareup/invoices/edit/PaymentMethodUtility;
.super Ljava/lang/Object;
.source "PaymentMethodUtility.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPaymentMethodString(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Lcom/squareup/util/Res;Lcom/squareup/protos/client/instruments/InstrumentSummary;)Ljava/lang/String;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/invoices/edit/PaymentMethodUtility$1;->$SwitchMap$com$squareup$protos$client$invoice$Invoice$PaymentMethod:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    .line 45
    sget p0, Lcom/squareup/features/invoices/R$string;->invoice_delivery_method_select:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    if-nez p2, :cond_1

    .line 35
    sget p0, Lcom/squareup/features/invoices/R$string;->invoice_delivery_method_cof_default:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 37
    :cond_1
    sget p0, Lcom/squareup/features/invoices/R$string;->invoice_charge_cof_concise:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    iget-object p1, p2, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    iget-object p1, p1, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    .line 38
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "card_brand"

    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    iget-object p1, p2, Lcom/squareup/protos/client/instruments/InstrumentSummary;->card:Lcom/squareup/protos/client/instruments/CardSummary;

    iget-object p1, p1, Lcom/squareup/protos/client/instruments/CardSummary;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/CardTender$Card;->pan_suffix:Ljava/lang/String;

    const-string p2, "pan"

    .line 39
    invoke-virtual {p0, p2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 40
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 32
    :cond_2
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_delivery_method_email:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 30
    :cond_3
    sget p0, Lcom/squareup/common/invoices/R$string;->invoice_delivery_method_manual:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static isValidPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Z
    .locals 2

    .line 11
    sget-object v0, Lcom/squareup/invoices/edit/PaymentMethodUtility$1;->$SwitchMap$com$squareup$protos$client$invoice$Invoice$PaymentMethod:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v1, 0x2

    if-eq p0, v1, :cond_1

    const/4 v1, 0x3

    if-eq p0, v1, :cond_1

    const/4 v0, 0x4

    const/4 v1, 0x0

    if-eq p0, v0, :cond_0

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    :cond_0
    return v1

    :cond_1
    return v0
.end method
