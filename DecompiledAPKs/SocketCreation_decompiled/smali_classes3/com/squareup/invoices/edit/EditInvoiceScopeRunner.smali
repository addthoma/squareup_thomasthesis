.class public Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunner.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/orderentry/KeypadEntryScreen$Runner;
.implements Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;
.implements Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;
.implements Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Runner;
.implements Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;,
        Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$UploadResult;,
        Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;,
        Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;
    }
.end annotation


# static fields
.field private static final DEFAULT_REMAINDER_DUE:J = 0x1eL

.field private static final EMPTY_INSTRUMENTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;"
        }
    .end annotation
.end field

.field private static final JPEG_MEDIA_TYPE:Lokhttp3/MediaType;

.field private static final KEYPAD_INFO_KEY:Ljava/lang/String; = "keypadInfo"

.field private static final RECURRING_BYMONTHDAY_LIMIT:I = 0x1c

.field private static final SAVED_BY_PREVIEW_KEY:Ljava/lang/String; = "savedByPreviewOrUpload"

.field private static final WORKING_INVOICE_KEY:Ljava/lang/String; = "workingInvoice"


# instance fields
.field private final adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final busy:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final busyLoadingInstruments:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field private final chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

.field private final chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

.field private final contactIdForInstruments:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private customAmountCartItem:Lcom/squareup/checkout/CartItem;

.field private final customAmountCartItemBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end field

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

.field private editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final features:Lcom/squareup/settings/server/Features;

.field private featuresTutorialRunner:Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;

.field private fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;>;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private hasValidationError:Z

.field private initialInvoice:Lcom/squareup/protos/client/invoice/Invoice;

.field private final instrumentsForContact:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;"
        }
    .end annotation
.end field

.field private final invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

.field private final invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

.field private final invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

.field private final invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

.field private keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

.field private latestTenderErrorDescription:Ljava/lang/CharSequence;

.field private latestTenderErrorTitle:Ljava/lang/CharSequence;

.field private final onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

.field private order:Lcom/squareup/payment/Order;

.field private path:Lcom/squareup/invoices/edit/EditInvoiceScope;

.field private final previewScreenDataFactory:Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$Factory;

.field private final recurrenceRule:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;"
        }
    .end annotation
.end field

.field private final remindersInfoFactory:Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private savedByPreviewOrUpload:Z

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final setupGuideDialogScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final setupGuideDialogScreenDataFactory:Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

.field private final shareLinkMessageFactory:Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;

.field private final shorterMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final standardReceiver:Lcom/squareup/receiving/StandardReceiver;

.field private final subscriptions:Lrx/subscriptions/CompositeSubscription;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final uploadResult:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$UploadResult;",
            ">;"
        }
    .end annotation
.end field

.field private workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

.field private final workingDiscountBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;"
        }
    .end annotation
.end field

.field private workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

.field private workingItem:Lcom/squareup/configure/item/WorkingItem;

.field private final workingItemBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "image/jpeg"

    .line 233
    invoke-static {v0}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->JPEG_MEDIA_TYPE:Lokhttp3/MediaType;

    .line 235
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->EMPTY_INSTRUMENTS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/time/CurrentTime;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/BundleKey;Lcom/squareup/payment/Transaction;Lcom/squareup/analytics/Analytics;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/text/Formatter;Lcom/squareup/settings/server/Features;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$Factory;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/invoices/image/InvoiceFileHelper;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingDiscount;",
            ">;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/checkout/CartItem;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            "Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
            "Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;",
            "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$Factory;",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;",
            "Lcom/squareup/receiving/StandardReceiver;",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            "Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    .line 288
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-boolean v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->hasValidationError:Z

    .line 289
    iput-boolean v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->savedByPreviewOrUpload:Z

    .line 291
    invoke-static {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 292
    new-instance v1, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v1}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    .line 293
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    .line 295
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->uploadResult:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 296
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->recurrenceRule:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 298
    invoke-static {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busyLoadingInstruments:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 299
    sget-object v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->EMPTY_INSTRUMENTS:Ljava/util/List;

    .line 300
    invoke-static {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->instrumentsForContact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 302
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 303
    invoke-static {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 304
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->contactIdForInstruments:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 308
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setupGuideDialogScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-object v1, p1

    .line 331
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    move-object v1, p2

    .line 332
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    move-object v1, p3

    .line 333
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    move-object v1, p4

    .line 334
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p5

    .line 335
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currentTime:Lcom/squareup/time/CurrentTime;

    move-object v1, p6

    .line 336
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItemBundleKey:Lcom/squareup/BundleKey;

    move-object v1, p7

    .line 337
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingDiscountBundleKey:Lcom/squareup/BundleKey;

    move-object v1, p8

    .line 338
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->customAmountCartItemBundleKey:Lcom/squareup/BundleKey;

    move-object v1, p9

    .line 339
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p10

    .line 340
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p11

    .line 341
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    move-object v1, p12

    .line 342
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->shorterMoneyFormatter:Lcom/squareup/text/Formatter;

    move-object/from16 v1, p13

    .line 343
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p14

    .line 344
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    move-object/from16 v1, p15

    .line 345
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    move-object/from16 v1, p16

    .line 346
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    move-object/from16 v1, p17

    .line 347
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object/from16 v1, p18

    .line 348
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    move-object/from16 v1, p19

    .line 349
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    move-object/from16 v1, p20

    .line 350
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->featuresTutorialRunner:Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;

    move-object/from16 v1, p21

    .line 351
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

    move-object/from16 v1, p22

    .line 352
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->shareLinkMessageFactory:Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;

    move-object/from16 v1, p23

    .line 353
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->previewScreenDataFactory:Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$Factory;

    move-object/from16 v1, p24

    .line 354
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    move-object/from16 v1, p25

    .line 355
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    move-object/from16 v1, p26

    .line 356
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setupGuideDialogScreenDataFactory:Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    move-object/from16 v1, p27

    .line 357
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    move-object/from16 v1, p28

    .line 358
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    move-object/from16 v1, p29

    .line 359
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->remindersInfoFactory:Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

    move-object/from16 v1, p30

    .line 360
    iput-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)Lcom/squareup/util/Res;
    .locals 0

    .line 225
    iget-object p0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)Lcom/squareup/invoices/ClientInvoiceServiceHelper;
    .locals 0

    .line 225
    iget-object p0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    return-object p0
.end method

.method private addFileAttachment(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V
    .locals 1

    .line 1492
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1493
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1494
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private busyLoadingInstruments()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 650
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busyLoadingInstruments:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method private canBeEndOfMonth(Lcom/squareup/protos/common/time/YearMonthDay;)Z
    .locals 2

    .line 796
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {p1, v0}, Lcom/squareup/invoices/InvoiceDatesKt;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z

    move-result v0

    const/16 v1, 0x1c

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 797
    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v0, v1, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    iget-object p1, p1, Lcom/squareup/protos/common/time/YearMonthDay;->day_of_month:Ljava/lang/Integer;

    .line 798
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-le p1, v1, :cond_2

    :cond_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private checkForOverlappingPaymentRequestDates()Z
    .locals 7

    .line 1930
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PAYMENT_SCHEDULE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 1932
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    .line 1933
    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->getRemainderPaymentRequest(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1934
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 1935
    iget-object v5, v4, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v6, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-ne v5, v6, :cond_2

    goto :goto_0

    .line 1938
    :cond_2
    iget-object v4, v4, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v6, v4, v2

    if-ltz v6, :cond_1

    const/4 v0, 0x1

    return v0

    :cond_3
    return v1
.end method

.method private clearTransactionPayment()V
    .locals 2

    .line 1664
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->dropPayment(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    return-void
.end method

.method private deleteDraft(Z)V
    .locals 3

    .line 1077
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v1, v2}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->deleteDraft(Lcom/squareup/protos/client/IdPair;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$fadL62_8Pc6j1BaqrYz3a-hcpsU;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$fadL62_8Pc6j1BaqrYz3a-hcpsU;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 1078
    invoke-virtual {v1, v2}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$JYLkP9YYaG5HcpFpJt8FmPfIRZY;

    invoke-direct {v2, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$JYLkP9YYaG5HcpFpJt8FmPfIRZY;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Z)V

    .line 1081
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 1077
    invoke-virtual {v0, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private deleteDraftSeries()V
    .locals 3

    .line 1092
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v1, v2}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->deleteDraftSeries(Lcom/squareup/protos/client/IdPair;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$YPpQir52QHd_Wm0UqZyC610EtxY;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$YPpQir52QHd_Wm0UqZyC610EtxY;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 1093
    invoke-virtual {v1, v2}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$DP3ObgrCRujPNZrFdpDd8PeuxQQ;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$DP3ObgrCRujPNZrFdpDd8PeuxQQ;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 1096
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    .line 1092
    invoke-virtual {v0, v1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private finish()V
    .locals 1

    const/4 v0, 0x0

    .line 1559
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setWorkingInvoice(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    .line 1560
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method private finishEditingCart()V
    .locals 3

    .line 1583
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->invalidate()V

    .line 1584
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/Order;->getCartProtoForInvoice(Lcom/squareup/protos/common/Money;Z)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v0

    .line 1585
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setCart(Lcom/squareup/protos/client/bills/Cart;)V

    .line 1586
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    invoke-interface {v0, v2}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->setCreatingCustomAmount(Z)V

    return-void
.end method

.method private getLimitText(JI)Ljava/lang/String;
    .locals 2

    .line 1564
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->shorterMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    .line 1565
    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v1

    invoke-static {p1, p2, v1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 1566
    iget-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    const-string p3, "amount"

    .line 1567
    invoke-virtual {p2, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 1568
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 1569
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getTotalSizeBytesUploaded()J
    .locals 5

    .line 1524
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1525
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 1526
    iget-object v3, v3, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-long v3, v3

    add-long/2addr v1, v3

    goto :goto_0

    :cond_0
    return-wide v1
.end method

.method private getWorkingInvoiceToSend()Lcom/squareup/protos/client/invoice/Invoice;
    .locals 4

    .line 1211
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice;->newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 1213
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v1, v2}, Lcom/squareup/invoices/InvoiceDatesKt;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 1214
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 1217
    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->noServerId(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1218
    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->withClientId(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 1221
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1222
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->attachment(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 1224
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v1

    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v1, v2, :cond_2

    .line 1225
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->additional_recipient_email:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1228
    :cond_2
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isCardOnFile()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->sanitizeReminders(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/settings/server/Features;ZZ)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 1230
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    return-object v0
.end method

.method private goBackToEditInvoice()V
    .locals 3

    .line 1656
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceScreen;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/edit/EditInvoiceScreen;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 8

    .line 1693
    iput-object p3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->latestTenderErrorTitle:Ljava/lang/CharSequence;

    .line 1694
    iput-object p4, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->latestTenderErrorDescription:Ljava/lang/CharSequence;

    .line 1695
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->inTender()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 1697
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->populateInvoicePayment(Lcom/squareup/invoices/edit/InvoiceAction;)V

    .line 1698
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    const/4 p2, 0x0

    invoke-interface {p1, p2}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->startBuyerFlowRecreatingSellerFlow(Z)V

    goto :goto_0

    .line 1700
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/invoices/edit/TenderInvoiceSentErrorDialog;

    iget-object p3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {p2, p3}, Lcom/squareup/invoices/edit/TenderInvoiceSentErrorDialog;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 1703
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v7, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;

    .line 1704
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getPath()Lcom/squareup/invoices/edit/EditInvoiceScope;

    move-result-object v2

    move-object v1, v7

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1703
    invoke-virtual {v0, v7}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private goToInvoicePreviewScreen()V
    .locals 3

    .line 1709
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private handleAttachmentResult(Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;)V
    .locals 1

    .line 1806
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Uploaded;

    if-eqz v0, :cond_0

    .line 1807
    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Uploaded;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Uploaded;->getAttachmentMetadata()Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->addFileAttachment(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    goto :goto_0

    .line 1808
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Updated;

    if-eqz v0, :cond_1

    .line 1809
    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Updated;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Updated;->getAttachmentToken()Ljava/lang/String;

    move-result-object v0

    .line 1810
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Updated;->getUpdatedTitle()Ljava/lang/String;

    move-result-object p1

    .line 1809
    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->updateFileAttachment(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1811
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Removed;

    if-eqz v0, :cond_2

    .line 1812
    check-cast p1, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Removed;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult$Removed;->getAttachmentToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->removeFileAttachment(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-void
.end method

.method private handleAutoRemindersResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutoRemindersResult;)V
    .locals 2

    .line 1789
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutoRemindersResult;->getOutput()Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;

    move-result-object p1

    .line 1791
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;->isConfig()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1793
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutputKt;->remindersAsConfigProto(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;)Ljava/util/List;

    move-result-object p1

    .line 1794
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    goto :goto_0

    .line 1796
    :cond_0
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutputKt;->remindersAsInstanceProto(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersOutput;)Ljava/util/List;

    move-result-object p1

    .line 1797
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$e5rLzLa8mQqyZ2EOk1yFKHIHSzM;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$e5rLzLa8mQqyZ2EOk1yFKHIHSzM;-><init>(Ljava/util/List;)V

    invoke-static {v0, v1}, Lcom/squareup/invoices/Invoices;->updateAllPaymentRequests(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    :goto_0
    return-void
.end method

.method private handleDeleteDraftResponse(Lcom/squareup/protos/client/invoice/DeleteDraftResponse;Z)V
    .locals 2

    if-eqz p2, :cond_0

    .line 1348
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->finish()V

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1351
    iget-object p2, p1, Lcom/squareup/protos/client/invoice/DeleteDraftResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p2, p2, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_2

    .line 1353
    sget-object p1, Lcom/squareup/invoices/edit/InvoiceAction;->DELETE:Lcom/squareup/invoices/edit/InvoiceAction;

    const-string v0, ""

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1355
    :cond_2
    new-instance v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;

    if-nez p1, :cond_3

    const/4 p1, 0x0

    goto :goto_1

    :cond_3
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/DeleteDraftResponse;->status:Lcom/squareup/protos/client/Status;

    :goto_1
    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->DELETE:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;)V

    .line 1358
    sget-object p1, Lcom/squareup/invoices/edit/InvoiceAction;->DELETE:Lcom/squareup/invoices/edit/InvoiceAction;

    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorTitle:Ljava/lang/CharSequence;

    iget-object v0, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorDescription:Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :goto_2
    return-void
.end method

.method private handleDeleteDraftSeriesResponse(Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;)V
    .locals 3

    if-eqz p1, :cond_0

    .line 1333
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 1335
    sget-object p1, Lcom/squareup/invoices/edit/InvoiceAction;->DELETE_SERIES:Lcom/squareup/invoices/edit/InvoiceAction;

    const-string v1, ""

    invoke-direct {p0, p1, v0, v1, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1337
    :cond_1
    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;

    if-nez p1, :cond_2

    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    :goto_1
    sget-object v2, Lcom/squareup/invoices/edit/InvoiceAction;->DELETE_SERIES:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;)V

    .line 1340
    sget-object p1, Lcom/squareup/invoices/edit/InvoiceAction;->DELETE_SERIES:Lcom/squareup/invoices/edit/InvoiceAction;

    iget-object v2, v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorTitle:Ljava/lang/CharSequence;

    iget-object v1, v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorDescription:Ljava/lang/CharSequence;

    invoke-direct {p0, p1, v0, v2, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :goto_2
    return-void
.end method

.method private handleDeliveryMethodResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DeliveryResult;)V
    .locals 2

    .line 462
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DeliveryResult;->getResult()Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;

    move-result-object p1

    .line 463
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;->getPaymentMethod()Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/invoices/Invoices;->updatePaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 464
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;->getInstrumentToken()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/invoices/Invoices;->updateInstrumentToken(Lcom/squareup/protos/client/invoice/Invoice$Builder;Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-void
.end method

.method private handleDueDateSelectedResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;)V
    .locals 4

    .line 507
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;->getResult()Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;->selectedDateFromOutput(Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 514
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v3}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/squareup/invoices/Invoices;->getFirstSentDate(Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v1

    .line 513
    invoke-static {v0, v1, p1}, Lcom/squareup/invoices/Invoices;->updateDueDate(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-void
.end method

.method private handleInvoiceMessageResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$MessageResult;)V
    .locals 1

    .line 519
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$MessageResult;->getInvoiceMessageResult()Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceMessageResult;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-void
.end method

.method private handlePaymentRequestResult(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;I)V
    .locals 1

    .line 1735
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Saved;

    if-eqz v0, :cond_0

    .line 1736
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Saved;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Saved;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->onPaymentRequestSaved(Lcom/squareup/protos/client/invoice/PaymentRequest;I)V

    goto :goto_0

    .line 1737
    :cond_0
    instance-of p1, p1, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult$Removed;

    if-eqz p1, :cond_1

    .line 1738
    invoke-direct {p0, p2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->onPaymentRequestRemoved(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method private handlePaymentScheduleResult(Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;)V
    .locals 2

    .line 1774
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult$Saved;

    if-eqz v0, :cond_0

    .line 1775
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult$Saved;

    .line 1776
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult$Saved;->getPaymentRequests()Ljava/util/List;

    move-result-object p1

    .line 1778
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iput-object p1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    .line 1780
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_SHOW_PAYMENT_SCHEDULE_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1781
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 1782
    invoke-static {p1}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p1

    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne p1, v0, :cond_0

    .line 1783
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->maybeTurnOnAutomaticReminders()V

    :cond_0
    return-void
.end method

.method private handleRecurringResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecurringResult;)V
    .locals 3

    .line 468
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecurringResult;->getResult()Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;->rRuleFromOutput(Lcom/squareup/invoices/workflow/edit/RecurringScheduleWorkflowOutput;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    .line 470
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->allowAutomaticPayments(Z)V

    .line 472
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v1

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-eqz v1, :cond_2

    if-nez v2, :cond_2

    .line 477
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config:Ljava/util/List;

    .line 478
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$zONzfYtBOXnQLYkkuo5rKsBlP1Y;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$zONzfYtBOXnQLYkkuo5rKsBlP1Y;-><init>(Ljava/util/List;)V

    invoke-static {v1, v2}, Lcom/squareup/invoices/Invoices;->updateAllPaymentRequests(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    goto :goto_1

    :cond_2
    if-nez v1, :cond_3

    if-eqz v2, :cond_3

    .line 486
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    .line 487
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/PaymentRequest;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/PaymentRequest;->reminders:Ljava/util/List;

    .line 488
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-static {v0}, Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;->toConfigs(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 491
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->recurrenceRule:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 492
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    sget-object v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$DhMeT2Epif_oB6w31-fL3Rrje88;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$DhMeT2Epif_oB6w31-fL3Rrje88;

    invoke-static {p1, v0}, Lcom/squareup/invoices/Invoices;->removePaymentRequests(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-void
.end method

.method private handleSaveForFileAttachmentsError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;",
            ">;)V"
        }
    .end annotation

    .line 2014
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_connection_error:I

    sget-object v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$neNP-Q5XgLUrdjuck6JNXCZCEg0;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$neNP-Q5XgLUrdjuck6JNXCZCEg0;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 2017
    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2018
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    const/4 v2, 0x0

    const-string v3, "cannot save invoice for file attachments"

    invoke-direct {v1, v0, v2, v3}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;ZLjava/lang/String;)V

    invoke-virtual {p1, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private handleScheduleSeries(Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;Lcom/squareup/invoices/edit/InvoiceAction;)V
    .locals 2

    .line 1317
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SCHEDULE_RECURRING:Lcom/squareup/invoices/edit/InvoiceAction;

    if-ne p2, v0, :cond_0

    .line 1318
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SEND_RECURRING_SERIES:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    :cond_0
    if-eqz p1, :cond_1

    .line 1321
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    const-string p1, ""

    .line 1323
    invoke-direct {p0, p2, v0, p1, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1325
    :cond_2
    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;

    if-nez p1, :cond_3

    const/4 p1, 0x0

    goto :goto_1

    :cond_3
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    :goto_1
    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;)V

    .line 1327
    iget-object p1, v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorTitle:Ljava/lang/CharSequence;

    iget-object v1, v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorDescription:Ljava/lang/CharSequence;

    invoke-direct {p0, p2, v0, p1, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :goto_2
    return-void
.end method

.method private handleScheduledDateSelectedResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;)V
    .locals 0

    .line 497
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;->getResult()Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;->selectedDateFromOutput(Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    .line 503
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->updateScheduledAt(Lcom/squareup/protos/common/time/YearMonthDay;)V

    return-void
.end method

.method private handleSingleResendOrUpdate(Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1290
    iget-object v0, p1, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    .line 1292
    sget-object p1, Lcom/squareup/invoices/edit/InvoiceAction;->UPDATE:Lcom/squareup/invoices/edit/InvoiceAction;

    if-ne p2, p1, :cond_1

    .line 1293
    iget-object p1, p3, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/Invoice;->newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setWorkingInvoice(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    :cond_1
    const-string p1, ""

    .line 1295
    invoke-direct {p0, p2, v0, p1, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1297
    :cond_2
    new-instance p3, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;

    invoke-direct {p3, p0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;)V

    .line 1298
    iget-object p1, p3, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorTitle:Ljava/lang/CharSequence;

    iget-object p3, p3, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorDescription:Ljava/lang/CharSequence;

    invoke-direct {p0, p2, v0, p1, p3}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method

.method private handleSingleSendOrScheduleResponse(Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;)V
    .locals 5

    if-eqz p1, :cond_0

    .line 1234
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1237
    :goto_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->INVOICE:Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;

    invoke-virtual {v2}, Lcom/squareup/log/fastcheckout/CheckoutInformationEvent$TenderType;->fullNameForLogging()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logPaymentFinished(Ljava/lang/String;)V

    .line 1239
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v1

    .line 1241
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v2, v3, :cond_1

    .line 1242
    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_CREATE_SHARE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 1243
    sget-object v2, Lcom/squareup/invoices/edit/InvoiceAction;->CREATE:Lcom/squareup/invoices/edit/InvoiceAction;

    goto :goto_3

    .line 1245
    :cond_1
    iget-object v2, v1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v2, v3}, Lcom/squareup/invoices/InvoiceDatesKt;->isToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1246
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v2

    sget-object v3, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v2, v3, :cond_2

    sget-object v2, Lcom/squareup/invoices/edit/InvoiceAction;->CHARGE:Lcom/squareup/invoices/edit/InvoiceAction;

    goto :goto_1

    :cond_2
    sget-object v2, Lcom/squareup/invoices/edit/InvoiceAction;->SEND:Lcom/squareup/invoices/edit/InvoiceAction;

    goto :goto_1

    .line 1249
    :cond_3
    sget-object v2, Lcom/squareup/invoices/edit/InvoiceAction;->SCHEDULE:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 1251
    :goto_1
    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v4, Lcom/squareup/invoices/edit/InvoiceAction;->SCHEDULE:Lcom/squareup/invoices/edit/InvoiceAction;

    if-ne v2, v4, :cond_4

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SCHEDULE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    goto :goto_2

    :cond_4
    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_SEND_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    :goto_2
    invoke-interface {v3, v4}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    :goto_3
    if-eqz v0, :cond_6

    .line 1258
    sget-object v3, Lcom/squareup/invoices/edit/InvoiceAction;->CREATE:Lcom/squareup/invoices/edit/InvoiceAction;

    if-ne v2, v3, :cond_5

    .line 1259
    iget-object v3, p1, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {v3}, Lcom/squareup/protos/client/invoice/Invoice;->newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setWorkingInvoice(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    :cond_5
    const-string v3, ""

    .line 1261
    invoke-direct {p0, v2, v0, v3, v3}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1262
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->adAnalytics:Lcom/squareup/adanalytics/AdAnalytics;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/UserSettings;->getMerchantToken()Ljava/lang/String;

    move-result-object v2

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-interface {v0, v2, p1, v1}, Lcom/squareup/adanalytics/AdAnalytics;->recordSendInvoice(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;)V

    goto :goto_5

    .line 1266
    :cond_6
    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;

    if-nez p1, :cond_7

    const/4 p1, 0x0

    goto :goto_4

    :cond_7
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    :goto_4
    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;)V

    .line 1268
    iget-object p1, v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorTitle:Ljava/lang/CharSequence;

    iget-object v1, v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorDescription:Ljava/lang/CharSequence;

    invoke-direct {p0, v2, v0, p1, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :goto_5
    return-void
.end method

.method private inTender()Z
    .locals 2

    .line 1688
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getPath()Lcom/squareup/invoices/edit/EditInvoiceScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScope;->getType()Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/edit/EditInvoiceScope$Type;->EDIT_INVOICE_IN_TENDER:Lcom/squareup/invoices/edit/EditInvoiceScope$Type;

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/edit/EditInvoiceScope$Type;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private initWithInvoice(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V
    .locals 5

    .line 599
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 601
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->attachment:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->addFileAttachmentsToList(Ljava/util/List;)V

    .line 603
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->inTender()Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    .line 605
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 607
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-static {v1, p1}, Lcom/squareup/invoices/Invoices;->setPayerFromContact(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/client/rolodex/Contact;)Z

    .line 613
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 614
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/squareup/payment/Order;->getCartProtoForInvoice(Lcom/squareup/protos/common/Money;Z)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v1

    .line 613
    invoke-virtual {p1, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 617
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 618
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->getRoundingType()Lcom/squareup/calc/constants/RoundingType;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v2

    xor-int/2addr v0, v2

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v4, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    .line 619
    invoke-interface {v3, v4}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v3

    .line 617
    invoke-static {p1, v1, v0, v2, v3}, Lcom/squareup/payment/OrderProtoConversions;->makeOrderFromInvoiceProto(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/calc/constants/RoundingType;ZLcom/squareup/util/Res;Z)Lcom/squareup/payment/Order;

    move-result-object p1

    .line 622
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lcom/squareup/payment/Order;->getCartProtoForInvoice(Lcom/squareup/protos/common/Money;Z)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 625
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->customAmountCartItem:Lcom/squareup/checkout/CartItem;

    if-eqz v0, :cond_2

    .line 626
    invoke-virtual {p1, v0}, Lcom/squareup/payment/Order;->pushItem(Lcom/squareup/checkout/CartItem;)V

    .line 628
    :cond_2
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setOrder(Lcom/squareup/payment/Order;)V

    .line 630
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/Invoice;->newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->initialInvoice:Lcom/squareup/protos/client/invoice/Invoice;

    .line 632
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    invoke-static {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isPayerEmpty(Lcom/squareup/protos/client/invoice/InvoiceContact;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 633
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->contactIdForInstruments:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->contact_token:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_3
    return-void
.end method

.method public static isPayerEmpty(Lcom/squareup/protos/client/invoice/InvoiceContact;)Z
    .locals 1

    if-eqz p0, :cond_1

    .line 1682
    iget-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceContact;->contact_token:Ljava/lang/String;

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static synthetic lambda$GsDpW2K5ic8qbNdemKvfZueqOqc(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleSaveForFileAttachmentsError(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V

    return-void
.end method

.method static synthetic lambda$goToAutomaticRemindersScreen$10(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 723
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    return-object p0
.end method

.method static synthetic lambda$goToAutomaticRemindersScreen$11(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    .line 725
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/InvoiceDefaults;->automatic_reminder_config:Ljava/util/List;

    return-object p0
.end method

.method static synthetic lambda$handleAutoRemindersResult$52(Ljava/util/List;Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;)Lkotlin/Unit;
    .locals 0

    .line 1799
    invoke-virtual {p1, p0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 1800
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method static synthetic lambda$handleRecurringResult$8(Ljava/util/List;Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;)Lkotlin/Unit;
    .locals 0

    .line 480
    invoke-static {p0}, Lcom/squareup/invoices/InvoiceReminderConfigUtilsKt;->toInstances(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->reminders(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    .line 481
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method static synthetic lambda$handleRecurringResult$9(Lcom/squareup/protos/client/invoice/PaymentRequest;)Ljava/lang/Boolean;
    .locals 1

    .line 492
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/PaymentRequest;->amount_type:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    sget-object v0, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->REMAINDER:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    if-eq p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$handleSaveForFileAttachmentsError$54(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 1

    .line 2016
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    iget-object p0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method static synthetic lambda$manuallySaveDraftAndShowConfirmation$37(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 1171
    iget-object p0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$manuallySaveDraftAndShowSetupPayments$41(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 1189
    iget-object p0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$null$2(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Ljava/util/List;
    .locals 1

    .line 394
    invoke-virtual {p0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/GetContactResponse;

    if-eqz p0, :cond_1

    .line 395
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;->instrument:Ljava/util/List;

    if-nez v0, :cond_0

    goto :goto_0

    .line 401
    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->instruments_on_file:Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/InstrumentsOnFile;->instrument:Ljava/util/List;

    return-object p0

    .line 399
    :cond_1
    :goto_0
    sget-object p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->EMPTY_INSTRUMENTS:Ljava/util/List;

    return-object p0
.end method

.method static synthetic lambda$null$47(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 1

    .line 1623
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    iget-object p0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method static synthetic lambda$onEnterScope$6(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)Ljava/lang/Boolean;
    .locals 1

    .line 443
    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;->getChooseCustomerResultKey()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    move-result-object p0

    sget-object v0, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->EDIT_INVOICE:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onSetupPaymentsDialogPrimaryClicked$45(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 1616
    iget-object p0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$saveThenStartFileAttachmentCreation$29(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 1111
    iget-object p0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$startEditingItemWithModifiers$13(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;ZLflow/History;)Lcom/squareup/container/Command;
    .locals 1

    .line 831
    new-instance v0, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;

    invoke-direct {v0, p0, p1}, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;)V

    .line 833
    invoke-virtual {p3}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p0

    invoke-virtual {p0, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p0

    if-eqz p2, :cond_0

    .line 835
    new-instance p1, Lcom/squareup/configure/item/ConfigureItemPriceScreen;

    invoke-virtual {v0}, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;->getParentKey()Lcom/squareup/configure/item/ConfigureItemScope;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/configure/item/ConfigureItemPriceScreen;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    invoke-virtual {p0, p1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 837
    :cond_0
    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    sget-object p1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {p0, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method

.method private static loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/wire/Message;",
            ">(",
            "Lcom/squareup/wire/ProtoAdapter<",
            "TT;>;",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .line 1853
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1855
    invoke-virtual {p0, p1}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wire/Message;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0

    :catch_0
    move-exception p0

    .line 1859
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method

.method private manuallySaveDraft()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;",
            ">;"
        }
    .end annotation

    .line 1156
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->saveSingleOrRecurringDraft(Z)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$Rtiaf7VqUp7mKaoqm0ant4oGz98;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$Rtiaf7VqUp7mKaoqm0ant4oGz98;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 1157
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$ViRi3Pv4Xt_tevx2_SbiH2Z9feg;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$ViRi3Pv4Xt_tevx2_SbiH2Z9feg;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 1161
    invoke-virtual {v0, v1}, Lrx/Observable;->doAfterTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method private manuallySaveDraftAndShowConfirmation()V
    .locals 5

    .line 1168
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v0

    .line 1169
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    .line 1170
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->manuallySaveDraft()Lrx/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v4, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$h10EBpYnUc5YH0_cU5BxZqvUrKM;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$h10EBpYnUc5YH0_cU5BxZqvUrKM;

    .line 1171
    invoke-static {v3, v4}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$HYUDd4--eiI3XLNyLhPAhjvQQ5Y;

    invoke-direct {v3, p0, v0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$HYUDd4--eiI3XLNyLhPAhjvQQ5Y;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Z)V

    .line 1172
    invoke-virtual {v2, v3}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 1169
    invoke-virtual {v1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private manuallySaveDraftAndShowSetupPayments()V
    .locals 5

    .line 1186
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SAVE_RECURRING:Lcom/squareup/invoices/edit/InvoiceAction;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SAVE:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 1187
    :goto_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    .line 1188
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->manuallySaveDraft()Lrx/Observable;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v4, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$zvzSC6D5f1lqxWiJmhVxB-TSZpw;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$zvzSC6D5f1lqxWiJmhVxB-TSZpw;

    .line 1189
    invoke-static {v3, v4}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$ELtWji6s99Mwx5-WZk2willkt4w;

    invoke-direct {v3, p0, v0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$ELtWji6s99Mwx5-WZk2willkt4w;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/invoices/edit/InvoiceAction;)V

    .line 1190
    invoke-virtual {v2, v3}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 1187
    invoke-virtual {v1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private maybeTurnOnAutomaticReminders()V
    .locals 2

    .line 1817
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 1818
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config:Ljava/util/List;

    .line 1819
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1820
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v1}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->getDefaultReminderConfigs()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminder_config(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    :cond_1
    return-void
.end method

.method private noServerId(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Z
    .locals 1

    .line 1573
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v0, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private onPaymentRequestRemoved(I)V
    .locals 1

    if-gez p1, :cond_0

    return-void

    .line 1847
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-void
.end method

.method private onPaymentRequestSaved(Lcom/squareup/protos/client/invoice/PaymentRequest;I)V
    .locals 3

    .line 1825
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 1826
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    const/4 p2, 0x0

    .line 1829
    invoke-interface {v1, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1830
    invoke-static {p1}, Lcom/squareup/invoices/PaymentRequestsKt;->isFixedAmountType(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result p2

    if-nez p2, :cond_0

    invoke-static {p1}, Lcom/squareup/invoices/PaymentRequestsKt;->isPercentageType(Lcom/squareup/protos/client/invoice/PaymentRequest;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 1831
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/PaymentRequest;->relative_due_on:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    invoke-virtual {p0, v1, p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->updateRemainderToBeDueAfterDeposit(Ljava/util/List;J)V

    goto :goto_0

    .line 1835
    :cond_1
    invoke-interface {v1, p2, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1837
    :cond_2
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request(Ljava/util/List;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 1838
    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object p1

    sget-object p2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-eq p1, p2, :cond_3

    .line 1839
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->maybeTurnOnAutomaticReminders()V

    :cond_3
    return-void
.end method

.method private populateInvoicePayment(Lcom/squareup/invoices/edit/InvoiceAction;)V
    .locals 2

    .line 1668
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireInvoicePayment()Lcom/squareup/payment/InvoicePayment;

    move-result-object v0

    .line 1669
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    if-eqz v1, :cond_0

    .line 1670
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/InvoicePayment;->setBuyerName(Ljava/lang/String;)V

    .line 1671
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/InvoicePayment;->setEmail(Ljava/lang/String;)V

    .line 1673
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/InvoicePayment;->setIdPair(Lcom/squareup/protos/client/IdPair;)V

    .line 1674
    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->SAVE:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-virtual {p1, v1}, Lcom/squareup/invoices/edit/InvoiceAction;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/InvoicePayment;->setDraft(Z)V

    .line 1675
    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->SCHEDULE:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-virtual {p1, v1}, Lcom/squareup/invoices/edit/InvoiceAction;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/InvoicePayment;->setScheduled(Z)V

    .line 1676
    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->CREATE:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-virtual {p1, v1}, Lcom/squareup/invoices/edit/InvoiceAction;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/payment/InvoicePayment;->setShareLinkInvoice(Z)V

    .line 1677
    sget-object v1, Lcom/squareup/invoices/edit/InvoiceAction;->CHARGE:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-virtual {p1, v1}, Lcom/squareup/invoices/edit/InvoiceAction;->equals(Ljava/lang/Object;)Z

    move-result p1

    .line 1678
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getSelectedInstrumentIfAny()Lcom/squareup/protos/client/instruments/InstrumentSummary;

    move-result-object v1

    .line 1677
    invoke-virtual {v0, p1, v1}, Lcom/squareup/payment/InvoicePayment;->setChargedInvoice(ZLcom/squareup/protos/client/instruments/InstrumentSummary;)V

    return-void
.end method

.method private previewAfterInvoiceMaterialized(Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/DisplayDetails;)V
    .locals 2

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    .line 1275
    iget-object v1, p1, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_1

    .line 1277
    invoke-direct {p0, p2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->resetWorkingInvoiceAfterMaterialization(Lcom/squareup/invoices/DisplayDetails;)V

    .line 1278
    iput-boolean v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->savedByPreviewOrUpload:Z

    .line 1279
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToInvoicePreviewScreen()V

    goto :goto_1

    .line 1281
    :cond_1
    new-instance p2, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;

    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->PREVIEW:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-direct {p2, p0, p1, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;)V

    .line 1283
    sget-object p1, Lcom/squareup/invoices/edit/InvoiceAction;->PREVIEW:Lcom/squareup/invoices/edit/InvoiceAction;

    iget-object v0, p2, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorTitle:Ljava/lang/CharSequence;

    iget-object p2, p2, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorDescription:Ljava/lang/CharSequence;

    invoke-direct {p0, p1, v1, v0, p2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method

.method private resetWorkingInvoiceAfterMaterialization(Lcom/squareup/invoices/DisplayDetails;)V
    .locals 1

    .line 1365
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    .line 1366
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice;->newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setWorkingInvoice(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    .line 1367
    instance-of v0, p1, Lcom/squareup/invoices/DisplayDetails$Recurring;

    if-eqz v0, :cond_0

    .line 1368
    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Recurring;

    .line 1369
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 1370
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Recurring;->getDetails()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/RecurringSeries;->recurrence_schedule:Lcom/squareup/protos/client/invoice/RecurringSchedule;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/RecurringSchedule;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 1369
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    :cond_0
    return-void
.end method

.method private saveSingleOrRecurringDraft(Z)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1000
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoiceToSend()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->recurrenceRule:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 1001
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    .line 1000
    invoke-virtual {p1, v0, v1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->saveRecurringDraft(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/invoices/edit/-$$Lambda$gyKLD5aVef74DxdbIXCSTHa9ig0;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$gyKLD5aVef74DxdbIXCSTHa9ig0;

    .line 1002
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 1004
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoiceToSend()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->saveDraftInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/invoices/edit/-$$Lambda$ofUFZ-ETHMpL_HtCgXYT72-Gu5I;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$ofUFZ-ETHMpL_HtCgXYT72-Gu5I;

    .line 1005
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method private saveThenStartFileAttachmentCreation()V
    .locals 4

    .line 1107
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->saveSingleOrRecurringDraft(Z)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$aOxZuzSSBXkJJl8iEBE4dVTQtAI;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$aOxZuzSSBXkJJl8iEBE4dVTQtAI;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    invoke-virtual {v1, v2}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v3, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$FZABCxOPVLcnoImHP54KpVum8Bo;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$FZABCxOPVLcnoImHP54KpVum8Bo;

    .line 1111
    invoke-static {v2, v3}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$mjhfD7Qf3QYTb35Tjv4lqAzDTqw;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$mjhfD7Qf3QYTb35Tjv4lqAzDTqw;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 1112
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    .line 1107
    invoke-virtual {v0, v1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private sendFromPreview()V
    .locals 1

    .line 1994
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getInvoiceValidationErrorIfAny()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1995
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->sendNewOrExistingInvoice()V

    goto :goto_0

    .line 1997
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    :goto_0
    return-void
.end method

.method private sendNewInvoice()V
    .locals 1

    .line 982
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 983
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SCHEDULE_RECURRING:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->showRecurringMonthlyDialogIfNecessary(Lcom/squareup/invoices/edit/InvoiceAction;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 985
    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->sendSingleInvoice(Lcom/squareup/invoices/edit/InvoiceAction;)V

    :goto_0
    return-void
.end method

.method private sendSingleInvoice(Lcom/squareup/invoices/edit/InvoiceAction;)V
    .locals 5

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 1010
    :goto_0
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoiceToSend()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->sendOrSchedule(Lcom/squareup/protos/client/invoice/Invoice;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$cABPgPRLbgF8qZbJaClhdr34WQQ;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$cABPgPRLbgF8qZbJaClhdr34WQQ;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 1011
    invoke-virtual {v2, v3}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v2

    new-instance v3, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$fwv2V9lzhMjkR3dx8NV4PaUqzho;

    invoke-direct {v3, p0, v0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$fwv2V9lzhMjkR3dx8NV4PaUqzho;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;ZLcom/squareup/invoices/edit/InvoiceAction;)V

    new-instance v4, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$rr-w39sTWUGvdX9nzSrRDH_GSys;

    invoke-direct {v4, p0, v0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$rr-w39sTWUGvdX9nzSrRDH_GSys;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;ZLcom/squareup/invoices/edit/InvoiceAction;)V

    .line 1014
    invoke-virtual {v2, v3, v4}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 1010
    invoke-virtual {v1, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method private shouldCompleteOnboardingSetUp()Z
    .locals 1

    .line 1590
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private shouldShowRecurringMonthlyDialog(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Z
    .locals 0

    .line 790
    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->canBeEndOfMonth(Lcom/squareup/protos/common/time/YearMonthDay;)Z

    move-result p1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 792
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->isFrequencyMonthly()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private showRecurringMonthlyDialogIfNecessary(Lcom/squareup/invoices/edit/InvoiceAction;)V
    .locals 3

    .line 1040
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoiceToSend()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    .line 1041
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->recurrenceRule:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    .line 1043
    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->shouldShowRecurringMonthlyDialog(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1044
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/edit/RecurringMonthlyDialog;

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getPath()Lcom/squareup/invoices/edit/EditInvoiceScope;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/squareup/invoices/edit/RecurringMonthlyDialog;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;Lcom/squareup/invoices/edit/InvoiceAction;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 1046
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->scheduleSeries(Lcom/squareup/invoices/edit/InvoiceAction;)V

    :goto_0
    return-void
.end method

.method private updateFileAttachment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 1498
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    .line 1499
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1500
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 1501
    iget-object v3, v2, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1502
    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->newBuilder()Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata$Builder;->build()Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1505
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method private userInitiatedDraftSaved(Lcom/squareup/protos/client/Status;Z)V
    .locals 2

    if-eqz p1, :cond_0

    .line 1304
    iget-object v0, p1, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 1305
    sget-object p2, Lcom/squareup/invoices/edit/InvoiceAction;->SAVE_RECURRING:Lcom/squareup/invoices/edit/InvoiceAction;

    goto :goto_1

    :cond_1
    sget-object p2, Lcom/squareup/invoices/edit/InvoiceAction;->SAVE:Lcom/squareup/invoices/edit/InvoiceAction;

    :goto_1
    if-eqz v0, :cond_2

    const-string p1, ""

    .line 1307
    invoke-direct {p0, p2, v0, p1, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1309
    :cond_2
    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;)V

    .line 1310
    iget-object p1, v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorTitle:Ljava/lang/CharSequence;

    iget-object v1, v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorDescription:Ljava/lang/CharSequence;

    invoke-direct {p0, p2, v0, p1, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :goto_2
    return-void
.end method

.method private withClientId(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 2

    .line 1577
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-static {v0}, Lcom/squareup/util/HexStrings;->randomHexString(Ljava/util/Random;)Ljava/lang/String;

    move-result-object v0

    .line 1579
    new-instance v1, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public addDiscountToCart(Lcom/squareup/checkout/Discount;)V
    .locals 2

    .line 889
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->ADD_DISCOUNT_EVENT:Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 890
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->addDiscount(Lcom/squareup/checkout/Discount;)V

    const/4 p1, 0x0

    .line 891
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    .line 892
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    .line 893
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->finishEditingCart()V

    return-void
.end method

.method addFileAttachmentsToList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 1486
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1487
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public addItemToCart(Lcom/squareup/checkout/CartItem;)V
    .locals 3

    .line 856
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sget-object v2, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->ADD_EDIT_CUSTOM_AMOUNT_EVENT:Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    goto :goto_1

    :cond_0
    new-instance v1, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    iget-object v2, p1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    if-nez v2, :cond_1

    const-string v2, "No Item ID"

    goto :goto_0

    :cond_1
    iget-object v2, p1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    :goto_0
    invoke-direct {v1, v2}, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;-><init>(Ljava/lang/String;)V

    :goto_1
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 860
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->pushItem(Lcom/squareup/checkout/CartItem;)V

    const/4 p1, 0x0

    .line 861
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 862
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->finishEditingCart()V

    return-void
.end method

.method public allowAutomaticPayments(Z)V
    .locals 2

    .line 926
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_automatic_charge_enroll_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 927
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_instrument_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-void
.end method

.method public cancelConfigureItemCard(Z)V
    .locals 2

    .line 842
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->setCreatingCustomAmount(Z)V

    if-nez p1, :cond_1

    .line 844
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    invoke-virtual {p1}, Lcom/squareup/payment/Order;->popUninterestingItem()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 847
    :cond_0
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goBackToEditInvoice()V

    goto :goto_1

    .line 845
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/invoices/edit/items/ItemSelectScreen;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v0, v1}, Lcom/squareup/invoices/edit/items/ItemSelectScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public closeKeypadEntryCard(Z)V
    .locals 2

    if-nez p1, :cond_0

    .line 567
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void

    .line 571
    :cond_0
    sget-object p1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$1;->$SwitchMap$com$squareup$orderentry$KeypadEntryScreen$KeypadInfo$Type:[I

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getType()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;->ordinal()I

    move-result v0

    aget p1, p1, v0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 582
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getPercentage()Lcom/squareup/util/Percentage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/configure/item/WorkingDiscount;->percentageString:Ljava/lang/String;

    goto :goto_0

    .line 585
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected Keypad Type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    invoke-virtual {v1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getType()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo$Type;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 573
    :cond_2
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    if-eqz p1, :cond_3

    .line 574
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/configure/item/WorkingItem;->currentVariablePrice:Lcom/squareup/protos/common/Money;

    .line 575
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {p1}, Lcom/squareup/configure/item/WorkingItem;->finish()Lcom/squareup/checkout/CartItem;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->addItemToCart(Lcom/squareup/checkout/CartItem;)V

    .line 576
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToEditInvoiceScreen()V

    return-void

    .line 579
    :cond_3
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    invoke-virtual {v0}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/configure/item/WorkingDiscount;->amountMoney:Lcom/squareup/protos/common/Money;

    .line 587
    :goto_0
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/configure/item/WorkingDiscount;->finish(Z)Lcom/squareup/checkout/Discount;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->addDiscountToCart(Lcom/squareup/checkout/Discount;)V

    .line 588
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToEditInvoiceScreen()V

    return-void
.end method

.method public deleteDraft()V
    .locals 2

    .line 990
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_DELETE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 991
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingDraftSeries()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 992
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->deleteDraftSeries()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 994
    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->deleteDraft(Z)V

    :goto_0
    return-void
.end method

.method discardInvoice()V
    .locals 1

    .line 902
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->inTender()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 903
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->clearTransactionPayment()V

    .line 905
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->savedByPreviewOrUpload:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 906
    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->deleteDraft(Z)V

    return-void

    .line 909
    :cond_1
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->finish()V

    return-void
.end method

.method displayDiscardChangesDialog()V
    .locals 3

    .line 897
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/edit/DiscardInvoiceChangesDialog;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public editItemInCart(ILcom/squareup/checkout/CartItem;)V
    .locals 2

    .line 866
    iget-object v0, p2, Lcom/squareup/checkout/CartItem;->backingType:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    sget-object v1, Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;->CUSTOM_ITEM_VARIATION:Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;

    if-ne v0, v1, :cond_0

    .line 867
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;->ADD_EDIT_CUSTOM_AMOUNT_EVENT:Lcom/squareup/invoices/analytics/AddItemToInvoiceEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 869
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/payment/Order;->replaceItem(ILcom/squareup/checkout/CartItem;)V

    .line 870
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->finishEditingCart()V

    .line 871
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goBackToEditInvoice()V

    return-void
.end method

.method exitAutoRemindersTutorial()V
    .locals 1

    .line 638
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->featuresTutorialRunner:Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/NewInvoiceFeaturesTutorialRunner;->onRequestExitTutorial()V

    return-void
.end method

.method public getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;
    .locals 1

    .line 1376
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    return-object v0
.end method

.method getFileMetadataList()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;>;"
        }
    .end annotation

    .line 1472
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public getFileMetadataListCurrentValue()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;"
        }
    .end annotation

    .line 1476
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method getInstruments()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;"
        }
    .end annotation

    .line 646
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->instrumentsForContact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public getInvoiceValidationErrorIfAny()Ljava/lang/String;
    .locals 7

    .line 1534
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getTransactionMaximum()J

    move-result-wide v0

    .line 1535
    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/PaymentSettings;->getTransactionMinimum()J

    move-result-wide v2

    .line 1537
    iget-object v4, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v4, v4, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    if-nez v4, :cond_0

    .line 1538
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_validation_error_no_customer:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1539
    :cond_0
    iget-object v4, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v4, v4, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v4, v4, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_name:Ljava/lang/String;

    invoke-static {v4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1540
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_validation_error_no_customer_name:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1541
    :cond_1
    iget-object v4, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v4, v4, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v4, v4, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    invoke-static {v4}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1542
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_validation_error_no_email:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1543
    :cond_2
    iget-object v4, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v4}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v6, v4, v0

    if-lez v6, :cond_3

    .line 1544
    sget v2, Lcom/squareup/features/invoices/R$string;->payment_type_above_maximum_invoice:I

    .line 1545
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getLimitText(JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1546
    :cond_3
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-gez v4, :cond_4

    .line 1547
    sget v0, Lcom/squareup/features/invoices/R$string;->payment_type_below_minimum_invoice:I

    .line 1548
    invoke-direct {p0, v2, v3, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getLimitText(JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1549
    :cond_4
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->checkForOverlappingPaymentRequestDates()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1550
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_validation_error_deposit_after_balance:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_6

    const/4 v1, 0x1

    goto :goto_1

    :cond_6
    const/4 v1, 0x0

    .line 1553
    :goto_1
    iput-boolean v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->hasValidationError:Z

    return-object v0
.end method

.method getLatestTenderErrorDescription()Ljava/lang/CharSequence;
    .locals 1

    .line 940
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->latestTenderErrorDescription:Ljava/lang/CharSequence;

    return-object v0
.end method

.method getLatestTenderErrorTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 936
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->latestTenderErrorTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    const-string v0, "EditInvoiceSession"

    return-object v0
.end method

.method public getOrder()Lcom/squareup/payment/Order;
    .locals 1

    .line 699
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    return-object v0
.end method

.method public getPath()Lcom/squareup/invoices/edit/EditInvoiceScope;
    .locals 1

    .line 695
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    return-object v0
.end method

.method public getSelectedInstrumentIfAny()Lcom/squareup/protos/client/instruments/InstrumentSummary;
    .locals 4

    .line 662
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->instrumentsForContact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 663
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/invoices/Invoices;->getInstrumentToken(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_1

    .line 665
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/instruments/InstrumentSummary;

    .line 666
    iget-object v3, v2, Lcom/squareup/protos/client/instruments/InstrumentSummary;->instrument_token:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v2

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method getSendButtonText()Ljava/lang/String;
    .locals 4

    .line 1420
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v0, v1}, Lcom/squareup/invoices/InvoiceDatesKt;->isBeforeOrEqualToday(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/time/CurrentTime;)Z

    move-result v0

    .line 1421
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/utilities/R$string;->send:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1423
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v2

    .line 1425
    sget-object v3, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v2, v3, :cond_0

    .line 1426
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_charge_cof_action:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1429
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v2, v3, :cond_1

    .line 1430
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->create:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_1
    if-nez v0, :cond_2

    .line 1434
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_schedule:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1436
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingNonDraftSeries()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1437
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->update:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1441
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingNonDraftSentOrSharedSingleInvoice()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1442
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v2, v0, :cond_3

    .line 1443
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/common/strings/R$string;->update:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1444
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingEmailedInvoice()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1445
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_resend:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_4
    :goto_0
    return-object v1
.end method

.method public getTransaction()Lcom/squareup/payment/Transaction;
    .locals 1

    .line 683
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    return-object v0
.end method

.method public getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;
    .locals 1

    .line 687
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-object v0
.end method

.method public goBackFromAutomaticPaymentScreen()V
    .locals 2

    .line 913
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public goBackFromItemSelect()V
    .locals 0

    .line 1660
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goBackToEditInvoice()V

    return-void
.end method

.method public goBackFromPreview()V
    .locals 1

    .line 2022
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public goBackFromSetupPaymentsDialog()V
    .locals 2

    .line 1652
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/setupguide/SetupPaymentsDialog;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method goToAddPaymentSchedule()V
    .locals 3

    .line 1743
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 1745
    invoke-static {v1}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->reminderDefaults(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;)Lio/reactivex/Observable;

    move-result-object v1

    .line 1746
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$3AsFWpWmceLCASMRKA8nU84XnhI;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$3AsFWpWmceLCASMRKA8nU84XnhI;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 1747
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 1744
    invoke-static {v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Subscription(Lio/reactivex/disposables/Disposable;)Lrx/Subscription;

    move-result-object v1

    .line 1743
    invoke-virtual {v0, v1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method public goToAutomaticPaymentScreen()V
    .locals 3

    .line 715
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentScreen;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/edit/automaticpayments/AutomaticPaymentScreen;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public goToAutomaticRemindersScreen()V
    .locals 4

    .line 719
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 722
    invoke-interface {v1}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->unitMetadataDisplayDetails()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$xVlKrAOJOWjOq0D2xMyZGd4tAD8;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$xVlKrAOJOWjOq0D2xMyZGd4tAD8;

    .line 723
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 724
    invoke-interface {v2}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->invoiceDefaultsList()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$Gxzq2d5bJYzWEnWegrDeaaF6NhM;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$Gxzq2d5bJYzWEnWegrDeaaF6NhM;

    .line 725
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 726
    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v3

    .line 721
    invoke-static {v1, v2, v3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v1

    .line 728
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$LlSjTXE0GFfJWUFx3q2vCchBhgM;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$LlSjTXE0GFfJWUFx3q2vCchBhgM;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 729
    invoke-static {v2}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 720
    invoke-static {v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Subscription(Lio/reactivex/disposables/Disposable;)Lrx/Subscription;

    move-result-object v1

    .line 719
    invoke-virtual {v0, v1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method public goToChooseDueDate()V
    .locals 6

    .line 1880
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Due;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Due;

    .line 1881
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v3

    .line 1882
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v4

    .line 1883
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v5

    .line 1881
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->createForInvoice(Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/DisplayDetails;Z)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Due;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Due;

    .line 1880
    invoke-interface {v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startDateChooser(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;Lcom/squareup/invoices/workflow/edit/ChooseDateType;)V

    return-void
.end method

.method public goToChooseSendDate()V
    .locals 6

    .line 1887
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

    sget-object v2, Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Scheduled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Scheduled;

    .line 1888
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v3

    .line 1889
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v4

    .line 1890
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v5

    .line 1888
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->createForInvoice(Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/DisplayDetails;Z)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Scheduled;->INSTANCE:Lcom/squareup/invoices/workflow/edit/ChooseDateType$InvoiceDateType$Scheduled;

    .line 1887
    invoke-interface {v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startDateChooser(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;Lcom/squareup/invoices/workflow/edit/ChooseDateType;)V

    return-void
.end method

.method public goToEditInvoiceScreen()V
    .locals 3

    .line 1713
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/edit/EditInvoiceScreen;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/edit/EditInvoiceScreen;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method goToEditPaymentSchedule()V
    .locals 3

    .line 1759
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 1761
    invoke-static {v1}, Lcom/squareup/invoices/util/InvoiceReminderUtilsKt;->reminderDefaults(Lcom/squareup/invoicesappletapi/InvoiceUnitCache;)Lio/reactivex/Observable;

    move-result-object v1

    .line 1762
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$4ZSSh0Uqmgve-g5xsnJZhKohfaA;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$4ZSSh0Uqmgve-g5xsnJZhKohfaA;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 1763
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 1760
    invoke-static {v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Subscription(Lio/reactivex/disposables/Disposable;)Lrx/Subscription;

    move-result-object v1

    .line 1759
    invoke-virtual {v0, v1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method public goToRequestDeposit()V
    .locals 7

    .line 1717
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    .line 1718
    sget-object v2, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->FIXED_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    .line 1719
    invoke-static {v0, v1}, Lcom/squareup/invoices/PaymentRequestsKt;->getIndexedPaymentRequestByTypes(Ljava/util/List;[Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;)Lkotlin/collections/IndexedValue;

    move-result-object v1

    .line 1720
    invoke-virtual {v1}, Lkotlin/collections/IndexedValue;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 1721
    invoke-virtual {v1}, Lkotlin/collections/IndexedValue;->getIndex()I

    move-result v1

    if-gez v1, :cond_0

    const/4 v1, -0x1

    .line 1725
    sget-object v2, Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;->PERCENTAGE_DEPOSIT:Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;

    iget-object v5, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 1726
    invoke-static {v0}, Lcom/squareup/invoices/PaymentRequestsKt;->getRemainderPaymentRequest(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    const/4 v6, 0x0

    .line 1725
    invoke-static {v2, v5, v0, v6}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContextKt;->constructDefaultDepositPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest$AmountType;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/client/invoice/PaymentRequest;Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v2

    .line 1729
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    .line 1730
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v5

    .line 1731
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v6

    if-gez v1, :cond_1

    const/4 v3, 0x1

    :cond_1
    iget-object v4, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v4}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v4

    .line 1730
    invoke-static {v2, v5, v6, v3, v4}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->createInfo(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;ZLcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v2

    .line 1729
    invoke-interface {v0, v2, v1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startEditingPaymentRequest(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;I)V

    return-void
.end method

.method hasValidationError()Z
    .locals 1

    .line 751
    iget-boolean v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->hasValidationError:Z

    return v0
.end method

.method hasWorkingInvoiceChanged()Z
    .locals 2

    .line 711
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->initialInvoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public invoicePreviewScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;",
            ">;"
        }
    .end annotation

    .line 1972
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isBusy()Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$Jf6uX1_16pcPg9FhF_x5j56czGU;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$Jf6uX1_16pcPg9FhF_x5j56czGU;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public isAutomaticPaymentsEnabled()Z
    .locals 1

    .line 917
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_automatic_charge_enroll_enabled:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 918
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0
.end method

.method public isBusy()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1464
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method isCardOnFile()Z
    .locals 2

    .line 1416
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isDuplicateInvoice()Z
    .locals 1

    .line 1400
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDuplicateInvoice()Z

    move-result v0

    return v0
.end method

.method isEditingDraftSeries()Z
    .locals 1

    .line 1396
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingDraftSeries()Z

    move-result v0

    return v0
.end method

.method public isEditingEmailedInvoice()Z
    .locals 2

    .line 810
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 811
    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isEditingInvoiceInRecurringSeries()Z
    .locals 1

    .line 1408
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingSingleInvoiceInRecurringSeries()Z

    move-result v0

    return v0
.end method

.method isEditingNonDraftSentOrSharedSingleInvoice()Z
    .locals 1

    .line 1404
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingNonDraftSentOrSharedSingleInvoice()Z

    move-result v0

    return v0
.end method

.method isEditingNonDraftSeries()Z
    .locals 1

    .line 1388
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingNonDraftSeries()Z

    move-result v0

    return v0
.end method

.method isEditingNotDraftSingleInvoice()Z
    .locals 1

    .line 1392
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingNonDraftSingleInvoice()Z

    move-result v0

    return v0
.end method

.method isEditingSeries()Z
    .locals 1

    .line 1384
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingSeries()Z

    move-result v0

    return v0
.end method

.method isEditingSingleInvoice()Z
    .locals 1

    .line 1380
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isEditingSingleInvoice()Z

    move-result v0

    return v0
.end method

.method isNewInvoice()Z
    .locals 1

    .line 707
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v0

    return v0
.end method

.method public isRecurring()Z
    .locals 1

    .line 1460
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->recurrenceRule:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public keypadInfo()Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;
    .locals 1

    .line 562
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    return-object v0
.end method

.method public synthetic lambda$deleteDraft$20$EditInvoiceScopeRunner()V
    .locals 2

    .line 1079
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1080
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$deleteDraft$23$EditInvoiceScopeRunner(ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 1082
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1083
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 1084
    new-instance v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$R4OHl5FwWswMM1flUnxBfh7BgXs;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$R4OHl5FwWswMM1flUnxBfh7BgXs;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Z)V

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$0FSpA6Olox7yt-xCl9Ui4Xzxb7o;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$0FSpA6Olox7yt-xCl9Ui4Xzxb7o;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Z)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$deleteDraftSeries$24$EditInvoiceScopeRunner()V
    .locals 2

    .line 1094
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1095
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$deleteDraftSeries$27$EditInvoiceScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 1097
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1098
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 1099
    new-instance v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$ko4TcHCUGG5uVACtHAbzaRWrwOo;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$ko4TcHCUGG5uVACtHAbzaRWrwOo;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$gPN0YivgdFJCUSKU9b---xRdZnc;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$gPN0YivgdFJCUSKU9b---xRdZnc;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$goToAddPaymentSchedule$50$EditInvoiceScopeRunner(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1748
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 1750
    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 1751
    invoke-static {v3}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 1749
    invoke-static {v1, v2, v3, v4, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleInputFactory;->createInputForAddPaymentSchedule(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/CurrencyCode;Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    move-result-object p1

    .line 1748
    invoke-interface {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startAddPaymentSchedule(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V

    return-void
.end method

.method public synthetic lambda$goToAutomaticRemindersScreen$12$EditInvoiceScopeRunner(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 730
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->remindersInfoFactory:Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

    .line 731
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 732
    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v2

    .line 733
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v3

    .line 731
    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;->createForInvoice(ZLcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object v0

    .line 735
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->remindersInfoFactory:Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

    .line 737
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 738
    invoke-virtual {v3}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v3

    .line 739
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v4

    .line 736
    invoke-virtual {v1, p2, v2, v3, v4}, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;->createDefaultListForInvoice(Ljava/util/List;ZLcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    move-result-object p2

    .line 742
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    invoke-interface {v1, v0, p2, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startReminders(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V

    return-void
.end method

.method public synthetic lambda$goToEditPaymentSchedule$51$EditInvoiceScopeRunner(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1764
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 1766
    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 1767
    invoke-static {v3}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v3

    .line 1765
    invoke-static {v1, v2, v3, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleInputFactory;->createInputForEditPaymentSchedule(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/util/List;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;

    move-result-object p1

    .line 1764
    invoke-interface {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startAddPaymentSchedule(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V

    return-void
.end method

.method public synthetic lambda$invoicePreviewScreenData$53$EditInvoiceScopeRunner(Ljava/lang/Boolean;)Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1973
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->previewScreenDataFactory:Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$Factory;

    .line 1974
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 1975
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$PreviewType;->RECURRING:Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$PreviewType;

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$PreviewType;->INVOICE:Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$PreviewType;

    :goto_0
    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->invoice_preview_title:I

    .line 1976
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1977
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getSendButtonText()Ljava/lang/String;

    move-result-object v4

    .line 1978
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 1973
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$Factory;->create(Ljava/lang/String;Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data$PreviewType;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/invoices/edit/preview/InvoicePreviewScreen$Data;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$manuallySaveDraft$35$EditInvoiceScopeRunner()V
    .locals 2

    .line 1158
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1159
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$manuallySaveDraft$36$EditInvoiceScopeRunner()V
    .locals 2

    .line 1162
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1163
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$manuallySaveDraftAndShowConfirmation$40$EditInvoiceScopeRunner(ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 1173
    new-instance v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$rH4VVQgrbGGhGWNsjywyRzGUH0g;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$rH4VVQgrbGGhGWNsjywyRzGUH0g;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Z)V

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$FfDVjDUAO_Bjq3oYyxEwhHa7eIc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$FfDVjDUAO_Bjq3oYyxEwhHa7eIc;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Z)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$manuallySaveDraftAndShowSetupPayments$44$EditInvoiceScopeRunner(Lcom/squareup/invoices/edit/InvoiceAction;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 1190
    new-instance v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$N3PRH8HD3rNqKMSXCR6O7BM5fFU;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$N3PRH8HD3rNqKMSXCR6O7BM5fFU;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$El5xnHqB5GUwVtTfCaj9V8tr0kE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$El5xnHqB5GUwVtTfCaj9V8tr0kE;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/invoices/edit/InvoiceAction;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$0$EditInvoiceScopeRunner(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 390
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busyLoadingInstruments:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$1$EditInvoiceScopeRunner()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 391
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busyLoadingInstruments:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$21$EditInvoiceScopeRunner(ZLcom/squareup/protos/client/invoice/DeleteDraftResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1085
    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleDeleteDraftResponse(Lcom/squareup/protos/client/invoice/DeleteDraftResponse;Z)V

    return-void
.end method

.method public synthetic lambda$null$22$EditInvoiceScopeRunner(ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1086
    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/invoice/DeleteDraftResponse;

    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleDeleteDraftResponse(Lcom/squareup/protos/client/invoice/DeleteDraftResponse;Z)V

    return-void
.end method

.method public synthetic lambda$null$25$EditInvoiceScopeRunner(Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1100
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleDeleteDraftSeriesResponse(Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;)V

    return-void
.end method

.method public synthetic lambda$null$26$EditInvoiceScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1101
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleDeleteDraftSeriesResponse(Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;)V

    return-void
.end method

.method public synthetic lambda$null$30$EditInvoiceScopeRunner(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1118
    iget-object p1, p1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->displayDetails:Lcom/squareup/invoices/DisplayDetails;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->resetWorkingInvoiceAfterMaterialization(Lcom/squareup/invoices/DisplayDetails;)V

    .line 1119
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->startCreatingFileAttachment()V

    return-void
.end method

.method public synthetic lambda$null$38$EditInvoiceScopeRunner(ZLcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1174
    iget-object p2, p2, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->userInitiatedDraftSaved(Lcom/squareup/protos/client/Status;Z)V

    return-void
.end method

.method public synthetic lambda$null$39$EditInvoiceScopeRunner(ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1176
    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;

    if-eqz p2, :cond_0

    .line 1178
    iget-object p2, p2, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->userInitiatedDraftSaved(Lcom/squareup/protos/client/Status;Z)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 1180
    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->userInitiatedDraftSaved(Lcom/squareup/protos/client/Status;Z)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$42$EditInvoiceScopeRunner(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1192
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setupGuideDialogScreenDataFactory:Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    sget-object v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->SAVE_DRAFT:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    .line 1193
    invoke-virtual {p1, v0}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;->forSetupPaymentsReminder(Ljava/lang/Object;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    move-result-object p1

    .line 1194
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setupGuideDialogScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 1195
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/setupguide/SetupPaymentsDialog;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v0, v1}, Lcom/squareup/setupguide/SetupPaymentsDialog;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$43$EditInvoiceScopeRunner(Lcom/squareup/invoices/edit/InvoiceAction;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1199
    invoke-virtual {p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;

    if-eqz p2, :cond_0

    .line 1201
    new-instance v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;

    iget-object p2, p2, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;)V

    goto :goto_0

    .line 1203
    :cond_0
    new-instance v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;

    const/4 p2, 0x0

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;)V

    :goto_0
    const/4 p2, 0x0

    .line 1205
    iget-object v1, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorTitle:Ljava/lang/CharSequence;

    iget-object v0, v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$InvoiceServiceErrorStrings;->errorDescription:Ljava/lang/CharSequence;

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$null$46$EditInvoiceScopeRunner(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1619
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    sget-object v0, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->RESTART:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    invoke-interface {p1, v0}, Lcom/squareup/onboarding/OnboardingDiverter;->maybeDivertToOnboardingOrBank(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V

    return-void
.end method

.method public synthetic lambda$null$48$EditInvoiceScopeRunner(Lcom/squareup/invoices/edit/InvoiceAction;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1621
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/features/invoices/R$string;->network_error_failed:I

    sget-object v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$_HkQeYEpOgXfkgnCQWCPXmbSd-M;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$_HkQeYEpOgXfkgnCQWCPXmbSd-M;

    .line 1622
    invoke-virtual {v0, p2, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p2

    .line 1624
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v7, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;

    .line 1625
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getPath()Lcom/squareup/invoices/edit/EditInvoiceScope;

    move-result-object v2

    .line 1626
    invoke-virtual {p2}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v6

    const/4 v4, 0x0

    move-object v1, v7

    move-object v3, p1

    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/edit/confirmation/InvoiceConfirmationScreen;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScope;Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 1624
    invoke-virtual {v0, v7}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$3$EditInvoiceScopeRunner(Ljava/lang/String;)Lrx/Observable;
    .locals 1

    if-nez p1, :cond_0

    .line 387
    sget-object p1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->EMPTY_INSTRUMENTS:Ljava/util/List;

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-interface {v0, p1}, Lcom/squareup/crm/RolodexServiceHelper;->getContact(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$kbgmhDMwCzeEYfYopVc6Jf9crE0;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$kbgmhDMwCzeEYfYopVc6Jf9crE0;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 390
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$RD6cNFG3c7mfcmUbmCI7mWSQzo4;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$RD6cNFG3c7mfcmUbmCI7mWSQzo4;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 391
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p1

    .line 389
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    .line 392
    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$7H4jq9Lbdd0SGV4rlweRYgQd_sU;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$7H4jq9Lbdd0SGV4rlweRYgQd_sU;

    .line 393
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$4$EditInvoiceScopeRunner(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 409
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    invoke-static {v0, p1}, Lcom/squareup/container/Flows;->pushStack(Lflow/Flow;Ljava/util/List;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$5$EditInvoiceScopeRunner(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 414
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DeliveryResult;

    if-eqz v0, :cond_0

    .line 415
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DeliveryResult;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleDeliveryMethodResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DeliveryResult;)V

    goto :goto_0

    .line 416
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecurringResult;

    if-eqz v0, :cond_1

    .line 417
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecurringResult;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleRecurringResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$RecurringResult;)V

    goto :goto_0

    .line 418
    :cond_1
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;

    if-eqz v0, :cond_2

    .line 419
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleScheduledDateSelectedResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$ScheduledDateResult;)V

    goto :goto_0

    .line 420
    :cond_2
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;

    if-eqz v0, :cond_3

    .line 421
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleDueDateSelectedResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$DueDateResult;)V

    goto :goto_0

    .line 422
    :cond_3
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$MessageResult;

    if-eqz v0, :cond_4

    .line 423
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$MessageResult;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleInvoiceMessageResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$MessageResult;)V

    goto :goto_0

    .line 424
    :cond_4
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentRequestResult;

    if-eqz v0, :cond_5

    .line 425
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentRequestResult;

    .line 426
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentRequestResult;->getEditPaymentRequestResult()Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;

    move-result-object v0

    .line 427
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentRequestResult;->getIndex()I

    move-result p1

    .line 425
    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handlePaymentRequestResult(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestResult;I)V

    goto :goto_0

    .line 428
    :cond_5
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentScheduleResult;

    if-eqz v0, :cond_6

    .line 429
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentScheduleResult;

    .line 430
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$PaymentScheduleResult;->getEditPaymentScheduleResult()Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;

    move-result-object p1

    .line 429
    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handlePaymentScheduleResult(Lcom/squareup/invoices/workflow/edit/EditPaymentScheduleResult;)V

    goto :goto_0

    .line 431
    :cond_6
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutoRemindersResult;

    if-eqz v0, :cond_7

    .line 432
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutoRemindersResult;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleAutoRemindersResult(Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AutoRemindersResult;)V

    goto :goto_0

    .line 433
    :cond_7
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AttachmentResult;

    if-eqz v0, :cond_8

    .line 434
    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AttachmentResult;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceResult$AttachmentResult;->getInvoiceAttachmentResult()Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleAttachmentResult(Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;)V

    .line 438
    :goto_0
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goBackToEditInvoice()V

    return-void

    .line 436
    :cond_8
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unknown result type."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic lambda$onEnterScope$7$EditInvoiceScopeRunner(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)V
    .locals 6

    .line 445
    instance-of v0, p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    if-eqz v0, :cond_1

    .line 447
    check-cast p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    .line 448
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    .line 449
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v2}, Lcom/squareup/payment/Order;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 450
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    sget-object v2, Lflow/Direction;->BACKWARD:Lflow/Direction;

    const/4 v3, 0x2

    new-array v3, v3, [Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    .line 452
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getHistoryFuncForConfirmation()Lkotlin/jvm/functions/Function1;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 453
    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getHistoryFuncForBackout()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    aput-object p1, v3, v4

    .line 450
    invoke-static {v1, v2, v3}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    .line 454
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/invoices/analytics/AddCustomerToInvoiceEvent;

    iget-object v2, v0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    if-nez v2, :cond_0

    const-string v0, "No contact token"

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    :goto_0
    invoke-direct {v1, v0}, Lcom/squareup/invoices/analytics/AddCustomerToInvoiceEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 457
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    return-void
.end method

.method public synthetic lambda$onSetupPaymentsDialogPrimaryClicked$49$EditInvoiceScopeRunner(Lcom/squareup/invoices/edit/InvoiceAction;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 1618
    new-instance v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$_VQcRjwGdeUhpoi4X1AbOQNaWwM;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$_VQcRjwGdeUhpoi4X1AbOQNaWwM;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$KI5y_eeOyldO6qQxKhHYE82C1T8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$KI5y_eeOyldO6qQxKhHYE82C1T8;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/invoices/edit/InvoiceAction;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$preview$32$EditInvoiceScopeRunner()V
    .locals 2

    .line 1129
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1130
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$preview$33$EditInvoiceScopeRunner(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;)V
    .locals 2

    .line 1133
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1134
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 1135
    iget-object v0, p1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$GenericSaveDraftResponse;->displayDetails:Lcom/squareup/invoices/DisplayDetails;

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->previewAfterInvoiceMaterialized(Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/DisplayDetails;)V

    return-void
.end method

.method public synthetic lambda$preview$34$EditInvoiceScopeRunner(Ljava/lang/Throwable;)V
    .locals 2

    .line 1137
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1138
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 1139
    instance-of v0, p1, Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    .line 1140
    invoke-direct {p0, p1, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->previewAfterInvoiceMaterialized(Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/DisplayDetails;)V

    return-void

    .line 1142
    :cond_0
    new-instance v0, Lrx/exceptions/OnErrorNotImplementedException;

    invoke-direct {v0, p1}, Lrx/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public synthetic lambda$saveThenStartFileAttachmentCreation$28$EditInvoiceScopeRunner()V
    .locals 2

    .line 1108
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1109
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$saveThenStartFileAttachmentCreation$31$EditInvoiceScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 1114
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1115
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 1116
    new-instance v0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$qoReb0LOXqh7_KlvHELCvqB3LhI;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$qoReb0LOXqh7_KlvHELCvqB3LhI;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$GsDpW2K5ic8qbNdemKvfZueqOqc;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$GsDpW2K5ic8qbNdemKvfZueqOqc;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$scheduleSeries$17$EditInvoiceScopeRunner()V
    .locals 2

    .line 1057
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1058
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$scheduleSeries$18$EditInvoiceScopeRunner(Lcom/squareup/invoices/edit/InvoiceAction;Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;)V
    .locals 2

    .line 1061
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1062
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 1063
    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleScheduleSeries(Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;Lcom/squareup/invoices/edit/InvoiceAction;)V

    return-void
.end method

.method public synthetic lambda$scheduleSeries$19$EditInvoiceScopeRunner(Lcom/squareup/invoices/edit/InvoiceAction;Ljava/lang/Throwable;)V
    .locals 2

    .line 1066
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1067
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 1068
    instance-of v0, p2, Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    const/4 p2, 0x0

    .line 1069
    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleScheduleSeries(Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;Lcom/squareup/invoices/edit/InvoiceAction;)V

    return-void

    .line 1071
    :cond_0
    new-instance p1, Lrx/exceptions/OnErrorNotImplementedException;

    invoke-direct {p1, p2}, Lrx/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method

.method public synthetic lambda$sendSingleInvoice$14$EditInvoiceScopeRunner()V
    .locals 2

    .line 1012
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1013
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$sendSingleInvoice$15$EditInvoiceScopeRunner(ZLcom/squareup/invoices/edit/InvoiceAction;Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;)V
    .locals 2

    .line 1016
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1017
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    if-nez p1, :cond_0

    .line 1019
    invoke-direct {p0, p3}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleSingleSendOrScheduleResponse(Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;)V

    goto :goto_0

    .line 1021
    :cond_0
    iget-object p1, p3, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p3, p3, Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleSingleResendOrUpdate(Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$sendSingleInvoice$16$EditInvoiceScopeRunner(ZLcom/squareup/invoices/edit/InvoiceAction;Ljava/lang/Throwable;)V
    .locals 2

    .line 1025
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 1026
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 1027
    instance-of v0, p3, Lretrofit/RetrofitError;

    if-eqz v0, :cond_1

    const/4 p3, 0x0

    if-nez p1, :cond_0

    .line 1029
    invoke-direct {p0, p3}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleSingleSendOrScheduleResponse(Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;)V

    goto :goto_0

    .line 1031
    :cond_0
    invoke-direct {p0, p3, p2, p3}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->handleSingleResendOrUpdate(Lcom/squareup/protos/client/Status;Lcom/squareup/invoices/edit/InvoiceAction;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    :goto_0
    return-void

    .line 1034
    :cond_1
    new-instance p1, Lrx/exceptions/OnErrorNotImplementedException;

    invoke-direct {p1, p3}, Lrx/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method

.method newOrEditingDraft()Z
    .locals 1

    .line 1412
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isNew()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->isDraft()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method numberOfFilesWithinLimit()Z
    .locals 2

    .line 1480
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 1481
    invoke-interface {v1}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->getFileAttachmentLimits()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->max_count:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    .line 364
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/EditInvoiceScope;

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    .line 365
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScope;->getEditInvoiceContext()Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    .line 367
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunnerKt;->getEditInvoiceWorkflowRunner(Lmortar/MortarScope;)Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    .line 369
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    if-nez v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    new-instance v1, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-direct {v1, v2, v3}, Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/time/CurrentTime;)V

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 372
    invoke-interface {v2}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->getCurrentDefaultMessage()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 373
    invoke-interface {v3}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->getInvoiceDefaults()Lcom/squareup/protos/client/invoice/InvoiceDefaults;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    .line 370
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->constructInitialTemplateToEdit(Lcom/squareup/invoices/edit/contexts/InvoiceCreationContext;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceDefaults;Lcom/squareup/settings/server/Features;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 375
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->recurrenceRule:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceContext:Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    invoke-virtual {v2}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->getInitialRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 376
    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->initWithInvoice(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    goto :goto_0

    .line 379
    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->initWithInvoice(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    .line 382
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_COF:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->contactIdForInstruments:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 384
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$ARRnAGcxcQDzrr6xype1gi48esU;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$ARRnAGcxcQDzrr6xype1gi48esU;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 385
    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->instrumentsForContact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 404
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 383
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 407
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    .line 408
    invoke-interface {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$3XQMeGWZbXGFNXZpYPE5u2LuFbQ;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$3XQMeGWZbXGFNXZpYPE5u2LuFbQ;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 409
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 407
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 411
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    .line 412
    invoke-interface {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$N2RJy14a_Jxhe16e5VlhU3BF24g;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$N2RJy14a_Jxhe16e5VlhU3BF24g;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 413
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 411
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 441
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    .line 442
    invoke-virtual {v0}, Lcom/squareup/ui/crm/ChooseCustomerFlow;->getResults()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$VLyO6xXx-cqOnRsDwnV1wBYos0E;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$VLyO6xXx-cqOnRsDwnV1wBYos0E;

    .line 443
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$X3_hsLVIYczlA2nMZBO-db73cWg;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$X3_hsLVIYczlA2nMZBO-db73cWg;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 444
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 441
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 555
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    .line 556
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    const/4 v0, 0x0

    .line 557
    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 558
    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 531
    :cond_0
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string/jumbo v1, "workingInvoice"

    .line 532
    invoke-static {v0, p1, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/Invoice;

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/Invoice;->newBuilder()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    .line 533
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItemBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v1, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/WorkingItem;

    iput-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 534
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingDiscountBundleKey:Lcom/squareup/BundleKey;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/configure/item/WorkingDiscount;

    iput-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    const-string v1, "keypadInfo"

    .line 535
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    iput-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    const-string v1, "savedByPreviewOrUpload"

    .line 536
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->savedByPreviewOrUpload:Z

    .line 537
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->customAmountCartItemBundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v1, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/CartItem;

    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->customAmountCartItem:Lcom/squareup/checkout/CartItem;

    .line 539
    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->initWithInvoice(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V

    return-void
.end method

.method public onPaymentRequestClicked(I)V
    .locals 6

    .line 1898
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    .line 1899
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->payment_request:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 1900
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v2

    .line 1901
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-static {v4}, Lcom/squareup/invoices/InvoiceDatesKt;->yearMonthDay(Lcom/squareup/time/CurrentTime;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v4

    const/4 v5, 0x0

    .line 1899
    invoke-static {v1, v2, v3, v5, v4}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestState;->createInfo(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/invoices/DisplayDetails;ZLcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;

    move-result-object v1

    .line 1898
    invoke-interface {v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startEditingPaymentRequest(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;I)V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 543
    sget-object v0, Lcom/squareup/protos/client/invoice/Invoice;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->build()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object v0

    const-string/jumbo v1, "workingInvoice"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 544
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItemBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 545
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingDiscountBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 546
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    const-string v1, "keypadInfo"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 547
    iget-boolean v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->savedByPreviewOrUpload:Z

    const-string v1, "savedByPreviewOrUpload"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 551
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->customAmountCartItemBundleKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->peekUninterestingItem()Lcom/squareup/checkout/CartItem;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method

.method public onSaveDraftClicked()V
    .locals 1

    .line 1148
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->shouldCompleteOnboardingSetUp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1149
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->manuallySaveDraftAndShowSetupPayments()V

    goto :goto_0

    .line 1151
    :cond_0
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->manuallySaveDraftAndShowConfirmation()V

    :goto_0
    return-void
.end method

.method public onSaveDraftFromPreviewClicked()V
    .locals 3

    .line 2002
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->shouldCompleteOnboardingSetUp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2003
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setupGuideDialogScreenDataFactory:Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    sget-object v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->SAVE_DRAFT:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    .line 2004
    invoke-virtual {v0, v1}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;->forSetupPaymentsReminder(Ljava/lang/Object;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    move-result-object v0

    .line 2005
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setupGuideDialogScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 2006
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/setupguide/SetupPaymentsDialog;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v1, v2}, Lcom/squareup/setupguide/SetupPaymentsDialog;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 2008
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->showDraftConfirmation()V

    :goto_0
    return-void
.end method

.method public onSendClicked()V
    .locals 3

    .line 948
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->shouldCompleteOnboardingSetUp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 949
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setupGuideDialogScreenDataFactory:Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    sget-object v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->SEND_INVOICE:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    .line 950
    invoke-virtual {v0, v1}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;->forSetupPaymentsPromptWhenSendingInvoice(Ljava/lang/Object;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    move-result-object v0

    .line 951
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setupGuideDialogScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 952
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/setupguide/SetupPaymentsDialog;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v1, v2}, Lcom/squareup/setupguide/SetupPaymentsDialog;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 954
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->sendNewOrExistingInvoice()V

    :goto_0
    return-void
.end method

.method public onSendFromPreviewClicked()V
    .locals 3

    .line 1983
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->shouldCompleteOnboardingSetUp()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getInvoiceValidationErrorIfAny()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1984
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setupGuideDialogScreenDataFactory:Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;

    sget-object v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->SEND_INVOICE:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    .line 1985
    invoke-virtual {v0, v1}, Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData$Factory;->forSetupPaymentsPromptWhenSendingInvoice(Ljava/lang/Object;)Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;

    move-result-object v0

    .line 1986
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setupGuideDialogScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 1987
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/setupguide/SetupPaymentsDialog;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v1, v2}, Lcom/squareup/setupguide/SetupPaymentsDialog;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 1989
    :cond_0
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->sendFromPreview()V

    :goto_0
    return-void
.end method

.method public onSetupPaymentsDialogPrimaryClicked(Ljava/lang/Object;)V
    .locals 4

    .line 1599
    check-cast p1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    .line 1602
    sget-object v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->SAVE_DRAFT:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    if-ne p1, v0, :cond_1

    .line 1603
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1604
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SAVE_RECURRING:Lcom/squareup/invoices/edit/InvoiceAction;

    goto :goto_0

    .line 1606
    :cond_0
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SAVE:Lcom/squareup/invoices/edit/InvoiceAction;

    goto :goto_0

    .line 1609
    :cond_1
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SEND:Lcom/squareup/invoices/edit/InvoiceAction;

    .line 1612
    :goto_0
    sget-object v1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$1;->$SwitchMap$com$squareup$invoices$edit$EditInvoiceScopeRunner$SetupPaymentsDialogKey:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    goto :goto_1

    .line 1632
    :cond_2
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/invoices/analytics/SetupPaymentDialogSetupPaymentTapped;

    const-string v1, "Save Invoice"

    invoke-direct {v0, v1}, Lcom/squareup/invoices/analytics/SetupPaymentDialogSetupPaymentTapped;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 1633
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    sget-object v0, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->RESTART:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    invoke-interface {p1, v0}, Lcom/squareup/onboarding/OnboardingDiverter;->maybeDivertToOnboardingOrBank(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V

    goto :goto_1

    .line 1614
    :cond_3
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/invoices/analytics/SetupPaymentDialogSetupPaymentTapped;

    const-string v2, "Send Invoice"

    invoke-direct {v1, v2}, Lcom/squareup/invoices/analytics/SetupPaymentDialogSetupPaymentTapped;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 1615
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->manuallySaveDraft()Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v3, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$pwVwhrpkplSKnxRbL6t1W9QSkMY;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$pwVwhrpkplSKnxRbL6t1W9QSkMY;

    .line 1616
    invoke-static {v2, v3}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$X5ziwLfCe9fUjLXZb7JBQ6cssDY;

    invoke-direct {v2, p0, v0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$X5ziwLfCe9fUjLXZb7JBQ6cssDY;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/invoices/edit/InvoiceAction;)V

    .line 1617
    invoke-virtual {v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 1615
    invoke-virtual {p1, v0}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    :goto_1
    return-void
.end method

.method public onSetupPaymentsDialogSecondaryClicked(Ljava/lang/Object;)V
    .locals 2

    .line 1639
    check-cast p1, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;

    .line 1640
    sget-object v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$1;->$SwitchMap$com$squareup$invoices$edit$EditInvoiceScopeRunner$SetupPaymentsDialogKey:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$SetupPaymentsDialogKey;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 1642
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/invoices/analytics/SetupPaymentDialogLaterTapped;

    const-string v1, "Save Invoice"

    invoke-direct {v0, v1}, Lcom/squareup/invoices/analytics/SetupPaymentDialogLaterTapped;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 1643
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->showDraftConfirmation()V

    goto :goto_0

    .line 1646
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/invoices/analytics/SetupPaymentDialogLaterTapped;

    const-string v1, "Send Invoice"

    invoke-direct {v0, v1}, Lcom/squareup/invoices/analytics/SetupPaymentDialogLaterTapped;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-void
.end method

.method public preview()V
    .locals 4

    .line 1127
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->saveSingleOrRecurringDraft(Z)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$oRQIMrdVJ-_k1yFLuWxk3Xu9TIA;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$oRQIMrdVJ-_k1yFLuWxk3Xu9TIA;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 1128
    invoke-virtual {v1, v2}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$ONntT2IUxNU_kLhvYq1XGq39jz0;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$ONntT2IUxNU_kLhvYq1XGq39jz0;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    new-instance v3, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$GmAjPhFV1A40ptryg57-3K3Z760;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$GmAjPhFV1A40ptryg57-3K3Z760;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 1132
    invoke-virtual {v1, v2, v3}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v1

    .line 1127
    invoke-virtual {v0, v1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method public recurrenceRule()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
            ">;"
        }
    .end annotation

    .line 1468
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->recurrenceRule:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public removeFileAttachment(Ljava/lang/String;)V
    .locals 3

    .line 1509
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1510
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1512
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1513
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 1514
    iget-object v2, v2, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1515
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 1519
    :cond_1
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->fileMetadataList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public removeItemFromCart(I)V
    .locals 1

    .line 875
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/Order;->removeItem(I)Lcom/squareup/checkout/CartItem;

    .line 876
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->finishEditingCart()V

    .line 877
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goBackToEditInvoice()V

    return-void
.end method

.method resetCofDeliveryMethod()V
    .locals 3

    .line 654
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/invoices/Invoices;->updateInstrumentToken(Lcom/squareup/protos/client/invoice/Invoice$Builder;Ljava/lang/String;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 655
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v0, v2, :cond_0

    .line 656
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    sget-object v2, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-static {v0, v2}, Lcom/squareup/invoices/Invoices;->updatePaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 658
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->contactIdForInstruments:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public resetUploadResult()V
    .locals 2

    .line 679
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->uploadResult:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public scheduleSeries(Lcom/squareup/invoices/edit/InvoiceAction;)V
    .locals 4

    .line 1051
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoiceToSend()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    .line 1052
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->recurrenceRule:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    .line 1053
    iget-object v2, v0, Lcom/squareup/protos/client/invoice/Invoice;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v1, v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->toRecurringSchedule(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/RecurringSchedule;

    move-result-object v1

    .line 1055
    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->subscriptions:Lrx/subscriptions/CompositeSubscription;

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v3, v0, v1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->scheduleRecurringSeries(Lcom/squareup/protos/client/invoice/Invoice;Lcom/squareup/protos/client/invoice/RecurringSchedule;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$BsAyzsuKUp1IiaCc5Kkbj3Yr6zs;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$BsAyzsuKUp1IiaCc5Kkbj3Yr6zs;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;)V

    .line 1056
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$bc2rKQ87CzZOAM6zL38vn9z5l4k;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$bc2rKQ87CzZOAM6zL38vn9z5l4k;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/invoices/edit/InvoiceAction;)V

    new-instance v3, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$TLBpM9HSLQ1e92Z9N2ALfkSoH5w;

    invoke-direct {v3, p0, p1}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$TLBpM9HSLQ1e92Z9N2ALfkSoH5w;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/invoices/edit/InvoiceAction;)V

    .line 1059
    invoke-virtual {v0, v1, v3}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 1055
    invoke-virtual {v2, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method public sendNewOrExistingInvoice()V
    .locals 2

    .line 963
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingNonDraftSentOrSharedSingleInvoice()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 964
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    if-ne v0, v1, :cond_0

    .line 965
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->UPDATE:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->sendSingleInvoice(Lcom/squareup/invoices/edit/InvoiceAction;)V

    goto :goto_0

    .line 966
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingEmailedInvoice()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 967
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->RESEND:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->sendSingleInvoice(Lcom/squareup/invoices/edit/InvoiceAction;)V

    goto :goto_0

    .line 972
    :cond_1
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SEND:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->sendSingleInvoice(Lcom/squareup/invoices/edit/InvoiceAction;)V

    goto :goto_0

    .line 974
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingNonDraftSeries()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 975
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->UPDATE_SERIES:Lcom/squareup/invoices/edit/InvoiceAction;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->showRecurringMonthlyDialogIfNecessary(Lcom/squareup/invoices/edit/InvoiceAction;)V

    goto :goto_0

    .line 977
    :cond_3
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->sendNewInvoice()V

    :goto_0
    return-void
.end method

.method public setCart(Lcom/squareup/protos/client/bills/Cart;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 805
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->cart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    :cond_0
    return-void
.end method

.method setContactId(Ljava/lang/String;)V
    .locals 1

    .line 642
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->contactIdForInstruments:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setOrder(Lcom/squareup/payment/Order;)V
    .locals 0

    .line 703
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->order:Lcom/squareup/payment/Order;

    return-void
.end method

.method public setWorkingInvoice(Lcom/squareup/protos/client/invoice/Invoice$Builder;)V
    .locals 0

    .line 691
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-void
.end method

.method public setupPaymentsDialogScreenData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;",
            ">;"
        }
    .end annotation

    .line 1595
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->setupGuideDialogScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method showDraftConfirmation()V
    .locals 3

    .line 944
    sget-object v0, Lcom/squareup/invoices/edit/InvoiceAction;->SAVE:Lcom/squareup/invoices/edit/InvoiceAction;

    const-string v1, ""

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2, v1, v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->goToConfirmationFromEditScreen(Lcom/squareup/invoices/edit/InvoiceAction;ZLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void
.end method

.method startCreatingFileAttachment()V
    .locals 7

    .line 1905
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->noServerId(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1906
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->saveThenStartFileAttachmentCreation()V

    return-void

    .line 1910
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Upload;

    new-instance v2, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Invoice;

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v3, v3, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Invoice;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    iget-object v4, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v4, v4, Lcom/squareup/protos/client/invoice/Invoice$Builder;->attachment:Ljava/util/List;

    sget v5, Lcom/squareup/features/invoices/R$string;->invoice_attachment_default_name:I

    .line 1912
    invoke-virtual {v3, v4, v5}, Lcom/squareup/invoices/image/InvoiceFileHelper;->nextFileAttachmentDefaultTitle(Ljava/util/List;I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;

    .line 1914
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getTotalSizeBytesUploaded()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;-><init>(J)V

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Upload;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/UploadValidationInfo;)V

    .line 1910
    invoke-interface {v0, v1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startInvoiceAttachment(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;)V

    return-void
.end method

.method public startEditingDeliveryMethod()V
    .locals 7

    .line 1869
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_delivery_method_title:I

    .line 1870
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1871
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getInstruments()Lrx/Observable;

    move-result-object v2

    invoke-static {v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v2

    .line 1872
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->busyLoadingInstruments()Lrx/Observable;

    move-result-object v3

    invoke-static {v3}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v3

    .line 1873
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/invoices/Invoices;->getPaymentMethod(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->shareLinkMessageFactory:Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;

    .line 1874
    invoke-virtual {v5}, Lcom/squareup/invoices/ui/edit/InvoiceShareLinkMessageFactory;->getShareLinkMessage()Ljava/lang/CharSequence;

    move-result-object v5

    .line 1875
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getWorkingInvoice()Lcom/squareup/protos/client/invoice/Invoice$Builder;

    move-result-object v6

    invoke-static {v6}, Lcom/squareup/invoices/Invoices;->getInstrumentToken(Lcom/squareup/protos/client/invoice/Invoice$Builder;)Ljava/lang/String;

    move-result-object v6

    .line 1869
    invoke-interface/range {v0 .. v6}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startDeliveryMethod(Ljava/lang/String;Lio/reactivex/Observable;Lio/reactivex/Observable;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/CharSequence;Ljava/lang/String;)V

    return-void
.end method

.method public startEditingDiscount(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const/4 v0, 0x0

    .line 882
    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 883
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingDiscount:Lcom/squareup/configure/item/WorkingDiscount;

    .line 884
    iput-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    .line 885
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance p2, Lcom/squareup/orderentry/KeypadEntryScreen;

    invoke-direct {p2, p3}, Lcom/squareup/orderentry/KeypadEntryScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public startEditingFrequency()V
    .locals 5

    .line 1864
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->recurrenceRule:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v3, v3, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 1865
    invoke-static {v3}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isEditingNonDraftSeries()Z

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Ljava/util/Date;Z)V

    .line 1864
    invoke-interface {v0, v1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startRecurring(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;)V

    return-void
.end method

.method public startEditingItemVariablePrice(Lcom/squareup/configure/item/WorkingItem;Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 5

    .line 816
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 817
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/WorkingItem;->selectVariation(I)V

    .line 818
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, v1}, Lcom/squareup/configure/item/WorkingItem;->setZeroCurrentAmount(Lcom/squareup/protos/common/CurrencyCode;)V

    .line 819
    new-instance v0, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    invoke-virtual {p1}, Lcom/squareup/configure/item/WorkingItem;->total()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/configure/item/WorkingItem;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/configure/item/R$string;->add:I

    .line 820
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/squareup/configure/item/WorkingItemUtilsKt;->getUnitAbbreviation(Lcom/squareup/configure/item/WorkingItem;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;-><init>(Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->keypadInfo:Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;

    .line 822
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/orderentry/KeypadEntryScreen;

    invoke-direct {v0, p2}, Lcom/squareup/orderentry/KeypadEntryScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public startEditingItemWithModifiers(Lcom/squareup/configure/item/WorkingItem;ZLcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 3

    .line 828
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingItem:Lcom/squareup/configure/item/WorkingItem;

    .line 830
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$EuQ93PJnJ31A_L7wRYAjM3W0pww;

    invoke-direct {v2, p3, p1, p2}, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoiceScopeRunner$EuQ93PJnJ31A_L7wRYAjM3W0pww;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;Z)V

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public startEditingOrder(I)V
    .locals 3

    .line 852
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;

    iget-object v2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->path:Lcom/squareup/invoices/edit/EditInvoiceScope;

    invoke-direct {v1, v2, p1}, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;I)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method startUpdatingFileAttachment(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V
    .locals 8

    .line 1919
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    new-instance v7, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;

    new-instance v2, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Invoice;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-direct {v2, v1}, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Invoice;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "."

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    iget-object v6, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->mime_type:Ljava/lang/String;

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$Update;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v7}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startInvoiceAttachment(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;)V

    return-void
.end method

.method public startUpdatingInvoiceMessage()V
    .locals 3

    .line 1894
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->editInvoiceWorkflowRunner:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice$Builder;->description:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->startInvoiceMessage(Ljava/lang/String;Z)V

    return-void
.end method

.method public toggleAutomaticReminders(Z)V
    .locals 2

    .line 931
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->automatic_reminders_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    .line 932
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/analytics/RegisterTapName;->AUTOMATIC_REMINDERS_ENABLED:Lcom/squareup/analytics/RegisterTapName;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/analytics/RegisterTapName;->AUTOMATIC_REMINDERS_DISABLED:Lcom/squareup/analytics/RegisterTapName;

    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method updateBuyerCofEnabled(Z)V
    .locals 1

    .line 1456
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_instrument_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-void
.end method

.method updateRemainderToBeDueAfterDeposit(Ljava/util/List;J)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/PaymentRequest;",
            ">;J)V"
        }
    .end annotation

    .line 1958
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 1962
    invoke-static {p1}, Lcom/squareup/invoices/PaymentRequestsKt;->getRemainderPaymentRequest(Ljava/util/List;)Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/PaymentRequest;->newBuilder()Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object v0

    const-wide/16 v1, 0x1e

    add-long/2addr p2, v1

    .line 1963
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->relative_due_on(Ljava/lang/Long;)Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    move-result-object p2

    .line 1964
    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->build()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object p2

    .line 1966
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p3

    add-int/lit8 p3, p3, -0x1

    invoke-interface {p1, p3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1967
    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    .line 1959
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Expected a list of PaymentRequests with at least one deposit and one remainder."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method updateScheduledAt(Lcom/squareup/protos/common/time/YearMonthDay;)V
    .locals 3

    .line 765
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v0, :cond_0

    .line 767
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->isRecurring()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 768
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->recurrenceRule:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    .line 769
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getRecurrenceEnd()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    move-result-object v1

    .line 771
    instance-of v2, v1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    if-eqz v2, :cond_0

    .line 772
    check-cast v1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;->getDate()Ljava/util/Date;

    move-result-object v1

    .line 773
    invoke-static {v1}, Lcom/squareup/util/ProtoDates;->dateToYmd(Ljava/util/Date;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/util/ProtoDates;->compareYmd(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 774
    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->recurrenceRule:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->endDate(Ljava/util/Date;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 779
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->scheduled_at(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-void
.end method

.method updateShippingAddressEnabled(Z)V
    .locals 1

    .line 1452
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->workingInvoice:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/Invoice$Builder;->buyer_entered_shipping_address_enabled(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/Invoice$Builder;

    return-void
.end method

.method public uploadResult()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$UploadResult;",
            ">;"
        }
    .end annotation

    .line 675
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->uploadResult:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method
