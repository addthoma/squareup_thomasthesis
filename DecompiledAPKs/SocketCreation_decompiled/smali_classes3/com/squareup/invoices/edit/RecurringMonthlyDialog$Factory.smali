.class public Lcom/squareup/invoices/edit/RecurringMonthlyDialog$Factory;
.super Ljava/lang/Object;
.source "RecurringMonthlyDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/RecurringMonthlyDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/invoices/edit/InvoiceAction;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->scheduleSeries(Lcom/squareup/invoices/edit/InvoiceAction;)V

    return-void
.end method

.method static synthetic lambda$create$1(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 46
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 33
    const-class v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;

    .line 34
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;

    .line 35
    invoke-interface {v0}, Lcom/squareup/invoices/edit/EditInvoiceScope$Component;->editInvoiceScopeRunner()Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    move-result-object v0

    .line 37
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/edit/RecurringMonthlyDialog;

    invoke-virtual {v1}, Lcom/squareup/invoices/edit/RecurringMonthlyDialog;->getInvoiceAction()Lcom/squareup/invoices/edit/InvoiceAction;

    move-result-object v1

    .line 39
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/features/invoices/R$string;->invoice_recurring_start_date_eom_error_title:I

    .line 40
    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_recurring_start_date_eom_error_message:I

    .line 41
    invoke-virtual {p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v2, Lcom/squareup/common/strings/R$string;->ok:I

    new-instance v3, Lcom/squareup/invoices/edit/-$$Lambda$RecurringMonthlyDialog$Factory$gGb1Totqba0Y-OUSSe_BlfK_xhE;

    invoke-direct {v3, v0, v1}, Lcom/squareup/invoices/edit/-$$Lambda$RecurringMonthlyDialog$Factory$gGb1Totqba0Y-OUSSe_BlfK_xhE;-><init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/invoices/edit/InvoiceAction;)V

    .line 42
    invoke-virtual {p1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    sget-object v1, Lcom/squareup/invoices/edit/-$$Lambda$RecurringMonthlyDialog$Factory$vtC1DTCi3ZcOPmlo_S6TzO_MRE8;->INSTANCE:Lcom/squareup/invoices/edit/-$$Lambda$RecurringMonthlyDialog$Factory$vtC1DTCi3ZcOPmlo_S6TzO_MRE8;

    .line 45
    invoke-virtual {p1, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 47
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 48
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 49
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
