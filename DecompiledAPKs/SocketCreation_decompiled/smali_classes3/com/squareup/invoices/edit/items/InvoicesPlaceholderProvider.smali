.class public final Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;
.super Ljava/lang/Object;
.source "InvoicesPlaceholderProvider.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;",
        "",
        "catalogIntegrationController",
        "Lcom/squareup/catalogapi/CatalogIntegrationController;",
        "(Lcom/squareup/catalogapi/CatalogIntegrationController;)V",
        "getPlaceholders",
        "",
        "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;


# direct methods
.method public constructor <init>(Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "catalogIntegrationController"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method


# virtual methods
.method public final getPlaceholders()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/invoices/edit/items/InvoicesPlaceholderProvider;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowPartiallyRolledOutFeature()Z

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x3

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    .line 18
    sget-object v5, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    aput-object v5, v0, v3

    sget-object v3, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->CUSTOM_AMOUNT:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    aput-object v3, v0, v2

    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_SERVICES:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_DISCOUNTS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    aput-object v1, v0, v4

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-array v0, v4, [Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    .line 20
    sget-object v4, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    aput-object v4, v0, v3

    sget-object v3, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->CUSTOM_AMOUNT:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    aput-object v3, v0, v2

    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_DISCOUNTS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    aput-object v2, v0, v1

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method
