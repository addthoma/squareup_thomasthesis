.class public interface abstract Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;
.super Ljava/lang/Object;
.source "ItemSelectScreenRunner.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J \u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH&J\u0018\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\t\u001a\u00020\nH&J \u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\t\u001a\u00020\nH&J\u0010\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013H&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;",
        "",
        "goBackFromItemSelect",
        "",
        "startEditingDiscount",
        "workingDiscount",
        "Lcom/squareup/configure/item/WorkingDiscount;",
        "info",
        "Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;",
        "parentPath",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "startEditingItemVariablePrice",
        "workingItem",
        "Lcom/squareup/configure/item/WorkingItem;",
        "startEditingItemWithModifiers",
        "showPriceScreenFirst",
        "",
        "startEditingOrder",
        "pos",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract goBackFromItemSelect()V
.end method

.method public abstract startEditingDiscount(Lcom/squareup/configure/item/WorkingDiscount;Lcom/squareup/orderentry/KeypadEntryScreen$KeypadInfo;Lcom/squareup/ui/main/RegisterTreeKey;)V
.end method

.method public abstract startEditingItemVariablePrice(Lcom/squareup/configure/item/WorkingItem;Lcom/squareup/ui/main/RegisterTreeKey;)V
.end method

.method public abstract startEditingItemWithModifiers(Lcom/squareup/configure/item/WorkingItem;ZLcom/squareup/ui/main/RegisterTreeKey;)V
.end method

.method public abstract startEditingOrder(I)V
.end method
