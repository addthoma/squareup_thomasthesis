.class public final Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;
.super Ljava/lang/Object;
.source "ItemSelectCardPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final entryHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleEntryHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final itemSelectScreenTutorialRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListAssistantProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleLibraryListAssistant;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListManager;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEditorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleLibraryListAssistant;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 75
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 76
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->orderEditorProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 77
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 78
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 79
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 80
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 81
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->libraryListManagerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 82
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->passcodeGatekeeperProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 83
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 84
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->entryHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 85
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->itemSelectScreenTutorialRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 86
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->libraryListAssistantProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 87
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 88
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 89
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 90
    iput-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/LibraryListManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleEntryHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/librarylist/SimpleLibraryListAssistant;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)",
            "Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 111
    new-instance v17, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v17
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;Ljavax/inject/Provider;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/permissions/PermissionGatekeeper;Lflow/Flow;Lcom/squareup/librarylist/SimpleEntryHandler;Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;Lcom/squareup/librarylist/SimpleLibraryListAssistant;Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/librarylist/LibraryListManager;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lflow/Flow;",
            "Lcom/squareup/librarylist/SimpleEntryHandler;",
            "Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;",
            "Lcom/squareup/librarylist/SimpleLibraryListAssistant;",
            "Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ")",
            "Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 122
    new-instance v17, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;Ljavax/inject/Provider;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/permissions/PermissionGatekeeper;Lflow/Flow;Lcom/squareup/librarylist/SimpleEntryHandler;Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;Lcom/squareup/librarylist/SimpleLibraryListAssistant;Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/catalogapi/CatalogIntegrationController;)V

    return-object v17
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;
    .locals 18

    move-object/from16 v0, p0

    .line 95
    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->orderEditorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;

    iget-object v4, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->libraryListManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/librarylist/LibraryListManager;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->passcodeGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->entryHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/librarylist/SimpleEntryHandler;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->itemSelectScreenTutorialRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->libraryListAssistantProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/librarylist/SimpleLibraryListAssistant;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static/range {v2 .. v17}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/invoices/edit/items/ItemSelectOrderEditor;Ljavax/inject/Provider;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/permissions/PermissionGatekeeper;Lflow/Flow;Lcom/squareup/librarylist/SimpleEntryHandler;Lcom/squareup/register/tutorial/ItemSelectScreenTutorialRunner;Lcom/squareup/librarylist/SimpleLibraryListAssistant;Lcom/squareup/invoices/edit/items/ItemSelectScreenRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/settings/server/Features;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter_Factory;->get()Lcom/squareup/invoices/edit/items/ItemSelectCardPresenter;

    move-result-object v0

    return-object v0
.end method
