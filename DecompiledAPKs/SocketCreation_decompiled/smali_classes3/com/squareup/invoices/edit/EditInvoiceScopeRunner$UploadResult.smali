.class public Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$UploadResult;
.super Ljava/lang/Object;
.source "EditInvoiceScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UploadResult"
.end annotation


# instance fields
.field public final attachmentToken:Ljava/lang/String;

.field public final isSuccessful:Z


# direct methods
.method private constructor <init>(ZLjava/lang/String;)V
    .locals 0

    .line 2087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2088
    iput-boolean p1, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$UploadResult;->isSuccessful:Z

    .line 2089
    iput-object p2, p0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$UploadResult;->attachmentToken:Ljava/lang/String;

    return-void
.end method

.method public static error()Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$UploadResult;
    .locals 3

    .line 2099
    new-instance v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$UploadResult;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$UploadResult;-><init>(ZLjava/lang/String;)V

    return-object v0
.end method

.method public static success(Ljava/lang/String;)Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$UploadResult;
    .locals 2

    .line 2094
    new-instance v0, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$UploadResult;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner$UploadResult;-><init>(ZLjava/lang/String;)V

    return-object v0
.end method
