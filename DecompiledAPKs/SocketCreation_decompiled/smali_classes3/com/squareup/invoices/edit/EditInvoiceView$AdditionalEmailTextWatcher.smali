.class Lcom/squareup/invoices/edit/EditInvoiceView$AdditionalEmailTextWatcher;
.super Lcom/squareup/invoices/edit/EditInvoiceView$InvoiceTextWatcher;
.source "EditInvoiceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/EditInvoiceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AdditionalEmailTextWatcher"
.end annotation


# instance fields
.field private hasAddedView:Z

.field final synthetic this$0:Lcom/squareup/invoices/edit/EditInvoiceView;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceView;Z)V
    .locals 0

    .line 717
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView$AdditionalEmailTextWatcher;->this$0:Lcom/squareup/invoices/edit/EditInvoiceView;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/edit/EditInvoiceView$InvoiceTextWatcher;-><init>(Lcom/squareup/invoices/edit/EditInvoiceView;)V

    .line 718
    iput-boolean p2, p0, Lcom/squareup/invoices/edit/EditInvoiceView$AdditionalEmailTextWatcher;->hasAddedView:Z

    return-void
.end method


# virtual methods
.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 724
    iget-boolean p2, p0, Lcom/squareup/invoices/edit/EditInvoiceView$AdditionalEmailTextWatcher;->hasAddedView:Z

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 725
    iget-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceView$AdditionalEmailTextWatcher;->this$0:Lcom/squareup/invoices/edit/EditInvoiceView;

    const/4 p2, 0x1

    const-string p3, ""

    invoke-static {p1, p3, p2}, Lcom/squareup/invoices/edit/EditInvoiceView;->access$000(Lcom/squareup/invoices/edit/EditInvoiceView;Ljava/lang/String;Z)V

    .line 726
    iput-boolean p2, p0, Lcom/squareup/invoices/edit/EditInvoiceView$AdditionalEmailTextWatcher;->hasAddedView:Z

    :cond_0
    return-void
.end method
