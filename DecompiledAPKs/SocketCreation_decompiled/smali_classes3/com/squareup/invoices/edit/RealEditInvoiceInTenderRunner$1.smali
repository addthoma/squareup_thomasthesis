.class Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "RealEditInvoiceInTenderRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner$1;->this$0:Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$success$0$RealEditInvoiceInTenderRunner$1(Lrx/Scheduler$Worker;)V
    .locals 1

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner$1;->this$0:Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;

    invoke-static {v0}, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->access$200(Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;)Lcom/squareup/invoices/edit/EditInvoiceGateway;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->goToNewInvoiceInTenderFlow()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    invoke-virtual {p1}, Lrx/Scheduler$Worker;->unsubscribe()V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lrx/Scheduler$Worker;->unsubscribe()V

    .line 110
    throw v0
.end method

.method public success()V
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner$1;->this$0:Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;

    invoke-static {v0}, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->access$000(Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->startInvoicePayment()Lcom/squareup/payment/InvoicePayment;

    .line 104
    iget-object v0, p0, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner$1;->this$0:Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;

    invoke-static {v0}, Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;->access$100(Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner;)Lrx/Scheduler;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Scheduler;->createWorker()Lrx/Scheduler$Worker;

    move-result-object v0

    .line 105
    new-instance v1, Lcom/squareup/invoices/edit/-$$Lambda$RealEditInvoiceInTenderRunner$1$JN69Be_csOTxguHWRAQdZtn2nJc;

    invoke-direct {v1, p0, v0}, Lcom/squareup/invoices/edit/-$$Lambda$RealEditInvoiceInTenderRunner$1$JN69Be_csOTxguHWRAQdZtn2nJc;-><init>(Lcom/squareup/invoices/edit/RealEditInvoiceInTenderRunner$1;Lrx/Scheduler$Worker;)V

    invoke-virtual {v0, v1}, Lrx/Scheduler$Worker;->schedule(Lrx/functions/Action0;)Lrx/Subscription;

    return-void
.end method
