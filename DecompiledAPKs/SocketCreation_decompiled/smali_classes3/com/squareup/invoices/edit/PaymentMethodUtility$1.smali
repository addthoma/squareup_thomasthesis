.class synthetic Lcom/squareup/invoices/edit/PaymentMethodUtility$1;
.super Ljava/lang/Object;
.source "PaymentMethodUtility.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/edit/PaymentMethodUtility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$protos$client$invoice$Invoice$PaymentMethod:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 11
    invoke-static {}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->values()[Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/invoices/edit/PaymentMethodUtility$1;->$SwitchMap$com$squareup$protos$client$invoice$Invoice$PaymentMethod:[I

    :try_start_0
    sget-object v0, Lcom/squareup/invoices/edit/PaymentMethodUtility$1;->$SwitchMap$com$squareup$protos$client$invoice$Invoice$PaymentMethod:[I

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->SHARE_LINK:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/invoices/edit/PaymentMethodUtility$1;->$SwitchMap$com$squareup$protos$client$invoice$Invoice$PaymentMethod:[I

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->EMAIL:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/invoices/edit/PaymentMethodUtility$1;->$SwitchMap$com$squareup$protos$client$invoice$Invoice$PaymentMethod:[I

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->CARD_ON_FILE:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/invoices/edit/PaymentMethodUtility$1;->$SwitchMap$com$squareup$protos$client$invoice$Invoice$PaymentMethod:[I

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNKNOWN_METHOD:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/invoices/edit/PaymentMethodUtility$1;->$SwitchMap$com$squareup$protos$client$invoice$Invoice$PaymentMethod:[I

    sget-object v1, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->UNSELECTED:Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    return-void
.end method
