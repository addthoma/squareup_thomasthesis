.class public final synthetic Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$uUacUWmEMqpL-qGaetPA7tvxMuI;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# instance fields
.field private final synthetic f$0:Lcom/squareup/invoices/edit/EditInvoicePresenter;

.field private final synthetic f$1:Lcom/squareup/payment/Order;

.field private final synthetic f$2:Lcom/squareup/protos/client/invoice/Invoice$Builder;

.field private final synthetic f$3:Lcom/squareup/invoices/edit/EditInvoiceView;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/invoices/edit/EditInvoicePresenter;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/edit/EditInvoiceView;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$uUacUWmEMqpL-qGaetPA7tvxMuI;->f$0:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    iput-object p2, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$uUacUWmEMqpL-qGaetPA7tvxMuI;->f$1:Lcom/squareup/payment/Order;

    iput-object p3, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$uUacUWmEMqpL-qGaetPA7tvxMuI;->f$2:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iput-object p4, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$uUacUWmEMqpL-qGaetPA7tvxMuI;->f$3:Lcom/squareup/invoices/edit/EditInvoiceView;

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$uUacUWmEMqpL-qGaetPA7tvxMuI;->f$0:Lcom/squareup/invoices/edit/EditInvoicePresenter;

    iget-object v1, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$uUacUWmEMqpL-qGaetPA7tvxMuI;->f$1:Lcom/squareup/payment/Order;

    iget-object v2, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$uUacUWmEMqpL-qGaetPA7tvxMuI;->f$2:Lcom/squareup/protos/client/invoice/Invoice$Builder;

    iget-object v3, p0, Lcom/squareup/invoices/edit/-$$Lambda$EditInvoicePresenter$uUacUWmEMqpL-qGaetPA7tvxMuI;->f$3:Lcom/squareup/invoices/edit/EditInvoiceView;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/invoices/edit/EditInvoicePresenter;->lambda$onLoad$8$EditInvoicePresenter(Lcom/squareup/payment/Order;Lcom/squareup/protos/client/invoice/Invoice$Builder;Lcom/squareup/invoices/edit/EditInvoiceView;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method
