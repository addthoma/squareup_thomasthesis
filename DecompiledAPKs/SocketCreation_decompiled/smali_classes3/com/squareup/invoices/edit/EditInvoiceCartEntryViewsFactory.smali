.class public Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;
.super Lcom/squareup/ui/cart/CartEntryViewsFactory;
.source "EditInvoiceCartEntryViewsFactory.java"


# instance fields
.field private final scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;Lcom/squareup/ui/cart/CartEntryViewModelFactory;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/squareup/ui/cart/CartEntryViewsFactory;-><init>(Lcom/squareup/ui/cart/CartEntryViewModelFactory;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;)V

    .line 32
    iput-object p1, p0, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    return-void
.end method

.method private getTaxTypeLabelId()I
    .locals 5

    .line 65
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAdditionalTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 66
    sget v0, Lcom/squareup/transaction/R$string;->cart_tax_row:I

    return v0

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getInclusiveTaxesAmount()J

    move-result-wide v0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    .line 69
    sget v0, Lcom/squareup/common/strings/R$string;->cart_tax_row_included:I

    return v0

    .line 71
    :cond_1
    sget v0, Lcom/squareup/transaction/R$string;->cart_tax_row:I

    return v0
.end method

.method private shouldShowDiscountsRow()Z
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->hasDiscounts()Z

    move-result v0

    return v0
.end method

.method private shouldShowTaxRow()Z
    .locals 5

    .line 61
    iget-object v0, p0, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Order;->getAllTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public addDiscounts(Lcom/squareup/protos/client/bills/Cart;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->shouldShowDiscountsRow()Z

    move-result p1

    if-nez p1, :cond_0

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 42
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->cartEntryViewModelFactory()Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    move-result-object p1

    sget v0, Lcom/squareup/transaction/R$string;->cart_discounts:I

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 43
    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getNegativeAllDiscounts()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 44
    invoke-virtual {v3}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/Order;->hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem()Z

    move-result v3

    .line 42
    invoke-interface {p1, v0, v1, v2, v3}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->discount(ILcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    .line 41
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public addTaxes(Lcom/squareup/protos/client/bills/Cart;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/Cart;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/cart/CartEntryViewModel;",
            ">;"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->shouldShowTaxRow()Z

    move-result p1

    if-nez p1, :cond_0

    .line 49
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 52
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->cartEntryViewModelFactory()Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    move-result-object p1

    invoke-direct {p0}, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->getTaxTypeLabelId()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/invoices/edit/EditInvoiceCartEntryViewsFactory;->scopeRunner:Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;

    .line 53
    invoke-virtual {v1}, Lcom/squareup/invoices/edit/EditInvoiceScopeRunner;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getAdditionalTaxes()Lcom/squareup/protos/common/Money;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 52
    invoke-interface {p1, v0, v1, v2, v3}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->taxes(ILcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
