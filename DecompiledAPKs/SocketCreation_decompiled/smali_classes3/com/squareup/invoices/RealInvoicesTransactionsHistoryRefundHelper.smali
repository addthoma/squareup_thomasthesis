.class public Lcom/squareup/invoices/RealInvoicesTransactionsHistoryRefundHelper;
.super Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;
.source "RealInvoicesTransactionsHistoryRefundHelper.java"


# instance fields
.field private final invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/invoices/ui/InvoiceBillLoader;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/invoices/RealInvoicesTransactionsHistoryRefundHelper;->res:Lcom/squareup/util/Res;

    .line 20
    iput-object p2, p0, Lcom/squareup/invoices/RealInvoicesTransactionsHistoryRefundHelper;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    return-void
.end method


# virtual methods
.method public ingestRefundsResponse(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/IssueRefundsResponse;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    .line 26
    iget-object p2, p2, Lcom/squareup/protos/client/bills/IssueRefundsResponse;->bill:Lcom/squareup/protos/client/bills/Bill;

    iget-object v0, p0, Lcom/squareup/invoices/RealInvoicesTransactionsHistoryRefundHelper;->res:Lcom/squareup/util/Res;

    invoke-static {p1, p2, v0}, Lcom/squareup/billhistory/Bills;->toRefundBill(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    .line 27
    iget-object p2, p0, Lcom/squareup/invoices/RealInvoicesTransactionsHistoryRefundHelper;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-virtual {p2, p1}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->loadFromBill(Lcom/squareup/billhistory/model/BillHistory;)V

    return-object p1
.end method
