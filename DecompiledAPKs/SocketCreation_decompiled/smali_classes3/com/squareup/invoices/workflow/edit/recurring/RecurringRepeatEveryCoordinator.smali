.class public final Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RecurringRepeatEveryCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u00010B-\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J&\u0010\u001d\u001a\n \u001f*\u0004\u0018\u00010\u001e0\u001e2\u000c\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00060!2\u0006\u0010\u0008\u001a\u00020\"H\u0002J\u0010\u0010#\u001a\u00020\u00192\u0006\u0010$\u001a\u00020\u0005H\u0002J(\u0010%\u001a\u00020\u00192\u0016\u0010&\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010\u001a\u001a\u00020\u001bH\u0002J\u0010\u0010\'\u001a\u00020\u00192\u0006\u0010(\u001a\u00020)H\u0002J\u0012\u0010*\u001a\u0004\u0018\u00010+2\u0006\u0010,\u001a\u00020-H\u0002J\u0010\u0010.\u001a\u00020-2\u0006\u0010/\u001a\u00020+H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "chooseInterval",
        "Lcom/squareup/widgets/CheckableGroup;",
        "dayOption",
        "Lcom/squareup/marketfont/MarketCheckedTextView;",
        "editQuantityRow",
        "Lcom/squareup/register/widgets/list/EditQuantityRow;",
        "monthOption",
        "repeatHelper",
        "Lcom/squareup/widgets/MessageView;",
        "weekOption",
        "yearOption",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "getActionBarConfig",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config;",
        "kotlin.jvm.PlatformType",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Landroid/content/res/Resources;",
        "setHelperText",
        "recurrenceInfo",
        "setUp",
        "screen",
        "toggleOptionText",
        "recurrenceInterval",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;",
        "unitForViewId",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;",
        "viewId",
        "",
        "viewIdForInterval",
        "unit",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private chooseInterval:Lcom/squareup/widgets/CheckableGroup;

.field private dayOption:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private editQuantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

.field private monthOption:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private repeatHelper:Lcom/squareup/widgets/MessageView;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private weekOption:Lcom/squareup/marketfont/MarketCheckedTextView;

.field private yearOption:Lcom/squareup/marketfont/MarketCheckedTextView;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;>;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/Res;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public static final synthetic access$setUp(Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->setUp(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$unitForViewId(Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;I)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->unitForViewId(I)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    move-result-object p0

    return-object p0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 114
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 116
    sget v0, Lcom/squareup/features/invoices/R$id;->choose_interval_unit:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/list/EditQuantityRow;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->editQuantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    .line 117
    sget v0, Lcom/squareup/features/invoices/R$id;->interval_options:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->chooseInterval:Lcom/squareup/widgets/CheckableGroup;

    .line 118
    sget v0, Lcom/squareup/features/invoices/R$id;->interval_day:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->dayOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 119
    sget v0, Lcom/squareup/features/invoices/R$id;->interval_week:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->weekOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 120
    sget v0, Lcom/squareup/features/invoices/R$id;->interval_month:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->monthOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 121
    sget v0, Lcom/squareup/features/invoices/R$id;->interval_year:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketCheckedTextView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->yearOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    .line 122
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_repeat_helper:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->repeatHelper:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final getActionBarConfig(Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;",
            "Landroid/content/res/Resources;",
            ")",
            "Lcom/squareup/marin/widgets/MarinActionBar$Config;"
        }
    .end annotation

    .line 107
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 108
    sget v1, Lcom/squareup/features/invoices/R$string;->repeat_every:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    const/4 v0, 0x1

    .line 109
    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 110
    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$getActionBarConfig$1;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$getActionBarConfig$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 111
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method private final setHelperText(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;)V
    .locals 4

    .line 98
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getStartDate()Ljava/util/Date;

    move-result-object v0

    .line 99
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->repeatHelper:Lcom/squareup/widgets/MessageView;

    if-nez v1, :cond_0

    const-string v2, "repeatHelper"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 100
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->res:Lcom/squareup/util/Res;

    const/4 v3, 0x0

    .line 99
    invoke-static {p1, v2, v0, v3}, Lcom/squareup/invoices/InvoicesHelperTextUtility;->getRecurringPeriodShortText(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/util/Res;Ljava/util/Date;Z)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setUp(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string/jumbo v3, "view.resources"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1, v2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->getActionBarConfig(Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 62
    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$setUp$1;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$setUp$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 64
    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->chooseInterval:Lcom/squareup/widgets/CheckableGroup;

    const-string v0, "chooseInterval"

    if-nez p2, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$setUp$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$setUp$2;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    invoke-virtual {p2, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 70
    iget-object p2, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p2, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p2

    if-nez p2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object p2

    .line 71
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->editQuantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    const-string v2, "editQuantityRow"

    if-nez v1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v3, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$setUp$3;

    invoke-direct {v3, p2, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$setUp$3;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v3, Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;

    invoke-virtual {v1, v3}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setOnQuantityChangedListener(Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;)V

    .line 77
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->editQuantityRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    if-nez v1, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getInterval()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setValue(I)V

    .line 78
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->chooseInterval:Lcom/squareup/widgets/CheckableGroup;

    if-nez v1, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getUnit()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->viewIdForInterval(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    .line 79
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->setHelperText(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;)V

    .line 80
    invoke-direct {p0, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->toggleOptionText(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;)V

    return-void
.end method

.method private final toggleOptionText(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;)V
    .locals 5

    .line 84
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getInterval()I

    move-result p1

    const-string/jumbo v0, "yearOption"

    const-string v1, "monthOption"

    const-string/jumbo v2, "weekOption"

    const-string v3, "dayOption"

    const/4 v4, 0x1

    if-ne p1, v4, :cond_4

    .line 85
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->dayOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v3, Lcom/squareup/invoicesappletapi/R$string;->day:I

    invoke-virtual {p1, v3}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(I)V

    .line 86
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->weekOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v2, Lcom/squareup/invoicesappletapi/R$string;->week:I

    invoke-virtual {p1, v2}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(I)V

    .line 87
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->monthOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v1, Lcom/squareup/invoicesappletapi/R$string;->month:I

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(I)V

    .line 88
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->yearOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v0, Lcom/squareup/invoicesappletapi/R$string;->year:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(I)V

    goto :goto_0

    .line 90
    :cond_4
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->dayOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    sget v3, Lcom/squareup/features/invoices/R$string;->days:I

    invoke-virtual {p1, v3}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(I)V

    .line 91
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->weekOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    sget v2, Lcom/squareup/features/invoices/R$string;->weeks:I

    invoke-virtual {p1, v2}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(I)V

    .line 92
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->monthOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    sget v1, Lcom/squareup/features/invoices/R$string;->months:I

    invoke-virtual {p1, v1}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(I)V

    .line 93
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->yearOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_8

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    sget v0, Lcom/squareup/features/invoices/R$string;->years:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(I)V

    :goto_0
    return-void
.end method

.method private final unitForViewId(I)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->dayOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v0, :cond_0

    const-string v1, "dayOption"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v0

    if-ne p1, v0, :cond_1

    sget-object p1, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->DAYS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    goto :goto_0

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->weekOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v0, :cond_2

    const-string/jumbo v1, "weekOption"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v0

    if-ne p1, v0, :cond_3

    sget-object p1, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->WEEKS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    goto :goto_0

    .line 136
    :cond_3
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->monthOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v0, :cond_4

    const-string v1, "monthOption"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v0

    if-ne p1, v0, :cond_5

    sget-object p1, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->MONTHS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    goto :goto_0

    .line 137
    :cond_5
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->yearOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez v0, :cond_6

    const-string/jumbo v1, "yearOption"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result v0

    if-ne p1, v0, :cond_7

    sget-object p1, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->YEARS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    goto :goto_0

    :cond_7
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final viewIdForInterval(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)I
    .locals 1

    .line 125
    sget-object v0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_6

    const/4 v0, 0x2

    if-eq p1, v0, :cond_4

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    .line 129
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->yearOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_1

    const-string/jumbo v0, "yearOption"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result p1

    goto :goto_0

    .line 128
    :cond_2
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->monthOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_3

    const-string v0, "monthOption"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result p1

    goto :goto_0

    .line 127
    :cond_4
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->weekOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_5

    const-string/jumbo v0, "weekOption"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result p1

    goto :goto_0

    .line 126
    :cond_6
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->dayOption:Lcom/squareup/marketfont/MarketCheckedTextView;

    if-nez p1, :cond_7

    const-string v0, "dayOption"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->getId()I

    move-result p1

    :goto_0
    return p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->bindViews(Landroid/view/View;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
