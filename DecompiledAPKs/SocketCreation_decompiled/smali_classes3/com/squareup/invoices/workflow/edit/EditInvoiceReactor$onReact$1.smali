.class final Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditInvoiceReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor;->onReact(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;Lcom/squareup/workflow/legacy/rx2/EventChannel;Lcom/squareup/workflow/legacy/WorkflowPool;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "+",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        ">;>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditInvoiceReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditInvoiceReactor.kt\ncom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1\n+ 2 EventSelectBuilder.kt\ncom/squareup/workflow/legacy/rx2/EventSelectBuilder\n*L\n1#1,291:1\n85#2,2:292\n85#2,2:294\n85#2,2:296\n85#2,2:298\n85#2,2:300\n85#2,2:302\n85#2,2:304\n85#2,2:306\n85#2,2:308\n85#2,2:310\n85#2,2:312\n85#2,2:314\n*E\n*S KotlinDebug\n*F\n+ 1 EditInvoiceReactor.kt\ncom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1\n*L\n108#1,2:292\n111#1,2:294\n124#1,2:296\n133#1,2:298\n136#1,2:300\n139#1,2:302\n142#1,2:304\n151#1,2:306\n154#1,2:308\n157#1,2:310\n160#1,2:312\n163#1,2:314\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u001a\u0012\u0004\u0012\u00020\u0003\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

.field final synthetic $workflows:Lcom/squareup/workflow/legacy/WorkflowPool;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;Lcom/squareup/workflow/legacy/WorkflowPool;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceEvent;",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    .line 107
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Initializing;

    if-eqz v1, :cond_0

    .line 108
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 292
    sget-object v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 111
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 294
    sget-object v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 124
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$3;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$3;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 296
    sget-object v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$3;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 133
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$4;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$4;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 298
    sget-object v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$4;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 136
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$5;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$5;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 300
    sget-object v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$5;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$5;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 139
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$6;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$6;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 302
    sget-object v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$6;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$6;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 142
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$7;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$7;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 304
    sget-object v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$7;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$7;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 151
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$8;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$8;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 306
    sget-object v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$8;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$8;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 154
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$9;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$9;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 308
    sget-object v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$9;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$9;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 157
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$10;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$10;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 310
    sget-object v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$10;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$10;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 160
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$11;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$11;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 312
    sget-object v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$11;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$11;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 163
    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$12;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$12;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 314
    sget-object v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$12;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$$special$$inlined$onEvent$12;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 167
    :cond_0
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;->getDeliveryMethodWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$13;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$13;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 173
    :cond_1
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;

    if-eqz v1, :cond_2

    .line 174
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;->getEditRecurringWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$14;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$14;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 180
    :cond_2
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;->getChooseDateWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$15;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$15;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 186
    :cond_3
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;->getChooseDateWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$16;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$16;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 192
    :cond_4
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;->getChooseDateWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$17;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$17;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 198
    :cond_5
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;->getInvoiceMessageWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$18;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$18;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 204
    :cond_6
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;->getInvoiceAttachmentWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$19;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$19;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    goto/16 :goto_0

    .line 210
    :cond_7
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;->getEditInvoiceDetailsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$20;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$20;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 216
    :cond_8
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;->getAutoRemindersWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$21;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$21;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 222
    :cond_9
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->getAdditionalRecipientsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$22;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$22;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 228
    :cond_a
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;->getPaymentRequestWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$23;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$23;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 234
    :cond_b
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutomaticPayments;->getEditAutomaticPaymentsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$24;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$24;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 240
    :cond_c
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1;->$workflows:Lcom/squareup/workflow/legacy/WorkflowPool;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;->getEditPaymentScheduleWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    sget-object v2, Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$25;->INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceReactor$onReact$1$25;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0, v2}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onWorkflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/jvm/functions/Function1;)V

    :cond_d
    :goto_0
    return-void
.end method
