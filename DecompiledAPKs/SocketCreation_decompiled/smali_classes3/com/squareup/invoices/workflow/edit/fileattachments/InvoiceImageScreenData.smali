.class public final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;
.super Ljava/lang/Object;
.source "InvoiceImageScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0013\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000  2\u00020\u0001:\u0001 B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\nH\u00c6\u0003J;\u0010\u001a\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nH\u00c6\u0001J\u0013\u0010\u001b\u001a\u00020\n2\u0008\u0010\u001c\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001J\t\u0010\u001f\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u000f\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
        "",
        "config",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;",
        "title",
        "",
        "extensionToPreserve",
        "previewImage",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;",
        "showProgress",
        "",
        "(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;Z)V",
        "getConfig",
        "()Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;",
        "getExtensionToPreserve",
        "()Ljava/lang/String;",
        "getPreviewImage",
        "()Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;",
        "getShowProgress",
        "()Z",
        "getTitle",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Factory:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData$Factory;


# instance fields
.field private final config:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

.field private final extensionToPreserve:Ljava/lang/String;

.field private final previewImage:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

.field private final showProgress:Z

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData$Factory;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData$Factory;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->Factory:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData$Factory;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;Z)V
    .locals 1

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "extensionToPreserve"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previewImage"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->config:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->title:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->extensionToPreserve:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->previewImage:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    iput-boolean p5, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->showProgress:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;ZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->config:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->title:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->extensionToPreserve:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->previewImage:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->showProgress:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->copy(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;Z)Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->config:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->extensionToPreserve:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->previewImage:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->showProgress:Z

    return v0
.end method

.method public final copy(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;Z)Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;
    .locals 7

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "title"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "extensionToPreserve"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previewImage"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;-><init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->config:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->config:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->title:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->title:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->extensionToPreserve:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->extensionToPreserve:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->previewImage:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->previewImage:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->showProgress:Z

    iget-boolean p1, p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->showProgress:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getConfig()Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->config:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    return-object v0
.end method

.method public final getExtensionToPreserve()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->extensionToPreserve:Ljava/lang/String;

    return-object v0
.end method

.method public final getPreviewImage()Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->previewImage:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    return-object v0
.end method

.method public final getShowProgress()Z
    .locals 1

    .line 36
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->showProgress:Z

    return v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->config:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->title:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->extensionToPreserve:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->previewImage:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->showProgress:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "InvoiceImageScreenData(config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->config:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", extensionToPreserve="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->extensionToPreserve:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", previewImage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->previewImage:Lcom/squareup/invoices/workflow/edit/fileattachments/PreviewImage;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showProgress="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;->showProgress:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
