.class public final Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AdditionalRecipientScreen.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$Factory;,
        Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$AdditionalEmailTextWatcher;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAdditionalRecipientScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AdditionalRecipientScreen.kt\ncom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,162:1\n1163#2:163\n1642#3,2:164\n*E\n*S KotlinDebug\n*F\n+ 1 AdditionalRecipientScreen.kt\ncom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator\n*L\n68#1:163\n98#1,2:164\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0002()B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u001a\u0010\u0013\u001a\u00020\u00142\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u000c2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0010\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0018\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0015\u001a\u00020\u000c2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0018\u0010\u001e\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001f\u001a\u00020\u0006H\u0002J$\u0010 \u001a\u00020\u00142\u0006\u0010!\u001a\u00020\"2\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020%\u0012\u0004\u0012\u00020\u00140$H\u0002J\u0016\u0010&\u001a\u00020\u00142\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen;",
        "(Lcom/squareup/util/Res;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "emails",
        "",
        "",
        "getEmails",
        "()Ljava/util/List;",
        "emailsContainer",
        "Landroid/widget/LinearLayout;",
        "maxRecipientCount",
        "",
        "addEmailView",
        "",
        "email",
        "canAddExtraView",
        "",
        "attach",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "emailRow",
        "Lcom/squareup/widgets/SelectableAutoCompleteEditText;",
        "update",
        "data",
        "updateActionBar",
        "resources",
        "Landroid/content/res/Resources;",
        "eventHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen$Event;",
        "updateEmailViews",
        "recipients",
        "AdditionalEmailTextWatcher",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private emailsContainer:Landroid/widget/LinearLayout;

.field private maxRecipientCount:I

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->screens:Lio/reactivex/Observable;

    const/16 p1, 0xa

    .line 64
    iput p1, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->maxRecipientCount:I

    return-void
.end method

.method public static final synthetic access$getEmails$p(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;)Ljava/util/List;
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->getEmails()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;Landroid/view/View;Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen;)V

    return-void
.end method

.method private final addEmailView(Ljava/lang/String;Z)V
    .locals 3

    .line 110
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->emailsContainer:Landroid/widget/LinearLayout;

    const-string v1, "emailsContainer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    iget v2, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->maxRecipientCount:I

    if-ge v0, v2, :cond_2

    .line 111
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->emailsContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->emailRow(Ljava/lang/String;Z)Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_2
    return-void
.end method

.method static synthetic addEmailView$default(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;Ljava/lang/String;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const-string p1, ""

    .line 109
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->addEmailView(Ljava/lang/String;Z)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 141
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 143
    sget v0, Lcom/squareup/features/invoices/R$id;->emails_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.emails_container)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->emailsContainer:Landroid/widget/LinearLayout;

    return-void
.end method

.method private final emailRow(Ljava/lang/String;Z)Lcom/squareup/widgets/SelectableAutoCompleteEditText;
    .locals 4

    .line 119
    sget v0, Lcom/squareup/features/invoices/R$layout;->pay_invoice_email_row_view_no_plus:I

    .line 120
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->emailsContainer:Landroid/widget/LinearLayout;

    if-nez v1, :cond_0

    const-string v2, "emailsContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Landroid/view/ViewGroup;

    .line 118
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 121
    check-cast v0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;

    .line 122
    new-instance v1, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    check-cast v1, Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 123
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setText(Ljava/lang/CharSequence;)V

    .line 124
    new-instance p1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$AdditionalEmailTextWatcher;

    invoke-direct {p1, p0, p2}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$AdditionalEmailTextWatcher;-><init>(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;Z)V

    check-cast p1, Landroid/text/TextWatcher;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-object v0
.end method

.method private final getEmails()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->emailsContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    const-string v1, "emailsContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    .line 163
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    invoke-static {v1, v2}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$emails$$inlined$getChildren$1;

    invoke-direct {v2, v0}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$emails$$inlined$getChildren$1;-><init>(Landroid/view/ViewGroup;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 69
    sget-object v1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$emails$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$emails$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 70
    sget-object v1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$emails$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$emails$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/sequences/SequencesKt;->filter(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v0

    .line 71
    invoke-static {v0}, Lkotlin/sequences/SequencesKt;->toList(Lkotlin/sequences/Sequence;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private final update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen;)V
    .locals 2

    .line 85
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen;->getMaxRecipientCount()I

    move-result v0

    iput v0, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->maxRecipientCount:I

    .line 87
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen;->getEventHandler()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 89
    new-instance v1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$update$1;

    invoke-direct {v1, v0}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$update$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 91
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string/jumbo v1, "view.resources"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->updateActionBar(Landroid/content/res/Resources;Lkotlin/jvm/functions/Function1;)V

    .line 92
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen;->getRecipients()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->updateEmailViews(Ljava/util/List;)V

    return-void
.end method

.method private final updateActionBar(Landroid/content/res/Resources;Lkotlin/jvm/functions/Function1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientScreen$Event;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 132
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 133
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v3, Lcom/squareup/features/invoices/R$string;->add_recipients_title:I

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 134
    new-instance v2, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$updateActionBar$1;

    invoke-direct {v2, p2}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$updateActionBar$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 135
    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 136
    new-instance v1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$updateActionBar$2;

    invoke-direct {v1, p0, p2}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$updateActionBar$2;-><init>(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 137
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final updateEmailViews(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->emailsContainer:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    const-string v1, "emailsContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 98
    check-cast p1, Ljava/lang/Iterable;

    .line 164
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    .line 98
    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->addEmailView(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    const/4 v0, 0x0

    .line 101
    invoke-static {p0, v0, p1, p1, v0}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->addEmailView$default(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;Ljava/lang/String;ZILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->bindViews(Landroid/view/View;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;->screens:Lio/reactivex/Observable;

    .line 77
    new-instance v1, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/additionalrecipients/AdditionalRecipientsCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        .subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
