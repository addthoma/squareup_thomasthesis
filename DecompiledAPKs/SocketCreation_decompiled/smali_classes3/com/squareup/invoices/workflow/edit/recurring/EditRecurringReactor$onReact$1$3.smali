.class final Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1$3;
.super Lkotlin/jvm/internal/Lambda;
.source "EditRecurringReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$RepeatEveryClicked;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$RepeatEveryClicked;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1$3;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$RepeatEveryClicked;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$RepeatEveryClicked;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1$3;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;

    iget-object v1, v1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast v1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;)V

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$RepeatEveryClicked;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$1$3;->invoke(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$RepeatEveryClicked;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
