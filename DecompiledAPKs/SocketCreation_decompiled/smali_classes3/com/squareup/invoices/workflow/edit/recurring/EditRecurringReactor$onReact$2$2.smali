.class final Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2$2;
.super Lkotlin/jvm/internal/Lambda;
.source "EditRecurringReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$IntervalUnitSelected;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;",
        "it",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$IntervalUnitSelected;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2$2;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$IntervalUnitSelected;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$IntervalUnitSelected;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2$2;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2;

    iget-object v0, v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 72
    :cond_0
    new-instance v1, Lcom/squareup/workflow/legacy/EnterState;

    .line 73
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2$2;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2;

    iget-object v2, v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast v2, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;

    .line 74
    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2$2;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2;

    iget-object v3, v3, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2;->$state:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    check-cast v3, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;

    invoke-virtual {v3}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object v4

    .line 76
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$IntervalUnitSelected;->getUnit()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    move-result-object p1

    invoke-virtual {v3, p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->changeUnit(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;)Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    move-result-object p1

    .line 75
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->changeInterval(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x6

    const/4 v9, 0x0

    .line 74
    invoke-static/range {v4 .. v9}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->copy$default(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Ljava/util/Date;ZILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object p1

    .line 73
    invoke-virtual {v2, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;->copy(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;)Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;

    move-result-object p1

    .line 72
    invoke-direct {v1, p1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$IntervalUnitSelected;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringReactor$onReact$2$2;->invoke(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent$IntervalUnitSelected;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
