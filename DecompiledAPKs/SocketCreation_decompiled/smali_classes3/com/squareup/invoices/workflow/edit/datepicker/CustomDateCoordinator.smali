.class public final Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "CustomDateCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomDateCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomDateCoordinator.kt\ncom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator\n*L\n1#1,123:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0001\"B-\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J.\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u00102\u0006\u0010\u0019\u001a\u00020\u00102\u0006\u0010\u001a\u001a\u00020\u00102\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u001cH\u0002J\u001e\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00020\u001f2\u000c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u001cH\u0002J(\u0010 \u001a\u00020\u00132\u0016\u0010!\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010\u0014\u001a\u00020\u0015H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/CustomDateInfo;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateScreen;",
        "locale",
        "Ljava/util/Locale;",
        "(Lio/reactivex/Observable;Ljava/util/Locale;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "calendar",
        "Lcom/squareup/timessquare/CalendarPickerView;",
        "lastKnownMaxDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "lastKnownStartDate",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "initializeCalendar",
        "minDateInclusive",
        "maxDateExclusive",
        "selectedDate",
        "input",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "setTitle",
        "title",
        "",
        "update",
        "screen",
        "Factory",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private calendar:Lcom/squareup/timessquare/CalendarPickerView;

.field private lastKnownMaxDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field private lastKnownStartDate:Lcom/squareup/protos/common/time/YearMonthDay;

.field private final locale:Ljava/util/Locale;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/CustomDateInfo;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Ljava/util/Locale;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/CustomDateInfo;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;>;",
            "Ljava/util/Locale;",
            ")V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->locale:Ljava/util/Locale;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Ljava/util/Locale;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;-><init>(Lio/reactivex/Observable;Ljava/util/Locale;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->update(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 4

    .line 111
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string/jumbo v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 113
    sget v0, Lcom/squareup/features/invoices/R$id;->calendar_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/timessquare/CalendarPickerView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->calendar:Lcom/squareup/timessquare/CalendarPickerView;

    .line 116
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->calendar:Lcom/squareup/timessquare/CalendarPickerView;

    const-string v1, "calendar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/features/invoices/R$drawable;->invoice_calendar_divider:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/timessquare/CalendarPickerView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->calendar:Lcom/squareup/timessquare/CalendarPickerView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/marin/R$dimen;->marin_gap_ultra_large:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/timessquare/CalendarPickerView;->setDividerHeight(I)V

    .line 119
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->calendar:Lcom/squareup/timessquare/CalendarPickerView;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v2, v3}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/timessquare/CalendarPickerView;->setTitleTypeface(Landroid/graphics/Typeface;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->calendar:Lcom/squareup/timessquare/CalendarPickerView;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {p1, v1}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Landroid/graphics/Typeface;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/timessquare/CalendarPickerView;->setDateTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method

.method private final initializeCalendar(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/protos/common/time/YearMonthDay;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;)V"
        }
    .end annotation

    .line 87
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->lastKnownStartDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "calendar"

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->lastKnownMaxDate:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->calendar:Lcom/squareup/timessquare/CalendarPickerView;

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {p3}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object p2

    const/4 p3, 0x1

    invoke-virtual {p1, p2, p3}, Lcom/squareup/timessquare/CalendarPickerView;->selectDate(Ljava/util/Date;Z)Z

    goto :goto_0

    .line 90
    :cond_1
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->lastKnownStartDate:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 91
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->lastKnownMaxDate:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 92
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->calendar:Lcom/squareup/timessquare/CalendarPickerView;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 94
    :cond_2
    invoke-static {p1}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object p1

    .line 95
    invoke-static {p2}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object p2

    .line 96
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->locale:Ljava/util/Locale;

    .line 93
    invoke-virtual {v0, p1, p2, v2}, Lcom/squareup/timessquare/CalendarPickerView;->init(Ljava/util/Date;Ljava/util/Date;Ljava/util/Locale;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;

    move-result-object p1

    .line 98
    invoke-static {p3}, Lcom/squareup/util/ProtoDates;->getDateForYearMonthDay(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;->withSelectedDate(Ljava/util/Date;)Lcom/squareup/timessquare/CalendarPickerView$FluentInitializer;

    .line 101
    :goto_0
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->calendar:Lcom/squareup/timessquare/CalendarPickerView;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance p2, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$initializeCalendar$1;

    invoke-direct {p2, p4}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$initializeCalendar$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast p2, Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;

    invoke-virtual {p1, p2}, Lcom/squareup/timessquare/CalendarPickerView;->setOnDateSelectedListener(Lcom/squareup/timessquare/CalendarPickerView$OnDateSelectedListener;)V

    return-void
.end method

.method private final setTitle(Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;)V"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 73
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 74
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 75
    new-instance v1, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$setTitle$1;

    invoke-direct {v1, p2}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$setTitle$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final update(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/CustomDateInfo;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 58
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0, v0, v1}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->setTitle(Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 59
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;

    .line 61
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getStartDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getMaxDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    .line 62
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/CustomDateInfo;->getSelectedDate()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    .line 63
    iget-object v3, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 60
    invoke-direct {p0, v1, v2, v0, v3}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->initializeCalendar(Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/protos/common/time/YearMonthDay;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 66
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$update$2;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$update$2;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->bindViews(Landroid/view/View;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
