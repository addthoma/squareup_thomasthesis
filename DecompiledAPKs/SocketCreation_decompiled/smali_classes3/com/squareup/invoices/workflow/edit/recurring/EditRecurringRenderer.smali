.class public final Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringRenderer;
.super Ljava/lang/Object;
.source "EditRecurringRenderer.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/Renderer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/Renderer<",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\tJH\u0010\n\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\u000b\u001a\u00020\u00022\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00030\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringRenderer;",
        "Lcom/squareup/workflow/legacy/Renderer;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "()V",
        "render",
        "state",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "workflows",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringRenderer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringRenderer;

    invoke-direct {v0}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringRenderer;-><init>()V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringRenderer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/lang/Object;
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringRenderer;->render(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflows"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    instance-of p3, p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    if-eqz p3, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyScreenKt;->RecurringFrequencyScreen(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 24
    :cond_0
    instance-of p3, p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;

    if-eqz p3, :cond_1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringRepeatEveryScreenKt;->RecurringRepeatEveryScreen(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 25
    :cond_1
    instance-of p3, p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;

    if-eqz p3, :cond_2

    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsScreenKt;->RecurringEndsScreen(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 26
    :cond_2
    instance-of p3, p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$EndsDate;

    if-eqz p3, :cond_3

    check-cast p1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$EndsDate;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$EndsDate;->getRecurrenceInfo()Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringEndsDateScreenKt;->RecurringEndsDateScreen(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    .line 28
    :goto_0
    sget-object p2, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1

    .line 26
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
