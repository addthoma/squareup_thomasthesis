.class public final Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;
.super Lcom/squareup/invoices/workflow/edit/EditInvoiceState;
.source "EditInvoiceState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/EditInvoiceState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Delivery"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001BK\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u0005\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0005\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u000fB\u001f\u0012\u0018\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0011\u00a2\u0006\u0002\u0010\u0015J\u001b\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0011H\u00c6\u0003J%\u0010\u0019\u001a\u00020\u00002\u001a\u0008\u0002\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0011H\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\t2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001J\t\u0010\u001f\u001a\u00020\u0003H\u00d6\u0001R#\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00140\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceState;",
        "title",
        "",
        "instruments",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "loadingInstruments",
        "",
        "currentPaymentMethod",
        "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
        "shareLinkMessage",
        "",
        "currentInstrumentToken",
        "(Ljava/lang/String;Lio/reactivex/Observable;Lio/reactivex/Observable;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/CharSequence;Ljava/lang/String;)V",
        "deliveryMethodWorkflow",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
        "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
        "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
        "(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V",
        "getDeliveryMethodWorkflow",
        "()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "component1",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final deliveryMethodWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "deliveryMethodWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;->deliveryMethodWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lio/reactivex/Observable;Lio/reactivex/Observable;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "instruments"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loadingInstruments"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentPaymentMethod"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "shareLinkMessage"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    sget-object v0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor;->Companion:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion;

    .line 76
    sget-object v1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;->Companion:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Companion;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v1 .. v7}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Companion;->startState(Ljava/lang/String;Lio/reactivex/Observable;Lio/reactivex/Observable;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    move-result-object p1

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    .line 75
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$Companion;->handle(Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p1

    .line 74
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;->deliveryMethodWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;->copy(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;->deliveryMethodWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-object v0
.end method

.method public final copy(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;"
        }
    .end annotation

    const-string v0, "deliveryMethodWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;->deliveryMethodWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;->deliveryMethodWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getDeliveryMethodWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;",
            "Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodEvent;",
            "Lcom/squareup/invoices/workflow/edit/DeliveryMethodResult;",
            ">;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;->deliveryMethodWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;->deliveryMethodWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Delivery(deliveryMethodWorkflow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;->deliveryMethodWorkflow:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
