.class public final Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "CustomDateCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator_Factory_Factory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator_Factory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/util/Locale;)Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;-><init>(Ljava/util/Locale;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator_Factory_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {v0}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator_Factory_Factory;->newInstance(Ljava/util/Locale;)Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator_Factory_Factory;->get()Lcom/squareup/invoices/workflow/edit/datepicker/CustomDateCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
