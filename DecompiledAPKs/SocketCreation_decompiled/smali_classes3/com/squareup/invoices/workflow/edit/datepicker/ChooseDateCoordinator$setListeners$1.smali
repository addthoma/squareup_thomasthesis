.class final Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setListeners$1;
.super Ljava/lang/Object;
.source "ChooseDateCoordinator.kt"

# interfaces
.implements Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->setListeners(Lcom/squareup/workflow/legacy/WorkflowInput;Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/widgets/CheckableGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "<anonymous parameter 2>",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $dateOptions:Ljava/util/List;

.field final synthetic $workflowInput:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;Ljava/util/List;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setListeners$1;->this$0:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setListeners$1;->$dateOptions:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setListeners$1;->$workflowInput:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 1

    .line 90
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setListeners$1;->this$0:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;

    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->access$getSuppressCheckChangeEvent$p(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 91
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setListeners$1;->this$0:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;

    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setListeners$1;->$dateOptions:Ljava/util/List;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator$setListeners$1;->$workflowInput:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-static {p1, p2, p3, v0}, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;->access$onOptionSelected(Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateCoordinator;ILjava/util/List;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    :cond_0
    return-void
.end method
