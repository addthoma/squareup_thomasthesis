.class public abstract Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;
.super Ljava/lang/Object;
.source "ChooseDateOutput.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$SelectedDate;,
        Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$NoDateSelected;,
        Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00032\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0002\u0006\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
        "",
        "()V",
        "Converter",
        "NoDateSelected",
        "SelectedDate",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$SelectedDate;",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$NoDateSelected;",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Converter:Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;->Converter:Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 5
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;-><init>()V

    return-void
.end method

.method public static final selectedDateFromOutput(Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;)Lcom/squareup/protos/common/time/YearMonthDay;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;->Converter:Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/workflow/edit/ChooseDateOutput$Converter;->selectedDateFromOutput(Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object p0

    return-object p0
.end method
