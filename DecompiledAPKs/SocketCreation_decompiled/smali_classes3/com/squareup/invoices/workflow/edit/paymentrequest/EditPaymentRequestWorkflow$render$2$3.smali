.class final Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2$3;
.super Lkotlin/jvm/internal/Lambda;
.source "EditPaymentRequestWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2;->invoke(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $event:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2$3;->$event:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 69
    check-cast p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2$3;->invoke(Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;)V
    .locals 1

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestWorkflow$render$2$3;->$event:Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$FixedAmountChanged;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestScreen$Event$FixedAmountChanged;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->fixed_amount:Lcom/squareup/protos/common/Money;

    const/4 v0, 0x0

    .line 175
    check-cast v0, Ljava/lang/Long;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/PaymentRequest$Builder;->percentage_amount:Ljava/lang/Long;

    return-void
.end method
