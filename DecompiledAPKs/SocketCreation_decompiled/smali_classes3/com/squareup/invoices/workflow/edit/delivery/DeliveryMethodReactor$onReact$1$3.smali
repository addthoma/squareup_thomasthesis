.class final Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$3;
.super Ljava/lang/Object;
.source "DeliveryMethodReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "it",
        "",
        "apply",
        "(Ljava/lang/Boolean;)Lio/reactivex/Observable;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$3;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Boolean;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$3;->this$0:Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1;->$state:Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    check-cast p1, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;

    invoke-virtual {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState$Loading;->getInstruments()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/delivery/DeliveryMethodReactor$onReact$1$3;->apply(Ljava/lang/Boolean;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
