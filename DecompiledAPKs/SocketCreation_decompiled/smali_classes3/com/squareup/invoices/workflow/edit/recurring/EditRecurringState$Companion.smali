.class public final Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Companion;
.super Ljava/lang/Object;
.source "EditRecurringState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditRecurringState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditRecurringState.kt\ncom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,90:1\n180#2:91\n*E\n*S KotlinDebug\n*F\n+ 1 EditRecurringState.kt\ncom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Companion\n*L\n70#1:91\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Companion;",
        "",
        "()V",
        "fromByteString",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;",
        "bytes",
        "Lokio/ByteString;",
        "startState",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;",
        "recurrenceInfo",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 66
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromByteString(Lokio/ByteString;)Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;
    .locals 6

    const-string v0, "bytes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 71
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    const/4 v1, 0x0

    .line 73
    move-object v3, v1

    check-cast v3, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    .line 74
    invoke-interface {p1}, Lokio/BufferedSource;->exhausted()Z

    move-result v4

    if-nez v4, :cond_1

    .line 75
    sget-object v3, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->Companion:Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v3, p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;->fromByteString(Lokio/ByteString;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v3

    .line 78
    :cond_1
    new-instance p1, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    const-string v5, "Calendar.getInstance()"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    const-string v5, "Calendar.getInstance().time"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, v3, v4, v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Ljava/util/Date;Z)V

    .line 79
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "Class.forName(stateClassName)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    .line 80
    const-class v3, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    const/4 v2, 0x2

    invoke-direct {v0, p1, v1, v2, v1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    goto :goto_1

    .line 81
    :cond_2
    const-class v1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$RepeatEvery;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    goto :goto_1

    .line 82
    :cond_3
    const-class v1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;->getRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v1

    if-nez v1, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->getEndCount()I

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Ends;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;I)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    goto :goto_1

    .line 83
    :cond_5
    const-class v1, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$EndsDate;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$EndsDate;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$EndsDate;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    :goto_1
    return-object v0

    .line 84
    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final startState(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;)Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;
    .locals 3

    const-string v0, "recurrenceInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {v0, p1, v1, v2, v1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$Frequency;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;Lcom/squareup/invoices/workflow/edit/RecurrenceRule;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
