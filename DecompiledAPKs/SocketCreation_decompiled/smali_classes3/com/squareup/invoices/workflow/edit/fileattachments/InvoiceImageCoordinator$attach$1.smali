.class final Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$1;
.super Ljava/lang/Object;
.source "InvoiceImageCoordinator.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        ">;+",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
        ">;",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012V\u0010\u0002\u001aR\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007\u0012\u000c\u0012\n \t*\u0004\u0018\u00010\u00080\u0008 \t*(\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007\u0012\u000c\u0012\n \t*\u0004\u0018\u00010\u00080\u0008\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreen;",
        "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lkotlin/Pair;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceAttachmentEvent;",
            ">;+",
            "Lcom/squareup/invoices/workflow/edit/fileattachments/PhotoState;",
            ">;)",
            "Lcom/squareup/register/widgets/GlassSpinnerState;"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 83
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$1;->this$0:Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;

    invoke-static {v0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;->access$spinnerData(Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator;Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageScreenData;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceImageCoordinator$attach$1;->call(Lkotlin/Pair;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method
