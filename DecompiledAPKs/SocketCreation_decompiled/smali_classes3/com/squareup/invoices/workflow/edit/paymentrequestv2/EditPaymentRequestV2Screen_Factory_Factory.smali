.class public final Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;
.super Ljava/lang/Object;
.source "EditPaymentRequestV2Screen_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final dateFormatProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final percentageFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/settings/server/Features;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;"
        }
    .end annotation

    .line 58
    new-instance v6, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/settings/server/Features;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;->percentageFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/Formatter;

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;->dateFormatProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/text/DateFormat;

    iget-object v4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/settings/server/Features;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen_Factory_Factory;->get()Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;

    move-result-object v0

    return-object v0
.end method
