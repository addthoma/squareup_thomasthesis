.class public abstract Lcom/squareup/invoices/workflow/edit/DateOption;
.super Ljava/lang/Object;
.source "DateOption.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;,
        Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;,
        Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;,
        Lcom/squareup/invoices/workflow/edit/DateOption$Never;,
        Lcom/squareup/invoices/workflow/edit/DateOption$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00072\u00020\u0001:\u0005\u0007\u0008\t\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0004\u000c\r\u000e\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/DateOption;",
        "Landroid/os/Parcelable;",
        "()V",
        "title",
        "",
        "getTitle",
        "()Ljava/lang/String;",
        "Companion",
        "CustomDate",
        "Never",
        "RelativeDate",
        "StaticDate",
        "Lcom/squareup/invoices/workflow/edit/DateOption$RelativeDate;",
        "Lcom/squareup/invoices/workflow/edit/DateOption$StaticDate;",
        "Lcom/squareup/invoices/workflow/edit/DateOption$CustomDate;",
        "Lcom/squareup/invoices/workflow/edit/DateOption$Never;",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/DateOption$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/DateOption$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/DateOption$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/DateOption;->Companion:Lcom/squareup/invoices/workflow/edit/DateOption$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/DateOption;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getTitle()Ljava/lang/String;
.end method
