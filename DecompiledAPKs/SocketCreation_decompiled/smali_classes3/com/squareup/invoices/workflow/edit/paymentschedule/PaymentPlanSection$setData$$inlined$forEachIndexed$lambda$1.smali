.class final Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "PaymentPlanSection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection;->setData(ZLcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$AddAnotherPaymentButton;Ljava/util/List;Ljava/lang/CharSequence;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "invoke",
        "com/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$2$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $i:I

.field final synthetic $onMoneyChanged$inlined:Lkotlin/jvm/functions/Function2;

.field final synthetic $paymentRequestClicked$inlined:Lkotlin/jvm/functions/Function1;

.field final synthetic $percentChanged$inlined:Lkotlin/jvm/functions/Function2;

.field final synthetic $rowDataList$inlined:Ljava/util/List;


# direct methods
.method constructor <init>(ILjava/util/List;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput p1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$1;->$i:I

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$1;->$rowDataList$inlined:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$1;->$onMoneyChanged$inlined:Lkotlin/jvm/functions/Function2;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$1;->$percentChanged$inlined:Lkotlin/jvm/functions/Function2;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$1;->$paymentRequestClicked$inlined:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$1;->invoke(Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/String;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$1;->$onMoneyChanged$inlined:Lkotlin/jvm/functions/Function2;

    iget v1, p0, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentPlanSection$setData$$inlined$forEachIndexed$lambda$1;->$i:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
