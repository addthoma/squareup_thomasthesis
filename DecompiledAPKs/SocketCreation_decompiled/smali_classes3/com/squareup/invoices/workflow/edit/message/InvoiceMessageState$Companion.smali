.class public final Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion;
.super Ljava/lang/Object;
.source "InvoiceMessageState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceMessageState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceMessageState.kt\ncom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,101:1\n180#2:102\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceMessageState.kt\ncom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion\n*L\n72#1:102\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0016\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion;",
        "",
        "()V",
        "fromByteString",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
        "bytes",
        "Lokio/ByteString;",
        "startState",
        "updatedMessage",
        "",
        "isDefaultMessageEnabled",
        "Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromByteString(Lokio/ByteString;)Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;
    .locals 5

    const-string v0, "bytes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 73
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 75
    const-class v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    .line 77
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 78
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabledKt;->readDefaultMessageEnabled(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    move-result-object p1

    .line 76
    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;-><init>(Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    goto :goto_0

    .line 81
    :cond_0
    const-class v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.IsDefaultMessageEnabled.Enabled"

    if-eqz v1, :cond_2

    .line 82
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;

    .line 83
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 84
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabledKt;->readDefaultMessageEnabled(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    .line 82
    invoke-direct {v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;-><init>(Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    goto :goto_0

    .line 84
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 87
    :cond_2
    const-class v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 88
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;

    .line 89
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v3

    .line 91
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v4

    .line 92
    invoke-static {p1}, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabledKt;->readDefaultMessageEnabled(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    move-result-object p1

    if-eqz p1, :cond_3

    check-cast p1, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    .line 88
    invoke-direct {v0, v1, v3, v4, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$ErrorSavingDefaultMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    :goto_0
    return-object v0

    .line 92
    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 95
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final startState(Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;
    .locals 1

    const-string/jumbo v0, "updatedMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "isDefaultMessageEnabled"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;-><init>(Ljava/lang/String;Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    return-object v0
.end method
