.class public final Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;
.super Ljava/lang/Object;
.source "RecurringFrequencyCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0019\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J+\u0010\u0007\u001a\u00020\u00082\u001c\u0010\t\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000bj\u0002`\u000e0\nH\u0000\u00a2\u0006\u0002\u0008\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;",
        "",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Ljava/text/DateFormat;Lcom/squareup/util/Res;)V",
        "build",
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
        "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
        "Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyScreen;",
        "build$invoices_hairball_release",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dateFormat:Ljava/text/DateFormat;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Ljava/text/DateFormat;Lcom/squareup/util/Res;)V
    .locals 1
    .param p1    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "dateFormat"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;->dateFormat:Ljava/text/DateFormat;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final build$invoices_hairball_release(Lio/reactivex/Observable;)Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
            "Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringEvent;",
            ">;>;)",
            "Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;

    .line 59
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;->dateFormat:Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator$Factory;->res:Lcom/squareup/util/Res;

    const/4 v3, 0x0

    .line 58
    invoke-direct {v0, p1, v1, v2, v3}, Lcom/squareup/invoices/workflow/edit/recurring/RecurringFrequencyCoordinator;-><init>(Lio/reactivex/Observable;Ljava/text/DateFormat;Lcom/squareup/util/Res;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
