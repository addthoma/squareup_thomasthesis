.class public final Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "EditPaymentRequestV2Workflow.kt"

# interfaces
.implements Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Workflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Workflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditPaymentRequestV2Workflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditPaymentRequestV2Workflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n+ 3 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 4 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 5 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,281:1\n149#2,5:282\n149#2,5:290\n85#3:287\n240#4:288\n276#5:289\n*E\n*S KotlinDebug\n*F\n+ 1 EditPaymentRequestV2Workflow.kt\ncom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow\n*L\n208#1,5:282\n225#1,5:290\n212#1:287\n212#1:288\n212#1:289\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012<\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0\u0002:\u0001.B?\u0008\u0017\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u00a2\u0006\u0002\u0010\u0019BU\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u001c\u0010\u0017\u001a\u0018\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\u001aj\u0002`\u001e\u00a2\u0006\u0002\u0010\u001fJ\u001a\u0010\"\u001a\u00020\u00042\u0006\u0010#\u001a\u00020\u00032\u0008\u0010$\u001a\u0004\u0018\u00010%H\u0016J\u000e\u0010&\u001a\u0008\u0012\u0004\u0012\u00020(0\'H\u0002JN\u0010)\u001a$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n2\u0006\u0010#\u001a\u00020\u00032\u0006\u0010*\u001a\u00020\u00042\u0012\u0010+\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050,H\u0016J\u0010\u0010-\u001a\u00020%2\u0006\u0010*\u001a\u00020\u0004H\u0016R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000RN\u0010 \u001aB\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d\u0012&\u0012$\u0012\u0004\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\u0008\u0012\u0004\u0012\u00020\u0007`\n0!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Workflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "remindersWorkflow",
        "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;",
        "editPaymentRequestScreenFactory",
        "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;",
        "chooseDateInfoFactory",
        "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
        "invoiceUnitCache",
        "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
        "remindersInfoFactory",
        "Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "dateReactor",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;",
        "(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/analytics/Analytics;Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;)V",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
        "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateLauncher;",
        "(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/analytics/Analytics;Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;)V",
        "chooseDateWorkflow",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "reminderSettings",
        "Lio/reactivex/Single;",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "Action",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

.field private final chooseDateWorkflow:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
            "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation
.end field

.field private final editPaymentRequestScreenFactory:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;

.field private final invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

.field private final remindersInfoFactory:Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

.field private final remindersWorkflow:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/analytics/Analytics;Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateReactor;)V
    .locals 10
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p7

    const-string v1, "remindersWorkflow"

    move-object v3, p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "editPaymentRequestScreenFactory"

    move-object v4, p2

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "chooseDateInfoFactory"

    move-object v5, p3

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "invoiceUnitCache"

    move-object v6, p4

    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "remindersInfoFactory"

    move-object v7, p5

    invoke-static {p5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "analytics"

    move-object/from16 v8, p6

    invoke-static {v8, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "dateReactor"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    move-object v9, v0

    check-cast v9, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-object v2, p0

    .line 87
    invoke-direct/range {v2 .. v9}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;-><init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/analytics/Analytics;Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;Lcom/squareup/analytics/Analytics;Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;",
            "Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            "Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateEvent;",
            "+",
            "Lcom/squareup/invoices/workflow/edit/ChooseDateOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "remindersWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editPaymentRequestScreenFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "chooseDateInfoFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoiceUnitCache"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "remindersInfoFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateReactor"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->remindersWorkflow:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->editPaymentRequestScreenFactory:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

    iput-object p4, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    iput-object p5, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->remindersInfoFactory:Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

    iput-object p6, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->analytics:Lcom/squareup/analytics/Analytics;

    .line 98
    sget-object p1, Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;->INSTANCE:Lcom/squareup/invoices/workflow/edit/datepicker/ChooseDateRenderer;

    check-cast p1, Lcom/squareup/workflow/legacy/Renderer;

    .line 99
    sget-object p2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$chooseDateWorkflow$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$chooseDateWorkflow$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 100
    sget-object p3, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$chooseDateWorkflow$2;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$chooseDateWorkflow$2;

    check-cast p3, Lkotlin/jvm/functions/Function1;

    .line 97
    invoke-static {p7, p1, p2, p3}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt;->asV2Workflow(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/Renderer;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->chooseDateWorkflow:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 63
    iget-object p0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method private final reminderSettings()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
            ">;"
        }
    .end annotation

    .line 270
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->invoiceUnitCache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->unitMetadataDisplayDetails()Lio/reactivex/Observable;

    move-result-object v0

    .line 271
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$reminderSettings$1;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$reminderSettings$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 274
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "invoiceUnitCache.unitMet\u2026}\n        .firstOrError()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getPaymentRequests()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getIndex()I

    move-result p1

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/PaymentRequest;

    .line 185
    new-instance p2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;

    invoke-direct {p2, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;-><init>(Lcom/squareup/protos/client/invoice/PaymentRequest;)V

    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;

    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->initialState(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;

    check-cast p2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->render(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;",
            "-",
            "Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Result;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v1, "props"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "state"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "context"

    invoke-static {p3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    invoke-interface {p3}, Lcom/squareup/workflow/RenderContext;->makeActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v1

    .line 196
    instance-of v4, p2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;

    const-string v9, ""

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->editPaymentRequestScreenFactory:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;

    .line 197
    move-object v4, p2

    check-cast v4, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;

    invoke-virtual {v4}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$Viewing;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v4

    .line 199
    new-instance v5, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$1;

    invoke-direct {v5, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 200
    new-instance v6, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$2;

    invoke-direct {v6, v1, p2}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$2;-><init>(Lcom/squareup/workflow/Sink;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;)V

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 201
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$3;

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$3;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;Lcom/squareup/workflow/Sink;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;)V

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 205
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$4;

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$4;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function0;

    .line 206
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$5;

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$5;-><init>(Lcom/squareup/workflow/Sink;)V

    move-object v10, v0

    check-cast v10, Lkotlin/jvm/functions/Function0;

    move-object v0, v3

    move-object v1, v4

    move-object v2, p1

    move-object v3, v5

    move-object v4, v6

    move-object v5, v7

    move-object v6, v8

    move-object v7, v10

    .line 196
    invoke-virtual/range {v0 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->from(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 283
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 284
    const-class v2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v9}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 285
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 283
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 209
    sget-object v0, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_0

    .line 211
    :cond_0
    instance-of v1, p2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$FetchingReminderSettings;

    if-eqz v1, :cond_1

    .line 212
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->reminderSettings()Lio/reactivex/Single;

    move-result-object v1

    .line 287
    sget-object v4, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v4, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$$inlined$asWorker$1;

    const/4 v5, 0x0

    invoke-direct {v4, v1, v5}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 288
    invoke-static {v4}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    .line 289
    const-class v4, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    invoke-static {v4}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v4

    new-instance v5, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v5, v4, v1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v4, v5

    check-cast v4, Lcom/squareup/workflow/Worker;

    const/4 v5, 0x0

    .line 212
    sget-object v1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$6;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$6;

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v3, p3

    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 216
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->editPaymentRequestScreenFactory:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;

    .line 217
    move-object v0, p2

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$FetchingReminderSettings;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$FetchingReminderSettings;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v3

    .line 219
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$7;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$7;

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function0;

    .line 220
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$8;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$8;

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 221
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$9;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$9;

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function0;

    .line 222
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$10;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$10;

    move-object v7, v0

    check-cast v7, Lkotlin/jvm/functions/Function0;

    .line 223
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$11;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$11;

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function0;

    move-object v0, v1

    move-object v1, v3

    move-object v2, p1

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    .line 216
    invoke-virtual/range {v0 .. v7}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen$Factory;->from(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 291
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 292
    const-class v2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Screen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-static {v2, v9}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 293
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 291
    invoke-direct {v1, v2, v0, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 226
    sget-object v0, Lcom/squareup/container/PosLayering;->CARD:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v0}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_0

    .line 229
    :cond_1
    instance-of v1, p2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingReminders;

    if-eqz v1, :cond_2

    .line 231
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->remindersWorkflow:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 232
    new-instance v4, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    .line 233
    iget-object v5, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->remindersInfoFactory:Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

    .line 234
    move-object v0, p2

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingReminders;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingReminders;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v6

    .line 235
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getFirstSentAt()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v7

    .line 233
    invoke-virtual {v5, v6, v7}, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;->createForPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    move-result-object v5

    check-cast v5, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    .line 237
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingReminders;->getReminderSettings()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    move-result-object v6

    .line 238
    iget-object v7, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->remindersInfoFactory:Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;

    .line 239
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingReminders;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    .line 240
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getDefaultConfigs()Ljava/util/List;

    move-result-object v8

    .line 241
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getFirstSentAt()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    .line 238
    invoke-virtual {v7, v0, v8, v2}, Lcom/squareup/invoices/util/InvoiceRemindersInfoFactory;->createDefaultListForPaymentRequest(Lcom/squareup/protos/client/invoice/PaymentRequest;Ljava/util/List;Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo$InstancesInfo;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;

    const/4 v2, 0x1

    .line 232
    invoke-direct {v4, v5, v0, v6, v2}, Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;Z)V

    const/4 v5, 0x0

    .line 245
    sget-object v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$12;->INSTANCE:Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$12;

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    move-object v3, v1

    .line 230
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    goto :goto_0

    .line 250
    :cond_2
    instance-of v1, p2, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingDueDate;

    if-eqz v1, :cond_4

    .line 251
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->chooseDateWorkflow:Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 252
    sget-object v4, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;->Companion:Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;

    .line 253
    iget-object v5, p0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->chooseDateInfoFactory:Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;

    .line 254
    move-object v0, p2

    check-cast v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingDueDate;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State$EditingDueDate;->getPaymentRequest()Lcom/squareup/protos/client/invoice/PaymentRequest;

    move-result-object v0

    .line 255
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getFirstSentAt()Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v6

    .line 256
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getIndex()I

    move-result v7

    .line 257
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;->getPaymentRequests()Ljava/util/List;

    move-result-object v8

    .line 253
    invoke-virtual {v5, v0, v6, v7, v8}, Lcom/squareup/invoices/workflow/edit/datepicker/InvoiceChooseDateInfoFactory;->createForPaymentRequestV2(Lcom/squareup/protos/client/invoice/PaymentRequest;Lcom/squareup/protos/common/time/YearMonthDay;ILjava/util/List;)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object v0

    .line 252
    invoke-virtual {v4, v0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;->startState(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;

    move-result-object v4

    const/4 v5, 0x0

    .line 260
    new-instance v0, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$13;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow$render$13;-><init>(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2Props;)V

    move-object v6, v0

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x4

    const/4 v8, 0x0

    move-object v2, p3

    move-object v3, v1

    .line 250
    invoke-static/range {v2 .. v8}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter$LegacyRendering;->getRendering()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_3

    goto :goto_0

    .line 265
    :cond_3
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_4
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public snapshotState(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentrequestv2/RealEditPaymentRequestV2Workflow;->snapshotState(Lcom/squareup/invoices/workflow/edit/paymentrequestv2/EditPaymentRequestV2State;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
