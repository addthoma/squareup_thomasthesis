.class public final Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "InvoiceMessageCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceMessageCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceMessageCoordinator.kt\ncom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,132:1\n1103#2,7:133\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceMessageCoordinator.kt\ncom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator\n*L\n88#1,7:133\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u001fB#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0016\u0010\u0016\u001a\u00020\u00122\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0018H\u0002J\u001e\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u001a\u001a\u00020\n2\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0018H\u0002J&\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u001c\u001a\u00020\u00052\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0018H\u0002J\u0010\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u001c\u001a\u00020\u001eH\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "customMessageView",
        "Lcom/squareup/widgets/SelectableEditText;",
        "defaultMessageHelper",
        "Lcom/squareup/widgets/MessageView;",
        "setAsDefaultButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "setListeners",
        "workflowInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "setUpActionbar",
        "actionBarView",
        "update",
        "state",
        "updateSetAsDefaultButton",
        "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;",
        "DefaultButtonState",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private customMessageView:Lcom/squareup/widgets/SelectableEditText;

.field private defaultMessageHelper:Lcom/squareup/widgets/MessageView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private setAsDefaultButton:Lcom/squareup/marketfont/MarketButton;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;Landroid/view/View;Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 116
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 117
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_custom_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->customMessageView:Lcom/squareup/widgets/SelectableEditText;

    .line 118
    sget v0, Lcom/squareup/features/invoices/R$id;->save_default_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->setAsDefaultButton:Lcom/squareup/marketfont/MarketButton;

    .line 119
    sget v0, Lcom/squareup/features/invoices/R$id;->set_default_message_helper:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->defaultMessageHelper:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final setListeners(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            ">;)V"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->setAsDefaultButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_0

    const-string v1, "setAsDefaultButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    .line 133
    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$setListeners$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$setListeners$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->customMessageView:Lcom/squareup/widgets/SelectableEditText;

    if-nez v0, :cond_1

    const-string v1, "customMessageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$setListeners$2;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$setListeners$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private final setUpActionbar(Lcom/squareup/marin/widgets/ActionBarView;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/marin/widgets/ActionBarView;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            ">;)V"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 109
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_message:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 v1, 0x1

    .line 110
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 111
    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$setUpActionbar$1;

    invoke-direct {v1, p2}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$setUpActionbar$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 112
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageEvent;",
            ">;)V"
        }
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$update$1;

    invoke-direct {v0, p2, p3}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$update$1;-><init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 58
    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p1, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0, p1, p3}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->setUpActionbar(Lcom/squareup/marin/widgets/ActionBarView;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 59
    invoke-direct {p0, p3}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->setListeners(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 62
    instance-of p1, p2, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    if-eqz p1, :cond_8

    .line 63
    move-object p1, p2

    check-cast p1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;->getUpdatedMessage()Ljava/lang/String;

    move-result-object p3

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->customMessageView:Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "customMessageView"

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    xor-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_3

    .line 64
    iget-object p3, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->customMessageView:Lcom/squareup/widgets/SelectableEditText;

    if-nez p3, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;->getUpdatedMessage()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p3, v0}, Lcom/squareup/widgets/SelectableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 67
    :cond_3
    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->isDefaultMessageEnabled()Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    move-result-object p3

    instance-of p3, p3, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    .line 69
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->setAsDefaultButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_4

    const-string v1, "setAsDefaultButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 70
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->defaultMessageHelper:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_5

    const-string v1, "defaultMessageHelper"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p3, :cond_9

    .line 74
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$UpdatingMessage;->getUpdatedMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->isDefaultMessageEnabled()Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled;

    move-result-object p2

    if-eqz p2, :cond_7

    check-cast p2, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;

    invoke-virtual {p2}, Lcom/squareup/invoices/workflow/edit/IsDefaultMessageEnabled$Enabled;->getMessage()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 75
    sget-object p1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->MESSAGE_SET_AS_DEFAULT:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    goto :goto_0

    .line 77
    :cond_6
    sget-object p1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->SET_MESSAGE_AS_DEFAULT:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    .line 73
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->updateSetAsDefaultButton(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;)V

    goto :goto_1

    .line 74
    :cond_7
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.invoices.workflow.edit.IsDefaultMessageEnabled.Enabled"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 81
    :cond_8
    instance-of p1, p2, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState$CommittingMessage;

    if-eqz p1, :cond_9

    .line 82
    sget-object p1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->SAVING:Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->updateSetAsDefaultButton(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;)V

    :cond_9
    :goto_1
    return-void
.end method

.method private final updateSetAsDefaultButton(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;)V
    .locals 3

    .line 100
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->setAsDefaultButton:Lcom/squareup/marketfont/MarketButton;

    const-string v1, "setAsDefaultButton"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->getStringId()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 101
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->setAsDefaultButton:Lcom/squareup/marketfont/MarketButton;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$DefaultButtonState;->getEnabled()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->bindViews(Landroid/view/View;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator$attach$1;-><init>(Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
