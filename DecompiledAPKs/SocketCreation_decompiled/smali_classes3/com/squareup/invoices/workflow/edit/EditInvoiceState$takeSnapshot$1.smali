.class final Lcom/squareup/invoices/workflow/edit/EditInvoiceState$takeSnapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditInvoiceState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/EditInvoiceState;->takeSnapshot$invoices_hairball_release()Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "sink",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/EditInvoiceState;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$takeSnapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "this::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 213
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    .line 214
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Delivery;->getDeliveryMethodWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/deliverymethod/DeliveryMethodState;->takeSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 215
    :cond_0
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$Recurring;->getEditRecurringWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;->takeSnapshot$invoices_hairball_release()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 216
    :cond_1
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingSendDate;->getChooseDateWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;->takeSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 217
    :cond_2
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;

    if-eqz v1, :cond_3

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingDueDate;->getChooseDateWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;->takeSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 218
    :cond_3
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;

    if-eqz v1, :cond_4

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$ChoosingExpiryDate;->getChooseDateWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    invoke-virtual {v0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;->takeSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 219
    :cond_4
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;

    if-eqz v1, :cond_5

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceMessage;->getInvoiceMessageWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/message/InvoiceMessageState;->takeSnapshot$invoices_hairball_release()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 220
    :cond_5
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;

    if-eqz v1, :cond_6

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceAttachment;->getInvoiceAttachmentWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-static {v0}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 221
    :cond_6
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;

    if-eqz v1, :cond_8

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$InvoiceDetails;->getEditInvoiceDetailsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    if-nez v0, :cond_7

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_7
    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 222
    :cond_8
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    if-eqz v1, :cond_a

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AdditionalRecipients;->getAdditionalRecipientsWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    if-nez v0, :cond_9

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_9
    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto :goto_0

    .line 223
    :cond_a
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;

    if-eqz v1, :cond_c

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$AutoReminders;->getAutoRemindersWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    if-nez v0, :cond_b

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_b
    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto :goto_0

    .line 224
    :cond_c
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;

    if-eqz v1, :cond_e

    .line 225
    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;->getIndex()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 226
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/EditInvoiceState;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentRequest;->getPaymentRequestWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    if-nez v0, :cond_d

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_d
    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto :goto_0

    .line 228
    :cond_e
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;

    if-eqz v1, :cond_10

    check-cast v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/EditInvoiceState$PaymentSchedule;->getEditPaymentScheduleWorkflow()Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacyintegration/LegacyState;->getSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    if-nez v0, :cond_f

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_f
    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    :cond_10
    :goto_0
    return-void
.end method
