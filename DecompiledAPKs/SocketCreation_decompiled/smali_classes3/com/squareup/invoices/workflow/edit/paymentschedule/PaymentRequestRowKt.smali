.class public final Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRowKt;
.super Ljava/lang/Object;
.source "PaymentRequestRow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPaymentRequestRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PaymentRequestRow.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRowKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,299:1\n1651#2,3:300\n*E\n*S KotlinDebug\n*F\n+ 1 PaymentRequestRow.kt\ncom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRowKt\n*L\n295#1,3:300\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002\u001a\u0012\u0010\u0004\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "setBorders",
        "",
        "",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;",
        "setData",
        "data",
        "Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final setBorders(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$setBorders"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 295
    move-object v0, p0

    check-cast v0, Ljava/lang/Iterable;

    .line 301
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;

    .line 296
    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->getLastIndex(Ljava/util/List;)I

    move-result v5

    if-ne v2, v5, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v3, v2}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->drawBorders$invoices_hairball_release(Z)V

    move v2, v4

    goto :goto_0

    :cond_2
    return-void
.end method

.method public static final setData(Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;)V
    .locals 5

    const-string v0, "$this$setData"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 268
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;->getDescriptionTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setDescriptionRowTitle(Ljava/lang/CharSequence;)V

    .line 270
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;->getReminderText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;->getDescriptionSubtitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " \n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;->getDescriptionSubtitle()Ljava/lang/String;

    move-result-object v0

    :goto_0
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setDescriptionRowSubtitle(Ljava/lang/CharSequence;)V

    .line 272
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;->getNumberAmount()Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;

    move-result-object v0

    .line 273
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$MoneyAmount;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->getInputType()Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;

    move-result-object v2

    sget-object v3, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;->MONEY:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;

    if-ne v2, v3, :cond_1

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->getMoneyText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-void

    .line 278
    :cond_1
    instance-of v2, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$PercentageAmount;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->getInputType()Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;

    move-result-object v3

    sget-object v4, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;->PERCENTAGE:Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow$InputType;

    if-ne v3, v4, :cond_2

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->getPercentageText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    return-void

    :cond_2
    if-eqz v1, :cond_3

    .line 285
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;->getText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setMoneyText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    if-eqz v2, :cond_4

    .line 286
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;->getText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setPercentageText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 287
    :cond_4
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount$Disabled;

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$NumberAmount;->getText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->setDisabledText(Ljava/lang/CharSequence;)V

    .line 290
    :cond_5
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/EditPaymentScheduleScreen$PaymentRequestRowData;->isEditable()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/paymentschedule/PaymentRequestRow;->displayEdit(Z)V

    return-void
.end method
