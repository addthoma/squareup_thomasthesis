.class public interface abstract Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;
.super Ljava/lang/Object;
.source "EditInvoiceWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner$ParentComponent;,
        Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u0000 72\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u000278J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0016\u0010\u0007\u001a\u00020\u00042\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH&J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u000c\u001a\u00020\rH&J\u0018\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H&JL\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\n2\u0012\u0010\u0015\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00170\t0\u00162\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\r0\u00162\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\nH&J\u0018\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H&J\u0010\u0010#\u001a\u00020\u00042\u0006\u0010$\u001a\u00020%H&J\u0010\u0010&\u001a\u00020\u00042\u0006\u0010\'\u001a\u00020(H&J\u0018\u0010)\u001a\u00020\u00042\u0006\u0010$\u001a\u00020%2\u0006\u0010*\u001a\u00020\rH&J\u0018\u0010+\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\n2\u0006\u0010-\u001a\u00020\rH&J\u0010\u0010.\u001a\u00020\u00042\u0006\u0010/\u001a\u000200H&J \u00101\u001a\u00020\u00042\u0006\u00102\u001a\u0002032\u0006\u00104\u001a\u0002032\u0006\u00105\u001a\u000206H&\u00a8\u00069"
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceResult;",
        "startAddPaymentSchedule",
        "",
        "editPaymentScheduleInput",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;",
        "startAdditionalRecipients",
        "recipients",
        "",
        "",
        "startAutomaticPayments",
        "allowAutoPayments",
        "",
        "startDateChooser",
        "chooseDateInfo",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
        "type",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateType;",
        "startDeliveryMethod",
        "title",
        "instruments",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
        "loadingInstruments",
        "currentPaymentMethod",
        "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
        "shareLinkMessage",
        "",
        "currentInstrumentToken",
        "startEditingPaymentRequest",
        "editPaymentRequestInfo",
        "Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;",
        "index",
        "",
        "startEstimateDetails",
        "detailsInfo",
        "Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;",
        "startInvoiceAttachment",
        "startAttachmentInfo",
        "Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;",
        "startInvoiceDetails",
        "disableId",
        "startInvoiceMessage",
        "message",
        "isDefaultMessageEnabled",
        "startRecurring",
        "recurrenceInfo",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;",
        "startReminders",
        "remindersListInfo",
        "Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;",
        "defaultRemindersListInfo",
        "reminderSettings",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;",
        "Companion",
        "ParentComponent",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner$Companion;

.field public static final NEW_PAYMENT_REQUEST:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner;->Companion:Lcom/squareup/invoices/workflow/edit/EditInvoiceWorkflowRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract startAddPaymentSchedule(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentScheduleInput;)V
.end method

.method public abstract startAdditionalRecipients(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract startAutomaticPayments(Z)V
.end method

.method public abstract startDateChooser(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;Lcom/squareup/invoices/workflow/edit/ChooseDateType;)V
.end method

.method public abstract startDeliveryMethod(Ljava/lang/String;Lio/reactivex/Observable;Lio/reactivex/Observable;Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/instruments/InstrumentSummary;",
            ">;>;",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/protos/client/invoice/Invoice$PaymentMethod;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract startEditingPaymentRequest(Lcom/squareup/invoices/workflow/edit/paymentrequest/EditPaymentRequestInfo;I)V
.end method

.method public abstract startEstimateDetails(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;)V
.end method

.method public abstract startInvoiceAttachment(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo;)V
.end method

.method public abstract startInvoiceDetails(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;Z)V
.end method

.method public abstract startInvoiceMessage(Ljava/lang/String;Z)V
.end method

.method public abstract startRecurring(Lcom/squareup/invoices/workflow/edit/RecurrenceInfo;)V
.end method

.method public abstract startReminders(Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/invoices/workflow/edit/InvoiceRemindersListInfo;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)V
.end method
