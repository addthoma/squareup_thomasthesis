.class final Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$takeSnapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EditRecurringState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;->takeSnapshot$invoices_hairball_release()Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "sink",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rule:Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

.field final synthetic $seriesActive:Z

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;ZLcom/squareup/invoices/workflow/edit/RecurrenceRule;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    iput-boolean p2, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$takeSnapshot$1;->$seriesActive:Z

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$takeSnapshot$1;->$rule:Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$takeSnapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$takeSnapshot$1;->this$0:Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "this::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 59
    iget-boolean v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$takeSnapshot$1;->$seriesActive:Z

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 60
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/recurring/EditRecurringState$takeSnapshot$1;->$rule:Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->toByteString()Lokio/ByteString;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeByteStringWithLength(Lokio/BufferedSink;Lokio/ByteString;)Lokio/BufferedSink;

    :cond_0
    return-void
.end method
