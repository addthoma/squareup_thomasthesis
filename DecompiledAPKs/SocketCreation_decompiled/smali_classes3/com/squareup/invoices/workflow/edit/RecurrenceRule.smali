.class public final Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
.super Ljava/lang/Object;
.source "RecurrenceRule.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecurrenceRule.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecurrenceRule.kt\ncom/squareup/invoices/workflow/edit/RecurrenceRule\n*L\n1#1,242:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \'2\u00020\u0001:\u0001\'B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u000f\u001a\u00020\u00002\u0006\u0010\u0010\u001a\u00020\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0014\u001a\u00020\u0008J\u000e\u0010\u0015\u001a\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u0017J\u0006\u0010\u0018\u001a\u00020\u0000J\u0013\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u0008H\u00d6\u0001J\u0006\u0010\u001d\u001a\u00020\u001aJ\u0006\u0010\u001e\u001a\u00020\u001fJ\u0010\u0010 \u001a\u00020!2\u0008\u0010\"\u001a\u0004\u0018\u00010#J\u0008\u0010$\u001a\u00020%H\u0002J\t\u0010&\u001a\u00020%H\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u00088F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/invoices/workflow/edit/RecurrenceRule;",
        "",
        "frequency",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;",
        "recurrenceEnd",
        "Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;",
        "(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;)V",
        "endCount",
        "",
        "getEndCount",
        "()I",
        "getFrequency",
        "()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;",
        "getRecurrenceEnd",
        "()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;",
        "changeInterval",
        "interval",
        "component1",
        "component2",
        "copy",
        "count",
        "endDate",
        "date",
        "Ljava/util/Date;",
        "endNever",
        "equals",
        "",
        "other",
        "hashCode",
        "isFrequencyMonthly",
        "toByteString",
        "Lokio/ByteString;",
        "toRecurringSchedule",
        "Lcom/squareup/protos/client/invoice/RecurringSchedule;",
        "startDate",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "toRfc5545",
        "",
        "toString",
        "Companion",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;


# instance fields
.field private final frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

.field private final recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->Companion:Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;)V
    .locals 1

    const-string v0, "frequency"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recurrenceEnd"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->copy(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p0

    return-object p0
.end method

.method public static final defaultRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->Companion:Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;->defaultRecurrenceRule()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v0

    return-object v0
.end method

.method public static final fromByteString(Lokio/ByteString;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->Companion:Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule$Companion;->fromByteString(Lokio/ByteString;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p0

    return-object p0
.end method

.method private final toRfc5545()Ljava/lang/String;
    .locals 3

    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FREQ="

    .line 94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getUnit()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->getRfcString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getInterval()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    const-string v1, ";INTERVAL="

    .line 97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getInterval()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    .line 101
    instance-of v2, v1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    if-eqz v2, :cond_1

    const-string v1, ";COUNT="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    check-cast v1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    invoke-virtual {v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "sb.append(\";COUNT=\").append(recurrenceEnd.count)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :cond_1
    instance-of v2, v1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;

    if-eqz v2, :cond_2

    goto :goto_0

    .line 104
    :cond_2
    instance-of v1, v1, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    .line 107
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sb.toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final changeInterval(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 2

    const-string v0, "interval"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x2

    .line 121
    invoke-static {p0, p1, v0, v1, v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->copy$default(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p1

    return-object p1
.end method

.method public final component1()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    return-object v0
.end method

.method public final component2()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;
    .locals 1

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    return-object v0
.end method

.method public final copy(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 1

    const-string v0, "frequency"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recurrenceEnd"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    invoke-direct {v0, p1, p2}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;-><init>(Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;)V

    return-object v0
.end method

.method public final endCount(I)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 2

    .line 127
    new-instance v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;-><init>(I)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->copy$default(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p1

    return-object p1
.end method

.method public final endDate(Ljava/util/Date;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 2

    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    new-instance v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;-><init>(Ljava/util/Date;)V

    check-cast v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    const/4 p1, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, p1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->copy$default(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object p1

    return-object p1
.end method

.method public final endNever()Lcom/squareup/invoices/workflow/edit/RecurrenceRule;
    .locals 3

    .line 123
    sget-object v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;->INSTANCE:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;

    check-cast v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {p0, v1, v0, v2, v1}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->copy$default(Lcom/squareup/invoices/workflow/edit/RecurrenceRule;Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;ILjava/lang/Object;)Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    iget-object v1, p1, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    iget-object p1, p1, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEndCount()I
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    .line 82
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;->getCount()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public final getFrequency()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    return-object v0
.end method

.method public final getRecurrenceEnd()Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final isFrequencyMonthly()Z
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;->getUnit()Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;->MONTHS:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval$IntervalUnit;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final toByteString()Lokio/ByteString;
    .locals 4

    .line 130
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    .line 132
    move-object v1, v0

    check-cast v1, Lokio/BufferedSink;

    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->toRfc5545()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 133
    iget-object v2, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    .line 134
    instance-of v3, v2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    if-eqz v3, :cond_0

    goto :goto_0

    .line 136
    :cond_0
    instance-of v3, v2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;

    if-eqz v3, :cond_1

    goto :goto_0

    .line 138
    :cond_1
    instance-of v3, v2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    if-eqz v3, :cond_2

    check-cast v2, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    invoke-virtual {v2}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;->getDate()Ljava/util/Date;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/util/ProtoDates;->dateToYmd(Ljava/util/Date;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v2

    check-cast v2, Lcom/squareup/wire/Message;

    invoke-static {v1, v2}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 141
    :cond_2
    :goto_0
    invoke-virtual {v0}, Lokio/Buffer;->readByteString()Lokio/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public final toRecurringSchedule(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/RecurringSchedule;
    .locals 3

    .line 111
    new-instance v0, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;-><init>()V

    .line 112
    invoke-direct {p0}, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->toRfc5545()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->recurrence_rule(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;

    move-result-object v0

    .line 113
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->start_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;

    move-result-object p1

    .line 114
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    .line 115
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AfterCount;

    const-string v2, "recurringScheduleBuilder.build()"

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->build()Lcom/squareup/protos/client/invoice/RecurringSchedule;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 116
    :cond_0
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$Never;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->build()Lcom/squareup/protos/client/invoice/RecurringSchedule;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 117
    :cond_1
    instance-of v1, v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;

    invoke-virtual {v0}, Lcom/squareup/invoices/workflow/edit/RecurrenceEnd$AtDate;->getDate()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/ProtoDates;->dateToYmd(Ljava/util/Date;)Lcom/squareup/protos/common/time/YearMonthDay;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->end_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/RecurringSchedule$Builder;->build()Lcom/squareup/protos/client/invoice/RecurringSchedule;

    move-result-object p1

    const-string v0, "recurringScheduleBuilder\u2026urrenceEnd.date)).build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RecurrenceRule(frequency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->frequency:Lcom/squareup/invoices/workflow/edit/RecurrenceInterval;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", recurrenceEnd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/invoices/workflow/edit/RecurrenceRule;->recurrenceEnd:Lcom/squareup/invoices/workflow/edit/RecurrenceEnd;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
