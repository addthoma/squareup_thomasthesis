.class final Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$3;
.super Lkotlin/jvm/internal/Lambda;
.source "AutomaticRemindersWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;->render(Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "index",
        "",
        "invoke",
        "com/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$1$3"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props$inlined:Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

.field final synthetic $sink$inlined:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$3;->this$0:Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow;

    iput-object p2, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$3;->$props$inlined:Lcom/squareup/invoices/workflow/edit/AutomaticRemindersProps;

    iput-object p3, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$3;->$sink$inlined:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 69
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$3;->invoke(I)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(I)V
    .locals 2

    .line 343
    iget-object v0, p0, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$render$$inlined$with$lambda$3;->$sink$inlined:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ReminderPressed;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/workflow/edit/autoreminders/AutomaticRemindersWorkflow$Action$ReminderPressed;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
