.class public Lcom/squareup/invoices/ui/BillHistoryDetailView;
.super Landroid/widget/LinearLayout;
.source "BillHistoryDetailView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

.field presenter:Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const-class p2, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Component;->inject(Lcom/squareup/invoices/ui/BillHistoryDetailView;)V

    .line 36
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/BillHistoryDetailView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->shortAnimTimeMs:I

    return-void
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 48
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 49
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->presenter:Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->presenter:Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 54
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 42
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 43
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_bill_history_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    .line 44
    sget v0, Lcom/squareup/features/invoices/R$id;->crm_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->progressBar:Landroid/widget/ProgressBar;

    return-void
.end method

.method onPrintGiftReceiptButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onPrintGiftReceiptButtonClicked()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method onReceiptButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onReceiptButtonClicked()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method onRefundButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onRefundButtonClicked()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method onReprintButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onReprintTicketButtonClicked()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 59
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method show(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->show(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 96
    iget-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->progressBar:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 98
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->progressBar:Landroid/widget/ProgressBar;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public temporarilySetPrintGiftReceiptButtonDisabled()V
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->temporarilyDisablePrintGiftReceiptButton()V

    return-void
.end method

.method public temporarilySetReprintTicketButtonDisabled()V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/invoices/ui/BillHistoryDetailView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->temporarilyDisableReprintTicketButton()V

    return-void
.end method
