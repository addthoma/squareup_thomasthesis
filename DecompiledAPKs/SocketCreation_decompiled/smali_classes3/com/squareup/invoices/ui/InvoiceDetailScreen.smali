.class public final Lcom/squareup/invoices/ui/InvoiceDetailScreen;
.super Lcom/squareup/invoices/ui/InInvoicesAppletScope;
.source "InvoiceDetailScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/ui/InvoiceDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoiceDetailScreen$Component;,
        Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceDetailScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceDetailScreen.kt\ncom/squareup/invoices/ui/InvoiceDetailScreen\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,47:1\n24#2,4:48\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceDetailScreen.kt\ncom/squareup/invoices/ui/InvoiceDetailScreen\n*L\n27#1,4:48\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0002\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0006\u001a\u00020\u0007H\u0016R\u0016\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceDetailScreen;",
        "Lcom/squareup/invoices/ui/InInvoicesAppletScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "screenLayout",
        "",
        "Component",
        "Event",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/ui/InvoiceDetailScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/invoices/ui/InvoiceDetailScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceDetailScreen;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/InvoiceDetailScreen;-><init>()V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceDetailScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceDetailScreen;

    .line 48
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceDetailScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 51
    sput-object v0, Lcom/squareup/invoices/ui/InvoiceDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InInvoicesAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 24
    sget v0, Lcom/squareup/features/invoices/R$layout;->invoice_read_write_detail_view:I

    return v0
.end method
