.class public final Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;
.super Ljava/lang/Object;
.source "InvoicePaymentAmountWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultRecordPaymentAmountProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;->resProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;->defaultRecordPaymentAmountProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ")",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;"
        }
    .end annotation

    .line 53
    new-instance v0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;
    .locals 4

    .line 41
    iget-object v0, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    iget-object v2, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;->defaultRecordPaymentAmountProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;

    iget-object v3, p0, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/invoices/ui/recordpayment/DefaultRecordPaymentAmountCalculator;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow_Factory;->get()Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflow;

    move-result-object v0

    return-object v0
.end method
