.class public Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;
.super Lcom/squareup/invoices/ui/InInvoicesAppletScope;
.source "SendReminderConfirmationDialog.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/invoices/ui/SendReminderConfirmationDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/SendReminderConfirmationDialog$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;-><init>()V

    sput-object v0, Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;->INSTANCE:Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;

    .line 46
    sget-object v0, Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;->INSTANCE:Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;

    .line 47
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InInvoicesAppletScope;-><init>()V

    return-void
.end method
