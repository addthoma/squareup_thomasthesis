.class public final Lcom/squareup/invoices/ui/InvoicesAppletScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "InvoicesAppletScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;,
        Lcom/squareup/invoices/ui/InvoicesAppletScope$Module;,
        Lcom/squareup/invoices/ui/InvoicesAppletScope$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/invoices/ui/InvoicesAppletScope;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScope;-><init>()V

    sput-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScope;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletScope;

    .line 103
    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScope;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/ui/InvoicesAppletScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 39
    const-class v0, Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;

    .line 40
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    .line 41
    invoke-interface {v0}, Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;->scopeRunner()Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method
