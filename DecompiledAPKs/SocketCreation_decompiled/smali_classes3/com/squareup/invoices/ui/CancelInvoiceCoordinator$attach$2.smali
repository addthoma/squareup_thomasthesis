.class final Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2;
.super Ljava/lang/Object;
.source "CancelInvoiceCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCancelInvoiceCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CancelInvoiceCoordinator.kt\ncom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,84:1\n1103#2,7:85\n*E\n*S KotlinDebug\n*F\n+ 1 CancelInvoiceCoordinator.kt\ncom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2\n*L\n57#1,7:85\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2;->this$0:Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;)V
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2;->this$0:Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->access$getNotifyRecipientToggle$p(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->getShowNotifyRecipients()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 50
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2;->this$0:Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->access$getNotifyRecipientToggle$p(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->getDefaultNotifyRecipients()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 52
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2;->this$0:Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->access$configureActionBar(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2;->this$0:Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->access$getConfirmationMessageView$p(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)Lcom/squareup/widgets/MessageView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->getHelperText()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2;->this$0:Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->access$getCancelInvoiceButton$p(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)Lcom/squareup/marketfont/MarketButton;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->getButtonText()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2;->this$0:Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;->access$getCancelInvoiceButton$p(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator;)Lcom/squareup/marketfont/MarketButton;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 85
    new-instance v1, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2$$special$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2;Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/CancelInvoiceCoordinator$attach$2;->accept(Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;)V

    return-void
.end method
