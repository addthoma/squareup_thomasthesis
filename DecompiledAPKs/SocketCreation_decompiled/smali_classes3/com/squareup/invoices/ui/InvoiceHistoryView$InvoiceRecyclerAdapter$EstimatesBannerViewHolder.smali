.class final Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;
.super Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;
.source "InvoiceHistoryView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "EstimatesBannerViewHolder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceHistoryView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceHistoryView.kt\ncom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,900:1\n1103#2,7:901\n1103#2,7:908\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceHistoryView.kt\ncom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder\n*L\n845#1,7:901\n871#1,7:908\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000eR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;",
        "view",
        "Landroid/view/ViewGroup;",
        "(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/ViewGroup;)V",
        "closeButton",
        "Landroid/view/View;",
        "downloadAppButton",
        "Landroid/widget/TextView;",
        "message",
        "title",
        "bind",
        "",
        "hasCreatedEstimates",
        "",
        "hasInvoicesAppInstalled",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final closeButton:Landroid/view/View;

.field private final downloadAppButton:Landroid/widget/TextView;

.field private final message:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

.field private final title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;Landroid/view/ViewGroup;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 832
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    check-cast p2, Landroid/view/View;

    invoke-direct {p0, p2}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 833
    sget p1, Lcom/squareup/features/invoices/R$id;->banner_title:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;->title:Landroid/widget/TextView;

    .line 835
    sget p1, Lcom/squareup/features/invoices/R$id;->banner_message:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;->message:Landroid/widget/TextView;

    .line 837
    sget p1, Lcom/squareup/features/invoices/R$id;->banner_download_app:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;->downloadAppButton:Landroid/widget/TextView;

    .line 839
    sget p1, Lcom/squareup/features/invoices/R$id;->banner_close_button:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;->closeButton:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public final bind(ZZ)V
    .locals 2

    .line 845
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;->closeButton:Landroid/view/View;

    .line 901
    new-instance v1, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder$bind$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder$bind$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez p1, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    .line 853
    :cond_0
    sget v0, Lcom/squareup/features/invoices/R$string;->estimate_banner_title_no_estimates:I

    goto :goto_1

    .line 851
    :cond_1
    :goto_0
    sget v0, Lcom/squareup/features/invoices/R$string;->estimate_banner_title_has_estimates:I

    :goto_1
    if-eqz p2, :cond_2

    const/4 p1, -0x1

    goto :goto_2

    :cond_2
    if-eqz p1, :cond_3

    .line 858
    sget p1, Lcom/squareup/features/invoices/R$string;->estimate_banner_message_has_estimates:I

    goto :goto_2

    .line 859
    :cond_3
    sget p1, Lcom/squareup/features/invoices/R$string;->estimate_banner_message_no_estimates:I

    :goto_2
    if-eqz p2, :cond_4

    .line 862
    sget p2, Lcom/squareup/features/invoices/R$string;->estimate_banner_view_in_invoices_app:I

    goto :goto_3

    .line 864
    :cond_4
    sget p2, Lcom/squareup/features/invoices/R$string;->estimate_banner_download_invoices_app:I

    .line 867
    :goto_3
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 868
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;->message:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;I)V

    .line 869
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;->downloadAppButton:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 871
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;->downloadAppButton:Landroid/widget/TextView;

    check-cast p1, Landroid/view/View;

    .line 908
    new-instance p2, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder$bind$$inlined$onClickDebounced$2;

    invoke-direct {p2, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder$bind$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter$EstimatesBannerViewHolder;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
