.class public final Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;
.super Ljava/lang/Object;
.source "InvoicesAppletConfirmationScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u001b\u0010\u0007\u001a\u00020\u0008\"\u0004\u0008\u0000\u0010\t2\u0006\u0010\n\u001a\u0002H\tH\u0002\u00a2\u0006\u0002\u0010\u000bJ\u001e\u0010\u000c\u001a\u00020\r\"\u0008\u0008\u0000\u0010\t*\u00020\u000e2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\t0\u0010J\u0006\u0010\u0011\u001a\u00020\rJ\u001e\u0010\u0012\u001a\u00020\r\"\u0008\u0008\u0000\u0010\t*\u00020\u00132\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\t0\u0010J\u0006\u0010\u0014\u001a\u00020\rJ\u001e\u0010\u0015\u001a\u00020\r\"\u0008\u0008\u0000\u0010\t*\u00020\u00162\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\t0\u0010J\u001e\u0010\u0017\u001a\u00020\r\"\u0008\u0008\u0000\u0010\t*\u00020\u00182\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\t0\u0010J\u0006\u0010\u0019\u001a\u00020\rJ\u0006\u0010\u001a\u001a\u00020\rJ\u001e\u0010\u001b\u001a\u00020\r\"\u0008\u0008\u0000\u0010\t*\u00020\u001c2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\t0\u0010J\u0006\u0010\u001d\u001a\u00020\rJ\u001e\u0010\u001e\u001a\u00020\r\"\u0008\u0008\u0000\u0010\t*\u00020\u001f2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\t0\u0010J\u000e\u0010 \u001a\u00020\r2\u0006\u0010!\u001a\u00020\"J\u001e\u0010#\u001a\u00020\r\"\u0008\u0008\u0000\u0010\t*\u00020$2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\t0\u0010J\u0006\u0010%\u001a\u00020\rJ\u0018\u0010&\u001a\u00020\'\"\u0004\u0008\u0000\u0010\t*\u0008\u0012\u0004\u0012\u0002H\t0\u0010H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "(Lcom/squareup/util/Res;Lcom/squareup/receiving/FailureMessageFactory;)V",
        "convertToStatus",
        "Lcom/squareup/protos/client/Status;",
        "T",
        "response",
        "(Ljava/lang/Object;)Lcom/squareup/protos/client/Status;",
        "createArchiveInvoiceFailure",
        "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;",
        "Lcom/squareup/protos/client/invoice/ArchiveInvoiceResponse;",
        "showFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "createArchiveInvoiceSuccess",
        "createCancelFailure",
        "Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;",
        "createCancelSuccess",
        "createDeleteDraftFailure",
        "Lcom/squareup/protos/client/invoice/DeleteDraftResponse;",
        "createDeleteDraftSeriesFailure",
        "Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;",
        "createDeleteDraftSeriesSuccess",
        "createDeleteDraftSuccess",
        "createEndSeriesFailure",
        "Lcom/squareup/protos/client/invoice/EndRecurringSeriesResponse;",
        "createEndSeriesSuccess",
        "createSendReminderFailure",
        "Lcom/squareup/protos/client/invoice/SendInvoiceReminderResponse;",
        "createSendReminderSuccess",
        "payerEmail",
        "",
        "createUnarchiveInvoiceFailure",
        "Lcom/squareup/protos/client/invoice/UnarchiveInvoiceResponse;",
        "createUnarchiveInvoiceSuccess",
        "toFailureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/receiving/FailureMessageFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    return-void
.end method

.method public static final synthetic access$convertToStatus(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;Ljava/lang/Object;)Lcom/squareup/protos/client/Status;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->convertToStatus(Ljava/lang/Object;)Lcom/squareup/protos/client/Status;

    move-result-object p0

    return-object p0
.end method

.method private final convertToStatus(Ljava/lang/Object;)Lcom/squareup/protos/client/Status;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/squareup/protos/client/Status;"
        }
    .end annotation

    .line 240
    instance-of v0, p1, Lcom/squareup/protos/client/invoice/SendInvoiceReminderResponse;

    const-string v1, "response.status"

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/protos/client/invoice/SendInvoiceReminderResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/SendInvoiceReminderResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 241
    :cond_0
    instance-of v0, p1, Lcom/squareup/protos/client/invoice/EndRecurringSeriesResponse;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/protos/client/invoice/EndRecurringSeriesResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/EndRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 242
    :cond_1
    instance-of v0, p1, Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 243
    :cond_2
    instance-of v0, p1, Lcom/squareup/protos/client/invoice/DeleteDraftResponse;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/protos/client/invoice/DeleteDraftResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/DeleteDraftResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 244
    :cond_3
    instance-of v0, p1, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 245
    :cond_4
    instance-of v0, p1, Lcom/squareup/protos/client/invoice/ArchiveInvoiceResponse;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/squareup/protos/client/invoice/ArchiveInvoiceResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/ArchiveInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    .line 246
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected response type"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;)",
            "Lcom/squareup/receiving/FailureMessage;"
        }
    .end annotation

    .line 233
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    .line 234
    sget v1, Lcom/squareup/common/strings/R$string;->failed:I

    .line 235
    new-instance v2, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory$toFailureMessage$1;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory$toFailureMessage$1;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 233
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public final createArchiveInvoiceFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/protos/client/invoice/ArchiveInvoiceResponse;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;)",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;"
        }
    .end annotation

    const-string v0, "showFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 146
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 147
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 148
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 150
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->archive_invoice:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 151
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x0

    move-object v0, v6

    .line 146
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method

.method public final createArchiveInvoiceSuccess()Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7

    .line 157
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 158
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_archived:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 161
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->archive_invoice:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 162
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v2, ""

    const/4 v3, 0x1

    move-object v0, v6

    .line 157
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method

.method public final createCancelFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;)",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;"
        }
    .end annotation

    const-string v0, "showFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 77
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 78
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 81
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_cancel_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 82
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x0

    move-object v0, v6

    .line 77
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method

.method public final createCancelSuccess()Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7

    .line 66
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 67
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_canceled:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 70
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_cancel_title:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 71
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v2, ""

    const/4 v3, 0x1

    move-object v0, v6

    .line 66
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method

.method public final createDeleteDraftFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/protos/client/invoice/DeleteDraftResponse;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;)",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;"
        }
    .end annotation

    const-string v0, "showFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 195
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 196
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 197
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 199
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    .line 200
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_delete_title:I

    .line 199
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 202
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x0

    move-object v0, v6

    .line 195
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method

.method public final createDeleteDraftSeriesFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;)",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;"
        }
    .end annotation

    const-string v0, "showFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 221
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 222
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 223
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 225
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    .line 226
    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_delete_series_title:I

    .line 225
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 228
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x0

    move-object v0, v6

    .line 221
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method

.method public final createDeleteDraftSeriesSuccess()Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7

    .line 207
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 208
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_deleted_series:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 211
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    .line 212
    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_delete_series_title:I

    .line 211
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 214
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v2, ""

    const/4 v3, 0x1

    move-object v0, v6

    .line 207
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method

.method public final createDeleteDraftSuccess()Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7

    .line 131
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 132
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_deleted:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 135
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    .line 136
    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_delete_title:I

    .line 135
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 138
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v2, ""

    const/4 v3, 0x1

    move-object v0, v6

    .line 131
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method

.method public final createEndSeriesFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/protos/client/invoice/EndRecurringSeriesResponse;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;)",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;"
        }
    .end annotation

    const-string v0, "showFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 99
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 100
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 103
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_end_series:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 104
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x0

    move-object v0, v6

    .line 99
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method

.method public final createEndSeriesSuccess()Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7

    .line 87
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 88
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_series_ended:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 91
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_end_series:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 92
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v2, ""

    const/4 v3, 0x1

    move-object v0, v6

    .line 87
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method

.method public final createSendReminderFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/protos/client/invoice/SendInvoiceReminderResponse;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;)",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;"
        }
    .end annotation

    const-string v0, "showFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 121
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 122
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 123
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 125
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->invoice_reminder_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 126
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x0

    move-object v0, v6

    .line 121
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method

.method public final createSendReminderSuccess(Ljava/lang/String;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7

    const-string v0, "payerEmail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    new-instance v0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 110
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_reminder_sent:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 113
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/features/invoices/R$string;->invoice_reminder_title:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 114
    sget-object v6, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v4, 0x1

    move-object v1, v0

    move-object v3, p1

    .line 109
    invoke-direct/range {v1 .. v6}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v0
.end method

.method public final createUnarchiveInvoiceFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/squareup/protos/client/invoice/UnarchiveInvoiceResponse;",
            ">(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+TT;>;)",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;"
        }
    .end annotation

    const-string v0, "showFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->toFailureMessage(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 171
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 172
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 173
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    .line 175
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/features/invoices/R$string;->unarchive_invoice:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 176
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v3, 0x0

    move-object v0, v6

    .line 171
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method

.method public final createUnarchiveInvoiceSuccess()Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;
    .locals 7

    .line 182
    new-instance v6, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    .line 183
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/features/invoices/R$string;->invoice_unarchived:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 186
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->unarchive_invoice:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 187
    sget-object v5, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const-string v2, ""

    const/4 v3, 0x1

    move-object v0, v6

    .line 182
    invoke-direct/range {v0 .. v5}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-object v6
.end method
