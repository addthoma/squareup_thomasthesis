.class final Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter$WhenAccessGranted;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "InvoiceIssueRefundStarter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WhenAccessGranted"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0008\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter$WhenAccessGranted;",
        "Lcom/squareup/permissions/PermissionGatekeeper$When;",
        "(Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;)V",
        "success",
        "",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 29
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter$WhenAccessGranted;->this$0:Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 5

    .line 32
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter$WhenAccessGranted;->this$0:Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;->access$getInvoiceBillLoader$p(Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;)Lcom/squareup/invoices/ui/InvoiceBillLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    .line 33
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter$WhenAccessGranted;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    .line 34
    sget-object v2, Lcom/squareup/invoices/ui/InvoicesAppletScope;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletScope;

    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 35
    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter$WhenAccessGranted;->this$0:Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

    invoke-static {v3}, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;->access$getFlow$p(Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;)Lflow/Flow;

    move-result-object v3

    const/4 v4, 0x1

    .line 31
    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/activity/IssueRefundScopeRunner;->startIssueRefundFlow(Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;Lflow/Flow;Z)V

    return-void
.end method
