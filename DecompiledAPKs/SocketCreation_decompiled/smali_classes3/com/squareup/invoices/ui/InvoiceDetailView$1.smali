.class Lcom/squareup/invoices/ui/InvoiceDetailView$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "InvoiceDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoiceDetailView;->showFileAttachmentRows(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceDetailView;

.field final synthetic val$file:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoiceDetailView;Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailView$1;->this$0:Lcom/squareup/invoices/ui/InvoiceDetailView;

    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceDetailView$1;->val$file:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 95
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceDetailView$1;->this$0:Lcom/squareup/invoices/ui/InvoiceDetailView;

    iget-object p1, p1, Lcom/squareup/invoices/ui/InvoiceDetailView;->presenter:Lcom/squareup/invoices/ui/InvoiceDetailPresenter;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceDetailView$1;->val$file:Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/InvoiceDetailPresenter;->onAttachmentClicked(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V

    return-void
.end method
