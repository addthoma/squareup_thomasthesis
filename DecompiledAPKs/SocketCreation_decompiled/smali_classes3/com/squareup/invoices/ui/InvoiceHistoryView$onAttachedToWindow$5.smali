.class final Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$5;
.super Ljava/lang/Object;
.source "InvoiceHistoryView.kt"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoiceHistoryView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "v",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "hasFocus",
        "",
        "onFocusChange"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$5;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 161
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$5;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->getPresenter$invoices_hairball_release()Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->searchClicked()V

    goto :goto_0

    .line 164
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$5;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->getPresenter$invoices_hairball_release()Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    move-result-object p1

    iget-boolean p1, p1, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->isLeavingHistoryScreen:Z

    if-nez p1, :cond_1

    .line 165
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$5;->this$0:Lcom/squareup/invoices/ui/InvoiceHistoryView;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->getPresenter$invoices_hairball_release()Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->searchExitClicked()V

    :cond_1
    :goto_0
    return-void
.end method
