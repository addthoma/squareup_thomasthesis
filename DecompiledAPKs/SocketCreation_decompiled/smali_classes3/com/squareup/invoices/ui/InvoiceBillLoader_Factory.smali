.class public final Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;
.super Ljava/lang/Object;
.source "InvoiceBillLoader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final billServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final failureMessageFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final standardReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final userTokenProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->billServiceProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->standardReceiverProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->resProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->userTokenProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;"
        }
    .end annotation

    .line 61
    new-instance v8, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/settings/server/Features;Ljava/lang/String;)Lcom/squareup/invoices/ui/InvoiceBillLoader;
    .locals 9

    .line 67
    new-instance v8, Lcom/squareup/invoices/ui/InvoiceBillLoader;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/invoices/ui/InvoiceBillLoader;-><init>(Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/settings/server/Features;Ljava/lang/String;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/ui/InvoiceBillLoader;
    .locals 8

    .line 52
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->billServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/server/bills/BillListServiceHelper;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->standardReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/receiving/StandardReceiver;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->failureMessageFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->userTokenProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    invoke-static/range {v1 .. v7}, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->newInstance(Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/settings/server/Features;Ljava/lang/String;)Lcom/squareup/invoices/ui/InvoiceBillLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceBillLoader_Factory;->get()Lcom/squareup/invoices/ui/InvoiceBillLoader;

    move-result-object v0

    return-object v0
.end method
