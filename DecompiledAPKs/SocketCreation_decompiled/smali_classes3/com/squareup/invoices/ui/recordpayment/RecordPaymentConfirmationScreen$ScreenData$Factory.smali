.class public final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;
.super Ljava/lang/Object;
.source "RecordPaymentConfirmationScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B7\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0014\u0010\u000e\u001a\u00020\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00120\u0011J\u0016\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "dateFormatter",
        "Ljava/text/DateFormat;",
        "res",
        "Lcom/squareup/util/Res;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "failureMessageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Res;Lcom/squareup/util/Clock;Lcom/squareup/receiving/FailureMessageFactory;)V",
        "createFailed",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;",
        "showFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandResponse;",
        "createSuccessful",
        "paymentAmount",
        "paymentType",
        "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Res;Lcom/squareup/util/Clock;Lcom/squareup/receiving/FailureMessageFactory;)V
    .locals 1
    .param p2    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessageFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->dateFormatter:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    iput-object p4, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    iput-object p5, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    return-void
.end method


# virtual methods
.method public final createFailed(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandResponse;",
            ">;)",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;"
        }
    .end annotation

    const-string v0, "showFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/features/invoices/R$string;->payment_recorded_failed:I

    sget-object v2, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory$createFailed$failureMessage$1;->INSTANCE:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory$createFailed$failureMessage$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 76
    new-instance v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final createSuccessful(Lcom/squareup/protos/common/Money;Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;
    .locals 3

    const-string v0, "paymentAmount"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->dateFormatter:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getGregorianCalenderInstance()Ljava/util/Calendar;

    move-result-object v1

    const-string v2, "clock.gregorianCalenderInstance"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->payment_recorded:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 58
    iget-object v2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v2, "amount"

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 59
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 62
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->payment_recorded_tender_type_on_date:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 63
    iget-object v2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData$Factory;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2}, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->getDisplayString()I

    move-result p2

    invoke-interface {v2, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string/jumbo v2, "type"

    invoke-virtual {v1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 64
    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "date"

    invoke-virtual {p2, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 65
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 66
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    .line 68
    new-instance v0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentConfirmationScreen$ScreenData;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method
