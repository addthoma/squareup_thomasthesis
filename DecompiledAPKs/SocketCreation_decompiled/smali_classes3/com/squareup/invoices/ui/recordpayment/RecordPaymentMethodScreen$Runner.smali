.class public interface abstract Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;
.super Ljava/lang/Object;
.source "RecordPaymentMethodScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u000e\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;",
        "",
        "goBackFromRecordPaymentMethodScreen",
        "",
        "method",
        "Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;",
        "recordPaymentMethodScreenData",
        "Lrx/Observable;",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract goBackFromRecordPaymentMethodScreen(Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;)V
.end method

.method public abstract recordPaymentMethodScreenData()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method
