.class public final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "RecordPaymentMethodCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0008\u0010\u0010\u001a\u00020\u000cH\u0002J\u0018\u0010\u0011\u001a\u00020\u000c2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "paymentMethodContainer",
        "Lcom/squareup/widgets/CheckableGroup;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "configureActionBar",
        "populateFromScreenData",
        "screenData",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;",
        "context",
        "Landroid/content/Context;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private paymentMethodContainer:Lcom/squareup/widgets/CheckableGroup;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->runner:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;

    iput-object p2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$getPaymentMethodContainer$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;)Lcom/squareup/widgets/CheckableGroup;
    .locals 1

    .line 18
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->paymentMethodContainer:Lcom/squareup/widgets/CheckableGroup;

    if-nez p0, :cond_0

    const-string v0, "paymentMethodContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;)Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->runner:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$populateFromScreenData(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;Landroid/content/Context;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->populateFromScreenData(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;Landroid/content/Context;)V

    return-void
.end method

.method public static final synthetic access$setPaymentMethodContainer$p(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;Lcom/squareup/widgets/CheckableGroup;)V
    .locals 0

    .line 18
    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->paymentMethodContainer:Lcom/squareup/widgets/CheckableGroup;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 69
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 70
    sget v0, Lcom/squareup/features/invoices/R$id;->record_payment_method_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.r\u2026payment_method_container)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/CheckableGroup;

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->paymentMethodContainer:Lcom/squareup/widgets/CheckableGroup;

    return-void
.end method

.method private final configureActionBar()V
    .locals 5

    .line 57
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v2, "actionBar.presenter"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 58
    iget-object v3, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/features/invoices/R$string;->record_payment_method_title:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v3, 0x1

    .line 59
    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 60
    new-instance v3, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator$configureActionBar$configBuilder$1;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator$configureActionBar$configBuilder$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 65
    iget-object v3, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v3, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v3}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final populateFromScreenData(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;Landroid/content/Context;)V
    .locals 6

    .line 42
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    .line 43
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->getMethods()Ljava/util/List;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->paymentMethodContainer:Lcom/squareup/widgets/CheckableGroup;

    const-string v2, "paymentMethodContainer"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1}, Lcom/squareup/widgets/CheckableGroup;->removeAllViews()V

    .line 46
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    .line 48
    iget-object v3, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->paymentMethodContainer:Lcom/squareup/widgets/CheckableGroup;

    if-nez v3, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->ordinal()I

    move-result v4

    iget-object v5, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {v1}, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->getDisplayString()I

    move-result v1

    invoke-interface {v5, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v5, 0x0

    .line 47
    invoke-static {p2, v3, v4, v1, v5}, Lcom/squareup/ui/CheckableGroups;->addCheckableRow(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;ILjava/lang/CharSequence;Z)V

    goto :goto_0

    .line 53
    :cond_2
    iget-object p2, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->paymentMethodContainer:Lcom/squareup/widgets/CheckableGroup;

    if-nez p2, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$ScreenData;->getCurrentPaymentMethod()Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/invoices/InvoiceRecordPaymentTenderType;->ordinal()I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->bindViews(Landroid/view/View;)V

    .line 29
    invoke-direct {p0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->configureActionBar()V

    .line 31
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;->runner:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodScreen$Runner;->recordPaymentMethodScreenData()Lrx/Observable;

    move-result-object v0

    .line 32
    new-instance v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator$attach$1;-><init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentMethodCoordinator;Landroid/view/View;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "runner.recordPaymentMeth\u2026, view.context)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    return-void
.end method
