.class final Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$attach$2;
.super Ljava/lang/Object;
.source "RecordPaymentCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$attach$2;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;)V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$attach$2;->this$0:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;->access$populateFromScreenData(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator;Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;)V

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentCoordinator$attach$2;->call(Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen$ScreenData;)V

    return-void
.end method
