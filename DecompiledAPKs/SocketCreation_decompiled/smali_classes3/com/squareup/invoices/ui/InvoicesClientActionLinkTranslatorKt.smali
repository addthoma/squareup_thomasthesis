.class public final Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslatorKt;
.super Ljava/lang/Object;
.source "InvoicesClientActionLinkTranslator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoicesClientActionLinkTranslator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoicesClientActionLinkTranslator.kt\ncom/squareup/invoices/ui/InvoicesClientActionLinkTranslatorKt\n*L\n1#1,41:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u001a\u001a\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0001*\u00020\u0004H\u0002\u001a\u000c\u0010\u0005\u001a\u00020\u0006*\u00020\u0004H\u0002\u001a\u000c\u0010\u0007\u001a\u00020\u0006*\u00020\u0004H\u0002\u00a8\u0006\u0008"
    }
    d2 = {
        "getOverdueInvoiceIds",
        "",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/protos/client/ClientAction;",
        "hasInvoiceId",
        "",
        "hasOverdueInvoices",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getOverdueInvoiceIds(Lcom/squareup/protos/client/ClientAction;)Ljava/util/List;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslatorKt;->getOverdueInvoiceIds(Lcom/squareup/protos/client/ClientAction;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$hasInvoiceId(Lcom/squareup/protos/client/ClientAction;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslatorKt;->hasInvoiceId(Lcom/squareup/protos/client/ClientAction;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$hasOverdueInvoices(Lcom/squareup/protos/client/ClientAction;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/invoices/ui/InvoicesClientActionLinkTranslatorKt;->hasOverdueInvoices(Lcom/squareup/protos/client/ClientAction;)Z

    move-result p0

    return p0
.end method

.method private static final getOverdueInvoiceIds(Lcom/squareup/protos/client/ClientAction;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/ClientAction;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 40
    iget-object p0, p0, Lcom/squareup/protos/client/ClientAction;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;->invoice_ids:Ljava/util/List;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p0

    :goto_1
    return-object p0
.end method

.method private static final hasInvoiceId(Lcom/squareup/protos/client/ClientAction;)Z
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/ClientAction$ViewInvoice;->invoice_id:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    if-eqz p0, :cond_1

    goto :goto_1

    :cond_1
    const-string p0, ""

    :goto_1
    check-cast p0, Ljava/lang/CharSequence;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result p0

    if-lez p0, :cond_2

    const/4 p0, 0x1

    goto :goto_2

    :cond_2
    const/4 p0, 0x0

    :goto_2
    return p0
.end method

.method private static final hasOverdueInvoices(Lcom/squareup/protos/client/ClientAction;)Z
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/protos/client/ClientAction;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method
