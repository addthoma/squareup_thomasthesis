.class public interface abstract Lcom/squareup/invoices/ui/BillHistoryDetailScreen$Controller;
.super Ljava/lang/Object;
.source "BillHistoryDetailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/invoices/ui/BillHistoryDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract billFromBillToken(Ljava/lang/String;)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;",
            ">;"
        }
    .end annotation
.end method

.method public abstract closeBillHistoryInvoice()V
.end method

.method public abstract reprintTicket()V
.end method

.method public abstract showIssueReceiptScreen()V
.end method

.method public abstract showIssueRefundScreen()V
.end method

.method public abstract startPrintGiftReceiptFlow()V
.end method
