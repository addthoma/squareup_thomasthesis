.class final Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator$attach$1;
.super Ljava/lang/Object;
.source "InvoiceActionBottomDialogCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ScreenData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ScreenData;)V
    .locals 3

    .line 28
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ScreenData;->getButtonData()Ljava/util/List;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;

    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;->access$getButtonContainer$p(Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;)Landroid/widget/LinearLayout;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;

    invoke-static {v1}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;->access$getFactory$p(Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;)Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator$attach$1;->this$0:Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;

    invoke-static {v2}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;->access$getRunner$p(Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator;)Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Runner;

    move-result-object v2

    check-cast v2, Lcom/squareup/features/invoices/widgets/EventHandler;

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactoryKt;->setOn(Ljava/util/List;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;Lcom/squareup/features/invoices/widgets/EventHandler;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialogCoordinator$attach$1;->accept(Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ScreenData;)V

    return-void
.end method
