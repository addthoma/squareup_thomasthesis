.class public Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;
.super Ljava/lang/Object;
.source "InvoicesDeepLinkHandler.java"

# interfaces
.implements Lcom/squareup/deeplinks/DeepLinkHandler;


# static fields
.field private static final INVOICES:Ljava/lang/String; = "invoice"

.field private static final OUTSTANDING:Ljava/lang/String; = "outstanding"

.field private static final VIEW:Ljava/lang/String; = "view"


# instance fields
.field private final defaultInvoicesStateFilter:Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;

.field private final invoiceToDeepLink:Lcom/squareup/invoices/ui/InvoiceToDeepLink;

.field private final invoicesApplet:Lcom/squareup/invoicesappletapi/InvoicesApplet;


# direct methods
.method public constructor <init>(Lcom/squareup/invoicesappletapi/InvoicesApplet;Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;Lcom/squareup/invoices/ui/InvoiceToDeepLink;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->invoicesApplet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    .line 31
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->defaultInvoicesStateFilter:Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;

    .line 32
    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->invoiceToDeepLink:Lcom/squareup/invoices/ui/InvoiceToDeepLink;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;)Lcom/squareup/invoicesappletapi/InvoicesApplet;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->invoicesApplet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    return-object p0
.end method

.method private handleInvoiceDeepLink(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 6

    .line 52
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    .line 53
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_2

    :cond_0
    const/4 v1, 0x1

    .line 58
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    .line 59
    aget-object v2, p1, v0

    const/4 v3, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v4

    const v5, -0x4e064266

    if-eq v4, v5, :cond_2

    const v5, 0x373aa5

    if-eq v4, v5, :cond_1

    goto :goto_0

    :cond_1
    const-string/jumbo v4, "view"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_2
    const-string v0, "outstanding"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_5

    if-eq v0, v1, :cond_4

    .line 86
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->invoicesApplet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    .line 83
    :cond_4
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->defaultInvoicesStateFilter:Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceStateFilter;->ALL_OUTSTANDING:Lcom/squareup/invoices/ui/InvoiceStateFilter;

    invoke-virtual {p1, v0}, Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;->setStateFilter(Lcom/squareup/invoices/ui/GenericListFilter;)V

    .line 84
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->invoicesApplet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    .line 65
    :cond_5
    aget-object p1, p1, v1

    .line 66
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->invoiceToDeepLink:Lcom/squareup/invoices/ui/InvoiceToDeepLink;

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;->NO_DELAY:Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/invoices/ui/InvoiceToDeepLink;->setInfo(Ljava/lang/String;Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;)V

    .line 68
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler$1;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler$1;-><init>(Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;)V

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    .line 54
    :cond_6
    :goto_2
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->invoicesApplet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1
.end method


# virtual methods
.method public handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 6

    .line 36
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x4

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v1, "invoice"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_1
    const-string v1, "square-alternative.app.link"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_2
    const-string v1, "square.test-app.link"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_3
    const-string v1, "bnc.lt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_4
    const-string v1, "square.app.link"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_3

    if-eq v0, v5, :cond_1

    if-eq v0, v4, :cond_1

    if-eq v0, v3, :cond_1

    if-eq v0, v2, :cond_1

    goto :goto_2

    .line 43
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    const-string v0, "/invoice"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 44
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->invoicesApplet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    .line 48
    :cond_2
    :goto_2
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    .line 38
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;->handleInvoiceDeepLink(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;

    move-result-object p1

    return-object p1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6d54daa8 -> :sswitch_4
        -0x52893ec1 -> :sswitch_3
        -0x4348448f -> :sswitch_2
        0x1284b78 -> :sswitch_1
        0x74d6432d -> :sswitch_0
    .end sparse-switch
.end method
