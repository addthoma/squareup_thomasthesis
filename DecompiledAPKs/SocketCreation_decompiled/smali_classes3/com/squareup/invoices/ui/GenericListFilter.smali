.class public interface abstract Lcom/squareup/invoices/ui/GenericListFilter;
.super Ljava/lang/Object;
.source "GenericListFilter.java"


# virtual methods
.method public abstract formatTitle(Lcom/squareup/util/Res;)Ljava/lang/String;
.end method

.method public abstract getLabel()I
.end method

.method public abstract getSearchBarHint()I
.end method

.method public abstract getTitle()I
.end method

.method public abstract isRecurringListFilter()Z
.end method

.method public abstract isStateFilter()Z
.end method
