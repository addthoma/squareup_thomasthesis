.class public Lcom/squareup/invoices/ui/InvoiceHistoryView;
.super Landroid/widget/LinearLayout;
.source "InvoiceHistoryView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRow;,
        Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceHistoryView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceHistoryView.kt\ncom/squareup/invoices/ui/InvoiceHistoryView\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,900:1\n52#2:901\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceHistoryView.kt\ncom/squareup/invoices/ui/InvoiceHistoryView\n*L\n102#1:901\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00cc\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u000e\u0008\u0016\u0018\u00002\u00020\u0001:\u0002fgB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010<\u001a\u00020=2\u0006\u0010>\u001a\u00020?H\u0016J\u0010\u0010@\u001a\u00020=2\u0006\u0010A\u001a\u00020!H\u0016J\u0016\u0010B\u001a\u00020=2\u000c\u0010C\u001a\u0008\u0012\u0004\u0012\u00020E0DH\u0016J\u0008\u0010F\u001a\u00020=H\u0002J\u0010\u0010G\u001a\u00020=2\u0006\u0010H\u001a\u00020IH\u0016J\u0010\u0010J\u001a\u00020=2\u0006\u0010K\u001a\u00020LH\u0016J\u0008\u0010M\u001a\u00020=H\u0016J\u0008\u0010N\u001a\u00020=H\u0016J\u0008\u0010O\u001a\u00020\u0008H\u0016J\u0008\u0010P\u001a\u00020=H\u0016J\u0008\u0010Q\u001a\u00020=H\u0014J\u0008\u0010R\u001a\u00020=H\u0014J\u000e\u0010S\u001a\u0008\u0012\u0004\u0012\u00020=0TH\u0016J\u0016\u0010U\u001a\u00020=2\u000c\u0010V\u001a\u0008\u0012\u0004\u0012\u00020W0DH\u0016J\u0010\u0010X\u001a\u00020=2\u0006\u0010Y\u001a\u00020ZH\u0016J\u0018\u0010[\u001a\u00020=2\u0006\u0010\\\u001a\u00020!2\u0006\u0010]\u001a\u00020!H\u0016J\u0012\u0010^\u001a\u00020=2\u0008\u0008\u0001\u0010_\u001a\u00020\u0018H\u0016J\u0010\u0010`\u001a\u00020=2\u0006\u0010a\u001a\u00020!H\u0002J\u0008\u0010b\u001a\u00020=H\u0016J\u0010\u0010c\u001a\u00020=2\u0006\u0010d\u001a\u00020!H\u0016J\u0008\u0010e\u001a\u00020!H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000b\u001a\u00020\u000c8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u001f\u001a&\u0012\u000c\u0012\n \"*\u0004\u0018\u00010!0! \"*\u0012\u0012\u000c\u0012\n \"*\u0004\u0018\u00010!0!\u0018\u00010 0 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001e\u0010#\u001a\u00020$8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008%\u0010&\"\u0004\u0008\'\u0010(R\u000e\u0010)\u001a\u00020*X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020,X\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010-\u001a\u00020.8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008/\u00100\"\u0004\u00081\u00102R\u000e\u00103\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00104\u001a\u000205X\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u00106\u001a\u0002078\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u00088\u00109\"\u0004\u0008:\u0010;\u00a8\u0006h"
    }
    d2 = {
        "Lcom/squareup/invoices/ui/InvoiceHistoryView;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "animator",
        "Lcom/squareup/widgets/SquareViewAnimator;",
        "badgePresenter",
        "Lcom/squareup/applet/BadgePresenter;",
        "getBadgePresenter$invoices_hairball_release",
        "()Lcom/squareup/applet/BadgePresenter;",
        "setBadgePresenter$invoices_hairball_release",
        "(Lcom/squareup/applet/BadgePresenter;)V",
        "dropDownArrow",
        "Lcom/squareup/marin/widgets/MarinVerticalCaretView;",
        "dropDownTitle",
        "Landroid/widget/TextView;",
        "dropDownView",
        "Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;",
        "fadeInDuration",
        "",
        "filterDropdownContainer",
        "Landroid/view/View;",
        "filterOptionsContainer",
        "invoiceList",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "minHeight",
        "onIsNearEndOfList",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "presenter",
        "Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;",
        "getPresenter$invoices_hairball_release",
        "()Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;",
        "setPresenter$invoices_hairball_release",
        "(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;)V",
        "progressBar",
        "Landroid/widget/ProgressBar;",
        "recyclerAdapter",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;",
        "scopeRunner",
        "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
        "getScopeRunner$invoices_hairball_release",
        "()Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
        "setScopeRunner$invoices_hairball_release",
        "(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V",
        "searchButton",
        "searchEditText",
        "Lcom/squareup/ui/XableEditText;",
        "viewConfiguration",
        "Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;",
        "getViewConfiguration$invoices_hairball_release",
        "()Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;",
        "setViewConfiguration$invoices_hairball_release",
        "(Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;)V",
        "addFilterRow",
        "",
        "stateFilter",
        "Lcom/squareup/invoices/ui/GenericListFilter;",
        "addHeaderRow",
        "isRecurring",
        "addInvoicesToList",
        "invoices",
        "",
        "Lcom/squareup/invoices/DisplayDetails;",
        "bindViews",
        "changeListState",
        "listState",
        "Lcom/squareup/invoices/ListState;",
        "changeLoadingRowState",
        "loadMoreState",
        "Lcom/squareup/ui/LoadMoreState;",
        "clearInvoices",
        "clearSearchTextFocus",
        "getActionBar",
        "hideFilterDropDownAndSearch",
        "onAttachedToWindow",
        "onDetachedFromWindow",
        "onNearEndOfList",
        "Lrx/Observable;",
        "refreshAnalyticsRow",
        "metrics",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
        "setFilterTitle",
        "filterTitle",
        "",
        "setSearch",
        "showSearchField",
        "withFlourish",
        "setSearchBarHint",
        "hint",
        "setSearchImmediately",
        "isSearching",
        "showFilterDropDownAndSearchIfPossible",
        "showProgressBarForLoadingDraftInvoice",
        "visible",
        "useCombinationFilterSearch",
        "InvoiceRecyclerAdapter",
        "InvoiceRow",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private animator:Lcom/squareup/widgets/SquareViewAnimator;

.field public badgePresenter:Lcom/squareup/applet/BadgePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

.field private dropDownTitle:Landroid/widget/TextView;

.field private dropDownView:Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;

.field private final fadeInDuration:I

.field private filterDropdownContainer:Landroid/view/View;

.field private filterOptionsContainer:Landroid/widget/LinearLayout;

.field private invoiceList:Landroidx/recyclerview/widget/RecyclerView;

.field private final minHeight:I

.field private final onIsNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;

.field private recyclerAdapter:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

.field public scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private searchButton:Landroid/view/View;

.field private searchEditText:Lcom/squareup/ui/XableEditText;

.field public viewConfiguration:Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->onIsNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 101
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "getContext()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 901
    const-class p2, Lcom/squareup/invoices/ui/InvoiceHistoryScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoiceHistoryScreen$Component;

    .line 103
    invoke-interface {p1, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryScreen$Component;->inject(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    .line 104
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x10e0001

    .line 105
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->fadeInDuration:I

    .line 106
    sget p2, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->minHeight:I

    return-void
.end method

.method public static final synthetic access$getDropDownArrow$p(Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lcom/squareup/marin/widgets/MarinVerticalCaretView;
    .locals 1

    .line 74
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    if-nez p0, :cond_0

    const-string v0, "dropDownArrow"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getOnIsNearEndOfList$p(Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 74
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->onIsNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getSearchEditText$p(Lcom/squareup/invoices/ui/InvoiceHistoryView;)Lcom/squareup/ui/XableEditText;
    .locals 1

    .line 74
    iget-object p0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p0, :cond_0

    const-string v0, "searchEditText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setDropDownArrow$p(Lcom/squareup/invoices/ui/InvoiceHistoryView;Lcom/squareup/marin/widgets/MarinVerticalCaretView;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    return-void
.end method

.method public static final synthetic access$setSearchEditText$p(Lcom/squareup/invoices/ui/InvoiceHistoryView;Lcom/squareup/ui/XableEditText;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    return-void
.end method

.method private final bindViews()V
    .locals 1

    .line 358
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 359
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->invoiceList:Landroidx/recyclerview/widget/RecyclerView;

    .line 360
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_history_animator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 361
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_filter_drop_down_arrow:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->dropDownArrow:Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    .line 362
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_filter_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->dropDownTitle:Landroid/widget/TextView;

    .line 363
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_filter_options_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterOptionsContainer:Landroid/widget/LinearLayout;

    .line 364
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_filter_dropdown:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->dropDownView:Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;

    .line 365
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_search:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    .line 366
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_search_activator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchButton:Landroid/view/View;

    .line 367
    sget v0, Lcom/squareup/features/invoices/R$id;->invoice_filter_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterDropdownContainer:Landroid/view/View;

    .line 368
    sget v0, Lcom/squareup/features/invoices/R$id;->progress_bar_load_invoice:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->progressBar:Landroid/widget/ProgressBar;

    return-void
.end method

.method private final setSearchImmediately(Z)V
    .locals 5

    const-string v0, "filterDropdownContainer"

    const/4 v1, 0x0

    const/16 v2, 0x8

    const-string v3, "searchEditText"

    if-eqz p1, :cond_4

    .line 342
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    .line 343
    iget-object v4, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez v4, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v4, p1, p1}, Lcom/squareup/ui/XableEditText;->setSelection(II)V

    .line 344
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->useCombinationFilterSearch()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 345
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1, v1}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    .line 346
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterDropdownContainer:Landroid/view/View;

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 349
    :cond_4
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_5

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    const/4 v4, 0x0

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {p1, v4}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 350
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->useCombinationFilterSearch()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 351
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_6

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1, v2}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    .line 352
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterDropdownContainer:Landroid/view/View;

    if-nez p1, :cond_7

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_8
    :goto_0
    return-void
.end method

.method private final useCombinationFilterSearch()Z
    .locals 2

    .line 373
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->hasNeverCreatedInvoice()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 374
    :cond_1
    invoke-static {p0}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    :goto_0
    return v1
.end method


# virtual methods
.method public addFilterRow(Lcom/squareup/invoices/ui/GenericListFilter;)V
    .locals 2

    const-string v0, "stateFilter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    sget v0, Lcom/squareup/features/invoices/R$layout;->dropdown_item:I

    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 275
    invoke-interface {p1}, Lcom/squareup/invoices/ui/GenericListFilter;->getTitle()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 277
    new-instance v1, Lcom/squareup/invoices/ui/InvoiceHistoryView$addFilterRow$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$addFilterRow$1;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;Lcom/squareup/invoices/ui/GenericListFilter;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 283
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterOptionsContainer:Landroid/widget/LinearLayout;

    if-nez p1, :cond_0

    const-string v1, "filterOptionsContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public addHeaderRow(Z)V
    .locals 2

    .line 264
    sget v0, Lcom/squareup/features/invoices/R$layout;->invoice_state_filter_dropdown_header:I

    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_0

    .line 266
    sget p1, Lcom/squareup/features/invoices/R$string;->titlecase_invoices:I

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    goto :goto_0

    .line 268
    :cond_0
    sget p1, Lcom/squareup/features/invoices/R$string;->titlecase_recurring_series:I

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 270
    :goto_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterOptionsContainer:Landroid/widget/LinearLayout;

    if-nez p1, :cond_1

    const-string v1, "filterOptionsContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public addInvoicesToList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;)V"
        }
    .end annotation

    const-string v0, "invoices"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->recyclerAdapter:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    if-nez v0, :cond_0

    const-string v1, "recyclerAdapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->addItems(Ljava/util/List;)V

    return-void
.end method

.method public changeListState(Lcom/squareup/invoices/ListState;)V
    .locals 2

    const-string v0, "listState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 332
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceHistoryView$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/invoices/ListState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 335
    sget p1, Lcom/squareup/features/invoices/R$id;->invoice_list:I

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 334
    :cond_1
    sget p1, Lcom/squareup/features/invoices/R$id;->invoices_list_error_message:I

    goto :goto_0

    .line 333
    :cond_2
    sget p1, Lcom/squareup/features/invoices/R$id;->invoice_history_progress_bar:I

    .line 337
    :goto_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez v0, :cond_3

    const-string v1, "animator"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public changeLoadingRowState(Lcom/squareup/ui/LoadMoreState;)V
    .locals 2

    const-string v0, "loadMoreState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->recyclerAdapter:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    if-nez v0, :cond_0

    const-string v1, "recyclerAdapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->changeLoadingState(Lcom/squareup/ui/LoadMoreState;)V

    return-void
.end method

.method public clearInvoices()V
    .locals 2

    .line 240
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->recyclerAdapter:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    if-nez v0, :cond_0

    const-string v1, "recyclerAdapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->clearItems()V

    return-void
.end method

.method public clearSearchTextFocus()V
    .locals 2

    .line 287
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "searchEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->clearFocus()V

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/ActionBarView;
    .locals 2

    .line 208
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getBadgePresenter$invoices_hairball_release()Lcom/squareup/applet/BadgePresenter;
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    if-nez v0, :cond_0

    const-string v1, "badgePresenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getPresenter$invoices_hairball_release()Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    if-nez v0, :cond_0

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getScopeRunner$invoices_hairball_release()Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    if-nez v0, :cond_0

    const-string v1, "scopeRunner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getViewConfiguration$invoices_hairball_release()Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->viewConfiguration:Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;

    if-nez v0, :cond_0

    const-string/jumbo v1, "viewConfiguration"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public hideFilterDropDownAndSearch()V
    .locals 3

    .line 234
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterDropdownContainer:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "filterDropdownContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_1

    const-string v2, "searchEditText"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchButton:Landroid/view/View;

    if-nez v0, :cond_2

    const-string v2, "searchButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 7

    .line 110
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 112
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->bindViews()V

    .line 113
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    if-nez v0, :cond_0

    const-string v1, "badgePresenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v1, :cond_1

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v1}, Lcom/squareup/applet/BadgePresenter;->takeView(Ljava/lang/Object;)V

    .line 115
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    const-string v2, "presenter"

    if-nez v1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->viewConfiguration:Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;

    if-nez v3, :cond_3

    const-string/jumbo v4, "viewConfiguration"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-interface {v3}, Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;->getShouldShowCreateButton()Z

    move-result v3

    invoke-direct {v0, v1, v3}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;Z)V

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->recyclerAdapter:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    .line 116
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->invoiceList:Landroidx/recyclerview/widget/RecyclerView;

    const-string v1, "invoiceList"

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->recyclerAdapter:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    const-string v4, "recyclerAdapter"

    if-nez v3, :cond_5

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v3, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v3}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 117
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 118
    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->invoiceList:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v3, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    move-object v5, v0

    check-cast v5, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    invoke-virtual {v3, v5}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 119
    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->invoiceList:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v3, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    new-instance v5, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;

    iget-object v6, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->recyclerAdapter:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    if-nez v6, :cond_8

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v6, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    invoke-direct {v5, v6}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;-><init>(Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;)V

    check-cast v5, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v3, v5}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 121
    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->invoiceList:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v3, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    new-instance v1, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$1;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;Landroidx/recyclerview/widget/LinearLayoutManager;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;

    invoke-virtual {v3, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 135
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->dropDownTitle:Landroid/widget/TextView;

    const-string v1, "dropDownTitle"

    if-nez v0, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->dropDownTitle:Landroid/widget/TextView;

    if-nez v3, :cond_b

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v3

    iget v4, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->minHeight:I

    mul-int/lit8 v4, v4, 0x2

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v5, v4, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 136
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->dropDownTitle:Landroid/widget/TextView;

    if-nez v0, :cond_c

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    new-instance v1, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$2;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$2;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->dropDownView:Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;

    if-nez v0, :cond_d

    const-string v1, "dropDownView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    new-instance v1, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$3;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$3;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    check-cast v1, Lcom/squareup/ui/DropDownContainer$DropDownListener;

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoiceFilterDropDownView;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    .line 152
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchButton:Landroid/view/View;

    if-nez v0, :cond_e

    const-string v1, "searchButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    new-instance v1, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$4;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$4;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    const-string v1, "searchEditText"

    if-nez v0, :cond_f

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    new-instance v3, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$5;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$5;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    check-cast v3, Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/XableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 170
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_10

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    new-instance v3, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$6;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$6;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/XableEditText;->setOnButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_11

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    new-instance v3, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$7;

    invoke-direct {v3, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$7;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    check-cast v3, Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_12

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    new-instance v1, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$8;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView$onAttachedToWindow$8;-><init>(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    check-cast v1, Lcom/squareup/debounce/DebouncedOnEditorActionListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 199
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    if-nez v0, :cond_13

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    invoke-virtual {v0, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .line 203
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    if-nez v0, :cond_0

    const-string v1, "badgePresenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v1, :cond_1

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v1, Lcom/squareup/marin/widgets/Badgeable;

    invoke-virtual {v0, v1}, Lcom/squareup/applet/BadgePresenter;->dropView(Lcom/squareup/marin/widgets/Badgeable;)V

    .line 204
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    if-nez v0, :cond_2

    const-string v1, "presenter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, p0}, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;->dropView(Ljava/lang/Object;)V

    .line 205
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public onNearEndOfList()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 215
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->onIsNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 216
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/PublishRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    .line 217
    sget-object v1, Lcom/squareup/invoices/ui/InvoiceHistoryView$onNearEndOfList$1;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceHistoryView$onNearEndOfList$1;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 218
    sget-object v1, Lcom/squareup/invoices/ui/InvoiceHistoryView$onNearEndOfList$2;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceHistoryView$onNearEndOfList$2;

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "onIsNearEndOfList\n      \u2026       .map { a -> Unit }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public refreshAnalyticsRow(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
            ">;)V"
        }
    .end annotation

    const-string v0, "metrics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->recyclerAdapter:Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;

    if-nez v0, :cond_0

    const-string v1, "recyclerAdapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView$InvoiceRecyclerAdapter;->updateAnalyticsRow(Ljava/util/List;)V

    return-void
.end method

.method public final setBadgePresenter$invoices_hairball_release(Lcom/squareup/applet/BadgePresenter;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    return-void
.end method

.method public setFilterTitle(Ljava/lang/String;)V
    .locals 2

    const-string v0, "filterTitle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->dropDownTitle:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "dropDownTitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setPresenter$invoices_hairball_release(Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    return-void
.end method

.method public final setScopeRunner$invoices_hairball_release(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    return-void
.end method

.method public setSearch(ZZ)V
    .locals 6

    if-nez p2, :cond_0

    .line 295
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->setSearchImmediately(Z)V

    goto/16 :goto_0

    :cond_0
    const/16 p2, 0x8

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v4, "filterDropdownContainer"

    const-string v5, "searchEditText"

    if-eqz p1, :cond_a

    .line 297
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->useCombinationFilterSearch()Z

    move-result p1

    if-eqz p1, :cond_5

    .line 298
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_1

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1, v2}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    .line 299
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_2

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1, v1}, Lcom/squareup/ui/XableEditText;->setAlpha(F)V

    .line 300
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_3

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 301
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 302
    iget v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->fadeInDuration:I

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 303
    invoke-virtual {p1, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 305
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterDropdownContainer:Landroid/view/View;

    if-nez p1, :cond_4

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 308
    :cond_5
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_6

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    const/4 p2, 0x1

    invoke-static {p1, v2, p2, v3}, Lcom/squareup/util/Views;->showSoftKeyboard$default(Landroid/view/View;ZILjava/lang/Object;)V

    .line 309
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_7

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    .line 310
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_8

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    .line 311
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p2, :cond_9

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {p2, p1, p1}, Lcom/squareup/ui/XableEditText;->setSelection(II)V

    goto :goto_0

    .line 313
    :cond_a
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->useCombinationFilterSearch()Z

    move-result p1

    if-eqz p1, :cond_f

    .line 314
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterDropdownContainer:Landroid/view/View;

    if-nez p1, :cond_b

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 315
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterDropdownContainer:Landroid/view/View;

    if-nez p1, :cond_c

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 316
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterDropdownContainer:Landroid/view/View;

    if-nez p1, :cond_d

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 317
    invoke-virtual {p1, v0}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 318
    iget v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->fadeInDuration:I

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object p1

    .line 319
    invoke-virtual {p1, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 321
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_e

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    invoke-virtual {p1, p2}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    .line 324
    :cond_f
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_10

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 325
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_11

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    invoke-virtual {p1, v3}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 326
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_12

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->clearFocus()V

    :goto_0
    return-void
.end method

.method public setSearchBarHint(I)V
    .locals 2

    .line 260
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "searchEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method public final setViewConfiguration$invoices_hairball_release(Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->viewConfiguration:Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;

    return-void
.end method

.method public showFilterDropDownAndSearchIfPossible()V
    .locals 6

    .line 222
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoiceHistoryView;->useCombinationFilterSearch()Z

    move-result v0

    const-string v1, "searchButton"

    const-string v2, "searchEditText"

    const-string v3, "filterDropdownContainer"

    const/4 v4, 0x0

    const/16 v5, 0x8

    if-eqz v0, :cond_3

    .line 223
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterDropdownContainer:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 224
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v5}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchButton:Landroid/view/View;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 227
    :cond_3
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->filterDropdownContainer:Landroid/view/View;

    if-nez v0, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 228
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchEditText:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0, v4}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    .line 229
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->searchButton:Landroid/view/View;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {v0, v5}, Landroid/view/View;->setSystemUiVisibility(I)V

    :goto_0
    return-void
.end method

.method public showProgressBarForLoadingDraftInvoice(Z)V
    .locals 2

    .line 211
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->progressBar:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    const-string v1, "progressBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    if-eqz p1, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method
