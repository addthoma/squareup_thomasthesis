.class public Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;
.super Ljava/lang/Object;
.source "InvoicesAppletScopeRunner.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/invoices/ui/AddPaymentScreen$Runner;
.implements Lcom/squareup/invoices/ui/CancelInvoiceScreen$Runner;
.implements Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$Runner;
.implements Lcom/squareup/invoices/InvoiceTimelineScreen$Runner;
.implements Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$Runner;
.implements Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResultHandler;
.implements Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentResultHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;
    }
.end annotation


# static fields
.field private static final DEFAULT_PAGE_SIZE:I = 0x14

.field private static final DISPLAY_DETAILS_INPUT_KEY:Ljava/lang/String; = "display_details_input"

.field private static final GET_INVOICE_DELAY_SECONDS:I = 0x4


# instance fields
.field private addPaymentScreenDataFactory:Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

.field private final cancelInvoiceScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;",
            ">;"
        }
    .end annotation
.end field

.field private final cancelScreenDataFactory:Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;

.field private final confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

.field private final currentDisplayDetails:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;>;"
        }
    .end annotation
.end field

.field private final currentListFilter:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/invoices/ui/GenericListFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultInvoicesStateFilter:Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;

.field private final displayDetailsInput:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;",
            ">;>;"
        }
    .end annotation
.end field

.field private final downloadManager:Lcom/squareup/http/SquareDownloadManager;

.field private final editInvoiceGateway:Lcom/squareup/invoices/edit/EditInvoiceGateway;

.field private final errorGettingInvoice:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;>;"
        }
    .end annotation
.end field

.field private final estimatesGateway:Lcom/squareup/invoices/timeline/EstimatesGateway;

.field private final failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final fetchingOrCloning:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final invoiceActionBottomDialogScreenData:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

.field private final invoiceDetailTimelineDataFactory:Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;

.field private final invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

.field private final invoiceIssueRefundStarter:Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

.field private final invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicePaymentStarter:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;

.field private final invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

.field private final invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

.field private final invoiceToDeepLink:Lcom/squareup/invoices/ui/InvoiceToDeepLink;

.field private final invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

.field private final invoicesApplet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

.field private final invoicesAppletRunner:Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;

.field private invoicesAppletScope:Lcom/squareup/invoices/ui/InvoicesAppletScope;

.field private final isSearching:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final loader:Lcom/squareup/invoices/ui/InvoiceLoader;

.field private final loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final standardReceiver:Lcom/squareup/receiving/StandardReceiver;

.field private final subscriptions:Lio/reactivex/disposables/CompositeDisposable;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;


# direct methods
.method public constructor <init>(Lcom/squareup/invoicesappletapi/InvoicesApplet;Lcom/squareup/invoices/ui/InvoiceLoader;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/invoicesappletapi/InvoiceUnitCache;Lcom/squareup/register/tutorial/InvoiceTutorialRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TransactionDiscountAdapter;Lcom/squareup/receiving/StandardReceiver;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/text/Formatter;Lcom/squareup/invoices/ui/InvoiceBillLoader;Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;Lcom/squareup/url/InvoiceShareUrlLauncher;Lcom/squareup/util/Res;Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;Lcom/squareup/invoices/timeline/EstimatesGateway;Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;Lcom/squareup/invoices/edit/EditInvoiceGateway;Lcom/squareup/http/SquareDownloadManager;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;Lcom/squareup/invoices/image/InvoiceFileHelper;Lcom/squareup/invoices/ui/InvoiceToDeepLink;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoicesappletapi/InvoicesApplet;",
            "Lcom/squareup/invoices/ui/InvoiceLoader;",
            "Lcom/squareup/analytics/Analytics;",
            "Lflow/Flow;",
            "Lcom/squareup/invoices/ClientInvoiceServiceHelper;",
            "Lcom/squareup/invoicesappletapi/InvoiceUnitCache;",
            "Lcom/squareup/register/tutorial/InvoiceTutorialRunner;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/tender/TenderStarter;",
            "Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/payment/TransactionDiscountAdapter;",
            "Lcom/squareup/receiving/StandardReceiver;",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/invoices/ui/InvoiceBillLoader;",
            "Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;",
            "Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;",
            "Lcom/squareup/invoices/timeline/EstimatesGateway;",
            "Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;",
            "Lcom/squareup/invoices/edit/EditInvoiceGateway;",
            "Lcom/squareup/http/SquareDownloadManager;",
            "Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            "Lcom/squareup/invoices/ui/InvoiceToDeepLink;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 173
    new-instance v1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    const/4 v1, 0x0

    .line 174
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 178
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cancelInvoiceScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 181
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceActionBottomDialogScreenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 184
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 186
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentListFilter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 187
    invoke-static {v1}, Lrx/subjects/BehaviorSubject;->create(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->isSearching:Lrx/subjects/BehaviorSubject;

    const-string v2, ""

    .line 188
    invoke-static {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 190
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 192
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->displayDetailsInput:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 193
    invoke-static {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->fetchingOrCloning:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 195
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-static {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->errorGettingInvoice:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 196
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;

    move-object v1, p1

    .line 223
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoicesApplet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    move-object v1, p2

    .line 224
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loader:Lcom/squareup/invoices/ui/InvoiceLoader;

    move-object v1, p3

    .line 225
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p4

    .line 226
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    move-object v1, p5

    .line 227
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    move-object v1, p6

    .line 228
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    move-object v1, p7

    .line 229
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    move-object v1, p8

    .line 230
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p9

    .line 231
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    move-object v1, p10

    .line 232
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoicesAppletRunner:Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;

    move-object v1, p11

    .line 233
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    move-object v1, p12

    .line 234
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    move-object/from16 v1, p13

    .line 235
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;

    move-object/from16 v1, p14

    .line 236
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    move-object/from16 v1, p15

    .line 237
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    move-object/from16 v1, p16

    .line 238
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    move-object/from16 v1, p17

    .line 239
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    move-object/from16 v1, p18

    .line 240
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceIssueRefundStarter:Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

    move-object/from16 v1, p20

    .line 241
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->addPaymentScreenDataFactory:Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;

    move-object/from16 v1, p21

    .line 242
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

    move-object/from16 v1, p19

    .line 243
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    move-object/from16 v1, p23

    .line 244
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceDetailTimelineDataFactory:Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;

    move-object/from16 v1, p22

    .line 245
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->res:Lcom/squareup/util/Res;

    move-object/from16 v1, p24

    .line 246
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cancelScreenDataFactory:Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;

    move-object/from16 v1, p25

    .line 247
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->estimatesGateway:Lcom/squareup/invoices/timeline/EstimatesGateway;

    move-object/from16 v1, p26

    .line 248
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->defaultInvoicesStateFilter:Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;

    move-object/from16 v1, p27

    .line 249
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    move-object/from16 v1, p28

    .line 250
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->editInvoiceGateway:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    move-object/from16 v1, p29

    .line 251
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->downloadManager:Lcom/squareup/http/SquareDownloadManager;

    move-object/from16 v1, p30

    .line 252
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoicePaymentStarter:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;

    move-object/from16 v1, p31

    .line 253
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    move-object/from16 v1, p32

    .line 254
    iput-object v1, v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceToDeepLink:Lcom/squareup/invoices/ui/InvoiceToDeepLink;

    return-void
.end method

.method private archiveInvoice()V
    .locals 3

    .line 1079
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    .line 1080
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    .line 1081
    invoke-virtual {v2, v0}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->archiveInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$3QAsz1TAPjXjrwQ5DvLsGCxtRgI;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$3QAsz1TAPjXjrwQ5DvLsGCxtRgI;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1082
    invoke-virtual {v0, v2}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$1oNLmNZH8_yLSFaGDzXol0iSl84;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$1oNLmNZH8_yLSFaGDzXol0iSl84;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1083
    invoke-virtual {v0, v2}, Lio/reactivex/Single;->doAfterTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ZtHpItZadVFeaFaB9uHm3f6y78U;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ZtHpItZadVFeaFaB9uHm3f6y78U;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1084
    invoke-virtual {v0, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 1080
    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private deleteDraft()V
    .locals 2

    .line 1023
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_DELETE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 1024
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    .line 1026
    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->isRecurring()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1027
    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->handleDeleteDraftSeries(Lcom/squareup/protos/client/IdPair;)V

    goto :goto_0

    .line 1029
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-direct {p0, v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->handleDeleteDraft(Lcom/squareup/protos/client/IdPair;)V

    :goto_0
    return-void
.end method

.method private downloadInvoice()V
    .locals 4

    .line 1003
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_DOWNLOAD_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 1004
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    .line 1005
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->download_invoice_pdf_path:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 1006
    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getId()Ljava/lang/String;

    move-result-object v2

    const-string v3, "invoice_token"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 1007
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 1008
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1010
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->downloadManager:Lcom/squareup/http/SquareDownloadManager;

    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$G5Ueqd3Zd800qtKa1H3qfwLbwqc;

    invoke-direct {v3, v0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$G5Ueqd3Zd800qtKa1H3qfwLbwqc;-><init>(Lcom/squareup/invoices/DisplayDetails;)V

    invoke-interface {v2, v1, v3}, Lcom/squareup/http/SquareDownloadManager;->enqueue(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)J

    return-void
.end method

.method private endSeries()V
    .locals 4

    .line 908
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    .line 909
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    .line 911
    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->version:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->endSeries(Ljava/lang/String;Ljava/lang/String;)Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$6cdSup6FJ-06Ah8_Qp_9pVos178;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$6cdSup6FJ-06Ah8_Qp_9pVos178;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 912
    invoke-virtual {v0, v2}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$vQE32asni6JPg_IPpz1yDHW6E0k;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$vQE32asni6JPg_IPpz1yDHW6E0k;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 913
    invoke-virtual {v0, v2}, Lrx/Observable;->doAfterTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$75IXJN0JTN9S9BcZY5gwFX_55N4;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$75IXJN0JTN9S9BcZY5gwFX_55N4;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 914
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 910
    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 909
    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private getContact()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
            ">;>;"
        }
    .end annotation

    .line 931
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceContact;->contact_token:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexServiceHelper;->getContact(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ld0YxqRaP23S3QTS9sycGvYznSo;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ld0YxqRaP23S3QTS9sycGvYznSo;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 932
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$TPteVDtJjDeA34QkFGdCo4Y_zLs;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$TPteVDtJjDeA34QkFGdCo4Y_zLs;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 933
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private getCurrentInvoiceDisplayState()Lcom/squareup/invoices/InvoiceDisplayState;
    .locals 1

    .line 344
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    .line 346
    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 345
    invoke-static {v0}, Lcom/squareup/invoices/InvoiceDisplayState;->forInvoiceDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v0

    return-object v0
.end method

.method private getInvoiceAndWrapDisplayDetails(Ljava/lang/String;Z)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;>;"
        }
    .end annotation

    if-eqz p2, :cond_0

    const-wide/16 v0, 0x4

    .line 970
    sget-object p2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, p2}, Lrx/Observable;->timer(JLjava/util/concurrent/TimeUnit;)Lrx/Observable;

    move-result-object p2

    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$YMvVr6J7HMYYHnFNPA0eOH9l37Q;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$YMvVr6J7HMYYHnFNPA0eOH9l37Q;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Ljava/lang/String;)V

    .line 971
    invoke-virtual {p2, v0}, Lrx/Observable;->flatMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    goto :goto_0

    .line 973
    :cond_0
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {p2, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->get(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    .line 976
    :goto_0
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$JHXHwlKSOoseQrqkBvg90Kh1PNY;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$JHXHwlKSOoseQrqkBvg90Kh1PNY;

    .line 977
    invoke-static {p2, v0}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$11nIuVIva-f5AFbjKtMr2gd6--s;

    invoke-direct {p2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$11nIuVIva-f5AFbjKtMr2gd6--s;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 978
    invoke-virtual {p1, p2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method private getSeriesAndWrapDisplayDetails(Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;>;"
        }
    .end annotation

    .line 947
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->getRecurringSeries(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$vQaU27yBG697dHuBvQG_tCheuRY;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$vQaU27yBG697dHuBvQG_tCheuRY;

    .line 948
    invoke-static {v0, v1}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$713eTEICbJ3iBVzZFyMNooZSM0Q;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$713eTEICbJ3iBVzZFyMNooZSM0Q;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 949
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method private goToPaymentScreen()V
    .locals 1

    .line 772
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderStarter;->startTenderFlow()V

    return-void
.end method

.method private goToTimelineScreen()V
    .locals 3

    .line 767
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_VIEW_TIMELINE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 768
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/InvoiceTimelineScreen;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoicesAppletScope:Lcom/squareup/invoices/ui/InvoicesAppletScope;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/InvoiceTimelineScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private handleDeleteDraft(Lcom/squareup/protos/client/IdPair;)V
    .locals 2

    .line 1034
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    .line 1036
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->deleteDraft(Lcom/squareup/protos/client/IdPair;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ojxHMA5M9oHR5x0pgpYNTw1WvTc;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ojxHMA5M9oHR5x0pgpYNTw1WvTc;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1037
    invoke-virtual {p1, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$wGkoPE-w5lwYd6gCZMAcELgNgaU;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$wGkoPE-w5lwYd6gCZMAcELgNgaU;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1038
    invoke-virtual {p1, v1}, Lrx/Observable;->doAfterTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$POQ-CZQagbv4-2ucn8gFYz0Afac;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$POQ-CZQagbv4-2ucn8gFYz0Afac;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1039
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 1035
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 1034
    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private handleDeleteDraftSeries(Lcom/squareup/protos/client/IdPair;)V
    .locals 2

    .line 1056
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    .line 1058
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->deleteDraftSeries(Lcom/squareup/protos/client/IdPair;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$vDx7x889akbmo9egefIyui9rNtA;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$vDx7x889akbmo9egefIyui9rNtA;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1059
    invoke-virtual {p1, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$wWFOFzJjrMpgcN32qFkbmleFeg4;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$wWFOFzJjrMpgcN32qFkbmleFeg4;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1060
    invoke-virtual {p1, v1}, Lrx/Observable;->doAfterTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$hbRdhTgI_9gta_AWpc43p9F_HEI;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$hbRdhTgI_9gta_AWpc43p9F_HEI;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1061
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 1057
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 1056
    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private handleTimelineCtaClicked(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;)V
    .locals 2

    .line 1116
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;->getCallToAction()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    move-result-object p1

    .line 1117
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    .line 1118
    sget-object v1, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$protos$client$invoice$InvoiceTimeline$CallToActionTarget:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 1126
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;->INVOICE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    if-eq v0, v1, :cond_1

    .line 1131
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->estimatesGateway:Lcom/squareup/invoices/timeline/EstimatesGateway;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->call_to_action_link:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_value:Ljava/lang/String;

    invoke-interface {v0, p1}, Lcom/squareup/invoices/timeline/EstimatesGateway;->goToEstimateDetail(Ljava/lang/String;)V

    goto :goto_0

    .line 1128
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot have link type of INVOICE in invoice timeline."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 1123
    :cond_2
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;->target_url:Ljava/lang/String;

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    goto :goto_0

    .line 1120
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->displaySendReminderConfirmation()V

    :goto_0
    return-void
.end method

.method public static synthetic lambda$8RL3DvJN7n-mpoKZQGisSIHNMzk(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/protos/client/invoice/CloneInvoiceResponse;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->onDuplicateInvoiceSuccess(Lcom/squareup/protos/client/invoice/CloneInvoiceResponse;)V

    return-void
.end method

.method public static synthetic lambda$Fho5dku3rJG4pWahZ-HQM4Rv6lc(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->onCancelInvoiceFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V

    return-void
.end method

.method public static synthetic lambda$IGmsAKJKL58tAqh08wBriphRNDY(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->onSendReminderFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V

    return-void
.end method

.method public static synthetic lambda$OHnR5q7rqN7BjBRxYTErJP67o0E(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->onEndSeriesFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V

    return-void
.end method

.method static synthetic lambda$addPayment$28(Lcom/squareup/util/Optional;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 804
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/DisplayDetails;

    invoke-virtual {p0}, Lcom/squareup/invoices/DisplayDetails;->isRecurring()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$addPayment$29(Lcom/squareup/util/Optional;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 806
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$cancelInvoice$8(Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 522
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$downloadInvoice$55(Lcom/squareup/invoices/DisplayDetails;Landroid/app/DownloadManager$Request;)Lkotlin/Unit;
    .locals 1

    .line 1011
    invoke-virtual {p1}, Landroid/app/DownloadManager$Request;->allowScanningByMediaScanner()V

    .line 1013
    invoke-static {p0}, Lcom/squareup/invoices/ui/recordpayment/InvoiceDownloadHelper;->generateFilename(Lcom/squareup/invoices/DisplayDetails;)Ljava/lang/String;

    move-result-object p0

    .line 1014
    invoke-virtual {p1, p0}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    move-result-object p1

    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    .line 1015
    invoke-virtual {p1, v0, p0}, Landroid/app/DownloadManager$Request;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    move-result-object p0

    const/4 p1, 0x1

    .line 1016
    invoke-virtual {p0, p1}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 1018
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method static synthetic lambda$duplicateInvoice$12(Lcom/squareup/protos/client/invoice/CloneInvoiceResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 638
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/CloneInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$getInvoiceAndWrapDisplayDetails$52(Lcom/squareup/protos/client/invoice/GetInvoiceResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 977
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/GetInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$getSeriesAndWrapDisplayDetails$48(Lcom/squareup/protos/client/invoice/GetRecurringSeriesResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 948
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/GetRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p0, p0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$handleResult$31(Lcom/squareup/util/Optional;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 825
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 826
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/DisplayDetails;

    invoke-virtual {p0}, Lcom/squareup/invoices/DisplayDetails;->isRecurring()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$handleResult$32(Lcom/squareup/util/Optional;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 829
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$33(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lkotlin/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 838
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 839
    new-instance v0, Lkotlin/Pair;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 841
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-direct {v0, p0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 843
    :cond_0
    new-instance p1, Lkotlin/Pair;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method static synthetic lambda$null$40(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-void
.end method

.method static synthetic lambda$null$49(Lcom/squareup/protos/client/invoice/GetRecurringSeriesResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 1

    .line 958
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/GetRecurringSeriesResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method static synthetic lambda$null$53(Lcom/squareup/protos/client/invoice/GetInvoiceResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 1

    .line 994
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/GetInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method public static synthetic lambda$o1iE0I6A7Kc1mekuffYLqIUs2q4(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->onDuplicateInvoiceFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V

    return-void
.end method

.method static synthetic lambda$onDuplicateInvoiceFailure$14(Lcom/squareup/protos/client/invoice/CloneInvoiceResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 1

    .line 653
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/CloneInvoiceResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-direct {v0, p0}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>(Lcom/squareup/protos/client/Status;)V

    return-object v0
.end method

.method static synthetic lambda$onEnterScope$3(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 305
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_invoices:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$setCurrentStateAndGoToHistoryIfNecessary$6(Lflow/History;)Lcom/squareup/container/Command;
    .locals 5

    .line 433
    invoke-virtual {p0}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    .line 434
    const-class v2, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/invoices/InvoiceTimelineScreen;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf([Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 435
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceHistoryScreen;

    invoke-static {p0, v0}, Lcom/squareup/container/Command;->set(Lflow/History;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0

    .line 438
    :cond_0
    const-class v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen;

    invoke-virtual {v0, v1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 439
    invoke-virtual {p0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p0

    new-array v0, v4, [Ljava/lang/Class;

    const-class v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen;

    aput-object v1, v0, v3

    invoke-static {p0, v0}, Lcom/squareup/container/Histories;->popWhile(Lflow/History$Builder;[Ljava/lang/Class;)Lflow/History$Builder;

    move-result-object p0

    .line 440
    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    sget-object v0, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-static {p0, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0

    .line 444
    :cond_1
    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p0, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$timelineScreenData$76(Lcom/squareup/util/Optional;)Ljava/lang/Boolean;
    .locals 1

    .line 1139
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/invoices/DisplayDetails;

    invoke-virtual {p0}, Lcom/squareup/invoices/DisplayDetails;->isRecurring()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private onCancelInvoiceFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+",
            "Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;",
            ">;)V"
        }
    .end annotation

    .line 549
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createCancelFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 550
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private onCancelInvoiceSuccess(Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;Z)V
    .locals 1

    if-eqz p2, :cond_0

    .line 539
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/invoices/ui/CancelInvoiceScreen;

    invoke-static {p2, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 540
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    invoke-static {v0}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 541
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->showIssueRefund()V

    goto :goto_0

    .line 543
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    invoke-virtual {p2}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createCancelSuccess()Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 544
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object p2, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private onDuplicateInvoiceFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+",
            "Lcom/squareup/protos/client/invoice/CloneInvoiceResponse;",
            ">;)V"
        }
    .end annotation

    .line 651
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/common/strings/R$string;->failed:I

    sget-object v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$qm08kwlsyZ7QUsGERvyPcqCqcHg;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$qm08kwlsyZ7QUsGERvyPcqCqcHg;

    .line 652
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 654
    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    .line 655
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    const/4 v2, 0x0

    const-string v3, "Invoice Duplication Error"

    invoke-direct {v1, v0, v2, v3}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;ZLjava/lang/String;)V

    invoke-virtual {p1, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private onDuplicateInvoiceSuccess(Lcom/squareup/protos/client/invoice/CloneInvoiceResponse;)V
    .locals 1

    .line 646
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->editInvoiceGateway:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/CloneInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    .line 647
    invoke-static {p1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->duplicateSingleInvoiceContext(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object p1

    .line 646
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->goToEditInvoiceInApplet(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    return-void
.end method

.method private onEndSeriesFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+",
            "Lcom/squareup/protos/client/invoice/EndRecurringSeriesResponse;",
            ">;)V"
        }
    .end annotation

    .line 926
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createEndSeriesFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 927
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private onEndSeriesSuccess()V
    .locals 2

    .line 921
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    invoke-virtual {v1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createEndSeriesSuccess()Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 922
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private onSendReminderFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "+",
            "Lcom/squareup/protos/client/invoice/SendInvoiceReminderResponse;",
            ">;)V"
        }
    .end annotation

    .line 758
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createSendReminderFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 759
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private onSendReminderSuccess()V
    .locals 3

    .line 752
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    .line 753
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    invoke-virtual {v2, v0}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createSendReminderSuccess(Ljava/lang/String;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 754
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private showIssueRefund()V
    .locals 3

    .line 705
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    .line 708
    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 710
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    .line 711
    invoke-virtual {v2, v0}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->loadFromToken(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$2ClvwzZyswvwkvpHqc4HAfsTq5w;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$2ClvwzZyswvwkvpHqc4HAfsTq5w;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 712
    invoke-virtual {v0, v2}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$NFLxqw18pMNgRomL5EildLIIvvk;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$NFLxqw18pMNgRomL5EildLIIvvk;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 713
    invoke-virtual {v0, v2}, Lio/reactivex/Single;->doOnEvent(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$Z4qXMScQHQ4RxveauukdfBB0cBY;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$Z4qXMScQHQ4RxveauukdfBB0cBY;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 714
    invoke-virtual {v0, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 710
    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private startPayment(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 2

    .line 941
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    const/4 v1, 0x1

    invoke-virtual {v0, p2, p1, v1}, Lcom/squareup/payment/Transaction;->startInvoiceTransaction(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/client/rolodex/Contact;Z)V

    .line 942
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->attributeChargeIfPossible()V

    .line 943
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->goToPaymentScreen()V

    return-void
.end method

.method private unarchiveInvoice()V
    .locals 3

    .line 1097
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    .line 1098
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    .line 1099
    invoke-virtual {v2, v0}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->unarchiveInvoice(Lcom/squareup/protos/client/invoice/Invoice;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$h2dHPFuzbpxMSEmGWMFx6A96l3g;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$h2dHPFuzbpxMSEmGWMFx6A96l3g;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1100
    invoke-virtual {v0, v2}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$jC_qmUGicOM_RMv-R_auY6HHzCw;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$jC_qmUGicOM_RMv-R_auY6HHzCw;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1101
    invoke-virtual {v0, v2}, Lio/reactivex/Single;->doAfterTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$YxoYG7g__3wuO8JTsKObOLwS19Y;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$YxoYG7g__3wuO8JTsKObOLwS19Y;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1102
    invoke-virtual {v0, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 1098
    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method


# virtual methods
.method addPayment()V
    .locals 3

    .line 800
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_PARTIALLY_PAY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PAYMENT_FLOW_USE_PAYMENT_CONFIG:Lcom/squareup/settings/server/Features$Feature;

    .line 801
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 802
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$y7pecX4e6qTW0TtML4m9YvbFQXc;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$y7pecX4e6qTW0TtML4m9YvbFQXc;

    .line 803
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$f6wFbWlXeys8OMzgk2fe6aGjByk;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$f6wFbWlXeys8OMzgk2fe6aGjByk;

    .line 805
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 807
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$zKTrm8Jcu-JuBFx5LwueMXQW3h4;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$zKTrm8Jcu-JuBFx5LwueMXQW3h4;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 808
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 802
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0

    .line 811
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/invoices/ui/AddPaymentScreen;->INSTANCE:Lcom/squareup/invoices/ui/AddPaymentScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public addPaymentScreenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 366
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    .line 367
    invoke-virtual {v0, v1}, Lrx/Observable;->cast(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$Jpj2-NA97gwELuP-fapLTjqAS4c;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$Jpj2-NA97gwELuP-fapLTjqAS4c;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 368
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public cancelInvoice(ZZ)V
    .locals 3

    .line 512
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_CANCEL_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 513
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    .line 514
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->cancel(Lcom/squareup/protos/client/IdPair;Z)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$eJuNicMBr_EcV7QCYbgluCfZ5WQ;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$eJuNicMBr_EcV7QCYbgluCfZ5WQ;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 516
    invoke-virtual {p1, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$5fq688ATqZ1k6boISYBNCryyYY8;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$5fq688ATqZ1k6boISYBNCryyYY8;

    .line 522
    invoke-static {v1, v2}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {p1, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$qF8jzHv8Qo2kVHXlf8ZEuTqcif0;

    invoke-direct {v1, p0, p2}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$qF8jzHv8Qo2kVHXlf8ZEuTqcif0;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Z)V

    .line 523
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 514
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 513
    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public cancelInvoiceScreenData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;",
            ">;"
        }
    .end annotation

    .line 504
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cancelInvoiceScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public chargeInvoice()V
    .locals 4

    .line 859
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/invoices/analytics/InvoiceTakePaymentEvent;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/squareup/invoices/analytics/InvoiceTakePaymentEvent;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 860
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/invoices/ui/AddPaymentScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 863
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    .line 864
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v1, v1, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    .line 866
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/InvoiceContact;->contact_token:Ljava/lang/String;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 867
    invoke-direct {p0, v1, v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->startPayment(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    return-void

    .line 871
    :cond_0
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getContact()Lio/reactivex/Single;

    move-result-object v2

    new-instance v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ebkXakDDdExPVwBBqxHg2sGWpKQ;

    invoke-direct {v3, p0, v0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ebkXakDDdExPVwBBqxHg2sGWpKQ;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    .line 872
    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 871
    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method clearInvoiceError()V
    .locals 2

    .line 792
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->errorGettingInvoice:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public confirmationScreenData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 1158
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method createNewInvoice()V
    .locals 2

    .line 627
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_CREATE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 628
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->editInvoiceGateway:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentListFilter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 629
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/ui/GenericListFilter;

    invoke-interface {v1}, Lcom/squareup/invoices/ui/GenericListFilter;->isRecurringListFilter()Z

    move-result v1

    .line 628
    invoke-virtual {v0, v1}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->goToNewInvoiceFromApplet(Z)V

    return-void
.end method

.method public currentDisplayDetails()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/DisplayDetails;",
            ">;"
        }
    .end annotation

    .line 380
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Lcom/squareup/util/OptionalExtensionsRx1Kt;->mapIfPresent()Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method currentListFilter()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/invoices/ui/GenericListFilter;",
            ">;"
        }
    .end annotation

    .line 406
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentListFilter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public displaySendReminderConfirmation()V
    .locals 2

    .line 731
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->INVOICES_CUSTOM_REMINDER_MESSAGE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 732
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/invoices/ui/SendReminderScreen;->INSTANCE:Lcom/squareup/invoices/ui/SendReminderScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 734
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;->INSTANCE:Lcom/squareup/invoices/ui/SendReminderConfirmationDialog;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method duplicateInvoice()V
    .locals 4

    .line 633
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->INVOICES_APPLET_DUPLICATE_INVOICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 634
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    .line 636
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v2, v0}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->clone(Lcom/squareup/protos/client/invoice/Invoice;)Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$wNmtGyWk_ihdrbcYk8qOcDeatnA;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$wNmtGyWk_ihdrbcYk8qOcDeatnA;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 637
    invoke-virtual {v0, v2}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    sget-object v3, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$e_WagdkOjz_ImXkq_CXd-SQ5jsI;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$e_WagdkOjz_ImXkq_CXd-SQ5jsI;

    .line 638
    invoke-static {v2, v3}, Lcom/squareup/receiving/StandardReceiverRetrofit1;->succeedOrFail(Lcom/squareup/receiving/StandardReceiver;Lkotlin/jvm/functions/Function1;)Lrx/Observable$Transformer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$f_jOQT_ypJno1MEsU9o23RfW1jw;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$f_jOQT_ypJno1MEsU9o23RfW1jw;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 639
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 636
    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-virtual {v1, v0}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method editCurrentInvoice()V
    .locals 2

    .line 684
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->editInvoiceGateway:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    .line 685
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->editExistingInvoiceContext(Lcom/squareup/invoices/DisplayDetails;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object v1

    .line 684
    invoke-virtual {v0, v1}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->goToEditInvoiceInApplet(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    return-void
.end method

.method editExistingInvoice(Lcom/squareup/invoices/DisplayDetails;)V
    .locals 3

    .line 660
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->isRecurring()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    .line 662
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 661
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->getRecurringSeries(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$jd6nYSs25q_g0Hz-Ff3BxK0TzoE;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$jd6nYSs25q_g0Hz-Ff3BxK0TzoE;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 663
    invoke-virtual {p1, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$Fhcow3t0ikZ3MF75kol-Xqo-6Gc;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$Fhcow3t0ikZ3MF75kol-Xqo-6Gc;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$yNPpK0jaAu1iHJZaakT3bnbxOp8;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$yNPpK0jaAu1iHJZaakT3bnbxOp8;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 664
    invoke-virtual {p1, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 661
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    goto :goto_0

    .line 671
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    .line 672
    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object p1, p1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->get(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ewz9y2BJ3QSbfXYr8zCSQvVsPk0;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ewz9y2BJ3QSbfXYr8zCSQvVsPk0;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 673
    invoke-virtual {p1, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ejeETntZCMKnMYW38uF-iR4CHNc;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ejeETntZCMKnMYW38uF-iR4CHNc;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$J1g131LmX1NqsTGQF1tWqRHbP54;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$J1g131LmX1NqsTGQF1tWqRHbP54;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 674
    invoke-virtual {p1, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 672
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 671
    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    :goto_0
    return-void
.end method

.method public getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;
    .locals 1

    .line 339
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Optional;

    invoke-virtual {v0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails;

    return-object v0
.end method

.method public getCurrentListFilter()Lcom/squareup/invoices/ui/GenericListFilter;
    .locals 1

    .line 402
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentListFilter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/GenericListFilter;

    return-object v0
.end method

.method getErrorGettingInvoice()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/receiving/FailureMessage;",
            ">;>;"
        }
    .end annotation

    .line 788
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->errorGettingInvoice:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method getFileMetadataFromToken(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;
    .locals 3

    .line 776
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 778
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 779
    iget-object v2, v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method getIsSearchingObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 458
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->isSearching:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->asObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    const-string v0, "InvoicesAppletScopeRunner"

    return-object v0
.end method

.method public getSearchTerm()Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 467
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method getTrimmedSearchTermString()Ljava/lang/String;
    .locals 1

    .line 471
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->searchTerm:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public goBackFromAddPayment()V
    .locals 2

    .line 372
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/invoices/ui/AddPaymentScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public goBackFromCancelScreen()V
    .locals 2

    .line 508
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/invoices/ui/CancelInvoiceScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public goBackFromConfirmationScreen(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 1163
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loader:Lcom/squareup/invoices/ui/InvoiceLoader;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceLoader;->refresh()V

    .line 1164
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/invoices/ui/InvoiceHistoryScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceHistoryScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 1166
    :cond_0
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    :goto_0
    return-void
.end method

.method public goBackFromInvoiceBottomDialog()V
    .locals 2

    .line 554
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method goBackFromSendReminderScreen()V
    .locals 2

    .line 763
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/invoices/ui/SendReminderScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public goBackFromTimelineScreen()V
    .locals 2

    .line 1152
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/invoices/InvoiceTimelineScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method goToCustomerFromDetail()V
    .locals 3

    .line 879
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getContact()Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$EHJx2gfqyAPeUfb8IHmmPuFgdx0;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$EHJx2gfqyAPeUfb8IHmmPuFgdx0;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public goToRecordPayment()V
    .locals 2

    .line 376
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen;->INSTANCE:Lcom/squareup/invoices/ui/recordpayment/RecordPaymentScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public handleResult(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult;)V
    .locals 3

    .line 817
    instance-of v0, p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Canceled;

    if-eqz v0, :cond_0

    .line 818
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void

    .line 822
    :cond_0
    check-cast p1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Saved;

    .line 823
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 824
    invoke-static {v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$7dLT07hQke_kR8kan1W2OJ9cpi4;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$7dLT07hQke_kR8kan1W2OJ9cpi4;

    .line 825
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    .line 827
    invoke-virtual {v1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v1

    sget-object v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$p49pHGReg8sM1duq1L_qG8MQ6uU;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$p49pHGReg8sM1duq1L_qG8MQ6uU;

    .line 828
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$vXoB0OvvWoXwXPwY3AwxDYOgIus;

    invoke-direct {v2, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$vXoB0OvvWoXwXPwY3AwxDYOgIus;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 831
    invoke-virtual {v1, v2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v1

    new-instance v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$LOq28_NUrGrj4SFq3ePusgbz0K4;

    invoke-direct {v2, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$LOq28_NUrGrj4SFq3ePusgbz0K4;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Saved;)V

    .line 848
    invoke-static {v2}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object p1

    invoke-virtual {v1, p1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 823
    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public handleResult(Lcom/squareup/invoices/workflow/edit/InvoiceAttachmentResult;)V
    .locals 3

    .line 855
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const-class v1, Lcom/squareup/container/WorkflowTreeKey;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method hasListFilter()Z
    .locals 1

    .line 398
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentListFilter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public invoiceActionBottomDialogScreenData()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ScreenData;",
            ">;"
        }
    .end annotation

    .line 390
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceActionBottomDialogScreenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public invoiceListBusy()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 419
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method isFetchingInvoice()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 454
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->fetchingOrCloning:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public isSearching()Z
    .locals 1

    .line 450
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->isSearching:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v0}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$addPayment$30$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 808
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner$BootstrapScreen;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountWorkflowRunner$BootstrapScreen;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$addPaymentScreenData$5$InvoicesAppletScopeRunner(Lcom/squareup/invoices/DisplayDetails$Invoice;)Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;
    .locals 1

    .line 368
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->addPaymentScreenDataFactory:Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData$Factory;->create(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lcom/squareup/invoices/ui/AddPaymentScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$archiveInvoice$66$InvoicesAppletScopeRunner(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1082
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$archiveInvoice$67$InvoicesAppletScopeRunner()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1083
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$archiveInvoice$70$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1085
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$mJvK6WLrG5-xNE-2rXppYnyvy-I;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$mJvK6WLrG5-xNE-2rXppYnyvy-I;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$vFVPXHI6wNaDUR5sBa0y0VAdb44;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$vFVPXHI6wNaDUR5sBa0y0VAdb44;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    .line 1091
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$cancelInvoice$10$InvoicesAppletScopeRunner(ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 3

    .line 524
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cancelInvoiceScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cancelInvoiceScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    .line 526
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cancelInvoiceScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->updateIsBusy(Z)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 528
    :cond_0
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$4akpiCQBQkHPFGZtlcJUcES9Vm4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$4akpiCQBQkHPFGZtlcJUcES9Vm4;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Z)V

    new-instance p1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$Fho5dku3rJG4pWahZ-HQM4Rv6lc;

    invoke-direct {p1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$Fho5dku3rJG4pWahZ-HQM4Rv6lc;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$cancelInvoice$7$InvoicesAppletScopeRunner()V
    .locals 3

    .line 517
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cancelInvoiceScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hasValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cancelInvoiceScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    .line 519
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cancelInvoiceScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;->updateIsBusy(Z)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$chargeInvoice$38$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 873
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$Tbjg51PO3De8E-olwCEQLj1iXfU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$Tbjg51PO3De8E-olwCEQLj1iXfU;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$EJvS1PcQX6gCDM1akPGHgHgoqo8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$EJvS1PcQX6gCDM1akPGHgHgoqo8;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$duplicateInvoice$11$InvoicesAppletScopeRunner()V
    .locals 2

    .line 637
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$duplicateInvoice$13$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 640
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 641
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$8RL3DvJN7n-mpoKZQGisSIHNMzk;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$8RL3DvJN7n-mpoKZQGisSIHNMzk;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$o1iE0I6A7Kc1mekuffYLqIUs2q4;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$o1iE0I6A7Kc1mekuffYLqIUs2q4;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$editExistingInvoice$15$InvoicesAppletScopeRunner()V
    .locals 2

    .line 663
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$editExistingInvoice$16$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/GetRecurringSeriesResponse;)V
    .locals 2

    .line 665
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 666
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->editInvoiceGateway:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    new-instance v1, Lcom/squareup/invoices/DisplayDetails$Recurring;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/GetRecurringSeriesResponse;->recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/DisplayDetails$Recurring;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    .line 667
    invoke-static {v1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->editExistingInvoiceContext(Lcom/squareup/invoices/DisplayDetails;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object p1

    .line 666
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->goToEditInvoiceInApplet(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    return-void
.end method

.method public synthetic lambda$editExistingInvoice$17$InvoicesAppletScopeRunner(Ljava/lang/Throwable;)V
    .locals 1

    .line 669
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$editExistingInvoice$18$InvoicesAppletScopeRunner()V
    .locals 2

    .line 673
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$editExistingInvoice$19$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/GetInvoiceResponse;)V
    .locals 2

    .line 675
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 676
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->editInvoiceGateway:Lcom/squareup/invoices/edit/EditInvoiceGateway;

    new-instance v1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/GetInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    .line 677
    invoke-static {v1}, Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;->editExistingInvoiceContext(Lcom/squareup/invoices/DisplayDetails;)Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;

    move-result-object p1

    .line 676
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/edit/EditInvoiceGateway;->goToEditInvoiceInApplet(Lcom/squareup/invoices/edit/contexts/EditInvoiceContext;)V

    return-void
.end method

.method public synthetic lambda$editExistingInvoice$20$InvoicesAppletScopeRunner(Ljava/lang/Throwable;)V
    .locals 1

    .line 679
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$endSeries$42$InvoicesAppletScopeRunner()V
    .locals 2

    .line 912
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$endSeries$43$InvoicesAppletScopeRunner()V
    .locals 2

    .line 913
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$endSeries$45$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 915
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$POK5Q78Wr39x6rNLdThkVYTjjx0;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$POK5Q78Wr39x6rNLdThkVYTjjx0;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$OHnR5q7rqN7BjBRxYTErJP67o0E;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$OHnR5q7rqN7BjBRxYTErJP67o0E;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$getContact$46$InvoicesAppletScopeRunner(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 932
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$getContact$47$InvoicesAppletScopeRunner()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 933
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$getInvoiceAndWrapDisplayDetails$51$InvoicesAppletScopeRunner(Ljava/lang/String;Ljava/lang/Long;)Lrx/Observable;
    .locals 0

    .line 971
    iget-object p2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {p2, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->get(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$getInvoiceAndWrapDisplayDetails$54$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/util/Optional;
    .locals 3

    .line 979
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->fetchingOrCloning:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 980
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_1

    .line 981
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/GetInvoiceResponse;

    .line 983
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentListFilter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->hasValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 984
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/GetInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    .line 985
    invoke-static {v0}, Lcom/squareup/invoices/ui/InvoiceStateFilter;->getDefaultInvoiceStateFilterForDisplayState(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/invoices/ui/InvoiceStateFilter;

    move-result-object v0

    .line 984
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setCurrentListFilter(Lcom/squareup/invoices/ui/GenericListFilter;)V

    .line 988
    :cond_0
    new-instance v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/GetInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    invoke-static {v0}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1

    .line 990
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_2

    .line 991
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_connection_error:I

    sget-object v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$f0ZzS4HAC8A4R_pE9gwElH52G10;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$f0ZzS4HAC8A4R_pE9gwElH52G10;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 995
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->errorGettingInvoice:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 998
    :cond_2
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$getSeriesAndWrapDisplayDetails$50$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/util/Optional;
    .locals 3

    .line 950
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->fetchingOrCloning:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 951
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 952
    new-instance v0, Lcom/squareup/invoices/DisplayDetails$Recurring;

    .line 953
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/invoice/GetRecurringSeriesResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/GetRecurringSeriesResponse;->recurring_invoice:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    invoke-direct {v0, p1}, Lcom/squareup/invoices/DisplayDetails$Recurring;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    .line 952
    invoke-static {v0}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1

    .line 954
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    .line 955
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->failureMessageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    sget v1, Lcom/squareup/common/invoices/R$string;->invoice_connection_error:I

    sget-object v2, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$eKfuS_JQ8wkD4p1yih0oRTm57vI;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$eKfuS_JQ8wkD4p1yih0oRTm57vI;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 959
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->errorGettingInvoice:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 962
    :cond_1
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$goToCustomerFromDetail$41$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 880
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$I9dGUJxStQ4ZkSBg6Pq3jaFesWA;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$I9dGUJxStQ4ZkSBg6Pq3jaFesWA;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    sget-object v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$JWvSqfVZ9pP6Xt6mbfppqBrRd8E;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$JWvSqfVZ9pP6Xt6mbfppqBrRd8E;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$handleDeleteDraft$56$InvoicesAppletScopeRunner()V
    .locals 2

    .line 1037
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$handleDeleteDraft$57$InvoicesAppletScopeRunner()V
    .locals 2

    .line 1038
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$handleDeleteDraft$60$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 1040
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$T9Ze1-PAE8pIBjOUztvu4gxol9k;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$T9Ze1-PAE8pIBjOUztvu4gxol9k;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$kR8SUk05CsvNbmw9qV_8_1TC90o;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$kR8SUk05CsvNbmw9qV_8_1TC90o;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$handleDeleteDraftSeries$61$InvoicesAppletScopeRunner()V
    .locals 2

    .line 1059
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$handleDeleteDraftSeries$62$InvoicesAppletScopeRunner()V
    .locals 2

    .line 1060
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$handleDeleteDraftSeries$65$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 1062
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$eSqoBZEgkiFBMJkCy5pJK4PLrjI;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$eSqoBZEgkiFBMJkCy5pJK4PLrjI;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$MG40H9d9XZUYORJB1m_gV8fjkhg;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$MG40H9d9XZUYORJB1m_gV8fjkhg;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$handleResult$34$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)Lio/reactivex/SingleSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 832
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->contact_token:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 833
    new-instance v0, Lkotlin/Pair;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 836
    :cond_0
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getContact()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$pbhmnMQ9Dw32UbBqq7QvQoMbOg0;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$pbhmnMQ9Dw32UbBqq7QvQoMbOg0;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    .line 837
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$handleResult$35$InvoicesAppletScopeRunner(Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Saved;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 849
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoicePaymentStarter:Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentAmountResult$Saved;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v0, p1, p3, p2}, Lcom/squareup/invoices/ui/payinvoice/InvoicePaymentStarter;->takePayment(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    return-void
.end method

.method public synthetic lambda$null$26$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/SendInvoiceReminderResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 746
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->onSendReminderSuccess()V

    return-void
.end method

.method public synthetic lambda$null$36$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/protos/client/rolodex/GetContactResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 874
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->startPayment(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    return-void
.end method

.method public synthetic lambda$null$37$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 p2, 0x0

    .line 875
    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->startPayment(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    return-void
.end method

.method public synthetic lambda$null$39$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/rolodex/GetContactResponse;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 881
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoicesAppletScope:Lcom/squareup/invoices/ui/InvoicesAppletScope;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v3, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->transactionDiscountAdapter:Lcom/squareup/payment/TransactionDiscountAdapter;

    .line 882
    invoke-static {v1, p1, v2, v3}, Lcom/squareup/ui/crm/flow/CrmScope;->forInvoiceCustomerInDetail(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/payment/Transaction;Lcom/squareup/checkout/HoldsCoupons;)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p1

    .line 881
    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$44$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/EndRecurringSeriesResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 915
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->onEndSeriesSuccess()V

    return-void
.end method

.method public synthetic lambda$null$58$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/DeleteDraftResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1042
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    .line 1043
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createDeleteDraftSuccess()Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object v0

    .line 1042
    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 1045
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$59$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1047
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    .line 1048
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createDeleteDraftFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object p1

    .line 1047
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 1050
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$63$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1064
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    .line 1065
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createDeleteDraftSeriesSuccess()Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object v0

    .line 1064
    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 1067
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$64$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1069
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    .line 1070
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createDeleteDraftSeriesFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object p1

    .line 1069
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 1073
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$68$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/ArchiveInvoiceResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1087
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    .line 1088
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createArchiveInvoiceSuccess()Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object v0

    .line 1087
    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$69$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1089
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    .line 1090
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createArchiveInvoiceFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object p1

    .line 1089
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$73$InvoicesAppletScopeRunner(Lcom/squareup/protos/client/invoice/UnarchiveInvoiceResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1105
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    .line 1106
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createUnarchiveInvoiceSuccess()Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object v0

    .line 1105
    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$74$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1107
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->confirmationScreenDataFactory:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;

    .line 1108
    invoke-virtual {v1, p1}, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData$Factory;->createUnarchiveInvoiceFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen$ScreenData;

    move-result-object p1

    .line 1107
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$9$InvoicesAppletScopeRunner(ZLcom/squareup/protos/client/invoice/CancelInvoiceResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 529
    invoke-direct {p0, p2, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->onCancelInvoiceSuccess(Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;Z)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$InvoicesAppletScopeRunner(Lcom/squareup/invoices/ui/DeepLinkInvoiceInfo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 270
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->displayDetailsInput:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 272
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/DeepLinkInvoiceInfo;->getStrategy()Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;->DELAY:Lcom/squareup/invoices/ui/InvoiceLoadingStrategy;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 273
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/DeepLinkInvoiceInfo;->getInvoiceToken()Ljava/lang/String;

    move-result-object p1

    .line 274
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->displayDetailsInput:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 275
    invoke-static {p1, v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->createSingleInvoiceInput(Ljava/lang/String;Z)Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    .line 274
    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$InvoicesAppletScopeRunner(Lcom/squareup/util/Optional;)Lrx/Observable;
    .locals 2

    .line 282
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 283
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->fetchingOrCloning:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 286
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;

    .line 287
    iget-boolean v0, p1, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->isRecurring:Z

    if-eqz v0, :cond_1

    .line 288
    iget-object p1, p1, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->id:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getSeriesAndWrapDisplayDetails(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 290
    :cond_1
    iget-object v0, p1, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->id:Ljava/lang/String;

    iget-boolean p1, p1, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->delayGet:Z

    invoke-direct {p0, v0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getInvoiceAndWrapDisplayDetails(Ljava/lang/String;Z)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$2$InvoicesAppletScopeRunner(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 296
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loader:Lcom/squareup/invoices/ui/InvoiceLoader;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceLoader;->refresh()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$4$InvoicesAppletScopeRunner(Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 307
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    invoke-interface {p1}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->startInvoiceTutorialIfPossible()V

    return-void
.end method

.method public synthetic lambda$sendReminder$24$InvoicesAppletScopeRunner()V
    .locals 2

    .line 743
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$sendReminder$25$InvoicesAppletScopeRunner()V
    .locals 2

    .line 744
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$sendReminder$27$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 746
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$BXQIsi5Ye-BA9z4DrDZ-v-raQSU;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$BXQIsi5Ye-BA9z4DrDZ-v-raQSU;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$IGmsAKJKL58tAqh08wBriphRNDY;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$IGmsAKJKL58tAqh08wBriphRNDY;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$showIssueRefund$21$InvoicesAppletScopeRunner(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 712
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$showIssueRefund$22$InvoicesAppletScopeRunner(Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 713
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object p2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$showIssueRefund$23$InvoicesAppletScopeRunner(Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 715
    instance-of v0, p1, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Loaded;

    if-eqz v0, :cond_0

    .line 716
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceIssueRefundStarter:Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;

    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceIssueRefundStarter;->showIssueRefundScreen()V

    goto :goto_0

    .line 718
    :cond_0
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;

    .line 720
    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    .line 721
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;->getFailureTitle()Ljava/lang/String;

    move-result-object v1

    .line 722
    invoke-virtual {p1}, Lcom/squareup/invoices/ui/InvoiceBillHistoryLoadedState$Failed;->getFailureDescription()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    const/4 v2, 0x0

    const-string v3, "Bill Loaded error"

    invoke-direct {v1, v0, v2, v3}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;ZLjava/lang/String;)V

    invoke-virtual {p1, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$timelineScreenData$77$InvoicesAppletScopeRunner(Lcom/squareup/util/Optional;)Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;
    .locals 4

    .line 1142
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object p1

    .line 1143
    new-instance v0, Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/features/invoices/R$string;->invoice_timeline_title:I

    .line 1144
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceDetailTimelineDataFactory:Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;

    const/4 v3, 0x0

    .line 1145
    invoke-virtual {v2, p1, v3}, Lcom/squareup/invoices/ui/InvoiceDetailTimelineDataFactory;->create(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;Z)Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;

    move-result-object p1

    invoke-direct {v0, v1, p1, v3}, Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;-><init>(Ljava/lang/String;Lcom/squareup/features/invoices/widgets/SectionElement$TimelineData;Z)V

    return-object v0
.end method

.method public synthetic lambda$unarchiveInvoice$71$InvoicesAppletScopeRunner(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1100
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$unarchiveInvoice$72$InvoicesAppletScopeRunner()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1101
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceListBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$unarchiveInvoice$75$InvoicesAppletScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1103
    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$g9-oc3CN2caWQqGsn7EEcMyiATA;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$g9-oc3CN2caWQqGsn7EEcMyiATA;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$FOvInz-94yom3kyB3ZXRl3CtS0w;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$FOvInz-94yom3kyB3ZXRl3CtS0w;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    .line 1109
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoicesAppletConfirmationScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method loadingOverInvoiceDetail()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 796
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loadingOverInvoiceDetail:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method maybeShowIssueRefundForPartiallyPaidInvoice(Lcom/squareup/invoices/InvoiceDisplayState;)V
    .locals 1

    .line 486
    sget-object v0, Lcom/squareup/invoices/InvoiceDisplayState;->CANCELED:Lcom/squareup/invoices/InvoiceDisplayState;

    if-ne p1, v0, :cond_0

    .line 487
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->showIssueRefund()V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    .line 489
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->showCancelInvoiceScreen(Z)V

    :goto_0
    return-void
.end method

.method onAttachmentClicked(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)V
    .locals 10

    .line 891
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    .line 893
    new-instance v1, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$BootstrapScreen;

    new-instance v9, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;

    new-instance v3, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Invoice;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    invoke-direct {v3, v0}, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Invoice;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->token:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceFileHelper:Lcom/squareup/invoices/image/InvoiceFileHelper;

    .line 900
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/image/InvoiceFileHelper;->uploadedAtAsStringOrEmpty(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->mime_type:Ljava/lang/String;

    move-object v2, v9

    invoke-direct/range {v2 .. v8}, Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;-><init>(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v9}, Lcom/squareup/invoices/workflow/edit/fileattachments/InvoiceFileAttachmentWorkflowRunner$BootstrapScreen;-><init>(Lcom/squareup/invoices/workflow/edit/StartAttachmentInfo$ReadOnly;)V

    .line 904
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {p1, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 258
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoicesApplet:Lcom/squareup/invoicesappletapi/InvoicesApplet;

    invoke-virtual {v0}, Lcom/squareup/invoicesappletapi/InvoicesApplet;->select()V

    .line 259
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoicesAppletScope;

    iput-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoicesAppletScope:Lcom/squareup/invoices/ui/InvoicesAppletScope;

    .line 261
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loader:Lcom/squareup/invoices/ui/InvoiceLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 263
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->maybeRefreshFromServer()V

    .line 264
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->loader:Lcom/squareup/invoices/ui/InvoiceLoader;

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/ui/InvoiceLoader;->setDefaultPageSize(Ljava/lang/Integer;)V

    .line 266
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceToDeepLink:Lcom/squareup/invoices/ui/InvoiceToDeepLink;

    .line 268
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceToDeepLink;->deepLinkInfo()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$c47SA0Wz_TOYtQF5-qmJpAjyjFM;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$c47SA0Wz_TOYtQF5-qmJpAjyjFM;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 269
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 266
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 279
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->displayDetailsInput:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 280
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$gDjkbrVTx45eUyMW7a3M5Mh6aI4;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$gDjkbrVTx45eUyMW7a3M5Mh6aI4;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 281
    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 293
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 279
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 295
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoicesAppletRunner:Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/RealInvoicesAppletRunner;->onInvoiceConfirmationCanceled()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$k205WipaQ6tCaPvB1p55foESqDc;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$k205WipaQ6tCaPvB1p55foESqDc;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 296
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 295
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 298
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->defaultInvoicesStateFilter:Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;

    .line 299
    invoke-virtual {v0}, Lcom/squareup/invoices/ui/DefaultInvoicesStateFilter;->stateFilter()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$vbMtZk2B_Q8eD43QlblOtmMqd9g;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$vbMtZk2B_Q8eD43QlblOtmMqd9g;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 298
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 301
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->setInvoicesAppletActiveState(Z)V

    .line 303
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cache:Lcom/squareup/invoicesappletapi/InvoiceUnitCache;

    .line 304
    invoke-interface {v0}, Lcom/squareup/invoicesappletapi/InvoiceUnitCache;->unitMetadataDisplayDetails()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ajg03rUtgkjfDGjlhBxYt4wMy3A;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$ajg03rUtgkjfDGjlhBxYt4wMy3A;

    .line 305
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$8PNv3TdiuoxBrrIBJpYfkL2ZVxI;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$8PNv3TdiuoxBrrIBJpYfkL2ZVxI;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 307
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 303
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onEvent(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;)V
    .locals 3

    .line 558
    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;

    if-eqz v0, :cond_0

    .line 559
    move-object v0, p1

    check-cast v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;

    .line 560
    invoke-virtual {v0}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 561
    instance-of v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    if-eqz v1, :cond_0

    .line 562
    check-cast v0, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    .line 563
    sget-object v1, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$1;->$SwitchMap$com$squareup$invoices$ui$InvoiceDetailScreen$Event:[I

    invoke-virtual {v0}, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 606
    :pswitch_0
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->unarchiveInvoice()V

    goto :goto_0

    .line 603
    :pswitch_1
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->archiveInvoice()V

    goto :goto_0

    .line 600
    :pswitch_2
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->deleteDraft()V

    goto :goto_0

    .line 597
    :pswitch_3
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->downloadInvoice()V

    goto :goto_0

    .line 594
    :pswitch_4
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->goToTimelineScreen()V

    goto :goto_0

    .line 591
    :pswitch_5
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoicesAppletScope:Lcom/squareup/invoices/ui/InvoicesAppletScope;

    invoke-direct {v1, v2}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 588
    :pswitch_6
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->endSeries()V

    goto :goto_0

    .line 583
    :pswitch_7
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    .line 584
    iget-object v1, v0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v1, v1, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->merchant_invoice_number:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setParentSeriesStateFilterAndGoBack(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 580
    :pswitch_8
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentInvoiceDisplayState()Lcom/squareup/invoices/InvoiceDisplayState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->maybeShowIssueRefundForPartiallyPaidInvoice(Lcom/squareup/invoices/InvoiceDisplayState;)V

    goto :goto_0

    :pswitch_9
    const/4 v0, 0x0

    .line 577
    invoke-virtual {p0, v0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->showCancelInvoiceScreen(Z)V

    goto :goto_0

    .line 574
    :pswitch_a
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->duplicateInvoice()V

    goto :goto_0

    .line 571
    :pswitch_b
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->showBillHistory()V

    goto :goto_0

    .line 568
    :pswitch_c
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->displaySendReminderConfirmation()V

    goto :goto_0

    .line 565
    :pswitch_d
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->shareLink()V

    .line 612
    :cond_0
    :goto_0
    instance-of v0, p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;

    if-eqz v0, :cond_1

    .line 613
    check-cast p1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;

    .line 616
    invoke-virtual {p1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 617
    instance-of v1, v0, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    if-eqz v1, :cond_1

    .line 618
    check-cast v0, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    .line 619
    sget-object v1, Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;->TIMELINE_CTA:Lcom/squareup/invoices/ui/InvoiceDetailScreen$Event;

    if-ne v0, v1, :cond_1

    .line 620
    invoke-direct {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->handleTimelineCtaClicked(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$TimelineCtaClicked;)V

    :cond_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onExitScope()V
    .locals 2

    .line 311
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceTutorialRunner:Lcom/squareup/register/tutorial/InvoiceTutorialRunner;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/register/tutorial/InvoiceTutorialRunner;->setInvoicesAppletActiveState(Z)V

    .line 312
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    const-string v0, "display_details_input"

    .line 321
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->displayDetailsInput:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->getFromBundle(Landroid/os/Bundle;)Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 3

    .line 329
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->displayDetailsInput:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Optional;

    invoke-virtual {v0}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;

    const-string v1, "display_details_input"

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 331
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    .line 333
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 334
    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->writeToBundle(Landroid/os/Bundle;)V

    :goto_0
    return-void
.end method

.method sendReminder(Ljava/lang/String;)V
    .locals 3

    .line 739
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    .line 740
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->subscriptions:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    .line 742
    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v2, v0, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->sendReminder(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$SJtDlnSZP9yIzzaK6bNz3-HGMzo;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$SJtDlnSZP9yIzzaK6bNz3-HGMzo;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 743
    invoke-virtual {p1, v0}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$9ZQPKfou7yNFsg0jE_9VcNf1H5s;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$9ZQPKfou7yNFsg0jE_9VcNf1H5s;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 744
    invoke-virtual {p1, v0}, Lrx/Observable;->doAfterTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$bv9yWErYp5Xh7cvDGOGAFAKnxl4;

    invoke-direct {v0, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$bv9yWErYp5Xh7cvDGOGAFAKnxl4;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 745
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 741
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 740
    invoke-virtual {v1, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method public setCurrentDisplayDetails(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V
    .locals 2

    .line 350
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/DisplayDetails$Invoice;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/DisplayDetails$Invoice;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;)V

    invoke-static {v1}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setCurrentDisplayDetails(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V
    .locals 2

    .line 354
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/DisplayDetails$Recurring;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/DisplayDetails$Recurring;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    invoke-static {v1}, Lcom/squareup/util/Optional;->of(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setCurrentListFilter(Lcom/squareup/invoices/ui/GenericListFilter;)V
    .locals 1

    .line 410
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentListFilter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method setCurrentStateAndGoToHistoryIfNecessary(Lcom/squareup/invoices/ui/GenericListFilter;)V
    .locals 3

    .line 423
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentListFilter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/GenericListFilter;

    .line 424
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentListFilter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    if-eqz v0, :cond_0

    .line 432
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/container/CalculatedKey;

    sget-object v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$hTUnzhowC-17-APF8AXXT2zJYIA;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$hTUnzhowC-17-APF8AXXT2zJYIA;

    const-string v2, "setCurrentStateAndGoToHistoryIfNecessary"

    invoke-direct {v0, v2, v1}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public setDisplayDetailsInput(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;)V
    .locals 1

    .line 394
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->displayDetailsInput:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method setInvoiceActionBottomDialogScreenData(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;)V"
        }
    .end annotation

    .line 384
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceActionBottomDialogScreenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ScreenData;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/ui/InvoiceActionBottomDialog$ScreenData;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setIsSearching(Z)V
    .locals 1

    .line 462
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 463
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->isSearching:Lrx/subjects/BehaviorSubject;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method setParentSeriesStateFilterAndGoBack(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 414
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentListFilter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/invoices/ui/ParentRecurringSeriesListFilter;

    invoke-direct {v1, p1, p2}, Lcom/squareup/invoices/ui/ParentRecurringSeriesListFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 415
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public shareLink()V
    .locals 4

    .line 475
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v0

    .line 476
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceShareUrlLauncher:Lcom/squareup/url/InvoiceShareUrlLauncher;

    iget-object v2, v0, Lcom/squareup/protos/client/invoice/Invoice;->id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/Invoice;->payer:Lcom/squareup/protos/client/invoice/InvoiceContact;

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceContact;->buyer_email:Ljava/lang/String;

    sget-object v3, Lcom/squareup/url/UrlType;->SINGLE_INVOICE:Lcom/squareup/url/UrlType;

    invoke-interface {v1, v2, v0, v3}, Lcom/squareup/url/InvoiceShareUrlLauncher;->shareUrl(Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/url/UrlType;)V

    return-void
.end method

.method showBillHistory()V
    .locals 4

    .line 690
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    .line 691
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getInvoice()Lcom/squareup/protos/client/invoice/Invoice;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/invoice/Invoice;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart;->amounts:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 693
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 696
    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 700
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoiceBillLoader:Lcom/squareup/invoices/ui/InvoiceBillLoader;

    invoke-virtual {v2}, Lcom/squareup/invoices/ui/InvoiceBillLoader;->clear()V

    .line 701
    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v3, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;

    invoke-direct {v3, v0, v1}, Lcom/squareup/invoices/ui/BillHistoryDetailScreen;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method showCancelInvoiceScreen(Z)V
    .locals 4

    .line 494
    invoke-virtual {p0}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->getCurrentDisplayDetails()Lcom/squareup/invoices/DisplayDetails;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/DisplayDetails$Invoice;

    .line 495
    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cancelInvoiceScreenData:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v2, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->cancelScreenDataFactory:Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;

    .line 496
    invoke-virtual {v0}, Lcom/squareup/invoices/DisplayDetails$Invoice;->getDetails()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v2, v0, p1, v3}, Lcom/squareup/invoices/ui/CancelInvoiceScreenDataFactory;->create(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;ZZ)Lcom/squareup/invoices/ui/CancelInvoiceScreen$Data;

    move-result-object p1

    .line 495
    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 499
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/invoices/ui/CancelInvoiceScreen;

    iget-object v1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->invoicesAppletScope:Lcom/squareup/invoices/ui/InvoicesAppletScope;

    invoke-direct {v0, v1}, Lcom/squareup/invoices/ui/CancelInvoiceScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method showInvoiceDetail(Ljava/lang/String;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 359
    invoke-static {p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->createSeriesInput(Ljava/lang/String;)Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;->createSingleInvoiceInput(Ljava/lang/String;Z)Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;

    move-result-object p1

    .line 360
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->setDisplayDetailsInput(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner$DisplayDetailsInput;)V

    .line 362
    iget-object p1, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->flow:Lflow/Flow;

    sget-object p2, Lcom/squareup/invoices/ui/InvoiceDetailScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceDetailScreen;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public timelineScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/invoices/InvoiceTimelineScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 1138
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;->currentDisplayDetails:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$LVM2p0ZkzMiPUtUK9p_cwZKzRUE;->INSTANCE:Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$LVM2p0ZkzMiPUtUK9p_cwZKzRUE;

    .line 1139
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$cpvaAl-nfDWSkvGD7Xq3LNCwyQI;

    invoke-direct {v1, p0}, Lcom/squareup/invoices/ui/-$$Lambda$InvoicesAppletScopeRunner$cpvaAl-nfDWSkvGD7Xq3LNCwyQI;-><init>(Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 1140
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 1138
    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
