.class public Lcom/squareup/invoices/ui/SendReminderScreen;
.super Lcom/squareup/invoices/ui/InInvoicesAppletScope;
.source "SendReminderScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/ui/SendReminderScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/invoices/ui/SendReminderScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/invoices/ui/SendReminderScreen;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/SendReminderScreen;-><init>()V

    sput-object v0, Lcom/squareup/invoices/ui/SendReminderScreen;->INSTANCE:Lcom/squareup/invoices/ui/SendReminderScreen;

    .line 28
    sget-object v0, Lcom/squareup/invoices/ui/SendReminderScreen;->INSTANCE:Lcom/squareup/invoices/ui/SendReminderScreen;

    .line 29
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/ui/SendReminderScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InInvoicesAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 24
    const-class v0, Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;

    .line 25
    invoke-interface {p1}, Lcom/squareup/invoices/ui/InvoicesAppletScope$Component;->sendReminderCoordinator()Lcom/squareup/invoices/ui/SendReminderCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 20
    sget v0, Lcom/squareup/features/invoices/R$layout;->send_reminder_view:I

    return v0
.end method
