.class public final Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;
.super Ljava/lang/Object;
.source "InvoiceHistoryView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/invoices/ui/InvoiceHistoryView;",
        ">;"
    }
.end annotation


# instance fields
.field private final badgePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final viewConfigurationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->badgePresenterProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->viewConfigurationProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/invoices/ui/InvoiceHistoryView;",
            ">;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectBadgePresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;Lcom/squareup/applet/BadgePresenter;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;Ljava/lang/Object;)V
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->presenter:Lcom/squareup/invoices/ui/InvoiceHistoryPresenter;

    return-void
.end method

.method public static injectScopeRunner(Lcom/squareup/invoices/ui/InvoiceHistoryView;Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->scopeRunner:Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    return-void
.end method

.method public static injectViewConfiguration(Lcom/squareup/invoices/ui/InvoiceHistoryView;Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView;->viewConfiguration:Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->badgePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/BadgePresenter;

    invoke-static {p1, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->injectBadgePresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;Lcom/squareup/applet/BadgePresenter;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->injectPresenter(Lcom/squareup/invoices/ui/InvoiceHistoryView;Ljava/lang/Object;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;

    invoke-static {p1, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->injectScopeRunner(Lcom/squareup/invoices/ui/InvoiceHistoryView;Lcom/squareup/invoices/ui/InvoicesAppletScopeRunner;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->viewConfigurationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;

    invoke-static {p1, v0}, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->injectViewConfiguration(Lcom/squareup/invoices/ui/InvoiceHistoryView;Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/invoices/ui/InvoiceHistoryView;

    invoke-virtual {p0, p1}, Lcom/squareup/invoices/ui/InvoiceHistoryView_MembersInjector;->injectMembers(Lcom/squareup/invoices/ui/InvoiceHistoryView;)V

    return-void
.end method
