.class public Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;
.super Lcom/squareup/invoices/ui/InInvoicesAppletScope;
.source "InvoiceFilterMasterScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/Master;
    applet = Lcom/squareup/invoicesappletapi/InvoicesApplet;
    existsWithoutDetail = false
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Component;,
        Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;

    invoke-direct {v0}, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;-><init>()V

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;

    .line 89
    sget-object v0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;->INSTANCE:Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;

    .line 90
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/invoices/ui/InvoiceFilterMasterScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/invoices/ui/InInvoicesAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 32
    sget v0, Lcom/squareup/features/invoices/R$layout;->invoices_applet_invoice_filter_master_view:I

    return v0
.end method
