.class public final Lcom/squareup/invoices/ShowCreateNewButtonConfiguration;
.super Ljava/lang/Object;
.source "InvoiceHistoryViewConfiguration.kt"

# interfaces
.implements Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/invoices/ShowCreateNewButtonConfiguration;",
        "Lcom/squareup/invoices/InvoiceHistoryViewConfiguration;",
        "()V",
        "shouldShowCreateButton",
        "",
        "getShouldShowCreateButton",
        "()Z",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final shouldShowCreateButton:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 11
    iput-boolean v0, p0, Lcom/squareup/invoices/ShowCreateNewButtonConfiguration;->shouldShowCreateButton:Z

    return-void
.end method


# virtual methods
.method public getShouldShowCreateButton()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/invoices/ShowCreateNewButtonConfiguration;->shouldShowCreateButton:Z

    return v0
.end method
