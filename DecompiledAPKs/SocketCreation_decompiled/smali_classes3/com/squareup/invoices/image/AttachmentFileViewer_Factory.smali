.class public final Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;
.super Ljava/lang/Object;
.source "AttachmentFileViewer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/invoices/image/AttachmentFileViewer;",
        ">;"
    }
.end annotation


# instance fields
.field private final fileViewerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/FileViewer;",
            ">;"
        }
    .end annotation
.end field

.field private final imageDownloaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageDownloader;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceFileHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/FileViewer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageDownloader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;->fileViewerProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;->imageDownloaderProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;->invoiceFileHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/FileViewer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/ImageDownloader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoices/image/InvoiceFileHelper;",
            ">;)",
            "Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/invoices/image/FileViewer;Lcom/squareup/invoices/image/ImageDownloader;Lcom/squareup/invoices/image/InvoiceFileHelper;)Lcom/squareup/invoices/image/AttachmentFileViewer;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/invoices/image/AttachmentFileViewer;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/invoices/image/AttachmentFileViewer;-><init>(Lcom/squareup/invoices/image/FileViewer;Lcom/squareup/invoices/image/ImageDownloader;Lcom/squareup/invoices/image/InvoiceFileHelper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/invoices/image/AttachmentFileViewer;
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;->fileViewerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/invoices/image/FileViewer;

    iget-object v1, p0, Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;->imageDownloaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/invoices/image/ImageDownloader;

    iget-object v2, p0, Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;->invoiceFileHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/invoices/image/InvoiceFileHelper;

    invoke-static {v0, v1, v2}, Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;->newInstance(Lcom/squareup/invoices/image/FileViewer;Lcom/squareup/invoices/image/ImageDownloader;Lcom/squareup/invoices/image/InvoiceFileHelper;)Lcom/squareup/invoices/image/AttachmentFileViewer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/invoices/image/AttachmentFileViewer_Factory;->get()Lcom/squareup/invoices/image/AttachmentFileViewer;

    move-result-object v0

    return-object v0
.end method
