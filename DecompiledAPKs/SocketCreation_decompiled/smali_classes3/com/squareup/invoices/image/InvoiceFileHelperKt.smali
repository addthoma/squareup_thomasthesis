.class public final Lcom/squareup/invoices/image/InvoiceFileHelperKt;
.super Ljava/lang/Object;
.source "InvoiceFileHelper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoiceFileHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoiceFileHelper.kt\ncom/squareup/invoices/image/InvoiceFileHelperKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,222:1\n1360#2:223\n1429#2,3:224\n*E\n*S KotlinDebug\n*F\n+ 1 InvoiceFileHelper.kt\ncom/squareup/invoices/image/InvoiceFileHelperKt\n*L\n221#1:223\n221#1,3:224\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0008\u0010\u0005\u001a\u00020\u0006H\u0002\u001a\n\u0010\u0007\u001a\u00020\u0008*\u00020\t\u001a\n\u0010\u0007\u001a\u00020\u0008*\u00020\n\u001a\u0014\u0010\u0007\u001a\u00020\u0008*\n\u0012\u0004\u0012\u00020\u000c\u0018\u00010\u000bH\u0002\u001a\n\u0010\r\u001a\u00020\u0001*\u00020\u000c\"\u001a\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u000e"
    }
    d2 = {
        "mimeType",
        "",
        "Ljava/io/File;",
        "getMimeType",
        "(Ljava/io/File;)Ljava/lang/String;",
        "isExternalStorageWritable",
        "",
        "getTotalSizeBytesUploaded",
        "",
        "Lcom/squareup/protos/client/estimate/Estimate;",
        "Lcom/squareup/protos/client/invoice/Invoice;",
        "",
        "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
        "toFileDisplayName",
        "invoices-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$isExternalStorageWritable()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/squareup/invoices/image/InvoiceFileHelperKt;->isExternalStorageWritable()Z

    move-result v0

    return v0
.end method

.method public static final getMimeType(Ljava/io/File;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$mimeType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v0

    .line 218
    invoke-static {p0}, Lkotlin/io/FilesKt;->getExtension(Ljava/io/File;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final getTotalSizeBytesUploaded(Lcom/squareup/protos/client/estimate/Estimate;)J
    .locals 2

    const-string v0, "$this$getTotalSizeBytesUploaded"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    iget-object p0, p0, Lcom/squareup/protos/client/estimate/Estimate;->attachment:Ljava/util/List;

    invoke-static {p0}, Lcom/squareup/invoices/image/InvoiceFileHelperKt;->getTotalSizeBytesUploaded(Ljava/util/List;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static final getTotalSizeBytesUploaded(Lcom/squareup/protos/client/invoice/Invoice;)J
    .locals 2

    const-string v0, "$this$getTotalSizeBytesUploaded"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/Invoice;->attachment:Ljava/util/List;

    invoke-static {p0}, Lcom/squareup/invoices/image/InvoiceFileHelperKt;->getTotalSizeBytesUploaded(Ljava/util/List;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static final getTotalSizeBytesUploaded(Ljava/util/List;)J
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;",
            ">;)J"
        }
    .end annotation

    if-eqz p0, :cond_1

    .line 221
    check-cast p0, Ljava/lang/Iterable;

    .line 223
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 224
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 225
    check-cast v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;

    .line 221
    iget-object v1, v1, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->size_bytes:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 226
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 221
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->sumOfLong(Ljava/lang/Iterable;)J

    move-result-wide v0

    goto :goto_1

    :cond_1
    const-wide/16 v0, 0x0

    :goto_1
    return-wide v0
.end method

.method private static final isExternalStorageWritable()Z
    .locals 2

    .line 204
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static final toFileDisplayName(Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$toFileDisplayName"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/protos/client/invoice/FileAttachmentMetadata;->extension:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
