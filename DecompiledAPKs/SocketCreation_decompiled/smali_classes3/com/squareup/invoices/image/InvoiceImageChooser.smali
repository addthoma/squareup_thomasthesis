.class public final Lcom/squareup/invoices/image/InvoiceImageChooser;
.super Ljava/lang/Object;
.source "InvoiceImageChooser.kt"

# interfaces
.implements Lcom/squareup/camerahelper/CameraHelper$Listener;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0008J\u0008\u0010\t\u001a\u00020\nH\u0016J\u0012\u0010\u000b\u001a\u00020\n2\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u000cH\u0016R\u001c\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/invoices/image/InvoiceImageChooser;",
        "Lcom/squareup/camerahelper/CameraHelper$Listener;",
        "()V",
        "imageRelay",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/invoices/image/ImageChooserResult;",
        "kotlin.jvm.PlatformType",
        "image",
        "Lio/reactivex/Single;",
        "onImageCanceled",
        "",
        "onImagePicked",
        "Landroid/net/Uri;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final imageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/invoices/image/ImageChooserResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    const-string v1, "PublishRelay.create<ImageChooserResult>()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/invoices/image/InvoiceImageChooser;->imageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method


# virtual methods
.method public final image()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/invoices/image/ImageChooserResult;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceImageChooser;->imageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "imageRelay.firstOrError()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onImageCanceled()V
    .locals 2

    .line 23
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceImageChooser;->imageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lcom/squareup/invoices/image/ImageChooserResult$Canceled;->INSTANCE:Lcom/squareup/invoices/image/ImageChooserResult$Canceled;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public onImagePicked(Landroid/net/Uri;)V
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/squareup/invoices/image/InvoiceImageChooser;->imageRelay:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/invoices/image/ImageChooserResult$Picked;

    invoke-direct {v1, p1}, Lcom/squareup/invoices/image/ImageChooserResult$Picked;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
