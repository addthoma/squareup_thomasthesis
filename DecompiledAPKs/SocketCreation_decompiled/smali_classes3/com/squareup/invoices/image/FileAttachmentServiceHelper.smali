.class public final Lcom/squareup/invoices/image/FileAttachmentServiceHelper;
.super Ljava/lang/Object;
.source "FileAttachmentServiceHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\"\u0010\u0007\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\n0\t0\u00082\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eJ.\u0010\u000f\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00100\t0\u00082\u0006\u0010\u000b\u001a\u00020\u000c2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/invoices/image/FileAttachmentServiceHelper;",
        "",
        "invoiceService",
        "Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;",
        "estimateService",
        "Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;",
        "(Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;)V",
        "downloadFile",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lokhttp3/ResponseBody;",
        "token",
        "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
        "attachmentToken",
        "",
        "uploadFile",
        "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
        "file",
        "Ljava/io/File;",
        "mediaType",
        "Lokhttp3/MediaType;",
        "invoices-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final estimateService:Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;

.field private final invoiceService:Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;


# direct methods
.method public constructor <init>(Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "invoiceService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "estimateService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/invoices/image/FileAttachmentServiceHelper;->invoiceService:Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;

    iput-object p2, p0, Lcom/squareup/invoices/image/FileAttachmentServiceHelper;->estimateService:Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;

    return-void
.end method


# virtual methods
.method public final downloadFile(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lokhttp3/ResponseBody;",
            ">;>;"
        }
    .end annotation

    const-string/jumbo v0, "token"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attachmentToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Estimate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/image/FileAttachmentServiceHelper;->estimateService:Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;->getToken()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;->downloadFile(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 37
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Invoice;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/image/FileAttachmentServiceHelper;->invoiceService:Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;->getToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;->downloadFile(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final uploadFile(Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;Ljava/io/File;Lokhttp3/MediaType;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;",
            "Ljava/io/File;",
            "Lokhttp3/MediaType;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/invoices/InvoiceFileUploadResponse;",
            ">;>;"
        }
    .end annotation

    const-string/jumbo v0, "token"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Estimate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/invoices/image/FileAttachmentServiceHelper;->estimateService:Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;->getToken()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/invoices/image/EstimateFileAttachmentServiceHelper;->uploadFile(Ljava/lang/String;Ljava/io/File;Lokhttp3/MediaType;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    .line 27
    :cond_0
    instance-of v0, p1, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType$Invoice;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/invoices/image/FileAttachmentServiceHelper;->invoiceService:Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;

    invoke-virtual {p1}, Lcom/squareup/invoices/workflow/edit/InvoiceTokenType;->getToken()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/invoices/image/InvoiceFileAttachmentServiceHelper;->uploadFile(Ljava/lang/String;Ljava/io/File;Lokhttp3/MediaType;)Lio/reactivex/Single;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
