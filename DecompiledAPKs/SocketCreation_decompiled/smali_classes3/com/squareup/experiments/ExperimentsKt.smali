.class public final Lcom/squareup/experiments/ExperimentsKt;
.super Ljava/lang/Object;
.source "Experiments.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\u001a \u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00012\u0006\u0010\u0005\u001a\u00020\u0001H\u0000\u001a\u0010\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007*\u00020\u0001\u00a8\u0006\t"
    }
    d2 = {
        "chooseExperiment",
        "Lcom/squareup/experiments/ExperimentProfile;",
        "server",
        "Lcom/squareup/http/Server;",
        "productionExperiment",
        "stagingExperiment",
        "inTestBucket",
        "Lio/reactivex/Observable;",
        "",
        "experiments_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final chooseExperiment(Lcom/squareup/http/Server;Lcom/squareup/experiments/ExperimentProfile;Lcom/squareup/experiments/ExperimentProfile;)Lcom/squareup/experiments/ExperimentProfile;
    .locals 1

    const-string v0, "server"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "productionExperiment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stagingExperiment"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-virtual {p0}, Lcom/squareup/http/Server;->getUrl()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/http/Endpoints;->getServerFromUrl(Ljava/lang/String;)Lcom/squareup/http/Endpoints$Server;

    move-result-object p0

    sget-object v0, Lcom/squareup/http/Endpoints$Server;->PRODUCTION:Lcom/squareup/http/Endpoints$Server;

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    move-object p1, p2

    :goto_0
    return-object p1
.end method

.method public static final inTestBucket(Lcom/squareup/experiments/ExperimentProfile;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/experiments/ExperimentProfile;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$inTestBucket"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lcom/squareup/experiments/ExperimentProfile;->bucket()Lio/reactivex/Observable;

    move-result-object v0

    .line 19
    new-instance v1, Lcom/squareup/experiments/ExperimentsKt$inTestBucket$1;

    invoke-direct {v1, p0}, Lcom/squareup/experiments/ExperimentsKt$inTestBucket$1;-><init>(Lcom/squareup/experiments/ExperimentProfile;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "bucket()\n    .map {\n    \u2026controlBucketName()\n    }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
