.class public Lcom/squareup/experiments/ExperimentProfile;
.super Ljava/lang/Object;
.source "ExperimentProfile.java"


# instance fields
.field private final controlBucketName:Ljava/lang/String;

.field private final defaultConfiguration:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

.field private final storage:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/experiments/ExperimentStorage;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ldagger/Lazy;Ljava/lang/String;Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/experiments/ExperimentStorage;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;",
            ")V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/experiments/ExperimentProfile;->storage:Ldagger/Lazy;

    .line 25
    iput-object p2, p0, Lcom/squareup/experiments/ExperimentProfile;->controlBucketName:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/squareup/experiments/ExperimentProfile;->defaultConfiguration:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    return-void
.end method


# virtual methods
.method bucket()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/server/ExperimentsResponse$Bucket;",
            ">;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/experiments/ExperimentProfile;->storage:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/experiments/ExperimentStorage;

    invoke-interface {v0, p0}, Lcom/squareup/experiments/ExperimentStorage;->assignedBucket(Lcom/squareup/experiments/ExperimentProfile;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method final controlBucketName()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/experiments/ExperimentProfile;->controlBucketName:Ljava/lang/String;

    return-object v0
.end method

.method final defaultConfiguration()Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/experiments/ExperimentProfile;->defaultConfiguration:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    return-object v0
.end method

.method public final name()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/experiments/ExperimentProfile;->defaultConfiguration:Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;

    iget-object v0, v0, Lcom/squareup/server/ExperimentsResponse$ExperimentConfig;->name:Ljava/lang/String;

    return-object v0
.end method
