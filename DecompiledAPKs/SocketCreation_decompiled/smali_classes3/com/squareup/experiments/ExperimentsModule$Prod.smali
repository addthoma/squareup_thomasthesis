.class public abstract Lcom/squareup/experiments/ExperimentsModule$Prod;
.super Ljava/lang/Object;
.source "ExperimentsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/experiments/ExperimentsModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Prod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideExperiments(Lcom/squareup/experiments/ServerExperiments;)Lcom/squareup/experiments/ExperimentStorage;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
