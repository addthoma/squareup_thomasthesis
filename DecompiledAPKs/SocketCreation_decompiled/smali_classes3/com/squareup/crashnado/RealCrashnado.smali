.class public Lcom/squareup/crashnado/RealCrashnado;
.super Ljava/lang/Object;
.source "RealCrashnado.java"

# interfaces
.implements Lcom/squareup/crashnado/Crashnado;


# instance fields
.field private final context:Landroid/content/Context;

.field private final crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;

.field private installed:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/crashnado/CrashnadoReporter;)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/crashnado/RealCrashnado;->context:Landroid/content/Context;

    .line 16
    iput-object p2, p0, Lcom/squareup/crashnado/RealCrashnado;->crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/crashnado/RealCrashnado;)Lcom/squareup/crashnado/CrashnadoReporter;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/squareup/crashnado/RealCrashnado;->crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/crashnado/RealCrashnado;)Landroid/content/Context;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/squareup/crashnado/RealCrashnado;->context:Landroid/content/Context;

    return-object p0
.end method

.method private doInstall(Lcom/squareup/crashnado/Crashnado$CrashnadoListener;)V
    .locals 5

    .line 46
    iget-object v0, p0, Lcom/squareup/crashnado/RealCrashnado;->crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;

    const-string v1, "Starting Crashnado install."

    invoke-interface {v0, v1}, Lcom/squareup/crashnado/CrashnadoReporter;->logCrashnadoState(Ljava/lang/String;)V

    .line 48
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 49
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v3, Lcom/squareup/crashnado/RealCrashnado$1;

    invoke-direct {v3, p0, v0}, Lcom/squareup/crashnado/RealCrashnado$1;-><init>(Lcom/squareup/crashnado/RealCrashnado;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-wide/16 v2, 0x1e

    .line 59
    :try_start_0
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iput-boolean v1, p0, Lcom/squareup/crashnado/RealCrashnado;->installed:Z

    .line 61
    invoke-virtual {p0}, Lcom/squareup/crashnado/RealCrashnado;->prepareStack()V

    .line 62
    invoke-interface {p1}, Lcom/squareup/crashnado/Crashnado$CrashnadoListener;->onCrashnadoInstalled()V

    goto :goto_0

    .line 64
    :cond_0
    iget-object p1, p0, Lcom/squareup/crashnado/RealCrashnado;->crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Timed out waiting for Crashnado to be installed."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/crashnado/CrashnadoReporter;->failedToInstall(Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 68
    iget-object v0, p0, Lcom/squareup/crashnado/RealCrashnado;->crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;

    invoke-interface {v0, p1}, Lcom/squareup/crashnado/CrashnadoReporter;->failedToInstall(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public install(Lcom/squareup/crashnado/Crashnado$CrashnadoListener;)V
    .locals 2

    .line 20
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 24
    monitor-enter p0

    .line 25
    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/crashnado/RealCrashnado;->installed:Z

    if-eqz v0, :cond_0

    .line 26
    iget-object p1, p0, Lcom/squareup/crashnado/RealCrashnado;->crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;

    const-string v0, "Skipping, Crashnado already installed."

    invoke-interface {p1, v0}, Lcom/squareup/crashnado/CrashnadoReporter;->logCrashnadoState(Ljava/lang/String;)V

    .line 27
    monitor-exit p0

    return-void

    .line 30
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/crashnado/RealCrashnado;->doInstall(Lcom/squareup/crashnado/Crashnado$CrashnadoListener;)V

    .line 31
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    .line 21
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Must not install Crashnado from the main thread."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public declared-synchronized prepareStack()V
    .locals 4

    monitor-enter p0

    .line 35
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/squareup/crashnado/RealCrashnado;->crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Preparing crashnado on: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/crashnado/CrashnadoReporter;->logCrashnadoState(Ljava/lang/String;)V

    .line 38
    iget-boolean v1, p0, Lcom/squareup/crashnado/RealCrashnado;->installed:Z

    if-eqz v1, :cond_0

    .line 39
    invoke-static {}, Lcom/squareup/crashnado/CrashnadoNative;->prepareStack()V

    goto :goto_0

    .line 41
    :cond_0
    iget-object v1, p0, Lcom/squareup/crashnado/RealCrashnado;->crashReporter:Lcom/squareup/crashnado/CrashnadoReporter;

    invoke-interface {v1, v0}, Lcom/squareup/crashnado/CrashnadoReporter;->preparingIllegalStack(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
