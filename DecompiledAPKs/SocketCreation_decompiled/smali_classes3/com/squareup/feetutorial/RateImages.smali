.class Lcom/squareup/feetutorial/RateImages;
.super Ljava/lang/Object;
.source "RateImages.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cnpImageFor(Lcom/squareup/protos/common/CurrencyCode;)I
    .locals 3

    .line 67
    sget-object v0, Lcom/squareup/feetutorial/RateImages$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 75
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rate_cnp_gbp:I

    return p0

    .line 79
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No image available for currency "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_1
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rate_cnp_usd:I

    return p0

    .line 71
    :cond_2
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rate_cnp_cad:I

    return p0

    .line 73
    :cond_3
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rate_cnp_jpy:I

    return p0

    .line 69
    :cond_4
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rate_cnp_aud:I

    return p0
.end method

.method private static dipImageFor(Lcom/squareup/protos/common/CurrencyCode;)I
    .locals 3

    .line 28
    sget-object v0, Lcom/squareup/feetutorial/RateImages$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 38
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No image available for currency: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_1
    :goto_0
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rate_dip:I

    return p0

    .line 33
    :cond_2
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rate_dip_cad:I

    return p0

    .line 31
    :cond_3
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rotated_chip_reader_and_card:I

    return p0
.end method

.method static imageFor(Lcom/squareup/feetutorial/RateCategory;Lcom/squareup/protos/common/CurrencyCode;)I
    .locals 2

    .line 9
    sget-object v0, Lcom/squareup/feetutorial/RateImages$1;->$SwitchMap$com$squareup$feetutorial$RateCategory:[I

    invoke-virtual {p0}, Lcom/squareup/feetutorial/RateCategory;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 23
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected category: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 21
    :pswitch_0
    invoke-static {p1}, Lcom/squareup/feetutorial/RateImages;->swipeImageFor(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p0

    return p0

    .line 19
    :pswitch_1
    invoke-static {p1}, Lcom/squareup/feetutorial/RateImages;->cnpImageFor(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p0

    return p0

    .line 17
    :pswitch_2
    invoke-static {p1}, Lcom/squareup/feetutorial/RateImages;->tapImageFor(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p0

    return p0

    .line 14
    :pswitch_3
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rotated_chip_reader_and_jcb:I

    return p0

    .line 12
    :pswitch_4
    invoke-static {p1}, Lcom/squareup/feetutorial/RateImages;->dipImageFor(Lcom/squareup/protos/common/CurrencyCode;)I

    move-result p0

    return p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static swipeImageFor(Lcom/squareup/protos/common/CurrencyCode;)I
    .locals 3

    .line 54
    sget-object v0, Lcom/squareup/feetutorial/RateImages$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 60
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rate_swipe_gbp:I

    return p0

    .line 62
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No image available for currency "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_1
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rate_swipe_dollar:I

    return p0

    .line 56
    :cond_2
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rate_swipe_cad:I

    return p0
.end method

.method private static tapImageFor(Lcom/squareup/protos/common/CurrencyCode;)I
    .locals 3

    .line 43
    sget-object v0, Lcom/squareup/feetutorial/RateImages$1;->$SwitchMap$com$squareup$protos$common$CurrencyCode:[I

    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 47
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rate_tap_interac:I

    return p0

    .line 49
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No image available for currency "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_1
    sget p0, Lcom/squareup/feetutorial/impl/R$drawable;->rate_tap_visa:I

    return p0
.end method
