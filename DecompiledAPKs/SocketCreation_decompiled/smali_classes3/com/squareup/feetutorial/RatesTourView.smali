.class public Lcom/squareup/feetutorial/RatesTourView;
.super Landroid/widget/RelativeLayout;
.source "RatesTourView.java"


# instance fields
.field private final adapter:Lcom/squareup/feetutorial/RateTourAdapter;

.field private closeButton:Lcom/squareup/glyph/SquareGlyphView;

.field currency:Lcom/squareup/protos/common/CurrencyCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private helpButton:Landroid/widget/ImageView;

.field private pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

.field private pager:Landroidx/viewpager/widget/ViewPager;

.field picasso:Lcom/squareup/picasso/Picasso;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/feetutorial/RatesTourScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field rateFormatter:Lcom/squareup/text/RateFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field shorterMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const-class p2, Lcom/squareup/feetutorial/RatesTourScreenComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/feetutorial/RatesTourScreenComponent;

    invoke-interface {p2, p0}, Lcom/squareup/feetutorial/RatesTourScreenComponent;->inject(Lcom/squareup/feetutorial/RatesTourView;)V

    .line 48
    new-instance p2, Lcom/squareup/feetutorial/RateTourAdapter;

    iget-object v2, p0, Lcom/squareup/feetutorial/RatesTourView;->res:Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v3

    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscape()Z

    move-result v4

    iget-object v5, p0, Lcom/squareup/feetutorial/RatesTourView;->rateFormatter:Lcom/squareup/text/RateFormatter;

    iget-object v6, p0, Lcom/squareup/feetutorial/RatesTourView;->shorterMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v7, p0, Lcom/squareup/feetutorial/RatesTourView;->currency:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v8, p0, Lcom/squareup/feetutorial/RatesTourView;->picasso:Lcom/squareup/picasso/Picasso;

    move-object v0, p2

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/squareup/feetutorial/RateTourAdapter;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;ZZLcom/squareup/text/RateFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/picasso/Picasso;)V

    iput-object p2, p0, Lcom/squareup/feetutorial/RatesTourView;->adapter:Lcom/squareup/feetutorial/RateTourAdapter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/feetutorial/RatesTourView;)Lcom/squareup/feetutorial/RateTourAdapter;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/feetutorial/RatesTourView;->adapter:Lcom/squareup/feetutorial/RateTourAdapter;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 99
    sget v0, Lcom/squareup/feetutorial/impl/R$id;->tour_pager:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/viewpager/widget/ViewPager;

    iput-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->pager:Landroidx/viewpager/widget/ViewPager;

    .line 100
    sget v0, Lcom/squareup/feetutorial/impl/R$id;->tour_close_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 101
    sget v0, Lcom/squareup/feetutorial/impl/R$id;->tour_help_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->helpButton:Landroid/widget/ImageView;

    .line 102
    sget v0, Lcom/squareup/feetutorial/impl/R$id;->page_indicator:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinPageIndicator;

    iput-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onFinishInflate$0$RatesTourView(Landroid/view/View;)V
    .locals 0

    .line 58
    iget-object p1, p0, Lcom/squareup/feetutorial/RatesTourView;->presenter:Lcom/squareup/feetutorial/RatesTourScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->close()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$RatesTourView(Landroid/view/View;)V
    .locals 0

    .line 60
    iget-object p1, p0, Lcom/squareup/feetutorial/RatesTourView;->presenter:Lcom/squareup/feetutorial/RatesTourScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->help()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$2$RatesTourView(Landroid/view/View;F)V
    .locals 2

    .line 63
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 64
    sget v1, Lcom/squareup/feetutorial/impl/R$id;->rate_image:I

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v1, p2, v1

    if-ltz v1, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v1, p2, v1

    if-gtz v1, :cond_1

    int-to-float v0, v0

    mul-float v0, v0, p2

    .line 68
    iget-object p2, p0, Lcom/squareup/feetutorial/RatesTourView;->device:Lcom/squareup/util/Device;

    invoke-interface {p2}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p2

    if-eqz p2, :cond_0

    const p2, 0x3f4ccccd    # 0.8f

    mul-float v0, v0, p2

    .line 73
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    :cond_1
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->presenter:Lcom/squareup/feetutorial/RatesTourScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 89
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 53
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 54
    invoke-direct {p0}, Lcom/squareup/feetutorial/RatesTourView;->bindViews()V

    .line 56
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->pager:Landroidx/viewpager/widget/ViewPager;

    iget-object v1, p0, Lcom/squareup/feetutorial/RatesTourView;->adapter:Lcom/squareup/feetutorial/RateTourAdapter;

    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->closeButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/feetutorial/-$$Lambda$RatesTourView$WEwdEO5_E44lvv5HUpuIJU_a6nc;

    invoke-direct {v1, p0}, Lcom/squareup/feetutorial/-$$Lambda$RatesTourView$WEwdEO5_E44lvv5HUpuIJU_a6nc;-><init>(Lcom/squareup/feetutorial/RatesTourView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->helpButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/squareup/feetutorial/-$$Lambda$RatesTourView$jD7plvgvophN4hFTSh5JyHppnYM;

    invoke-direct {v1, p0}, Lcom/squareup/feetutorial/-$$Lambda$RatesTourView$jD7plvgvophN4hFTSh5JyHppnYM;-><init>(Lcom/squareup/feetutorial/RatesTourView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->pager:Landroidx/viewpager/widget/ViewPager;

    new-instance v1, Lcom/squareup/feetutorial/-$$Lambda$RatesTourView$ri_asDRQhxYJzeOjq6hfgc9b-uc;

    invoke-direct {v1, p0}, Lcom/squareup/feetutorial/-$$Lambda$RatesTourView$ri_asDRQhxYJzeOjq6hfgc9b-uc;-><init>(Lcom/squareup/feetutorial/RatesTourView;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroidx/viewpager/widget/ViewPager;->setPageTransformer(ZLandroidx/viewpager/widget/ViewPager$PageTransformer;)V

    .line 77
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    iget-object v1, p0, Lcom/squareup/feetutorial/RatesTourView;->pager:Landroidx/viewpager/widget/ViewPager;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    new-instance v1, Lcom/squareup/feetutorial/RatesTourView$1;

    invoke-direct {v1, p0}, Lcom/squareup/feetutorial/RatesTourView$1;-><init>(Lcom/squareup/feetutorial/RatesTourView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinPageIndicator;->setOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->presenter:Lcom/squareup/feetutorial/RatesTourScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/feetutorial/RatesTourScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setItems(Ljava/util/List;Lcom/squareup/feetutorial/RateCategory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/feetutorial/RatePage;",
            ">;",
            "Lcom/squareup/feetutorial/RateCategory;",
            ")V"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->adapter:Lcom/squareup/feetutorial/RateTourAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/feetutorial/RateTourAdapter;->updatePages(Ljava/util/List;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/feetutorial/RatesTourView;->pager:Landroidx/viewpager/widget/ViewPager;

    iget-object v0, p0, Lcom/squareup/feetutorial/RatesTourView;->adapter:Lcom/squareup/feetutorial/RateTourAdapter;

    invoke-virtual {v0, p2}, Lcom/squareup/feetutorial/RateTourAdapter;->indexFromCategory(Lcom/squareup/feetutorial/RateCategory;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 95
    iget-object p1, p0, Lcom/squareup/feetutorial/RatesTourView;->pageIndicator:Lcom/squareup/marin/widgets/MarinPageIndicator;

    iget-object p2, p0, Lcom/squareup/feetutorial/RatesTourView;->adapter:Lcom/squareup/feetutorial/RateTourAdapter;

    iget-object p2, p2, Lcom/squareup/feetutorial/RateTourAdapter;->pages:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
