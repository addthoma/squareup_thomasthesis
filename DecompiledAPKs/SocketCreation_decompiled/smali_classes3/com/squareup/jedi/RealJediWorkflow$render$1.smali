.class final Lcom/squareup/jedi/RealJediWorkflow$render$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealJediWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jedi/RealJediWorkflow;->render(Lcom/squareup/jedi/JediWorkflowProps;Lcom/squareup/jedi/JediWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/jedi/JediWorkflowState;",
        "+",
        "Lcom/squareup/jedi/JediWorkflowOutput;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealJediWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealJediWorkflow.kt\ncom/squareup/jedi/RealJediWorkflow$render$1\n*L\n1#1,179:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/jedi/JediWorkflowState;",
        "Lcom/squareup/jedi/JediWorkflowOutput;",
        "jediScreenData",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/jedi/JediWorkflowState;


# direct methods
.method constructor <init>(Lcom/squareup/jedi/JediWorkflowState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/jedi/RealJediWorkflow$render$1;->$state:Lcom/squareup/jedi/JediWorkflowState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/jedi/JediHelpScreenData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/jedi/JediHelpScreenData;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/jedi/JediWorkflowState;",
            "Lcom/squareup/jedi/JediWorkflowOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "jediScreenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    instance-of v0, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    if-eqz v0, :cond_0

    .line 67
    new-instance v0, Lcom/squareup/jedi/JediWorkflowAction$EnterShowingScreen;

    new-instance v1, Lcom/squareup/jedi/JediWorkflowState$InShowingState;

    .line 68
    iget-object v2, p0, Lcom/squareup/jedi/RealJediWorkflow$render$1;->$state:Lcom/squareup/jedi/JediWorkflowState;

    invoke-virtual {v2}, Lcom/squareup/jedi/JediWorkflowState;->getScreenDataStack()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-static {v2, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 67
    invoke-direct {v1, p1}, Lcom/squareup/jedi/JediWorkflowState$InShowingState;-><init>(Ljava/util/List;)V

    check-cast v1, Lcom/squareup/jedi/JediWorkflowState;

    invoke-direct {v0, v1}, Lcom/squareup/jedi/JediWorkflowAction$EnterShowingScreen;-><init>(Lcom/squareup/jedi/JediWorkflowState;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 70
    :cond_0
    instance-of v0, p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;

    if-eqz v0, :cond_1

    .line 71
    new-instance v0, Lcom/squareup/jedi/JediWorkflowAction$EnterShowingScreen;

    new-instance v1, Lcom/squareup/jedi/JediWorkflowState$InErrorState;

    .line 72
    check-cast p1, Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;

    .line 73
    iget-object v2, p0, Lcom/squareup/jedi/RealJediWorkflow$render$1;->$state:Lcom/squareup/jedi/JediWorkflowState;

    invoke-virtual {v2}, Lcom/squareup/jedi/JediWorkflowState;->getScreenDataStack()Ljava/util/List;

    move-result-object v2

    .line 71
    invoke-direct {v1, p1, v2}, Lcom/squareup/jedi/JediWorkflowState$InErrorState;-><init>(Lcom/squareup/jedi/JediHelpScreenData$JediHelpErrorScreenData;Ljava/util/List;)V

    check-cast v1, Lcom/squareup/jedi/JediWorkflowState;

    invoke-direct {v0, v1}, Lcom/squareup/jedi/JediWorkflowAction$EnterShowingScreen;-><init>(Lcom/squareup/jedi/JediWorkflowState;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object v0

    .line 75
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected Jedi screen data type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/jedi/JediHelpScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/jedi/RealJediWorkflow$render$1;->invoke(Lcom/squareup/jedi/JediHelpScreenData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
