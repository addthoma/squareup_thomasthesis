.class final Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$2;
.super Lkotlin/jvm/internal/Lambda;
.source "JediWorkflowScreenCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->onScreen(Landroid/view/View;Lcom/squareup/jedi/JediWorkflowScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0004\u0008\u0003\u0010\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "invoke",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/jedi/JediWorkflowScreenCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$2;->this$0:Lcom/squareup/jedi/JediWorkflowScreenCoordinator;

    iput-object p2, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$2;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$2;->invoke(Lkotlin/Unit;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Unit;)V
    .locals 6

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$2;->this$0:Lcom/squareup/jedi/JediWorkflowScreenCoordinator;

    invoke-static {p1}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator;->access$getGlassSpinner$p(Lcom/squareup/jedi/JediWorkflowScreenCoordinator;)Lcom/squareup/register/widgets/GlassSpinner;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$2;->$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 90
    sget-object v1, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner$default(Lcom/squareup/register/widgets/GlassSpinnerState$Factory;ZIILjava/lang/Object;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object v1

    .line 89
    invoke-virtual {p1, v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->setSpinnerState(Landroid/content/Context;Lcom/squareup/register/widgets/GlassSpinnerState;)V

    .line 92
    iget-object p1, p0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$2;->$view:Landroid/view/View;

    new-instance v0, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$2$1;

    invoke-direct {v0, p0}, Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$2$1;-><init>(Lcom/squareup/jedi/JediWorkflowScreenCoordinator$onScreen$2;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
