.class public final Lcom/squareup/jedi/RealJediComponentInputHandler;
.super Ljava/lang/Object;
.source "RealJediComponentInputHandler.kt"

# interfaces
.implements Lcom/squareup/jedi/JediComponentInputHandler;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealJediComponentInputHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealJediComponentInputHandler.kt\ncom/squareup/jedi/RealJediComponentInputHandler\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,106:1\n1529#2,3:107\n67#3:110\n92#3,3:111\n*E\n*S KotlinDebug\n*F\n+ 1 RealJediComponentInputHandler.kt\ncom/squareup/jedi/RealJediComponentInputHandler\n*L\n37#1,3:107\n93#1:110\n93#1,3:111\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\n0\u000eH\u0016J\u000e\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010H\u0002J&\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u00072\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u00072\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0007H\u0016J\u0010\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0007H\u0016J\u0018\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0007H\u0016J\u0008\u0010\u001d\u001a\u00020\u0013H\u0016J \u0010\u001e\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u001f\u001a\u00020\u00072\u0006\u0010 \u001a\u00020!H\u0016J.\u0010\"\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u00072\u0006\u0010#\u001a\u00020\u00072\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u00072\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0007H\u0016J\u0018\u0010$\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u001f\u001a\u00020\u0007H\u0016R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0008\u001a\u0010\u0012\u000c\u0012\n \u000b*\u0004\u0018\u00010\n0\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/jedi/RealJediComponentInputHandler;",
        "Lcom/squareup/jedi/JediComponentInputHandler;",
        "screen",
        "Lcom/squareup/jedi/JediWorkflowScreen;",
        "(Lcom/squareup/jedi/JediWorkflowScreen;)V",
        "currentTextInputs",
        "",
        "",
        "requiredTextInputsUpdated",
        "Lio/reactivex/subjects/BehaviorSubject;",
        "",
        "kotlin.jvm.PlatformType",
        "requiredTextStatus",
        "canPressButton",
        "Lio/reactivex/Observable;",
        "getTextInputsAndClear",
        "",
        "Lcom/squareup/protos/jedi/service/Input;",
        "onButtonPressInput",
        "",
        "componentName",
        "sessionToken",
        "transitionId",
        "onCall",
        "phoneLink",
        "onLink",
        "context",
        "Landroid/content/Context;",
        "link",
        "onMessageUs",
        "onRequiredTextInput",
        "text",
        "minLength",
        "",
        "onSearchResultButtonPressInput",
        "linkPanelToken",
        "onTextInput",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTextInputs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final requiredTextInputsUpdated:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final requiredTextStatus:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final screen:Lcom/squareup/jedi/JediWorkflowScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/jedi/JediWorkflowScreen;)V
    .locals 1

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->screen:Lcom/squareup/jedi/JediWorkflowScreen;

    const/4 p1, 0x1

    .line 22
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/subjects/BehaviorSubject;->createDefault(Ljava/lang/Object;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object p1

    const-string v0, "BehaviorSubject.createDefault(true)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->requiredTextInputsUpdated:Lio/reactivex/subjects/BehaviorSubject;

    .line 25
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->currentTextInputs:Ljava/util/Map;

    .line 28
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->requiredTextStatus:Ljava/util/Map;

    return-void
.end method

.method private final getTextInputsAndClear()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/jedi/service/Input;",
            ">;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->currentTextInputs:Ljava/util/Map;

    .line 110
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 111
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 94
    new-instance v3, Lcom/squareup/protos/jedi/service/Input$Builder;

    invoke-direct {v3}, Lcom/squareup/protos/jedi/service/Input$Builder;-><init>()V

    .line 95
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/squareup/protos/jedi/service/Input$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object v3

    .line 96
    sget-object v4, Lcom/squareup/protos/jedi/service/InputKind;->TEXT:Lcom/squareup/protos/jedi/service/InputKind;

    invoke-virtual {v3, v4}, Lcom/squareup/protos/jedi/service/Input$Builder;->kind(Lcom/squareup/protos/jedi/service/InputKind;)Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object v3

    .line 97
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Lcom/squareup/protos/jedi/service/Input$Builder;->value(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object v2

    .line 98
    invoke-virtual {v2}, Lcom/squareup/protos/jedi/service/Input$Builder;->build()Lcom/squareup/protos/jedi/service/Input;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 101
    iget-object v0, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->currentTextInputs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 102
    iget-object v0, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->requiredTextStatus:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 103
    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toList(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public canPressButton()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->requiredTextInputsUpdated:Lio/reactivex/subjects/BehaviorSubject;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public onButtonPressInput(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 50
    new-instance v0, Lcom/squareup/protos/jedi/service/Input$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/jedi/service/Input$Builder;-><init>()V

    .line 51
    invoke-virtual {v0, p1}, Lcom/squareup/protos/jedi/service/Input$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object p1

    .line 52
    sget-object v0, Lcom/squareup/protos/jedi/service/InputKind;->BUTTON_PRESS:Lcom/squareup/protos/jedi/service/InputKind;

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/Input$Builder;->kind(Lcom/squareup/protos/jedi/service/InputKind;)Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object p1

    .line 53
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/jedi/service/Input$Builder;->value(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/Input$Builder;

    move-result-object p1

    .line 54
    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/Input$Builder;->build()Lcom/squareup/protos/jedi/service/Input;

    move-result-object p1

    .line 55
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    invoke-direct {p0}, Lcom/squareup/jedi/RealJediComponentInputHandler;->getTextInputsAndClear()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v5

    .line 56
    new-instance p1, Lcom/squareup/jedi/JediInput;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v1 .. v7}, Lcom/squareup/jedi/JediInput;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 60
    iget-object p2, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->screen:Lcom/squareup/jedi/JediWorkflowScreen;

    invoke-virtual {p2}, Lcom/squareup/jedi/JediWorkflowScreen;->getOnJediInput()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onCall(Ljava/lang/String;)V
    .locals 1

    const-string v0, "phoneLink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->screen:Lcom/squareup/jedi/JediWorkflowScreen;

    invoke-virtual {v0}, Lcom/squareup/jedi/JediWorkflowScreen;->getOnLinkTapped()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onLink(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "link"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->screen:Lcom/squareup/jedi/JediWorkflowScreen;

    invoke-virtual {p1}, Lcom/squareup/jedi/JediWorkflowScreen;->getOnLinkTapped()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onMessageUs()V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->screen:Lcom/squareup/jedi/JediWorkflowScreen;

    invoke-virtual {v0}, Lcom/squareup/jedi/JediWorkflowScreen;->getOnMessagingTapped()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onRequiredTextInput(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->currentTextInputs:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    iget-object v0, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->requiredTextStatus:Ljava/util/Map;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lt p2, p3, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iget-object p1, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->requiredTextInputsUpdated:Lio/reactivex/subjects/BehaviorSubject;

    iget-object p2, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->requiredTextStatus:Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    .line 107
    instance-of p3, p2, Ljava/util/Collection;

    if-eqz p3, :cond_1

    move-object p3, p2

    check-cast p3, Ljava/util/Collection;

    invoke-interface {p3}, Ljava/util/Collection;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_1

    goto :goto_1

    .line 108
    :cond_1
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    if-nez p3, :cond_2

    const/4 v2, 0x0

    .line 109
    :cond_3
    :goto_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    .line 37
    invoke-virtual {p1, p2}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onSearchResultButtonPressInput(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const-string p1, "linkPanelToken"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    new-instance p1, Lcom/squareup/jedi/JediInput;

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p1

    move-object v1, p3

    move-object v2, p4

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/jedi/JediInput;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 74
    iget-object p2, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->screen:Lcom/squareup/jedi/JediWorkflowScreen;

    invoke-virtual {p2}, Lcom/squareup/jedi/JediWorkflowScreen;->getOnJediInput()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onTextInput(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "componentName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/jedi/RealJediComponentInputHandler;->currentTextInputs:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
