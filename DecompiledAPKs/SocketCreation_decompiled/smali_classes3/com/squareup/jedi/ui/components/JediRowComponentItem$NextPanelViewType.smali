.class public final enum Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;
.super Ljava/lang/Enum;
.source "JediRowComponentItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/jedi/ui/components/JediRowComponentItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NextPanelViewType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

.field public static final enum MESSAGING:Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

.field public static final enum NONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 40
    new-instance v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    const/4 v1, 0x0

    const-string v2, "NONE"

    const-string v3, "none"

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->NONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    .line 41
    new-instance v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    const/4 v2, 0x1

    const-string v3, "MESSAGING"

    const-string v4, "messaging"

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->MESSAGING:Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    .line 39
    sget-object v3, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->NONE:Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->MESSAGING:Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->$VALUES:[Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput-object p3, p0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->value:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;
    .locals 0

    .line 39
    invoke-static {p0}, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->fromValue(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    move-result-object p0

    return-object p0
.end method

.method private static fromValue(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;
    .locals 5

    .line 50
    invoke-static {}, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->values()[Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 51
    iget-object v4, v3, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;
    .locals 1

    .line 39
    const-class v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->$VALUES:[Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    invoke-virtual {v0}, [Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/jedi/ui/components/JediRowComponentItem$NextPanelViewType;

    return-object v0
.end method
