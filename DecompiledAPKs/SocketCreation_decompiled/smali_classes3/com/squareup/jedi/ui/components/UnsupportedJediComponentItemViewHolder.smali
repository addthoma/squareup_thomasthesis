.class public Lcom/squareup/jedi/ui/components/UnsupportedJediComponentItemViewHolder;
.super Lcom/squareup/jedi/ui/JediComponentItemViewHolder;
.source "UnsupportedJediComponentItemViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
        "Lcom/squareup/jedi/ui/JediComponentItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .line 18
    sget v0, Lcom/squareup/jedi/impl/R$layout;->jedi_unsupported_component_view:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/jedi/ui/JediComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;I)V

    return-void
.end method


# virtual methods
.method public onBind(Lcom/squareup/jedi/ui/JediComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 1

    .line 23
    iget-object p2, p0, Lcom/squareup/jedi/ui/components/UnsupportedJediComponentItemViewHolder;->itemView:Landroid/view/View;

    sget p3, Lcom/squareup/jedi/impl/R$id;->unsupported_text_view:I

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketTextView;

    .line 25
    sget p3, Lcom/squareup/jedi/impl/R$string;->jedi_unexpected_rendering_error:I

    invoke-static {p2, p3}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object p3

    iget-object p1, p1, Lcom/squareup/jedi/ui/JediComponentItem;->component:Lcom/squareup/protos/jedi/service/Component;

    .line 26
    invoke-virtual {p1}, Lcom/squareup/protos/jedi/service/Component;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "component"

    invoke-virtual {p3, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 28
    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
