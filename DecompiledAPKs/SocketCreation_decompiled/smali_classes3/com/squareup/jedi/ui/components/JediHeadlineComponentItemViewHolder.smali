.class public Lcom/squareup/jedi/ui/components/JediHeadlineComponentItemViewHolder;
.super Lcom/squareup/jedi/ui/JediComponentItemViewHolder;
.source "JediHeadlineComponentItemViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/jedi/ui/JediComponentItemViewHolder<",
        "Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 1

    .line 17
    sget v0, Lcom/squareup/jedi/impl/R$layout;->jedi_headline_component_view:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/jedi/ui/JediComponentItemViewHolder;-><init>(Landroid/view/ViewGroup;I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onBind(Lcom/squareup/jedi/ui/JediComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItemViewHolder;->onBind(Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void
.end method

.method public onBind(Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V
    .locals 1

    .line 23
    iget-object p2, p0, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItemViewHolder;->itemView:Landroid/view/View;

    sget p3, Lcom/squareup/jedi/impl/R$id;->headline:I

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketTextView;

    .line 25
    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;->text()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 27
    iget-object p3, p0, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItemViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p3

    .line 29
    sget-object v0, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItemViewHolder$1;->$SwitchMap$com$squareup$jedi$ui$components$JediHeadlineComponentItem$HeadlineType:[I

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem;->type()Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/jedi/ui/components/JediHeadlineComponentItem$HeadlineType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 37
    :cond_0
    sget p1, Lcom/squareup/jedi/impl/R$style;->JediComponent_TextAppearance_Superheading:I

    invoke-virtual {p2, p3, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0

    .line 34
    :cond_1
    sget p1, Lcom/squareup/jedi/impl/R$style;->JediComponent_TextAppearance_Subheading:I

    invoke-virtual {p2, p3, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0

    .line 31
    :cond_2
    sget p1, Lcom/squareup/jedi/impl/R$style;->JediComponent_TextAppearance_Heading:I

    invoke-virtual {p2, p3, p1}, Lcom/squareup/marketfont/MarketTextView;->setTextAppearance(Landroid/content/Context;I)V

    :goto_0
    return-void
.end method
