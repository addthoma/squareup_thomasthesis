.class public abstract Lcom/squareup/jedi/JediWorkflowAction;
.super Ljava/lang/Object;
.source "JediWorkflowAction.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jedi/JediWorkflowAction$Error;,
        Lcom/squareup/jedi/JediWorkflowAction$EnterShowingScreen;,
        Lcom/squareup/jedi/JediWorkflowAction$BackButtonTapped;,
        Lcom/squareup/jedi/JediWorkflowAction$FetchNextPanel;,
        Lcom/squareup/jedi/JediWorkflowAction$LinkTapped;,
        Lcom/squareup/jedi/JediWorkflowAction$SearchJedi;,
        Lcom/squareup/jedi/JediWorkflowAction$SearchCleared;,
        Lcom/squareup/jedi/JediWorkflowAction$MessagingTapped;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/jedi/JediWorkflowState;",
        "Lcom/squareup/jedi/JediWorkflowOutput;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nJediWorkflowAction.kt\nKotlin\n*S Kotlin\n*F\n+ 1 JediWorkflowAction.kt\ncom/squareup/jedi/JediWorkflowAction\n*L\n1#1,70:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0008\u0007\u0008\t\n\u000b\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0003*\u0008\u0012\u0004\u0012\u00020\u00020\u0006H\u0016\u0082\u0001\u0008\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/jedi/JediWorkflowAction;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/jedi/JediWorkflowState;",
        "Lcom/squareup/jedi/JediWorkflowOutput;",
        "()V",
        "apply",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "BackButtonTapped",
        "EnterShowingScreen",
        "Error",
        "FetchNextPanel",
        "LinkTapped",
        "MessagingTapped",
        "SearchCleared",
        "SearchJedi",
        "Lcom/squareup/jedi/JediWorkflowAction$Error;",
        "Lcom/squareup/jedi/JediWorkflowAction$EnterShowingScreen;",
        "Lcom/squareup/jedi/JediWorkflowAction$BackButtonTapped;",
        "Lcom/squareup/jedi/JediWorkflowAction$FetchNextPanel;",
        "Lcom/squareup/jedi/JediWorkflowAction$LinkTapped;",
        "Lcom/squareup/jedi/JediWorkflowAction$SearchJedi;",
        "Lcom/squareup/jedi/JediWorkflowAction$SearchCleared;",
        "Lcom/squareup/jedi/JediWorkflowAction$MessagingTapped;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/jedi/JediWorkflowAction;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/jedi/JediWorkflowOutput;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/jedi/JediWorkflowState;",
            ">;)",
            "Lcom/squareup/jedi/JediWorkflowOutput;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    instance-of v0, p0, Lcom/squareup/jedi/JediWorkflowAction$EnterShowingScreen;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 32
    move-object v0, p0

    check-cast v0, Lcom/squareup/jedi/JediWorkflowAction$EnterShowingScreen;

    invoke-virtual {v0}, Lcom/squareup/jedi/JediWorkflowAction$EnterShowingScreen;->getNewState()Lcom/squareup/jedi/JediWorkflowState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 36
    :cond_0
    instance-of v0, p0, Lcom/squareup/jedi/JediWorkflowAction$SearchJedi;

    if-eqz v0, :cond_1

    .line 37
    new-instance v0, Lcom/squareup/jedi/JediWorkflowState$InSearchingState;

    move-object v2, p0

    check-cast v2, Lcom/squareup/jedi/JediWorkflowAction$SearchJedi;

    invoke-virtual {v2}, Lcom/squareup/jedi/JediWorkflowAction$SearchJedi;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/jedi/JediWorkflowState;

    invoke-virtual {v3}, Lcom/squareup/jedi/JediWorkflowState;->getScreenDataStack()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/squareup/jedi/JediWorkflowState$InSearchingState;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 41
    :cond_1
    instance-of v0, p0, Lcom/squareup/jedi/JediWorkflowAction$SearchCleared;

    const/4 v2, 0x1

    if-eqz v0, :cond_2

    .line 42
    move-object v0, p0

    check-cast v0, Lcom/squareup/jedi/JediWorkflowAction$SearchCleared;

    invoke-virtual {v0}, Lcom/squareup/jedi/JediWorkflowAction$SearchCleared;->getCanGoBackFromScreenData()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 43
    new-instance v0, Lcom/squareup/jedi/JediWorkflowState$InShowingState;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/jedi/JediWorkflowState;

    invoke-virtual {v3}, Lcom/squareup/jedi/JediWorkflowState;->getScreenDataStack()Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    invoke-static {v3, v2}, Lkotlin/collections/CollectionsKt;->take(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/jedi/JediWorkflowState$InShowingState;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_1

    .line 48
    :cond_2
    instance-of v0, p0, Lcom/squareup/jedi/JediWorkflowAction$BackButtonTapped;

    if-eqz v0, :cond_4

    .line 49
    move-object v0, p0

    check-cast v0, Lcom/squareup/jedi/JediWorkflowAction$BackButtonTapped;

    invoke-virtual {v0}, Lcom/squareup/jedi/JediWorkflowAction$BackButtonTapped;->getCanGoBackFromScreenData()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 50
    new-instance v0, Lcom/squareup/jedi/JediWorkflowState$InShowingState;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/jedi/JediWorkflowState;

    invoke-virtual {v3}, Lcom/squareup/jedi/JediWorkflowState;->getScreenDataStack()Ljava/util/List;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/collections/CollectionsKt;->dropLast(Ljava/util/List;I)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/squareup/jedi/JediWorkflowState$InShowingState;-><init>(Ljava/util/List;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :cond_3
    sget-object v1, Lcom/squareup/jedi/JediWorkflowOutput$GoBackFromJedi;->INSTANCE:Lcom/squareup/jedi/JediWorkflowOutput$GoBackFromJedi;

    .line 49
    :goto_0
    check-cast v1, Lcom/squareup/jedi/JediWorkflowOutput;

    goto :goto_1

    .line 57
    :cond_4
    instance-of v0, p0, Lcom/squareup/jedi/JediWorkflowAction$LinkTapped;

    if-eqz v0, :cond_5

    new-instance p1, Lcom/squareup/jedi/JediWorkflowOutput$GoToLink;

    move-object v0, p0

    check-cast v0, Lcom/squareup/jedi/JediWorkflowAction$LinkTapped;

    invoke-virtual {v0}, Lcom/squareup/jedi/JediWorkflowAction$LinkTapped;->getLink()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/jedi/JediWorkflowOutput$GoToLink;-><init>(Ljava/lang/String;)V

    move-object v1, p1

    check-cast v1, Lcom/squareup/jedi/JediWorkflowOutput;

    goto :goto_1

    .line 59
    :cond_5
    instance-of v0, p0, Lcom/squareup/jedi/JediWorkflowAction$FetchNextPanel;

    if-eqz v0, :cond_6

    .line 60
    new-instance v0, Lcom/squareup/jedi/JediWorkflowState$InLoadingState;

    move-object v2, p0

    check-cast v2, Lcom/squareup/jedi/JediWorkflowAction$FetchNextPanel;

    invoke-virtual {v2}, Lcom/squareup/jedi/JediWorkflowAction$FetchNextPanel;->getJediInput()Lcom/squareup/jedi/JediInput;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Mutator;->getState()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/jedi/JediWorkflowState;

    invoke-virtual {v3}, Lcom/squareup/jedi/JediWorkflowState;->getScreenDataStack()Ljava/util/List;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/squareup/jedi/JediWorkflowState$InLoadingState;-><init>(Lcom/squareup/jedi/JediInput;Ljava/util/List;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    goto :goto_1

    .line 64
    :cond_6
    sget-object p1, Lcom/squareup/jedi/JediWorkflowAction$MessagingTapped;->INSTANCE:Lcom/squareup/jedi/JediWorkflowAction$MessagingTapped;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_8

    sget-object p1, Lcom/squareup/jedi/JediWorkflowOutput$OpenMessaging;->INSTANCE:Lcom/squareup/jedi/JediWorkflowOutput$OpenMessaging;

    move-object v1, p1

    check-cast v1, Lcom/squareup/jedi/JediWorkflowOutput;

    :cond_7
    :goto_1
    return-object v1

    .line 66
    :cond_8
    sget-object p1, Lcom/squareup/jedi/JediWorkflowAction$Error;->INSTANCE:Lcom/squareup/jedi/JediWorkflowAction$Error;

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_9

    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Ineligible action"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 12
    invoke-virtual {p0, p1}, Lcom/squareup/jedi/JediWorkflowAction;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/jedi/JediWorkflowOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/jedi/JediWorkflowState;",
            "-",
            "Lcom/squareup/jedi/JediWorkflowOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
