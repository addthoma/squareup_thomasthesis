.class final Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealJediWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jedi/RealJediWorkflow;->legacyWorkflowScreen(Lcom/squareup/workflow/Sink;Lcom/squareup/jedi/JediWorkflowState;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0004\u0008\u0003\u0010\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "invoke",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $canGoBack:Z

.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$1;->$sink:Lcom/squareup/workflow/Sink;

    iput-boolean p2, p0, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$1;->$canGoBack:Z

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$1;->invoke(Lkotlin/Unit;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Unit;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-object p1, p0, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$1;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v0, Lcom/squareup/jedi/JediWorkflowAction$BackButtonTapped;

    iget-boolean v1, p0, Lcom/squareup/jedi/RealJediWorkflow$legacyWorkflowScreen$1;->$canGoBack:Z

    invoke-direct {v0, v1}, Lcom/squareup/jedi/JediWorkflowAction$BackButtonTapped;-><init>(Z)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
