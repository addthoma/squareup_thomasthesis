.class public final Lcom/squareup/opt/objc/Entity$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Entity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/opt/objc/Entity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/opt/objc/Entity;",
        "Lcom/squareup/opt/objc/Entity$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 85
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/opt/objc/Entity;
    .locals 3

    .line 98
    new-instance v0, Lcom/squareup/opt/objc/Entity;

    iget-object v1, p0, Lcom/squareup/opt/objc/Entity$Builder;->name:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/opt/objc/Entity;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/opt/objc/Entity$Builder;->build()Lcom/squareup/opt/objc/Entity;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/opt/objc/Entity$Builder;
    .locals 0

    .line 92
    iput-object p1, p0, Lcom/squareup/opt/objc/Entity$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
