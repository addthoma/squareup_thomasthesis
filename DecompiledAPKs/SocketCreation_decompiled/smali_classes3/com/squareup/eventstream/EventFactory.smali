.class public interface abstract Lcom/squareup/eventstream/EventFactory;
.super Ljava/lang/Object;
.source "EventFactory.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ServerEventT:",
        "Ljava/lang/Object;",
        "AppEventT:",
        "Ljava/lang/Object;",
        "StateT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0004\u0008f\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0004\u0008\u0001\u0010\u0002*\u0004\u0008\u0002\u0010\u00032\u00020\u0004J\u001d\u0010\u0005\u001a\u00028\u00002\u0006\u0010\u0006\u001a\u00028\u00012\u0006\u0010\u0007\u001a\u00020\u0008H&\u00a2\u0006\u0002\u0010\tJ\r\u0010\n\u001a\u00028\u0002H&\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/eventstream/EventFactory;",
        "ServerEventT",
        "AppEventT",
        "StateT",
        "",
        "create",
        "eventToLog",
        "timeMillis",
        "",
        "(Ljava/lang/Object;J)Ljava/lang/Object;",
        "state",
        "()Ljava/lang/Object;",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract create(Ljava/lang/Object;J)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TAppEventT;J)TServerEventT;"
        }
    .end annotation
.end method

.method public abstract state()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TStateT;"
        }
    .end annotation
.end method
