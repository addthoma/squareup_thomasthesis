.class public Lcom/squareup/eventstream/EventQueue;
.super Ljava/lang/Object;
.source "EventQueue.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/eventstream/EventQueue$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u0010\u0018\u0000  *\u0004\u0008\u0000\u0010\u00012\u00020\u0002:\u0001 B+\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0015\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\u0014\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00028\u0000H\u0002\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\u0016\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0010\u0010\u001d\u001a\u00020\u00112\u0006\u0010\u001e\u001a\u00020\u001cH\u0016J\u0008\u0010\u001f\u001a\u00020\u0011H\u0016J\u0008\u0010\u000e\u001a\u00020\u001cH\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00028\u00000\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/eventstream/EventQueue;",
        "T",
        "",
        "queueFactory",
        "Lcom/squareup/eventstream/QueueFactory;",
        "logger",
        "Lcom/squareup/eventstream/EventStreamLog;",
        "queueExecutor",
        "Ljava/util/concurrent/ExecutorService;",
        "droppedEventCounter",
        "Lcom/squareup/eventstream/DroppedEventCounter;",
        "(Lcom/squareup/eventstream/QueueFactory;Lcom/squareup/eventstream/EventStreamLog;Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/DroppedEventCounter;)V",
        "queue",
        "Lcom/squareup/tape/FileObjectQueue;",
        "size",
        "Ljava/util/concurrent/atomic/AtomicInteger;",
        "add",
        "",
        "entry",
        "(Ljava/lang/Object;)V",
        "addBlocking",
        "addFromQueueThread",
        "handleQueueFailure",
        "exception",
        "Lcom/squareup/tape/FileException;",
        "peekBlocking",
        "",
        "max",
        "",
        "remove",
        "count",
        "shutdown",
        "Companion",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/eventstream/EventQueue$Companion;

.field public static final MAX_BYTE_SIZE:I = 0x200000

.field public static final MAX_ITEM_COUNT:I = 0x1388


# instance fields
.field private final droppedEventCounter:Lcom/squareup/eventstream/DroppedEventCounter;

.field private final logger:Lcom/squareup/eventstream/EventStreamLog;

.field private volatile queue:Lcom/squareup/tape/FileObjectQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tape/FileObjectQueue<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final queueExecutor:Ljava/util/concurrent/ExecutorService;

.field private final queueFactory:Lcom/squareup/eventstream/QueueFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/eventstream/QueueFactory<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final size:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/eventstream/EventQueue$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/eventstream/EventQueue$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/eventstream/EventQueue;->Companion:Lcom/squareup/eventstream/EventQueue$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/eventstream/QueueFactory;Lcom/squareup/eventstream/EventStreamLog;Ljava/util/concurrent/ExecutorService;Lcom/squareup/eventstream/DroppedEventCounter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/eventstream/QueueFactory<",
            "TT;>;",
            "Lcom/squareup/eventstream/EventStreamLog;",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/squareup/eventstream/DroppedEventCounter;",
            ")V"
        }
    .end annotation

    const-string v0, "queueFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "logger"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "queueExecutor"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "droppedEventCounter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/eventstream/EventQueue;->queueFactory:Lcom/squareup/eventstream/QueueFactory;

    iput-object p2, p0, Lcom/squareup/eventstream/EventQueue;->logger:Lcom/squareup/eventstream/EventStreamLog;

    iput-object p3, p0, Lcom/squareup/eventstream/EventQueue;->queueExecutor:Ljava/util/concurrent/ExecutorService;

    iput-object p4, p0, Lcom/squareup/eventstream/EventQueue;->droppedEventCounter:Lcom/squareup/eventstream/DroppedEventCounter;

    .line 43
    new-instance p1, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {p1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object p1, p0, Lcom/squareup/eventstream/EventQueue;->size:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 47
    :try_start_0
    iget-object p1, p0, Lcom/squareup/eventstream/EventQueue;->size:Ljava/util/concurrent/atomic/AtomicInteger;

    iget-object p2, p0, Lcom/squareup/eventstream/EventQueue;->queueExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance p3, Lcom/squareup/eventstream/EventQueue$1;

    invoke-direct {p3, p0}, Lcom/squareup/eventstream/EventQueue$1;-><init>(Lcom/squareup/eventstream/EventQueue;)V

    check-cast p3, Ljava/util/concurrent/Callable;

    invoke-interface {p2, p3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object p2

    .line 56
    invoke-interface {p2}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object p2

    const-string p3, "queueExecutor.submit<Int\u2026ueue.size()\n      }.get()"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    .line 47
    invoke-virtual {p1, p2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 58
    new-instance p2, Ljava/lang/IllegalStateException;

    check-cast p1, Ljava/lang/Throwable;

    const-string p3, "Could not create queue"

    invoke-direct {p2, p3, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public static final synthetic access$addFromQueueThread(Lcom/squareup/eventstream/EventQueue;Ljava/lang/Object;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/eventstream/EventQueue;->addFromQueueThread(Ljava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$getDroppedEventCounter$p(Lcom/squareup/eventstream/EventQueue;)Lcom/squareup/eventstream/DroppedEventCounter;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/eventstream/EventQueue;->droppedEventCounter:Lcom/squareup/eventstream/DroppedEventCounter;

    return-object p0
.end method

.method public static final synthetic access$getLogger$p(Lcom/squareup/eventstream/EventQueue;)Lcom/squareup/eventstream/EventStreamLog;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/eventstream/EventQueue;->logger:Lcom/squareup/eventstream/EventStreamLog;

    return-object p0
.end method

.method public static final synthetic access$getQueue$p(Lcom/squareup/eventstream/EventQueue;)Lcom/squareup/tape/FileObjectQueue;
    .locals 1

    .line 28
    iget-object p0, p0, Lcom/squareup/eventstream/EventQueue;->queue:Lcom/squareup/tape/FileObjectQueue;

    if-nez p0, :cond_0

    const-string v0, "queue"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getQueueFactory$p(Lcom/squareup/eventstream/EventQueue;)Lcom/squareup/eventstream/QueueFactory;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/eventstream/EventQueue;->queueFactory:Lcom/squareup/eventstream/QueueFactory;

    return-object p0
.end method

.method public static final synthetic access$getSize$p(Lcom/squareup/eventstream/EventQueue;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/eventstream/EventQueue;->size:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object p0
.end method

.method public static final synthetic access$handleQueueFailure(Lcom/squareup/eventstream/EventQueue;Lcom/squareup/tape/FileException;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/eventstream/EventQueue;->handleQueueFailure(Lcom/squareup/tape/FileException;)V

    return-void
.end method

.method public static final synthetic access$setQueue$p(Lcom/squareup/eventstream/EventQueue;Lcom/squareup/tape/FileObjectQueue;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lcom/squareup/eventstream/EventQueue;->queue:Lcom/squareup/tape/FileObjectQueue;

    return-void
.end method

.method private final addFromQueueThread(Ljava/lang/Object;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string v0, "queue"

    .line 138
    iget-object v1, p0, Lcom/squareup/eventstream/EventQueue;->queueFactory:Lcom/squareup/eventstream/QueueFactory;

    invoke-virtual {v1}, Lcom/squareup/eventstream/QueueFactory;->queueFileLength()J

    move-result-wide v1

    const/high16 v3, 0x200000

    int-to-long v3, v3

    const/4 v5, 0x1

    cmp-long v6, v1, v3

    if-lez v6, :cond_0

    .line 139
    iget-object p1, p0, Lcom/squareup/eventstream/EventQueue;->droppedEventCounter:Lcom/squareup/eventstream/DroppedEventCounter;

    sget-object v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->MAX_BYTES:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-virtual {p1, v5, v0}, Lcom/squareup/eventstream/DroppedEventCounter;->add(ILcom/squareup/eventstream/DroppedEventCounter$DropType;)V

    return-void

    .line 142
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/eventstream/EventQueue;->size()I

    move-result v1

    const/16 v2, 0x1388

    if-lt v1, v2, :cond_1

    .line 143
    iget-object p1, p0, Lcom/squareup/eventstream/EventQueue;->droppedEventCounter:Lcom/squareup/eventstream/DroppedEventCounter;

    sget-object v0, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->MAX_ITEMS:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-virtual {p1, v5, v0}, Lcom/squareup/eventstream/DroppedEventCounter;->add(ILcom/squareup/eventstream/DroppedEventCounter$DropType;)V

    return-void

    .line 148
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/squareup/eventstream/EventQueue;->queue:Lcom/squareup/tape/FileObjectQueue;

    if-nez v1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v1, p1}, Lcom/squareup/tape/FileObjectQueue;->add(Ljava/lang/Object;)V

    .line 149
    iget-object v1, p0, Lcom/squareup/eventstream/EventQueue;->size:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I
    :try_end_0
    .catch Lcom/squareup/tape/FileException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 151
    invoke-direct {p0, v1}, Lcom/squareup/eventstream/EventQueue;->handleQueueFailure(Lcom/squareup/tape/FileException;)V

    .line 152
    iget-object v1, p0, Lcom/squareup/eventstream/EventQueue;->queue:Lcom/squareup/tape/FileObjectQueue;

    if-nez v1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v1, p1}, Lcom/squareup/tape/FileObjectQueue;->add(Ljava/lang/Object;)V

    .line 153
    iget-object p1, p0, Lcom/squareup/eventstream/EventQueue;->size:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    :goto_0
    return-void
.end method

.method private final handleQueueFailure(Lcom/squareup/tape/FileException;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .line 176
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue;->droppedEventCounter:Lcom/squareup/eventstream/DroppedEventCounter;

    iget-object v1, p0, Lcom/squareup/eventstream/EventQueue;->size:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    sget-object v2, Lcom/squareup/eventstream/DroppedEventCounter$DropType;->QUEUE_FAILURE:Lcom/squareup/eventstream/DroppedEventCounter$DropType;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/eventstream/DroppedEventCounter;->add(ILcom/squareup/eventstream/DroppedEventCounter$DropType;)V

    .line 177
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue;->logger:Lcom/squareup/eventstream/EventStreamLog;

    check-cast p1, Ljava/lang/Throwable;

    invoke-interface {v0, p1}, Lcom/squareup/eventstream/EventStreamLog;->report(Ljava/lang/Throwable;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue;->size:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 179
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue;->queue:Lcom/squareup/tape/FileObjectQueue;

    if-nez v0, :cond_0

    const-string v1, "queue"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/tape/FileObjectQueue;->close()V

    .line 180
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue;->queueFactory:Lcom/squareup/eventstream/QueueFactory;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/QueueFactory;->recreate(Ljava/lang/Throwable;)Lcom/squareup/tape/FileObjectQueue;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/eventstream/EventQueue;->queue:Lcom/squareup/tape/FileObjectQueue;

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 115
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue;->queueExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/eventstream/EventQueue$add$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/eventstream/EventQueue$add$1;-><init>(Lcom/squareup/eventstream/EventQueue;Ljava/lang/Object;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public addBlocking(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 123
    :try_start_0
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue;->queueExecutor:Ljava/util/concurrent/ExecutorService;

    .line 124
    new-instance v1, Lcom/squareup/eventstream/EventQueue$addBlocking$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/eventstream/EventQueue$addBlocking$1;-><init>(Lcom/squareup/eventstream/EventQueue;Ljava/lang/Object;)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object p1

    .line 128
    invoke-interface {p1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public peekBlocking(I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue;->queueExecutor:Ljava/util/concurrent/ExecutorService;

    .line 89
    new-instance v1, Lcom/squareup/eventstream/EventQueue$peekBlocking$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/eventstream/EventQueue$peekBlocking$1;-><init>(Lcom/squareup/eventstream/EventQueue;I)V

    check-cast v1, Ljava/util/concurrent/Callable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object p1

    .line 97
    invoke-interface {p1}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object p1

    const-string v0, "queueExecutor\n        .s\u2026        })\n        .get()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    .line 102
    new-instance v0, Ljava/lang/RuntimeException;

    check-cast p1, Ljava/lang/Throwable;

    const-string v1, "Unexpected exception"

    invoke-direct {v0, v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :catch_1
    move-exception p1

    .line 100
    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public remove(I)V
    .locals 2

    .line 158
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue;->queueExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/eventstream/EventQueue$remove$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/eventstream/EventQueue$remove$1;-><init>(Lcom/squareup/eventstream/EventQueue;I)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public shutdown()V
    .locals 4

    .line 69
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue;->queueExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/eventstream/EventQueue$shutdown$1;

    iget-object v2, p0, Lcom/squareup/eventstream/EventQueue;->queue:Lcom/squareup/tape/FileObjectQueue;

    if-nez v2, :cond_0

    const-string v3, "queue"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {v1, v2}, Lcom/squareup/eventstream/EventQueue$shutdown$1;-><init>(Lcom/squareup/tape/FileObjectQueue;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v2, Lcom/squareup/eventstream/EventQueue$sam$java_lang_Runnable$0;

    invoke-direct {v2, v1}, Lcom/squareup/eventstream/EventQueue$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 70
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue;->queueExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    return-void
.end method

.method public size()I
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/eventstream/EventQueue;->size:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    return v0
.end method
