.class public Lcom/squareup/eventstream/v2/catalog/UCatalog;
.super Ljava/lang/Object;
.source "UCatalog.java"


# instance fields
.field private u_library_name:Ljava/lang/String;

.field private u_library_version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setLibraryName(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/UCatalog;
    .locals 0

    .line 15
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/UCatalog;->u_library_name:Ljava/lang/String;

    return-object p0
.end method

.method public setLibraryVersion(Ljava/lang/String;)Lcom/squareup/eventstream/v2/catalog/UCatalog;
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/eventstream/v2/catalog/UCatalog;->u_library_version:Ljava/lang/String;

    return-object p0
.end method
