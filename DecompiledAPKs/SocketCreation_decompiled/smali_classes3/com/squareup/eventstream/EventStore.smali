.class public Lcom/squareup/eventstream/EventStore;
.super Ljava/lang/Object;
.source "EventStore.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/eventstream/EventStore$Clock;,
        Lcom/squareup/eventstream/EventStore$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0008\u0008\u0016\u0018\u0000 \u0014*\u0004\u0008\u0000\u0010\u00012\u00020\u0002:\u0002\u0013\u0014B%\u0008\u0000\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0015\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u000fJ\u0015\u0010\u0010\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u000fJ\u0008\u0010\u0011\u001a\u00020\rH\u0002J\u0006\u0010\u0012\u001a\u00020\rR\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0007\n\u0005\u0008\u0091F0\u0001\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/eventstream/EventStore;",
        "T",
        "",
        "queue",
        "Lcom/squareup/eventstream/EventQueue;",
        "scheduler",
        "Lcom/squareup/eventstream/JobBatchScheduler;",
        "clock",
        "Lcom/squareup/eventstream/EventStore$Clock;",
        "(Lcom/squareup/eventstream/EventQueue;Lcom/squareup/eventstream/JobBatchScheduler;Lcom/squareup/eventstream/EventStore$Clock;)V",
        "lastBatchScheduleUptimeMillis",
        "",
        "log",
        "",
        "item",
        "(Ljava/lang/Object;)V",
        "logBlocking",
        "scheduleBatchUpload",
        "shutdown",
        "Clock",
        "Companion",
        "eventstream-common_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final BATCH_SIZE:I = 0x3e8

.field public static final Companion:Lcom/squareup/eventstream/EventStore$Companion;

.field public static final MAX_QUEUE_AGE_MILLIS:J


# instance fields
.field private final clock:Lcom/squareup/eventstream/EventStore$Clock;

.field private lastBatchScheduleUptimeMillis:J

.field private final queue:Lcom/squareup/eventstream/EventQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/eventstream/EventQueue<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final scheduler:Lcom/squareup/eventstream/JobBatchScheduler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/eventstream/EventStore$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/eventstream/EventStore$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/eventstream/EventStore;->Companion:Lcom/squareup/eventstream/EventStore$Companion;

    .line 72
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/eventstream/EventStore;->MAX_QUEUE_AGE_MILLIS:J

    return-void
.end method

.method public constructor <init>(Lcom/squareup/eventstream/EventQueue;Lcom/squareup/eventstream/JobBatchScheduler;Lcom/squareup/eventstream/EventStore$Clock;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/eventstream/EventQueue<",
            "TT;>;",
            "Lcom/squareup/eventstream/JobBatchScheduler;",
            "Lcom/squareup/eventstream/EventStore$Clock;",
            ")V"
        }
    .end annotation

    const-string v0, "queue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/eventstream/EventStore;->queue:Lcom/squareup/eventstream/EventQueue;

    iput-object p2, p0, Lcom/squareup/eventstream/EventStore;->scheduler:Lcom/squareup/eventstream/JobBatchScheduler;

    iput-object p3, p0, Lcom/squareup/eventstream/EventStore;->clock:Lcom/squareup/eventstream/EventStore$Clock;

    return-void
.end method

.method private final scheduleBatchUpload()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/eventstream/EventStore;->clock:Lcom/squareup/eventstream/EventStore$Clock;

    invoke-interface {v0}, Lcom/squareup/eventstream/EventStore$Clock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/eventstream/EventStore;->lastBatchScheduleUptimeMillis:J

    .line 46
    iget-object v0, p0, Lcom/squareup/eventstream/EventStore;->scheduler:Lcom/squareup/eventstream/JobBatchScheduler;

    invoke-virtual {v0}, Lcom/squareup/eventstream/JobBatchScheduler;->startNow()V

    return-void
.end method


# virtual methods
.method public log(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/eventstream/EventStore;->clock:Lcom/squareup/eventstream/EventStore$Clock;

    invoke-interface {v0}, Lcom/squareup/eventstream/EventStore$Clock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/eventstream/EventStore;->lastBatchScheduleUptimeMillis:J

    sub-long/2addr v0, v2

    .line 29
    sget-wide v2, Lcom/squareup/eventstream/EventStore;->MAX_QUEUE_AGE_MILLIS:J

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/eventstream/EventStore;->queue:Lcom/squareup/eventstream/EventQueue;

    invoke-virtual {v0}, Lcom/squareup/eventstream/EventQueue;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0x3e8

    if-lt v0, v1, :cond_1

    .line 30
    :cond_0
    invoke-direct {p0}, Lcom/squareup/eventstream/EventStore;->scheduleBatchUpload()V

    .line 32
    :cond_1
    iget-object v0, p0, Lcom/squareup/eventstream/EventStore;->queue:Lcom/squareup/eventstream/EventQueue;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/EventQueue;->add(Ljava/lang/Object;)V

    return-void
.end method

.method public logBlocking(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/eventstream/EventStore;->queue:Lcom/squareup/eventstream/EventQueue;

    invoke-virtual {v0, p1}, Lcom/squareup/eventstream/EventQueue;->addBlocking(Ljava/lang/Object;)V

    .line 41
    invoke-direct {p0}, Lcom/squareup/eventstream/EventStore;->scheduleBatchUpload()V

    return-void
.end method

.method public final shutdown()V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/eventstream/EventStore;->queue:Lcom/squareup/eventstream/EventQueue;

    invoke-virtual {v0}, Lcom/squareup/eventstream/EventQueue;->shutdown()V

    .line 52
    iget-object v0, p0, Lcom/squareup/eventstream/EventStore;->scheduler:Lcom/squareup/eventstream/JobBatchScheduler;

    invoke-virtual {v0}, Lcom/squareup/eventstream/JobBatchScheduler;->shutdown()V

    return-void
.end method
