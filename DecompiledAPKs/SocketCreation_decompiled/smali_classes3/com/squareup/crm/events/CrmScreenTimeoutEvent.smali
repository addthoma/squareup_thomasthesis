.class public Lcom/squareup/crm/events/CrmScreenTimeoutEvent;
.super Lcom/squareup/analytics/event/v1/TimingEvent;
.source "CrmScreenTimeoutEvent.java"


# instance fields
.field private final current_screen:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/analytics/RegisterTimingName;->SCREEN_TIMED_OUT:Lcom/squareup/analytics/RegisterTimingName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/TimingEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;)V

    .line 12
    iput-object p1, p0, Lcom/squareup/crm/events/CrmScreenTimeoutEvent;->current_screen:Ljava/lang/String;

    return-void
.end method
