.class public final Lcom/squareup/crm/ConversationLoader_Factory;
.super Ljava/lang/Object;
.source "ConversationLoader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/crm/ConversationLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final dialogueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/crm/ConversationLoader_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/crm/ConversationLoader_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/crm/ConversationLoader_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/crm/ConversationLoader_Factory;->dialogueProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/crm/ConversationLoader_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            ">;)",
            "Lcom/squareup/crm/ConversationLoader_Factory;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/crm/ConversationLoader_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/crm/ConversationLoader_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/DialogueServiceHelper;)Lcom/squareup/crm/ConversationLoader;
    .locals 1

    .line 50
    new-instance v0, Lcom/squareup/crm/ConversationLoader;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/crm/ConversationLoader;-><init>(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/DialogueServiceHelper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/crm/ConversationLoader;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/crm/ConversationLoader_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, p0, Lcom/squareup/crm/ConversationLoader_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/Scheduler;

    iget-object v2, p0, Lcom/squareup/crm/ConversationLoader_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v3, p0, Lcom/squareup/crm/ConversationLoader_Factory;->dialogueProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/crm/DialogueServiceHelper;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/crm/ConversationLoader_Factory;->newInstance(Lcom/squareup/connectivity/ConnectivityMonitor;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/DialogueServiceHelper;)Lcom/squareup/crm/ConversationLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/crm/ConversationLoader_Factory;->get()Lcom/squareup/crm/ConversationLoader;

    move-result-object v0

    return-object v0
.end method
