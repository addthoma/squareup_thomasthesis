.class public Lcom/squareup/crm/DialogueServiceHelper;
.super Ljava/lang/Object;
.source "DialogueServiceHelper.java"


# instance fields
.field private final dialogue:Lcom/squareup/server/crm/DialogueService;


# direct methods
.method public constructor <init>(Lcom/squareup/server/crm/DialogueService;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/crm/DialogueServiceHelper;->dialogue:Lcom/squareup/server/crm/DialogueService;

    return-void
.end method

.method static synthetic lambda$createComment$1(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 41
    sget-object v0, Lcom/squareup/crm/-$$Lambda$DialogueServiceHelper$RTRLdMBRkG5jLeAlWdpXyMkPPgU;->INSTANCE:Lcom/squareup/crm/-$$Lambda$DialogueServiceHelper$RTRLdMBRkG5jLeAlWdpXyMkPPgU;

    invoke-static {p0, v0}, Lcom/squareup/receiving/StandardReceiver;->rejectIfNot(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$getConversation$3(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 67
    sget-object v0, Lcom/squareup/crm/-$$Lambda$DialogueServiceHelper$CphqLa2wGx6sRsdGGLUdPDU-ETU;->INSTANCE:Lcom/squareup/crm/-$$Lambda$DialogueServiceHelper$CphqLa2wGx6sRsdGGLUdPDU-ETU;

    invoke-static {p0, v0}, Lcom/squareup/receiving/StandardReceiver;->rejectIfNot(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$0(Lcom/squareup/protos/client/dialogue/CreateCommentResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/protos/client/dialogue/CreateCommentResponse;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$2(Lcom/squareup/protos/client/dialogue/GetConversationResponse;)Ljava/lang/Boolean;
    .locals 0

    .line 68
    iget-object p0, p0, Lcom/squareup/protos/client/dialogue/GetConversationResponse;->conversation:Lcom/squareup/protos/client/dialogue/Conversation;

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public createComment(Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/UUID;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/dialogue/CreateCommentResponse;",
            ">;>;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/protos/client/dialogue/CreateCommentRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/CreateCommentRequest$Builder;-><init>()V

    .line 34
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/dialogue/CreateCommentRequest$Builder;->conversation_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/CreateCommentRequest$Builder;

    move-result-object p1

    .line 35
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/dialogue/CreateCommentRequest$Builder;->comment(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/CreateCommentRequest$Builder;

    move-result-object p1

    .line 36
    invoke-virtual {p3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/dialogue/CreateCommentRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/CreateCommentRequest$Builder;

    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/CreateCommentRequest$Builder;->build()Lcom/squareup/protos/client/dialogue/CreateCommentRequest;

    move-result-object p1

    .line 39
    iget-object p2, p0, Lcom/squareup/crm/DialogueServiceHelper;->dialogue:Lcom/squareup/server/crm/DialogueService;

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/DialogueService;->createComment(Lcom/squareup/protos/client/dialogue/CreateCommentRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 40
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    sget-object p2, Lcom/squareup/crm/-$$Lambda$DialogueServiceHelper$K87v-RrwXTj-dD2AWbeT7Lq4Xug;->INSTANCE:Lcom/squareup/crm/-$$Lambda$DialogueServiceHelper$K87v-RrwXTj-dD2AWbeT7Lq4Xug;

    .line 41
    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public createConversation(Ljava/lang/String;Ljava/lang/String;Ljava/util/UUID;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/UUID;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/dialogue/CreateConversationResponse;",
            ">;>;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/protos/client/dialogue/CreateConversationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/CreateConversationRequest$Builder;-><init>()V

    .line 49
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/dialogue/CreateConversationRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/CreateConversationRequest$Builder;

    move-result-object p1

    .line 50
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/dialogue/CreateConversationRequest$Builder;->comment(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/CreateConversationRequest$Builder;

    move-result-object p1

    .line 51
    invoke-virtual {p3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/dialogue/CreateConversationRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/CreateConversationRequest$Builder;

    move-result-object p1

    .line 52
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/CreateConversationRequest$Builder;->build()Lcom/squareup/protos/client/dialogue/CreateConversationRequest;

    move-result-object p1

    .line 54
    iget-object p2, p0, Lcom/squareup/crm/DialogueServiceHelper;->dialogue:Lcom/squareup/server/crm/DialogueService;

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/DialogueService;->createConversation(Lcom/squareup/protos/client/dialogue/CreateConversationRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public createCoupon(Ljava/lang/String;Lcom/squareup/api/items/Discount;Ljava/util/UUID;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/Discount;",
            "Ljava/util/UUID;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/dialogue/CreateCouponResponse;",
            ">;>;"
        }
    .end annotation

    .line 85
    new-instance v0, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;-><init>()V

    .line 86
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;->conversation_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;

    move-result-object p1

    .line 87
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;->discount(Lcom/squareup/api/items/Discount;)Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;

    move-result-object p1

    .line 88
    invoke-virtual {p3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;->request_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;

    move-result-object p1

    .line 89
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/CreateCouponRequest$Builder;->build()Lcom/squareup/protos/client/dialogue/CreateCouponRequest;

    move-result-object p1

    .line 91
    iget-object p2, p0, Lcom/squareup/crm/DialogueServiceHelper;->dialogue:Lcom/squareup/server/crm/DialogueService;

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/DialogueService;->createCoupon(Lcom/squareup/protos/client/dialogue/CreateCouponRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getConversation(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/dialogue/GetConversationResponse;",
            ">;>;"
        }
    .end annotation

    .line 59
    new-instance v0, Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;-><init>()V

    .line 60
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;->conversation_token(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 61
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;->opened(Ljava/lang/Boolean;)Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;

    move-result-object p1

    .line 62
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/GetConversationRequest$Builder;->build()Lcom/squareup/protos/client/dialogue/GetConversationRequest;

    move-result-object p1

    .line 64
    iget-object v0, p0, Lcom/squareup/crm/DialogueServiceHelper;->dialogue:Lcom/squareup/server/crm/DialogueService;

    invoke-interface {v0, p1}, Lcom/squareup/server/crm/DialogueService;->getConversation(Lcom/squareup/protos/client/dialogue/GetConversationRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 65
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    sget-object v0, Lcom/squareup/crm/-$$Lambda$DialogueServiceHelper$O08oYqpF_S8euYMh9AYJxJP2NO8;->INSTANCE:Lcom/squareup/crm/-$$Lambda$DialogueServiceHelper$O08oYqpF_S8euYMh9AYJxJP2NO8;

    .line 67
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public listConversations(Ljava/lang/String;Ljava/lang/Integer;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/dialogue/ListConversationsResponse;",
            ">;>;"
        }
    .end annotation

    .line 74
    new-instance v0, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;-><init>()V

    .line 75
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;

    move-result-object p1

    .line 76
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;

    move-result-object p1

    .line 77
    invoke-virtual {p1}, Lcom/squareup/protos/client/dialogue/ListConversationsRequest$Builder;->build()Lcom/squareup/protos/client/dialogue/ListConversationsRequest;

    move-result-object p1

    .line 79
    iget-object p2, p0, Lcom/squareup/crm/DialogueServiceHelper;->dialogue:Lcom/squareup/server/crm/DialogueService;

    invoke-interface {p2, p1}, Lcom/squareup/server/crm/DialogueService;->listConversations(Lcom/squareup/protos/client/dialogue/ListConversationsRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
