.class public abstract Lcom/squareup/crm/groups/CrmGroupsModule;
.super Ljava/lang/Object;
.source "CrmGroupsModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH!J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u0005\u001a\u00020\rH!J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\t\u001a\u00020\u0010H!\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/crm/groups/CrmGroupsModule;",
        "",
        "()V",
        "bindChooseGroupsViewFactory",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsViewFactory;",
        "realViewFactory",
        "Lcom/squareup/crm/groups/choose/RealChooseGroupsViewFactory;",
        "bindChooseGroupsWorkflow",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;",
        "realWorkflow",
        "Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;",
        "bindEditGroupViewFactory",
        "Lcom/squareup/crm/groups/edit/EditGroupViewFactory;",
        "Lcom/squareup/crm/groups/edit/RealEditGroupViewFactory;",
        "bindEditGroupWorkflow",
        "Lcom/squareup/crm/groups/edit/EditGroupWorkflow;",
        "Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindChooseGroupsViewFactory(Lcom/squareup/crm/groups/choose/RealChooseGroupsViewFactory;)Lcom/squareup/crm/groups/choose/ChooseGroupsViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindChooseGroupsWorkflow(Lcom/squareup/crm/groups/choose/RealChooseGroupsWorkflow;)Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindEditGroupViewFactory(Lcom/squareup/crm/groups/edit/RealEditGroupViewFactory;)Lcom/squareup/crm/groups/edit/EditGroupViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindEditGroupWorkflow(Lcom/squareup/crm/groups/edit/RealEditGroupWorkflow;)Lcom/squareup/crm/groups/edit/EditGroupWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
