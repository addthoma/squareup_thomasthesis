.class public final Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration_Factory;
.super Ljava/lang/Object;
.source "DefaultViewCustomerConfiguration_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration_Factory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration_Factory;
    .locals 1

    .line 17
    invoke-static {}, Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration_Factory$InstanceHolder;->access$000()Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration_Factory;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance()Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;

    invoke-direct {v0}, Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;-><init>()V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;
    .locals 1

    .line 13
    invoke-static {}, Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration_Factory;->newInstance()Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration_Factory;->get()Lcom/squareup/crm/viewcustomerconfiguration/api/DefaultViewCustomerConfiguration;

    move-result-object v0

    return-object v0
.end method
