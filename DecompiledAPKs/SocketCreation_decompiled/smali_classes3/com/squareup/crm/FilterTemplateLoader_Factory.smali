.class public final Lcom/squareup/crm/FilterTemplateLoader_Factory;
.super Ljava/lang/Object;
.source "FilterTemplateLoader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/crm/FilterTemplateLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/crm/FilterTemplateLoader_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/crm/FilterTemplateLoader_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/crm/FilterTemplateLoader_Factory;->rolodexProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/crm/FilterTemplateLoader_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;)",
            "Lcom/squareup/crm/FilterTemplateLoader_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/crm/FilterTemplateLoader_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/crm/FilterTemplateLoader_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/crm/FilterTemplateLoader;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/crm/FilterTemplateLoader;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/crm/FilterTemplateLoader;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/crm/RolodexServiceHelper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/crm/FilterTemplateLoader;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/crm/FilterTemplateLoader_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/crm/FilterTemplateLoader_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v2, p0, Lcom/squareup/crm/FilterTemplateLoader_Factory;->rolodexProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/crm/RolodexServiceHelper;

    invoke-static {v0, v1, v2}, Lcom/squareup/crm/FilterTemplateLoader_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/crm/FilterTemplateLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/crm/FilterTemplateLoader_Factory;->get()Lcom/squareup/crm/FilterTemplateLoader;

    move-result-object v0

    return-object v0
.end method
