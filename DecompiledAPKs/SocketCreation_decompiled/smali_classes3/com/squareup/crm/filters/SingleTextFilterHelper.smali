.class public Lcom/squareup/crm/filters/SingleTextFilterHelper;
.super Ljava/lang/Object;
.source "SingleTextFilterHelper.java"


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/crm/filters/SingleTextFilterHelper;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public getHint(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/lang/String;
    .locals 2

    .line 38
    sget-object v0, Lcom/squareup/crm/filters/SingleTextFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 p1, 0x2

    if-eq v0, p1, :cond_1

    const/4 p1, 0x3

    if-ne v0, p1, :cond_0

    .line 46
    iget-object p1, p0, Lcom/squareup/crm/filters/SingleTextFilterHelper;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/applet/R$string;->crm_custom_attribute_email_hint:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 49
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    .line 43
    :cond_1
    iget-object p1, p0, Lcom/squareup/crm/filters/SingleTextFilterHelper;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/applet/R$string;->crm_custom_attribute_phone_hint:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 40
    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    return-object p1
.end method

.method public getText(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/lang/String;
    .locals 2

    .line 54
    sget-object v0, Lcom/squareup/crm/filters/SingleTextFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 62
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_value:Ljava/lang/String;

    return-object p1

    .line 65
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    .line 59
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_value:Ljava/lang/String;

    return-object p1

    .line 56
    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_value:Ljava/lang/String;

    return-object p1
.end method

.method public getTextInputType(Lcom/squareup/protos/client/rolodex/Filter;)I
    .locals 2

    .line 71
    sget-object v0, Lcom/squareup/crm/filters/SingleTextFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    const/4 v1, 0x3

    if-eq p1, v0, :cond_1

    if-ne p1, v1, :cond_0

    const/16 p1, 0x21

    return p1

    .line 82
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_1
    return v1

    :cond_2
    return v0
.end method

.method public isValid(Lcom/squareup/protos/client/rolodex/Filter;)Z
    .locals 3

    .line 22
    sget-object v0, Lcom/squareup/crm/filters/SingleTextFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 30
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cae_attribute_value:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/2addr p1, v1

    return p1

    .line 33
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    .line 27
    :cond_1
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cap_attribute_value:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/2addr p1, v1

    return p1

    .line 24
    :cond_2
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->cat_attribute_value:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/2addr p1, v1

    return p1
.end method

.method public setText(Lcom/squareup/protos/client/rolodex/Filter;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter;
    .locals 2

    .line 87
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter;->newBuilder()Lcom/squareup/protos/client/rolodex/Filter$Builder;

    move-result-object v0

    .line 89
    sget-object v1, Lcom/squareup/crm/filters/SingleTextFilterHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    .line 99
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cae_attribute_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 103
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    .line 95
    :cond_1
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cap_attribute_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    goto :goto_0

    .line 91
    :cond_2
    invoke-virtual {v0, p2}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->cat_attribute_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    .line 106
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->build()Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p1

    return-object p1
.end method
