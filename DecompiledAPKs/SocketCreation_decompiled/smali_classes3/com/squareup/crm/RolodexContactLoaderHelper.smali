.class public Lcom/squareup/crm/RolodexContactLoaderHelper;
.super Ljava/lang/Object;
.source "RolodexContactLoaderHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;
    }
.end annotation


# static fields
.field public static final TOTAL_COUNT_UNDEFINED:I = -0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static canDetermineTotalContactCount(Lcom/squareup/crm/RolodexContactLoader$Input;)Z
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->searchTerm:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->filterList:Ljava/util/List;

    invoke-static {p0}, Lcom/squareup/util/SquareCollections;->isNullOrEmpty(Ljava/util/Collection;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static getTotalCountFound(Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexGroupLoader;Ljava/lang/String;Lrx/functions/Action1;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/RolodexContactLoader;",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            "Ljava/lang/String;",
            "Lrx/functions/Action1<",
            "-",
            "Lrx/Subscription;",
            ">;)",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 113
    invoke-virtual {p0}, Lcom/squareup/crm/RolodexContactLoader;->results()Lrx/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/crm/-$$Lambda$RolodexContactLoaderHelper$LESh5o7kHfR5k2vdduNGwn9uIlE;

    invoke-direct {v0, p2}, Lcom/squareup/crm/-$$Lambda$RolodexContactLoaderHelper$LESh5o7kHfR5k2vdduNGwn9uIlE;-><init>(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p0, v0}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    new-instance v0, Lcom/squareup/crm/-$$Lambda$RolodexContactLoaderHelper$9-DOIO_P8g8-cxZPGxk05tVVRd4;

    invoke-direct {v0, p2, p1}, Lcom/squareup/crm/-$$Lambda$RolodexContactLoaderHelper$9-DOIO_P8g8-cxZPGxk05tVVRd4;-><init>(Ljava/lang/String;Lcom/squareup/crm/RolodexGroupLoader;)V

    .line 115
    invoke-virtual {p0, v0}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    .line 127
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    const/4 p1, 0x1

    .line 128
    invoke-virtual {p0, p1}, Lrx/Observable;->replay(I)Lrx/observables/ConnectableObservable;

    move-result-object p0

    .line 129
    invoke-virtual {p0, p1, p3}, Lrx/observables/ConnectableObservable;->autoConnect(ILrx/functions/Action1;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$getTotalCountFound$3(Ljava/lang/String;Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)Ljava/lang/Boolean;
    .locals 0

    .line 114
    iget-object p1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->input:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/crm/RolodexContactLoader$Input;

    iget-object p1, p1, Lcom/squareup/crm/RolodexContactLoader$Input;->groupToken:Ljava/lang/String;

    invoke-static {p1, p0}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$getTotalCountFound$5(Ljava/lang/String;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)Lrx/Observable;
    .locals 0

    .line 115
    iget-object p2, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->input:Ljava/lang/Object;

    check-cast p2, Lcom/squareup/crm/RolodexContactLoader$Input;

    invoke-static {p2}, Lcom/squareup/crm/RolodexContactLoaderHelper;->canDetermineTotalContactCount(Lcom/squareup/crm/RolodexContactLoader$Input;)Z

    move-result p2

    if-nez p2, :cond_0

    const/4 p0, -0x1

    .line 116
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-static {p0}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p0

    goto :goto_1

    :cond_0
    if-nez p0, :cond_1

    .line 118
    invoke-interface {p1}, Lcom/squareup/crm/RolodexGroupLoader;->allCustomersCount()Lio/reactivex/Observable;

    move-result-object p0

    goto :goto_0

    .line 119
    :cond_1
    invoke-interface {p1, p0}, Lcom/squareup/crm/RolodexGroupLoader;->success(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p0

    sget-object p1, Lcom/squareup/crm/-$$Lambda$RolodexContactLoaderHelper$o39U7sUoT0VkivmAZ7N_ZM2-2KI;->INSTANCE:Lcom/squareup/crm/-$$Lambda$RolodexContactLoaderHelper$o39U7sUoT0VkivmAZ7N_ZM2-2KI;

    .line 120
    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    :goto_0
    sget-object p1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    .line 117
    invoke-static {p0, p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p0

    :goto_1
    return-object p0
.end method

.method static synthetic lambda$null$4(Lcom/squareup/protos/client/rolodex/Group;)Ljava/lang/Integer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 121
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Group;->num_customers:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 122
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Group;->num_customers:Ljava/lang/Long;

    invoke-virtual {p0}, Ljava/lang/Long;->intValue()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, -0x1

    .line 124
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$visualStateOf$0(Ljava/lang/String;Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)Ljava/lang/Boolean;
    .locals 0

    .line 60
    iget-object p1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->input:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/crm/RolodexContactLoader$Input;

    iget-object p1, p1, Lcom/squareup/crm/RolodexContactLoader$Input;->groupToken:Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$visualStateOf$1(Lcom/squareup/util/Optional;Lcom/squareup/util/Optional;Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;
    .locals 1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;

    iget-boolean p0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->isFirstPage:Z

    if-eqz p0, :cond_0

    .line 70
    sget-object p0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_PROGRESS_SPINNER:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    return-object p0

    .line 71
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Failure;

    iget-boolean p0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Failure;->isFirstPage:Z

    if-eqz p0, :cond_1

    .line 72
    sget-object p0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_FAILED_TO_LOAD:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    return-object p0

    :cond_1
    if-eqz p2, :cond_6

    .line 75
    iget-object p0, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->input:Ljava/lang/Object;

    check-cast p0, Lcom/squareup/crm/RolodexContactLoader$Input;

    iget-object p0, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->searchTerm:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_2

    iget-object p0, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->input:Ljava/lang/Object;

    check-cast p0, Lcom/squareup/crm/RolodexContactLoader$Input;

    iget-object p0, p0, Lcom/squareup/crm/RolodexContactLoader$Input;->filterList:Ljava/util/List;

    .line 76
    invoke-static {p0}, Lcom/squareup/util/SquareCollections;->isNullOrEmpty(Ljava/util/Collection;)Z

    move-result p0

    if-eqz p0, :cond_2

    const/4 p0, 0x1

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    .line 78
    :goto_0
    iget-object p1, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_4

    if-eqz p0, :cond_3

    .line 79
    sget-object p0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_NO_CUSTOMERS_AT_ALL:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    goto :goto_1

    :cond_3
    sget-object p0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_NO_CUSTOMERS_FOUND:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    :goto_1
    return-object p0

    :cond_4
    if-eqz p0, :cond_5

    .line 83
    sget-object p0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_CUSTOMERS_ALL:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    goto :goto_2

    :cond_5
    sget-object p0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_CUSTOMERS_FOUND:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    :goto_2
    return-object p0

    :cond_6
    const/4 p0, 0x0

    return-object p0
.end method

.method static synthetic lambda$visualStateOf$2(Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)Ljava/lang/Boolean;
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 91
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public static visualStateOf(Lcom/squareup/crm/RolodexContactLoader;Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/RolodexContactLoader;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
            ">;"
        }
    .end annotation

    .line 59
    invoke-virtual {p0}, Lcom/squareup/crm/RolodexContactLoader;->results()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/crm/-$$Lambda$RolodexContactLoaderHelper$nXr5VyPIJkx5wKoNn9VcRl5MALQ;

    invoke-direct {v1, p1}, Lcom/squareup/crm/-$$Lambda$RolodexContactLoaderHelper$nXr5VyPIJkx5wKoNn9VcRl5MALQ;-><init>(Ljava/lang/String;)V

    .line 60
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    const/4 v0, 0x0

    check-cast v0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;

    .line 61
    invoke-virtual {p1, v0}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    .line 65
    invoke-virtual {p0}, Lcom/squareup/crm/RolodexContactLoader;->progress()Lrx/Observable;

    move-result-object v0

    .line 66
    invoke-virtual {p0}, Lcom/squareup/crm/RolodexContactLoader;->failure()Lrx/Observable;

    move-result-object p0

    sget-object v1, Lcom/squareup/crm/-$$Lambda$RolodexContactLoaderHelper$nHXfRH72sFeX--G8LHmvZZkKxq8;->INSTANCE:Lcom/squareup/crm/-$$Lambda$RolodexContactLoaderHelper$nHXfRH72sFeX--G8LHmvZZkKxq8;

    .line 64
    invoke-static {v0, p0, p1, v1}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object p0

    sget-object p1, Lcom/squareup/crm/-$$Lambda$RolodexContactLoaderHelper$f6eYq13Q62fFT28AoggayvOHHKg;->INSTANCE:Lcom/squareup/crm/-$$Lambda$RolodexContactLoaderHelper$f6eYq13Q62fFT28AoggayvOHHKg;

    .line 91
    invoke-virtual {p0, p1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    .line 92
    invoke-virtual {p0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p0

    return-object p0
.end method
