.class public final Lcom/squareup/crm/applet/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/crm/applet/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final crm_action_create_manual_group:I = 0x7f0a03dc

.field public static final crm_add_filter:I = 0x7f0a03e2

.field public static final crm_applet_conversation_container:I = 0x7f0a03ed

.field public static final crm_contact_list:I = 0x7f0a040f

.field public static final crm_conversation:I = 0x7f0a0414

.field public static final crm_create_group:I = 0x7f0a041b

.field public static final crm_delete_group:I = 0x7f0a0436

.field public static final crm_drop_down_container:I = 0x7f0a0438

.field public static final crm_empty_filter_content:I = 0x7f0a043e

.field public static final crm_empty_filter_content_stub:I = 0x7f0a043f

.field public static final crm_errors_bar:I = 0x7f0a0442

.field public static final crm_filter_bubble_name:I = 0x7f0a0445

.field public static final crm_filter_bubble_name_container:I = 0x7f0a0446

.field public static final crm_filter_bubble_x:I = 0x7f0a0447

.field public static final crm_filter_layout_linearlayout:I = 0x7f0a0448

.field public static final crm_filter_list:I = 0x7f0a0449

.field public static final crm_filter_search_box:I = 0x7f0a044a

.field public static final crm_filter_search_empty:I = 0x7f0a044b

.field public static final crm_filters_layout:I = 0x7f0a044c

.field public static final crm_group_list:I = 0x7f0a0453

.field public static final crm_group_name:I = 0x7f0a0454

.field public static final crm_group_name_description:I = 0x7f0a0455

.field public static final crm_group_name_field:I = 0x7f0a0456

.field public static final crm_group_name_label:I = 0x7f0a0458

.field public static final crm_group_row_name:I = 0x7f0a0459

.field public static final crm_groups_list:I = 0x7f0a045d

.field public static final crm_master_contact_list:I = 0x7f0a046f

.field public static final crm_master_create_customer_button:I = 0x7f0a0470

.field public static final crm_master_customer_lookup:I = 0x7f0a0471

.field public static final crm_master_drop_down:I = 0x7f0a0472

.field public static final crm_master_empty_directory_message_phone:I = 0x7f0a0473

.field public static final crm_master_menu_add_to_manual_group:I = 0x7f0a0474

.field public static final crm_master_menu_create_customer:I = 0x7f0a0475

.field public static final crm_master_menu_default:I = 0x7f0a0476

.field public static final crm_master_menu_delete_customers:I = 0x7f0a0477

.field public static final crm_master_menu_edit_group:I = 0x7f0a0478

.field public static final crm_master_menu_filter_list:I = 0x7f0a0479

.field public static final crm_master_menu_merge_customers:I = 0x7f0a047a

.field public static final crm_master_menu_remove_from_manual_group:I = 0x7f0a047b

.field public static final crm_master_menu_resolve_duplicates:I = 0x7f0a047c

.field public static final crm_master_menu_view_feedback:I = 0x7f0a047d

.field public static final crm_master_menu_view_groups:I = 0x7f0a047e

.field public static final crm_master_multiselect_button_blue:I = 0x7f0a047f

.field public static final crm_master_multiselect_button_red:I = 0x7f0a0480

.field public static final crm_master_progress:I = 0x7f0a0481

.field public static final crm_master_warning_message:I = 0x7f0a0482

.field public static final crm_merge_proposal_included_check:I = 0x7f0a0483

.field public static final crm_merge_proposal_list:I = 0x7f0a0484

.field public static final crm_merge_proposal_new_contact_title:I = 0x7f0a0485

.field public static final crm_merge_proposal_title_row:I = 0x7f0a0486

.field public static final crm_message:I = 0x7f0a0487

.field public static final crm_messages_list_empty_list:I = 0x7f0a048e

.field public static final crm_messages_list_view:I = 0x7f0a048f

.field public static final crm_minimum_visits:I = 0x7f0a0490

.field public static final crm_minimum_visits_header:I = 0x7f0a0491

.field public static final crm_multi_option_filter_content:I = 0x7f0a0492

.field public static final crm_multi_option_filter_content_stub:I = 0x7f0a0493

.field public static final crm_multiselect_actionbar:I = 0x7f0a0494

.field public static final crm_multiselect_cancel:I = 0x7f0a0495

.field public static final crm_multiselect_dropdown:I = 0x7f0a0496

.field public static final crm_multiselect_dropdown_container:I = 0x7f0a0497

.field public static final crm_multiselect_dropdown_view:I = 0x7f0a0498

.field public static final crm_multiselect_menu_deselect_all:I = 0x7f0a0499

.field public static final crm_multiselect_menu_select_all:I = 0x7f0a049a

.field public static final crm_multiselect_title:I = 0x7f0a049b

.field public static final crm_no_customer_selected_row1:I = 0x7f0a04a0

.field public static final crm_no_customer_selected_row2:I = 0x7f0a04a1

.field public static final crm_no_customer_selected_screen:I = 0x7f0a04a2

.field public static final crm_option_list:I = 0x7f0a04ac

.field public static final crm_progress_bar:I = 0x7f0a04cb

.field public static final crm_remove_all_filters:I = 0x7f0a04da

.field public static final crm_remove_filter_button:I = 0x7f0a04db

.field public static final crm_save_filters:I = 0x7f0a04e4

.field public static final crm_select_loyalty_hint:I = 0x7f0a04f7

.field public static final crm_single_option_filter_content:I = 0x7f0a04fd

.field public static final crm_single_option_filter_content_stub:I = 0x7f0a04fe

.field public static final crm_single_text_filter_content:I = 0x7f0a04ff

.field public static final crm_single_text_filter_content_stub:I = 0x7f0a0500

.field public static final crm_text_field:I = 0x7f0a0503

.field public static final crm_time_period_header:I = 0x7f0a0504

.field public static final crm_time_period_list:I = 0x7f0a0505

.field public static final crm_v2_all_customers_list:I = 0x7f0a0509

.field public static final crm_v2_view_group:I = 0x7f0a050a

.field public static final crm_v2_view_groups_list:I = 0x7f0a050b

.field public static final crm_visit_frequency_filter_content:I = 0x7f0a0514

.field public static final crm_visit_frequency_filter_content_stub:I = 0x7f0a0515

.field public static final customers_applet_customer_messages_list_view:I = 0x7f0a053d

.field public static final customers_applet_customer_messages_progress_bar:I = 0x7f0a053e


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
