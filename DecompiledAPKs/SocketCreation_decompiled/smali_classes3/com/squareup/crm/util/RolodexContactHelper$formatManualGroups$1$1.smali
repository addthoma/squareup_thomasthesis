.class final Lcom/squareup/crm/util/RolodexContactHelper$formatManualGroups$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RolodexContactHelper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/util/RolodexContactHelper;->formatManualGroups(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/protos/client/rolodex/Group;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/crm/util/RolodexContactHelper$formatManualGroups$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/crm/util/RolodexContactHelper$formatManualGroups$1$1;

    invoke-direct {v0}, Lcom/squareup/crm/util/RolodexContactHelper$formatManualGroups$1$1;-><init>()V

    sput-object v0, Lcom/squareup/crm/util/RolodexContactHelper$formatManualGroups$1$1;->INSTANCE:Lcom/squareup/crm/util/RolodexContactHelper$formatManualGroups$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/util/RolodexContactHelper$formatManualGroups$1$1;->invoke(Lcom/squareup/protos/client/rolodex/Group;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/client/rolodex/Group;)Ljava/lang/String;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 338
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    const-string v0, "it.display_name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
