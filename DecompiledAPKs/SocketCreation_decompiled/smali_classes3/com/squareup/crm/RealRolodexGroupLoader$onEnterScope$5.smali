.class final Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$5;
.super Ljava/lang/Object;
.source "RealRolodexGroupLoader.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/crm/RealRolodexGroupLoader;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lkotlin/Pair<",
        "+",
        "Lkotlin/Unit;",
        "+",
        "Ljava/lang/Long;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0000\u0010\u0000\u001a\u00020\u00012\"\u0010\u0002\u001a\u001e\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lkotlin/Pair;",
        "",
        "kotlin.jvm.PlatformType",
        "",
        "test"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/crm/RealRolodexGroupLoader;


# direct methods
.method constructor <init>(Lcom/squareup/crm/RealRolodexGroupLoader;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$5;->this$0:Lcom/squareup/crm/RealRolodexGroupLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 31
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$5;->test(Lkotlin/Pair;)Z

    move-result p1

    return p1
.end method

.method public final test(Lkotlin/Pair;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lkotlin/Unit;",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object p1, p0, Lcom/squareup/crm/RealRolodexGroupLoader$onEnterScope$5;->this$0:Lcom/squareup/crm/RealRolodexGroupLoader;

    invoke-static {p1}, Lcom/squareup/crm/RealRolodexGroupLoader;->access$getIncludeCounts$p(Lcom/squareup/crm/RealRolodexGroupLoader;)Z

    move-result p1

    return p1
.end method
