.class public final Lcom/squareup/gson/GsonModule;
.super Ljava/lang/Object;
.source "GsonModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0001J\u0008\u0010\u0005\u001a\u00020\u0004H\u0001\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/gson/GsonModule;",
        "",
        "()V",
        "provideGson",
        "Lcom/google/gson/Gson;",
        "provideWireGson",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/gson/GsonModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/gson/GsonModule;

    invoke-direct {v0}, Lcom/squareup/gson/GsonModule;-><init>()V

    sput-object v0, Lcom/squareup/gson/GsonModule;->INSTANCE:Lcom/squareup/gson/GsonModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideGson()Lcom/google/gson/Gson;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 14
    invoke-static {}, Lcom/squareup/gson/GsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v0

    const-string v1, "GsonProvider.gson()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final provideWireGson()Lcom/google/gson/Gson;
    .locals 2
    .annotation runtime Lcom/squareup/gson/WireGson;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 16
    invoke-static {}, Lcom/squareup/gson/WireGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v0

    const-string v1, "WireGsonProvider.gson()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
