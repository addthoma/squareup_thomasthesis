.class Lcom/squareup/gson/GsonProvider$JediComponentKindAdapter;
.super Lcom/google/gson/TypeAdapter;
.source "GsonProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/gson/GsonProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "JediComponentKindAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/TypeAdapter<",
        "Lcom/squareup/protos/jedi/service/ComponentKind;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 175
    invoke-direct {p0}, Lcom/google/gson/TypeAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/gson/GsonProvider$1;)V
    .locals 0

    .line 175
    invoke-direct {p0}, Lcom/squareup/gson/GsonProvider$JediComponentKindAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public read(Lcom/google/gson/stream/JsonReader;)Lcom/squareup/protos/jedi/service/ComponentKind;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 181
    invoke-virtual {p1}, Lcom/google/gson/stream/JsonReader;->nextInt()I

    move-result p1

    invoke-static {p1}, Lcom/squareup/protos/jedi/service/ComponentKind;->fromValue(I)Lcom/squareup/protos/jedi/service/ComponentKind;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic read(Lcom/google/gson/stream/JsonReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 175
    invoke-virtual {p0, p1}, Lcom/squareup/gson/GsonProvider$JediComponentKindAdapter;->read(Lcom/google/gson/stream/JsonReader;)Lcom/squareup/protos/jedi/service/ComponentKind;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/google/gson/stream/JsonWriter;Lcom/squareup/protos/jedi/service/ComponentKind;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 177
    invoke-virtual {p2}, Lcom/squareup/protos/jedi/service/ComponentKind;->getValue()I

    move-result p2

    int-to-long v0, p2

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/stream/JsonWriter;->value(J)Lcom/google/gson/stream/JsonWriter;

    return-void
.end method

.method public bridge synthetic write(Lcom/google/gson/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 175
    check-cast p2, Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/gson/GsonProvider$JediComponentKindAdapter;->write(Lcom/google/gson/stream/JsonWriter;Lcom/squareup/protos/jedi/service/ComponentKind;)V

    return-void
.end method
