.class final Lcom/squareup/cycler/Recycler$update$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "Recycler.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecycler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Recycler.kt\ncom/squareup/cycler/Recycler$update$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,606:1\n1587#2,2:607\n1587#2,2:609\n*E\n*S KotlinDebug\n*F\n+ 1 Recycler.kt\ncom/squareup/cycler/Recycler$update$1\n*L\n191#1,2:607\n193#1,2:609\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u00020\u0004H\u008a@\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.cycler.Recycler$update$1"
    f = "Recycler.kt"
    i = {
        0x0
    }
    l = {
        0xb8
    }
    m = "invokeSuspend"
    n = {
        "$this$launch"
    }
    s = {
        "L$0"
    }
.end annotation


# instance fields
.field final synthetic $newUpdate:Lcom/squareup/cycler/Update;

.field L$0:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field final synthetic this$0:Lcom/squareup/cycler/Recycler;


# direct methods
.method constructor <init>(Lcom/squareup/cycler/Recycler;Lcom/squareup/cycler/Update;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    iput-object p2, p0, Lcom/squareup/cycler/Recycler$update$1;->$newUpdate:Lcom/squareup/cycler/Update;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cycler/Recycler$update$1;

    iget-object v1, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    iget-object v2, p0, Lcom/squareup/cycler/Recycler$update$1;->$newUpdate:Lcom/squareup/cycler/Update;

    invoke-direct {v0, v1, v2, p2}, Lcom/squareup/cycler/Recycler$update$1;-><init>(Lcom/squareup/cycler/Recycler;Lcom/squareup/cycler/Update;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/cycler/Recycler$update$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cycler/Recycler$update$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/cycler/Recycler$update$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/cycler/Recycler$update$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 179
    iget v1, p0, Lcom/squareup/cycler/Recycler$update$1;->label:I

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/cycler/Recycler$update$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    goto :goto_0

    .line 204
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 179
    :cond_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/cycler/Recycler$update$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    .line 182
    iget-object v1, p0, Lcom/squareup/cycler/Recycler$update$1;->$newUpdate:Lcom/squareup/cycler/Update;

    .line 183
    iget-object v3, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    invoke-static {v3}, Lcom/squareup/cycler/Recycler;->access$getAdapter$p(Lcom/squareup/cycler/Recycler;)Lcom/squareup/cycler/Recycler$Adapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/cycler/Recycler$Adapter;->getItemComparator$lib_release()Lcom/squareup/cycler/ItemComparator;

    move-result-object v3

    .line 184
    iget-object v4, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    invoke-static {v4}, Lcom/squareup/cycler/Recycler;->access$getBackgroundContext$p(Lcom/squareup/cycler/Recycler;)Lkotlin/coroutines/CoroutineContext;

    move-result-object v4

    iput-object p1, p0, Lcom/squareup/cycler/Recycler$update$1;->L$0:Ljava/lang/Object;

    iput v2, p0, Lcom/squareup/cycler/Recycler$update$1;->label:I

    .line 182
    invoke-virtual {v1, v3, v4, p0}, Lcom/squareup/cycler/Update;->generateDataChangesLambdas$lib_release(Lcom/squareup/cycler/ItemComparator;Lkotlin/coroutines/CoroutineContext;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_2

    return-object v0

    .line 179
    :cond_2
    :goto_0
    check-cast p1, Ljava/util/List;

    .line 187
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    invoke-static {v0}, Lcom/squareup/cycler/Recycler;->access$getCurrentUpdate$p(Lcom/squareup/cycler/Recycler;)Lcom/squareup/cycler/Update;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cycler/Recycler$update$1;->$newUpdate:Lcom/squareup/cycler/Update;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 189
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$update$1;->$newUpdate:Lcom/squareup/cycler/Update;

    iget-object v1, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    invoke-static {v1}, Lcom/squareup/cycler/Recycler;->access$getConfig$p(Lcom/squareup/cycler/Recycler;)Lcom/squareup/cycler/Recycler$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Update;->createNewRecyclerData(Lcom/squareup/cycler/Recycler$Config;)Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    .line 190
    iget-object v1, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    invoke-static {v1}, Lcom/squareup/cycler/Recycler;->access$getAdapter$p(Lcom/squareup/cycler/Recycler;)Lcom/squareup/cycler/Recycler$Adapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Adapter;->setCurrentRecyclerData(Lcom/squareup/cycler/RecyclerData;)V

    .line 191
    iget-object v1, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    invoke-static {v1}, Lcom/squareup/cycler/Recycler;->access$getExtensions$p(Lcom/squareup/cycler/Recycler;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 607
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cycler/Extension;

    .line 191
    invoke-interface {v2, v0}, Lcom/squareup/cycler/Extension;->setData(Lcom/squareup/cycler/RecyclerData;)V

    goto :goto_1

    .line 193
    :cond_3
    check-cast p1, Ljava/lang/Iterable;

    .line 609
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 193
    iget-object v1, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    invoke-static {v1}, Lcom/squareup/cycler/Recycler;->access$getAdapter$p(Lcom/squareup/cycler/Recycler;)Lcom/squareup/cycler/Recycler$Adapter;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 194
    :cond_4
    iget-object p1, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    move-result-object p1

    if-nez p1, :cond_5

    .line 197
    iget-object p1, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    invoke-static {v0}, Lcom/squareup/cycler/Recycler;->access$getAdapter$p(Lcom/squareup/cycler/Recycler;)Lcom/squareup/cycler/Recycler$Adapter;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 199
    :cond_5
    iget-object p1, p0, Lcom/squareup/cycler/Recycler$update$1;->this$0:Lcom/squareup/cycler/Recycler;

    const/4 v0, 0x0

    check-cast v0, Lcom/squareup/cycler/Update;

    invoke-static {p1, v0}, Lcom/squareup/cycler/Recycler;->access$setCurrentUpdate$p(Lcom/squareup/cycler/Recycler;Lcom/squareup/cycler/Update;)V

    .line 200
    iget-object p1, p0, Lcom/squareup/cycler/Recycler$update$1;->$newUpdate:Lcom/squareup/cycler/Update;

    invoke-virtual {p1}, Lcom/squareup/cycler/Update;->getOnReady$lib_release()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    goto :goto_3

    .line 202
    :cond_6
    iget-object p1, p0, Lcom/squareup/cycler/Recycler$update$1;->$newUpdate:Lcom/squareup/cycler/Update;

    invoke-virtual {p1}, Lcom/squareup/cycler/Update;->getOnCancelled$lib_release()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 204
    :goto_3
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method
