.class public final Lcom/squareup/cycler/DataSourceDiff;
.super Landroidx/recyclerview/widget/DiffUtil$Callback;
.source "RecyclerDiff.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroidx/recyclerview/widget/DiffUtil$Callback;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0001\n\u0002\u0008\u0003\u0018\u0000*\u0004\u0008\u0000\u0010\u00012\u00020\u0002B/\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006\u00a2\u0006\u0002\u0010\u0008J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000cH\u0016J\u0018\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000cH\u0016J\u001a\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000cH\u0016J\u0008\u0010\u0011\u001a\u00020\u000cH\u0016J\u0008\u0010\u0012\u001a\u00020\u000cH\u0016R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/cycler/DataSourceDiff;",
        "T",
        "Landroidx/recyclerview/widget/DiffUtil$Callback;",
        "helper",
        "Lcom/squareup/cycler/ItemComparator;",
        "oldList",
        "Lcom/squareup/cycler/DataSource;",
        "newList",
        "(Lcom/squareup/cycler/ItemComparator;Lcom/squareup/cycler/DataSource;Lcom/squareup/cycler/DataSource;)V",
        "areContentsTheSame",
        "",
        "oldItemPosition",
        "",
        "newItemPosition",
        "areItemsTheSame",
        "getChangePayload",
        "",
        "getNewListSize",
        "getOldListSize",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private final helper:Lcom/squareup/cycler/ItemComparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/ItemComparator<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final newList:Lcom/squareup/cycler/DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/DataSource<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final oldList:Lcom/squareup/cycler/DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/DataSource<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/ItemComparator;Lcom/squareup/cycler/DataSource;Lcom/squareup/cycler/DataSource;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/ItemComparator<",
            "-TT;>;",
            "Lcom/squareup/cycler/DataSource<",
            "+TT;>;",
            "Lcom/squareup/cycler/DataSource<",
            "+TT;>;)V"
        }
    .end annotation

    const-string v0, "helper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "oldList"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newList"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Landroidx/recyclerview/widget/DiffUtil$Callback;-><init>()V

    iput-object p1, p0, Lcom/squareup/cycler/DataSourceDiff;->helper:Lcom/squareup/cycler/ItemComparator;

    iput-object p2, p0, Lcom/squareup/cycler/DataSourceDiff;->oldList:Lcom/squareup/cycler/DataSource;

    iput-object p3, p0, Lcom/squareup/cycler/DataSourceDiff;->newList:Lcom/squareup/cycler/DataSource;

    return-void
.end method


# virtual methods
.method public areContentsTheSame(II)Z
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/squareup/cycler/DataSourceDiff;->helper:Lcom/squareup/cycler/ItemComparator;

    iget-object v1, p0, Lcom/squareup/cycler/DataSourceDiff;->oldList:Lcom/squareup/cycler/DataSource;

    invoke-interface {v1, p1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/cycler/DataSourceDiff;->newList:Lcom/squareup/cycler/DataSource;

    invoke-interface {v1, p2}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lcom/squareup/cycler/ItemComparator;->areSameContent(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public areItemsTheSame(II)Z
    .locals 2

    .line 16
    iget-object v0, p0, Lcom/squareup/cycler/DataSourceDiff;->helper:Lcom/squareup/cycler/ItemComparator;

    iget-object v1, p0, Lcom/squareup/cycler/DataSourceDiff;->oldList:Lcom/squareup/cycler/DataSource;

    invoke-interface {v1, p1}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/cycler/DataSourceDiff;->newList:Lcom/squareup/cycler/DataSource;

    invoke-interface {v1, p2}, Lcom/squareup/cycler/DataSource;->get(I)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Lcom/squareup/cycler/ItemComparator;->areSameIdentity(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic getChangePayload(II)Ljava/lang/Object;
    .locals 0

    .line 6
    invoke-virtual {p0, p1, p2}, Lcom/squareup/cycler/DataSourceDiff;->getChangePayload(II)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public getChangePayload(II)Ljava/lang/Void;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getNewListSize()I
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/cycler/DataSourceDiff;->newList:Lcom/squareup/cycler/DataSource;

    invoke-interface {v0}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v0

    return v0
.end method

.method public getOldListSize()I
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/cycler/DataSourceDiff;->oldList:Lcom/squareup/cycler/DataSource;

    invoke-interface {v0}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v0

    return v0
.end method
