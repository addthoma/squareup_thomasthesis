.class final Lcom/squareup/cycler/Update$generateDataChangesLambdas$4;
.super Lkotlin/jvm/internal/Lambda;
.source "Update.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cycler/Update;->generateDataChangesLambdas$lib_release(Lcom/squareup/cycler/ItemComparator;Lkotlin/coroutines/CoroutineContext;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "*>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\n\u0010\u0004\u001a\u0006\u0012\u0002\u0008\u00030\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "adapter",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cycler/Update;


# direct methods
.method constructor <init>(Lcom/squareup/cycler/Update;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$4;->this$0:Lcom/squareup/cycler/Update;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {p0, p1}, Lcom/squareup/cycler/Update$generateDataChangesLambdas$4;->invoke(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$4;->this$0:Lcom/squareup/cycler/Update;

    invoke-static {v0}, Lcom/squareup/cycler/Update;->access$getOldRecyclerData$p(Lcom/squareup/cycler/Update;)Lcom/squareup/cycler/RecyclerData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v0

    .line 105
    iget-object v1, p0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$4;->this$0:Lcom/squareup/cycler/Update;

    invoke-static {v1}, Lcom/squareup/cycler/Update;->access$getAddedChunks$p(Lcom/squareup/cycler/Update;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v1

    sget-object v2, Lcom/squareup/cycler/Update$generateDataChangesLambdas$4$count$1;->INSTANCE:Lkotlin/reflect/KProperty1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, v2}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v1

    invoke-static {v1}, Lkotlin/sequences/SequencesKt;->sumOfInt(Lkotlin/sequences/Sequence;)I

    move-result v1

    .line 106
    invoke-virtual {p1, v0, v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRangeInserted(II)V

    return-void
.end method
