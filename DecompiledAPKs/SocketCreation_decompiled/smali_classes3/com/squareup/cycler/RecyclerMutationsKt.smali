.class public final Lcom/squareup/cycler/RecyclerMutationsKt;
.super Ljava/lang/Object;
.source "RecyclerMutations.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerMutations.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerMutations.kt\ncom/squareup/cycler/RecyclerMutationsKt\n+ 2 Recycler.kt\ncom/squareup/cycler/Recycler\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$CreatorContext\n*L\n1#1,278:1\n112#2:279\n112#2:281\n131#3:280\n*E\n*S KotlinDebug\n*F\n+ 1 RecyclerMutations.kt\ncom/squareup/cycler/RecyclerMutationsKt\n*L\n86#1:279\n109#1:281\n109#1:280\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001aB\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u0002H\u0002\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00072\u0006\u0010\u0008\u001a\u00020\u0006\u001a9\u0010\t\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\n2\u001d\u0010\u000b\u001a\u0019\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00020\r\u0012\u0004\u0012\u00020\u00010\u000c\u00a2\u0006\u0002\u0008\u000e\u001a\u001a\u0010\u000f\u001a\u00020\u0010\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\u0011\u001a \u0010\u0012\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0013\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003*\u0008\u0012\u0004\u0012\u0002H\u00020\u0011\u00a8\u0006\u0014"
    }
    d2 = {
        "dragHandle",
        "",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "view",
        "enableMutations",
        "Lcom/squareup/cycler/Recycler$Config;",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/cycler/MutationExtensionSpec;",
        "Lkotlin/ExtensionFunctionType;",
        "getCurrentMutations",
        "Lcom/squareup/cycler/MutationMap;",
        "Lcom/squareup/cycler/Recycler;",
        "getMutatedData",
        "Lcom/squareup/cycler/DataSource;",
        "lib_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# direct methods
.method public static final dragHandle(Lcom/squareup/cycler/StandardRowSpec$Creator;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;V:",
            "Landroid/view/View;",
            ">(",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "TI;TS;TV;>;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    const-string v0, "$this$dragHandle"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getCreatorContext()Lcom/squareup/cycler/Recycler$CreatorContext;

    move-result-object p0

    .line 280
    invoke-virtual {p0}, Lcom/squareup/cycler/Recycler$CreatorContext;->getRecycler()Lcom/squareup/cycler/Recycler;

    move-result-object p0

    .line 281
    const-class v0, Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler;->extension(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 109
    check-cast p0, Lcom/squareup/cycler/MutationExtension;

    .line 111
    invoke-virtual {p0, p1}, Lcom/squareup/cycler/MutationExtension;->setUpDragHandle$lib_release(Landroid/view/View;)V

    return-void

    .line 109
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Drag and drop extension was not configured for the Recycler."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final enableMutations(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/MutationExtensionSpec<",
            "TI;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$enableMutations"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    new-instance v0, Lcom/squareup/cycler/MutationExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/cycler/MutationExtensionSpec;-><init>()V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    return-void
.end method

.method public static final getCurrentMutations(Lcom/squareup/cycler/Recycler;)Lcom/squareup/cycler/MutationMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/cycler/Recycler<",
            "TI;>;)",
            "Lcom/squareup/cycler/MutationMap;"
        }
    .end annotation

    const-string v0, "$this$getCurrentMutations"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 279
    const-class v0, Lcom/squareup/cycler/MutationExtension;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Recycler;->extension(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cycler/MutationExtension;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/cycler/MutationExtension;->getData()Lcom/squareup/cycler/RecyclerData;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/squareup/cycler/RecyclerData;->copyMutationMap()Lcom/squareup/cycler/MutationMap;

    move-result-object p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    new-instance p0, Lcom/squareup/cycler/MutationMap;

    invoke-direct {p0}, Lcom/squareup/cycler/MutationMap;-><init>()V

    :goto_0
    return-object p0
.end method

.method public static final getMutatedData(Lcom/squareup/cycler/Recycler;)Lcom/squareup/cycler/DataSource;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/cycler/Recycler<",
            "TI;>;)",
            "Lcom/squareup/cycler/DataSource<",
            "TI;>;"
        }
    .end annotation

    const-string v0, "$this$getMutatedData"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0}, Lcom/squareup/cycler/Recycler;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object p0

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 96
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {p0}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v3, v2}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v2

    new-instance v3, Lcom/squareup/cycler/RecyclerMutationsKt$getMutatedData$1$1$1;

    invoke-direct {v3, p0}, Lcom/squareup/cycler/RecyclerMutationsKt$getMutatedData$1$1$1;-><init>(Lcom/squareup/cycler/DataSource;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v3}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p0

    invoke-static {v1, p0}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Lkotlin/sequences/Sequence;)Z

    .line 98
    invoke-static {v0}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p0

    return-object p0
.end method
