.class public final Lcom/squareup/cycler/MutationMapKt;
.super Ljava/lang/Object;
.source "MutationMap.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u0008\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0004"
    }
    d2 = {
        "EMPTY_ARRAY",
        "",
        "MINIMUM_SIZE",
        "",
        "lib_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field private static final EMPTY_ARRAY:[I

.field private static final MINIMUM_SIZE:I = 0x10


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [I

    .line 83
    sput-object v0, Lcom/squareup/cycler/MutationMapKt;->EMPTY_ARRAY:[I

    return-void
.end method

.method public static final synthetic access$getEMPTY_ARRAY$p()[I
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/cycler/MutationMapKt;->EMPTY_ARRAY:[I

    return-object v0
.end method
