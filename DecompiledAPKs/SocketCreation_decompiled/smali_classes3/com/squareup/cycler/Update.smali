.class public final Lcom/squareup/cycler/Update;
.super Ljava/lang/Object;
.source "Update.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUpdate.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Update.kt\ncom/squareup/cycler/Update\n+ 2 Delegates.kt\nkotlin/properties/Delegates\n*L\n1#1,166:1\n33#2,3:167\n*E\n*S KotlinDebug\n*F\n+ 1 Update.kt\ncom/squareup/cycler/Update\n*L\n39#1,3:167\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0002B\u0015\u0008\u0016\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0000\u00a2\u0006\u0002\u0010\u0004B\u0013\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006\u00a2\u0006\u0002\u0010\u0007J\u0014\u0010(\u001a\u00020\"2\u000c\u0010)\u001a\u0008\u0012\u0004\u0012\u00028\u00000\nJ&\u0010*\u001a\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030,\u0012\u0004\u0012\u00020\"0+2\u000c\u0010-\u001a\u0008\u0012\u0004\u0012\u00028\u00000.H\u0002J\u000e\u0010/\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000cH\u0002J\u001a\u00100\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00062\u000c\u00101\u001a\u0008\u0012\u0004\u0012\u00028\u000002JA\u00103\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030,\u0012\u0004\u0012\u00020\"0+0\n2\u000e\u0010-\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010.2\u0006\u00104\u001a\u000205H\u0080@\u00f8\u0001\u0000\u00a2\u0006\u0004\u00086\u00107J\u0018\u00108\u001a\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030,\u0012\u0004\u0012\u00020\"0+H\u0002J\u0014\u0010#\u001a\u00020\"2\u000c\u00109\u001a\u0008\u0012\u0004\u0012\u00020\"0!J\u0014\u0010&\u001a\u00020\"2\u000c\u00109\u001a\u0008\u0012\u0004\u0012\u00020\"0!R\u001a\u0010\u0008\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R7\u0010\r\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000c2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000c8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0012\u0010\u0013\u001a\u0004\u0008\u000e\u0010\u000f\"\u0004\u0008\u0010\u0010\u0011R\u0014\u0010\u0014\u001a\u00020\u00158BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u00158BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u0017R\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u0002X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001b\u0010\u001c\"\u0004\u0008\u001d\u0010\u001eR\u001a\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000c8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010\u000fR\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\"0!2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\"0!@BX\u0080\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010%R*\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\"0!2\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\"0!@BX\u0080\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010%\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006:"
    }
    d2 = {
        "Lcom/squareup/cycler/Update;",
        "I",
        "",
        "otherUpdate",
        "(Lcom/squareup/cycler/Update;)V",
        "oldRecyclerData",
        "Lcom/squareup/cycler/RecyclerData;",
        "(Lcom/squareup/cycler/RecyclerData;)V",
        "addedChunks",
        "",
        "",
        "<set-?>",
        "Lcom/squareup/cycler/DataSource;",
        "data",
        "getData",
        "()Lcom/squareup/cycler/DataSource;",
        "setData",
        "(Lcom/squareup/cycler/DataSource;)V",
        "data$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "dataAdded",
        "",
        "getDataAdded",
        "()Z",
        "dataReplaced",
        "getDataReplaced",
        "extraItem",
        "getExtraItem",
        "()Ljava/lang/Object;",
        "setExtraItem",
        "(Ljava/lang/Object;)V",
        "newData",
        "getNewData",
        "Lkotlin/Function0;",
        "",
        "onCancelled",
        "getOnCancelled$lib_release",
        "()Lkotlin/jvm/functions/Function0;",
        "onReady",
        "getOnReady$lib_release",
        "addChunk",
        "chunk",
        "calculateDataChanges",
        "Lkotlin/Function1;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "itemComparator",
        "Lcom/squareup/cycler/ItemComparator;",
        "concatenateAddedChunks",
        "createNewRecyclerData",
        "config",
        "Lcom/squareup/cycler/Recycler$Config;",
        "generateDataChangesLambdas",
        "backgroundContext",
        "Lkotlin/coroutines/CoroutineContext;",
        "generateDataChangesLambdas$lib_release",
        "(Lcom/squareup/cycler/ItemComparator;Lkotlin/coroutines/CoroutineContext;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;",
        "notifyChangesExtraItem",
        "block",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final addedChunks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "TI;>;>;"
        }
    .end annotation
.end field

.field private final data$delegate:Lkotlin/properties/ReadWriteProperty;

.field private extraItem:Ljava/lang/Object;

.field private final oldRecyclerData:Lcom/squareup/cycler/RecyclerData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;"
        }
    .end annotation
.end field

.field private onCancelled:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onReady:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/cycler/Update;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "data"

    const-string v4, "getData()Lcom/squareup/cycler/DataSource;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cycler/Update;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cycler/RecyclerData;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;)V"
        }
    .end annotation

    const-string v0, "oldRecyclerData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cycler/Update;->oldRecyclerData:Lcom/squareup/cycler/RecyclerData;

    .line 35
    iget-object p1, p0, Lcom/squareup/cycler/Update;->oldRecyclerData:Lcom/squareup/cycler/RecyclerData;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/RecyclerData;->setFrozen$lib_release(Z)V

    .line 39
    sget-object p1, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    .line 40
    iget-object p1, p0, Lcom/squareup/cycler/Update;->oldRecyclerData:Lcom/squareup/cycler/RecyclerData;

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object p1

    .line 167
    new-instance v0, Lcom/squareup/cycler/Update$$special$$inlined$observable$1;

    invoke-direct {v0, p1, p1, p0}, Lcom/squareup/cycler/Update$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/cycler/Update;)V

    check-cast v0, Lkotlin/properties/ReadWriteProperty;

    .line 169
    iput-object v0, p0, Lcom/squareup/cycler/Update;->data$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 42
    iget-object p1, p0, Lcom/squareup/cycler/Update;->oldRecyclerData:Lcom/squareup/cycler/RecyclerData;

    invoke-virtual {p1}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cycler/Update;->extraItem:Ljava/lang/Object;

    .line 44
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/cycler/Update;->addedChunks:Ljava/util/List;

    .line 62
    sget-object p1, Lcom/squareup/cycler/Update$onReady$1;->INSTANCE:Lcom/squareup/cycler/Update$onReady$1;

    check-cast p1, Lkotlin/jvm/functions/Function0;

    iput-object p1, p0, Lcom/squareup/cycler/Update;->onReady:Lkotlin/jvm/functions/Function0;

    .line 64
    sget-object p1, Lcom/squareup/cycler/Update$onCancelled$1;->INSTANCE:Lcom/squareup/cycler/Update$onCancelled$1;

    check-cast p1, Lkotlin/jvm/functions/Function0;

    iput-object p1, p0, Lcom/squareup/cycler/Update;->onCancelled:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cycler/Update;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Update<",
            "TI;>;)V"
        }
    .end annotation

    const-string v0, "otherUpdate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    iget-object v0, p1, Lcom/squareup/cycler/Update;->oldRecyclerData:Lcom/squareup/cycler/RecyclerData;

    invoke-direct {p0, v0}, Lcom/squareup/cycler/Update;-><init>(Lcom/squareup/cycler/RecyclerData;)V

    .line 51
    invoke-virtual {p1}, Lcom/squareup/cycler/Update;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/Update;->setData(Lcom/squareup/cycler/DataSource;)V

    .line 52
    iget-object v0, p1, Lcom/squareup/cycler/Update;->extraItem:Ljava/lang/Object;

    iput-object v0, p0, Lcom/squareup/cycler/Update;->extraItem:Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lcom/squareup/cycler/Update;->addedChunks:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/cycler/Update;->addedChunks:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public static final synthetic access$calculateDataChanges(Lcom/squareup/cycler/Update;Lcom/squareup/cycler/ItemComparator;)Lkotlin/jvm/functions/Function1;
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/cycler/Update;->calculateDataChanges(Lcom/squareup/cycler/ItemComparator;)Lkotlin/jvm/functions/Function1;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getAddedChunks$p(Lcom/squareup/cycler/Update;)Ljava/util/List;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/cycler/Update;->addedChunks:Ljava/util/List;

    return-object p0
.end method

.method public static final synthetic access$getOldRecyclerData$p(Lcom/squareup/cycler/Update;)Lcom/squareup/cycler/RecyclerData;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/cycler/Update;->oldRecyclerData:Lcom/squareup/cycler/RecyclerData;

    return-object p0
.end method

.method private final calculateDataChanges(Lcom/squareup/cycler/ItemComparator;)Lkotlin/jvm/functions/Function1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/ItemComparator<",
            "-TI;>;)",
            "Lkotlin/jvm/functions/Function1<",
            "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
            "*>;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 123
    new-instance v0, Lcom/squareup/cycler/DataSourceDiff;

    .line 124
    iget-object v1, p0, Lcom/squareup/cycler/Update;->oldRecyclerData:Lcom/squareup/cycler/RecyclerData;

    invoke-virtual {v1}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/cycler/Update;->getNewData()Lcom/squareup/cycler/DataSource;

    move-result-object v2

    .line 123
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/cycler/DataSourceDiff;-><init>(Lcom/squareup/cycler/ItemComparator;Lcom/squareup/cycler/DataSource;Lcom/squareup/cycler/DataSource;)V

    .line 127
    check-cast v0, Landroidx/recyclerview/widget/DiffUtil$Callback;

    invoke-static {v0}, Landroidx/recyclerview/widget/DiffUtil;->calculateDiff(Landroidx/recyclerview/widget/DiffUtil$Callback;)Landroidx/recyclerview/widget/DiffUtil$DiffResult;

    move-result-object p1

    const-string v0, "DiffUtil.calculateDiff(callback)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    new-instance v0, Lcom/squareup/cycler/Update$calculateDataChanges$1;

    invoke-direct {v0, p1}, Lcom/squareup/cycler/Update$calculateDataChanges$1;-><init>(Landroidx/recyclerview/widget/DiffUtil$DiffResult;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method private final concatenateAddedChunks()Lcom/squareup/cycler/DataSource;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "TI;>;"
        }
    .end annotation

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 162
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {p0}, Lcom/squareup/cycler/Update;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/cycler/DataSource;->getSize()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v3, v2}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v2

    new-instance v3, Lcom/squareup/cycler/Update$concatenateAddedChunks$1$1;

    invoke-virtual {p0}, Lcom/squareup/cycler/Update;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/squareup/cycler/Update$concatenateAddedChunks$1$1;-><init>(Lcom/squareup/cycler/DataSource;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, v3}, Lkotlin/sequences/SequencesKt;->map(Lkotlin/sequences/Sequence;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Lkotlin/sequences/Sequence;)Z

    .line 163
    iget-object v2, p0, Lcom/squareup/cycler/Update;->addedChunks:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object v2

    invoke-static {v2}, Lkotlin/sequences/SequencesKt;->flattenSequenceOfIterable(Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Lkotlin/sequences/Sequence;)Z

    .line 164
    invoke-static {v0}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    return-object v0
.end method

.method private final getDataAdded()Z
    .locals 2

    .line 46
    invoke-direct {p0}, Lcom/squareup/cycler/Update;->getDataReplaced()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cycler/Update;->addedChunks:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private final getDataReplaced()Z
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/cycler/Update;->oldRecyclerData:Lcom/squareup/cycler/RecyclerData;

    invoke-virtual {v0}, Lcom/squareup/cycler/RecyclerData;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/cycler/Update;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private final getNewData()Lcom/squareup/cycler/DataSource;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "TI;>;"
        }
    .end annotation

    .line 47
    invoke-virtual {p0}, Lcom/squareup/cycler/Update;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    return-object v0
.end method

.method private final notifyChangesExtraItem()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
            "*>;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 131
    new-instance v0, Lcom/squareup/cycler/Update$notifyChangesExtraItem$1;

    invoke-direct {v0, p0}, Lcom/squareup/cycler/Update$notifyChangesExtraItem$1;-><init>(Lcom/squareup/cycler/Update;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    return-object v0
.end method


# virtual methods
.method public final addChunk(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TI;>;)V"
        }
    .end annotation

    const-string v0, "chunk"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/squareup/cycler/Update;->addedChunks:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public final createNewRecyclerData(Lcom/squareup/cycler/Recycler$Config;)Lcom/squareup/cycler/RecyclerData;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;)",
            "Lcom/squareup/cycler/RecyclerData<",
            "TI;>;"
        }
    .end annotation

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    invoke-direct {p0}, Lcom/squareup/cycler/Update;->getDataAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/cycler/Update;->concatenateAddedChunks()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    goto :goto_0

    .line 148
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cycler/Update;->getData()Lcom/squareup/cycler/DataSource;

    move-result-object v0

    .line 150
    :goto_0
    new-instance v1, Lcom/squareup/cycler/RecyclerData;

    iget-object v2, p0, Lcom/squareup/cycler/Update;->extraItem:Ljava/lang/Object;

    invoke-direct {v1, p1, v0, v2}, Lcom/squareup/cycler/RecyclerData;-><init>(Lcom/squareup/cycler/Recycler$Config;Lcom/squareup/cycler/DataSource;Ljava/lang/Object;)V

    return-object v1
.end method

.method public final generateDataChangesLambdas$lib_release(Lcom/squareup/cycler/ItemComparator;Lkotlin/coroutines/CoroutineContext;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/ItemComparator<",
            "-TI;>;",
            "Lkotlin/coroutines/CoroutineContext;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/util/List<",
            "+",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
            "*>;",
            "Lkotlin/Unit;",
            ">;>;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    instance-of v0, p3, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;

    if-eqz v0, :cond_0

    move-object v0, p3

    check-cast v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;

    iget v1, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->label:I

    const/high16 v2, -0x80000000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    iget p3, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->label:I

    sub-int/2addr p3, v2

    iput p3, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->label:I

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;

    invoke-direct {v0, p0, p3}, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;-><init>(Lcom/squareup/cycler/Update;Lkotlin/coroutines/Continuation;)V

    :goto_0
    iget-object p3, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->result:Ljava/lang/Object;

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v1

    .line 75
    iget v2, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->label:I

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    if-ne v2, v3, :cond_1

    iget-object p1, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->L$4:Ljava/lang/Object;

    check-cast p1, Ljava/util/Collection;

    iget-object p2, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->L$3:Ljava/lang/Object;

    check-cast p2, Ljava/util/List;

    iget-object v1, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->L$2:Ljava/lang/Object;

    check-cast v1, Lkotlin/coroutines/CoroutineContext;

    iget-object v1, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->L$1:Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cycler/ItemComparator;

    iget-object v0, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->L$0:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/cycler/Update;

    invoke-static {p3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v4, p2

    move-object v6, p3

    move-object p3, p1

    move-object p1, v6

    goto :goto_2

    .line 112
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 75
    :cond_2
    invoke-static {p3}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    .line 80
    iget-object p3, p0, Lcom/squareup/cycler/Update;->oldRecyclerData:Lcom/squareup/cycler/RecyclerData;

    invoke-virtual {p3}, Lcom/squareup/cycler/RecyclerData;->getExtraItem()Ljava/lang/Object;

    move-result-object p3

    iget-object v2, p0, Lcom/squareup/cycler/Update;->extraItem:Ljava/lang/Object;

    invoke-static {p3, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p3

    xor-int/2addr p3, v3

    .line 81
    invoke-direct {p0}, Lcom/squareup/cycler/Update;->getDataReplaced()Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez p1, :cond_3

    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    .line 83
    :goto_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    check-cast v4, Ljava/util/List;

    if-eqz v2, :cond_4

    .line 86
    move-object p1, v4

    check-cast p1, Ljava/util/Collection;

    sget-object p2, Lcom/squareup/cycler/Update$generateDataChangesLambdas$2;->INSTANCE:Lcom/squareup/cycler/Update$generateDataChangesLambdas$2;

    invoke-interface {p1, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    if-eqz p3, :cond_5

    .line 91
    move-object p3, v4

    check-cast p3, Ljava/util/Collection;

    invoke-direct {p0}, Lcom/squareup/cycler/Update;->notifyChangesExtraItem()Lkotlin/jvm/functions/Function1;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_5
    invoke-direct {p0}, Lcom/squareup/cycler/Update;->getDataReplaced()Z

    move-result p3

    if-eqz p3, :cond_7

    .line 98
    move-object p3, v4

    check-cast p3, Ljava/util/Collection;

    new-instance v2, Lcom/squareup/cycler/Update$generateDataChangesLambdas$3;

    const/4 v5, 0x0

    invoke-direct {v2, p0, p1, v5}, Lcom/squareup/cycler/Update$generateDataChangesLambdas$3;-><init>(Lcom/squareup/cycler/Update;Lcom/squareup/cycler/ItemComparator;Lkotlin/coroutines/Continuation;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    iput-object p0, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->L$0:Ljava/lang/Object;

    iput-object p1, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->L$1:Ljava/lang/Object;

    iput-object p2, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->L$2:Ljava/lang/Object;

    iput-object v4, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->L$3:Ljava/lang/Object;

    iput-object p3, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->L$4:Ljava/lang/Object;

    iput v3, v0, Lcom/squareup/cycler/Update$generateDataChangesLambdas$1;->label:I

    invoke-static {p2, v2, v0}, Lkotlinx/coroutines/BuildersKt;->withContext(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v1, :cond_6

    return-object v1

    .line 75
    :cond_6
    :goto_2
    invoke-interface {p3, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 102
    :cond_7
    invoke-direct {p0}, Lcom/squareup/cycler/Update;->getDataAdded()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 103
    move-object p1, v4

    check-cast p1, Ljava/util/Collection;

    new-instance p2, Lcom/squareup/cycler/Update$generateDataChangesLambdas$4;

    invoke-direct {p2, p0}, Lcom/squareup/cycler/Update$generateDataChangesLambdas$4;-><init>(Lcom/squareup/cycler/Update;)V

    invoke-interface {p1, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_8
    :goto_3
    return-object v4
.end method

.method public final getData()Lcom/squareup/cycler/DataSource;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/DataSource<",
            "TI;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/cycler/Update;->data$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/cycler/Update;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cycler/DataSource;

    return-object v0
.end method

.method public final getExtraItem()Ljava/lang/Object;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/cycler/Update;->extraItem:Ljava/lang/Object;

    return-object v0
.end method

.method public final getOnCancelled$lib_release()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/squareup/cycler/Update;->onCancelled:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getOnReady$lib_release()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/cycler/Update;->onReady:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final onCancelled(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iput-object p1, p0, Lcom/squareup/cycler/Update;->onCancelled:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final onReady(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iput-object p1, p0, Lcom/squareup/cycler/Update;->onReady:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public final setData(Lcom/squareup/cycler/DataSource;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/DataSource<",
            "+TI;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/cycler/Update;->data$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/cycler/Update;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setExtraItem(Ljava/lang/Object;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/cycler/Update;->extraItem:Ljava/lang/Object;

    return-void
.end method
