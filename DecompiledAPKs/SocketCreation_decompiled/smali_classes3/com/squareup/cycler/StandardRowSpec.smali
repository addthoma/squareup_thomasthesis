.class public final Lcom/squareup/cycler/StandardRowSpec;
.super Lcom/squareup/cycler/Recycler$RowSpec;
.source "StandardRowSpec.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cycler/StandardRowSpec$Creator;,
        Lcom/squareup/cycler/StandardRowSpec$StandardViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<I:",
        "Ljava/lang/Object;",
        "S::TI;V:",
        "Landroid/view/View;",
        ">",
        "Lcom/squareup/cycler/Recycler$RowSpec<",
        "TI;TS;TV;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStandardRowSpec.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n*L\n1#1,87:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u0002H\u0001*\u0008\u0008\u0002\u0010\u0004*\u00020\u00052\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0006:\u0002\u001a\u001bB\u001b\u0008\u0001\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0002\u0010\nJ7\u0010\u0011\u001a\u00020\u000f2/\u0010\u0012\u001a+\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\r\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\u000c\u00a2\u0006\u0002\u0008\u0010J@\u0010\u0011\u001a\u00020\u000f2\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u00142+\u0008\u0004\u0010\u0012\u001a%\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\r\u0012\u0004\u0012\u00020\u000f0\u0008\u00a2\u0006\u0002\u0008\u0010H\u0086\u0008J\u001e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00028\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0014H\u0016R7\u0010\u000b\u001a+\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\r\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\u000c\u00a2\u0006\u0002\u0008\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/cycler/StandardRowSpec;",
        "I",
        "",
        "S",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/cycler/Recycler$RowSpec;",
        "typeMatchBlock",
        "Lkotlin/Function1;",
        "",
        "(Lkotlin/jvm/functions/Function1;)V",
        "createBlock",
        "Lkotlin/Function2;",
        "Lcom/squareup/cycler/StandardRowSpec$Creator;",
        "Landroid/content/Context;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "create",
        "block",
        "layoutId",
        "",
        "createViewHolder",
        "Lcom/squareup/cycler/Recycler$ViewHolder;",
        "creatorContext",
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "subType",
        "Creator",
        "StandardViewHolder",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private createBlock:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "TI;TS;TV;>;-",
            "Landroid/content/Context;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TI;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo v0, "typeMatchBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/cycler/Recycler$RowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public final create(ILkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "TI;TS;TV;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec$create$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/cycler/StandardRowSpec$create$1;-><init>(ILkotlin/jvm/functions/Function1;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p0, v0}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method public final create(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/cycler/StandardRowSpec$Creator<",
            "TI;TS;TV;>;-",
            "Landroid/content/Context;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iput-object p1, p0, Lcom/squareup/cycler/StandardRowSpec;->createBlock:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public createViewHolder(Lcom/squareup/cycler/Recycler$CreatorContext;I)Lcom/squareup/cycler/Recycler$ViewHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler$CreatorContext;",
            "I)",
            "Lcom/squareup/cycler/Recycler$ViewHolder<",
            "TV;>;"
        }
    .end annotation

    const-string v0, "creatorContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    if-eqz p2, :cond_3

    .line 46
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec$Creator;

    invoke-direct {p2, p1}, Lcom/squareup/cycler/StandardRowSpec$Creator;-><init>(Lcom/squareup/cycler/Recycler$CreatorContext;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/cycler/StandardRowSpec;->createBlock:Lkotlin/jvm/functions/Function2;

    if-nez v0, :cond_1

    const-string v1, "createBlock"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler$CreatorContext;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {v0, p2, p1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    invoke-virtual {p2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getView()Landroid/view/View;

    move-result-object p1

    .line 51
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_2

    .line 52
    invoke-static {}, Lcom/squareup/cycler/UtilsKt;->createItemLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 53
    :cond_2
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec$StandardViewHolder;

    invoke-virtual {p2}, Lcom/squareup/cycler/StandardRowSpec$Creator;->getBindBlock$lib_release()Lkotlin/jvm/functions/Function2;

    move-result-object p2

    invoke-direct {v0, p1, p2}, Lcom/squareup/cycler/StandardRowSpec$StandardViewHolder;-><init>(Landroid/view/View;Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lcom/squareup/cycler/Recycler$ViewHolder;

    return-object v0

    .line 44
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Expected subType == 0 ("

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class p2, Lcom/squareup/cycler/StandardRowSpec;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, " doesn\'t use subTypes)."

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 43
    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method
