.class public final Lcom/squareup/cycler/Recycler$CreatorContext;
.super Ljava/lang/Object;
.source "Recycler.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cycler/Recycler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreatorContext"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecycler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Recycler.kt\ncom/squareup/cycler/Recycler$CreatorContext\n+ 2 Recycler.kt\ncom/squareup/cycler/Recycler\n*L\n1#1,606:1\n112#2:607\n*E\n*S KotlinDebug\n*F\n+ 1 Recycler.kt\ncom/squareup/cycler/Recycler$CreatorContext\n*L\n131#1:607\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u0011\u0012\n\u0010\u0002\u001a\u0006\u0012\u0002\u0008\u00030\u0003\u00a2\u0006\u0002\u0010\u0004J\u001c\u0010\u000b\u001a\u0004\u0018\u0001H\u000c\"\n\u0008\u0001\u0010\u000c\u0018\u0001*\u00020\u0001H\u0086\u0008\u00a2\u0006\u0002\u0010\rR\u0011\u0010\u0005\u001a\u00020\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0015\u0010\u0002\u001a\u0006\u0012\u0002\u0008\u00030\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/cycler/Recycler$CreatorContext;",
        "",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "(Lcom/squareup/cycler/Recycler;)V",
        "context",
        "Landroid/content/Context;",
        "getContext",
        "()Landroid/content/Context;",
        "getRecycler",
        "()Lcom/squareup/cycler/Recycler;",
        "extension",
        "T",
        "()Ljava/lang/Object;",
        "lib_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xf
    }
.end annotation


# instance fields
.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cycler/Recycler;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler<",
            "*>;)V"
        }
    .end annotation

    const-string v0, "recycler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cycler/Recycler$CreatorContext;->recycler:Lcom/squareup/cycler/Recycler;

    return-void
.end method


# virtual methods
.method public final synthetic extension()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()TT;"
        }
    .end annotation

    .line 131
    invoke-virtual {p0}, Lcom/squareup/cycler/Recycler$CreatorContext;->getRecycler()Lcom/squareup/cycler/Recycler;

    move-result-object v0

    const/4 v1, 0x4

    const-string v2, "T"

    .line 607
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler;->extension(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$CreatorContext;->recycler:Lcom/squareup/cycler/Recycler;

    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "recycler.view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getRecycler()Lcom/squareup/cycler/Recycler;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/cycler/Recycler<",
            "*>;"
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/squareup/cycler/Recycler$CreatorContext;->recycler:Lcom/squareup/cycler/Recycler;

    return-object v0
.end method
