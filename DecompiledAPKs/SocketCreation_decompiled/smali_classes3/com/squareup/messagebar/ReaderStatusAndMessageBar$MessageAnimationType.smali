.class final enum Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;
.super Ljava/lang/Enum;
.source "ReaderStatusAndMessageBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/messagebar/ReaderStatusAndMessageBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "MessageAnimationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

.field public static final enum CYCLE_IN_FROM_TOP:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

.field public static final enum IGNORE_MESSAGE:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

.field public static final enum SHOW_IMMEDIATELY:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 699
    new-instance v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    const/4 v1, 0x0

    const-string v2, "IGNORE_MESSAGE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->IGNORE_MESSAGE:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    .line 700
    new-instance v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    const/4 v2, 0x1

    const-string v3, "SHOW_IMMEDIATELY"

    invoke-direct {v0, v3, v2}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->SHOW_IMMEDIATELY:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    .line 701
    new-instance v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    const/4 v3, 0x2

    const-string v4, "CYCLE_IN_FROM_TOP"

    invoke-direct {v0, v4, v3}, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->CYCLE_IN_FROM_TOP:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    .line 698
    sget-object v4, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->IGNORE_MESSAGE:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->SHOW_IMMEDIATELY:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->CYCLE_IN_FROM_TOP:Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->$VALUES:[Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 698
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;
    .locals 1

    .line 698
    const-class v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;
    .locals 1

    .line 698
    sget-object v0, Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->$VALUES:[Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    invoke-virtual {v0}, [Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/messagebar/ReaderStatusAndMessageBar$MessageAnimationType;

    return-object v0
.end method
