.class public Lcom/squareup/datafetch/Rx1AbstractLoader$Results;
.super Ljava/lang/Object;
.source "Rx1AbstractLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/datafetch/Rx1AbstractLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Results"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TInput:",
        "Ljava/lang/Object;",
        "TItem:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final hasMore:Z

.field public final input:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTInput;"
        }
    .end annotation
.end field

.field public final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/List<",
            "TTItem;>;>;"
        }
    .end annotation
.end field

.field private lazyItemCount:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "TTItem;>;>;Z)V"
        }
    .end annotation

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->input:Ljava/lang/Object;

    .line 119
    iput-object p2, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    .line 120
    iput-boolean p3, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->hasMore:Z

    return-void
.end method


# virtual methods
.method public getItemAt(I)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTItem;"
        }
    .end annotation

    .line 142
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 143
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ge p1, v2, :cond_0

    .line 144
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    .line 146
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr p1, v1

    goto :goto_0

    .line 148
    :cond_1
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {p1}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw p1
.end method

.method public hasItems()Z
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public isFirstPage()Z
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public itemCount()I
    .locals 3

    .line 132
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->lazyItemCount:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 133
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->lazyItemCount:Ljava/lang/Integer;

    .line 134
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 135
    iget-object v2, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->lazyItemCount:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->lazyItemCount:Ljava/lang/Integer;

    goto :goto_0

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->lazyItemCount:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method
