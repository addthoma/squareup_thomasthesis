.class public Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;
.super Ljava/lang/Object;
.source "Rx1AbstractLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/datafetch/Rx1AbstractLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Progress"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TInput:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final input:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTInput;"
        }
    .end annotation
.end field

.field public final isFirstPage:Z


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTInput;",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;",
            ")V"
        }
    .end annotation

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iput-object p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->input:Ljava/lang/Object;

    .line 178
    iget-object p1, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$PagingParams;->pagingKey:Ljava/lang/String;

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->isFirstPage:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 183
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 185
    :cond_1
    check-cast p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;

    .line 186
    iget-object v2, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->input:Ljava/lang/Object;

    iget-object v3, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->input:Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->isFirstPage:Z

    iget-boolean p1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->isFirstPage:Z

    if-ne v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 196
    iget-object v1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->input:Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->isFirstPage:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Progress{input=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->input:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\', isFirstPage=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;->isFirstPage:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
