.class public Lcom/squareup/encryption/AesGcmKeystoreEncryptor;
.super Ljava/lang/Object;
.source "AesGcmKeystoreEncryptor.java"

# interfaces
.implements Lcom/squareup/encryption/KeystoreEncryptor;


# static fields
.field private static final AES_GCM_IV_LEN:I = 0xc

.field private static final AES_GCM_NO_PADDING:Ljava/lang/String; = "AES/GCM/NoPadding"

.field private static final AUTH_TAG_LEN_BITS:I = 0x80


# instance fields
.field private cipher:Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;

.field private secretKey:Ljavax/crypto/SecretKey;


# direct methods
.method public constructor <init>(Lcom/squareup/encryption/AesGcmSecretKeyProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/UnrecoverableEntryException;,
            Ljava/security/InvalidAlgorithmParameterException;,
            Ljavax/crypto/NoSuchPaddingException;
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iget-object v0, p0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;->cipher:Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;->secretKey:Ljavax/crypto/SecretKey;

    if-nez v0, :cond_1

    .line 41
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/encryption/AesGcmSecretKeyProvider;->get()Ljavax/crypto/SecretKey;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;->secretKey:Ljavax/crypto/SecretKey;

    .line 42
    new-instance p1, Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;

    const-string v0, "AES/GCM/NoPadding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;-><init>(Ljavax/crypto/Cipher;)V

    iput-object p1, p0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;->cipher:Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;

    :cond_1
    return-void
.end method


# virtual methods
.method public decrypt([B)[B
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/BadPaddingException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljava/security/InvalidKeyException;,
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    .line 94
    array-length v0, p1

    const/16 v1, 0xc

    if-ge v0, v1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 97
    :cond_0
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object p1

    new-array v0, v1, [B

    const/4 v2, 0x0

    .line 99
    invoke-virtual {p1, v0, v2, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 100
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    new-array v1, v1, [B

    .line 101
    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 103
    new-instance p1, Ljavax/crypto/spec/GCMParameterSpec;

    const/16 v2, 0x80

    invoke-direct {p1, v2, v0}, Ljavax/crypto/spec/GCMParameterSpec;-><init>(I[B)V

    .line 104
    iget-object v0, p0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;->cipher:Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;->secretKey:Ljavax/crypto/SecretKey;

    invoke-virtual {v0, v2, v3, p1}, Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;->cipher:Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;

    invoke-virtual {p1, v1}, Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;->doFinal([B)[B

    move-result-object p1

    return-object p1
.end method

.method public encrypt([B)[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/BadPaddingException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;->cipher:Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    .line 66
    iget-object v2, p0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;->secretKey:Ljavax/crypto/SecretKey;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;->init(ILjava/security/Key;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;->cipher:Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;

    invoke-virtual {v0}, Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;->getIV()[B

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;->cipher:Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;

    invoke-virtual {v1, p1}, Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;->doFinal([B)[B

    move-result-object p1

    .line 72
    array-length v1, v0

    array-length v2, p1

    add-int/2addr v1, v2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 73
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 74
    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 76
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object p1

    return-object p1

    .line 58
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Not initialized: call cipherInit()"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public testOverrideCipherDelegator(Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;)V
    .locals 0

    .line 110
    iput-object p1, p0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;->cipher:Lcom/squareup/encryption/KeystoreEncryptor$CipherDelegator;

    return-void
.end method
