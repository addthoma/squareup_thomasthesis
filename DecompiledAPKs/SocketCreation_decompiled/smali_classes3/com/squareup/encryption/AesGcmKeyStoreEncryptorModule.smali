.class public Lcom/squareup/encryption/AesGcmKeyStoreEncryptorModule;
.super Ljava/lang/Object;
.source "AesGcmKeyStoreEncryptorModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# static fields
.field private static final ANDROID_KEY_STORE:Ljava/lang/String; = "AndroidKeyStore"

.field private static final BLACKLISTED_MANUFACTURER_SUBSTRINGS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final KEY_ALIAS:Ljava/lang/String; = "CacheKey"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "huawei"

    const-string v2, "square"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    .line 25
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/squareup/encryption/AesGcmKeyStoreEncryptorModule;->BLACKLISTED_MANUFACTURER_SUBSTRINGS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isEmulator()Z
    .locals 3

    .line 34
    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    const-string v1, "generic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "google_sdk"

    .line 35
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "Emulator"

    .line 36
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "Android SDK built for x86"

    .line 37
    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    .line 38
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method static provideEncryptor()Lcom/squareup/encryption/KeystoreEncryptor;
    .locals 4
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 44
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_3

    invoke-static {}, Lcom/squareup/encryption/AesGcmKeyStoreEncryptorModule;->isEmulator()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 49
    :cond_0
    sget-object v0, Lcom/squareup/encryption/AesGcmKeyStoreEncryptorModule;->BLACKLISTED_MANUFACTURER_SUBSTRINGS:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 50
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 51
    new-instance v0, Lcom/squareup/encryption/NoKeystoreEncryptor;

    invoke-direct {v0}, Lcom/squareup/encryption/NoKeystoreEncryptor;-><init>()V

    return-object v0

    .line 55
    :cond_2
    new-instance v0, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;

    new-instance v1, Lcom/squareup/encryption/AesGcmSecretKeyProvider;

    const-string v2, "AndroidKeyStore"

    const-string v3, "CacheKey"

    invoke-direct {v1, v2, v3}, Lcom/squareup/encryption/AesGcmSecretKeyProvider;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/encryption/AesGcmKeystoreEncryptor;-><init>(Lcom/squareup/encryption/AesGcmSecretKeyProvider;)V

    return-object v0

    .line 45
    :cond_3
    :goto_0
    new-instance v0, Lcom/squareup/encryption/NoKeystoreEncryptor;

    invoke-direct {v0}, Lcom/squareup/encryption/NoKeystoreEncryptor;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "Failed to initialize an AesGcmKeystoreEncryptor"

    .line 58
    invoke-static {v0, v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/squareup/encryption/NoKeystoreEncryptor;

    invoke-direct {v0}, Lcom/squareup/encryption/NoKeystoreEncryptor;-><init>()V

    return-object v0
.end method
