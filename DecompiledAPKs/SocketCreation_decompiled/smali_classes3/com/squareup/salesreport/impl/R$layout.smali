.class public final Lcom/squareup/salesreport/impl/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final amount_cell:I = 0x7f0d0054

.field public static final comparison_range_row:I = 0x7f0d00ee

.field public static final customize_report_view:I = 0x7f0d01ae

.field public static final email_report_view:I = 0x7f0d0245

.field public static final employee_selection_view:I = 0x7f0d024e

.field public static final export_report_dialog:I = 0x7f0d0268

.field public static final noho_action_bar_second_action_icon:I = 0x7f0d038a

.field public static final noho_action_bar_with_two_action_icons:I = 0x7f0d038b

.field public static final popup_actions_view:I = 0x7f0d044b

.field public static final print_report_view:I = 0x7f0d0454

.field public static final sales_report_chart_view:I = 0x7f0d0495

.field public static final sales_report_fee_row:I = 0x7f0d049b

.field public static final sales_report_payment_method_row:I = 0x7f0d049f

.field public static final sales_report_payment_method_total_row:I = 0x7f0d04a0

.field public static final sales_report_recycler_view_space:I = 0x7f0d04a1

.field public static final sales_report_sales_details_row:I = 0x7f0d04a2

.field public static final sales_report_section_header:I = 0x7f0d04a7

.field public static final sales_report_section_header_with_image:I = 0x7f0d04a8

.field public static final sales_report_subsection_header:I = 0x7f0d04a9

.field public static final sales_report_time_selector_view:I = 0x7f0d04aa

.field public static final sales_report_top_bar_view:I = 0x7f0d04ab

.field public static final sales_report_two_col_payment_method_row:I = 0x7f0d04ac

.field public static final sales_report_two_col_sales_details_row:I = 0x7f0d04ad

.field public static final sales_report_two_toggle_row:I = 0x7f0d04ae

.field public static final sales_report_v2_overview:I = 0x7f0d04af

.field public static final sales_report_view:I = 0x7f0d04b0


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
