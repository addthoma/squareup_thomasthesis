.class public final Lcom/squareup/salesreport/analytics/RealSalesReportAnalyticsKt;
.super Ljava/lang/Object;
.source "RealSalesReportAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "eventValue",
        "",
        "Lcom/squareup/customreport/data/ComparisonRange;",
        "getEventValue",
        "(Lcom/squareup/customreport/data/ComparisonRange;)Ljava/lang/String;",
        "Lcom/squareup/customreport/data/RangeSelection;",
        "(Lcom/squareup/customreport/data/RangeSelection;)Ljava/lang/String;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getEventValue$p(Lcom/squareup/customreport/data/ComparisonRange;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/salesreport/analytics/RealSalesReportAnalyticsKt;->getEventValue(Lcom/squareup/customreport/data/ComparisonRange;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getEventValue$p(Lcom/squareup/customreport/data/RangeSelection;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/salesreport/analytics/RealSalesReportAnalyticsKt;->getEventValue(Lcom/squareup/customreport/data/RangeSelection;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static final getEventValue(Lcom/squareup/customreport/data/ComparisonRange;)Ljava/lang/String;
    .locals 1

    .line 426
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "same_day_previous_week"

    goto/16 :goto_0

    .line 427
    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p0, "same_day_previous_year"

    goto/16 :goto_0

    .line 428
    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$Yesterday;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$Yesterday;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo p0, "yesterday"

    goto :goto_0

    .line 429
    :cond_2
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousDay;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string p0, "previous_day"

    goto :goto_0

    .line 430
    :cond_3
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousWeek;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string p0, "previous_week"

    goto :goto_0

    .line 431
    :cond_4
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameWeekPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameWeekPreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string p0, "same_week_previous_year"

    goto :goto_0

    .line 432
    :cond_5
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousMonth;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string p0, "previous_month"

    goto :goto_0

    .line 433
    :cond_6
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameMonthPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameMonthPreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string p0, "same_month_previous_year"

    goto :goto_0

    .line 434
    :cond_7
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string p0, "previous_three_months"

    goto :goto_0

    .line 435
    :cond_8
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameThreeMonthsPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameThreeMonthsPreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string p0, "same_three_months_previous_year"

    goto :goto_0

    .line 436
    :cond_9
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string p0, "previous_year"

    goto :goto_0

    .line 437
    :cond_a
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$NoComparison;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$NoComparison;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    const-string p0, "no_comparison"

    :goto_0
    return-object p0

    :cond_b
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final getEventValue(Lcom/squareup/customreport/data/RangeSelection;)Ljava/lang/String;
    .locals 1

    .line 442
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_0
    const-string p0, "one_day"

    goto :goto_4

    .line 443
    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :goto_1
    const-string p0, "one_week"

    goto :goto_4

    .line 444
    :cond_3
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomOneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :goto_2
    const-string p0, "one_month"

    goto :goto_4

    .line 445
    :cond_5
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$ThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    goto :goto_3

    :cond_6
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_3
    const-string/jumbo p0, "three_months"

    goto :goto_4

    .line 446
    :cond_7
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string p0, "one_year"

    goto :goto_4

    .line 447
    :cond_8
    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$CustomRangeSelection$CustomRange;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    const-string p0, "custom_range_selection"

    :goto_4
    return-object p0

    :cond_9
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
