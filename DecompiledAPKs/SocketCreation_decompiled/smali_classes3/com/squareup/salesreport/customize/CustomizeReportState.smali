.class public abstract Lcom/squareup/salesreport/customize/CustomizeReportState;
.super Ljava/lang/Object;
.source "CustomizeReportState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;,
        Lcom/squareup/salesreport/customize/CustomizeReportState$EditingStartTime;,
        Lcom/squareup/salesreport/customize/CustomizeReportState$EditingEndTime;,
        Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0004\u0007\u0008\t\nB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0004\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "Landroid/os/Parcelable;",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "(Lcom/squareup/customreport/data/ReportConfig;)V",
        "getReportConfig",
        "()Lcom/squareup/customreport/data/ReportConfig;",
        "CustomizeAllFields",
        "CustomizeEmployeeSelection",
        "EditingEndTime",
        "EditingStartTime",
        "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState$EditingStartTime;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState$EditingEndTime;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeEmployeeSelection;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final reportConfig:Lcom/squareup/customreport/data/ReportConfig;


# direct methods
.method private constructor <init>(Lcom/squareup/customreport/data/ReportConfig;)V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportState;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/customreport/data/ReportConfig;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1}, Lcom/squareup/salesreport/customize/CustomizeReportState;-><init>(Lcom/squareup/customreport/data/ReportConfig;)V

    return-void
.end method


# virtual methods
.method public getReportConfig()Lcom/squareup/customreport/data/ReportConfig;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportState;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-object v0
.end method
