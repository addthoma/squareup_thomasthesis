.class public final Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;
.super Lcom/squareup/salesreport/customize/CustomizeReportState;
.source "CustomizeReportState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/customize/CustomizeReportState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CustomizeAllFields"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000b\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u000f\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\u0013\u0010\u0012\u001a\u00020\u00052\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\u0019\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0011H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0008R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u0008\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;",
        "Lcom/squareup/salesreport/customize/CustomizeReportState;",
        "reportConfig",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "showEmployeeFilter",
        "",
        "isDatePickerInitialized",
        "(Lcom/squareup/customreport/data/ReportConfig;ZZ)V",
        "()Z",
        "getReportConfig",
        "()Lcom/squareup/customreport/data/ReportConfig;",
        "getShowEmployeeFilter",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final isDatePickerInitialized:Z

.field private final reportConfig:Lcom/squareup/customreport/data/ReportConfig;

.field private final showEmployeeFilter:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields$Creator;

    invoke-direct {v0}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields$Creator;-><init>()V

    sput-object v0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/customreport/data/ReportConfig;ZZ)V
    .locals 1

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, p1, v0}, Lcom/squareup/salesreport/customize/CustomizeReportState;-><init>(Lcom/squareup/customreport/data/ReportConfig;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    iput-boolean p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->showEmployeeFilter:Z

    iput-boolean p3, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->isDatePickerInitialized:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/customreport/data/ReportConfig;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x1

    .line 17
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;-><init>(Lcom/squareup/customreport/data/ReportConfig;ZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;Lcom/squareup/customreport/data/ReportConfig;ZZILjava/lang/Object;)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->showEmployeeFilter:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->isDatePickerInitialized:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->copy(Lcom/squareup/customreport/data/ReportConfig;ZZ)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/customreport/data/ReportConfig;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->showEmployeeFilter:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->isDatePickerInitialized:Z

    return v0
.end method

.method public final copy(Lcom/squareup/customreport/data/ReportConfig;ZZ)Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;
    .locals 1

    const-string v0, "reportConfig"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;-><init>(Lcom/squareup/customreport/data/ReportConfig;ZZ)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;

    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->showEmployeeFilter:Z

    iget-boolean v1, p1, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->showEmployeeFilter:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->isDatePickerInitialized:Z

    iget-boolean p1, p1, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->isDatePickerInitialized:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getReportConfig()Lcom/squareup/customreport/data/ReportConfig;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    return-object v0
.end method

.method public final getShowEmployeeFilter()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->showEmployeeFilter:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->showEmployeeFilter:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->isDatePickerInitialized:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isDatePickerInitialized()Z
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->isDatePickerInitialized:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CustomizeAllFields(reportConfig="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showEmployeeFilter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->showEmployeeFilter:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isDatePickerInitialized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->isDatePickerInitialized:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->reportConfig:Lcom/squareup/customreport/data/ReportConfig;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->showEmployeeFilter:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean p2, p0, Lcom/squareup/salesreport/customize/CustomizeReportState$CustomizeAllFields;->isDatePickerInitialized:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
