.class public final Lcom/squareup/salesreport/chart/ChartViewsKt;
.super Ljava/lang/Object;
.source "ChartViews.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0004\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001aN\u0010\u0005\u001a\u00020\u0006*\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\u00022\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\r2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0000\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028CX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0015"
    }
    d2 = {
        "dateFormat",
        "",
        "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
        "getDateFormat",
        "(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;)I",
        "salesAdapterFromRow",
        "Lcom/squareup/salesreport/chart/SalesAdapter;",
        "Lcom/squareup/chartography/ChartView;",
        "localeProvider",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "row",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "numberFormatter",
        "",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "current24HourClockMode",
        "Lcom/squareup/time/Current24HourClockMode;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final getDateFormat(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;)I
    .locals 2

    .line 61
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getSalesChart()Lcom/squareup/customreport/data/WithSalesChartReport;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/customreport/data/WithSalesChartReport;->getType()Lcom/squareup/customreport/data/SalesReportType$Charted;

    move-result-object v0

    .line 62
    sget-object v1, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 65
    sget-object v1, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDayOfWeek;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_day_of_week_letter:I

    goto :goto_0

    .line 66
    :cond_0
    sget-object v1, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByMonth;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_only:I

    goto :goto_0

    .line 67
    :cond_1
    sget-object v1, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDay;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByDay;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 68
    invoke-virtual {p0}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getReportConfig()Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getRangeSelection()Lcom/squareup/customreport/data/RangeSelection;

    move-result-object p0

    sget-object v0, Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;->INSTANCE:Lcom/squareup/customreport/data/RangeSelection$PresetRangeSelection$OneMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 69
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_day_only:I

    goto :goto_0

    .line 71
    :cond_2
    sget p0, Lcom/squareup/salesreport/impl/R$string;->sales_report_date_format_month_day:I

    :goto_0
    return p0

    .line 68
    :cond_3
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 62
    :cond_4
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "No date format for BY_HOUR_OF_DAY adapter. BY_HOUR_OF_DAY uses a custom formatter"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method

.method public static final salesAdapterFromRow(Lcom/squareup/chartography/ChartView;Ljavax/inject/Provider;Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/time/Current24HourClockMode;)Lcom/squareup/salesreport/chart/SalesAdapter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/chartography/ChartView;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Ljava/lang/Number;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/time/Current24HourClockMode;",
            ")",
            "Lcom/squareup/salesreport/chart/SalesAdapter;"
        }
    .end annotation

    const-string v0, "$this$salesAdapterFromRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localeProvider"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "row"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "numberFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "current24HourClockMode"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-static {p2}, Lcom/squareup/salesreport/chart/SalesChartRowsKt;->getDataPoints(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;)Ljava/util/List;

    move-result-object v0

    .line 32
    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getSalesChart()Lcom/squareup/customreport/data/WithSalesChartReport;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/customreport/data/WithSalesChartReport;->getType()Lcom/squareup/customreport/data/SalesReportType$Charted;

    move-result-object v1

    .line 33
    sget-object v2, Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;->INSTANCE:Lcom/squareup/customreport/data/SalesReportType$Charted$SalesByHourOfDay;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance p0, Lcom/squareup/salesreport/chart/SalesByHourAdapter;

    .line 36
    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getChartSalesSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object p2

    invoke-static {p2, p3, p4, p5}, Lcom/squareup/salesreport/util/ChartSalesSelectionsKt;->rangeLabelFormatter(Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 33
    invoke-direct {p0, p1, v0, p2, p6}, Lcom/squareup/salesreport/chart/SalesByHourAdapter;-><init>(Ljavax/inject/Provider;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lcom/squareup/time/Current24HourClockMode;)V

    check-cast p0, Lcom/squareup/salesreport/chart/SalesAdapter;

    goto :goto_0

    .line 43
    :cond_0
    new-instance p1, Lcom/squareup/salesreport/chart/SalesByDateAdapter;

    .line 44
    invoke-virtual {p0}, Lcom/squareup/chartography/ChartView;->getContext()Landroid/content/Context;

    move-result-object p0

    const-string p6, "context"

    invoke-static {p0, p6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    new-instance p6, Lcom/squareup/util/ViewString$ResourceString;

    invoke-static {p2}, Lcom/squareup/salesreport/chart/ChartViewsKt;->getDateFormat(Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;)I

    move-result v1

    invoke-direct {p6, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast p6, Lcom/squareup/util/ViewString;

    .line 47
    invoke-virtual {p2}, Lcom/squareup/salesreport/util/SalesReportRow$SalesChartRow;->getChartSalesSelection()Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;

    move-result-object p2

    invoke-static {p2, p3, p4, p5}, Lcom/squareup/salesreport/util/ChartSalesSelectionsKt;->rangeLabelFormatter(Lcom/squareup/salesreport/SalesReportState$ChartSalesSelection;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 43
    invoke-direct {p1, p0, v0, p6, p2}, Lcom/squareup/salesreport/chart/SalesByDateAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/squareup/util/ViewString;Lkotlin/jvm/functions/Function1;)V

    move-object p0, p1

    check-cast p0, Lcom/squareup/salesreport/chart/SalesAdapter;

    :goto_0
    return-object p0
.end method
