.class final Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$5;
.super Lkotlin/jvm/internal/Lambda;
.source "ExportReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/export/RealExportReportWorkflow;->render(Lcom/squareup/salesreport/export/ExportReportProps;Lcom/squareup/salesreport/export/ExportReportState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/export/RealExportReportWorkflow;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$5;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    iput-object p2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$5;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$5;->invoke(Z)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Z)V
    .locals 3

    .line 133
    iget-object v0, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$5;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;

    iget-object v2, p0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$render$card$5;->this$0:Lcom/squareup/salesreport/export/RealExportReportWorkflow;

    invoke-static {v2}, Lcom/squareup/salesreport/export/RealExportReportWorkflow;->access$getIncludeItemsInReportLocalSetting$p(Lcom/squareup/salesreport/export/RealExportReportWorkflow;)Lcom/squareup/settings/LocalSetting;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$SetIncludeItemsConfig;-><init>(Lcom/squareup/settings/LocalSetting;Z)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
