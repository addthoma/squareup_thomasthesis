.class public final Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$Exit;
.super Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;
.source "ExportReportWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Exit"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u0004*\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u0016\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$Exit;",
        "Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/salesreport/export/ExportReportState;",
        "(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$Exit;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 199
    new-instance v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$Exit;

    invoke-direct {v0}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$Exit;-><init>()V

    sput-object v0, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$Exit;->INSTANCE:Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$Exit;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 199
    invoke-direct {p0, v0}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 199
    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/export/RealExportReportWorkflow$Action$Exit;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lkotlin/Unit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/salesreport/export/ExportReportState;",
            ">;)",
            "Lkotlin/Unit;"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method
