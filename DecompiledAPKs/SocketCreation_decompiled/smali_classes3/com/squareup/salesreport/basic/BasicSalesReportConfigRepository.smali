.class public interface abstract Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;
.super Ljava/lang/Object;
.source "BasicSalesReportConfigRepository.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0003H&\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/salesreport/basic/BasicSalesReportConfigRepository;",
        "",
        "get",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "set",
        "",
        "reportConfig",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract get()Lcom/squareup/customreport/data/ReportConfig;
.end method

.method public abstract set(Lcom/squareup/customreport/data/ReportConfig;)V
.end method
