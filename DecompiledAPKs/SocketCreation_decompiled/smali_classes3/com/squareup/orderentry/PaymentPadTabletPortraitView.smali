.class public Lcom/squareup/orderentry/PaymentPadTabletPortraitView;
.super Lcom/squareup/orderentry/PaymentPadView;
.source "PaymentPadTabletPortraitView.java"


# instance fields
.field private cartAmount:Landroid/widget/TextView;

.field private currentAnimator:Landroid/animation/ObjectAnimator;

.field private paymentPadLeftHalf:Landroid/view/View;

.field presenter:Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private saleQuantity:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/squareup/orderentry/PaymentPadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$002(Lcom/squareup/orderentry/PaymentPadTabletPortraitView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .line 22
    iput-object p1, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    return-object p1
.end method


# virtual methods
.method protected animateToCartList()V
    .locals 4

    .line 90
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->saleFrame:Landroid/view/View;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    const-string/jumbo v2, "y"

    invoke-static {v0, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    .line 94
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 95
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 97
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$3;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$3;-><init>(Lcom/squareup/orderentry/PaymentPadTabletPortraitView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method protected animateToEditMode()V
    .locals 4

    .line 66
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->saleFrame:Landroid/view/View;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->saleFrame:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    aput v3, v1, v2

    const-string/jumbo v2, "y"

    invoke-static {v0, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    .line 70
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v1, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 71
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->endOnDetach(Landroid/animation/Animator;Landroid/view/View;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$2;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$2;-><init>(Lcom/squareup/orderentry/PaymentPadTabletPortraitView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method getPresenter()Lcom/squareup/orderentry/PaymentPadPresenter;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->presenter:Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    return-object v0
.end method

.method public getSaleQuantity()Landroid/widget/TextView;
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->saleQuantity:Landroid/widget/TextView;

    return-object v0
.end method

.method initializeForOpenTickets()V
    .locals 4

    .line 119
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->cartAmount:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 122
    sget v0, Lcom/squareup/orderentry/R$id;->payment_pad_right_half:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 123
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v3, 0x3f800000    # 1.0f

    .line 124
    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 125
    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 126
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/squareup/marin/R$dimen;->marin_cart_item_gutter_half:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 128
    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestLayout()V

    return-void
.end method

.method protected inject()V
    .locals 2

    .line 36
    invoke-virtual {p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;

    invoke-interface {v0, p0}, Lcom/squareup/orderentry/OrderEntryScreen$TabletComponent;->inject(Lcom/squareup/orderentry/PaymentPadTabletPortraitView;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->currentAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->presenter:Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->dropView(Ljava/lang/Object;)V

    .line 58
    invoke-super {p0}, Lcom/squareup/orderentry/PaymentPadView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 40
    invoke-super {p0}, Lcom/squareup/orderentry/PaymentPadView;->onFinishInflate()V

    .line 41
    sget v0, Lcom/squareup/orderentry/R$id;->cart_header_sale_quantity:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->saleQuantity:Landroid/widget/TextView;

    .line 42
    sget v0, Lcom/squareup/orderentry/R$id;->payment_pad_portrait_amount:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->cartAmount:Landroid/widget/TextView;

    .line 43
    sget v0, Lcom/squareup/orderentry/R$id;->payment_pad_left_half:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->paymentPadLeftHalf:Landroid/view/View;

    .line 44
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->paymentPadLeftHalf:Landroid/view/View;

    new-instance v1, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$1;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitView$1;-><init>(Lcom/squareup/orderentry/PaymentPadTabletPortraitView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->presenter:Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/orderentry/PaymentPadTabletPortraitPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public removeBottomSaleBorder()V
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->saleFrame:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$drawable;->payment_pad_portrait_background_sale_no_border:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    return-void
.end method

.method setTotalAmount(Ljava/lang/String;)V
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->cartAmount:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showBottomSaleBorder()V
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/orderentry/PaymentPadTabletPortraitView;->saleFrame:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$drawable;->payment_pad_portrait_background_sale:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    return-void
.end method
