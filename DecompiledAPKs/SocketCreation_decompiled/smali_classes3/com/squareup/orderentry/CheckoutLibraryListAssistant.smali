.class public Lcom/squareup/orderentry/CheckoutLibraryListAssistant;
.super Ljava/lang/Object;
.source "CheckoutLibraryListAssistant.java"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListAssistant;


# instance fields
.field private final itemsAppletGateway:Lcom/squareup/ui/items/ItemsAppletGateway;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/ui/items/ItemsAppletGateway;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 21
    iput-object p2, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    .line 22
    iput-object p3, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant;->itemsAppletGateway:Lcom/squareup/ui/items/ItemsAppletGateway;

    return-void
.end method


# virtual methods
.method public getHasSearchText()Z
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->isLibrarySearchActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 27
    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->hasLibrarySearchText()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public logLibraryListItemClicked()V
    .locals 2

    .line 35
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_LIBRARY_LIST:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method

.method public logLibraryListItemLongClicked()V
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_LIBRARY_LIST:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method

.method public openItemsApplet()V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListAssistant;->itemsAppletGateway:Lcom/squareup/ui/items/ItemsAppletGateway;

    invoke-interface {v0}, Lcom/squareup/ui/items/ItemsAppletGateway;->activateItemsApplet()V

    return-void
.end method
