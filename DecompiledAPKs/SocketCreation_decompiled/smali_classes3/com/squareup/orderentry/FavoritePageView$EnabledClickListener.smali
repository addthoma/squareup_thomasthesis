.class abstract Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "FavoritePageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/FavoritePageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "EnabledClickListener"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1024
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/orderentry/FavoritePageView$1;)V
    .locals 0

    .line 1024
    invoke-direct {p0}, Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract click(Landroid/view/View;)V
.end method

.method public final doClick(Landroid/view/View;)V
    .locals 1

    .line 1027
    instance-of v0, p1, Lcom/squareup/orderentry/HasEnabledLook;

    if-eqz v0, :cond_0

    .line 1028
    move-object v0, p1

    check-cast v0, Lcom/squareup/orderentry/HasEnabledLook;

    .line 1029
    invoke-interface {v0}, Lcom/squareup/orderentry/HasEnabledLook;->looksEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1030
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;->click(Landroid/view/View;)V

    goto :goto_0

    .line 1033
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;->click(Landroid/view/View;)V

    :cond_1
    :goto_0
    return-void
.end method
