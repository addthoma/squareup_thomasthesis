.class public Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "ClearKeypadPanelDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogCardScreen;
    value = Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private clearOption:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

.field private confirmation:Lcom/squareup/register/widgets/Confirmation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 67
    sget-object v0, Lcom/squareup/orderentry/-$$Lambda$ClearKeypadPanelDialogScreen$gnMLKIW-DKCW4rmIkhCPrKjkc-U;->INSTANCE:Lcom/squareup/orderentry/-$$Lambda$ClearKeypadPanelDialogScreen$gnMLKIW-DKCW4rmIkhCPrKjkc-U;

    .line 68
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/register/widgets/Confirmation;Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;->confirmation:Lcom/squareup/register/widgets/Confirmation;

    .line 30
    iput-object p2, p0, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;->clearOption:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;)Lcom/squareup/register/widgets/Confirmation;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;->confirmation:Lcom/squareup/register/widgets/Confirmation;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;)Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;->clearOption:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;
    .locals 2

    .line 69
    const-class v0, Lcom/squareup/register/widgets/Confirmation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/Confirmation;

    .line 70
    invoke-virtual {p0}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    .line 71
    new-instance v1, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;

    invoke-direct {v1, v0, p0}, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;-><init>(Lcom/squareup/register/widgets/Confirmation;Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;)V

    return-object v1
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;->confirmation:Lcom/squareup/register/widgets/Confirmation;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 64
    iget-object p2, p0, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;->clearOption:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
