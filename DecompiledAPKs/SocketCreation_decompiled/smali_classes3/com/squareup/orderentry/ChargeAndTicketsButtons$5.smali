.class Lcom/squareup/orderentry/ChargeAndTicketsButtons$5;
.super Landroid/animation/AnimatorListenerAdapter;
.source "ChargeAndTicketsButtons.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/ChargeAndTicketsButtons;->buildTicketSavedButtonsInAnimator()Landroid/animation/AnimatorSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)V
    .locals 0

    .line 282
    iput-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$5;->this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1

    .line 290
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$5;->this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-static {p1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->access$300(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Landroid/widget/TextView;

    move-result-object p1

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 291
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$5;->this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->setEnabled(Z)V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .line 284
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$5;->this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-static {p1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->access$300(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Landroid/widget/TextView;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 285
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$5;->this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-static {p1}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->access$400(Lcom/squareup/orderentry/ChargeAndTicketsButtons;)Landroid/view/ViewGroup;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 286
    iget-object p1, p0, Lcom/squareup/orderentry/ChargeAndTicketsButtons$5;->this$0:Lcom/squareup/orderentry/ChargeAndTicketsButtons;

    invoke-virtual {p1, v0}, Lcom/squareup/orderentry/ChargeAndTicketsButtons;->setEnabled(Z)V

    return-void
.end method
