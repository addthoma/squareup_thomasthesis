.class public final Lcom/squareup/orderentry/ClockSkew_Factory;
.super Ljava/lang/Object;
.source "ClockSkew_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/ClockSkew;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/orderentry/ClockSkew_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/orderentry/ClockSkew_Factory;->clockProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/orderentry/ClockSkew_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/orderentry/ClockSkew_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/orderentry/ClockSkew_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/ClockSkew_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/orderentry/ClockSkew_Factory;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/squareup/orderentry/ClockSkew_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orderentry/ClockSkew_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Clock;Lcom/squareup/connectivity/ConnectivityMonitor;Lflow/Flow;Lcom/squareup/settings/server/Features;)Lcom/squareup/orderentry/ClockSkew;
    .locals 7

    .line 54
    new-instance v6, Lcom/squareup/orderentry/ClockSkew;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orderentry/ClockSkew;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Clock;Lcom/squareup/connectivity/ConnectivityMonitor;Lflow/Flow;Lcom/squareup/settings/server/Features;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/ClockSkew;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/orderentry/ClockSkew_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/orderentry/ClockSkew_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Clock;

    iget-object v2, p0, Lcom/squareup/orderentry/ClockSkew_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v3, p0, Lcom/squareup/orderentry/ClockSkew_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflow/Flow;

    iget-object v4, p0, Lcom/squareup/orderentry/ClockSkew_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/orderentry/ClockSkew_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Clock;Lcom/squareup/connectivity/ConnectivityMonitor;Lflow/Flow;Lcom/squareup/settings/server/Features;)Lcom/squareup/orderentry/ClockSkew;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/orderentry/ClockSkew_Factory;->get()Lcom/squareup/orderentry/ClockSkew;

    move-result-object v0

    return-object v0
.end method
