.class Lcom/squareup/orderentry/FavoritePageView$8;
.super Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;
.source "FavoritePageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/orderentry/FavoritePageView;->createAllGiftCardsView(Ljava/lang/String;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/FavoritePageView;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/FavoritePageView;)V
    .locals 0

    .line 461
    iput-object p1, p0, Lcom/squareup/orderentry/FavoritePageView$8;->this$0:Lcom/squareup/orderentry/FavoritePageView;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/FavoritePageView$EnabledClickListener;-><init>(Lcom/squareup/orderentry/FavoritePageView$1;)V

    return-void
.end method


# virtual methods
.method public click(Landroid/view/View;)V
    .locals 0

    .line 463
    iget-object p1, p0, Lcom/squareup/orderentry/FavoritePageView$8;->this$0:Lcom/squareup/orderentry/FavoritePageView;

    iget-object p1, p1, Lcom/squareup/orderentry/FavoritePageView;->presenter:Lcom/squareup/orderentry/FavoritePageScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/orderentry/FavoritePageScreen$Presenter;->onAllGiftCardsClicked()V

    return-void
.end method
