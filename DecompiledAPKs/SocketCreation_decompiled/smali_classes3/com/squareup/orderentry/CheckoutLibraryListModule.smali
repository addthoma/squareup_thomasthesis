.class public abstract Lcom/squareup/orderentry/CheckoutLibraryListModule;
.super Ljava/lang/Object;
.source "CheckoutLibraryListModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideRealLibraryListStateManager(Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/giftcardactivation/GiftCardActivationFlow;Lcom/squareup/orderentry/CheckoutLibraryListConfiguration;Lcom/squareup/librarylist/CheckoutLibraryListStateSaver;)Lcom/squareup/librarylist/RealLibraryListStateManager;
    .locals 10
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 38
    new-instance v9, Lcom/squareup/librarylist/RealLibraryListStateManager;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/librarylist/RealLibraryListStateManager;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/giftcardactivation/GiftCardActivationFlow;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListStateSaver;)V

    return-object v9
.end method

.method static provideSavedItemSuggestionsPref(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation runtime Lcom/squareup/librarylist/SavedItemSuggestions;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string v0, "saved_item_suggestions"

    .line 52
    invoke-virtual {p0, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getStringSet(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract bindLibraryListStateManager(Lcom/squareup/librarylist/RealLibraryListStateManager;)Lcom/squareup/librarylist/LibraryListStateManager;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
