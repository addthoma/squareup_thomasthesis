.class public final Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "PageLabelEditScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lflow/Flow;Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;-><init>(Lflow/Flow;Ljavax/inject/Provider;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2}, Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;->newInstance(Lflow/Flow;Ljavax/inject/Provider;Lcom/squareup/util/Res;)Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/orderentry/PageLabelEditScreen_Presenter_Factory;->get()Lcom/squareup/orderentry/PageLabelEditScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
