.class public final Lcom/squareup/orderentry/OrderEntryScreenState_Factory;
.super Ljava/lang/Object;
.source "OrderEntryScreenState_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/orderentry/OrderEntryScreenState;",
        ">;"
    }
.end annotation


# instance fields
.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenState_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/orderentry/OrderEntryScreenState_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Lcom/squareup/orderentry/OrderEntryScreenState_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenState_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/OrderEntryScreenState_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/orderentry/OrderEntryScreenState;
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/OrderEntryScreenState;-><init>(Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/orderentry/OrderEntryScreenState;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryScreenState_Factory;->newInstance(Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/orderentry/OrderEntryScreenState;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/orderentry/OrderEntryScreenState_Factory;->get()Lcom/squareup/orderentry/OrderEntryScreenState;

    move-result-object v0

    return-object v0
.end method
