.class synthetic Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen$1;
.super Ljava/lang/Object;
.source "ClearKeypadPanelDialogScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$ui$seller$SellerScopeRunner$ClearOptions:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 47
    invoke-static {}, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->values()[Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen$1;->$SwitchMap$com$squareup$ui$seller$SellerScopeRunner$ClearOptions:[I

    :try_start_0
    sget-object v0, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen$1;->$SwitchMap$com$squareup$ui$seller$SellerScopeRunner$ClearOptions:[I

    sget-object v1, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->CLEAR_CART:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    invoke-virtual {v1}, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/orderentry/ClearKeypadPanelDialogScreen$1;->$SwitchMap$com$squareup$ui$seller$SellerScopeRunner$ClearOptions:[I

    sget-object v1, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->CLEAR_NON_LOCKED_ITEMS:Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;

    invoke-virtual {v1}, Lcom/squareup/ui/seller/SellerScopeRunner$ClearOptions;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method
