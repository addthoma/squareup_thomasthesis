.class public final Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsNonHomeApplet;
.super Lcom/squareup/orderentry/RealOrderEntryAppletGateway;
.source "RealOrderEntryAppletGateway.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/RealOrderEntryAppletGateway;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AsNonHomeApplet"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsNonHomeApplet;",
        "Lcom/squareup/orderentry/RealOrderEntryAppletGateway;",
        "orderEntryApplet",
        "Lcom/squareup/orderentry/OrderEntryApplet;",
        "(Lcom/squareup/orderentry/OrderEntryApplet;)V",
        "getOrderEntryApplet",
        "()Lcom/squareup/orderentry/OrderEntryApplet;",
        "historyFactoryForMode",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "mode",
        "Lcom/squareup/orderentry/OrderEntryMode;",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final orderEntryApplet:Lcom/squareup/orderentry/OrderEntryApplet;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/OrderEntryApplet;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderEntryApplet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 59
    invoke-direct {p0, v0}, Lcom/squareup/orderentry/RealOrderEntryAppletGateway;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsNonHomeApplet;->orderEntryApplet:Lcom/squareup/orderentry/OrderEntryApplet;

    return-void
.end method


# virtual methods
.method public getOrderEntryApplet()Lcom/squareup/orderentry/OrderEntryApplet;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsNonHomeApplet;->orderEntryApplet:Lcom/squareup/orderentry/OrderEntryApplet;

    return-object v0
.end method

.method public historyFactoryForMode(Lcom/squareup/orderentry/OrderEntryMode;)Lcom/squareup/ui/main/HistoryFactory;
    .locals 2

    const-string v0, "mode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    new-instance v0, Lcom/squareup/ui/main/HistoryFactory$Simple;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/RealOrderEntryAppletGateway$AsNonHomeApplet;->orderEntryScreenForMode(Lcom/squareup/orderentry/OrderEntryMode;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    invoke-static {p1}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object p1

    const-string v1, "History.single(orderEntryScreenForMode(mode))"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, p1}, Lcom/squareup/ui/main/HistoryFactory$Simple;-><init>(Lflow/History;)V

    check-cast v0, Lcom/squareup/ui/main/HistoryFactory;

    return-object v0
.end method
