.class public final Lcom/squareup/orderentry/OrderEntryViewPager_MembersInjector;
.super Ljava/lang/Object;
.source "OrderEntryViewPager_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/orderentry/OrderEntryViewPager;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/orderentry/OrderEntryViewPager_MembersInjector;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/orderentry/OrderEntryViewPager;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/orderentry/OrderEntryViewPager_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/orderentry/OrderEntryViewPager;Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->presenter:Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    return-void
.end method

.method public static injectTutorialCore(Lcom/squareup/orderentry/OrderEntryViewPager;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryViewPager;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/orderentry/OrderEntryViewPager;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryViewPager_MembersInjector;->injectPresenter(Lcom/squareup/orderentry/OrderEntryViewPager;Lcom/squareup/orderentry/OrderEntryViewPagerPresenter;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryViewPager_MembersInjector;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static {p1, v0}, Lcom/squareup/orderentry/OrderEntryViewPager_MembersInjector;->injectTutorialCore(Lcom/squareup/orderentry/OrderEntryViewPager;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/orderentry/OrderEntryViewPager;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/OrderEntryViewPager_MembersInjector;->injectMembers(Lcom/squareup/orderentry/OrderEntryViewPager;)V

    return-void
.end method
