.class final enum Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;
.super Ljava/lang/Enum;
.source "ClearCardOrSalePopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/ClearCardOrSalePopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Choices"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

.field public static final enum CANCEL:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

.field public static final enum CARD:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

.field public static final enum SALE:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 16
    new-instance v0, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    const/4 v1, 0x0

    const-string v2, "CARD"

    invoke-direct {v0, v2, v1}, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->CARD:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    new-instance v0, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    const/4 v2, 0x1

    const-string v3, "SALE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->SALE:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    new-instance v0, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    const/4 v3, 0x2

    const-string v4, "CANCEL"

    invoke-direct {v0, v4, v3}, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->CANCEL:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    sget-object v4, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->CARD:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->SALE:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->CANCEL:Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->$VALUES:[Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;
    .locals 1

    .line 16
    const-class v0, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->$VALUES:[Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    invoke-virtual {v0}, [Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orderentry/ClearCardOrSalePopup$Choices;

    return-object v0
.end method
