.class public final Lcom/squareup/orderentry/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final add_note:I = 0x7f12008e

.field public static final are_you_sure:I = 0x7f1200df

.field public static final cart_items_all:I = 0x7f12034b

.field public static final cart_items_none:I = 0x7f12034c

.field public static final cart_items_one:I = 0x7f12034d

.field public static final cart_items_some:I = 0x7f12034e

.field public static final cart_menu_drop_down_button_content_description:I = 0x7f12034f

.field public static final cart_menu_drop_down_content_description:I = 0x7f120350

.field public static final cart_tax_reset:I = 0x7f120357

.field public static final cart_tax_row_off:I = 0x7f12035a

.field public static final cart_total:I = 0x7f12035c

.field public static final cash_drawers_charge_button_open:I = 0x7f120372

.field public static final cash_drawers_charge_button_open_confirm:I = 0x7f120373

.field public static final charge_amount:I = 0x7f1203f7

.field public static final charge_amount_confirm:I = 0x7f1203f8

.field public static final charge_amount_including_tax:I = 0x7f1203f9

.field public static final clear:I = 0x7f120422

.field public static final clear_card:I = 0x7f120423

.field public static final clear_items:I = 0x7f120424

.field public static final clear_sale:I = 0x7f120426

.field public static final comp_ticket:I = 0x7f120460

.field public static final comp_ticket_help_text:I = 0x7f120461

.field public static final create_new_dialog_create_discount:I = 0x7f1205e6

.field public static final create_new_dialog_create_item:I = 0x7f1205e7

.field public static final crm_customer_added_format:I = 0x7f120683

.field public static final crm_view_customer_label:I = 0x7f120782

.field public static final current_sale:I = 0x7f120796

.field public static final discount_warning_no_matching_item:I = 0x7f12088d

.field public static final duplicate_gift_card_message:I = 0x7f1208e8

.field public static final duplicate_gift_card_title:I = 0x7f1208e9

.field public static final employee_management_clock_in_button:I = 0x7f120a01

.field public static final employee_management_lock_button_full:I = 0x7f120a1a

.field public static final employee_management_lock_button_guest:I = 0x7f120a1b

.field public static final employee_management_lock_button_login:I = 0x7f120a1c

.field public static final employee_management_lock_button_logout:I = 0x7f120a1d

.field public static final favorite_tile_all_discounts:I = 0x7f120aa9

.field public static final favorite_tile_all_items:I = 0x7f120aaa

.field public static final favorite_tile_all_items_and_services:I = 0x7f120aab

.field public static final favorite_tile_rewards_search:I = 0x7f120aac

.field public static final favorite_tile_unknown_abbrev:I = 0x7f120aad

.field public static final favorite_tile_unknown_clicked:I = 0x7f120aae

.field public static final favorite_tile_unknown_name:I = 0x7f120aaf

.field public static final gift_card_account_unsupported_message:I = 0x7f120af9

.field public static final gift_card_account_unsupported_title:I = 0x7f120afa

.field public static final gift_card_activating:I = 0x7f120afb

.field public static final gift_card_activation_error_message:I = 0x7f120afc

.field public static final gift_card_custom:I = 0x7f120b13

.field public static final gift_card_load:I = 0x7f120b1a

.field public static final gift_card_load_amount:I = 0x7f120b1b

.field public static final gift_card_unsupported_message:I = 0x7f120b26

.field public static final gift_card_unsupported_title:I = 0x7f120b28

.field public static final including_tax:I = 0x7f120bf4

.field public static final item_count_plural:I = 0x7f120dd0

.field public static final item_count_single:I = 0x7f120dd1

.field public static final item_list_add_to_grid_title:I = 0x7f120e39

.field public static final item_list_no_items_title:I = 0x7f120e3a

.field public static final loyalty_cart_row_label:I = 0x7f120f0e

.field public static final loyalty_cart_row_value:I = 0x7f120f0f

.field public static final open_tickets_add_customer:I = 0x7f12116a

.field public static final open_tickets_available_tickets:I = 0x7f12116e

.field public static final open_tickets_cart_total:I = 0x7f121170

.field public static final open_tickets_charge:I = 0x7f121171

.field public static final open_tickets_clear_new_items:I = 0x7f121173

.field public static final open_tickets_delete:I = 0x7f121174

.field public static final open_tickets_delete_ticket:I = 0x7f121175

.field public static final open_tickets_delete_ticket_warning_keep:I = 0x7f121177

.field public static final open_tickets_delete_ticket_warning_many:I = 0x7f121178

.field public static final open_tickets_delete_ticket_warning_one:I = 0x7f121179

.field public static final open_tickets_delete_tickets:I = 0x7f12117a

.field public static final open_tickets_deleted_many:I = 0x7f12117b

.field public static final open_tickets_deleted_one:I = 0x7f12117c

.field public static final open_tickets_dismiss:I = 0x7f12117d

.field public static final open_tickets_edit_ticket:I = 0x7f12117e

.field public static final open_tickets_error_offline_message:I = 0x7f12117f

.field public static final open_tickets_error_offline_title:I = 0x7f121180

.field public static final open_tickets_error_offline_title_short:I = 0x7f121181

.field public static final open_tickets_error_sync_message_many:I = 0x7f121182

.field public static final open_tickets_error_sync_message_one:I = 0x7f121183

.field public static final open_tickets_error_sync_message_update_register:I = 0x7f121184

.field public static final open_tickets_error_sync_title:I = 0x7f121185

.field public static final open_tickets_error_sync_unfixable_message_many:I = 0x7f121186

.field public static final open_tickets_error_sync_unfixable_message_one:I = 0x7f121187

.field public static final open_tickets_loading:I = 0x7f121188

.field public static final open_tickets_merge:I = 0x7f121189

.field public static final open_tickets_merge_canceled_confirmation:I = 0x7f12118a

.field public static final open_tickets_merge_canceled_message:I = 0x7f12118b

.field public static final open_tickets_merge_canceled_title:I = 0x7f12118c

.field public static final open_tickets_merge_in_progress:I = 0x7f12118d

.field public static final open_tickets_merge_tickets_prompt:I = 0x7f12118f

.field public static final open_tickets_merge_tickets_success:I = 0x7f121190

.field public static final open_tickets_merge_with:I = 0x7f121191

.field public static final open_tickets_move:I = 0x7f121192

.field public static final open_tickets_move_canceled_title_many:I = 0x7f121193

.field public static final open_tickets_move_canceled_title_one:I = 0x7f121194

.field public static final open_tickets_move_in_progress:I = 0x7f121195

.field public static final open_tickets_move_ticket_list_many:I = 0x7f121197

.field public static final open_tickets_move_ticket_list_one:I = 0x7f121198

.field public static final open_tickets_move_ticket_list_two:I = 0x7f121199

.field public static final open_tickets_move_tickets_many:I = 0x7f12119a

.field public static final open_tickets_move_tickets_one:I = 0x7f12119b

.field public static final open_tickets_move_tickets_success_many:I = 0x7f12119c

.field public static final open_tickets_move_tickets_success_one:I = 0x7f12119d

.field public static final open_tickets_new_items_multiple:I = 0x7f12119e

.field public static final open_tickets_new_items_one:I = 0x7f12119f

.field public static final open_tickets_open_tickets:I = 0x7f1211a3

.field public static final open_tickets_other_tickets:I = 0x7f1211a4

.field public static final open_tickets_save:I = 0x7f1211a7

.field public static final open_tickets_save_items_to:I = 0x7f1211a8

.field public static final open_tickets_saving:I = 0x7f1211a9

.field public static final open_tickets_search_tickets:I = 0x7f1211ab

.field public static final open_tickets_ticket_name_hint:I = 0x7f1211b4

.field public static final open_tickets_ticket_name_hint_swipe_allowed:I = 0x7f1211b5

.field public static final open_tickets_ticket_new_ticket:I = 0x7f1211b6

.field public static final open_tickets_tickets:I = 0x7f1211b8

.field public static final open_tickets_too_many_error_dialog_body:I = 0x7f1211be

.field public static final open_tickets_too_many_error_dialog_title:I = 0x7f1211bf

.field public static final open_tickets_total:I = 0x7f1211c0

.field public static final open_tickets_total_amount:I = 0x7f1211c1

.field public static final open_tickets_transfer:I = 0x7f1211c2

.field public static final open_tickets_transfer_tickets_many:I = 0x7f1211c4

.field public static final open_tickets_transfer_tickets_one:I = 0x7f1211c5

.field public static final open_tickets_transferred_many:I = 0x7f1211c6

.field public static final open_tickets_transferred_one:I = 0x7f1211c7

.field public static final open_tickets_view_all_tickets:I = 0x7f1211ce

.field public static final open_tickets_void_in_progress:I = 0x7f1211cf

.field public static final open_tickets_void_ticket_warning_many:I = 0x7f1211d0

.field public static final open_tickets_void_ticket_warning_one:I = 0x7f1211d1

.field public static final open_tickets_voided_many:I = 0x7f1211d2

.field public static final open_tickets_voided_one:I = 0x7f1211d3

.field public static final open_tickets_your_tickets:I = 0x7f1211d4

.field public static final page_label_edit_title:I = 0x7f1212fb

.field public static final predefined_tickets_all_tickets:I = 0x7f121469

.field public static final predefined_tickets_convert_to_custom_ticket:I = 0x7f12146e

.field public static final predefined_tickets_custom:I = 0x7f121470

.field public static final predefined_tickets_custom_ticket:I = 0x7f121471

.field public static final predefined_tickets_edit_custom_ticket:I = 0x7f121478

.field public static final predefined_tickets_log_out:I = 0x7f12147a

.field public static final predefined_tickets_new_group_ticket:I = 0x7f12147b

.field public static final predefined_tickets_no_group_tickets_available_title:I = 0x7f12147c

.field public static final predefined_tickets_no_group_tickets_message:I = 0x7f12147d

.field public static final predefined_tickets_no_group_tickets_title:I = 0x7f12147e

.field public static final predefined_tickets_no_predefined_tickets_message:I = 0x7f12147f

.field public static final predefined_tickets_no_predefined_tickets_title:I = 0x7f121480

.field public static final predefined_tickets_no_tickets_title:I = 0x7f121482

.field public static final predefined_tickets_none:I = 0x7f121483

.field public static final predefined_tickets_search_all_tickets:I = 0x7f121489

.field public static final remove_discount_content_description:I = 0x7f12166d

.field public static final remove_tax_content_description:I = 0x7f121674

.field public static final sku_not_found_actionbar:I = 0x7f121821

.field public static final sku_not_found_title:I = 0x7f121823

.field public static final split_ticket_move_item_plural:I = 0x7f121864

.field public static final split_ticket_move_item_singular:I = 0x7f121865

.field public static final split_ticket_new_ticket:I = 0x7f121867

.field public static final split_ticket_print_all:I = 0x7f121869

.field public static final split_ticket_save_all:I = 0x7f12186a

.field public static final split_ticket_ticket_name_action_bar:I = 0x7f12186b

.field public static final square_url:I = 0x7f1218a7

.field public static final unapplied_discount_row_title_choose_item:I = 0x7f121ae2

.field public static final variable_percent_discount_standalone_character:I = 0x7f121b93

.field public static final void_ticket:I = 0x7f121bb7

.field public static final void_tickets:I = 0x7f121bb9


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
