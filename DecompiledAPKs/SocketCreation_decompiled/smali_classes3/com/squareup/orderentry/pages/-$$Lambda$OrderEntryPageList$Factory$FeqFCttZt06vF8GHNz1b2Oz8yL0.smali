.class public final synthetic Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$FeqFCttZt06vF8GHNz1b2Oz8yL0;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Ljavax/inject/Provider;


# instance fields
.field private final synthetic f$0:Lcom/squareup/util/Res;

.field private final synthetic f$1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

.field private final synthetic f$2:Lcom/squareup/catalogapi/CatalogIntegrationController;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$FeqFCttZt06vF8GHNz1b2Oz8yL0;->f$0:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$FeqFCttZt06vF8GHNz1b2Oz8yL0;->f$1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    iput-object p3, p0, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$FeqFCttZt06vF8GHNz1b2Oz8yL0;->f$2:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$FeqFCttZt06vF8GHNz1b2Oz8yL0;->f$0:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$FeqFCttZt06vF8GHNz1b2Oz8yL0;->f$1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    iget-object v2, p0, Lcom/squareup/orderentry/pages/-$$Lambda$OrderEntryPageList$Factory$FeqFCttZt06vF8GHNz1b2Oz8yL0;->f$2:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static {v0, v1, v2}, Lcom/squareup/orderentry/pages/OrderEntryPageList$Factory;->lambda$createOtherPage$3(Lcom/squareup/util/Res;Lcom/squareup/orderentry/pages/OrderEntryPageKey;Lcom/squareup/catalogapi/CatalogIntegrationController;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
