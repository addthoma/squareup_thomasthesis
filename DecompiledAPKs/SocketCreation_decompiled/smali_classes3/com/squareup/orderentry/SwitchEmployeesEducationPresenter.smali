.class public Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;
.super Lcom/squareup/mortar/PopupPresenter;
.source "SwitchEmployeesEducationPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/ui/Showing;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field public static final GUEST_EMPLOYEE_TOKEN_FOR_TOOLTIP:Ljava/lang/String; = "GUEST_EMPLOYEE"


# instance fields
.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final employeesWhoHaveSeenTooltipSetSetting:Lcom/squareup/settings/StringSetLocalSetting;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final res:Lcom/squareup/util/Res;

.field private final tooltipEnabled:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/settings/StringSetLocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .param p3    # Lcom/squareup/settings/StringSetLocalSetting;
        .annotation runtime Lcom/squareup/orderentry/SwitchEmployeesSettingsModule$SwitchEmployeesSeen;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;",
            ">;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/settings/StringSetLocalSetting;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 42
    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->tooltipEnabled:Lcom/squareup/settings/LocalSetting;

    .line 44
    iput-object p2, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 45
    iput-object p3, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->employeesWhoHaveSeenTooltipSetSetting:Lcom/squareup/settings/StringSetLocalSetting;

    .line 46
    iput-object p4, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    .line 47
    iput-object p5, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->res:Lcom/squareup/util/Res;

    .line 48
    iput-object p6, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/common/R$string;->switch_employee_tooltip:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onlyRequireForRestrictedActions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/common/R$string;->switch_employee_tooltip_restricted:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/common/R$string;->switch_employee_tooltip_passcode:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->onPopupResult(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPopupResult(Ljava/lang/Void;)V
    .locals 3

    .line 52
    iget-object p1, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->employeesWhoHaveSeenTooltipSetSetting:Lcom/squareup/settings/StringSetLocalSetting;

    invoke-virtual {p1}, Lcom/squareup/settings/StringSetLocalSetting;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    .line 55
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isCurrentEmployeeAGuest()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GUEST_EMPLOYEE"

    goto :goto_0

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v0

    .line 61
    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 62
    new-instance v1, Ljava/util/LinkedHashSet;

    .line 63
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 64
    invoke-interface {v1, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 65
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 66
    iget-object p1, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->employeesWhoHaveSeenTooltipSetSetting:Lcom/squareup/settings/StringSetLocalSetting;

    invoke-virtual {p1, v1}, Lcom/squareup/settings/StringSetLocalSetting;->set(Ljava/util/Set;)V

    :cond_1
    return-void
.end method

.method public shouldBeShown()Z
    .locals 3

    .line 77
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->tooltipEnabled:Lcom/squareup/settings/LocalSetting;

    sget-object v1, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;->ENABLED:Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;->DISABLED:Lcom/squareup/orderentry/SwitchEmployeesTooltipEnabled;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    return v2

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-eqz v0, :cond_1

    return v2

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_1

    .line 83
    :cond_2
    iget-object v0, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->employeesWhoHaveSeenTooltipSetSetting:Lcom/squareup/settings/StringSetLocalSetting;

    invoke-virtual {v0}, Lcom/squareup/settings/StringSetLocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 84
    iget-object v1, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isCurrentEmployeeAGuest()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "GUEST_EMPLOYEE"

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 86
    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v1

    .line 87
    :goto_0
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0

    :cond_4
    :goto_1
    return v2
.end method

.method public showIfUnseen()V
    .locals 1

    .line 71
    invoke-virtual {p0}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->shouldBeShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    new-instance v0, Lcom/squareup/ui/Showing;

    invoke-direct {v0}, Lcom/squareup/ui/Showing;-><init>()V

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/SwitchEmployeesEducationPresenter;->show(Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method
