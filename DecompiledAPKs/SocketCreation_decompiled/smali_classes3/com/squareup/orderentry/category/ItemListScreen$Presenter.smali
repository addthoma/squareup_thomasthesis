.class Lcom/squareup/orderentry/category/ItemListScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ItemListScreen.java"

# interfaces
.implements Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/category/ItemListScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/orderentry/category/ItemListView;",
        ">;",
        "Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;"
    }
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field private final editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

.field private final entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

.field private final favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

.field private final flow:Lflow/Flow;

.field private final itemsAppletGateway:Lcom/squareup/ui/items/ItemsAppletGateway;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/orderentry/category/ItemListScreen;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/ui/main/CheckoutEntryHandler;Ljavax/inject/Provider;Lcom/squareup/badbus/BadBus;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/ui/items/ItemsAppletGateway;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Lcom/squareup/ui/main/CheckoutEntryHandler;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/barcodescanners/BarcodeScannerTracker;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            "Lcom/squareup/ui/items/ItemsAppletGateway;",
            "Lcom/squareup/ui/items/EditItemGateway;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 171
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 172
    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 173
    iput-object p2, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->flow:Lflow/Flow;

    .line 174
    iput-object p3, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    .line 175
    iput-object p4, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    .line 176
    iput-object p5, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    .line 177
    iput-object p6, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 178
    iput-object p7, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    .line 179
    iput-object p8, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 180
    iput-object p9, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    .line 181
    iput-object p10, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->itemsAppletGateway:Lcom/squareup/ui/items/ItemsAppletGateway;

    .line 182
    iput-object p11, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    .line 183
    iput-object p12, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method private closeCursor()V
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    const/4 v0, 0x0

    .line 199
    iput-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    :cond_0
    return-void
.end method

.method private createPlaceholderClicked(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;)V
    .locals 4

    .line 384
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/orderentry/category/ItemListScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 385
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->selectPlaceholderItem(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;)V

    return-void
.end method

.method private createTile(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)V
    .locals 4

    .line 342
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/orderentry/category/ItemListScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 343
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->selectCatalogItem(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic lambda$tErWZNGb3zpdOe1k1ROhxP046b4(Lcom/squareup/orderentry/category/ItemListScreen$Presenter;Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V

    return-void
.end method

.method private onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 260
    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_IMAGE:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_VARIATION:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 261
    invoke-direct {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->reloadCursor()V

    :cond_0
    return-void
.end method

.method private reloadAllItemsWithCount()V
    .locals 3

    .line 295
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$gOBKVSSLyFVvjAqeJzuYkadFvNo;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$gOBKVSSLyFVvjAqeJzuYkadFvNo;-><init>(Lcom/squareup/orderentry/category/ItemListScreen$Presenter;)V

    new-instance v2, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$08HoRCP90ynfWVk1yxxOJasQnmI;

    invoke-direct {v2, p0}, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$08HoRCP90ynfWVk1yxxOJasQnmI;-><init>(Lcom/squareup/orderentry/category/ItemListScreen$Presenter;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method private reloadCursor()V
    .locals 3

    .line 266
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v0}, Lcom/squareup/orderentry/category/ItemListScreen;->isCreateTile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    invoke-direct {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->reloadAllItemsWithCount()V

    goto :goto_0

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$p_xVqGDE7Agk6PsT0W3aR3DSTnM;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$p_xVqGDE7Agk6PsT0W3aR3DSTnM;-><init>(Lcom/squareup/orderentry/category/ItemListScreen$Presenter;)V

    new-instance v2, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$oZDk5rXdaoiKCjTHWDOYrxhO3K0;

    invoke-direct {v2, p0}, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$oZDk5rXdaoiKCjTHWDOYrxhO3K0;-><init>(Lcom/squareup/orderentry/category/ItemListScreen$Presenter;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    :goto_0
    return-void
.end method

.method private updateViewCursor()V
    .locals 2

    .line 315
    invoke-virtual {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/category/ItemListView;

    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/category/ItemListView;->updateCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-void
.end method

.method private updateViewCursorWithPlaceholders(Lcom/squareup/orderentry/PlaceholderCounts;)V
    .locals 2

    .line 319
    invoke-virtual {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/category/ItemListView;

    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/orderentry/category/ItemListView;->updateCursorWithPlaceholders(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Lcom/squareup/orderentry/PlaceholderCounts;)V

    return-void
.end method


# virtual methods
.method public barcodeScanned(Ljava/lang/String;)V
    .locals 3

    .line 389
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$hbxXM-TTC5ysecu9HhAz43YKOWI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$hbxXM-TTC5ysecu9HhAz43YKOWI;-><init>(Lcom/squareup/orderentry/category/ItemListScreen$Presenter;Ljava/lang/String;)V

    new-instance v2, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$c8ERvFQLRH5q6DdPNhIZIBThURo;

    invoke-direct {v2, p0, p1}, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$c8ERvFQLRH5q6DdPNhIZIBThURo;-><init>(Lcom/squareup/orderentry/category/ItemListScreen$Presenter;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method public dropView(Lcom/squareup/orderentry/category/ItemListView;)V
    .locals 1

    .line 240
    invoke-virtual {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v0}, Lcom/squareup/orderentry/category/ItemListScreen;->isCreateTile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-virtual {v0, p0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->removeBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 243
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 146
    check-cast p1, Lcom/squareup/orderentry/category/ItemListView;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->dropView(Lcom/squareup/orderentry/category/ItemListView;)V

    return-void
.end method

.method protected finish()V
    .locals 4

    .line 247
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/orderentry/category/ItemListScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 248
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->favoritesTileItemSelectionEvents:Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    invoke-virtual {v0}, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;->discardChanges()V

    .line 249
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Item List Screen Dismissed"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method public isEnabled(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z
    .locals 2

    .line 253
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v0}, Lcom/squareup/orderentry/category/ItemListScreen;->isCreateTile()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    return p1

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/main/CheckoutEntryHandler;->isEntryEnabled(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$barcodeScanned$4$ItemListScreen$Presenter(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;
    .locals 4

    .line 390
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;

    .line 391
    invoke-interface {p2, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;

    .line 392
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/api/items/Item$Type;

    sget-object v2, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 393
    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v0

    .line 392
    invoke-virtual {p2, p1, v0}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->getInfoForSku(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$barcodeScanned$5$ItemListScreen$Presenter(Ljava/lang/String;Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1

    .line 395
    invoke-interface {p2}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    .line 396
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 397
    sget-object p1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;

    iget-object p2, p2, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;->itemId:Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->createTile(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)V

    goto :goto_0

    .line 399
    :cond_0
    iget-object p2, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/BarcodeNotFoundScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/BarcodeNotFoundScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$reloadAllItemsWithCount$2$ItemListScreen$Presenter(Lcom/squareup/shared/catalog/Catalog$Local;)Lkotlin/Pair;
    .locals 3

    .line 296
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->res:Lcom/squareup/util/Res;

    const/4 v2, 0x0

    invoke-static {v0, p1, v1, v2}, Lcom/squareup/orderentry/PlaceholderCounts;->from(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/models/PageTiles;)Lcom/squareup/orderentry/PlaceholderCounts;

    move-result-object v0

    .line 297
    const-class v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 298
    invoke-interface {p1, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 299
    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/squareup/api/items/Item$Type;

    .line 300
    invoke-virtual {v1, v2}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v1

    .line 299
    invoke-virtual {p1, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllCategoryDiscountItemOrderByType(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    .line 301
    new-instance v1, Lkotlin/Pair;

    invoke-direct {v1, v0, p1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method public synthetic lambda$reloadAllItemsWithCount$3$ItemListScreen$Presenter(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1

    .line 303
    invoke-direct {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->closeCursor()V

    .line 304
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Pair;

    .line 305
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/PlaceholderCounts;

    .line 306
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 307
    invoke-virtual {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->hasView()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 310
    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->updateViewCursorWithPlaceholders(Lcom/squareup/orderentry/PlaceholderCounts;)V

    return-void
.end method

.method public synthetic lambda$reloadCursor$0$ItemListScreen$Presenter(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 5

    .line 270
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 271
    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 272
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v0}, Lcom/squareup/orderentry/category/ItemListScreen;->isAllItems()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    new-array v2, v2, [Lcom/squareup/api/items/Item$Type;

    sget-object v3, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object v3, v2, v1

    .line 274
    invoke-virtual {v0, v2}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v0

    .line 273
    invoke-virtual {p1, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readItemsWithTypes(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    return-object p1

    .line 275
    :cond_0
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v0}, Lcom/squareup/orderentry/category/ItemListScreen;->isAllDiscounts()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllDiscounts()Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    return-object p1

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v0}, Lcom/squareup/orderentry/category/ItemListScreen;->isCategory()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 278
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v0}, Lcom/squareup/orderentry/category/ItemListScreen;->getCategoryId()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    new-array v2, v2, [Lcom/squareup/api/items/Item$Type;

    sget-object v4, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object v4, v2, v1

    .line 279
    invoke-virtual {v3, v2}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v1

    .line 278
    invoke-virtual {p1, v0, v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->findCatalogItemsForCategoryId(Ljava/lang/String;Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    return-object p1

    .line 281
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    iget-object v1, v1, Lcom/squareup/orderentry/category/ItemListScreen;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic lambda$reloadCursor$1$ItemListScreen$Presenter(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 0

    .line 284
    invoke-direct {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->closeCursor()V

    .line 285
    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    iput-object p1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 286
    invoke-virtual {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->hasView()Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 289
    :cond_0
    invoke-direct {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->updateViewCursor()V

    return-void
.end method

.method onCreateAllDiscountsClicked()V
    .locals 2

    .line 356
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->favorite_tile_all_discounts:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 357
    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->DISCOUNTS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    invoke-direct {p0, v1, v0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->createPlaceholderClicked(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;)V

    return-void
.end method

.method onCreateAllGiftCardsClicked()V
    .locals 2

    .line 366
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/librarylist/R$string;->item_library_all_gift_cards:I

    .line 367
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 368
    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->GIFT_CARDS_CATEGORY:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    invoke-direct {p0, v1, v0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->createPlaceholderClicked(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;)V

    return-void
.end method

.method onCreateAllItemsClicked()V
    .locals 2

    .line 351
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->favorite_tile_all_items:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 352
    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->ALL_ITEMS:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    invoke-direct {p0, v1, v0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->createPlaceholderClicked(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;)V

    return-void
.end method

.method public onCreateDiscountClicked()V
    .locals 1

    .line 378
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    .line 379
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    invoke-interface {v0}, Lcom/squareup/ui/items/EditItemGateway;->startNewDiscount()V

    return-void
.end method

.method public onCreateItemClicked()V
    .locals 2

    .line 372
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Item Create Clicked"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 373
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    .line 374
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    sget-object v1, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    invoke-interface {v0, v1}, Lcom/squareup/ui/items/EditItemGateway;->startEditNewItemFlow(Lcom/squareup/api/items/Item$Type;)V

    return-void
.end method

.method onCreateRewardsClicked()V
    .locals 2

    .line 361
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->favorite_tile_rewards_search:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 362
    sget-object v1, Lcom/squareup/api/items/Placeholder$PlaceholderType;->REWARDS_FINDER:Lcom/squareup/api/items/Placeholder$PlaceholderType;

    invoke-direct {p0, v1, v0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->createPlaceholderClicked(Lcom/squareup/api/items/Placeholder$PlaceholderType;Ljava/lang/String;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 187
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/category/ItemListScreen;

    iput-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    .line 188
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/cogs/CatalogUpdateEvent;

    .line 189
    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$tErWZNGb3zpdOe1k1ROhxP046b4;

    invoke-direct {v1, p0}, Lcom/squareup/orderentry/category/-$$Lambda$ItemListScreen$Presenter$tErWZNGb3zpdOe1k1ROhxP046b4;-><init>(Lcom/squareup/orderentry/category/ItemListScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 188
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method onEntryClicked(Landroid/view/View;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V
    .locals 8

    .line 328
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v0}, Lcom/squareup/orderentry/category/ItemListScreen;->isCreateTile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->createTile(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;)V

    goto :goto_0

    .line 331
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->DISCOUNT:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v0, v1, :cond_1

    .line 332
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x1

    const-class v2, Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/squareup/ui/main/CheckoutEntryHandler;->discountClicked(Landroid/view/View;Ljava/lang/String;ZLjava/lang/Class;)V

    goto :goto_0

    .line 334
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v0

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v0, v1, :cond_2

    .line 335
    iget-object v2, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->entryHandler:Lcom/squareup/ui/main/CheckoutEntryHandler;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v4

    .line 336
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v5

    const/4 v6, 0x0

    const-class v7, Lcom/squareup/orderentry/category/ItemListScreen;

    move-object v3, p1

    .line 335
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/ui/main/CheckoutEntryHandler;->itemClicked(Landroid/view/View;Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/Class;)V

    :cond_2
    :goto_0
    return-void
.end method

.method protected onExitScope()V
    .locals 0

    .line 193
    invoke-direct {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->closeCursor()V

    return-void
.end method

.method onItemListScreenShown()V
    .locals 2

    .line 347
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Item List Screen Shown"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 204
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 205
    invoke-virtual {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/category/ItemListView;

    invoke-virtual {p1}, Lcom/squareup/orderentry/category/ItemListView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    .line 206
    new-instance v0, Lcom/squareup/orderentry/category/-$$Lambda$bt7I8TrFaO_uMQPPtFhQ3viEf3A;

    invoke-direct {v0, p0}, Lcom/squareup/orderentry/category/-$$Lambda$bt7I8TrFaO_uMQPPtFhQ3viEf3A;-><init>(Lcom/squareup/orderentry/category/ItemListScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 207
    invoke-direct {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->reloadCursor()V

    .line 209
    invoke-virtual {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/category/ItemListView;

    .line 213
    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v1}, Lcom/squareup/orderentry/category/ItemListScreen;->isAllItems()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->favorite_tile_all_items:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 215
    sget-object v3, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListState$Filter;

    goto :goto_0

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v1}, Lcom/squareup/orderentry/category/ItemListScreen;->isAllDiscounts()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 217
    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->favorite_tile_all_discounts:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 218
    sget-object v3, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_DISCOUNTS:Lcom/squareup/librarylist/LibraryListState$Filter;

    goto :goto_0

    .line 219
    :cond_1
    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v1}, Lcom/squareup/orderentry/category/ItemListScreen;->isCategory()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 220
    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    iget-object v1, v1, Lcom/squareup/orderentry/category/ItemListScreen;->categoryName:Ljava/lang/String;

    .line 221
    sget-object v3, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    .line 222
    iget-object v2, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    iget-object v2, v2, Lcom/squareup/orderentry/category/ItemListScreen;->categoryName:Ljava/lang/String;

    goto :goto_0

    .line 223
    :cond_2
    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    invoke-virtual {v1}, Lcom/squareup/orderentry/category/ItemListScreen;->isCreateTile()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 225
    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->item_list_add_to_grid_title:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 226
    sget-object v3, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

    .line 227
    iget-object v4, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-virtual {v4, p0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->addBarcodeScannedListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$BarcodeScannedListener;)V

    .line 231
    :goto_0
    sget-object v4, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v4, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 232
    invoke-virtual {v0, v3, v2}, Lcom/squareup/orderentry/category/ItemListView;->setEmptyItemCardViewCategory(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;)V

    .line 234
    iget-object p1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->cursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz p1, :cond_3

    .line 235
    invoke-direct {p0}, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->updateViewCursor()V

    :cond_3
    return-void

    .line 229
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->screen:Lcom/squareup/orderentry/category/ItemListScreen;

    iget-object v1, v1, Lcom/squareup/orderentry/category/ItemListScreen;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method openItemsApplet()V
    .locals 1

    .line 323
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->endEditing()V

    .line 324
    iget-object v0, p0, Lcom/squareup/orderentry/category/ItemListScreen$Presenter;->itemsAppletGateway:Lcom/squareup/ui/items/ItemsAppletGateway;

    invoke-interface {v0}, Lcom/squareup/ui/items/ItemsAppletGateway;->activateItemsApplet()V

    return-void
.end method
