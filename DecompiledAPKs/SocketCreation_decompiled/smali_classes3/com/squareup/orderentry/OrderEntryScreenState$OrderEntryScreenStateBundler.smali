.class public Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;
.super Ljava/lang/Object;
.source "OrderEntryScreenState.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/OrderEntryScreenState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OrderEntryScreenStateBundler"
.end annotation


# static fields
.field private static final IS_INTERACTION_MODE_ANIMATING_KEY:Ljava/lang/String; = "IS_INTERACTION_MODE_ANIMATING_KEY"

.field private static final IS_LIBRARY_SEARCH_ACTIVE_KEY:Ljava/lang/String; = "IS_LIBRARY_SEARCH_ACTIVE_KEY"


# instance fields
.field final synthetic this$0:Lcom/squareup/orderentry/OrderEntryScreenState;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/OrderEntryScreenState;)V
    .locals 0

    .line 595
    iput-object p1, p0, Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;->this$0:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 602
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 610
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;->this$0:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->access$000(Lcom/squareup/orderentry/OrderEntryScreenState;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    const-string v1, "IS_INTERACTION_MODE_ANIMATING_KEY"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 611
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;->this$0:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->access$100(Lcom/squareup/orderentry/OrderEntryScreenState;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    const-string v1, "IS_LIBRARY_SEARCH_ACTIVE_KEY"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 612
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;->this$0:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->access$200(Lcom/squareup/orderentry/OrderEntryScreenState;)Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->load(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 617
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;->this$0:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->access$000(Lcom/squareup/orderentry/OrderEntryScreenState;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v1, "IS_INTERACTION_MODE_ANIMATING_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 618
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;->this$0:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->access$100(Lcom/squareup/orderentry/OrderEntryScreenState;)Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v1, "IS_LIBRARY_SEARCH_ACTIVE_KEY"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 619
    iget-object v0, p0, Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;->this$0:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->access$200(Lcom/squareup/orderentry/OrderEntryScreenState;)Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->save(Landroid/os/Bundle;)V

    return-void
.end method
