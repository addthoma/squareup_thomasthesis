.class Lcom/squareup/orderentry/FlyByCoordinator$Offsets;
.super Ljava/lang/Object;
.source "FlyByCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orderentry/FlyByCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Offsets"
.end annotation


# instance fields
.field final displacement:Landroid/graphics/Point;

.field final source:Landroid/graphics/Point;


# direct methods
.method private constructor <init>(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;->source:Landroid/graphics/Point;

    .line 55
    iput-object p2, p0, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;->displacement:Landroid/graphics/Point;

    return-void
.end method

.method synthetic constructor <init>(Landroid/graphics/Point;Landroid/graphics/Point;Lcom/squareup/orderentry/FlyByCoordinator$1;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/squareup/orderentry/FlyByCoordinator$Offsets;-><init>(Landroid/graphics/Point;Landroid/graphics/Point;)V

    return-void
.end method
