.class public Lcom/squareup/orderentry/CheckoutLibraryListView;
.super Lcom/squareup/librarylist/LibraryListView;
.source "CheckoutLibraryListView.java"


# static fields
.field private static final MAX_DISABLED_TIME_MS:J = 0xbb8L


# instance fields
.field private final borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

.field private final reenableListViewRunnable:Ljava/lang/Runnable;

.field private searchBarOrNull:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Lcom/squareup/librarylist/LibraryListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const-class p2, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-interface {p1, p0}, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;->inject(Lcom/squareup/orderentry/CheckoutLibraryListView;)V

    .line 37
    new-instance p1, Lcom/squareup/marin/widgets/BorderPainter;

    invoke-direct {p1, p0}, Lcom/squareup/marin/widgets/BorderPainter;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    .line 38
    iget-object p1, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/BorderPainter;->setMultiply()V

    .line 41
    new-instance p1, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListView$TVfXvKAVjki7EmdbwMQWjUx_l70;

    invoke-direct {p1, p0}, Lcom/squareup/orderentry/-$$Lambda$CheckoutLibraryListView$TVfXvKAVjki7EmdbwMQWjUx_l70;-><init>(Lcom/squareup/orderentry/CheckoutLibraryListView;)V

    iput-object p1, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->reenableListViewRunnable:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .line 62
    invoke-super {p0, p1}, Lcom/squareup/librarylist/LibraryListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->searchBarOrNull:Landroid/view/View;

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/BorderPainter;->drawBorderInside()V

    .line 66
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    .line 67
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->borderPainter:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->drawBorders(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$new$0$CheckoutLibraryListView()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "LibraryList view should not be disabled for more than 3 seconds as a result of animations"

    .line 44
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->libraryList:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->libraryList:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->reenableListViewRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/CheckoutLibraryListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 58
    invoke-super {p0}, Lcom/squareup/librarylist/LibraryListView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 52
    invoke-super {p0}, Lcom/squareup/librarylist/LibraryListView;->onFinishInflate()V

    .line 53
    sget v0, Lcom/squareup/orderentry/R$id;->library_bar_search:I

    invoke-virtual {p0, v0}, Lcom/squareup/orderentry/CheckoutLibraryListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->searchBarOrNull:Landroid/view/View;

    return-void
.end method

.method public setListEnabled(Z)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 73
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "LibraryList set enabled: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->libraryList:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setEnabled(Z)V

    if-nez p1, :cond_0

    .line 76
    iget-object p1, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->reenableListViewRunnable:Ljava/lang/Runnable;

    const-wide/16 v0, 0xbb8

    invoke-virtual {p0, p1, v0, v1}, Lcom/squareup/orderentry/CheckoutLibraryListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 78
    :cond_0
    iget-object p1, p0, Lcom/squareup/orderentry/CheckoutLibraryListView;->reenableListViewRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, p1}, Lcom/squareup/orderentry/CheckoutLibraryListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    :goto_0
    return-void
.end method
