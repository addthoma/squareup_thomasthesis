.class public final Lcom/squareup/compvoidcontroller/BillPaymentsKt;
.super Ljava/lang/Object;
.source "BillPayments.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBillPayments.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BillPayments.kt\ncom/squareup/compvoidcontroller/BillPaymentsKt\n*L\n1#1,51:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0000\u001a\u0016\u0010\u0007\u001a\u00020\u0008*\u00020\u00022\u0008\u0008\u0001\u0010\t\u001a\u00020\nH\u0000\u001a\u000c\u0010\u000b\u001a\u00020\u0008*\u00020\u0002H\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "addZeroAmountLocalTender",
        "",
        "Lcom/squareup/payment/BillPayment;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "tenderFactory",
        "Lcom/squareup/payment/tender/TenderFactory;",
        "capture",
        "",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "captureLocalPayment",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final addZeroAmountLocalTender(Lcom/squareup/payment/BillPayment;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/tender/TenderFactory;)V
    .locals 1

    const-string v0, "$this$addZeroAmountLocalTender"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/PaymentSettings;->canUseZeroAmountTender()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 19
    invoke-virtual {p2}, Lcom/squareup/payment/tender/TenderFactory;->createZeroToReplaceCash()Lcom/squareup/payment/tender/ZeroTender$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/tender/BaseLocalTender$Builder;

    goto :goto_0

    .line 21
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/payment/tender/TenderFactory;->createZeroCash()Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/payment/tender/BaseLocalTender$Builder;

    .line 23
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/payment/BillPayment;->addLocalTender(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V

    return-void
.end method

.method public static final capture(Lcom/squareup/payment/BillPayment;Lio/reactivex/Scheduler;)Z
    .locals 1
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param

    const-string v0, "$this$capture"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    invoke-static {p0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p0

    .line 48
    invoke-virtual {p0, p1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p0

    .line 49
    sget-object p1, Lcom/squareup/compvoidcontroller/BillPaymentsKt$capture$1;->INSTANCE:Lcom/squareup/compvoidcontroller/BillPaymentsKt$capture$1;

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p0

    .line 50
    invoke-virtual {p0}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object p0

    const-string p1, "Single.just(this)\n    .o\u2026nt() }\n    .blockingGet()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public static final captureLocalPayment(Lcom/squareup/payment/BillPayment;)Z
    .locals 1

    const-string v0, "$this$captureLocalPayment"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p0}, Lcom/squareup/payment/BillPayment;->isLocalPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 32
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/squareup/payment/BillPayment;->capture(Z)Z

    move-result p0
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    move-exception p0

    .line 34
    new-instance v0, Ljava/lang/RuntimeException;

    check-cast p0, Ljava/lang/Throwable;

    invoke-direct {v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 30
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Must be local payment."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method
