.class synthetic Lcom/squareup/gen2/Gen2DenialEvent$1;
.super Ljava/lang/Object;
.source "Gen2DenialEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/gen2/Gen2DenialEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$gen2$Gen2DenialDialog$Result:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 19
    invoke-static {}, Lcom/squareup/gen2/Gen2DenialDialog$Result;->values()[Lcom/squareup/gen2/Gen2DenialDialog$Result;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/gen2/Gen2DenialEvent$1;->$SwitchMap$com$squareup$gen2$Gen2DenialDialog$Result:[I

    :try_start_0
    sget-object v0, Lcom/squareup/gen2/Gen2DenialEvent$1;->$SwitchMap$com$squareup$gen2$Gen2DenialDialog$Result:[I

    sget-object v1, Lcom/squareup/gen2/Gen2DenialDialog$Result;->OK:Lcom/squareup/gen2/Gen2DenialDialog$Result;

    invoke-virtual {v1}, Lcom/squareup/gen2/Gen2DenialDialog$Result;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/gen2/Gen2DenialEvent$1;->$SwitchMap$com$squareup$gen2$Gen2DenialDialog$Result:[I

    sget-object v1, Lcom/squareup/gen2/Gen2DenialDialog$Result;->LEARN_MORE:Lcom/squareup/gen2/Gen2DenialDialog$Result;

    invoke-virtual {v1}, Lcom/squareup/gen2/Gen2DenialDialog$Result;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/gen2/Gen2DenialEvent$1;->$SwitchMap$com$squareup$gen2$Gen2DenialDialog$Result:[I

    sget-object v1, Lcom/squareup/gen2/Gen2DenialDialog$Result;->REQUEST_A_READER:Lcom/squareup/gen2/Gen2DenialDialog$Result;

    invoke-virtual {v1}, Lcom/squareup/gen2/Gen2DenialDialog$Result;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method
