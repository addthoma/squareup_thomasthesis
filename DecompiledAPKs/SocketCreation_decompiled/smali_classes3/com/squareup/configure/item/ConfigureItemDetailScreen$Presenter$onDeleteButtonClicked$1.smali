.class public final Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDeleteButtonClicked$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "ConfigureItemDetailScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onDeleteButtonClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"
    }
    d2 = {
        "com/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDeleteButtonClicked$1",
        "Lcom/squareup/permissions/PermissionGatekeeper$When;",
        "success",
        "",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1450
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDeleteButtonClicked$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 1

    .line 1452
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$onDeleteButtonClicked$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$doDelete(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V

    return-void
.end method
