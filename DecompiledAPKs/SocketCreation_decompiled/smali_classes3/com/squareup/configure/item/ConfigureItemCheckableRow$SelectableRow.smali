.class final Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;
.super Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;
.source "ConfigureItemCheckableRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/ConfigureItemCheckableRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SelectableRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0006H\u0016R$\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068V@VX\u0096\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\nR$\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u000b8V@VX\u0096\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0005\u001a\u00020\u00118V@VX\u0096\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R(\u0010\u0005\u001a\u0004\u0018\u00010\u000b2\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u000b8V@VX\u0096\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0017\u0010\u000e\"\u0004\u0008\u0018\u0010\u0010\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;",
        "Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;",
        "nohoSelectable",
        "Lcom/squareup/noho/NohoSelectable;",
        "(Lcom/squareup/noho/NohoSelectable;)V",
        "value",
        "",
        "isEnabled",
        "()Z",
        "setEnabled",
        "(Z)V",
        "",
        "label",
        "getLabel",
        "()Ljava/lang/String;",
        "setLabel",
        "(Ljava/lang/String;)V",
        "",
        "rowId",
        "getRowId",
        "()I",
        "setRowId",
        "(I)V",
        "getValue",
        "setValue",
        "setSingleSelect",
        "",
        "isSingleSelect",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final nohoSelectable:Lcom/squareup/noho/NohoSelectable;


# direct methods
.method public constructor <init>(Lcom/squareup/noho/NohoSelectable;)V
    .locals 2

    const-string v0, "nohoSelectable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    move-object v0, p1

    check-cast v0, Landroid/view/View;

    move-object v1, p1

    check-cast v1, Landroid/widget/Checkable;

    invoke-direct {p0, v0, v1}, Lcom/squareup/configure/item/ConfigureItemCheckableRow$ConfigureItemCheckableRowAdapter;-><init>(Landroid/view/View;Landroid/widget/Checkable;)V

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;->nohoSelectable:Lcom/squareup/noho/NohoSelectable;

    return-void
.end method


# virtual methods
.method public getLabel()Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;->nohoSelectable:Lcom/squareup/noho/NohoSelectable;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoSelectable;->getLabel()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRowId()I
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;->nohoSelectable:Lcom/squareup/noho/NohoSelectable;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoSelectable;->getId()I

    move-result v0

    return v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;->nohoSelectable:Lcom/squareup/noho/NohoSelectable;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoSelectable;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;->nohoSelectable:Lcom/squareup/noho/NohoSelectable;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoSelectable;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;->nohoSelectable:Lcom/squareup/noho/NohoSelectable;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoSelectable;->setEnabled(Z)V

    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;->nohoSelectable:Lcom/squareup/noho/NohoSelectable;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoSelectable;->setLabel(Ljava/lang/String;)V

    return-void
.end method

.method public setRowId(I)V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;->nohoSelectable:Lcom/squareup/noho/NohoSelectable;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoSelectable;->setId(I)V

    return-void
.end method

.method public setSingleSelect(Z)V
    .locals 0

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemCheckableRow$SelectableRow;->nohoSelectable:Lcom/squareup/noho/NohoSelectable;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoSelectable;->setValue(Ljava/lang/String;)V

    return-void
.end method
