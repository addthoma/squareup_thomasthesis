.class final Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateViews$commitCommand$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ConfigureItemDetailScreen.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->buildAndPopulateViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateViews$commitCommand$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 181
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateViews$commitCommand$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 470
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateViews$commitCommand$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getHost$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemHost;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    iget-object v2, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateViews$commitCommand$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-static {v2}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->access$getScopeRunner$p(Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;)Lcom/squareup/configure/item/ConfigureItemScopeRunner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/configure/item/ConfigureItemScopeRunner;->getState()Lcom/squareup/configure/item/ConfigureItemState;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/configure/item/ConfigureItemHost;->onCommit(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/ConfigureItemState;)V

    .line 471
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter$buildAndPopulateViews$commitCommand$1;->this$0:Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/configure/item/ConfigureItemDetailScreen$Presenter;->onCommitSelected()V

    return-void
.end method
