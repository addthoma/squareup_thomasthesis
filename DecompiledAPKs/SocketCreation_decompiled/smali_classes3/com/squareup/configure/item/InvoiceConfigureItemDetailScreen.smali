.class public Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;
.super Lcom/squareup/configure/item/InConfigureItemScope;
.source "InvoiceConfigureItemDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen$Component;,
        Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen$AtLeastZeroAmountValidationModule;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/configure/item/-$$Lambda$InvoiceConfigureItemDetailScreen$sDg_kv-OAF51-Su_9ODikQnjyyw;->INSTANCE:Lcom/squareup/configure/item/-$$Lambda$InvoiceConfigureItemDetailScreen$sDg_kv-OAF51-Su_9ODikQnjyyw;

    .line 75
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/configure/item/ConfigureItemScope;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/configure/item/InConfigureItemScope;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;I)V
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {v0, p1, p2}, Lcom/squareup/configure/item/ConfigureItemScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;I)V

    invoke-direct {p0, v0}, Lcom/squareup/configure/item/InConfigureItemScope;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;)V
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-direct {v0, p1, p2}, Lcom/squareup/configure/item/ConfigureItemScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/configure/item/WorkingItem;)V

    invoke-direct {p0, v0}, Lcom/squareup/configure/item/InConfigureItemScope;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;
    .locals 1

    .line 76
    const-class v0, Lcom/squareup/configure/item/ConfigureItemScope;

    .line 77
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/configure/item/ConfigureItemScope;

    .line 78
    new-instance v0, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;

    invoke-direct {v0, p0}, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;-><init>(Lcom/squareup/configure/item/ConfigureItemScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 49
    invoke-super {p0, p1, p2}, Lcom/squareup/configure/item/InConfigureItemScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 50
    iget-object v0, p0, Lcom/squareup/configure/item/InvoiceConfigureItemDetailScreen;->configureItemPath:Lcom/squareup/configure/item/ConfigureItemScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CONFIGURE_ITEM_DETAIL:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 45
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 41
    sget v0, Lcom/squareup/configure/item/R$layout;->configure_item_detail_view:I

    return v0
.end method
