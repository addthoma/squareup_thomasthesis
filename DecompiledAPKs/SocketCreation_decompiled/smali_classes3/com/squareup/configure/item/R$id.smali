.class public final Lcom/squareup/configure/item/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/configure/item/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final buttons_container:I = 0x7f0a027a

.field public static final checkable_row:I = 0x7f0a0316

.field public static final conditional_taxes_help_text:I = 0x7f0a0374

.field public static final configure_item_checkable_row:I = 0x7f0a037f

.field public static final configure_item_description:I = 0x7f0a0380

.field public static final configure_item_detail_view:I = 0x7f0a0381

.field public static final configure_item_duration:I = 0x7f0a0382

.field public static final configure_item_duration_container:I = 0x7f0a0383

.field public static final configure_item_final_duration:I = 0x7f0a0384

.field public static final configure_item_gap_duration:I = 0x7f0a0385

.field public static final configure_item_gap_time_container:I = 0x7f0a0386

.field public static final configure_item_gap_time_toggle:I = 0x7f0a0387

.field public static final configure_item_initial_duration:I = 0x7f0a0388

.field public static final configure_item_note:I = 0x7f0a0389

.field public static final content:I = 0x7f0a03a3

.field public static final edit_quantity_row:I = 0x7f0a065f

.field public static final edit_unit_quantity_row:I = 0x7f0a068c

.field public static final fixed_price_override_button:I = 0x7f0a0760

.field public static final fixed_price_override_container:I = 0x7f0a0761

.field public static final item_comp_button_row:I = 0x7f0a08d1

.field public static final item_remove_button_row:I = 0x7f0a08e1

.field public static final item_uncomp_button_row:I = 0x7f0a08e4

.field public static final item_void_button_row:I = 0x7f0a08e6

.field public static final notes_section_header:I = 0x7f0a0a49

.field public static final price_button:I = 0x7f0a0c50

.field public static final price_edit_text:I = 0x7f0a0c51

.field public static final price_header:I = 0x7f0a0c53

.field public static final quantity_footer_text:I = 0x7f0a0c9d

.field public static final quantity_section_header:I = 0x7f0a0c9f

.field public static final section_checkablegroup:I = 0x7f0a0e36

.field public static final section_container:I = 0x7f0a0e37

.field public static final section_header:I = 0x7f0a0e38

.field public static final title_text:I = 0x7f0a1047

.field public static final void_comp_cancel_button:I = 0x7f0a1109

.field public static final void_comp_cancel_button_text:I = 0x7f0a110a

.field public static final void_comp_help_text:I = 0x7f0a110b

.field public static final void_comp_primary_button:I = 0x7f0a110c

.field public static final void_comp_reason_header:I = 0x7f0a110d

.field public static final void_comp_reason_radios:I = 0x7f0a110e


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
