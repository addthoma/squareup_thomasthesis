.class public final Lcom/squareup/configure/item/ScaleItemizationAddEvent;
.super Lcom/squareup/configure/item/ScaleLogEvent;
.source "ScaleLogEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/ScaleItemizationAddEvent$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nR\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/configure/item/ScaleItemizationAddEvent;",
        "Lcom/squareup/configure/item/ScaleLogEvent;",
        "weight_recorded",
        "Ljava/math/BigDecimal;",
        "unit_selected",
        "",
        "quantityEntryType",
        "Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;",
        "hardwareScale",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "(Ljava/math/BigDecimal;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V",
        "measure_method",
        "Lcom/squareup/configure/item/MeasurementMethod;",
        "getMeasure_method",
        "()Lcom/squareup/configure/item/MeasurementMethod;",
        "measure_type",
        "getMeasure_type",
        "()Ljava/lang/String;",
        "getUnit_selected",
        "getWeight_recorded",
        "()Ljava/math/BigDecimal;",
        "Companion",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/configure/item/ScaleItemizationAddEvent$Companion;

.field public static final MEASUREMENT_TYPE_NET:Ljava/lang/String; = "net"


# instance fields
.field private final measure_method:Lcom/squareup/configure/item/MeasurementMethod;

.field private final measure_type:Ljava/lang/String;

.field private final unit_selected:Ljava/lang/String;

.field private final weight_recorded:Ljava/math/BigDecimal;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/configure/item/ScaleItemizationAddEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/configure/item/ScaleItemizationAddEvent$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/configure/item/ScaleItemizationAddEvent;->Companion:Lcom/squareup/configure/item/ScaleItemizationAddEvent$Companion;

    return-void
.end method

.method public constructor <init>(Ljava/math/BigDecimal;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V
    .locals 3

    const-string/jumbo v0, "weight_recorded"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SCALE_ITEMIZATION_ADD:Lcom/squareup/analytics/RegisterTapName;

    iget-object v1, v1, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    const-string v2, "SCALE_ITEMIZATION_ADD.value"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p4, v2}, Lcom/squareup/configure/item/ScaleLogEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$HardwareScale;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/configure/item/ScaleItemizationAddEvent;->weight_recorded:Ljava/math/BigDecimal;

    iput-object p2, p0, Lcom/squareup/configure/item/ScaleItemizationAddEvent;->unit_selected:Ljava/lang/String;

    if-eqz p3, :cond_0

    .line 46
    invoke-static {p3}, Lcom/squareup/configure/item/ScaleLogEventKt;->access$toMeasurementType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v2

    :goto_0
    iput-object p1, p0, Lcom/squareup/configure/item/ScaleItemizationAddEvent;->measure_type:Ljava/lang/String;

    if-eqz p3, :cond_1

    .line 47
    invoke-static {p3}, Lcom/squareup/configure/item/ScaleLogEventKt;->access$toMeasurementMethod(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)Lcom/squareup/configure/item/MeasurementMethod;

    move-result-object v2

    :cond_1
    iput-object v2, p0, Lcom/squareup/configure/item/ScaleItemizationAddEvent;->measure_method:Lcom/squareup/configure/item/MeasurementMethod;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/math/BigDecimal;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/scales/ScaleTracker$HardwareScale;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 44
    check-cast p4, Lcom/squareup/scales/ScaleTracker$HardwareScale;

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/configure/item/ScaleItemizationAddEvent;-><init>(Ljava/math/BigDecimal;Ljava/lang/String;Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    return-void
.end method


# virtual methods
.method public final getMeasure_method()Lcom/squareup/configure/item/MeasurementMethod;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/configure/item/ScaleItemizationAddEvent;->measure_method:Lcom/squareup/configure/item/MeasurementMethod;

    return-object v0
.end method

.method public final getMeasure_type()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/configure/item/ScaleItemizationAddEvent;->measure_type:Ljava/lang/String;

    return-object v0
.end method

.method public final getUnit_selected()Ljava/lang/String;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/configure/item/ScaleItemizationAddEvent;->unit_selected:Ljava/lang/String;

    return-object v0
.end method

.method public final getWeight_recorded()Ljava/math/BigDecimal;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/configure/item/ScaleItemizationAddEvent;->weight_recorded:Ljava/math/BigDecimal;

    return-object v0
.end method
