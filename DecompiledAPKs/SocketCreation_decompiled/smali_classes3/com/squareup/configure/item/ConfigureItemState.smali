.class public abstract Lcom/squareup/configure/item/ConfigureItemState;
.super Ljava/lang/Object;
.source "ConfigureItemState.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/configure/item/ConfigureItemState$CommitResult;
    }
.end annotation


# static fields
.field protected static final HAS_LAST_SELECTED_OPTION:Ljava/lang/String; = "HAS_LAST_SELECTED_OPTION"

.field protected static final LAST_SELECTED_OPTION:Ljava/lang/String; = "LAST_SELECTED_OPTION"

.field public static final MAXIMUM_ITEM_QUANTITY:Ljava/math/BigDecimal;

.field protected static final PREVIOUS_SELECTED_OPTION_PREFIX:Ljava/lang/String; = "PREVIOUS_SELECTED_OPTION_"

.field protected static final PREVIOUS_SELECTED_OPTION_SIZE:Ljava/lang/String; = "PREVIOUS_SELECTED_OPTION_SIZE"

.field protected static final PREVIOUS_VARIABLE_PRICE_KEY:Ljava/lang/String; = "PREVIOUS_VARIABLE_PRICE_KEY"

.field protected static final SELECTED_OPTION_PREFIX:Ljava/lang/String; = "SELECTED_OPTION_"

.field protected static final SELECTED_OPTION_SIZE:Ljava/lang/String; = "SELECTED_OPTION_SIZE"


# instance fields
.field private lastSelectedOption:Ljava/lang/String;

.field private previousSelectedOptionValues:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private previousVariablePrice:J

.field private previousVariation:Lcom/squareup/checkout/OrderVariation;

.field private selectedOptionValues:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/32 v0, 0xf423f

    .line 40
    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, Lcom/squareup/configure/item/ConfigureItemState;->MAXIMUM_ITEM_QUANTITY:Ljava/math/BigDecimal;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->selectedOptionValues:Ljava/util/Set;

    .line 54
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousSelectedOptionValues:Ljava/util/Set;

    return-void
.end method

.method private getSetElementKey(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .line 429
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private loadStringSetFromBundle(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 420
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p2

    .line 421
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p2, :cond_0

    .line 423
    invoke-direct {p0, p3, v1}, Lcom/squareup/configure/item/ConfigureItemState;->getSetElementKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private saveStringSetToBundle(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 408
    invoke-interface {p3}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 410
    invoke-interface {p3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 p3, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    add-int/lit8 v1, p3, 0x1

    .line 411
    invoke-direct {p0, p4, p3}, Lcom/squareup/configure/item/ConfigureItemState;->getSetElementKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move p3, v1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public cacheCurrentVariationAndVariablePrice()V
    .locals 2

    .line 237
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemState;->getCurrentVariablePrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    .line 239
    iput-wide v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousVariablePrice:J

    goto :goto_0

    .line 241
    :cond_0
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousVariablePrice:J

    .line 243
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemState;->getSelectedVariation()Lcom/squareup/checkout/OrderVariation;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousVariation:Lcom/squareup/checkout/OrderVariation;

    .line 245
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousSelectedOptionValues:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 246
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousSelectedOptionValues:Ljava/util/Set;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemState;->selectedOptionValues:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 247
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->lastSelectedOption:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 248
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousSelectedOptionValues:Ljava/util/Set;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemState;->lastSelectedOption:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public abstract cacheHasHiddenModifier(Z)V
.end method

.method public abstract clearQuantityPrecisionOverride()V
.end method

.method public abstract commit()Lcom/squareup/configure/item/ConfigureItemState$CommitResult;
.end method

.method public abstract compItem(Lcom/squareup/shared/catalog/models/CatalogDiscount;Lcom/squareup/protos/client/Employee;)V
.end method

.method public abstract delete()V
.end method

.method public deselectItemOptionValue(Ljava/lang/String;)V
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->selectedOptionValues:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public abstract getAppliedDiscounts()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Discount;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAppliedTaxes()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getBackingType()Lcom/squareup/protos/client/bills/Itemization$Configuration$BackingType;
.end method

.method protected abstract getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
.end method

.method public abstract getCurrentVariablePrice()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getDuration()Lorg/threeten/bp/Duration;
.end method

.method public abstract getIntermissions()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getItemDescription()Ljava/lang/String;
.end method

.method public abstract getItemOptions()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getItemType()Lcom/squareup/api/items/Item$Type;
.end method

.method public abstract getModifierLists()Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifierList;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getNote()Ljava/lang/String;
.end method

.method public abstract getOverridePrice()Lcom/squareup/protos/common/Money;
.end method

.method public getPreviousVariation()Lcom/squareup/checkout/OrderVariation;
    .locals 1

    .line 256
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousVariation:Lcom/squareup/checkout/OrderVariation;

    return-object v0
.end method

.method public abstract getQuantity()Ljava/math/BigDecimal;
.end method

.method public abstract getQuantityEntryType()Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;
.end method

.method public abstract getQuantityPrecision()I
.end method

.method public abstract getSelectedDiningOption()Lcom/squareup/checkout/DiningOption;
.end method

.method public getSelectedItemOptionValues()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 347
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->selectedOptionValues:Ljava/util/Set;

    return-object v0
.end method

.method public abstract getSelectedModifiers()Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/OrderModifier;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getSelectedVariation()Lcom/squareup/checkout/OrderVariation;
.end method

.method public abstract getTotal()Lcom/squareup/protos/common/Money;
.end method

.method public abstract getUnitAbbreviation()Ljava/lang/String;
.end method

.method public abstract getVariations()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/OrderVariation;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isCompable()Z
.end method

.method public abstract isConfigurationLockedFromScale()Z
.end method

.method public abstract isConfigurationLockedFromTicket()Z
.end method

.method public abstract isDeletable()Z
.end method

.method public abstract isGiftCard()Z
.end method

.method public abstract isService()Z
.end method

.method public abstract isUncompable()Z
.end method

.method public abstract isUnitPriced()Z
.end method

.method public abstract isVoidable()Z
.end method

.method public itemOptionValueSelectedByUser(Ljava/lang/String;)V
    .locals 0

    .line 343
    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemState;->lastSelectedOption:Ljava/lang/String;

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_1

    const-string v0, "PREVIOUS_VARIABLE_PRICE_KEY"

    .line 354
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousVariablePrice:J

    const-string v0, "HAS_LAST_SELECTED_OPTION"

    .line 357
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "LAST_SELECTED_OPTION"

    .line 359
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->lastSelectedOption:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 361
    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->lastSelectedOption:Ljava/lang/String;

    :goto_0
    const-string v0, "SELECTED_OPTION_SIZE"

    const-string v1, "SELECTED_OPTION_"

    .line 364
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/configure/item/ConfigureItemState;->loadStringSetFromBundle(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->selectedOptionValues:Ljava/util/Set;

    const-string v0, "PREVIOUS_SELECTED_OPTION_SIZE"

    const-string v1, "PREVIOUS_SELECTED_OPTION_"

    .line 370
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/configure/item/ConfigureItemState;->loadStringSetFromBundle(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousSelectedOptionValues:Ljava/util/Set;

    :cond_1
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 3

    .line 379
    iget-wide v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousVariablePrice:J

    const-string v2, "PREVIOUS_VARIABLE_PRICE_KEY"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 381
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->lastSelectedOption:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "HAS_LAST_SELECTED_OPTION"

    .line 382
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    if-eqz v0, :cond_1

    .line 384
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->lastSelectedOption:Ljava/lang/String;

    const-string v1, "LAST_SELECTED_OPTION"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :cond_1
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->selectedOptionValues:Ljava/util/Set;

    const-string v1, "SELECTED_OPTION_SIZE"

    const-string v2, "SELECTED_OPTION_"

    invoke-direct {p0, p1, v1, v0, v2}, Lcom/squareup/configure/item/ConfigureItemState;->saveStringSetToBundle(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)V

    .line 394
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousSelectedOptionValues:Ljava/util/Set;

    const-string v1, "PREVIOUS_SELECTED_OPTION_SIZE"

    const-string v2, "PREVIOUS_SELECTED_OPTION_"

    invoke-direct {p0, p1, v1, v0, v2}, Lcom/squareup/configure/item/ConfigureItemState;->saveStringSetToBundle(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;)V

    return-void
.end method

.method public abstract orderDestination()Lcom/squareup/checkout/OrderDestination;
.end method

.method public revertToPreviousVariationAndVariablePrice()V
    .locals 6

    .line 267
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousVariation:Lcom/squareup/checkout/OrderVariation;

    invoke-virtual {p0, v0}, Lcom/squareup/configure/item/ConfigureItemState;->setSelectedVariation(Lcom/squareup/checkout/OrderVariation;)V

    .line 268
    iget-wide v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousVariablePrice:J

    const/4 v2, 0x0

    const-wide/16 v3, -0x1

    cmp-long v5, v0, v3

    if-nez v5, :cond_0

    .line 269
    invoke-virtual {p0, v2}, Lcom/squareup/configure/item/ConfigureItemState;->setCurrentVariablePrice(Lcom/squareup/protos/common/Money;)V

    goto :goto_0

    .line 271
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/configure/item/ConfigureItemState;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/configure/item/ConfigureItemState;->setCurrentVariablePrice(Lcom/squareup/protos/common/Money;)V

    .line 274
    :goto_0
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->selectedOptionValues:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 275
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->selectedOptionValues:Ljava/util/Set;

    iget-object v1, p0, Lcom/squareup/configure/item/ConfigureItemState;->previousSelectedOptionValues:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 276
    iput-object v2, p0, Lcom/squareup/configure/item/ConfigureItemState;->lastSelectedOption:Ljava/lang/String;

    return-void
.end method

.method public selectItemOptionValue(Ljava/lang/String;)V
    .locals 1

    .line 322
    iget-object v0, p0, Lcom/squareup/configure/item/ConfigureItemState;->selectedOptionValues:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public abstract setCurrentVariablePrice(Lcom/squareup/protos/common/Money;)V
.end method

.method public abstract setDiscountApplied(Lcom/squareup/checkout/Discount;Z)V
.end method

.method public abstract setDiscountApplied(Lcom/squareup/checkout/Discount;ZLcom/squareup/protos/client/Employee;)V
.end method

.method public abstract setDuration(Lorg/threeten/bp/Duration;)V
.end method

.method public abstract setIntermissions(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Intermission;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setName(Ljava/lang/String;)V
.end method

.method public abstract setNote(Ljava/lang/String;)V
.end method

.method public abstract setOverridePrice(Lcom/squareup/protos/common/Money;)V
.end method

.method public abstract setQuantity(Ljava/math/BigDecimal;)V
.end method

.method public abstract setQuantityEntryType(Lcom/squareup/protos/client/bills/Itemization$QuantityEntryType;)V
.end method

.method public abstract setQuantityPrecisionOverride(I)V
.end method

.method public abstract setSelectedDiningOption(Lcom/squareup/checkout/DiningOption;)V
.end method

.method public abstract setSelectedVariation(Lcom/squareup/checkout/OrderVariation;)V
.end method

.method public abstract setTaxApplied(Lcom/squareup/checkout/Tax;Z)V
.end method

.method public abstract uncompItem()V
.end method

.method public abstract voidItem(Ljava/lang/String;Lcom/squareup/protos/client/Employee;)V
.end method
