.class public abstract Lcom/squareup/configure/item/ScaleLogEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "ScaleLogEvent.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nScaleLogEvent.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ScaleLogEvent.kt\ncom/squareup/configure/item/ScaleLogEvent\n*L\n1#1,96:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001B#\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0008R\u0013\u0010\t\u001a\u0004\u0018\u00010\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u0082\u0001\u0003\r\u000e\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/configure/item/ScaleLogEvent;",
        "Lcom/squareup/eventstream/v1/EventStreamEvent;",
        "eventName",
        "Lcom/squareup/eventstream/v1/EventStream$Name;",
        "eventValue",
        "",
        "hardwareScale",
        "Lcom/squareup/scales/ScaleTracker$HardwareScale;",
        "(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V",
        "scale",
        "Lcom/squareup/configure/item/ScaleInfo;",
        "getScale",
        "()Lcom/squareup/configure/item/ScaleInfo;",
        "Lcom/squareup/configure/item/ScaleItemizationAddEvent;",
        "Lcom/squareup/configure/item/ScaleItemizationCancelEvent;",
        "Lcom/squareup/configure/item/ScaleDisplayErrorEvent;",
        "configure-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final scale:Lcom/squareup/configure/item/ScaleInfo;


# direct methods
.method private constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V
    .locals 1

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    if-eqz p3, :cond_0

    .line 29
    new-instance p1, Lcom/squareup/configure/item/ScaleInfo;

    .line 30
    invoke-virtual {p3}, Lcom/squareup/scales/ScaleTracker$HardwareScale;->getDeviceName()Ljava/lang/String;

    move-result-object p2

    .line 31
    invoke-virtual {p3}, Lcom/squareup/scales/ScaleTracker$HardwareScale;->getManufacturer()Lcom/squareup/scales/ScaleTracker$Manufacturer;

    move-result-object v0

    .line 32
    invoke-virtual {p3}, Lcom/squareup/scales/ScaleTracker$HardwareScale;->getConnectionType()Lcom/squareup/scales/ScaleTracker$ConnectionType;

    move-result-object p3

    .line 29
    invoke-direct {p1, p2, v0, p3}, Lcom/squareup/configure/item/ScaleInfo;-><init>(Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$Manufacturer;Lcom/squareup/scales/ScaleTracker$ConnectionType;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 28
    :goto_0
    iput-object p1, p0, Lcom/squareup/configure/item/ScaleLogEvent;->scale:Lcom/squareup/configure/item/ScaleInfo;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$HardwareScale;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 26
    check-cast p3, Lcom/squareup/scales/ScaleTracker$HardwareScale;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/configure/item/ScaleLogEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$HardwareScale;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/configure/item/ScaleLogEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    return-void
.end method


# virtual methods
.method public final getScale()Lcom/squareup/configure/item/ScaleInfo;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/configure/item/ScaleLogEvent;->scale:Lcom/squareup/configure/item/ScaleInfo;

    return-object v0
.end method
