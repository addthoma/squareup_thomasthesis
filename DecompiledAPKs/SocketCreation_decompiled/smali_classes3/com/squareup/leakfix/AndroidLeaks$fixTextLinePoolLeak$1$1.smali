.class public final Lcom/squareup/leakfix/AndroidLeaks$fixTextLinePoolLeak$1$1;
.super Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;
.source "AndroidLeaks.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/leakfix/AndroidLeaks$fixTextLinePoolLeak$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAndroidLeaks.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AndroidLeaks.kt\ncom/squareup/leakfix/AndroidLeaks$fixTextLinePoolLeak$1$1\n*L\n1#1,431:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/leakfix/AndroidLeaks$fixTextLinePoolLeak$1$1",
        "Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;",
        "onActivityDestroyed",
        "",
        "activity",
        "Landroid/app/Activity;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sCached:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/leakfix/AndroidLeaks$fixTextLinePoolLeak$1$1;->$sCached:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/squareup/util/ActivityLifecycleCallbacksAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 4

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/leakfix/AndroidLeaks$fixTextLinePoolLeak$1$1;->$sCached:Ljava/lang/Object;

    monitor-enter p1

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/squareup/leakfix/AndroidLeaks$fixTextLinePoolLeak$1$1;->$sCached:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    .line 102
    iget-object v2, p0, Lcom/squareup/leakfix/AndroidLeaks$fixTextLinePoolLeak$1$1;->$sCached:Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 104
    :cond_0
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1

    throw v0
.end method
