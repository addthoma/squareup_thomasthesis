.class public final Lcom/squareup/librarylist/RealLibraryListPresenter$adapter$1;
.super Ljava/lang/Object;
.source "RealLibraryListPresenter.kt"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/librarylist/RealLibraryListPresenter;-><init>(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/librarylist/LibraryListAssistant;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListAdapter$Factory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/librarylist/RealLibraryListPresenter$adapter$1",
        "Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;",
        "isItemDisabled",
        "",
        "entry",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/librarylist/RealLibraryListPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/librarylist/RealLibraryListPresenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 51
    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListPresenter$adapter$1;->this$0:Lcom/squareup/librarylist/RealLibraryListPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isItemDisabled(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListPresenter$adapter$1;->this$0:Lcom/squareup/librarylist/RealLibraryListPresenter;

    invoke-static {v0, p1}, Lcom/squareup/librarylist/RealLibraryListPresenter;->access$isLibraryEntryEnabled(Lcom/squareup/librarylist/RealLibraryListPresenter;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method
