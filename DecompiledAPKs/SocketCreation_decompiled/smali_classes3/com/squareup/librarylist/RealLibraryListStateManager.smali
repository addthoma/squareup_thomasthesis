.class public final Lcom/squareup/librarylist/RealLibraryListStateManager;
.super Ljava/lang/Object;
.source "CheckoutLibraryListStateManager.kt"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListStateManager;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u0016\u0010)\u001a\u0008\u0012\u0004\u0012\u00020+0*2\u0006\u0010,\u001a\u00020\u001dH\u0016J\u000e\u0010-\u001a\u0008\u0012\u0004\u0012\u00020+0*H\u0016J\u0016\u0010.\u001a\u0008\u0012\u0004\u0012\u00020+0*2\u0006\u0010,\u001a\u00020\u001dH\u0016J\u0008\u0010/\u001a\u00020\u001dH\u0016J\u000e\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b00H\u0016J\u0010\u00101\u001a\u0002022\u0006\u00103\u001a\u000204H\u0016J\u0018\u00105\u001a\u0002022\u0006\u00106\u001a\u00020+2\u0006\u00107\u001a\u000208H\u0016J\u0010\u00109\u001a\u0002022\u0006\u0010:\u001a\u00020;H\u0016J\u0010\u0010<\u001a\u0002022\u0006\u0010=\u001a\u00020\u001bH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0013\u001a\u0004\u0018\u00010\u00148WX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016R\u0016\u0010\u0017\u001a\u0004\u0018\u00010\u00148WX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001c\u001a\u00020\u001d8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\u001eR\u001a\u0010\u001f\u001a\u00020\u001d8VX\u0097\u0004\u00a2\u0006\u000c\u0012\u0004\u0008 \u0010!\u001a\u0004\u0008\u001f\u0010\u001eR$\u0010$\u001a\u00020#2\u0006\u0010\"\u001a\u00020#8W@VX\u0096\u000e\u00a2\u0006\u000c\u001a\u0004\u0008%\u0010&\"\u0004\u0008\'\u0010(R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006>"
    }
    d2 = {
        "Lcom/squareup/librarylist/RealLibraryListStateManager;",
        "Lcom/squareup/librarylist/LibraryListStateManager;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "device",
        "Lcom/squareup/util/Device;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "redeemRewardsFlow",
        "Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;",
        "giftCardActivationFlow",
        "Lcom/squareup/giftcardactivation/GiftCardActivationFlow;",
        "libraryListConfiguration",
        "Lcom/squareup/librarylist/LibraryListConfiguration;",
        "stateSaver",
        "Lcom/squareup/librarylist/LibraryListStateSaver;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/giftcardactivation/GiftCardActivationFlow;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListStateSaver;)V",
        "currentCategoryId",
        "",
        "getCurrentCategoryId",
        "()Ljava/lang/String;",
        "currentCategoryName",
        "getCurrentCategoryName",
        "holder",
        "Lcom/jakewharton/rxrelay/BehaviorRelay;",
        "Lcom/squareup/librarylist/CheckoutLibraryListState;",
        "isNarrowTablet",
        "",
        "()Z",
        "isTopLevel",
        "isTopLevel$annotations",
        "()V",
        "filter",
        "Lcom/squareup/librarylist/LibraryListState$Filter;",
        "libraryFilter",
        "getLibraryFilter",
        "()Lcom/squareup/librarylist/LibraryListState$Filter;",
        "setLibraryFilter",
        "(Lcom/squareup/librarylist/LibraryListState$Filter;)V",
        "buildAllItemsPlaceholders",
        "",
        "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
        "shouldHideRewardsAndGiftCards",
        "buildCategoryPlaceholders",
        "buildGiftCardPlaceholder",
        "goBack",
        "Lrx/Observable;",
        "onCogsUpdate",
        "",
        "event",
        "Lcom/squareup/cogs/CatalogUpdateEvent;",
        "placeholderClicked",
        "placeholder",
        "flow",
        "Lflow/Flow;",
        "setModeToSingleCategory",
        "entry",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "updateHolder",
        "value",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final device:Lcom/squareup/util/Device;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final giftCardActivationFlow:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

.field private final holder:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/librarylist/CheckoutLibraryListState;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryListConfiguration:Lcom/squareup/librarylist/LibraryListConfiguration;

.field private final redeemRewardsFlow:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final stateSaver:Lcom/squareup/librarylist/LibraryListStateSaver;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;Lcom/squareup/giftcardactivation/GiftCardActivationFlow;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListStateSaver;)V
    .locals 1

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "redeemRewardsFlow"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "giftCardActivationFlow"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "libraryListConfiguration"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stateSaver"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p3, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->device:Lcom/squareup/util/Device;

    iput-object p4, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p5, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->redeemRewardsFlow:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    iput-object p6, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->giftCardActivationFlow:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    iput-object p7, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->libraryListConfiguration:Lcom/squareup/librarylist/LibraryListConfiguration;

    iput-object p8, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->stateSaver:Lcom/squareup/librarylist/LibraryListStateSaver;

    .line 69
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->stateSaver:Lcom/squareup/librarylist/LibraryListStateSaver;

    invoke-interface {p1}, Lcom/squareup/librarylist/LibraryListStateSaver;->load()Lcom/squareup/librarylist/CheckoutLibraryListState;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create(stateSaver.load())"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->holder:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method

.method private final isNarrowTablet()Z
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic isTopLevel$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use [#topLevel] instead."
    .end annotation

    return-void
.end method

.method private final updateHolder(Lcom/squareup/librarylist/CheckoutLibraryListState;)V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->stateSaver:Lcom/squareup/librarylist/LibraryListStateSaver;

    invoke-interface {v0, p1}, Lcom/squareup/librarylist/LibraryListStateSaver;->save(Lcom/squareup/librarylist/CheckoutLibraryListState;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->holder:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public buildAllItemsPlaceholders(Z)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation

    .line 188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 189
    invoke-direct {p0}, Lcom/squareup/librarylist/RealLibraryListStateManager;->isNarrowTablet()Z

    move-result v1

    if-nez v1, :cond_1

    .line 190
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->libraryListConfiguration:Lcom/squareup/librarylist/LibraryListConfiguration;

    invoke-interface {v1}, Lcom/squareup/librarylist/LibraryListConfiguration;->getTopLevelPlaceholders()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->REWARDS_FLOW:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->canUseRewards()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    .line 194
    sget-object v1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->REWARDS_FLOW:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->libraryListConfiguration:Lcom/squareup/librarylist/LibraryListConfiguration;

    invoke-interface {v1}, Lcom/squareup/librarylist/LibraryListConfiguration;->getTopLevelPlaceholders()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/GiftCardSettings;->canUseGiftCards()Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    .line 200
    sget-object p1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public buildCategoryPlaceholders()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation

    .line 151
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->libraryListConfiguration:Lcom/squareup/librarylist/LibraryListConfiguration;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListConfiguration;->getTopLevelPlaceholders()Ljava/util/List;

    move-result-object v0

    .line 152
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 154
    invoke-direct {p0}, Lcom/squareup/librarylist/RealLibraryListStateManager;->isNarrowTablet()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 155
    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->REWARDS_FLOW:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->canUseRewards()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 156
    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->REWARDS_FLOW:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    :cond_0
    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 160
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getGiftCardSettings()Lcom/squareup/settings/server/GiftCardSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/GiftCardSettings;->canUseGiftCards()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 162
    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    :cond_1
    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 167
    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    :cond_2
    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_SERVICES:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 171
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->LIBRARY_LIST_SHOW_SERVICES:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 173
    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_SERVICES:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 176
    :cond_3
    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->CUSTOM_AMOUNT:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 177
    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->CUSTOM_AMOUNT:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_4
    sget-object v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_DISCOUNTS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 181
    sget-object v0, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_DISCOUNTS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    :cond_5
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public buildGiftCardPlaceholder(Z)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    .line 208
    sget-object p1, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getCurrentCategoryId()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        message = "Use {@link #holder()} instead."
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->holder:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/CheckoutLibraryListState;

    invoke-virtual {v0}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getCurrentCategoryId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentCategoryName()Ljava/lang/String;
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        message = "Use {@link #holder()} instead."
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->holder:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/CheckoutLibraryListState;

    invoke-virtual {v0}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getCurrentCategoryName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLibraryFilter()Lcom/squareup/librarylist/LibraryListState$Filter;
    .locals 1
    .annotation runtime Lkotlin/Deprecated;
        message = "Use {@link #holder()} instead. "
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->holder:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/CheckoutLibraryListState;

    invoke-virtual {v0}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v0

    return-object v0
.end method

.method public goBack()Z
    .locals 1

    .line 105
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListStateManager;->isTopLevel()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    sget-object v0, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-virtual {p0, v0}, Lcom/squareup/librarylist/RealLibraryListStateManager;->setLibraryFilter(Lcom/squareup/librarylist/LibraryListState$Filter;)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public holder()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/librarylist/CheckoutLibraryListState;",
            ">;"
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->holder:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public isTopLevel()Z
    .locals 4

    .line 85
    invoke-virtual {p0}, Lcom/squareup/librarylist/RealLibraryListStateManager;->getLibraryFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 87
    sget-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 89
    :cond_0
    sget-object v1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method public onCogsUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 3

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 118
    invoke-static {v1, v0, v1}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread$default(Ljava/lang/String;ILjava/lang/Object;)V

    .line 120
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->holder:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/librarylist/CheckoutLibraryListState;

    invoke-virtual {v1}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v1

    sget-object v2, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    if-eq v1, v2, :cond_0

    return-void

    .line 124
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasMultipleUpdateBatches()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 125
    sget-object p1, Lcom/squareup/librarylist/CheckoutLibraryListState;->Companion:Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;

    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->device:Lcom/squareup/util/Device;

    invoke-virtual {p1, v0}, Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;->getDefaultHolder(Lcom/squareup/util/Device;)Lcom/squareup/librarylist/CheckoutLibraryListState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/librarylist/RealLibraryListStateManager;->updateHolder(Lcom/squareup/librarylist/CheckoutLibraryListState;)V

    return-void

    :cond_1
    new-array v0, v0, [Lcom/squareup/shared/catalog/models/CatalogObjectType;

    const/4 v1, 0x0

    .line 129
    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Lcom/squareup/cogs/CatalogUpdateEvent;->hasOneOf([Lcom/squareup/shared/catalog/models/CatalogObjectType;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 130
    invoke-virtual {p1}, Lcom/squareup/cogs/CatalogUpdateEvent;->getDeleted()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogObject;

    const-string v1, "deletedObject"

    .line 131
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    sget-object v2, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM_CATEGORY:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    if-ne v1, v2, :cond_2

    .line 132
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogObject;->getId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->holder:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/librarylist/CheckoutLibraryListState;

    invoke-virtual {v1}, Lcom/squareup/librarylist/CheckoutLibraryListState;->getCurrentCategoryId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 133
    sget-object p1, Lcom/squareup/librarylist/CheckoutLibraryListState;->Companion:Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;

    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->device:Lcom/squareup/util/Device;

    invoke-virtual {p1, v0}, Lcom/squareup/librarylist/CheckoutLibraryListState$Companion;->getDefaultHolder(Lcom/squareup/util/Device;)Lcom/squareup/librarylist/CheckoutLibraryListState;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/librarylist/RealLibraryListStateManager;->updateHolder(Lcom/squareup/librarylist/CheckoutLibraryListState;)V

    :cond_3
    return-void
.end method

.method public placeholderClicked(Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;Lflow/Flow;)V
    .locals 1

    const-string v0, "placeholder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    sget-object v0, Lcom/squareup/librarylist/RealLibraryListStateManager$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 221
    :pswitch_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Caller must handle CUSTOM_AMOUNT!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 220
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->redeemRewardsFlow:Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;

    invoke-interface {p1}, Lcom/squareup/redeemrewardsapi/RedeemRewardsFlow;->getFirstScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    invoke-virtual {p2, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 219
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->giftCardActivationFlow:Lcom/squareup/giftcardactivation/GiftCardActivationFlow;

    invoke-interface {p1}, Lcom/squareup/giftcardactivation/GiftCardActivationFlow;->activationScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p1

    invoke-virtual {p2, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 218
    :pswitch_3
    sget-object p1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_DISCOUNTS:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListStateManager;->setLibraryFilter(Lcom/squareup/librarylist/LibraryListState$Filter;)V

    goto :goto_0

    .line 217
    :pswitch_4
    sget-object p1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_SERVICES:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListStateManager;->setLibraryFilter(Lcom/squareup/librarylist/LibraryListState$Filter;)V

    goto :goto_0

    .line 216
    :pswitch_5
    sget-object p1, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-virtual {p0, p1}, Lcom/squareup/librarylist/RealLibraryListStateManager;->setLibraryFilter(Lcom/squareup/librarylist/LibraryListState$Filter;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setLibraryFilter(Lcom/squareup/librarylist/LibraryListState$Filter;)V
    .locals 9

    const-string v0, "filter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    sget-object v0, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    if-eq p1, v0, :cond_0

    .line 78
    new-instance v0, Lcom/squareup/librarylist/CheckoutLibraryListState;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1e

    const/4 v8, 0x0

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v8}, Lcom/squareup/librarylist/CheckoutLibraryListState;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p0, v0}, Lcom/squareup/librarylist/RealLibraryListStateManager;->updateHolder(Lcom/squareup/librarylist/CheckoutLibraryListState;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/librarylist/FilterEvent;

    const/4 v2, 0x2

    invoke-direct {v1, p1, v3, v2, v3}, Lcom/squareup/librarylist/FilterEvent;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void

    .line 76
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Call setModeToSingleCategory() instead."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public setModeToSingleCategory(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V
    .locals 7

    const-string v0, "entry"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    new-instance v0, Lcom/squareup/librarylist/CheckoutLibraryListState;

    .line 144
    sget-object v2, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getAbbreviation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getColor()Ljava/lang/String;

    move-result-object v6

    move-object v1, v0

    .line 143
    invoke-direct/range {v1 .. v6}, Lcom/squareup/librarylist/CheckoutLibraryListState;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-direct {p0, v0}, Lcom/squareup/librarylist/RealLibraryListStateManager;->updateHolder(Lcom/squareup/librarylist/CheckoutLibraryListState;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListStateManager;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/librarylist/FilterEvent;

    sget-object v2, Lcom/squareup/librarylist/LibraryListState$Filter;->SINGLE_CATEGORY:Lcom/squareup/librarylist/LibraryListState$Filter;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/librarylist/FilterEvent;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
