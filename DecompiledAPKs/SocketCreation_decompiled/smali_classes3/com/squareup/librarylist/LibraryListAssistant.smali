.class public interface abstract Lcom/squareup/librarylist/LibraryListAssistant;
.super Ljava/lang/Object;
.source "LibraryListAssistant.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\u0007H&J\u0008\u0010\t\u001a\u00020\u0007H&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListAssistant;",
        "",
        "hasSearchText",
        "",
        "getHasSearchText",
        "()Z",
        "logLibraryListItemClicked",
        "",
        "logLibraryListItemLongClicked",
        "openItemsApplet",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getHasSearchText()Z
.end method

.method public abstract logLibraryListItemClicked()V
.end method

.method public abstract logLibraryListItemLongClicked()V
.end method

.method public abstract openItemsApplet()V
.end method
