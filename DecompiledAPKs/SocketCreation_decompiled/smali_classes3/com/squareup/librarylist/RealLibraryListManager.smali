.class public final Lcom/squareup/librarylist/RealLibraryListManager;
.super Ljava/lang/Object;
.source "LibraryListManager.kt"

# interfaces
.implements Lcom/squareup/librarylist/LibraryListManager;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/RealLibraryListManager$Action;,
        Lcom/squareup/librarylist/RealLibraryListManager$FilterEvent;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLibraryListManager.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LibraryListManager.kt\ncom/squareup/librarylist/RealLibraryListManager\n*L\n1#1,370:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u00029:B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0010\u001a\u00020*2\u0006\u0010+\u001a\u00020,H\u0016J\u0008\u0010-\u001a\u00020*H\u0016J\u0010\u0010.\u001a\u00020*2\u0006\u0010/\u001a\u000200H\u0016J\u0008\u00101\u001a\u00020*H\u0016J\u0010\u0010\u001c\u001a\u00020*2\u0006\u00102\u001a\u000203H\u0016J\u0008\u0010 \u001a\u00020\u0014H\u0002J\u0010\u0010$\u001a\u00020*2\u0006\u00104\u001a\u000205H\u0016J\u0010\u0010&\u001a\u00020*2\u0006\u00106\u001a\u00020!H\u0016J\u0010\u0010(\u001a\u00020*2\u0006\u00107\u001a\u000208H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0018X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u0014\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010 \u001a\u00020!8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\"\u0010#R\u0014\u0010$\u001a\u0008\u0012\u0004\u0012\u00020%0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\'0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010(\u001a\u0008\u0012\u0004\u0012\u00020)0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006;"
    }
    d2 = {
        "Lcom/squareup/librarylist/RealLibraryListManager;",
        "Lcom/squareup/librarylist/LibraryListManager;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "device",
        "Lcom/squareup/util/Device;",
        "configuration",
        "Lcom/squareup/librarylist/LibraryListConfiguration;",
        "libraryListSearcher",
        "Lcom/squareup/librarylist/LibraryListSearcher;",
        "mainScheduler",
        "Lrx/Scheduler;",
        "(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListSearcher;Lrx/Scheduler;)V",
        "back",
        "Lcom/jakewharton/rxrelay/PublishRelay;",
        "Lcom/squareup/librarylist/RealLibraryListManager$Action$Back;",
        "catalogUpdate",
        "Lcom/squareup/librarylist/RealLibraryListManager$Action$CatalogUpdate;",
        "currentState",
        "Lcom/jakewharton/rxrelay/BehaviorRelay;",
        "Lcom/squareup/librarylist/LibraryListState;",
        "isLibraryEmpty",
        "",
        "results",
        "Lrx/Observable;",
        "Lcom/squareup/librarylist/LibraryListResults;",
        "getResults",
        "()Lrx/Observable;",
        "search",
        "Lcom/squareup/librarylist/RealLibraryListManager$Action$Search;",
        "subscription",
        "Lrx/subscriptions/SerialSubscription;",
        "topLevelFilter",
        "Lcom/squareup/librarylist/LibraryListState$Filter;",
        "getTopLevelFilter",
        "()Lcom/squareup/librarylist/LibraryListState$Filter;",
        "viewCategory",
        "Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewCategory;",
        "viewFilter",
        "Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewFilter;",
        "viewPlaceholder",
        "Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewPlaceholder;",
        "",
        "event",
        "Lcom/squareup/cogs/CatalogUpdateEvent;",
        "goBack",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "searchText",
        "",
        "category",
        "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
        "filter",
        "placeholder",
        "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
        "Action",
        "FilterEvent",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final back:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/librarylist/RealLibraryListManager$Action$Back;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogUpdate:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/librarylist/RealLibraryListManager$Action$CatalogUpdate;",
            ">;"
        }
    .end annotation
.end field

.field private final configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

.field private final currentState:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/librarylist/LibraryListState;",
            ">;"
        }
    .end annotation
.end field

.field private final device:Lcom/squareup/util/Device;

.field private isLibraryEmpty:Z

.field private final libraryListSearcher:Lcom/squareup/librarylist/LibraryListSearcher;

.field private final mainScheduler:Lrx/Scheduler;

.field private final results:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/librarylist/LibraryListResults;",
            ">;"
        }
    .end annotation
.end field

.field private final search:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/librarylist/RealLibraryListManager$Action$Search;",
            ">;"
        }
    .end annotation
.end field

.field private final subscription:Lrx/subscriptions/SerialSubscription;

.field private final viewCategory:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewCategory;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFilter:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final viewPlaceholder:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewPlaceholder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListSearcher;Lrx/Scheduler;)V
    .locals 1

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "libraryListSearcher"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p2, p0, Lcom/squareup/librarylist/RealLibraryListManager;->device:Lcom/squareup/util/Device;

    iput-object p3, p0, Lcom/squareup/librarylist/RealLibraryListManager;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    iput-object p4, p0, Lcom/squareup/librarylist/RealLibraryListManager;->libraryListSearcher:Lcom/squareup/librarylist/LibraryListSearcher;

    iput-object p5, p0, Lcom/squareup/librarylist/RealLibraryListManager;->mainScheduler:Lrx/Scheduler;

    .line 99
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->back:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 100
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->catalogUpdate:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 101
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->viewPlaceholder:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 102
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->viewFilter:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 103
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->viewCategory:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 104
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->search:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 p1, 0x1

    .line 106
    iput-boolean p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->isLibraryEmpty:Z

    .line 107
    new-instance p1, Lrx/subscriptions/SerialSubscription;

    invoke-direct {p1}, Lrx/subscriptions/SerialSubscription;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->subscription:Lrx/subscriptions/SerialSubscription;

    .line 108
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->currentState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 112
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->currentState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 113
    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    .line 114
    sget-object p2, Lcom/squareup/librarylist/RealLibraryListManager$1;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$1;

    check-cast p2, Lrx/functions/Action1;

    invoke-virtual {p1, p2}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    .line 117
    new-instance p2, Lcom/squareup/librarylist/RealLibraryListManager$2;

    invoke-direct {p2, p0}, Lcom/squareup/librarylist/RealLibraryListManager$2;-><init>(Lcom/squareup/librarylist/RealLibraryListManager;)V

    check-cast p2, Lrx/functions/Func1;

    invoke-virtual {p1, p2}, Lrx/Observable;->flatMapSingle(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 129
    sget-object p2, Lcom/squareup/librarylist/RealLibraryListManager$3;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$3;

    check-cast p2, Lrx/functions/Action1;

    invoke-virtual {p1, p2}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    .line 132
    new-instance p2, Lcom/squareup/librarylist/RealLibraryListManager$4;

    invoke-direct {p2, p0}, Lcom/squareup/librarylist/RealLibraryListManager$4;-><init>(Lcom/squareup/librarylist/RealLibraryListManager;)V

    check-cast p2, Lrx/functions/Func1;

    invoke-virtual {p1, p2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 170
    sget-object p2, Lcom/squareup/librarylist/RealLibraryListManager$5;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$5;

    check-cast p2, Lrx/functions/Action1;

    invoke-virtual {p1, p2}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    const-string p2, "currentState\n        .di\u2026tResults: $it\")\n        }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->results:Lrx/Observable;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 83
    iget-object p0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getConfiguration$p(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListConfiguration;
    .locals 0

    .line 83
    iget-object p0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    return-object p0
.end method

.method public static final synthetic access$getLibraryListSearcher$p(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListSearcher;
    .locals 0

    .line 83
    iget-object p0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->libraryListSearcher:Lcom/squareup/librarylist/LibraryListSearcher;

    return-object p0
.end method

.method public static final synthetic access$isLibraryEmpty$p(Lcom/squareup/librarylist/RealLibraryListManager;)Z
    .locals 0

    .line 83
    iget-boolean p0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->isLibraryEmpty:Z

    return p0
.end method

.method public static final synthetic access$setLibraryEmpty$p(Lcom/squareup/librarylist/RealLibraryListManager;Z)V
    .locals 0

    .line 83
    iput-boolean p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->isLibraryEmpty:Z

    return-void
.end method

.method public static final synthetic access$topLevelFilter(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListState;
    .locals 0

    .line 83
    invoke-direct {p0}, Lcom/squareup/librarylist/RealLibraryListManager;->topLevelFilter()Lcom/squareup/librarylist/LibraryListState;

    move-result-object p0

    return-object p0
.end method

.method private final getTopLevelFilter()Lcom/squareup/librarylist/LibraryListState$Filter;
    .locals 3

    .line 341
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListConfiguration;->getTopLevelPlaceholders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 342
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    invoke-interface {v0}, Lcom/squareup/librarylist/LibraryListConfiguration;->getTopLevelPlaceholders()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    sget-object v1, Lcom/squareup/librarylist/RealLibraryListManager$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 347
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 348
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t convert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListManager;->configuration:Lcom/squareup/librarylist/LibraryListConfiguration;

    invoke-interface {v2}, Lcom/squareup/librarylist/LibraryListConfiguration;->getTopLevelPlaceholders()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " to a Filter"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 347
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 346
    :pswitch_1
    sget-object v0, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_GIFT_CARDS:Lcom/squareup/librarylist/LibraryListState$Filter;

    goto :goto_0

    .line 345
    :pswitch_2
    sget-object v0, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_DISCOUNTS:Lcom/squareup/librarylist/LibraryListState$Filter;

    goto :goto_0

    .line 344
    :pswitch_3
    sget-object v0, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_SERVICES:Lcom/squareup/librarylist/LibraryListState$Filter;

    goto :goto_0

    .line 343
    :pswitch_4
    sget-object v0, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListState$Filter;

    goto :goto_0

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 353
    sget-object v0, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_CATEGORIES:Lcom/squareup/librarylist/LibraryListState$Filter;

    goto :goto_0

    .line 355
    :cond_1
    sget-object v0, Lcom/squareup/librarylist/LibraryListState$Filter;->ALL_ITEMS:Lcom/squareup/librarylist/LibraryListState$Filter;

    :goto_0
    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private final topLevelFilter()Lcom/squareup/librarylist/LibraryListState;
    .locals 11

    .line 333
    new-instance v10, Lcom/squareup/librarylist/LibraryListState;

    .line 334
    invoke-direct {p0}, Lcom/squareup/librarylist/RealLibraryListManager;->getTopLevelFilter()Lcom/squareup/librarylist/LibraryListState$Filter;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/16 v8, 0x3e

    const/4 v9, 0x0

    move-object v0, v10

    .line 333
    invoke-direct/range {v0 .. v9}, Lcom/squareup/librarylist/LibraryListState;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;Lcom/squareup/librarylist/LibraryListState$SearchQuery;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v10
.end method


# virtual methods
.method public catalogUpdate(Lcom/squareup/cogs/CatalogUpdateEvent;)V
    .locals 2

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 303
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->catalogUpdate:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/librarylist/RealLibraryListManager$Action$CatalogUpdate;

    invoke-direct {v1, p1}, Lcom/squareup/librarylist/RealLibraryListManager$Action$CatalogUpdate;-><init>(Lcom/squareup/cogs/CatalogUpdateEvent;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public getResults()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/librarylist/LibraryListResults;",
            ">;"
        }
    .end annotation

    .line 109
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->results:Lrx/Observable;

    return-object v0
.end method

.method public goBack()V
    .locals 2

    .line 299
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->back:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lcom/squareup/librarylist/RealLibraryListManager$Action$Back;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$Action$Back;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    iget-object p1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->currentState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-direct {p0}, Lcom/squareup/librarylist/RealLibraryListManager;->topLevelFilter()Lcom/squareup/librarylist/LibraryListState;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 178
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    .line 180
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->back:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 181
    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->currentState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v1, Lrx/Observable;

    sget-object v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$1;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$1;

    check-cast v2, Lrx/functions/Func2;

    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 182
    new-instance v1, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$2;-><init>(Lcom/squareup/librarylist/RealLibraryListManager;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "it"

    .line 190
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->catalogUpdate:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 193
    sget-object v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$4;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$4;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 194
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListManager;->currentState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v2, Lrx/Observable;

    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 195
    new-instance v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$5;

    invoke-direct {v2, p0}, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$5;-><init>(Lcom/squareup/librarylist/RealLibraryListManager;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 216
    sget-object v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$6;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$6;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 217
    sget-object v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$7;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$7;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 218
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListManager;->mainScheduler:Lrx/Scheduler;

    invoke-virtual {v0, v2}, Lrx/Observable;->subscribeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    .line 219
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->viewPlaceholder:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 222
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListManager;->currentState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v2, Lrx/Observable;

    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/jakewharton/rxrelay/PublishRelay;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 223
    sget-object v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$9;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$9;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 236
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->viewFilter:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 239
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListManager;->currentState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v2, Lrx/Observable;

    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/jakewharton/rxrelay/PublishRelay;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 240
    sget-object v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$11;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$11;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 246
    new-instance v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$12;

    invoke-direct {v2, p0}, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$12;-><init>(Lcom/squareup/librarylist/RealLibraryListManager;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v0, v2}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    .line 249
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->viewCategory:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 252
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListManager;->currentState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v2, Lrx/Observable;

    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/jakewharton/rxrelay/PublishRelay;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 253
    sget-object v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$14;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$14;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 264
    new-instance v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$15;

    invoke-direct {v2, p0}, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$15;-><init>(Lcom/squareup/librarylist/RealLibraryListManager;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v0, v2}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object v0

    .line 267
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->search:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 270
    sget-object v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$17;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$17;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 271
    iget-object v2, p0, Lcom/squareup/librarylist/RealLibraryListManager;->currentState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    check-cast v2, Lrx/Observable;

    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 272
    sget-object v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$18;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$18;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 280
    sget-object v2, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$19;->INSTANCE:Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$19;

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v0, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 287
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lrx/Observable;->merge(Ljava/lang/Iterable;)Lrx/Observable;

    move-result-object p1

    .line 290
    new-instance v0, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$21;

    iget-object v1, p0, Lcom/squareup/librarylist/RealLibraryListManager;->currentState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-direct {v0, v1}, Lcom/squareup/librarylist/RealLibraryListManager$onEnterScope$21;-><init>(Lcom/jakewharton/rxrelay/BehaviorRelay;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    new-instance v1, Lcom/squareup/librarylist/LibraryListManagerKt$sam$rx_functions_Action1$0;

    invoke-direct {v1, v0}, Lcom/squareup/librarylist/LibraryListManagerKt$sam$rx_functions_Action1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    .line 291
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->subscription:Lrx/subscriptions/SerialSubscription;

    invoke-virtual {v0, p1}, Lrx/subscriptions/SerialSubscription;->set(Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 295
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->subscription:Lrx/subscriptions/SerialSubscription;

    invoke-virtual {v0}, Lrx/subscriptions/SerialSubscription;->unsubscribe()V

    return-void
.end method

.method public search(Ljava/lang/String;)V
    .locals 2

    const-string v0, "searchText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 319
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->search:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/librarylist/RealLibraryListManager$Action$Search;

    invoke-direct {v1, p1}, Lcom/squareup/librarylist/RealLibraryListManager$Action$Search;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public viewCategory(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V
    .locals 2

    const-string v0, "category"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->viewCategory:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewCategory;

    invoke-direct {v1, p1}, Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewCategory;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public viewFilter(Lcom/squareup/librarylist/LibraryListState$Filter;)V
    .locals 2

    const-string v0, "filter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->viewFilter:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewFilter;

    invoke-direct {v1, p1}, Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewFilter;-><init>(Lcom/squareup/librarylist/LibraryListState$Filter;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public viewPlaceholder(Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;)V
    .locals 2

    const-string v0, "placeholder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/squareup/librarylist/RealLibraryListManager;->viewPlaceholder:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewPlaceholder;

    invoke-direct {v1, p1}, Lcom/squareup/librarylist/RealLibraryListManager$Action$ViewPlaceholder;-><init>(Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
