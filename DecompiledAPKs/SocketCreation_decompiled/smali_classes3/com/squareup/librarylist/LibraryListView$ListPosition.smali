.class final Lcom/squareup/librarylist/LibraryListView$ListPosition;
.super Ljava/lang/Object;
.source "LibraryListView.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/LibraryListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ListPosition"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/LibraryListView$ListPosition$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\u0008\u0002\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u0017\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListView$ListPosition;",
        "",
        "index",
        "",
        "offset",
        "(II)V",
        "getIndex",
        "()I",
        "getOffset",
        "Companion",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/librarylist/LibraryListView$ListPosition$Companion;

.field private static final TOP:Lcom/squareup/librarylist/LibraryListView$ListPosition;


# instance fields
.field private final index:I

.field private final offset:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/librarylist/LibraryListView$ListPosition$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/librarylist/LibraryListView$ListPosition$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/librarylist/LibraryListView$ListPosition;->Companion:Lcom/squareup/librarylist/LibraryListView$ListPosition$Companion;

    .line 198
    new-instance v0, Lcom/squareup/librarylist/LibraryListView$ListPosition;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Lcom/squareup/librarylist/LibraryListView$ListPosition;-><init>(II)V

    sput-object v0, Lcom/squareup/librarylist/LibraryListView$ListPosition;->TOP:Lcom/squareup/librarylist/LibraryListView$ListPosition;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/librarylist/LibraryListView$ListPosition;->index:I

    iput p2, p0, Lcom/squareup/librarylist/LibraryListView$ListPosition;->offset:I

    return-void
.end method

.method public static final synthetic access$getTOP$cp()Lcom/squareup/librarylist/LibraryListView$ListPosition;
    .locals 1

    .line 193
    sget-object v0, Lcom/squareup/librarylist/LibraryListView$ListPosition;->TOP:Lcom/squareup/librarylist/LibraryListView$ListPosition;

    return-object v0
.end method


# virtual methods
.method public final getIndex()I
    .locals 1

    .line 194
    iget v0, p0, Lcom/squareup/librarylist/LibraryListView$ListPosition;->index:I

    return v0
.end method

.method public final getOffset()I
    .locals 1

    .line 195
    iget v0, p0, Lcom/squareup/librarylist/LibraryListView$ListPosition;->offset:I

    return v0
.end method
