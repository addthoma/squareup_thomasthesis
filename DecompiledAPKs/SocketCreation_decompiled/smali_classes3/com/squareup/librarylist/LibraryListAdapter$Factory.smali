.class public final Lcom/squareup/librarylist/LibraryListAdapter$Factory;
.super Ljava/lang/Object;
.source "LibraryListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/librarylist/LibraryListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001BG\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0008\u0001\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/librarylist/LibraryListAdapter$Factory;",
        "",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "tileAppearanceSettings",
        "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/util/Res;)V",
        "build",
        "Lcom/squareup/librarylist/LibraryListAdapter;",
        "itemDisabledController",
        "Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;",
        "library-list_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/util/Res;)V
    .locals 1
    .param p3    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "itemPhotos"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tileAppearanceSettings"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iput-object p2, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p3, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iput-object p5, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p6, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iput-object p7, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final build(Lcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;)Lcom/squareup/librarylist/LibraryListAdapter;
    .locals 10

    const-string v0, "itemDisabledController"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    new-instance v0, Lcom/squareup/librarylist/LibraryListAdapter;

    .line 209
    iget-object v2, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v3, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v4, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v5, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    .line 210
    iget-object v6, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v7

    iget-object v9, p0, Lcom/squareup/librarylist/LibraryListAdapter$Factory;->res:Lcom/squareup/util/Res;

    move-object v1, v0

    move-object v8, p1

    .line 208
    invoke-direct/range {v1 .. v9}, Lcom/squareup/librarylist/LibraryListAdapter;-><init>(Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;ZLcom/squareup/librarylist/LibraryListAdapter$ItemDisabledController;Lcom/squareup/util/Res;)V

    return-object v0
.end method
