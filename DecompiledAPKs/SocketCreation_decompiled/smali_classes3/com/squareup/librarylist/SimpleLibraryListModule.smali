.class public Lcom/squareup/librarylist/SimpleLibraryListModule;
.super Ljava/lang/Object;
.source "SimpleLibraryListModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/librarylist/SimpleLibraryListModule$SharedScope;
    }
.end annotation


# instance fields
.field private final placeholders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/librarylist/LibraryListConfiguration$Placeholder;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/librarylist/SimpleLibraryListModule;->placeholders:Ljava/util/List;

    return-void
.end method


# virtual methods
.method provideEntryHandler()Lcom/squareup/librarylist/SimpleEntryHandler;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 85
    new-instance v0, Lcom/squareup/librarylist/SimpleEntryHandler;

    invoke-direct {v0}, Lcom/squareup/librarylist/SimpleEntryHandler;-><init>()V

    return-object v0
.end method

.method provideLibraryListConfiguration()Lcom/squareup/librarylist/SimpleLibraryListConfiguration;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 73
    new-instance v0, Lcom/squareup/librarylist/SimpleLibraryListConfiguration;

    iget-object v1, p0, Lcom/squareup/librarylist/SimpleLibraryListModule;->placeholders:Ljava/util/List;

    invoke-direct {v0, v1}, Lcom/squareup/librarylist/SimpleLibraryListConfiguration;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method provideLibraryListManager(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/librarylist/SimpleLibraryListConfiguration;Lcom/squareup/librarylist/LibraryListSearcher;Lrx/Scheduler;)Lcom/squareup/librarylist/RealLibraryListManager;
    .locals 7
    .param p5    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/librarylist/RealLibraryListManager;

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/librarylist/RealLibraryListManager;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListSearcher;Lrx/Scheduler;)V

    return-object v6
.end method

.method provideLibraryListPresenter(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/librarylist/SimpleEntryHandler;Lcom/squareup/librarylist/SimpleLibraryListConfiguration;Lcom/squareup/librarylist/SimpleLibraryListAssistant;Lcom/squareup/librarylist/LibraryListAdapter$Factory;)Lcom/squareup/librarylist/LibraryListPresenter;
    .locals 10
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 39
    new-instance v9, Lcom/squareup/librarylist/RealLibraryListPresenter;

    move-object v0, v9

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p7

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/squareup/librarylist/RealLibraryListPresenter;-><init>(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/librarylist/LibraryListManager;Lcom/squareup/librarylist/LibraryListAssistant;Lcom/squareup/librarylist/LibraryListEntryHandler;Lcom/squareup/librarylist/LibraryListConfiguration;Lcom/squareup/librarylist/LibraryListAdapter$Factory;)V

    return-object v9
.end method

.method provideLibraryListSearcher(Lcom/squareup/cogs/Cogs;)Lcom/squareup/librarylist/LibraryListSearcher;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 59
    new-instance v0, Lcom/squareup/librarylist/RealLibraryListSearcher;

    invoke-direct {v0, p1}, Lcom/squareup/librarylist/RealLibraryListSearcher;-><init>(Lcom/squareup/cogs/Cogs;)V

    return-object v0
.end method

.method provideLibraryListStateManager(Lcom/squareup/librarylist/RealLibraryListManager;)Lcom/squareup/librarylist/LibraryListManager;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p1
.end method

.method provideSimpleLibraryListAssistant()Lcom/squareup/librarylist/SimpleLibraryListAssistant;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 79
    new-instance v0, Lcom/squareup/librarylist/SimpleLibraryListAssistant;

    invoke-direct {v0}, Lcom/squareup/librarylist/SimpleLibraryListAssistant;-><init>()V

    return-object v0
.end method
