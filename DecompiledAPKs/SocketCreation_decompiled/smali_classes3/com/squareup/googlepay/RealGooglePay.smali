.class public final Lcom/squareup/googlepay/RealGooglePay;
.super Ljava/lang/Object;
.source "RealGooglePay.kt"

# interfaces
.implements Lcom/squareup/googlepay/GooglePay;
.implements Lcom/squareup/ui/ActivityDelegate;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/googlepay/RealGooglePay$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealGooglePay.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealGooglePay.kt\ncom/squareup/googlepay/RealGooglePay\n*L\n1#1,237:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u0000 )2\u00020\u00012\u00020\u0002:\u0001)B-\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\nJ\u0016\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0016\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000e2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002J\u0008\u0010\u0013\u001a\u00020\u000cH\u0002J\"\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u000e2\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00110\u0017H\u0002J\u000e\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0010\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J.\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u000e2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u000f2\u0006\u0010\"\u001a\u00020\u000f2\u0006\u0010#\u001a\u00020$H\u0016J\u000e\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0016J\u000e\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0002J\u000c\u0010\'\u001a\u00020(*\u00020$H\u0002R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/googlepay/RealGooglePay;",
        "Lcom/squareup/googlepay/GooglePay;",
        "Lcom/squareup/ui/ActivityDelegate;",
        "apiClient",
        "Lcom/google/android/gms/common/api/GoogleApiClient;",
        "activityResultHandler",
        "Lcom/squareup/ui/ActivityResultHandler;",
        "ioScheduler",
        "Lio/reactivex/Scheduler;",
        "mainScheduler",
        "(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/squareup/ui/ActivityResultHandler;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;)V",
        "activity",
        "Landroid/app/Activity;",
        "activeWalletId",
        "Lio/reactivex/Single;",
        "",
        "createOnFailure",
        "",
        "createWallet",
        "getActivity",
        "observeActivityResults",
        "Lcom/squareup/ui/ActivityResultHandler$IntentResult;",
        "filterBlock",
        "Lkotlin/Function1;",
        "onConnected",
        "Lio/reactivex/Observable;",
        "onCreate",
        "",
        "onDestroy",
        "pushTokenize",
        "Lcom/squareup/googlepay/GooglePayResponse;",
        "opaqueCard",
        "",
        "lastFour",
        "displayName",
        "address",
        "Lcom/squareup/googlepay/GooglePayAddress;",
        "stableHardwareId",
        "tryToObtainWalletId",
        "toUserAddress",
        "Lcom/google/android/gms/identity/intents/model/UserAddress;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATE_WALLET:I = 0x12d

.field public static final Companion:Lcom/squareup/googlepay/RealGooglePay$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final PUSH_TOKENIZE:I = 0x12e


# instance fields
.field private activity:Landroid/app/Activity;

.field private final activityResultHandler:Lcom/squareup/ui/ActivityResultHandler;

.field private final apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private final ioScheduler:Lio/reactivex/Scheduler;

.field private final mainScheduler:Lio/reactivex/Scheduler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/googlepay/RealGooglePay$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/googlepay/RealGooglePay$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/googlepay/RealGooglePay;->Companion:Lcom/squareup/googlepay/RealGooglePay$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/squareup/ui/ActivityResultHandler;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p1    # Lcom/google/android/gms/common/api/GoogleApiClient;
        .annotation runtime Lcom/squareup/googlepay/client/GooglePayClient;
        .end annotation
    .end param
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "apiClient"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityResultHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ioScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/googlepay/RealGooglePay;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    iput-object p2, p0, Lcom/squareup/googlepay/RealGooglePay;->activityResultHandler:Lcom/squareup/ui/ActivityResultHandler;

    iput-object p3, p0, Lcom/squareup/googlepay/RealGooglePay;->ioScheduler:Lio/reactivex/Scheduler;

    iput-object p4, p0, Lcom/squareup/googlepay/RealGooglePay;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/squareup/googlepay/RealGooglePay$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/googlepay/RealGooglePay;->Companion:Lcom/squareup/googlepay/RealGooglePay$Companion;

    return-object v0
.end method

.method public static final synthetic access$createWallet(Lcom/squareup/googlepay/RealGooglePay;Landroid/app/Activity;)Lio/reactivex/Single;
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/googlepay/RealGooglePay;->createWallet(Landroid/app/Activity;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getActivity(Lcom/squareup/googlepay/RealGooglePay;)Landroid/app/Activity;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/googlepay/RealGooglePay;->getActivity()Landroid/app/Activity;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getApiClient$p(Lcom/squareup/googlepay/RealGooglePay;)Lcom/google/android/gms/common/api/GoogleApiClient;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/googlepay/RealGooglePay;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    return-object p0
.end method

.method public static final synthetic access$getMainScheduler$p(Lcom/squareup/googlepay/RealGooglePay;)Lio/reactivex/Scheduler;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/googlepay/RealGooglePay;->mainScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method public static final synthetic access$tryToObtainWalletId(Lcom/squareup/googlepay/RealGooglePay;)Lio/reactivex/Single;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/googlepay/RealGooglePay;->tryToObtainWalletId()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final createWallet(Landroid/app/Activity;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 163
    sget-object v0, Lcom/squareup/googlepay/RealGooglePay$createWallet$1;->INSTANCE:Lcom/squareup/googlepay/RealGooglePay$createWallet$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0}, Lcom/squareup/googlepay/RealGooglePay;->observeActivityResults(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object v0

    .line 166
    new-instance v1, Lcom/squareup/googlepay/RealGooglePay$createWallet$2;

    invoke-direct {v1, p0}, Lcom/squareup/googlepay/RealGooglePay$createWallet$2;-><init>(Lcom/squareup/googlepay/RealGooglePay;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 173
    iget-object v1, p0, Lcom/squareup/googlepay/RealGooglePay;->ioScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 174
    new-instance v1, Lcom/squareup/googlepay/RealGooglePay$createWallet$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/googlepay/RealGooglePay$createWallet$3;-><init>(Lcom/squareup/googlepay/RealGooglePay;Landroid/app/Activity;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "observeActivityResults {\u2026 CREATE_WALLET)\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getActivity()Landroid/app/Activity;
    .locals 1

    .line 216
    iget-object v0, p0, Lcom/squareup/googlepay/RealGooglePay;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final observeActivityResults(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/ui/ActivityResultHandler$IntentResult;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/ActivityResultHandler$IntentResult;",
            ">;"
        }
    .end annotation

    .line 209
    iget-object v0, p0, Lcom/squareup/googlepay/RealGooglePay;->activityResultHandler:Lcom/squareup/ui/ActivityResultHandler;

    .line 210
    invoke-virtual {v0}, Lcom/squareup/ui/ActivityResultHandler;->results()Lio/reactivex/Observable;

    move-result-object v0

    if-eqz p1, :cond_0

    .line 211
    new-instance v1, Lcom/squareup/googlepay/RealGooglePay$sam$io_reactivex_functions_Predicate$0;

    invoke-direct {v1, p1}, Lcom/squareup/googlepay/RealGooglePay$sam$io_reactivex_functions_Predicate$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p1, v1

    :cond_0
    check-cast p1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, p1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    .line 212
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "activityResultHandler\n  \u2026)\n        .firstOrError()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final toUserAddress(Lcom/squareup/googlepay/GooglePayAddress;)Lcom/google/android/gms/identity/intents/model/UserAddress;
    .locals 2

    .line 220
    invoke-static {}, Lcom/google/android/gms/identity/intents/model/UserAddress;->newBuilder()Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;

    move-result-object v0

    .line 221
    invoke-virtual {p1}, Lcom/squareup/googlepay/GooglePayAddress;->getAddressLine1()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;->setAddress1(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;

    move-result-object v0

    .line 222
    invoke-virtual {p1}, Lcom/squareup/googlepay/GooglePayAddress;->getAddressLine2()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;->setAddress2(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;

    move-result-object v0

    .line 223
    invoke-virtual {p1}, Lcom/squareup/googlepay/GooglePayAddress;->getAdministrativeArea()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;->setAdministrativeArea(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;

    move-result-object v0

    .line 224
    invoke-virtual {p1}, Lcom/squareup/googlepay/GooglePayAddress;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;->setCountryCode(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;

    move-result-object v0

    .line 225
    invoke-virtual {p1}, Lcom/squareup/googlepay/GooglePayAddress;->getLocality()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;->setLocality(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;

    move-result-object v0

    .line 226
    invoke-virtual {p1}, Lcom/squareup/googlepay/GooglePayAddress;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;->setName(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;

    move-result-object v0

    .line 227
    invoke-virtual {p1}, Lcom/squareup/googlepay/GooglePayAddress;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;->setPostalCode(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;

    move-result-object v0

    .line 228
    invoke-virtual {p1}, Lcom/squareup/googlepay/GooglePayAddress;->getPhoneNumber()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;->setPhoneNumber(Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;

    move-result-object p1

    .line 229
    invoke-virtual {p1}, Lcom/google/android/gms/identity/intents/model/UserAddress$Builder;->build()Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object p1

    const-string v0, "UserAddress.newBuilder()\u2026eNumber)\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final tryToObtainWalletId()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 181
    new-instance v0, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1;

    invoke-direct {v0, p0}, Lcom/squareup/googlepay/RealGooglePay$tryToObtainWalletId$1;-><init>(Lcom/squareup/googlepay/RealGooglePay;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object v0

    .line 205
    iget-object v1, p0, Lcom/squareup/googlepay/RealGooglePay;->ioScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Single\n        .create<S\u2026.subscribeOn(ioScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public activeWalletId(Z)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 72
    invoke-direct {p0}, Lcom/squareup/googlepay/RealGooglePay;->tryToObtainWalletId()Lio/reactivex/Single;

    move-result-object v0

    .line 73
    new-instance v1, Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/googlepay/RealGooglePay$activeWalletId$1;-><init>(Lcom/squareup/googlepay/RealGooglePay;Z)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->onErrorResumeNext(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 86
    iget-object v0, p0, Lcom/squareup/googlepay/RealGooglePay;->ioScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 87
    iget-object v0, p0, Lcom/squareup/googlepay/RealGooglePay;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string/jumbo v0, "tryToObtainWalletId()\n  \u2026.observeOn(mainScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public onConnected()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 54
    new-instance v0, Lcom/squareup/googlepay/RealGooglePay$onConnected$1;

    invoke-direct {v0, p0}, Lcom/squareup/googlepay/RealGooglePay$onConnected$1;-><init>(Lcom/squareup/googlepay/RealGooglePay;)V

    check-cast v0, Lio/reactivex/ObservableOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/squareup/googlepay/RealGooglePay;->ioScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/squareup/googlepay/RealGooglePay;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable\n        .crea\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreate(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iput-object p1, p0, Lcom/squareup/googlepay/RealGooglePay;->activity:Landroid/app/Activity;

    return-void
.end method

.method public onDestroy(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x0

    .line 49
    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/squareup/googlepay/RealGooglePay;->activity:Landroid/app/Activity;

    return-void
.end method

.method public pushTokenize([BLjava/lang/String;Ljava/lang/String;Lcom/squareup/googlepay/GooglePayAddress;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/googlepay/GooglePayAddress;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/googlepay/GooglePayResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "opaqueCard"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lastFour"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "displayName"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "address"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    new-instance v0, Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;-><init>()V

    .line 126
    invoke-virtual {v0, p1}, Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;->setOpaquePaymentCard([B)Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;

    move-result-object p1

    const/4 v0, 0x3

    .line 127
    invoke-virtual {p1, v0}, Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;->setNetwork(I)Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;

    move-result-object p1

    .line 128
    invoke-virtual {p1, v0}, Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;->setTokenServiceProvider(I)Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;

    move-result-object p1

    .line 129
    invoke-virtual {p1, p3}, Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;->setDisplayName(Ljava/lang/String;)Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;

    move-result-object p1

    .line 130
    invoke-virtual {p1, p2}, Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;->setLastDigits(Ljava/lang/String;)Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;

    move-result-object p1

    .line 132
    invoke-virtual {p4}, Lcom/squareup/googlepay/GooglePayAddress;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_0

    .line 133
    invoke-direct {p0, p4}, Lcom/squareup/googlepay/RealGooglePay;->toUserAddress(Lcom/squareup/googlepay/GooglePayAddress;)Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;->setUserAddress(Lcom/google/android/gms/identity/intents/model/UserAddress;)Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;

    .line 136
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest$Builder;->build()Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest;

    move-result-object p1

    .line 138
    sget-object p2, Lcom/squareup/googlepay/RealGooglePay$pushTokenize$1;->INSTANCE:Lcom/squareup/googlepay/RealGooglePay$pushTokenize$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p2}, Lcom/squareup/googlepay/RealGooglePay;->observeActivityResults(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p2

    .line 139
    sget-object p3, Lcom/squareup/googlepay/RealGooglePay$pushTokenize$2;->INSTANCE:Lcom/squareup/googlepay/RealGooglePay$pushTokenize$2;

    check-cast p3, Lio/reactivex/functions/Function;

    invoke-virtual {p2, p3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p2

    .line 147
    iget-object p3, p0, Lcom/squareup/googlepay/RealGooglePay;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p2, p3}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p2

    .line 149
    iget-object p3, p0, Lcom/squareup/googlepay/RealGooglePay;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p2, p3}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p2

    .line 150
    new-instance p3, Lcom/squareup/googlepay/RealGooglePay$pushTokenize$3;

    invoke-direct {p3, p0, p1}, Lcom/squareup/googlepay/RealGooglePay$pushTokenize$3;-><init>(Lcom/squareup/googlepay/RealGooglePay;Lcom/google/android/gms/tapandpay/issuer/PushTokenizeRequest;)V

    check-cast p3, Lio/reactivex/functions/Consumer;

    invoke-virtual {p2, p3}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    .line 158
    sget-object p2, Lcom/squareup/googlepay/RealGooglePay$pushTokenize$4;->INSTANCE:Lcom/squareup/googlepay/RealGooglePay$pushTokenize$4;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->onErrorResumeNext(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "observeActivityResults {\u2026error(UnknownException) }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public stableHardwareId()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 92
    new-instance v0, Lcom/squareup/googlepay/RealGooglePay$stableHardwareId$1;

    invoke-direct {v0, p0}, Lcom/squareup/googlepay/RealGooglePay$stableHardwareId$1;-><init>(Lcom/squareup/googlepay/RealGooglePay;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lcom/squareup/googlepay/RealGooglePay;->ioScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 116
    iget-object v1, p0, Lcom/squareup/googlepay/RealGooglePay;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Single\n        .create<S\u2026.observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
