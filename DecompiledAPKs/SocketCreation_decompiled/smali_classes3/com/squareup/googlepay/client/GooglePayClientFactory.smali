.class public final Lcom/squareup/googlepay/client/GooglePayClientFactory;
.super Ljava/lang/Object;
.source "GooglePayClientFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/googlepay/client/GooglePayClientFactory;",
        "",
        "()V",
        "connectionCallback",
        "Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;",
        "connectionFailedCallback",
        "Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;",
        "create",
        "Lcom/google/android/gms/common/api/GoogleApiClient;",
        "applicationContext",
        "Landroid/app/Application;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/googlepay/client/GooglePayClientFactory;

.field private static final connectionCallback:Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

.field private static final connectionFailedCallback:Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/googlepay/client/GooglePayClientFactory;

    invoke-direct {v0}, Lcom/squareup/googlepay/client/GooglePayClientFactory;-><init>()V

    sput-object v0, Lcom/squareup/googlepay/client/GooglePayClientFactory;->INSTANCE:Lcom/squareup/googlepay/client/GooglePayClientFactory;

    .line 16
    new-instance v0, Lcom/squareup/googlepay/client/GooglePayClientFactory$connectionCallback$1;

    invoke-direct {v0}, Lcom/squareup/googlepay/client/GooglePayClientFactory$connectionCallback$1;-><init>()V

    check-cast v0, Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

    sput-object v0, Lcom/squareup/googlepay/client/GooglePayClientFactory;->connectionCallback:Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

    .line 27
    sget-object v0, Lcom/squareup/googlepay/client/GooglePayClientFactory$connectionFailedCallback$1;->INSTANCE:Lcom/squareup/googlepay/client/GooglePayClientFactory$connectionFailedCallback$1;

    check-cast v0, Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;

    sput-object v0, Lcom/squareup/googlepay/client/GooglePayClientFactory;->connectionFailedCallback:Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Landroid/app/Application;)Lcom/google/android/gms/common/api/GoogleApiClient;
    .locals 3

    const-string v0, "applicationContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    check-cast p1, Landroid/content/Context;

    sget-object v1, Lcom/squareup/googlepay/client/GooglePayClientFactory;->connectionCallback:Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

    sget-object v2, Lcom/squareup/googlepay/client/GooglePayClientFactory;->connectionFailedCallback:Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V

    .line 37
    sget-object p1, Lcom/google/android/gms/tapandpay/TapAndPay;->TAP_AND_PAY_API:Lcom/google/android/gms/common/api/Api;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object p1

    .line 38
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object p1

    const-string v0, "GoogleApiClient\n        \u2026PAY_API)\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
