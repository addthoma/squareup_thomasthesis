.class final Lcom/squareup/googlepay/RealGooglePay$pushTokenize$2;
.super Ljava/lang/Object;
.source "RealGooglePay.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/googlepay/RealGooglePay;->pushTokenize([BLjava/lang/String;Ljava/lang/String;Lcom/squareup/googlepay/GooglePayAddress;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/googlepay/GooglePayResponse;",
        "result",
        "Lcom/squareup/ui/ActivityResultHandler$IntentResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/googlepay/RealGooglePay$pushTokenize$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/googlepay/RealGooglePay$pushTokenize$2;

    invoke-direct {v0}, Lcom/squareup/googlepay/RealGooglePay$pushTokenize$2;-><init>()V

    sput-object v0, Lcom/squareup/googlepay/RealGooglePay$pushTokenize$2;->INSTANCE:Lcom/squareup/googlepay/RealGooglePay$pushTokenize$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/ActivityResultHandler$IntentResult;)Lcom/squareup/googlepay/GooglePayResponse;
    .locals 2

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Received tokenizer result :: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    invoke-virtual {p1}, Lcom/squareup/ui/ActivityResultHandler$IntentResult;->getResultCode()I

    move-result p1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    if-nez p1, :cond_0

    .line 143
    sget-object p1, Lcom/squareup/googlepay/GooglePayResponse$Cancelled;->INSTANCE:Lcom/squareup/googlepay/GooglePayResponse$Cancelled;

    check-cast p1, Lcom/squareup/googlepay/GooglePayResponse;

    goto :goto_0

    .line 144
    :cond_0
    sget-object p1, Lcom/squareup/googlepay/GooglePayException$UnknownException;->INSTANCE:Lcom/squareup/googlepay/GooglePayException$UnknownException;

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 142
    :cond_1
    sget-object p1, Lcom/squareup/googlepay/GooglePayResponse$Success;->INSTANCE:Lcom/squareup/googlepay/GooglePayResponse$Success;

    check-cast p1, Lcom/squareup/googlepay/GooglePayResponse;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/ui/ActivityResultHandler$IntentResult;

    invoke-virtual {p0, p1}, Lcom/squareup/googlepay/RealGooglePay$pushTokenize$2;->apply(Lcom/squareup/ui/ActivityResultHandler$IntentResult;)Lcom/squareup/googlepay/GooglePayResponse;

    move-result-object p1

    return-object p1
.end method
