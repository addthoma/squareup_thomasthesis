.class final Lcom/squareup/container/AbstractPosWorkflowRunner$workflowLayerings$1;
.super Ljava/lang/Object;
.source "AbstractPosWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/AbstractPosWorkflowRunner;->workflowLayerings()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0004\u0010\u0000\u001a4\u0012\u0004\u0012\u0002H\u0002\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0004\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0005j\u0002`\u00060\u0003j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0004`\u00070\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0008\"\u0008\u0008\u0001\u0010\t*\u00020\u0008\"\u0004\u0008\u0002\u0010\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\n0\u0001H\n\u00a2\u0006\u0002\u0008\u000c"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;",
        "P",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "",
        "O",
        "R",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/container/AbstractPosWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowLayerings$1;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "+TP;+TR;>;)",
            "Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot<",
            "TP;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 489
    iget-object v0, p0, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowLayerings$1;->this$0:Lcom/squareup/container/AbstractPosWorkflowRunner;

    invoke-static {v0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner;->access$renderingsToLayeredRenderings(Lcom/squareup/container/AbstractPosWorkflowRunner;Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 140
    check-cast p1, Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    invoke-virtual {p0, p1}, Lcom/squareup/container/AbstractPosWorkflowRunner$workflowLayerings$1;->apply(Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;)Lcom/squareup/container/AbstractPosWorkflowRunner$PropsRenderingSnapshot;

    move-result-object p1

    return-object p1
.end method
