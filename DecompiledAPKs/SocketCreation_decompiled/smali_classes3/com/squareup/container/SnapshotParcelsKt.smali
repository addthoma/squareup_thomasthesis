.class public final Lcom/squareup/container/SnapshotParcelsKt;
.super Ljava/lang/Object;
.source "SnapshotParcels.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSnapshotParcels.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,72:1\n37#1,7:73\n37#1,7:81\n180#2:80\n*E\n*S KotlinDebug\n*F\n+ 1 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n33#1,7:73\n53#1,7:81\n53#1:80\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u001a\u0014\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u00022\u0006\u0010\u0003\u001a\u00020\u0001\u001a \u0010\u0004\u001a\u0004\u0018\u0001H\u0005\"\n\u0008\u0000\u0010\u0005\u0018\u0001*\u00020\u0006*\u00020\u0002H\u0086\u0008\u00a2\u0006\u0002\u0010\u0007\u001a\u001e\u0010\u0004\u001a\u0002H\u0005\"\n\u0008\u0000\u0010\u0005\u0018\u0001*\u00020\u0006*\u00020\u0008H\u0086\u0008\u00a2\u0006\u0002\u0010\t\u001a\n\u0010\n\u001a\u00020\u0002*\u00020\u0006\u001a\n\u0010\n\u001a\u00020\u0002*\u00020\u0001\u00a8\u0006\u000b"
    }
    d2 = {
        "toHistory",
        "Lflow/History;",
        "Lcom/squareup/workflow/Snapshot;",
        "default",
        "toParcelable",
        "T",
        "Landroid/os/Parcelable;",
        "(Lcom/squareup/workflow/Snapshot;)Landroid/os/Parcelable;",
        "Lokio/ByteString;",
        "(Lokio/ByteString;)Landroid/os/Parcelable;",
        "toSnapshot",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toHistory(Lcom/squareup/workflow/Snapshot;Lflow/History;)Lflow/History;
    .locals 3

    const-string v0, "default"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p0, :cond_3

    .line 53
    invoke-virtual {p0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p0

    if-eqz p0, :cond_3

    .line 52
    invoke-virtual {p0}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    if-eqz p0, :cond_3

    .line 80
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p0}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p0

    check-cast p0, Lokio/BufferedSource;

    .line 54
    invoke-interface {p0}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object p0

    .line 81
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Lokio/ByteString;->toByteArray()[B

    move-result-object p0

    .line 83
    array-length v2, p0

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 84
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 85
    const-class p0, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    if-nez p0, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string v1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 56
    new-instance v0, Lcom/squareup/container/PassthroughParcer;

    invoke-direct {v0}, Lcom/squareup/container/PassthroughParcer;-><init>()V

    check-cast v0, Lflow/KeyParceler;

    invoke-static {p0, v0}, Lflow/History;->from(Landroid/os/Parcelable;Lflow/KeyParceler;)Lflow/History;

    move-result-object p0

    if-eqz p0, :cond_3

    goto :goto_2

    :cond_3
    move-object p0, p1

    :goto_2
    return-object p0
.end method

.method public static final synthetic toParcelable(Lcom/squareup/workflow/Snapshot;)Landroid/os/Parcelable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Lcom/squareup/workflow/Snapshot;",
            ")TT;"
        }
    .end annotation

    const-string v0, "$this$toParcelable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-virtual {p0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p0

    invoke-virtual {p0}, Lokio/ByteString;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    move-object p0, v2

    :goto_1
    if-eqz p0, :cond_3

    .line 73
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v2, "Parcel.obtain()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lokio/ByteString;->toByteArray()[B

    move-result-object p0

    .line 75
    array-length v2, p0

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 76
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 77
    const-class p0, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    if-nez v2, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p0, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    :cond_3
    return-object v2
.end method

.method public static final synthetic toParcelable(Lokio/ByteString;)Landroid/os/Parcelable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Landroid/os/Parcelable;",
            ">(",
            "Lokio/ByteString;",
            ")TT;"
        }
    .end annotation

    const-string v0, "$this$toParcelable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v1, "Parcel.obtain()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0}, Lokio/ByteString;->toByteArray()[B

    move-result-object p0

    .line 39
    array-length v1, p0

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 40
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 41
    const-class p0, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p0

    invoke-virtual {v0, p0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    if-nez p0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return-object p0
.end method

.method public static final toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    const-string v0, "$this$toSnapshot"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/container/SnapshotParcelsKt$toSnapshot$1;

    invoke-direct {v1, p0}, Lcom/squareup/container/SnapshotParcelsKt$toSnapshot$1;-><init>(Landroid/os/Parcelable;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final toSnapshot(Lflow/History;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    const-string v0, "$this$toSnapshot"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/container/SnapshotParcelsKt$toSnapshot$2;

    invoke-direct {v1, p0}, Lcom/squareup/container/SnapshotParcelsKt$toSnapshot$2;-><init>(Lflow/History;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0
.end method
