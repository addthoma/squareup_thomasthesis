.class public final Lcom/squareup/container/ParcelableTester;
.super Lcom/squareup/container/ContainerTreeKey;
.source "ParcelableTester.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/ParcelableTester$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nParcelableTester.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ParcelableTester.kt\ncom/squareup/container/ParcelableTester\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,116:1\n24#2,4:117\n*E\n*S KotlinDebug\n*F\n+ 1 ParcelableTester.kt\ncom/squareup/container/ParcelableTester\n*L\n44#1,4:117\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0018\u0000 \u000e2\u00020\u00012\u00020\u0002:\u0001\u000eB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0014R\u0013\u0010\u0003\u001a\u00020\u0002\u00a2\u0006\n\n\u0002\u0008\u0007\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/container/ParcelableTester;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "Landroid/os/Parcelable;",
        "tested",
        "(Landroid/os/Parcelable;)V",
        "getTested",
        "()Landroid/os/Parcelable;",
        "tested$1",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/container/ParcelableTester;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/container/ParcelableTester$Companion;

.field private static final tested:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/container/ParcelableTester$Companion$TestRecord;",
            ">;"
        }
    .end annotation
.end field

.field public static verificationEnabled:Z


# instance fields
.field private final tested$1:Landroid/os/Parcelable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/container/ParcelableTester$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/ParcelableTester$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/ParcelableTester;->Companion:Lcom/squareup/container/ParcelableTester$Companion;

    const/4 v0, 0x1

    .line 39
    sput-boolean v0, Lcom/squareup/container/ParcelableTester;->verificationEnabled:Z

    .line 117
    new-instance v0, Lcom/squareup/container/ParcelableTester$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/container/ParcelableTester$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 120
    sput-object v0, Lcom/squareup/container/ParcelableTester;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 63
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v0, Ljava/util/Set;

    sput-object v0, Lcom/squareup/container/ParcelableTester;->tested:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 1

    const-string v0, "tested"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/container/ParcelableTester;->tested$1:Landroid/os/Parcelable;

    .line 17
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "For use only in dev builds."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final synthetic access$getTested$cp()Ljava/util/Set;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/container/ParcelableTester;->tested:Ljava/util/Set;

    return-object v0
.end method

.method public static final assertScreenCanBeParceled(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/container/ParcelableTester;->Companion:Lcom/squareup/container/ParcelableTester$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/container/ParcelableTester$Companion;->assertScreenCanBeParceled(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 3

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/container/ParcelableTester;->tested$1:Landroid/os/Parcelable;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 26
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/squareup/container/ContainerTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 27
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 30
    iget-object v1, p0, Lcom/squareup/container/ParcelableTester;->tested$1:Landroid/os/Parcelable;

    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 31
    iget-object v1, p0, Lcom/squareup/container/ParcelableTester;->tested$1:Landroid/os/Parcelable;

    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 33
    new-instance p2, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to write parcelable: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    check-cast p1, Ljava/lang/Throwable;

    invoke-direct {p2, v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public final getTested()Landroid/os/Parcelable;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/container/ParcelableTester;->tested$1:Landroid/os/Parcelable;

    return-object v0
.end method
