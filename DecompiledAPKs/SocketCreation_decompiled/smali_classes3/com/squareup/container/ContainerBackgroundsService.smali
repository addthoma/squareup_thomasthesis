.class public Lcom/squareup/container/ContainerBackgroundsService;
.super Ljava/lang/Object;
.source "ContainerBackgroundsService.java"


# static fields
.field private static final SERVICE_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    const-class v0, Lcom/squareup/container/ContainerBackgroundsService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/container/ContainerBackgroundsService;->SERVICE_NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addToScope(Lmortar/MortarScope$Builder;Lcom/squareup/container/ContainerBackgroundsProvider;)V
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/container/ContainerBackgroundsService;->SERVICE_NAME:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    return-void
.end method

.method private static get(Landroid/content/Context;)Lcom/squareup/container/ContainerBackgroundsProvider;
    .locals 1

    .line 17
    invoke-static {p0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p0

    sget-object v0, Lcom/squareup/container/ContainerBackgroundsService;->SERVICE_NAME:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/container/ContainerBackgroundsProvider;

    return-object p0
.end method

.method public static getCardBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 25
    invoke-static {p0}, Lcom/squareup/container/ContainerBackgroundsService;->get(Landroid/content/Context;)Lcom/squareup/container/ContainerBackgroundsProvider;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/squareup/container/ContainerBackgroundsProvider;->getCardScreenBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method

.method public static getCardScrimColor(Landroid/content/Context;)I
    .locals 1

    .line 29
    invoke-static {p0}, Lcom/squareup/container/ContainerBackgroundsService;->get(Landroid/content/Context;)Lcom/squareup/container/ContainerBackgroundsProvider;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/squareup/container/ContainerBackgroundsProvider;->getCardScreenScrimColor(Landroid/content/Context;)I

    move-result p0

    return p0
.end method

.method public static getWindowBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .line 21
    invoke-static {p0}, Lcom/squareup/container/ContainerBackgroundsService;->get(Landroid/content/Context;)Lcom/squareup/container/ContainerBackgroundsProvider;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/squareup/container/ContainerBackgroundsProvider;->getWindowBackground(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method
