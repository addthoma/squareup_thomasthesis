.class public Lcom/squareup/container/AssertNeverShownView;
.super Landroid/view/ViewGroup;
.source "AssertNeverShownView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 24
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "This view should never be drawn."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    return-void
.end method
