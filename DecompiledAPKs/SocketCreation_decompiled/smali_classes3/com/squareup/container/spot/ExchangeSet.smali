.class public Lcom/squareup/container/spot/ExchangeSet;
.super Ljava/lang/Object;
.source "ExchangeSet.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/spot/ExchangeSet$Builder;,
        Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    }
.end annotation


# static fields
.field private static final NO_EXCHANGE:Lcom/squareup/container/spot/ExchangeSet$Exchanger;


# instance fields
.field private final defaultExchange:Lcom/squareup/container/spot/ExchangeSet$Exchanger;

.field private final exchangeDuration:I

.field private final exchangers:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/container/spot/ExchangeSet$Exchanger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Lcom/squareup/container/spot/ExchangeSet$Exchanger;Ljava/util/LinkedHashMap;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/container/spot/ExchangeSet$Exchanger;",
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/container/spot/ExchangeSet$Exchanger;",
            ">;I)V"
        }
    .end annotation

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p2, p0, Lcom/squareup/container/spot/ExchangeSet;->exchangers:Ljava/util/LinkedHashMap;

    .line 107
    iput-object p1, p0, Lcom/squareup/container/spot/ExchangeSet;->defaultExchange:Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    .line 108
    iput p3, p0, Lcom/squareup/container/spot/ExchangeSet;->exchangeDuration:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/container/spot/ExchangeSet$Exchanger;Ljava/util/LinkedHashMap;ILcom/squareup/container/spot/ExchangeSet$1;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/container/spot/ExchangeSet;-><init>(Lcom/squareup/container/spot/ExchangeSet$Exchanger;Ljava/util/LinkedHashMap;I)V

    return-void
.end method

.method static synthetic access$000()Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/container/spot/ExchangeSet;->NO_EXCHANGE:Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    return-object v0
.end method

.method static synthetic access$100(Lcom/squareup/container/spot/ExchangeSet;Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/container/spot/ExchangeSet;->exchangeElementsForScreens(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method private static createAnimatorSetBuilder(Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet$Builder;
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput v1, v0, v1

    .line 203
    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object p0

    return-object p0
.end method

.method public static createSet()Lcom/squareup/container/spot/ExchangeSet$Builder;
    .locals 1

    .line 86
    new-instance v0, Lcom/squareup/container/spot/ExchangeSet$Builder;

    invoke-direct {v0}, Lcom/squareup/container/spot/ExchangeSet$Builder;-><init>()V

    return-object v0
.end method

.method private exchangeElementsForScreens(Landroid/animation/AnimatorSet;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 7

    .line 139
    instance-of v0, p3, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    instance-of v0, p4, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 143
    invoke-static {p1}, Lcom/squareup/container/spot/ExchangeSet;->createAnimatorSetBuilder(Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    .line 144
    iget-object p1, p0, Lcom/squareup/container/spot/ExchangeSet;->exchangers:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/squareup/container/spot/ExchangeSet;->defaultExchange:Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    iget v6, p0, Lcom/squareup/container/spot/ExchangeSet;->exchangeDuration:I

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-interface/range {v1 .. v6}, Lcom/squareup/container/spot/ExchangeSet$Exchanger;->appendAnimator(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Landroid/animation/AnimatorSet$Builder;I)V

    goto :goto_0

    .line 150
    :cond_0
    move-object v3, p3

    check-cast v3, Landroid/view/ViewGroup;

    move-object v4, p4

    check-cast v4, Landroid/view/ViewGroup;

    iget v6, p0, Lcom/squareup/container/spot/ExchangeSet;->exchangeDuration:I

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v1 .. v6}, Lcom/squareup/container/spot/ExchangeSet;->moveTreeElements(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/animation/AnimatorSet$Builder;I)V

    :goto_0
    return-void

    .line 140
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "exchangeElements must start with ViewGroups"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private getExchange(I)Lcom/squareup/container/spot/ExchangeSet$Exchanger;
    .locals 1

    .line 126
    invoke-direct {p0, p1}, Lcom/squareup/container/spot/ExchangeSet;->shouldSkipExchange(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/squareup/container/spot/ExchangeSet;->exchangers:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    if-nez p1, :cond_0

    .line 132
    iget-object p1, p0, Lcom/squareup/container/spot/ExchangeSet;->defaultExchange:Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    :cond_0
    return-object p1

    .line 127
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Should never attempt to access exchangers for a skip id!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private hasNonDefaultExchange(I)Z
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/container/spot/ExchangeSet;->exchangers:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private hasNonDefaultNonSkipExchange(I)Z
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/container/spot/ExchangeSet;->exchangers:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/container/spot/ExchangeSet;->exchangers:Ljava/util/LinkedHashMap;

    .line 117
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    sget-object v0, Lcom/squareup/container/spot/ExchangeSet;->NO_EXCHANGE:Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private moveTreeElements(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/animation/AnimatorSet$Builder;I)V
    .locals 7

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 158
    :goto_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v6, v0, :cond_3

    .line 159
    invoke-virtual {p2, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 160
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v0

    .line 163
    invoke-direct {p0, v0}, Lcom/squareup/container/spot/ExchangeSet;->hasNonDefaultExchange(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    invoke-direct {p0, v0}, Lcom/squareup/container/spot/ExchangeSet;->hasNonDefaultNonSkipExchange(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 167
    invoke-direct {p0, v0}, Lcom/squareup/container/spot/ExchangeSet;->getExchange(I)Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    .line 168
    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v0, v1

    move-object v1, p1

    move-object v4, p4

    move v5, p5

    .line 167
    invoke-interface/range {v0 .. v5}, Lcom/squareup/container/spot/ExchangeSet$Exchanger;->appendAnimator(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Landroid/animation/AnimatorSet$Builder;I)V

    goto :goto_1

    .line 173
    :cond_0
    invoke-direct {p0, v2}, Lcom/squareup/container/spot/ExchangeSet;->treeHasChildWithNonDefaultExchange(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 174
    check-cast v2, Landroid/view/ViewGroup;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/container/spot/ExchangeSet;->moveTreeElements(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/animation/AnimatorSet$Builder;I)V

    goto :goto_1

    .line 176
    :cond_1
    invoke-direct {p0, v0}, Lcom/squareup/container/spot/ExchangeSet;->getExchange(I)Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    move-result-object v1

    .line 177
    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v0, v1

    move-object v1, p1

    move-object v4, p4

    move v5, p5

    .line 176
    invoke-interface/range {v0 .. v5}, Lcom/squareup/container/spot/ExchangeSet$Exchanger;->appendAnimator(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;Landroid/animation/AnimatorSet$Builder;I)V

    :cond_2
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method private shouldSkipExchange(I)Z
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/container/spot/ExchangeSet;->exchangers:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/container/spot/ExchangeSet;->exchangers:Ljava/util/LinkedHashMap;

    .line 122
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    sget-object v0, Lcom/squareup/container/spot/ExchangeSet;->NO_EXCHANGE:Lcom/squareup/container/spot/ExchangeSet$Exchanger;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private treeHasChildWithNonDefaultExchange(Landroid/view/View;)Z
    .locals 5

    .line 183
    instance-of v0, p1, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 187
    :cond_0
    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    .line 189
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 190
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 191
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/squareup/container/spot/ExchangeSet;->hasNonDefaultExchange(I)Z

    move-result v3

    const/4 v4, 0x1

    if-eqz v3, :cond_1

    return v4

    .line 195
    :cond_1
    invoke-direct {p0, v2}, Lcom/squareup/container/spot/ExchangeSet;->treeHasChildWithNonDefaultExchange(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    return v4

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return v1
.end method
