.class Lcom/squareup/container/spot/Spot$1;
.super Ljava/lang/Object;
.source "Spot.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/spot/Spot;->forSpotSingleton(Lcom/squareup/container/spot/Spot;)Landroid/os/Parcelable$Creator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final synthetic val$instance:Lcom/squareup/container/spot/Spot;


# direct methods
.method constructor <init>(Lcom/squareup/container/spot/Spot;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/container/spot/Spot$1;->val$instance:Lcom/squareup/container/spot/Spot;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/spot/Spot;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")TT;"
        }
    .end annotation

    .line 106
    iget-object p1, p0, Lcom/squareup/container/spot/Spot$1;->val$instance:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 104
    invoke-virtual {p0, p1}, Lcom/squareup/container/spot/Spot$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/spot/Spot;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/container/spot/Spot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)[TT;"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lcom/squareup/container/spot/Spot$1;->val$instance:Lcom/squareup/container/spot/Spot;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, p1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 104
    invoke-virtual {p0, p1}, Lcom/squareup/container/spot/Spot$1;->newArray(I)[Lcom/squareup/container/spot/Spot;

    move-result-object p1

    return-object p1
.end method
