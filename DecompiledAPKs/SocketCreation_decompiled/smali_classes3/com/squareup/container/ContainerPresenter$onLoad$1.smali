.class final synthetic Lcom/squareup/container/ContainerPresenter$onLoad$1;
.super Lkotlin/jvm/internal/FunctionReference;
.source "ContainerPresenter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/ContainerPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function1<",
        "Lflow/Traversal;",
        "Lflow/Traversal;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00032\u0015\u0010\u0004\u001a\u00110\u0001\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u0007\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lflow/Traversal;",
        "V",
        "Lcom/squareup/container/ContainerView;",
        "p1",
        "Lkotlin/ParameterName;",
        "name",
        "traversal",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/container/ContainerPresenter;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/jvm/internal/FunctionReference;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "preprocessFlowTraversal"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/container/ContainerPresenter;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "preprocessFlowTraversal(Lflow/Traversal;)Lflow/Traversal;"

    return-object v0
.end method

.method public final invoke(Lflow/Traversal;)Lflow/Traversal;
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/container/ContainerPresenter$onLoad$1;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/container/ContainerPresenter;

    .line 174
    invoke-static {v0, p1}, Lcom/squareup/container/ContainerPresenter;->access$preprocessFlowTraversal(Lcom/squareup/container/ContainerPresenter;Lflow/Traversal;)Lflow/Traversal;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lflow/Traversal;

    invoke-virtual {p0, p1}, Lcom/squareup/container/ContainerPresenter$onLoad$1;->invoke(Lflow/Traversal;)Lflow/Traversal;

    move-result-object p1

    return-object p1
.end method
