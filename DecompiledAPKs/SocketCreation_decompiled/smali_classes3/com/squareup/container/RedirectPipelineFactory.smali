.class public final Lcom/squareup/container/RedirectPipelineFactory;
.super Ljava/lang/Object;
.source "RedirectPipelineFactory.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/container/RedirectPipelineFactory$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRedirectPipelineFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RedirectPipelineFactory.kt\ncom/squareup/container/RedirectPipelineFactory\n*L\n1#1,320:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B=\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0018\u0010\u0013\u001a\u000c\u0012\u0004\u0012\u00020\u00150\u0014j\u0002`\u00162\u0006\u0010\u0017\u001a\u00020\u0018R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u001d\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0011\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/container/RedirectPipelineFactory;",
        "",
        "layerIndex",
        "Lkotlin/Function1;",
        "Lcom/squareup/container/ContainerTreeKey;",
        "",
        "hasMasterLayout",
        "",
        "initialDetailScreenForAnnotation",
        "Lcom/squareup/container/layer/Master;",
        "Lflow/path/Path;",
        "additionalContainerLayerSetup",
        "Lcom/squareup/container/AdditionalContainerLayerSetup;",
        "(Lkotlin/jvm/functions/Function1;ZLkotlin/jvm/functions/Function1;Lcom/squareup/container/AdditionalContainerLayerSetup;)V",
        "getHasMasterLayout",
        "()Z",
        "getInitialDetailScreenForAnnotation",
        "()Lkotlin/jvm/functions/Function1;",
        "getLayerIndex",
        "buildPipeline",
        "",
        "Lcom/squareup/container/RedirectStep;",
        "Lcom/squareup/container/RedirectPipeline;",
        "containerScope",
        "Lmortar/MortarScope;",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/container/RedirectPipelineFactory$Companion;


# instance fields
.field private final additionalContainerLayerSetup:Lcom/squareup/container/AdditionalContainerLayerSetup;

.field private final hasMasterLayout:Z

.field private final initialDetailScreenForAnnotation:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/container/layer/Master;",
            "Lflow/path/Path;",
            ">;"
        }
    .end annotation
.end field

.field private final layerIndex:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/container/RedirectPipelineFactory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/container/RedirectPipelineFactory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/container/RedirectPipelineFactory;->Companion:Lcom/squareup/container/RedirectPipelineFactory$Companion;

    return-void
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function1;ZLkotlin/jvm/functions/Function1;Lcom/squareup/container/AdditionalContainerLayerSetup;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Ljava/lang/Integer;",
            ">;Z",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/container/layer/Master;",
            "+",
            "Lflow/path/Path;",
            ">;",
            "Lcom/squareup/container/AdditionalContainerLayerSetup;",
            ")V"
        }
    .end annotation

    const-string v0, "layerIndex"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialDetailScreenForAnnotation"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalContainerLayerSetup"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/container/RedirectPipelineFactory;->layerIndex:Lkotlin/jvm/functions/Function1;

    iput-boolean p2, p0, Lcom/squareup/container/RedirectPipelineFactory;->hasMasterLayout:Z

    iput-object p3, p0, Lcom/squareup/container/RedirectPipelineFactory;->initialDetailScreenForAnnotation:Lkotlin/jvm/functions/Function1;

    iput-object p4, p0, Lcom/squareup/container/RedirectPipelineFactory;->additionalContainerLayerSetup:Lcom/squareup/container/AdditionalContainerLayerSetup;

    return-void
.end method

.method public static final synthetic access$getAdditionalContainerLayerSetup$p(Lcom/squareup/container/RedirectPipelineFactory;)Lcom/squareup/container/AdditionalContainerLayerSetup;
    .locals 0

    .line 34
    iget-object p0, p0, Lcom/squareup/container/RedirectPipelineFactory;->additionalContainerLayerSetup:Lcom/squareup/container/AdditionalContainerLayerSetup;

    return-object p0
.end method


# virtual methods
.method public final buildPipeline(Lmortar/MortarScope;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmortar/MortarScope;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/container/RedirectStep;",
            ">;"
        }
    .end annotation

    const-string v0, "containerScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 44
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    sget-object v2, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$1;->INSTANCE:Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_0

    new-instance v3, Lcom/squareup/container/RedirectPipelineFactoryKt$sam$com_squareup_container_RedirectStep$0;

    invoke-direct {v3, v2}, Lcom/squareup/container/RedirectPipelineFactoryKt$sam$com_squareup_container_RedirectStep$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v2, v3

    :cond_0
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 45
    invoke-static {p1}, Lcom/squareup/container/BlockingBootstrapTreeKeyKt;->executeAndPopBlockingBootstrapTreeKey(Lmortar/MortarScope;)Lcom/squareup/container/RedirectStep;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 46
    new-instance p1, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$2;

    sget-object v2, Lcom/squareup/container/RedirectPipelineFactory;->Companion:Lcom/squareup/container/RedirectPipelineFactory$Companion;

    invoke-direct {p1, v2}, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$2;-><init>(Lcom/squareup/container/RedirectPipelineFactory$Companion;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/container/RedirectPipelineFactoryKt$sam$com_squareup_container_RedirectStep$0;

    invoke-direct {v2, p1}, Lcom/squareup/container/RedirectPipelineFactoryKt$sam$com_squareup_container_RedirectStep$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance p1, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$3;

    invoke-direct {p1, p0}, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$3;-><init>(Lcom/squareup/container/RedirectPipelineFactory;)V

    invoke-interface {v1, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 48
    new-instance p1, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$4;

    invoke-direct {p1, p0}, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$4;-><init>(Lcom/squareup/container/RedirectPipelineFactory;)V

    invoke-interface {v1, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 49
    new-instance p1, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$5;

    invoke-direct {p1, p0}, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$5;-><init>(Lcom/squareup/container/RedirectPipelineFactory;)V

    invoke-interface {v1, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 50
    new-instance p1, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$6;

    invoke-direct {p1, p0}, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$6;-><init>(Lcom/squareup/container/RedirectPipelineFactory;)V

    invoke-interface {v1, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 51
    new-instance p1, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$7;

    invoke-direct {p1, p0}, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$7;-><init>(Lcom/squareup/container/RedirectPipelineFactory;)V

    invoke-interface {v1, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 53
    new-instance p1, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$8;

    sget-object v2, Lcom/squareup/container/BootstrapTreeKey;->Companion:Lcom/squareup/container/BootstrapTreeKey$Companion;

    invoke-direct {p1, v2}, Lcom/squareup/container/RedirectPipelineFactory$buildPipeline$8;-><init>(Lcom/squareup/container/BootstrapTreeKey$Companion;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/container/RedirectPipelineFactoryKt$sam$com_squareup_container_RedirectStep$0;

    invoke-direct {v2, p1}, Lcom/squareup/container/RedirectPipelineFactoryKt$sam$com_squareup_container_RedirectStep$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public final getHasMasterLayout()Z
    .locals 1

    .line 36
    iget-boolean v0, p0, Lcom/squareup/container/RedirectPipelineFactory;->hasMasterLayout:Z

    return v0
.end method

.method public final getInitialDetailScreenForAnnotation()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/container/layer/Master;",
            "Lflow/path/Path;",
            ">;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/container/RedirectPipelineFactory;->initialDetailScreenForAnnotation:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getLayerIndex()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/container/ContainerTreeKey;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/container/RedirectPipelineFactory;->layerIndex:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method
