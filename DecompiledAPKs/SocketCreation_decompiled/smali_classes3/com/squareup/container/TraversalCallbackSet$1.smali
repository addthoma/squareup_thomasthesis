.class Lcom/squareup/container/TraversalCallbackSet$1;
.super Ljava/lang/Object;
.source "TraversalCallbackSet.java"

# interfaces
.implements Lflow/TraversalCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/container/TraversalCallbackSet;->add()Lflow/TraversalCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/container/TraversalCallbackSet;


# direct methods
.method constructor <init>(Lcom/squareup/container/TraversalCallbackSet;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/squareup/container/TraversalCallbackSet$1;->this$0:Lcom/squareup/container/TraversalCallbackSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTraversalCompleted()V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/container/TraversalCallbackSet$1;->this$0:Lcom/squareup/container/TraversalCallbackSet;

    invoke-static {v0}, Lcom/squareup/container/TraversalCallbackSet;->access$000(Lcom/squareup/container/TraversalCallbackSet;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/squareup/container/TraversalCallbackSet$1;->this$0:Lcom/squareup/container/TraversalCallbackSet;

    invoke-static {v0}, Lcom/squareup/container/TraversalCallbackSet;->access$100(Lcom/squareup/container/TraversalCallbackSet;)V

    return-void

    .line 43
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must not call more than once."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
