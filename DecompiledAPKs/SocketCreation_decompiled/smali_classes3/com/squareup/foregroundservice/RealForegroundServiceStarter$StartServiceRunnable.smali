.class Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;
.super Ljava/lang/Object;
.source "RealForegroundServiceStarter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/foregroundservice/RealForegroundServiceStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StartServiceRunnable"
.end annotation


# instance fields
.field private final serviceIntent:Landroid/content/Intent;

.field final synthetic this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;


# direct methods
.method private constructor <init>(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;Landroid/content/Intent;)V
    .locals 3

    .line 62
    iput-object p1, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 64
    invoke-static {p1}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$000(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    iput-object p2, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->serviceIntent:Landroid/content/Intent;

    return-void

    .line 68
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "This RealForegroundServiceStarter instance can only be used to start "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-static {p1}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$000(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " not "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method synthetic constructor <init>(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;Landroid/content/Intent;Lcom/squareup/foregroundservice/RealForegroundServiceStarter$1;)V
    .locals 0

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;-><init>(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$700(Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->waitForForeground()V

    return-void
.end method

.method static synthetic access$800(Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->startForegroundService()V

    return-void
.end method

.method static synthetic access$900(Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->startService()V

    return-void
.end method

.method private cleanupQueuedWork()V
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$100(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$300(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v1}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$100(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$102(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;)Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    :cond_0
    return-void
.end method

.method private startForegroundService()V
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->serviceIntent:Landroid/content/Intent;

    invoke-static {}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$400()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 113
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$500(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Landroid/app/Application;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->serviceIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/app/Application;->startForegroundService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 114
    invoke-direct {p0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->cleanupQueuedWork()V

    return-void
.end method

.method private startService()V
    .locals 3

    .line 118
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->serviceIntent:Landroid/content/Intent;

    invoke-static {}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$400()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 119
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$500(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Landroid/app/Application;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->serviceIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/app/Application;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 120
    invoke-direct {p0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->cleanupQueuedWork()V

    return-void
.end method

.method private waitForForeground()V
    .locals 4

    .line 78
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 79
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-virtual {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->shouldStartAsForegroundService()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$100(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$200(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/log/OhSnapLogger;

    move-result-object v0

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    .line 82
    invoke-static {v3}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$000(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " not started, already waiting for foreground"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 81
    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v0, p0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$102(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;)Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    .line 87
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$200(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/log/OhSnapLogger;

    move-result-object v0

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v3}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$000(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " not started, now waiting for foreground"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$300(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v1}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$100(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;

    move-result-object v1

    const-wide/16 v2, 0x4e20

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$200(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/log/OhSnapLogger;

    move-result-object v0

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v3}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$000(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " starting as normal service without waiting, we\'re in foreground"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 94
    invoke-direct {p0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->startService()V

    :goto_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 99
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-virtual {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->shouldStartAsForegroundService()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$200(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/log/OhSnapLogger;

    move-result-object v0

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    .line 101
    invoke-static {v3}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$000(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " starting as foreground service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 100
    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 102
    invoke-direct {p0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->startForegroundService()V

    goto :goto_0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    invoke-static {v0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$200(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Lcom/squareup/log/OhSnapLogger;

    move-result-object v0

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->QUEUE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->this$0:Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    .line 105
    invoke-static {v3}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;->access$000(Lcom/squareup/foregroundservice/RealForegroundServiceStarter;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " starting as normal service, we\'re in foreground"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 104
    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 106
    invoke-direct {p0}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter$StartServiceRunnable;->startService()V

    :goto_0
    return-void
.end method
