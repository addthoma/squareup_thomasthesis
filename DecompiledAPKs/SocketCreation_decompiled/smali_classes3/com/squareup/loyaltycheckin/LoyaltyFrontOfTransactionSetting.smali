.class public interface abstract Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;
.super Ljava/lang/Object;
.source "LoyaltyFrontOfTransactionSetting.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u000e\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0006H&J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0003H&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionSetting;",
        "",
        "featureFlagOn",
        "",
        "getValue",
        "getValuesStream",
        "Lio/reactivex/Observable;",
        "setValueLocallyInternal",
        "",
        "enabledLocally",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract featureFlagOn()Z
.end method

.method public abstract getValue()Z
.end method

.method public abstract getValuesStream()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setValueLocallyInternal(Z)V
.end method
