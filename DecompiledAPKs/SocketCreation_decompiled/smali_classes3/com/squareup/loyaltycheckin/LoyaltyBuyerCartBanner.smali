.class public abstract Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner;
.super Ljava/lang/Object;
.source "LoyaltyBuyerCartBannerFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$NoBanner;,
        Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$Banner;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0005\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0003\u001a\u0004\u0018\u00010\u0004H&\u0082\u0001\u0002\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner;",
        "",
        "()V",
        "toProto",
        "Lcom/squareup/comms/protos/seller/LoyaltyCartBanner;",
        "Banner",
        "NoBanner",
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$NoBanner;",
        "Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner$Banner;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/loyaltycheckin/LoyaltyBuyerCartBanner;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract toProto()Lcom/squareup/comms/protos/seller/LoyaltyCartBanner;
.end method
