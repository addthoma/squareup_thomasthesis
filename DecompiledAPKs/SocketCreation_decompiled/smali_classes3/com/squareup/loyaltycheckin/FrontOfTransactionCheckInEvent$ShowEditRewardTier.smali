.class public final Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;
.super Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent;
.source "FrontOfTransactionCheckInEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowEditRewardTier"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\t\u0010\u000c\u001a\u00020\u0003H\u00d6\u0001J\u0013\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u00d6\u0003J\t\u0010\u0011\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\u0019\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;",
        "Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent;",
        "tierIndex",
        "",
        "quantity",
        "(II)V",
        "getQuantity",
        "()I",
        "getTierIndex",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final quantity:I

.field private final tierIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier$Creator;

    invoke-direct {v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier$Creator;-><init>()V

    sput-object v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0, v0}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->tierIndex:I

    iput p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->quantity:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;IIILjava/lang/Object;)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->tierIndex:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->quantity:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->copy(II)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->tierIndex:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->quantity:I

    return v0
.end method

.method public final copy(II)Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;
    .locals 1

    new-instance v0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;

    invoke-direct {v0, p1, p2}, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;-><init>(II)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;

    iget v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->tierIndex:I

    iget v1, p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->tierIndex:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->quantity:I

    iget p1, p1, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->quantity:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getQuantity()I
    .locals 1

    .line 30
    iget v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->quantity:I

    return v0
.end method

.method public final getTierIndex()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->tierIndex:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->tierIndex:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->quantity:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowEditRewardTier(tierIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->tierIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->quantity:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->tierIndex:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget p2, p0, Lcom/squareup/loyaltycheckin/FrontOfTransactionCheckInEvent$ShowEditRewardTier;->quantity:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
