.class final Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$goBackToRewardCarouselAction$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RewardCarouselWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow;-><init>(Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$goBackToRewardCarouselAction$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$goBackToRewardCarouselAction$1;

    invoke-direct {v0}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$goBackToRewardCarouselAction$1;-><init>()V

    sput-object v0, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$goBackToRewardCarouselAction$1;->INSTANCE:Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$goBackToRewardCarouselAction$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Mutator;

    invoke-virtual {p0, p1}, Lcom/squareup/loyaltycheckin/RewardCarouselWorkflow$goBackToRewardCarouselAction$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/loyaltycheckin/RewardCarouselState;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    sget-object v0, Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardCarousel;->INSTANCE:Lcom/squareup/loyaltycheckin/RewardCarouselState$RewardCarousel;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    const/4 p1, 0x0

    return-object p1
.end method
