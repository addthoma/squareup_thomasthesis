.class public final Lcom/squareup/mosaic/components/NightModeUiModel;
.super Lcom/squareup/mosaic/core/UiModel;
.source "NightModeUiModel.kt"

# interfaces
.implements Lcom/squareup/mosaic/core/UiModelContext;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mosaic/core/UiModel<",
        "TP;>;",
        "Lcom/squareup/mosaic/core/UiModelContext<",
        "TP;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNightModeUiModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NightModeUiModel.kt\ncom/squareup/mosaic/components/NightModeUiModel\n*L\n1#1,45:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u00032\u0008\u0012\u0004\u0012\u0002H\u00010\u0004B\u001f\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012\u0010\u0008\u0002\u0010\u0006\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0007J\u0016\u0010\u0011\u001a\u00020\u00122\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0003H\u0016J\u0008\u0010\u0014\u001a\u00020\u0012H\u0001J\u000e\u0010\u0015\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000fJ\u0016\u0010\u0016\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0003H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0017J0\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0005\u001a\u00028\u00002\u0010\u0008\u0002\u0010\u0006\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0003H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0019J\r\u0010\u001a\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u000fJ\u0018\u0010\u001b\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0013\u0010\u001f\u001a\u00020 2\u0008\u0010!\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010\"\u001a\u00020#H\u00d6\u0001J\t\u0010$\u001a\u00020%H\u00d6\u0001R,\u0010\u0006\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u00038\u0000@\u0000X\u0081\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\u0008\u0010\t\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u0016\u0010\u0005\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0010\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/NightModeUiModel;",
        "P",
        "",
        "Lcom/squareup/mosaic/core/UiModel;",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "params",
        "innerModel",
        "(Ljava/lang/Object;Lcom/squareup/mosaic/core/UiModel;)V",
        "innerModel$annotations",
        "()V",
        "getInnerModel",
        "()Lcom/squareup/mosaic/core/UiModel;",
        "setInnerModel",
        "(Lcom/squareup/mosaic/core/UiModel;)V",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "add",
        "",
        "model",
        "checkInitialized",
        "component1",
        "component2",
        "component2$public_release",
        "copy",
        "(Ljava/lang/Object;Lcom/squareup/mosaic/core/UiModel;)Lcom/squareup/mosaic/components/NightModeUiModel;",
        "createParams",
        "createViewRef",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "context",
        "Landroid/content/Context;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private innerModel:Lcom/squareup/mosaic/core/UiModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;"
        }
    .end annotation
.end field

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/squareup/mosaic/core/UiModel;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->params:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Lcom/squareup/mosaic/core/UiModel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 26
    check-cast p2, Lcom/squareup/mosaic/core/UiModel;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/mosaic/components/NightModeUiModel;-><init>(Ljava/lang/Object;Lcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/components/NightModeUiModel;Ljava/lang/Object;Lcom/squareup/mosaic/core/UiModel;ILjava/lang/Object;)Lcom/squareup/mosaic/components/NightModeUiModel;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/NightModeUiModel;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/NightModeUiModel;->copy(Ljava/lang/Object;Lcom/squareup/mosaic/core/UiModel;)Lcom/squareup/mosaic/components/NightModeUiModel;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic innerModel$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/mosaic/core/UiModel;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;)V"
        }
    .end annotation

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 36
    iput-object p1, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    return-void

    .line 34
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected NightModel to only have one inner model, but got "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p1, 0x2e

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 33
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final checkInitialized()V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected NightModel to get an inner model."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/NightModeUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2$public_release()Lcom/squareup/mosaic/core/UiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;Lcom/squareup/mosaic/core/UiModel;)Lcom/squareup/mosaic/components/NightModeUiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;)",
            "Lcom/squareup/mosaic/components/NightModeUiModel<",
            "TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mosaic/components/NightModeUiModel;

    invoke-direct {v0, p1, p2}, Lcom/squareup/mosaic/components/NightModeUiModel;-><init>(Ljava/lang/Object;Lcom/squareup/mosaic/core/UiModel;)V

    return-object v0
.end method

.method public createParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 39
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/NightModeUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createViewRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/squareup/mosaic/components/NightModeViewRef;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/components/NightModeViewRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/ViewRef;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/components/NightModeUiModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/components/NightModeUiModel;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/NightModeUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/NightModeUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    iget-object p1, p1, Lcom/squareup/mosaic/components/NightModeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getInnerModel()Lcom/squareup/mosaic/core/UiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    return-object v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/NightModeUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public final setInnerModel(Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "TP;>;)V"
        }
    .end annotation

    .line 26
    iput-object p1, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NightModeUiModel(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/NightModeUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", innerModel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/NightModeUiModel;->innerModel:Lcom/squareup/mosaic/core/UiModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
