.class public final Lcom/squareup/mosaic/components/NohoActionBarUiModel;
.super Lcom/squareup/mosaic/core/StandardUiModel;
.source "NohoActionBarUiModel.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mosaic/core/StandardUiModel<",
        "Lcom/squareup/noho/NohoActionBar;",
        "TP;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u0003B\u0017\u0008\u0001\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\u000e\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000cJ\u000e\u0010\u000f\u001a\u00020\u0007H\u00c0\u0003\u00a2\u0006\u0002\u0008\u0010J(\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0005\u001a\u00028\u00002\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0012J\u0018\u0010\u0013\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0013\u0010\u0017\u001a\u00020\u00182\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001R\u0014\u0010\u0006\u001a\u00020\u0007X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0016\u0010\u0005\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\r\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/NohoActionBarUiModel;",
        "P",
        "",
        "Lcom/squareup/mosaic/core/StandardUiModel;",
        "Lcom/squareup/noho/NohoActionBar;",
        "params",
        "configBuilder",
        "Lcom/squareup/noho/NohoActionBar$Config$Builder;",
        "(Ljava/lang/Object;Lcom/squareup/noho/NohoActionBar$Config$Builder;)V",
        "getConfigBuilder$public_release",
        "()Lcom/squareup/noho/NohoActionBar$Config$Builder;",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "component1",
        "component2",
        "component2$public_release",
        "copy",
        "(Ljava/lang/Object;Lcom/squareup/noho/NohoActionBar$Config$Builder;)Lcom/squareup/mosaic/components/NohoActionBarUiModel;",
        "createViewRef",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "context",
        "Landroid/content/Context;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final configBuilder:Lcom/squareup/noho/NohoActionBar$Config$Builder;

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/noho/NohoActionBar$Config$Builder;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/squareup/noho/NohoActionBar$Config$Builder;",
            ")V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configBuilder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Lcom/squareup/mosaic/core/StandardUiModel;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->params:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->configBuilder:Lcom/squareup/noho/NohoActionBar$Config$Builder;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/components/NohoActionBarUiModel;Ljava/lang/Object;Lcom/squareup/noho/NohoActionBar$Config$Builder;ILjava/lang/Object;)Lcom/squareup/mosaic/components/NohoActionBarUiModel;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->configBuilder:Lcom/squareup/noho/NohoActionBar$Config$Builder;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->copy(Ljava/lang/Object;Lcom/squareup/noho/NohoActionBar$Config$Builder;)Lcom/squareup/mosaic/components/NohoActionBarUiModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2$public_release()Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->configBuilder:Lcom/squareup/noho/NohoActionBar$Config$Builder;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;Lcom/squareup/noho/NohoActionBar$Config$Builder;)Lcom/squareup/mosaic/components/NohoActionBarUiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;",
            "Lcom/squareup/noho/NohoActionBar$Config$Builder;",
            ")",
            "Lcom/squareup/mosaic/components/NohoActionBarUiModel<",
            "TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configBuilder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mosaic/components/NohoActionBarUiModel;

    invoke-direct {v0, p1, p2}, Lcom/squareup/mosaic/components/NohoActionBarUiModel;-><init>(Ljava/lang/Object;Lcom/squareup/noho/NohoActionBar$Config$Builder;)V

    return-object v0
.end method

.method public createViewRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/squareup/mosaic/components/NohoActionBarViewRef;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/components/NohoActionBarViewRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/ViewRef;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/components/NohoActionBarUiModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/components/NohoActionBarUiModel;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->configBuilder:Lcom/squareup/noho/NohoActionBar$Config$Builder;

    iget-object p1, p1, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->configBuilder:Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getConfigBuilder$public_release()Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->configBuilder:Lcom/squareup/noho/NohoActionBar$Config$Builder;

    return-object v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->configBuilder:Lcom/squareup/noho/NohoActionBar$Config$Builder;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NohoActionBarUiModel(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", configBuilder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/NohoActionBarUiModel;->configBuilder:Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
