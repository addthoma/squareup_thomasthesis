.class public final Lcom/squareup/mosaic/components/VerticalScrollUiModel;
.super Lcom/squareup/mosaic/core/StandardUiModel;
.source "VerticalScrollUiModel.kt"

# interfaces
.implements Lcom/squareup/mosaic/core/UiModelContext;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<P:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/mosaic/core/StandardUiModel<",
        "Landroid/widget/ScrollView;",
        "TP;>;",
        "Lcom/squareup/mosaic/core/UiModelContext<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u0002H\u00010\u00032\u0008\u0012\u0004\u0012\u00020\u00060\u0005B)\u0012\u0006\u0010\u0007\u001a\u00028\u0000\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u0012\u0010\u0008\u0002\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0016\u0010\u0018\u001a\u00020\u00062\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u000bH\u0016J\u000e\u0010\u001a\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0012J\t\u0010\u001b\u001a\u00020\tH\u00c6\u0003J\u0016\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u000bH\u00c0\u0003\u00a2\u0006\u0002\u0008\u001dJ:\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0007\u001a\u00028\u00002\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0010\u0008\u0002\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u000bH\u00c6\u0001\u00a2\u0006\u0002\u0010\u001fJ\u0008\u0010 \u001a\u00020\u0006H\u0016J\u0018\u0010!\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"2\u0006\u0010#\u001a\u00020$H\u0016J\u0013\u0010%\u001a\u00020\t2\u0008\u0010&\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010\'\u001a\u00020(H\u00d6\u0001J\t\u0010)\u001a\u00020*H\u00d6\u0001R\u001a\u0010\u0008\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\r\u0010\u000e\"\u0004\u0008\u000f\u0010\u0010R\u0016\u0010\u0007\u001a\u00028\u0000X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0013\u001a\u0004\u0008\u0011\u0010\u0012R\"\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u000bX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\"\u0004\u0008\u0016\u0010\u0017\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/VerticalScrollUiModel;",
        "P",
        "",
        "Lcom/squareup/mosaic/core/StandardUiModel;",
        "Landroid/widget/ScrollView;",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "",
        "params",
        "fillViewport",
        "",
        "subModel",
        "Lcom/squareup/mosaic/core/UiModel;",
        "(Ljava/lang/Object;ZLcom/squareup/mosaic/core/UiModel;)V",
        "getFillViewport",
        "()Z",
        "setFillViewport",
        "(Z)V",
        "getParams",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getSubModel$public_release",
        "()Lcom/squareup/mosaic/core/UiModel;",
        "setSubModel$public_release",
        "(Lcom/squareup/mosaic/core/UiModel;)V",
        "add",
        "model",
        "component1",
        "component2",
        "component3",
        "component3$public_release",
        "copy",
        "(Ljava/lang/Object;ZLcom/squareup/mosaic/core/UiModel;)Lcom/squareup/mosaic/components/VerticalScrollUiModel;",
        "createParams",
        "createViewRef",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "context",
        "Landroid/content/Context;",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private fillViewport:Z

.field private final params:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private subModel:Lcom/squareup/mosaic/core/UiModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;ZLcom/squareup/mosaic/core/UiModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;Z",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Lcom/squareup/mosaic/core/StandardUiModel;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->params:Ljava/lang/Object;

    iput-boolean p2, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->fillViewport:Z

    iput-object p3, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->subModel:Lcom/squareup/mosaic/core/UiModel;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;ZLcom/squareup/mosaic/core/UiModel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 28
    check-cast p3, Lcom/squareup/mosaic/core/UiModel;

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/mosaic/components/VerticalScrollUiModel;-><init>(Ljava/lang/Object;ZLcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/components/VerticalScrollUiModel;Ljava/lang/Object;ZLcom/squareup/mosaic/core/UiModel;ILjava/lang/Object;)Lcom/squareup/mosaic/components/VerticalScrollUiModel;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->getParams()Ljava/lang/Object;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->fillViewport:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->subModel:Lcom/squareup/mosaic/core/UiModel;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->copy(Ljava/lang/Object;ZLcom/squareup/mosaic/core/UiModel;)Lcom/squareup/mosaic/components/VerticalScrollUiModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public add(Lcom/squareup/mosaic/core/UiModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iput-object p1, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->subModel:Lcom/squareup/mosaic/core/UiModel;

    return-void
.end method

.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->fillViewport:Z

    return v0
.end method

.method public final component3$public_release()Lcom/squareup/mosaic/core/UiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->subModel:Lcom/squareup/mosaic/core/UiModel;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;ZLcom/squareup/mosaic/core/UiModel;)Lcom/squareup/mosaic/components/VerticalScrollUiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;Z",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/mosaic/components/VerticalScrollUiModel<",
            "TP;>;"
        }
    .end annotation

    const-string v0, "params"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/mosaic/components/VerticalScrollUiModel;-><init>(Ljava/lang/Object;ZLcom/squareup/mosaic/core/UiModel;)V

    return-object v0
.end method

.method public bridge synthetic createParams()Ljava/lang/Object;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->createParams()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public createParams()V
    .locals 0

    return-void
.end method

.method public createViewRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/squareup/mosaic/components/VerticalScrollViewRef;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/components/VerticalScrollViewRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/ViewRef;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/components/VerticalScrollUiModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/components/VerticalScrollUiModel;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->fillViewport:Z

    iget-boolean v1, p1, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->fillViewport:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->subModel:Lcom/squareup/mosaic/core/UiModel;

    iget-object p1, p1, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->subModel:Lcom/squareup/mosaic/core/UiModel;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFillViewport()Z
    .locals 1

    .line 27
    iget-boolean v0, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->fillViewport:Z

    return v0
.end method

.method public getParams()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->params:Ljava/lang/Object;

    return-object v0
.end method

.method public final getSubModel$public_release()Lcom/squareup/mosaic/core/UiModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->subModel:Lcom/squareup/mosaic/core/UiModel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->fillViewport:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->subModel:Lcom/squareup/mosaic/core/UiModel;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final setFillViewport(Z)V
    .locals 0

    .line 27
    iput-boolean p1, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->fillViewport:Z

    return-void
.end method

.method public final setSubModel$public_release(Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 28
    iput-object p1, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->subModel:Lcom/squareup/mosaic/core/UiModel;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VerticalScrollUiModel(params="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", fillViewport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->fillViewport:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", subModel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/components/VerticalScrollUiModel;->subModel:Lcom/squareup/mosaic/core/UiModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
