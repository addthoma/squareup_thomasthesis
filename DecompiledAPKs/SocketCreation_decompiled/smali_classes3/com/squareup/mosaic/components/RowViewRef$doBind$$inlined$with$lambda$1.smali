.class final Lcom/squareup/mosaic/components/RowViewRef$doBind$$inlined$with$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RowViewRef.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/mosaic/components/RowViewRef;->doBind(Lcom/squareup/mosaic/components/RowUiModel;Lcom/squareup/mosaic/components/RowUiModel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0004\n\u0002\u0008\u0005\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006\u00a8\u0006\n"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "isChecked",
        "",
        "invoke",
        "com/squareup/mosaic/components/RowViewRef$doBind$1$5$1$1$1",
        "com/squareup/mosaic/components/RowViewRef$$special$$inlined$let$lambda$1",
        "com/squareup/mosaic/components/RowViewRef$$special$$inlined$ifChanged$lambda$1",
        "com/squareup/mosaic/components/RowViewRef$$special$$inlined$run$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $handler:Lkotlin/jvm/functions/Function1;

.field final synthetic $newModel$inlined:Lcom/squareup/mosaic/components/RowUiModel;

.field final synthetic $oldModel$inlined:Lcom/squareup/mosaic/components/RowUiModel;

.field final synthetic $this_run$inlined:Lcom/squareup/noho/NohoCheckableRow;

.field final synthetic this$0:Lcom/squareup/mosaic/components/RowViewRef;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function1;Lcom/squareup/noho/NohoCheckableRow;Lcom/squareup/mosaic/components/RowViewRef;Lcom/squareup/mosaic/components/RowUiModel;Lcom/squareup/mosaic/components/RowUiModel;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/mosaic/components/RowViewRef$doBind$$inlined$with$lambda$1;->$handler:Lkotlin/jvm/functions/Function1;

    iput-object p2, p0, Lcom/squareup/mosaic/components/RowViewRef$doBind$$inlined$with$lambda$1;->$this_run$inlined:Lcom/squareup/noho/NohoCheckableRow;

    iput-object p3, p0, Lcom/squareup/mosaic/components/RowViewRef$doBind$$inlined$with$lambda$1;->this$0:Lcom/squareup/mosaic/components/RowViewRef;

    iput-object p4, p0, Lcom/squareup/mosaic/components/RowViewRef$doBind$$inlined$with$lambda$1;->$oldModel$inlined:Lcom/squareup/mosaic/components/RowUiModel;

    iput-object p5, p0, Lcom/squareup/mosaic/components/RowViewRef$doBind$$inlined$with$lambda$1;->$newModel$inlined:Lcom/squareup/mosaic/components/RowUiModel;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/RowViewRef$doBind$$inlined$with$lambda$1;->invoke(Lcom/squareup/noho/NohoCheckableRow;Z)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/noho/NohoCheckableRow;Z)V
    .locals 1

    const-string v0, "<anonymous parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object p1, p0, Lcom/squareup/mosaic/components/RowViewRef$doBind$$inlined$with$lambda$1;->this$0:Lcom/squareup/mosaic/components/RowViewRef;

    invoke-static {p1}, Lcom/squareup/mosaic/components/RowViewRef;->access$isBinding$p(Lcom/squareup/mosaic/components/RowViewRef;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/mosaic/components/RowViewRef$doBind$$inlined$with$lambda$1;->$handler:Lkotlin/jvm/functions/Function1;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
