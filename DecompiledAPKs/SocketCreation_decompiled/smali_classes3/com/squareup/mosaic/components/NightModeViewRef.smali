.class public final Lcom/squareup/mosaic/components/NightModeViewRef;
.super Lcom/squareup/mosaic/core/ViewRef;
.source "NightModeViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/core/ViewRef<",
        "Lcom/squareup/mosaic/components/NightModeUiModel<",
        "*>;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNightModeViewRef.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NightModeViewRef.kt\ncom/squareup/mosaic/components/NightModeViewRef\n*L\n1#1,48:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u0012\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J \u0010\u000c\u001a\u00020\r2\n\u0010\u000e\u001a\u0006\u0012\u0002\u0008\u00030\u00022\n\u0010\u000f\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0014J\"\u0010\u0010\u001a\u00020\u00112\u000c\u0010\u0012\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\u00022\n\u0010\u000f\u001a\u0006\u0012\u0002\u0008\u00030\u0002H\u0016J\u0010\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0008\u0010\u0016\u001a\u00020\u0015H\u0016R\u0014\u0010\u0007\u001a\u00020\u00038VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0018\u0010\n\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\u0001X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/NightModeViewRef;",
        "Lcom/squareup/mosaic/core/ViewRef;",
        "Lcom/squareup/mosaic/components/NightModeUiModel;",
        "Landroid/view/View;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "androidView",
        "getAndroidView",
        "()Landroid/view/View;",
        "innerViewRef",
        "nightContext",
        "canUpdateTo",
        "",
        "currentModel",
        "newModel",
        "doBind",
        "",
        "oldModel",
        "restoreInstanceState",
        "parcelable",
        "Landroid/os/Parcelable;",
        "saveInstanceState",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private innerViewRef:Lcom/squareup/mosaic/core/ViewRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/ViewRef<",
            "**>;"
        }
    .end annotation
.end field

.field private final nightContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Lcom/squareup/mosaic/core/ViewRef;-><init>()V

    .line 13
    invoke-static {p1}, Lcom/squareup/noho/NightModeUtilsKt;->nightMode(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/mosaic/components/NightModeViewRef;->nightContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected canUpdateTo(Lcom/squareup/mosaic/components/NightModeUiModel;Lcom/squareup/mosaic/components/NightModeUiModel;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/NightModeUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/NightModeUiModel<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "currentModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "newModel"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object p1, p0, Lcom/squareup/mosaic/components/NightModeViewRef;->innerViewRef:Lcom/squareup/mosaic/core/ViewRef;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/NightModeUiModel;->getInnerModel()Lcom/squareup/mosaic/core/UiModel;

    move-result-object p2

    if-nez p2, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {p1, p2}, Lcom/squareup/mosaic/core/ViewRef;->canAccept(Lcom/squareup/mosaic/core/UiModel;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic canUpdateTo(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)Z
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/mosaic/components/NightModeUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/NightModeUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/NightModeViewRef;->canUpdateTo(Lcom/squareup/mosaic/components/NightModeUiModel;Lcom/squareup/mosaic/components/NightModeUiModel;)Z

    move-result p1

    return p1
.end method

.method public doBind(Lcom/squareup/mosaic/components/NightModeUiModel;Lcom/squareup/mosaic/components/NightModeUiModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/components/NightModeUiModel<",
            "*>;",
            "Lcom/squareup/mosaic/components/NightModeUiModel<",
            "*>;)V"
        }
    .end annotation

    const-string p1, "newModel"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-virtual {p2}, Lcom/squareup/mosaic/components/NightModeUiModel;->getInnerModel()Lcom/squareup/mosaic/core/UiModel;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 37
    :cond_0
    iget-object p2, p0, Lcom/squareup/mosaic/components/NightModeViewRef;->innerViewRef:Lcom/squareup/mosaic/core/ViewRef;

    if-eqz p2, :cond_1

    .line 39
    invoke-static {p2, p1}, Lcom/squareup/mosaic/core/ViewRefKt;->castAndBind(Lcom/squareup/mosaic/core/ViewRef;Lcom/squareup/mosaic/core/UiModel;)V

    if-eqz p2, :cond_1

    goto :goto_0

    .line 40
    :cond_1
    iget-object p2, p0, Lcom/squareup/mosaic/components/NightModeViewRef;->nightContext:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/squareup/mosaic/core/UiModelKt;->toView(Lcom/squareup/mosaic/core/UiModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/ViewRef;

    move-result-object p2

    :goto_0
    iput-object p2, p0, Lcom/squareup/mosaic/components/NightModeViewRef;->innerViewRef:Lcom/squareup/mosaic/core/ViewRef;

    return-void
.end method

.method public bridge synthetic doBind(Lcom/squareup/mosaic/core/UiModel;Lcom/squareup/mosaic/core/UiModel;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/mosaic/components/NightModeUiModel;

    check-cast p2, Lcom/squareup/mosaic/components/NightModeUiModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/components/NightModeViewRef;->doBind(Lcom/squareup/mosaic/components/NightModeUiModel;Lcom/squareup/mosaic/components/NightModeUiModel;)V

    return-void
.end method

.method public getAndroidView()Landroid/view/View;
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/mosaic/components/NightModeViewRef;->innerViewRef:Lcom/squareup/mosaic/core/ViewRef;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/mosaic/core/ViewRef;->getAndroidView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public restoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    const-string v0, "parcelable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/mosaic/components/NightModeViewRef;->innerViewRef:Lcom/squareup/mosaic/core/ViewRef;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/mosaic/core/ViewRef;->restoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method public saveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/mosaic/components/NightModeViewRef;->innerViewRef:Lcom/squareup/mosaic/core/ViewRef;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/mosaic/core/ViewRef;->saveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method
