.class public final Lcom/squareup/mosaic/components/DividerEdge;
.super Ljava/lang/Object;
.source "VerticalDividersStackUiModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mosaic/components/DividerEdge$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\r\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086@\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\u0012\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00f8\u0001\u0000\u00a2\u0006\u0004\u0008\u0004\u0010\u0005J\u0010\u0010\u0008\u001a\u00020\u0000\u00f8\u0001\u0000\u00a2\u0006\u0004\u0008\t\u0010\u0005J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0000\u00f8\u0001\u0000\u00a2\u0006\u0004\u0008\r\u0010\u000eJ\u0013\u0010\u000f\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00d6\u0001J\u0018\u0010\u0011\u001a\u00020\u00002\u0006\u0010\u000c\u001a\u00020\u0000\u00f8\u0001\u0000\u00a2\u0006\u0004\u0008\u0012\u0010\u0013J\u001b\u0010\u0014\u001a\u00020\u00002\u0006\u0010\u000c\u001a\u00020\u0000H\u0086\u0002\u00f8\u0001\u0000\u00a2\u0006\u0004\u0008\u0015\u0010\u0013J\u001b\u0010\u0016\u001a\u00020\u00002\u0006\u0010\u000c\u001a\u00020\u0000H\u0086\u0002\u00f8\u0001\u0000\u00a2\u0006\u0004\u0008\u0017\u0010\u0013J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00f8\u0001\u0000\u0082\u0002\u0004\n\u0002\u0008\u0019\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/mosaic/components/DividerEdge;",
        "",
        "value",
        "",
        "constructor-impl",
        "(I)I",
        "getValue",
        "()I",
        "complement",
        "complement-impl",
        "contains",
        "",
        "other",
        "contains-mqrhnls",
        "(II)Z",
        "equals",
        "hashCode",
        "intersect",
        "intersect-mqrhnls",
        "(II)I",
        "minus",
        "minus-mqrhnls",
        "plus",
        "plus-mqrhnls",
        "toString",
        "",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ALL:I

.field private static final BOTTOM:I

.field public static final Companion:Lcom/squareup/mosaic/components/DividerEdge$Companion;

.field private static final LEFT:I

.field private static final NONE:I

.field private static final RIGHT:I

.field private static final TOP:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/mosaic/components/DividerEdge$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/mosaic/components/DividerEdge$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/mosaic/components/DividerEdge;->Companion:Lcom/squareup/mosaic/components/DividerEdge$Companion;

    const/4 v0, 0x0

    .line 78
    invoke-static {v0}, Lcom/squareup/mosaic/components/DividerEdge;->constructor-impl(I)I

    move-result v0

    sput v0, Lcom/squareup/mosaic/components/DividerEdge;->NONE:I

    const/4 v0, 0x1

    .line 79
    invoke-static {v0}, Lcom/squareup/mosaic/components/DividerEdge;->constructor-impl(I)I

    move-result v0

    sput v0, Lcom/squareup/mosaic/components/DividerEdge;->LEFT:I

    const/4 v0, 0x2

    .line 80
    invoke-static {v0}, Lcom/squareup/mosaic/components/DividerEdge;->constructor-impl(I)I

    move-result v0

    sput v0, Lcom/squareup/mosaic/components/DividerEdge;->TOP:I

    const/4 v0, 0x4

    .line 81
    invoke-static {v0}, Lcom/squareup/mosaic/components/DividerEdge;->constructor-impl(I)I

    move-result v0

    sput v0, Lcom/squareup/mosaic/components/DividerEdge;->RIGHT:I

    const/16 v0, 0x8

    .line 82
    invoke-static {v0}, Lcom/squareup/mosaic/components/DividerEdge;->constructor-impl(I)I

    move-result v0

    sput v0, Lcom/squareup/mosaic/components/DividerEdge;->BOTTOM:I

    .line 83
    sget v0, Lcom/squareup/mosaic/components/DividerEdge;->LEFT:I

    sget v1, Lcom/squareup/mosaic/components/DividerEdge;->TOP:I

    invoke-static {v0, v1}, Lcom/squareup/mosaic/components/DividerEdge;->plus-mqrhnls(II)I

    move-result v0

    sget v1, Lcom/squareup/mosaic/components/DividerEdge;->RIGHT:I

    invoke-static {v0, v1}, Lcom/squareup/mosaic/components/DividerEdge;->plus-mqrhnls(II)I

    move-result v0

    sget v1, Lcom/squareup/mosaic/components/DividerEdge;->BOTTOM:I

    invoke-static {v0, v1}, Lcom/squareup/mosaic/components/DividerEdge;->plus-mqrhnls(II)I

    move-result v0

    sput v0, Lcom/squareup/mosaic/components/DividerEdge;->ALL:I

    return-void
.end method

.method private synthetic constructor <init>(I)V
    .locals 0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/mosaic/components/DividerEdge;->value:I

    return-void
.end method

.method public static final synthetic access$getALL$cp()I
    .locals 1

    .line 76
    sget v0, Lcom/squareup/mosaic/components/DividerEdge;->ALL:I

    return v0
.end method

.method public static final synthetic access$getBOTTOM$cp()I
    .locals 1

    .line 76
    sget v0, Lcom/squareup/mosaic/components/DividerEdge;->BOTTOM:I

    return v0
.end method

.method public static final synthetic access$getLEFT$cp()I
    .locals 1

    .line 76
    sget v0, Lcom/squareup/mosaic/components/DividerEdge;->LEFT:I

    return v0
.end method

.method public static final synthetic access$getNONE$cp()I
    .locals 1

    .line 76
    sget v0, Lcom/squareup/mosaic/components/DividerEdge;->NONE:I

    return v0
.end method

.method public static final synthetic access$getRIGHT$cp()I
    .locals 1

    .line 76
    sget v0, Lcom/squareup/mosaic/components/DividerEdge;->RIGHT:I

    return v0
.end method

.method public static final synthetic access$getTOP$cp()I
    .locals 1

    .line 76
    sget v0, Lcom/squareup/mosaic/components/DividerEdge;->TOP:I

    return v0
.end method

.method public static final synthetic box-impl(I)Lcom/squareup/mosaic/components/DividerEdge;
    .locals 1

    new-instance v0, Lcom/squareup/mosaic/components/DividerEdge;

    invoke-direct {v0, p0}, Lcom/squareup/mosaic/components/DividerEdge;-><init>(I)V

    return-object v0
.end method

.method public static final complement-impl(I)I
    .locals 1

    .line 96
    sget v0, Lcom/squareup/mosaic/components/DividerEdge;->ALL:I

    invoke-static {v0, p0}, Lcom/squareup/mosaic/components/DividerEdge;->minus-mqrhnls(II)I

    move-result p0

    return p0
.end method

.method public static constructor-impl(I)I
    .locals 0

    return p0
.end method

.method public static final contains-mqrhnls(II)Z
    .locals 0

    .line 99
    invoke-static {p0, p1}, Lcom/squareup/mosaic/components/DividerEdge;->intersect-mqrhnls(II)I

    move-result p0

    invoke-static {p0, p1}, Lcom/squareup/mosaic/components/DividerEdge;->equals-impl0(II)Z

    move-result p0

    return p0
.end method

.method public static equals-impl(ILjava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lcom/squareup/mosaic/components/DividerEdge;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/components/DividerEdge;

    invoke-virtual {p1}, Lcom/squareup/mosaic/components/DividerEdge;->unbox-impl()I

    move-result p1

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    return p0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method

.method public static final equals-impl0(II)Z
    .locals 0

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static hashCode-impl(I)I
    .locals 0

    invoke-static {p0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result p0

    return p0
.end method

.method public static final intersect-mqrhnls(II)I
    .locals 0

    and-int/2addr p0, p1

    .line 93
    invoke-static {p0}, Lcom/squareup/mosaic/components/DividerEdge;->constructor-impl(I)I

    move-result p0

    return p0
.end method

.method public static final minus-mqrhnls(II)I
    .locals 0

    not-int p1, p1

    and-int/2addr p0, p1

    .line 90
    invoke-static {p0}, Lcom/squareup/mosaic/components/DividerEdge;->constructor-impl(I)I

    move-result p0

    return p0
.end method

.method public static final plus-mqrhnls(II)I
    .locals 0

    or-int/2addr p0, p1

    .line 87
    invoke-static {p0}, Lcom/squareup/mosaic/components/DividerEdge;->constructor-impl(I)I

    move-result p0

    return p0
.end method

.method public static toString-impl(I)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DividerEdge(value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ")"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/squareup/mosaic/components/DividerEdge;->value:I

    invoke-static {v0, p1}, Lcom/squareup/mosaic/components/DividerEdge;->equals-impl(ILjava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final getValue()I
    .locals 1

    .line 76
    iget v0, p0, Lcom/squareup/mosaic/components/DividerEdge;->value:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/squareup/mosaic/components/DividerEdge;->value:I

    invoke-static {v0}, Lcom/squareup/mosaic/components/DividerEdge;->hashCode-impl(I)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    iget v0, p0, Lcom/squareup/mosaic/components/DividerEdge;->value:I

    invoke-static {v0}, Lcom/squareup/mosaic/components/DividerEdge;->toString-impl(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic unbox-impl()I
    .locals 1

    iget v0, p0, Lcom/squareup/mosaic/components/DividerEdge;->value:I

    return v0
.end method
