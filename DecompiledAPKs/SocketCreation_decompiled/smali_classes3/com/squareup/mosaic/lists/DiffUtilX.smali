.class public final Lcom/squareup/mosaic/lists/DiffUtilX;
.super Ljava/lang/Object;
.source "DiffUtilX.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;,
        Lcom/squareup/mosaic/lists/DiffUtilX$Results;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDiffUtilX.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DiffUtilX.kt\ncom/squareup/mosaic/lists/DiffUtilX\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,161:1\n1313#2:162\n1382#2,3:163\n1600#2,3:166\n*E\n*S KotlinDebug\n*F\n+ 1 DiffUtilX.kt\ncom/squareup/mosaic/lists/DiffUtilX\n*L\n66#1:162\n66#1,3:163\n111#1,3:166\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u0004\"\u0004\u0008\u0000\u0010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\t\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/mosaic/lists/DiffUtilX;",
        "",
        "()V",
        "calculateDiff",
        "",
        "T",
        "comparator",
        "Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;",
        "resultsCallback",
        "Lcom/squareup/mosaic/lists/DiffUtilX$Results;",
        "Comparator",
        "Results",
        "mosaic-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/mosaic/lists/DiffUtilX;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/mosaic/lists/DiffUtilX;

    invoke-direct {v0}, Lcom/squareup/mosaic/lists/DiffUtilX;-><init>()V

    sput-object v0, Lcom/squareup/mosaic/lists/DiffUtilX;->INSTANCE:Lcom/squareup/mosaic/lists/DiffUtilX;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final calculateDiff(Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;Lcom/squareup/mosaic/lists/DiffUtilX$Results;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;",
            "Lcom/squareup/mosaic/lists/DiffUtilX$Results<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "comparator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resultsCallback"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$diffUtilComparator$1;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$diffUtilComparator$1;-><init>(Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;)V

    .line 50
    check-cast v0, Landroidx/recyclerview/widget/DiffUtil$Callback;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroidx/recyclerview/widget/DiffUtil;->calculateDiff(Landroidx/recyclerview/widget/DiffUtil$Callback;Z)Landroidx/recyclerview/widget/DiffUtil$DiffResult;

    move-result-object v0

    const-string v2, "DiffUtil.calculateDiff(diffUtilComparator, false)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-interface {p1}, Lcom/squareup/mosaic/lists/DiffUtilX$Comparator;->getOldListSize()I

    move-result p1

    invoke-static {v1, p1}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 162
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {p1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 163
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v3, p1

    check-cast v3, Lkotlin/collections/IntIterator;

    invoke-virtual {v3}, Lkotlin/collections/IntIterator;->nextInt()I

    .line 66
    new-instance v3, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct {v3, v1, v5, v4}, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;-><init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 165
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/util/Collection;

    .line 67
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    .line 69
    move-object v2, p1

    check-cast v2, Ljava/util/Collection;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    .line 70
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/List;

    .line 76
    new-instance v4, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$1;

    invoke-direct {v4, v3, p1}, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$1;-><init>(Ljava/util/List;Ljava/util/List;)V

    check-cast v4, Landroidx/recyclerview/widget/ListUpdateCallback;

    invoke-virtual {v0, v4}, Landroidx/recyclerview/widget/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroidx/recyclerview/widget/ListUpdateCallback;)V

    .line 111
    check-cast p1, Ljava/lang/Iterable;

    .line 167
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    add-int/lit8 v5, v1, 0x1

    if-gez v1, :cond_1

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_1
    check-cast v4, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;

    .line 111
    invoke-virtual {v4, v1}, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$SomeItem;->setFinalPos(I)V

    move v1, v5

    goto :goto_1

    .line 112
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 117
    new-instance v1, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$3;

    invoke-direct {v1, v2, p2, p1}, Lcom/squareup/mosaic/lists/DiffUtilX$calculateDiff$3;-><init>(Ljava/util/List;Lcom/squareup/mosaic/lists/DiffUtilX$Results;Ljava/util/Iterator;)V

    check-cast v1, Landroidx/recyclerview/widget/ListUpdateCallback;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/DiffUtil$DiffResult;->dispatchUpdatesTo(Landroidx/recyclerview/widget/ListUpdateCallback;)V

    return-void
.end method
