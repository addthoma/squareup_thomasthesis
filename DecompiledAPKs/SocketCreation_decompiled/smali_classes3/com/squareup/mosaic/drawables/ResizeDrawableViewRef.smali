.class public final Lcom/squareup/mosaic/drawables/ResizeDrawableViewRef;
.super Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;
.source "ResizeDrawableViewRef.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef<",
        "Lcom/squareup/mosaic/drawables/ResizeDrawableModel;",
        "Lcom/squareup/noho/ResizeDrawable;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010\u000c\u001a\u00020\u0002H\u0014J\u0018\u0010\r\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u0002H\u0016J\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u0002H\u0016R\u0016\u0010\u0007\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/mosaic/drawables/ResizeDrawableViewRef;",
        "Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;",
        "Lcom/squareup/mosaic/drawables/ResizeDrawableModel;",
        "Lcom/squareup/noho/ResizeDrawable;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "subDrawableRef",
        "Lcom/squareup/mosaic/core/DrawableRef;",
        "canUpdateTo",
        "",
        "currentModel",
        "newModel",
        "createDrawable",
        "model",
        "updateDrawable",
        "",
        "drawable",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private subDrawableRef:Lcom/squareup/mosaic/core/DrawableRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/core/DrawableRef<",
            "**>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0, p1}, Lcom/squareup/mosaic/drawables/DelayedDrawableViewRef;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic canUpdateTo(Lcom/squareup/mosaic/core/DrawableModel;Lcom/squareup/mosaic/core/DrawableModel;)Z
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;

    check-cast p2, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/drawables/ResizeDrawableViewRef;->canUpdateTo(Lcom/squareup/mosaic/drawables/ResizeDrawableModel;Lcom/squareup/mosaic/drawables/ResizeDrawableModel;)Z

    move-result p1

    return p1
.end method

.method protected canUpdateTo(Lcom/squareup/mosaic/drawables/ResizeDrawableModel;Lcom/squareup/mosaic/drawables/ResizeDrawableModel;)Z
    .locals 1

    const-string v0, "currentModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newModel"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic createDrawable(Landroid/content/Context;Lcom/squareup/mosaic/core/DrawableModel;)Landroid/graphics/drawable/Drawable;
    .locals 0

    .line 8
    check-cast p2, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/drawables/ResizeDrawableViewRef;->createDrawable(Landroid/content/Context;Lcom/squareup/mosaic/drawables/ResizeDrawableModel;)Lcom/squareup/noho/ResizeDrawable;

    move-result-object p1

    check-cast p1, Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method public createDrawable(Landroid/content/Context;Lcom/squareup/mosaic/drawables/ResizeDrawableModel;)Lcom/squareup/noho/ResizeDrawable;
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p2}, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->getSubModel$public_release()Lcom/squareup/mosaic/core/DrawableModel;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/mosaic/core/DrawableModelKt;->toDrawableRef(Lcom/squareup/mosaic/core/DrawableModel;Landroid/content/Context;)Lcom/squareup/mosaic/core/DrawableRef;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableViewRef;->subDrawableRef:Lcom/squareup/mosaic/core/DrawableRef;

    .line 24
    new-instance v0, Lcom/squareup/noho/ResizeDrawable;

    .line 25
    iget-object v1, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableViewRef;->subDrawableRef:Lcom/squareup/mosaic/core/DrawableRef;

    if-nez v1, :cond_1

    const-string v2, "subDrawableRef"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Lcom/squareup/mosaic/core/DrawableRef;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 26
    invoke-virtual {p2}, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->getEffectiveWidth$public_release()Lcom/squareup/resources/DimenModel;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/squareup/resources/DimenModel;->toSize(Landroid/content/Context;)I

    move-result v2

    .line 27
    invoke-virtual {p2}, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->getEffectiveHeight$public_release()Lcom/squareup/resources/DimenModel;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/squareup/resources/DimenModel;->toSize(Landroid/content/Context;)I

    move-result p1

    .line 24
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/noho/ResizeDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    return-object v0
.end method

.method public bridge synthetic updateDrawable(Landroid/graphics/drawable/Drawable;Lcom/squareup/mosaic/core/DrawableModel;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/noho/ResizeDrawable;

    check-cast p2, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/mosaic/drawables/ResizeDrawableViewRef;->updateDrawable(Lcom/squareup/noho/ResizeDrawable;Lcom/squareup/mosaic/drawables/ResizeDrawableModel;)V

    return-void
.end method

.method public updateDrawable(Lcom/squareup/noho/ResizeDrawable;Lcom/squareup/mosaic/drawables/ResizeDrawableModel;)V
    .locals 1

    const-string v0, "drawable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "model"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method
