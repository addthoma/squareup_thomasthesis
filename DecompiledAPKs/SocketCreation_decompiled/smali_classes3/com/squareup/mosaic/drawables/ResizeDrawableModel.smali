.class public final Lcom/squareup/mosaic/drawables/ResizeDrawableModel;
.super Lcom/squareup/mosaic/core/DrawableModel;
.source "ResizeDrawableModel.kt"

# interfaces
.implements Lcom/squareup/mosaic/core/DrawableModelContext;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nResizeDrawableModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ResizeDrawableModel.kt\ncom/squareup/mosaic/drawables/ResizeDrawableModel\n*L\n1#1,44:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0015\n\u0002\u0010\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u00012\u00020\u0002B7\u0008\u0001\u0012\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0001\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0001H\u0016J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0004H\u00c6\u0003J\u0010\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00c0\u0003\u00a2\u0006\u0002\u0008 J9\u0010!\u001a\u00020\u00002\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u00042\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\u0008\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00042\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0001H\u00c6\u0001J\u0018\u0010\"\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030#2\u0006\u0010$\u001a\u00020%H\u0016J\u0013\u0010&\u001a\u00020\'2\u0008\u0010(\u001a\u0004\u0018\u00010)H\u00d6\u0003J\t\u0010*\u001a\u00020+H\u00d6\u0001J\t\u0010,\u001a\u00020-H\u00d6\u0001R\u0014\u0010\t\u001a\u00020\u00048@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u000c\u001a\u00020\u00048@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000bR\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\u000b\"\u0004\u0008\u000f\u0010\u0010R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0011\u0010\u000b\"\u0004\u0008\u0012\u0010\u0010R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0001X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014\"\u0004\u0008\u0015\u0010\u0016R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0017\u0010\u000b\"\u0004\u0008\u0018\u0010\u0010\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/mosaic/drawables/ResizeDrawableModel;",
        "Lcom/squareup/mosaic/core/DrawableModel;",
        "Lcom/squareup/mosaic/core/DrawableModelContext;",
        "size",
        "Lcom/squareup/resources/DimenModel;",
        "width",
        "height",
        "subModel",
        "(Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/core/DrawableModel;)V",
        "effectiveHeight",
        "getEffectiveHeight$public_release",
        "()Lcom/squareup/resources/DimenModel;",
        "effectiveWidth",
        "getEffectiveWidth$public_release",
        "getHeight",
        "setHeight",
        "(Lcom/squareup/resources/DimenModel;)V",
        "getSize",
        "setSize",
        "getSubModel$public_release",
        "()Lcom/squareup/mosaic/core/DrawableModel;",
        "setSubModel$public_release",
        "(Lcom/squareup/mosaic/core/DrawableModel;)V",
        "getWidth",
        "setWidth",
        "add",
        "",
        "model",
        "component1",
        "component2",
        "component3",
        "component4",
        "component4$public_release",
        "copy",
        "createDrawableRef",
        "Lcom/squareup/mosaic/core/DrawableRef;",
        "context",
        "Landroid/content/Context;",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private height:Lcom/squareup/resources/DimenModel;

.field private size:Lcom/squareup/resources/DimenModel;

.field private subModel:Lcom/squareup/mosaic/core/DrawableModel;

.field private width:Lcom/squareup/resources/DimenModel;


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;-><init>(Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/core/DrawableModel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/core/DrawableModel;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/mosaic/core/DrawableModel;-><init>()V

    iput-object p1, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->size:Lcom/squareup/resources/DimenModel;

    iput-object p2, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->width:Lcom/squareup/resources/DimenModel;

    iput-object p3, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->height:Lcom/squareup/resources/DimenModel;

    iput-object p4, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->subModel:Lcom/squareup/mosaic/core/DrawableModel;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/core/DrawableModel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x1

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    .line 32
    move-object p1, v0

    check-cast p1, Lcom/squareup/resources/DimenModel;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    .line 33
    move-object p2, v0

    check-cast p2, Lcom/squareup/resources/DimenModel;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    .line 34
    move-object p3, v0

    check-cast p3, Lcom/squareup/resources/DimenModel;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    .line 35
    move-object p4, v0

    check-cast p4, Lcom/squareup/mosaic/core/DrawableModel;

    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;-><init>(Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/core/DrawableModel;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/mosaic/drawables/ResizeDrawableModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/core/DrawableModel;ILjava/lang/Object;)Lcom/squareup/mosaic/drawables/ResizeDrawableModel;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->size:Lcom/squareup/resources/DimenModel;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->width:Lcom/squareup/resources/DimenModel;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->height:Lcom/squareup/resources/DimenModel;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->subModel:Lcom/squareup/mosaic/core/DrawableModel;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->copy(Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/core/DrawableModel;)Lcom/squareup/mosaic/drawables/ResizeDrawableModel;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public add(Lcom/squareup/mosaic/core/DrawableModel;)V
    .locals 1

    const-string v0, "model"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iput-object p1, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->subModel:Lcom/squareup/mosaic/core/DrawableModel;

    return-void
.end method

.method public final component1()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->size:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final component2()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->width:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final component3()Lcom/squareup/resources/DimenModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->height:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final component4$public_release()Lcom/squareup/mosaic/core/DrawableModel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->subModel:Lcom/squareup/mosaic/core/DrawableModel;

    return-object v0
.end method

.method public final copy(Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/core/DrawableModel;)Lcom/squareup/mosaic/drawables/ResizeDrawableModel;
    .locals 1

    new-instance v0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;-><init>(Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/resources/DimenModel;Lcom/squareup/mosaic/core/DrawableModel;)V

    return-object v0
.end method

.method public createDrawableRef(Landroid/content/Context;)Lcom/squareup/mosaic/core/DrawableRef;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/squareup/mosaic/core/DrawableRef<",
            "**>;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    new-instance v0, Lcom/squareup/mosaic/drawables/ResizeDrawableViewRef;

    invoke-direct {v0, p1}, Lcom/squareup/mosaic/drawables/ResizeDrawableViewRef;-><init>(Landroid/content/Context;)V

    check-cast v0, Lcom/squareup/mosaic/core/DrawableRef;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;

    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->size:Lcom/squareup/resources/DimenModel;

    iget-object v1, p1, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->size:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->width:Lcom/squareup/resources/DimenModel;

    iget-object v1, p1, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->width:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->height:Lcom/squareup/resources/DimenModel;

    iget-object v1, p1, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->height:Lcom/squareup/resources/DimenModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->subModel:Lcom/squareup/mosaic/core/DrawableModel;

    iget-object p1, p1, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->subModel:Lcom/squareup/mosaic/core/DrawableModel;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEffectiveHeight$public_release()Lcom/squareup/resources/DimenModel;
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->height:Lcom/squareup/resources/DimenModel;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->size:Lcom/squareup/resources/DimenModel;

    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Set height or size."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getEffectiveWidth$public_release()Lcom/squareup/resources/DimenModel;
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->width:Lcom/squareup/resources/DimenModel;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->size:Lcom/squareup/resources/DimenModel;

    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Set width or size."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getHeight()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->height:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final getSize()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->size:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public final getSubModel$public_release()Lcom/squareup/mosaic/core/DrawableModel;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->subModel:Lcom/squareup/mosaic/core/DrawableModel;

    return-object v0
.end method

.method public final getWidth()Lcom/squareup/resources/DimenModel;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->width:Lcom/squareup/resources/DimenModel;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->size:Lcom/squareup/resources/DimenModel;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->width:Lcom/squareup/resources/DimenModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->height:Lcom/squareup/resources/DimenModel;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->subModel:Lcom/squareup/mosaic/core/DrawableModel;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public final setHeight(Lcom/squareup/resources/DimenModel;)V
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->height:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public final setSize(Lcom/squareup/resources/DimenModel;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->size:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public final setSubModel$public_release(Lcom/squareup/mosaic/core/DrawableModel;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->subModel:Lcom/squareup/mosaic/core/DrawableModel;

    return-void
.end method

.method public final setWidth(Lcom/squareup/resources/DimenModel;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->width:Lcom/squareup/resources/DimenModel;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ResizeDrawableModel(size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->size:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->width:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->height:Lcom/squareup/resources/DimenModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subModel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/mosaic/drawables/ResizeDrawableModel;->subModel:Lcom/squareup/mosaic/core/DrawableModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
