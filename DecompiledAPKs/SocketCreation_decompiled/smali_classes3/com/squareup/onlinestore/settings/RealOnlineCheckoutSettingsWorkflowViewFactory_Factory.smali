.class public final Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory_Factory;
.super Ljava/lang/Object;
.source "RealOnlineCheckoutSettingsWorkflowViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;",
            ">;)",
            "Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory_Factory;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;)Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory;

    invoke-direct {v0, p0}, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory;-><init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;

    invoke-static {v0}, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory_Factory;->newInstance(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2WorkflowViewFactory;)Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory_Factory;->get()Lcom/squareup/onlinestore/settings/RealOnlineCheckoutSettingsWorkflowViewFactory;

    move-result-object v0

    return-object v0
.end method
