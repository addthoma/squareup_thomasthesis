.class public final Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;
.super Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;
.source "OnlineCheckoutSettingsV2State.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateLinkState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0006\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0007\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\t\u0010\u0008\u001a\u00020\tH\u00d6\u0001J\u0013\u0010\n\u001a\u00020\u00032\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\tH\u00d6\u0001J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0019\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\tH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0005\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
        "isSosEnabled",
        "",
        "(Z)V",
        "()Z",
        "component1",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final isSosEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState$Creator;

    invoke-direct {v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState$Creator;-><init>()V

    sput-object v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;->isSosEnabled:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;ZILjava/lang/Object;)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-boolean p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;->isSosEnabled:Z

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;->copy(Z)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;->isSosEnabled:Z

    return v0
.end method

.method public final copy(Z)Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;
    .locals 1

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;-><init>(Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;

    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;->isSosEnabled:Z

    iget-boolean p1, p1, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;->isSosEnabled:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;->isSosEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final isSosEnabled()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;->isSosEnabled:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CreateLinkState(isSosEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;->isSosEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean p2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;->isSosEnabled:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
