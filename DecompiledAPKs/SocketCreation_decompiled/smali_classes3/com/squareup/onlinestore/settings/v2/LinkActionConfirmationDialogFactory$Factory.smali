.class public final Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$Factory;
.super Ljava/lang/Object;
.source "LinkActionConfirmationDialogFactory.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J$\u0010\u0003\u001a\u00020\u00042\u001c\u0010\u0005\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\t0\u0007j\u0002`\n0\u0006\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$Factory;",
        "",
        "()V",
        "create",
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData;",
        "",
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreen;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;

    invoke-direct {v0, p1}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;-><init>(Lio/reactivex/Observable;)V

    return-object v0
.end method
