.class public abstract Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;
.super Ljava/lang/Object;
.source "OnlineCheckoutSettingsV2State.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$InitialLoadingState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$LoadCheckoutLinkListState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$NewCheckoutLinkState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$EditLinkState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledConfirmationState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$DeleteCheckoutLinkConfirmationState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$DeleteCheckoutLinkState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ActionBottomSheetDialogState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$FeatureNotAvailableState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;,
        Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorKind;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u000f\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010\u0011B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u000e\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;",
        "Landroid/os/Parcelable;",
        "()V",
        "ActionBottomSheetDialogState",
        "CheckoutLinkListState",
        "CheckoutLinkState",
        "CreateLinkState",
        "DeleteCheckoutLinkConfirmationState",
        "DeleteCheckoutLinkState",
        "EditLinkState",
        "ErrorKind",
        "ErrorState",
        "FeatureNotAvailableState",
        "InitialLoadingState",
        "LoadCheckoutLinkListState",
        "NewCheckoutLinkState",
        "UpdateLinkEnabledConfirmationState",
        "UpdateLinkEnabledState",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$InitialLoadingState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$LoadCheckoutLinkListState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkListState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CheckoutLinkState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$NewCheckoutLinkState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$CreateLinkState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$EditLinkState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledConfirmationState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$UpdateLinkEnabledState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$DeleteCheckoutLinkConfirmationState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$DeleteCheckoutLinkState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ActionBottomSheetDialogState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$FeatureNotAvailableState;",
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State$ErrorState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2State;-><init>()V

    return-void
.end method
