.class public final Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2InternalWorkflowViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "OnlineCheckoutSettingsV2WorkflowViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2InternalWorkflowViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "checkoutLinkListLayoutRunnerFactory",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Factory;",
        "checkoutLinkScreenLayoutRunnerFactory",
        "Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Factory;",
        "actionOptionBottomSheetDialogFactory",
        "Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Factory;",
        "linkActionConfirmationDialogFactory",
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$Factory;",
        "(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Factory;Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Factory;Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Factory;Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Factory;Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Factory;Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Factory;Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$Factory;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "checkoutLinkListLayoutRunnerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutLinkScreenLayoutRunnerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionOptionBottomSheetDialogFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "linkActionConfirmationDialogFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 25
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Binding;

    invoke-direct {v1, p1}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Binding;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$Factory;)V

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p1, 0x0

    aput-object v1, v0, p1

    .line 26
    new-instance p1, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Binding;

    invoke-direct {p1, p2}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Binding;-><init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkScreenLayoutRunner$Factory;)V

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x1

    aput-object p1, v0, p2

    .line 27
    new-instance p1, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Binding;

    invoke-direct {p1, p3}, Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Binding;-><init>(Lcom/squareup/onlinestore/settings/v2/ActionOptionBottomSheetDialogFactory$Factory;)V

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x2

    aput-object p1, v0, p2

    .line 28
    new-instance p1, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$Binding;

    invoke-direct {p1, p4}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$Binding;-><init>(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$Factory;)V

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 p2, 0x3

    aput-object p1, v0, p2

    .line 24
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
