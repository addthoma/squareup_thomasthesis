.class final Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CheckoutLinkListScreenLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;->invoke(Lcom/squareup/cycler/Update;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2$1;->this$0:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 50
    invoke-virtual {p0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2$1;->this$0:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;

    iget-object v0, v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;->$screen:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getLoadingNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2$1;->this$0:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;

    iget-object v0, v0, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenLayoutRunner$showCheckoutLinksScreen$2;->$screen:Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;

    invoke-virtual {v0}, Lcom/squareup/onlinestore/settings/v2/CheckoutLinkListScreenData$CheckoutLinksScreenData;->getOnLoadNext()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    :cond_0
    return-void
.end method
