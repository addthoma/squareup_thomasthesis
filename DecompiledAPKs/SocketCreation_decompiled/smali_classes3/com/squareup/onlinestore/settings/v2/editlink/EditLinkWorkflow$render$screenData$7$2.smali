.class final Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7$2;
.super Lkotlin/jvm/internal/Lambda;
.source "EditLinkWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;->invoke(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$UpdatePayLinkResult;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;",
        "-",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7$2;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7$2;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;",
            "-",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$LinkAlreadyExistsErrorState;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7$2;->this$0:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;

    iget-object v1, v1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkWorkflow$render$screenData$7;->$state:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState;

    check-cast v1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$SaveLinkState;

    invoke-virtual {v1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$SaveLinkState;->getEditLinkInfo()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowState$LinkAlreadyExistsErrorState;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
