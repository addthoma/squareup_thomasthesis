.class public final Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;
.super Ljava/lang/Object;
.source "LinkActionConfirmationDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$Factory;,
        Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLinkActionConfirmationDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LinkActionConfirmationDialogFactory.kt\ncom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,137:1\n1103#2,7:138\n1103#2,7:145\n1103#2,7:152\n1103#2,7:159\n*E\n*S KotlinDebug\n*F\n+ 1 LinkActionConfirmationDialogFactory.kt\ncom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory\n*L\n74#1,7:138\n80#1,7:145\n111#1,7:152\n117#1,7:159\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0002\u0013\u0014B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0018\u0010\t\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0018\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0012H\u0002R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData;",
        "",
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreen;",
        "(Lio/reactivex/Observable;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "getDeactivateLinkConfirmationDialogScreen",
        "screenData",
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;",
        "getDeleteLinkConfirmationDialogScreen",
        "Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeleteLinkConfirmationDialogScreenData;",
        "Binding",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getDeactivateLinkConfirmationDialogScreen(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;Landroid/content/Context;Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;)Landroid/app/Dialog;
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;->getDeactivateLinkConfirmationDialogScreen(Landroid/content/Context;Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;)Landroid/app/Dialog;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDeleteLinkConfirmationDialogScreen(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;Landroid/content/Context;Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeleteLinkConfirmationDialogScreenData;)Landroid/app/Dialog;
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;->getDeleteLinkConfirmationDialogScreen(Landroid/content/Context;Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeleteLinkConfirmationDialogScreenData;)Landroid/app/Dialog;

    move-result-object p0

    return-object p0
.end method

.method private final getDeactivateLinkConfirmationDialogScreen(Landroid/content/Context;Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;)Landroid/app/Dialog;
    .locals 3

    .line 45
    sget v0, Lcom/squareup/onlinestore/settings/impl/R$layout;->online_checkout_settings_deactivate_confirmation_dialog:I

    const/4 v1, 0x0

    .line 44
    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view"

    .line 48
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v1, Lcom/squareup/onlinestore/settings/impl/R$id;->title:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 49
    check-cast v1, Lcom/squareup/noho/NohoLabel;

    .line 50
    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;->isLinkEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 51
    sget v2, Lcom/squareup/onlinestore/settings/impl/R$string;->deactivate_confirmation_dialog_title:I

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoLabel;->setText(I)V

    goto :goto_0

    .line 53
    :cond_0
    sget v2, Lcom/squareup/onlinestore/settings/impl/R$string;->activate_confirmation_dialog_title:I

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoLabel;->setText(I)V

    .line 57
    :goto_0
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$id;->body:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 58
    check-cast v1, Lcom/squareup/noho/NohoLabel;

    .line 59
    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;->isLinkEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 60
    sget v2, Lcom/squareup/onlinestore/settings/impl/R$string;->deactivate_confirmation_dialog_body:I

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoLabel;->setText(I)V

    goto :goto_1

    .line 62
    :cond_1
    sget v2, Lcom/squareup/onlinestore/settings/impl/R$string;->activate_confirmation_dialog_body:I

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoLabel;->setText(I)V

    .line 66
    :goto_1
    new-instance v1, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 67
    invoke-virtual {v1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    .line 68
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$getDeactivateLinkConfirmationDialogScreen$dialog$1;

    invoke-direct {v1, p2}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$getDeactivateLinkConfirmationDialogScreen$dialog$1;-><init>(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    .line 71
    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object p1

    const-string v1, "AlertDialog.Builder(cont\u2026      }\n        .create()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$id;->cancel_button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 138
    new-instance v2, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$getDeactivateLinkConfirmationDialogScreen$$inlined$onClickDebounced$1;

    invoke-direct {v2, p2, p1}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$getDeactivateLinkConfirmationDialogScreen$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;Landroidx/appcompat/app/AlertDialog;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$id;->deactivate_button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 80
    check-cast v0, Lcom/squareup/noho/NohoButton;

    .line 81
    invoke-virtual {p2}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;->isLinkEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 82
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->deactivate_link:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setText(I)V

    goto :goto_2

    .line 84
    :cond_2
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$string;->activate_link:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setText(I)V

    .line 86
    :goto_2
    check-cast v0, Landroid/view/View;

    .line 145
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$getDeactivateLinkConfirmationDialogScreen$$inlined$apply$lambda$1;

    invoke-direct {v1, p2, p1}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$getDeactivateLinkConfirmationDialogScreen$$inlined$apply$lambda$1;-><init>(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeactivateLinkConfirmationDialogScreenData;Landroidx/appcompat/app/AlertDialog;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method

.method private final getDeleteLinkConfirmationDialogScreen(Landroid/content/Context;Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeleteLinkConfirmationDialogScreenData;)Landroid/app/Dialog;
    .locals 3

    .line 100
    sget v0, Lcom/squareup/onlinestore/settings/impl/R$layout;->online_checkout_settings_delete_confirmation_dialog:I

    const/4 v1, 0x0

    .line 99
    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 103
    new-instance v1, Landroidx/appcompat/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroidx/appcompat/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 104
    invoke-virtual {v1, v0}, Landroidx/appcompat/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    .line 105
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$getDeleteLinkConfirmationDialogScreen$dialog$1;

    invoke-direct {v1, p2}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$getDeleteLinkConfirmationDialogScreen$dialog$1;-><init>(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeleteLinkConfirmationDialogScreenData;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, v1}, Landroidx/appcompat/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroidx/appcompat/app/AlertDialog$Builder;

    move-result-object p1

    .line 108
    invoke-virtual {p1}, Landroidx/appcompat/app/AlertDialog$Builder;->create()Landroidx/appcompat/app/AlertDialog;

    move-result-object p1

    const-string v1, "AlertDialog.Builder(cont\u2026      }\n        .create()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v1, "view"

    .line 110
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget v1, Lcom/squareup/onlinestore/settings/impl/R$id;->cancel_button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 152
    new-instance v2, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$getDeleteLinkConfirmationDialogScreen$$inlined$onClickDebounced$1;

    invoke-direct {v2, p2, p1}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$getDeleteLinkConfirmationDialogScreen$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeleteLinkConfirmationDialogScreenData;Landroidx/appcompat/app/AlertDialog;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    sget v1, Lcom/squareup/onlinestore/settings/impl/R$id;->delete_button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 159
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$getDeleteLinkConfirmationDialogScreen$$inlined$onClickDebounced$2;

    invoke-direct {v1, p2, p1}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$getDeleteLinkConfirmationDialogScreen$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogScreenData$DeleteLinkConfirmationDialogScreenData;Landroidx/appcompat/app/AlertDialog;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;->screens:Lio/reactivex/Observable;

    .line 26
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory$create$1;-><init>(Lcom/squareup/onlinestore/settings/v2/LinkActionConfirmationDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screens\n        .firstOr\u2026  }\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
