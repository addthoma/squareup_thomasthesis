.class public final Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;
.super Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;
.source "EditLinkFlowScreenData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EditLinkScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0008\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\u0015\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\u000f\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0008H\u00c6\u0003J9\u0010\u0013\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0014\u0008\u0002\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00060\u00052\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0008H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;",
        "editLinkInfo",
        "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
        "onClickSaveButton",
        "Lkotlin/Function1;",
        "",
        "onClickUpButton",
        "Lkotlin/Function0;",
        "(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V",
        "getEditLinkInfo",
        "()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
        "getOnClickSaveButton",
        "()Lkotlin/jvm/functions/Function1;",
        "getOnClickUpButton",
        "()Lkotlin/jvm/functions/Function0;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final editLinkInfo:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

.field private final onClickSaveButton:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onClickUpButton:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "editLinkInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickSaveButton"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickUpButton"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0, v0}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->editLinkInfo:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickSaveButton:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickUpButton:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->editLinkInfo:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickSaveButton:Lkotlin/jvm/functions/Function1;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickUpButton:Lkotlin/jvm/functions/Function0;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->copy(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->editLinkInfo:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickSaveButton:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component3()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickUpButton:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final copy(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;"
        }
    .end annotation

    const-string v0, "editLinkInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickSaveButton"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onClickUpButton"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;-><init>(Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->editLinkInfo:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    iget-object v1, p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->editLinkInfo:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickSaveButton:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickSaveButton:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickUpButton:Lkotlin/jvm/functions/Function0;

    iget-object p1, p1, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickUpButton:Lkotlin/jvm/functions/Function0;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEditLinkInfo()Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->editLinkInfo:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    return-object v0
.end method

.method public final getOnClickSaveButton()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 8
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickSaveButton:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnClickUpButton()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickUpButton:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->editLinkInfo:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickSaveButton:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickUpButton:Lkotlin/jvm/functions/Function0;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EditLinkScreenData(editLinkInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->editLinkInfo:Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onClickSaveButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickSaveButton:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onClickUpButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/editlink/EditLinkFlowScreenData$EditLinkScreenData;->onClickUpButton:Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
