.class final Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OnlineCheckoutSettingsV2Workflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->renderCheckoutLinkScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/onlinestore/settings/v2/CheckoutLink;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/content/Context;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/content/Context;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $checkoutLinkUrl:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$2;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    iput-object p2, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$2;->$checkoutLinkUrl:Ljava/lang/String;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 94
    check-cast p1, Landroid/content/Context;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$2;->invoke(Landroid/content/Context;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/content/Context;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 453
    iget-object v0, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$2;->this$0:Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;

    iget-object v1, p0, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow$renderCheckoutLinkScreen$2;->$checkoutLinkUrl:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;->access$onClickCheckoutLinkUrl(Lcom/squareup/onlinestore/settings/v2/OnlineCheckoutSettingsV2Workflow;Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method
