.class public final enum Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;
.super Ljava/lang/Enum;
.source "OnlineCheckoutSettingsEvents.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0014\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;",
        "",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getValue",
        "()Ljava/lang/String;",
        "ACTIVATE",
        "ACTIVATE_DONATION",
        "CHECKOUT_LINKS_ENABLE",
        "CHECKOUT_LINKS_ENABLE_FAILURE",
        "DEACTIVATE",
        "DEACTIVATE_DONATION",
        "DELETE_DONATION",
        "DELETE_PAY",
        "EDIT_LINK",
        "HOME",
        "SAVE_DONATION",
        "SAVE_PAY",
        "SHARE_DONATION",
        "SHARE_PAY",
        "UPDATE_DONATION",
        "UPDATE_PAY",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum ACTIVATE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum ACTIVATE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum CHECKOUT_LINKS_ENABLE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum CHECKOUT_LINKS_ENABLE_FAILURE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum DEACTIVATE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum DEACTIVATE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum DELETE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum DELETE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum EDIT_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum HOME:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum SAVE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum SAVE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum SHARE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum SHARE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum UPDATE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

.field public static final enum UPDATE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/4 v2, 0x0

    const-string v3, "ACTIVATE"

    const-string v4, "SPOS Settings: Online Checkout: Home: Activate"

    .line 9
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->ACTIVATE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/4 v2, 0x1

    const-string v3, "ACTIVATE_DONATION"

    const-string v4, "SPOS Settings: Online Checkout: Home: Activate Donation"

    .line 10
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->ACTIVATE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/4 v2, 0x2

    const-string v3, "CHECKOUT_LINKS_ENABLE"

    const-string v4, "SPOS Settings: Checkout Links: Enable"

    .line 11
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->CHECKOUT_LINKS_ENABLE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/4 v2, 0x3

    const-string v3, "CHECKOUT_LINKS_ENABLE_FAILURE"

    const-string v4, "SPOS Settings: Checkout Links: Enable: Failure"

    .line 12
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->CHECKOUT_LINKS_ENABLE_FAILURE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/4 v2, 0x4

    const-string v3, "DEACTIVATE"

    const-string v4, "SPOS Settings: Online Checkout: Home: Deactivate"

    .line 13
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->DEACTIVATE:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/4 v2, 0x5

    const-string v3, "DEACTIVATE_DONATION"

    const-string v4, "SPOS Settings: Online Checkout: Home: Deactivate Donation"

    .line 14
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->DEACTIVATE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/4 v2, 0x6

    const-string v3, "DELETE_DONATION"

    const-string v4, "SPOS Settings: Online Checkout: Home: Delete Donation"

    .line 15
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->DELETE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/4 v2, 0x7

    const-string v3, "DELETE_PAY"

    const-string v4, "SPOS Settings: Online Checkout: Home: Delete"

    .line 16
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->DELETE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/16 v2, 0x8

    const-string v3, "EDIT_LINK"

    const-string v4, "SPOS Settings: Online Checkout: Home: Edit Link"

    .line 17
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->EDIT_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/16 v2, 0x9

    const-string v3, "HOME"

    const-string v4, "SPOS Settings: Online Checkout: Home"

    .line 18
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->HOME:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/16 v2, 0xa

    const-string v3, "SAVE_DONATION"

    const-string v4, "SPOS Settings: Online Checkout: Home: Save Donation"

    .line 19
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->SAVE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/16 v2, 0xb

    const-string v3, "SAVE_PAY"

    const-string v4, "SPOS Settings: Online Checkout: Home: Save Pay"

    .line 20
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->SAVE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/16 v2, 0xc

    const-string v3, "SHARE_DONATION"

    const-string v4, "SPOS Settings: Online Checkout: Home: Share Donation"

    .line 21
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->SHARE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/16 v2, 0xd

    const-string v3, "SHARE_PAY"

    const-string v4, "SPOS Settings: Online Checkout: Home: Share Pay"

    .line 22
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->SHARE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/16 v2, 0xe

    const-string v3, "UPDATE_DONATION"

    const-string v4, "SPOS Settings: Online Checkout: Home: Update Donation"

    .line 23
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->UPDATE_DONATION:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const-string v2, "UPDATE_PAY"

    const/16 v3, 0xf

    const-string v4, "SPOS Settings: Online Checkout: Home: Update Pay"

    .line 24
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->UPDATE_PAY:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->$VALUES:[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;
    .locals 1

    const-class v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;
    .locals 1

    sget-object v0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->$VALUES:[Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    invoke-virtual {v0}, [Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;

    return-object v0
.end method


# virtual methods
.method public final getValue()Ljava/lang/String;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutSettingsTapEventName;->value:Ljava/lang/String;

    return-object v0
.end method
