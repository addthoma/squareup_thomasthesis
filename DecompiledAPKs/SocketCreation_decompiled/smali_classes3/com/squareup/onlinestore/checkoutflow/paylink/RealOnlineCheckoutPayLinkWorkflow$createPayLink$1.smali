.class final Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1;
.super Ljava/lang/Object;
.source "RealOnlineCheckoutPayLinkWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->createPayLink(Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult;",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $amount:Lcom/squareup/protos/common/Money;

.field final synthetic $name:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;

    iput-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1;->$name:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1;->$amount:Lcom/squareup/protos/common/Money;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    instance-of v0, p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;

    invoke-static {v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->access$getCheckoutLinksRepository$p(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;)Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;

    move-result-object v0

    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$Success;->getJsonWebToken()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1;->$name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1;->$amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, p1, v1, v2}, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;->createPayLink(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lio/reactivex/Single;

    move-result-object p1

    .line 262
    sget-object v0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1$1;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "checkoutLinksRepository.\u2026    }\n                  }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 269
    :cond_0
    sget-object v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$UnableToEnableSos;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$UnableToEnableSos;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 270
    sget-object p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult$NotAvailable;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult$NotAvailable;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(NotAvailable)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 271
    :cond_1
    sget-object v0, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult$OtherError;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 272
    sget-object p1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult$OtherError;->INSTANCE:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$PayLinkWorkerResult$OtherError;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.just(OtherError)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 72
    check-cast p1, Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;

    invoke-virtual {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$createPayLink$1;->apply(Lcom/squareup/onlinestore/repository/CheckoutLinksRepository$SquareSyncJwtTokenResult;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
