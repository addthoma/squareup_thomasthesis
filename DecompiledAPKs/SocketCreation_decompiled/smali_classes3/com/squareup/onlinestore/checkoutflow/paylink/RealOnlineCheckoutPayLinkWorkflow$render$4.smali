.class final Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$4;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOnlineCheckoutPayLinkWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic $state:Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$4;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$4;->$state:Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 72
    invoke-virtual {p0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$4;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 164
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$4;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$SetName;

    iget-object v2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$render$4;->$state:Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState;

    check-cast v2, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;

    invoke-virtual {v2}, Lcom/squareup/onlinestore/checkoutflow/paylink/OnlineCheckoutPayLinkState$LoadingState;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$Action$SetName;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
