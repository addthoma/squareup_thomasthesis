.class public final Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$logViewEventWorker$1;
.super Lcom/squareup/workflow/LifecycleWorker;
.source "RealOnlineCheckoutPayLinkWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;-><init>(Lcom/squareup/util/Res;Lcom/squareup/http/Server;Lcom/squareup/checkoutlink/sharesheet/CheckoutLinkShareSheet;Lcom/squareup/util/Clipboard;Lcom/squareup/text/Formatter;Lcom/squareup/util/ToastFactory;Lcom/squareup/qrcodegenerator/QrCodeGenerator;Lcom/squareup/onlinestore/repository/CheckoutLinksRepository;Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"
    }
    d2 = {
        "com/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$logViewEventWorker$1",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "onStarted",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 89
    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$logViewEventWorker$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;

    invoke-direct {p0}, Lcom/squareup/workflow/LifecycleWorker;-><init>()V

    return-void
.end method


# virtual methods
.method public onStarted()V
    .locals 3

    .line 91
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow$logViewEventWorker$1;->this$0:Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;

    invoke-static {v0}, Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;->access$getAnalytics$p(Lcom/squareup/onlinestore/checkoutflow/paylink/RealOnlineCheckoutPayLinkWorkflow;)Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkViewEvent;

    sget-object v2, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkViewEventName;->VIEW_SHARE_PAY_LINK:Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkViewEventName;

    invoke-direct {v1, v2}, Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkViewEvent;-><init>(Lcom/squareup/onlinestore/analytics/events/OnlineCheckoutPayLinkViewEventName;)V

    check-cast v1, Lcom/squareup/onlinestore/analytics/OnlineStoreViewEvent;

    invoke-interface {v0, v1}, Lcom/squareup/onlinestore/analytics/OnlineStoreAnalytics;->logView(Lcom/squareup/onlinestore/analytics/OnlineStoreViewEvent;)V

    return-void
.end method
