.class public final Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;
.super Ljava/lang/Object;
.source "CreatePayLinkScreenLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCreatePayLinkScreenLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CreatePayLinkScreenLayoutRunner.kt\ncom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,107:1\n1103#2,7:108\n*E\n*S KotlinDebug\n*F\n+ 1 CreatePayLinkScreenLayoutRunner.kt\ncom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner\n*L\n66#1,7:108\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00192\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0016\u0010\u000e\u001a\u00020\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u0011H\u0002J\u0012\u0010\u0012\u001a\u00020\u000f2\u0008\u0008\u0001\u0010\u0013\u001a\u00020\u0014H\u0002J\u0018\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u0018H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "errorMsg",
        "Lcom/squareup/noho/NohoLabel;",
        "getLinkButton",
        "payLinkAmountLabel",
        "payLinkName",
        "Lcom/squareup/noho/NohoEditRow;",
        "setupActionBar",
        "",
        "onClickUpButton",
        "Lkotlin/Function0;",
        "showNameError",
        "msg",
        "",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final errorMsg:Lcom/squareup/noho/NohoLabel;

.field private final getLinkButton:Landroid/view/View;

.field private final payLinkAmountLabel:Lcom/squareup/noho/NohoLabel;

.field private final payLinkName:Lcom/squareup/noho/NohoEditRow;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->Companion:Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->view:Landroid/view/View;

    .line 31
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 33
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->pay_link_amount_label:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->payLinkAmountLabel:Lcom/squareup/noho/NohoLabel;

    .line 34
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->pay_link_name:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->payLinkName:Lcom/squareup/noho/NohoEditRow;

    .line 35
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->pay_link_get_link_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->getLinkButton:Landroid/view/View;

    .line 36
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$id;->error_msg:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->errorMsg:Lcom/squareup/noho/NohoLabel;

    return-void
.end method

.method public static final synthetic access$getPayLinkName$p(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;)Lcom/squareup/noho/NohoEditRow;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->payLinkName:Lcom/squareup/noho/NohoEditRow;

    return-object p0
.end method

.method public static final synthetic access$showNameError(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;I)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->showNameError(I)V

    return-void
.end method

.method private final setupActionBar(Lkotlin/jvm/functions/Function0;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 91
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 93
    new-instance v2, Lcom/squareup/resources/ResourceString;

    sget v3, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_title:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 94
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 96
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final showNameError(I)V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->payLinkName:Lcom/squareup/noho/NohoEditRow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 86
    iget-object v0, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->errorMsg:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(I)V

    .line 87
    iget-object p1, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->errorMsg:Lcom/squareup/noho/NohoLabel;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;->getOnClickUpButton()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 43
    new-instance p2, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;)V

    check-cast p2, Lkotlin/jvm/functions/Function0;

    invoke-direct {p0, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->setupActionBar(Lkotlin/jvm/functions/Function0;)V

    .line 49
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->payLinkName:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;->getName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoEditRow;->setText(Ljava/lang/CharSequence;)V

    .line 50
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;->getName()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 51
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->payLinkName:Lcom/squareup/noho/NohoEditRow;

    invoke-static {p2}, Lcom/squareup/onlinestore/ui/util/NohoEditRowUtilKt;->moveCursorPosToEnd(Lcom/squareup/noho/NohoEditRow;)V

    .line 54
    :cond_0
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->payLinkName:Lcom/squareup/noho/NohoEditRow;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;->getShowAlreadyExistsError()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoEditRow;->setError(Z)V

    .line 55
    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;->getShowAlreadyExistsError()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 56
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->errorMsg:Lcom/squareup/noho/NohoLabel;

    sget v0, Lcom/squareup/onlinestore/checkoutflow/impl/R$string;->online_checkout_pay_link_already_exists_error_msg:I

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoLabel;->setText(I)V

    .line 57
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->errorMsg:Lcom/squareup/noho/NohoLabel;

    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 59
    :cond_1
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->errorMsg:Lcom/squareup/noho/NohoLabel;

    check-cast p2, Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 63
    :goto_0
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->payLinkName:Lcom/squareup/noho/NohoEditRow;

    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$2;

    invoke-direct {v0, p0}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$2;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoEditRow;->post(Ljava/lang/Runnable;)Z

    .line 65
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->payLinkAmountLabel:Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;->getAmount()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object p2, p0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->getLinkButton:Landroid/view/View;

    .line 108
    new-instance v0, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenLayoutRunner;->showRendering(Lcom/squareup/onlinestore/checkoutflow/paylink/CreatePayLinkScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
