.class public final Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;
.super Lcom/squareup/disputes/SummaryAdapter$FormData;
.source "SummaryAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/SummaryAdapter$FormData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TextAnswer"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;",
        "Lcom/squareup/disputes/SummaryAdapter$FormData;",
        "answer",
        "Lcom/squareup/util/ViewString;",
        "(Lcom/squareup/util/ViewString;)V",
        "getAnswer",
        "()Lcom/squareup/util/ViewString;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final answer:Lcom/squareup/util/ViewString;


# direct methods
.method public constructor <init>(Lcom/squareup/util/ViewString;)V
    .locals 2

    const-string v0, "answer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    sget-object v0, Lcom/squareup/disputes/SummaryAdapter$ViewType;->TYPE_TEXT_ANSWER:Lcom/squareup/disputes/SummaryAdapter$ViewType;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/disputes/SummaryAdapter$FormData;-><init>(Lcom/squareup/disputes/SummaryAdapter$ViewType;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;->answer:Lcom/squareup/util/ViewString;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;Lcom/squareup/util/ViewString;ILjava/lang/Object;)Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;->answer:Lcom/squareup/util/ViewString;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;->copy(Lcom/squareup/util/ViewString;)Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/util/ViewString;
    .locals 1

    iget-object v0, p0, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;->answer:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public final copy(Lcom/squareup/util/ViewString;)Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;
    .locals 1

    const-string v0, "answer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;

    invoke-direct {v0, p1}, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;-><init>(Lcom/squareup/util/ViewString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;

    iget-object v0, p0, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;->answer:Lcom/squareup/util/ViewString;

    iget-object p1, p1, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;->answer:Lcom/squareup/util/ViewString;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAnswer()Lcom/squareup/util/ViewString;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;->answer:Lcom/squareup/util/ViewString;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;->answer:Lcom/squareup/util/ViewString;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TextAnswer(answer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/disputes/SummaryAdapter$FormData$TextAnswer;->answer:Lcom/squareup/util/ViewString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
