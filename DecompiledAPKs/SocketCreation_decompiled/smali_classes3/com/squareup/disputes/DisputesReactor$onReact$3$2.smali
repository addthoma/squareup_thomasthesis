.class final Lcom/squareup/disputes/DisputesReactor$onReact$3$2;
.super Lkotlin/jvm/internal/Lambda;
.source "DisputesReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/DisputesReactor$onReact$3;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;",
        "it",
        "Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/disputes/DisputesReactor$onReact$3;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/DisputesReactor$onReact$3;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$3$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$3;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    iget-object p1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$3$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$3;

    iget-object p1, p1, Lcom/squareup/disputes/DisputesReactor$onReact$3;->this$0:Lcom/squareup/disputes/DisputesReactor;

    invoke-static {p1}, Lcom/squareup/disputes/DisputesReactor;->access$getAnalytics$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/disputes/DisputesAnalyticsKt;->learnedMore(Lcom/squareup/analytics/Analytics;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$3$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$3;

    iget-object p1, p1, Lcom/squareup/disputes/DisputesReactor$onReact$3;->this$0:Lcom/squareup/disputes/DisputesReactor;

    invoke-static {p1}, Lcom/squareup/disputes/DisputesReactor;->access$getBrowserLauncher$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/util/BrowserLauncher;

    move-result-object p1

    sget v0, Lcom/squareup/disputes/R$string;->disputes_learn_more_url:I

    invoke-interface {p1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(I)V

    .line 98
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    iget-object v0, p0, Lcom/squareup/disputes/DisputesReactor$onReact$3$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$3;

    iget-object v0, v0, Lcom/squareup/disputes/DisputesReactor$onReact$3;->$state:Lcom/squareup/disputes/DisputesState;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$3$2;->invoke(Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
