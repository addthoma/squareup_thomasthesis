.class final Lcom/squareup/disputes/DisputesReactor$onReact$5$2;
.super Lkotlin/jvm/internal/Lambda;
.source "DisputesReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/disputes/DisputesReactor$onReact$5;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;",
        "it",
        "Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/disputes/DisputesReactor$onReact$5;


# direct methods
.method constructor <init>(Lcom/squareup/disputes/DisputesReactor$onReact$5;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$5$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$5;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    iget-object p1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$5$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$5;

    iget-object p1, p1, Lcom/squareup/disputes/DisputesReactor$onReact$5;->this$0:Lcom/squareup/disputes/DisputesReactor;

    invoke-static {p1}, Lcom/squareup/disputes/DisputesReactor;->access$getAnalytics$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/disputes/DisputesAnalyticsKt;->learnedMore(Lcom/squareup/analytics/Analytics;)V

    .line 135
    iget-object p1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$5$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$5;

    iget-object p1, p1, Lcom/squareup/disputes/DisputesReactor$onReact$5;->this$0:Lcom/squareup/disputes/DisputesReactor;

    invoke-static {p1}, Lcom/squareup/disputes/DisputesReactor;->access$getBrowserLauncher$p(Lcom/squareup/disputes/DisputesReactor;)Lcom/squareup/util/BrowserLauncher;

    move-result-object p1

    sget v0, Lcom/squareup/disputes/R$string;->disputes_learn_more_url:I

    invoke-interface {p1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(I)V

    .line 137
    new-instance p1, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v0, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    iget-object v1, p0, Lcom/squareup/disputes/DisputesReactor$onReact$5$2;->this$0:Lcom/squareup/disputes/DisputesReactor$onReact$5;

    iget-object v1, v1, Lcom/squareup/disputes/DisputesReactor$onReact$5;->$state:Lcom/squareup/disputes/DisputesState;

    check-cast v1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;

    invoke-virtual {v1}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;->getLastResponse()Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;-><init>(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;

    invoke-virtual {p0, p1}, Lcom/squareup/disputes/DisputesReactor$onReact$5$2;->invoke(Lcom/squareup/disputes/AllDisputesEvent$LearnMoreClicked;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
