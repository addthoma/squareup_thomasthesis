.class public final Lcom/squareup/disputes/DisputesReactor_Factory;
.super Ljava/lang/Object;
.source "DisputesReactor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/disputes/DisputesReactor;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final disputesLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final handlesDisputesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;"
        }
    .end annotation
.end field

.field private final serverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/disputes/DisputesReactor_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/disputes/DisputesReactor_Factory;->serverProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/disputes/DisputesReactor_Factory;->disputesLoaderProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/disputes/DisputesReactor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/disputes/DisputesReactor_Factory;->handlesDisputesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/disputes/DisputesReactor_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/http/Server;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/api/HandlesDisputes;",
            ">;)",
            "Lcom/squareup/disputes/DisputesReactor_Factory;"
        }
    .end annotation

    .line 48
    new-instance v6, Lcom/squareup/disputes/DisputesReactor_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/disputes/DisputesReactor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/http/Server;Lcom/squareup/disputes/DisputesLoader;Lcom/squareup/analytics/Analytics;Lcom/squareup/disputes/api/HandlesDisputes;)Lcom/squareup/disputes/DisputesReactor;
    .locals 7

    .line 53
    new-instance v6, Lcom/squareup/disputes/DisputesReactor;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/disputes/DisputesReactor;-><init>(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/http/Server;Lcom/squareup/disputes/DisputesLoader;Lcom/squareup/analytics/Analytics;Lcom/squareup/disputes/api/HandlesDisputes;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/disputes/DisputesReactor;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/disputes/DisputesReactor_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/BrowserLauncher;

    iget-object v1, p0, Lcom/squareup/disputes/DisputesReactor_Factory;->serverProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/http/Server;

    iget-object v2, p0, Lcom/squareup/disputes/DisputesReactor_Factory;->disputesLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/disputes/DisputesLoader;

    iget-object v3, p0, Lcom/squareup/disputes/DisputesReactor_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v4, p0, Lcom/squareup/disputes/DisputesReactor_Factory;->handlesDisputesProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/disputes/api/HandlesDisputes;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/disputes/DisputesReactor_Factory;->newInstance(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/http/Server;Lcom/squareup/disputes/DisputesLoader;Lcom/squareup/analytics/Analytics;Lcom/squareup/disputes/api/HandlesDisputes;)Lcom/squareup/disputes/DisputesReactor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/disputes/DisputesReactor_Factory;->get()Lcom/squareup/disputes/DisputesReactor;

    move-result-object v0

    return-object v0
.end method
