.class public final Lcom/squareup/disputes/AllDisputesAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "AllDisputesAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/disputes/AllDisputesAdapter$ViewType;,
        Lcom/squareup/disputes/AllDisputesAdapter$Data;,
        Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;,
        Lcom/squareup/disputes/AllDisputesAdapter$HeaderViewHolder;,
        Lcom/squareup/disputes/AllDisputesAdapter$SummaryViewHolder;,
        Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;,
        Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder<",
        "+",
        "Lcom/squareup/disputes/AllDisputesAdapter$Data;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAllDisputesAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AllDisputesAdapter.kt\ncom/squareup/disputes/AllDisputesAdapter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,336:1\n1429#2,3:337\n1429#2,3:340\n*E\n*S KotlinDebug\n*F\n+ 1 AllDisputesAdapter.kt\ncom/squareup/disputes/AllDisputesAdapter\n*L\n119#1,3:337\n182#1,3:340\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\u0008\u0000\u0018\u00002\u0010\u0012\u000c\u0012\n\u0012\u0006\u0008\u0001\u0012\u00020\u00030\u00020\u0001:\u0007789:;<=B7\u0008\u0007\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\u0008\u0012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\u0010\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0008\u0010\u001d\u001a\u00020\u001cH\u0016J\u0010\u0010\u001e\u001a\u00020\u001c2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J$\u0010\u001f\u001a\u00020\u00172\u0006\u0010 \u001a\u00020\u00192\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0010\u001a\u00020\u0011J\u0010\u0010!\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\u0010\u0010\"\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J \u0010#\u001a\u00020\u00172\u000e\u0010$\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00030\u00022\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J \u0010%\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00030\u00022\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020\u001cH\u0016J$\u0010)\u001a\u00020\u00172\u000c\u0010*\u001a\u0008\u0012\u0004\u0012\u00020,0+2\u0006\u0010-\u001a\u00020\u001c2\u0006\u0010.\u001a\u00020/J\u000c\u00100\u001a\u000201*\u00020\u0006H\u0002J\u0016\u00100\u001a\u000201*\u0002022\u0008\u0008\u0001\u00103\u001a\u00020\u001cH\u0002J\u0016\u00100\u001a\u000201*\u0002042\u0008\u0008\u0001\u00103\u001a\u00020\u001cH\u0002J\u000c\u00105\u001a\u000201*\u000204H\u0002J\u000c\u00106\u001a\u000201*\u000202H\u0002J\u000c\u00106\u001a\u000201*\u000204H\u0002R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006>"
    }
    d2 = {
        "Lcom/squareup/disputes/AllDisputesAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;",
        "Lcom/squareup/disputes/AllDisputesAdapter$Data;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "mediumDateFormat",
        "Ljava/text/DateFormat;",
        "mediumDateNoYearFormat",
        "locale",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljavax/inject/Provider;)V",
        "baseRows",
        "",
        "context",
        "Landroid/content/Context;",
        "resolvedRows",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/disputes/AllDisputesEvent;",
        "addSummary",
        "",
        "screenData",
        "Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;",
        "getData",
        "position",
        "",
        "getItemCount",
        "getItemViewType",
        "init",
        "data",
        "maybeAddClosed",
        "maybeAddOpen",
        "onBindViewHolder",
        "holder",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "updateResolved",
        "resolvedDisputes",
        "",
        "Lcom/squareup/protos/client/cbms/DisputedPayment;",
        "nextCursor",
        "moreType",
        "Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;",
        "format",
        "",
        "Lcom/squareup/protos/common/time/DateTime;",
        "phraseId",
        "Lcom/squareup/protos/common/time/YearMonthDay;",
        "formatMedium",
        "formatMediumNoYear",
        "BaseViewHolder",
        "Data",
        "DisputeViewHolder",
        "HeaderViewHolder",
        "LoadMoreViewHolder",
        "SummaryViewHolder",
        "ViewType",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final baseRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/disputes/AllDisputesAdapter$Data;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private final locale:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mediumDateFormat:Ljava/text/DateFormat;

.field private final mediumDateNoYearFormat:Ljava/text/DateFormat;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final resolvedRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/disputes/AllDisputesAdapter$Data;",
            ">;"
        }
    .end annotation
.end field

.field private workflow:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljavax/inject/Provider;)V
    .locals 1
    .param p2    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .param p3    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumFormNoYear;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediumDateFormat"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediumDateNoYearFormat"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/AllDisputesAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/disputes/AllDisputesAdapter;->mediumDateFormat:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/disputes/AllDisputesAdapter;->mediumDateNoYearFormat:Ljava/text/DateFormat;

    iput-object p4, p0, Lcom/squareup/disputes/AllDisputesAdapter;->locale:Ljavax/inject/Provider;

    .line 69
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/disputes/AllDisputesAdapter;->baseRows:Ljava/util/List;

    .line 70
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/disputes/AllDisputesAdapter;->resolvedRows:Ljava/util/List;

    return-void
.end method

.method private final addSummary(Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;)V
    .locals 5

    .line 102
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->baseRows:Ljava/util/List;

    .line 103
    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;

    .line 104
    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getCount()J

    move-result-wide v2

    .line 105
    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getDisputed()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/squareup/disputes/AllDisputesAdapter;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 106
    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getProtection()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 103
    invoke-direct {v1, v2, v3, v4, p1}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Summary;-><init>(JLjava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 102
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private final format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 1

    .line 309
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "moneyFormatter.format(this)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final format(Lcom/squareup/protos/common/time/DateTime;I)Ljava/lang/CharSequence;
    .locals 2

    .line 329
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    const-string v1, "context"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 330
    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter;->formatMediumNoYear(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "date"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 331
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "Phrase.from(context, phr\u2026Year())\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final format(Lcom/squareup/protos/common/time/YearMonthDay;I)Ljava/lang/CharSequence;
    .locals 2

    .line 323
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->context:Landroid/content/Context;

    if-nez v0, :cond_0

    const-string v1, "context"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 324
    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter;->formatMediumNoYear(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v0, "date"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 325
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "Phrase.from(context, phr\u2026Year())\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final formatMedium(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;
    .locals 1

    .line 312
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->mediumDateFormat:Ljava/text/DateFormat;

    invoke-static {p1}, Lcom/squareup/disputes/AllDisputesAdapterKt;->asDate(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "mediumDateFormat.format(asDate())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method private final formatMediumNoYear(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/CharSequence;
    .locals 2

    .line 320
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->mediumDateNoYearFormat:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/disputes/AllDisputesAdapter;->locale:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-static {p1, v1}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "mediumDateNoYearFormat.f\u2026Date(this, locale.get()))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method private final formatMediumNoYear(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;
    .locals 1

    .line 316
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->mediumDateNoYearFormat:Ljava/text/DateFormat;

    invoke-static {p1}, Lcom/squareup/disputes/AllDisputesAdapterKt;->asDate(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "mediumDateNoYearFormat.format(asDate())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method private final getData(I)Lcom/squareup/disputes/AllDisputesAdapter$Data;
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->baseRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 95
    iget-object v1, p0, Lcom/squareup/disputes/AllDisputesAdapter;->resolvedRows:Ljava/util/List;

    sub-int/2addr p1, v0

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/disputes/AllDisputesAdapter$Data;

    goto :goto_0

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->baseRows:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/disputes/AllDisputesAdapter$Data;

    :goto_0
    return-object p1
.end method

.method private final maybeAddClosed(Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;)V
    .locals 3

    .line 168
    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getResolvedDisputes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->baseRows:Ljava/util/List;

    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Header;

    sget v2, Lcom/squareup/disputes/R$string;->uppercase_closed_disputes:I

    invoke-direct {v1, v2}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Header;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getResolvedDisputes()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getNextCursor()I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getMoreType()Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    move-result-object p1

    invoke-virtual {p0, v0, v1, p1}, Lcom/squareup/disputes/AllDisputesAdapter;->updateResolved(Ljava/util/List;ILcom/squareup/disputes/AllDisputes$ScreenData$MoreType;)V

    return-void
.end method

.method private final maybeAddOpen(Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;)V
    .locals 8

    .line 114
    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getUnresolvedDisputes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->baseRows:Ljava/util/List;

    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Header;

    sget v2, Lcom/squareup/disputes/R$string;->uppercase_open_disputes:I

    invoke-direct {v1, v2}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Header;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;->getUnresolvedDisputes()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 119
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->baseRows:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    .line 337
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 338
    move-object v7, v1

    check-cast v7, Lcom/squareup/protos/client/cbms/DisputedPayment;

    .line 120
    iget-object v1, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    if-eqz v1, :cond_3

    sget-object v2, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/ActionableStatus;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    const-string v3, "it.latest_reporting_date"

    const-string v4, "it.current_disputed_amount"

    if-eq v1, v2, :cond_2

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_3

    .line 136
    :cond_1
    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;

    .line 137
    iget-object v2, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/disputes/AllDisputesAdapter;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 138
    iget-object v2, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/disputes/AllDisputesAdapter;->formatMedium(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 139
    iget-object v2, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    const-string v4, "it.expected_resolution_date"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    sget v4, Lcom/squareup/disputes/R$string;->dispute_list_pending:I

    .line 139
    invoke-direct {p0, v2, v4}, Lcom/squareup/disputes/AllDisputesAdapter;->format(Lcom/squareup/protos/common/time/YearMonthDay;I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 142
    sget v5, Lcom/squareup/disputes/R$style;->TextAppearance_Disputes_Description_Pending:I

    move-object v2, v1

    .line 136
    invoke-direct/range {v2 .. v7}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    check-cast v1, Lcom/squareup/disputes/AllDisputesAdapter$Data;

    goto :goto_1

    .line 122
    :cond_2
    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;

    .line 123
    iget-object v2, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/disputes/AllDisputesAdapter;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 124
    iget-object v2, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/disputes/AllDisputesAdapter;->formatMedium(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 125
    iget-object v2, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->information_request:Ljava/util/List;

    const-string v4, "it.information_request"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/cbms/InformationRequest;

    iget-object v2, v2, Lcom/squareup/protos/client/cbms/InformationRequest;->customer_due_at:Lcom/squareup/protos/common/time/DateTime;

    const-string v4, "it.information_request.first().customer_due_at"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    sget v4, Lcom/squareup/disputes/R$string;->dispute_list_action_required:I

    .line 125
    invoke-direct {p0, v2, v4}, Lcom/squareup/disputes/AllDisputesAdapter;->format(Lcom/squareup/protos/common/time/DateTime;I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 128
    sget v5, Lcom/squareup/disputes/R$style;->TextAppearance_Disputes_Description_Actionable:I

    move-object v2, v1

    .line 122
    invoke-direct/range {v2 .. v7}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    check-cast v1, Lcom/squareup/disputes/AllDisputesAdapter$Data;

    .line 148
    :goto_1
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 145
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid resolution: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_4
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->baseRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/disputes/AllDisputesAdapter;->resolvedRows:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 0

    .line 91
    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter;->getData(I)Lcom/squareup/disputes/AllDisputesAdapter$Data;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputesAdapter$Data;->getViewType()Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->ordinal()I

    move-result p1

    return p1
.end method

.method public final init(Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/Context;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/disputes/AllDisputesEvent;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    iput-object p2, p0, Lcom/squareup/disputes/AllDisputesAdapter;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 159
    iput-object p3, p0, Lcom/squareup/disputes/AllDisputesAdapter;->context:Landroid/content/Context;

    .line 160
    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter;->addSummary(Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;)V

    .line 161
    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter;->maybeAddOpen(Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;)V

    .line 162
    invoke-direct {p0, p1}, Lcom/squareup/disputes/AllDisputesAdapter;->maybeAddClosed(Lcom/squareup/disputes/AllDisputes$ScreenData$WithData;)V

    return-void
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 59
    check-cast p1, Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/disputes/AllDisputesAdapter;->onBindViewHolder(Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder<",
            "+",
            "Lcom/squareup/disputes/AllDisputesAdapter$Data;",
            ">;I)V"
        }
    .end annotation

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-direct {p0, p2}, Lcom/squareup/disputes/AllDisputesAdapter;->getData(I)Lcom/squareup/disputes/AllDisputesAdapter$Data;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;->uncheckedBind(Lcom/squareup/disputes/AllDisputesAdapter$Data;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 59
    invoke-virtual {p0, p1, p2}, Lcom/squareup/disputes/AllDisputesAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "I)",
            "Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder<",
            "+",
            "Lcom/squareup/disputes/AllDisputesAdapter$Data;",
            ">;"
        }
    .end annotation

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-static {}, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->values()[Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    move-result-object v0

    aget-object p2, v0, p2

    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p2}, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_5

    const/4 v0, 0x2

    if-eq p2, v0, :cond_4

    const/4 v0, 0x3

    const-string/jumbo v1, "workflow"

    if-eq p2, v0, :cond_2

    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    .line 79
    new-instance p2, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;

    sget v0, Lcom/squareup/disputes/R$layout;->row_load_more:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p2, p1, v0}, Lcom/squareup/disputes/AllDisputesAdapter$LoadMoreViewHolder;-><init>(Landroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast p2, Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 78
    :cond_2
    new-instance p2, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;

    sget v0, Lcom/squareup/disputes/R$layout;->row_dispute:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-direct {p2, p1, v0}, Lcom/squareup/disputes/AllDisputesAdapter$DisputeViewHolder;-><init>(Landroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast p2, Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;

    goto :goto_0

    .line 77
    :cond_4
    new-instance p2, Lcom/squareup/disputes/AllDisputesAdapter$SummaryViewHolder;

    sget v0, Lcom/squareup/disputes/R$layout;->row_summary:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/disputes/AllDisputesAdapter$SummaryViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;

    goto :goto_0

    .line 76
    :cond_5
    new-instance p2, Lcom/squareup/disputes/AllDisputesAdapter$HeaderViewHolder;

    sget v0, Lcom/squareup/disputes/R$layout;->row_header:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/squareup/disputes/AllDisputesAdapter$HeaderViewHolder;-><init>(Landroid/view/View;)V

    check-cast p2, Lcom/squareup/disputes/AllDisputesAdapter$BaseViewHolder;

    :goto_0
    return-object p2
.end method

.method public final updateResolved(Ljava/util/List;ILcom/squareup/disputes/AllDisputes$ScreenData$MoreType;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;I",
            "Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;",
            ")V"
        }
    .end annotation

    const-string v0, "resolvedDisputes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moreType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->resolvedRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 181
    check-cast p1, Ljava/lang/Iterable;

    .line 182
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter;->resolvedRows:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    .line 340
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 341
    move-object v7, v1

    check-cast v7, Lcom/squareup/protos/client/cbms/DisputedPayment;

    .line 183
    iget-object v1, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    if-eqz v1, :cond_5

    sget-object v2, Lcom/squareup/disputes/AllDisputesAdapter$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    const-string v3, "context"

    const-string v4, "it.latest_reporting_date"

    const-string v5, "it.current_disputed_amount"

    if-eq v1, v2, :cond_3

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_5

    .line 203
    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;

    .line 204
    iget-object v2, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/disputes/AllDisputesAdapter;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 205
    iget-object v2, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/disputes/AllDisputesAdapter;->formatMedium(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 206
    iget-object v2, p0, Lcom/squareup/disputes/AllDisputesAdapter;->context:Landroid/content/Context;

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v3, Lcom/squareup/disputes/R$string;->dispute_list_resolved_accepted:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(R.stri\u2026e_list_resolved_accepted)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v2

    check-cast v5, Ljava/lang/CharSequence;

    .line 207
    sget v8, Lcom/squareup/disputes/R$style;->TextAppearance_Disputes_Description_Won:I

    move-object v2, v1

    move-object v3, v4

    move-object v4, v5

    move v5, v8

    .line 203
    invoke-direct/range {v2 .. v7}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    check-cast v1, Lcom/squareup/disputes/AllDisputesAdapter$Data;

    goto/16 :goto_1

    .line 194
    :cond_1
    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;

    .line 195
    iget-object v2, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/disputes/AllDisputesAdapter;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 196
    iget-object v2, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/disputes/AllDisputesAdapter;->formatMedium(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 197
    iget-object v2, p0, Lcom/squareup/disputes/AllDisputesAdapter;->context:Landroid/content/Context;

    if-nez v2, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v3, Lcom/squareup/disputes/R$string;->dispute_list_resolved_cardholder:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(R.stri\u2026list_resolved_cardholder)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v2

    check-cast v5, Ljava/lang/CharSequence;

    .line 198
    sget v8, Lcom/squareup/disputes/R$style;->TextAppearance_Disputes_Description_Lost:I

    move-object v2, v1

    move-object v3, v4

    move-object v4, v5

    move v5, v8

    .line 194
    invoke-direct/range {v2 .. v7}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    check-cast v1, Lcom/squareup/disputes/AllDisputesAdapter$Data;

    goto :goto_1

    .line 185
    :cond_3
    new-instance v1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;

    .line 186
    iget-object v2, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/disputes/AllDisputesAdapter;->format(Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 187
    iget-object v2, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/disputes/AllDisputesAdapter;->formatMedium(Lcom/squareup/protos/common/time/YearMonthDay;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 188
    iget-object v2, p0, Lcom/squareup/disputes/AllDisputesAdapter;->context:Landroid/content/Context;

    if-nez v2, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget v3, Lcom/squareup/disputes/R$string;->dispute_list_resolved_merchant:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "context.getString(R.stri\u2026e_list_resolved_merchant)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v5, v2

    check-cast v5, Ljava/lang/CharSequence;

    .line 189
    sget v8, Lcom/squareup/disputes/R$style;->TextAppearance_Disputes_Description_Won:I

    move-object v2, v1

    move-object v3, v4

    move-object v4, v5

    move v5, v8

    .line 185
    invoke-direct/range {v2 .. v7}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    check-cast v1, Lcom/squareup/disputes/AllDisputesAdapter$Data;

    .line 213
    :goto_1
    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 210
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 211
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Invalid resolution: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, v7, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 210
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 215
    :cond_6
    sget-object p1, Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;->NO_MORE:Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    if-eq p3, p1, :cond_7

    sget-object p1, Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;->LOAD_MORE_ERROR:Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;

    if-eq p3, p1, :cond_7

    .line 216
    iget-object p1, p0, Lcom/squareup/disputes/AllDisputesAdapter;->resolvedRows:Ljava/util/List;

    new-instance v0, Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;

    invoke-direct {v0, p3, p2}, Lcom/squareup/disputes/AllDisputesAdapter$Data$LoadMore;-><init>(Lcom/squareup/disputes/AllDisputes$ScreenData$MoreType;I)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    :cond_7
    invoke-virtual {p0}, Lcom/squareup/disputes/AllDisputesAdapter;->notifyDataSetChanged()V

    return-void
.end method
