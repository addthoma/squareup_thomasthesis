.class public final Lcom/squareup/disputes/DisputesSerializer;
.super Ljava/lang/Object;
.source "DisputesSerializer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisputesSerializer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisputesSerializer.kt\ncom/squareup/disputes/DisputesSerializer\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,107:1\n180#2:108\n56#3:109\n56#3:110\n56#3:111\n56#3:112\n56#3:113\n56#3:114\n56#3:115\n56#3:116\n56#3:117\n*E\n*S KotlinDebug\n*F\n+ 1 DisputesSerializer.kt\ncom/squareup/disputes/DisputesSerializer\n*L\n63#1:108\n63#1:109\n63#1:110\n63#1:111\n63#1:112\n63#1:113\n63#1:114\n63#1:115\n63#1:116\n63#1:117\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\u0004\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesSerializer;",
        "",
        "()V",
        "deserializeState",
        "Lcom/squareup/disputes/DisputesState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "snapshotState",
        "state",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/disputes/DisputesSerializer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/disputes/DisputesSerializer;

    invoke-direct {v0}, Lcom/squareup/disputes/DisputesSerializer;-><init>()V

    sput-object v0, Lcom/squareup/disputes/DisputesSerializer;->INSTANCE:Lcom/squareup/disputes/DisputesSerializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/disputes/DisputesState;
    .locals 4

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 108
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 64
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 65
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Class.forName(className)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    .line 68
    const-class v1, Lcom/squareup/disputes/DisputesState$WaitingToStart;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p1, Lcom/squareup/disputes/DisputesState$WaitingToStart;->INSTANCE:Lcom/squareup/disputes/DisputesState$WaitingToStart;

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    goto/16 :goto_0

    .line 69
    :cond_0
    const-class v1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 70
    new-instance v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;

    .line 71
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v2

    .line 70
    :cond_1
    invoke-direct {v0, v2}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputes;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    goto/16 :goto_0

    .line 78
    :cond_2
    const-class v1, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 109
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    const/4 v0, 0x0

    const/4 v1, 0x2

    .line 78
    new-instance v3, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;

    invoke-direct {v3, p1, v0, v1, v2}, Lcom/squareup/disputes/DisputesState$OverviewState$DisputesLoaded;-><init>(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    move-object p1, v3

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    goto/16 :goto_0

    .line 79
    :cond_3
    const-class v1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 110
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    .line 79
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result p1

    new-instance v1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;

    invoke-direct {v1, v0, p1}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreDisputes;-><init>(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;I)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    goto/16 :goto_0

    .line 80
    :cond_4
    const-class v1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 81
    new-instance v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingDisputesError;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    goto/16 :goto_0

    .line 83
    :cond_5
    const-class v1, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreError;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 111
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;

    .line 84
    new-instance v0, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreError;

    invoke-direct {v0, p1}, Lcom/squareup/disputes/DisputesState$OverviewState$LoadingMoreError;-><init>(Lcom/squareup/protos/client/cbms/GetDisputeSummaryAndListDisputesResponse;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    goto/16 :goto_0

    .line 86
    :cond_6
    const-class v1, Lcom/squareup/disputes/DisputesState$DetailState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 112
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    .line 87
    new-instance v0, Lcom/squareup/disputes/DisputesState$DetailState;

    invoke-direct {v0, p1}, Lcom/squareup/disputes/DisputesState$DetailState;-><init>(Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    goto/16 :goto_0

    .line 89
    :cond_7
    const-class v1, Lcom/squareup/disputes/DisputesState$ActionDialogState;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 113
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    .line 90
    new-instance v0, Lcom/squareup/disputes/DisputesState$ActionDialogState;

    invoke-direct {v0, p1}, Lcom/squareup/disputes/DisputesState$ActionDialogState;-><init>(Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    goto/16 :goto_0

    .line 92
    :cond_8
    const-class v1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingForm;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 114
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    .line 93
    new-instance v0, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingForm;

    invoke-direct {v0, p1}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingForm;-><init>(Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    goto :goto_0

    .line 95
    :cond_9
    const-class v1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 115
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/client/irf/GetFormResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/irf/GetFormResponse;

    .line 116
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    .line 96
    new-instance v1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;

    invoke-direct {v1, v0, p1}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$FormLoaded;-><init>(Lcom/squareup/protos/client/irf/GetFormResponse;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    goto :goto_0

    .line 98
    :cond_a
    const-class v1, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 99
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    .line 117
    sget-object v2, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v3, Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-virtual {v2, v3}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    .line 99
    new-instance v2, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;

    invoke-direct {v2, v0, v1, p1}, Lcom/squareup/disputes/DisputesState$ChallengeSummaryState$LoadingFormError;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    move-object p1, v2

    check-cast p1, Lcom/squareup/disputes/DisputesState;

    :goto_0
    return-object p1

    .line 102
    :cond_b
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final snapshotState(Lcom/squareup/disputes/DisputesState;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;

    invoke-direct {v1, p1}, Lcom/squareup/disputes/DisputesSerializer$snapshotState$1;-><init>(Lcom/squareup/disputes/DisputesState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
