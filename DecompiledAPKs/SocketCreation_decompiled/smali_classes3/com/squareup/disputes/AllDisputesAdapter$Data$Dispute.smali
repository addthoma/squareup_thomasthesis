.class public final Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;
.super Lcom/squareup/disputes/AllDisputesAdapter$Data;
.source "AllDisputesAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/AllDisputesAdapter$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Dispute"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0010\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\tH\u00c6\u0003J;\u0010\u0018\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u0006H\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001R\u0011\u0010\u0007\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000cR\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;",
        "Lcom/squareup/disputes/AllDisputesAdapter$Data;",
        "date",
        "",
        "description",
        "descriptionStyle",
        "",
        "amount",
        "disputedPayment",
        "Lcom/squareup/protos/client/cbms/DisputedPayment;",
        "(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lcom/squareup/protos/client/cbms/DisputedPayment;)V",
        "getAmount",
        "()Ljava/lang/CharSequence;",
        "getDate",
        "getDescription",
        "getDescriptionStyle",
        "()I",
        "getDisputedPayment",
        "()Lcom/squareup/protos/client/cbms/DisputedPayment;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amount:Ljava/lang/CharSequence;

.field private final date:Ljava/lang/CharSequence;

.field private final description:Ljava/lang/CharSequence;

.field private final descriptionStyle:I

.field private final disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lcom/squareup/protos/client/cbms/DisputedPayment;)V
    .locals 2

    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disputedPayment"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    sget-object v0, Lcom/squareup/disputes/AllDisputesAdapter$ViewType;->TYPE_DISPUTE:Lcom/squareup/disputes/AllDisputesAdapter$ViewType;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/disputes/AllDisputesAdapter$Data;-><init>(Lcom/squareup/disputes/AllDisputesAdapter$ViewType;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->date:Ljava/lang/CharSequence;

    iput-object p2, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->description:Ljava/lang/CharSequence;

    iput p3, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->descriptionStyle:I

    iput-object p4, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->amount:Ljava/lang/CharSequence;

    iput-object p5, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lcom/squareup/protos/client/cbms/DisputedPayment;ILjava/lang/Object;)Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->date:Ljava/lang/CharSequence;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->description:Ljava/lang/CharSequence;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget p3, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->descriptionStyle:I

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->amount:Ljava/lang/CharSequence;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->copy(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lcom/squareup/protos/client/cbms/DisputedPayment;)Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->date:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component2()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->description:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->descriptionStyle:I

    return v0
.end method

.method public final component4()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->amount:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/client/cbms/DisputedPayment;
    .locals 1

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    return-object v0
.end method

.method public final copy(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lcom/squareup/protos/client/cbms/DisputedPayment;)Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;
    .locals 7

    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disputedPayment"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lcom/squareup/protos/client/cbms/DisputedPayment;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->date:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->date:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->description:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->description:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->descriptionStyle:I

    iget v1, p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->descriptionStyle:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->amount:Ljava/lang/CharSequence;

    iget-object v1, p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->amount:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    iget-object p1, p1, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Ljava/lang/CharSequence;
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->amount:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getDate()Ljava/lang/CharSequence;
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->date:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/CharSequence;
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->description:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getDescriptionStyle()I
    .locals 1

    .line 239
    iget v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->descriptionStyle:I

    return v0
.end method

.method public final getDisputedPayment()Lcom/squareup/protos/client/cbms/DisputedPayment;
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->date:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->description:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->descriptionStyle:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->amount:Ljava/lang/CharSequence;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Dispute(date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->date:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->description:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", descriptionStyle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->descriptionStyle:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->amount:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, ", disputedPayment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/disputes/AllDisputesAdapter$Data$Dispute;->disputedPayment:Lcom/squareup/protos/client/cbms/DisputedPayment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
