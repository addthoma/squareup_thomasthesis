.class public final Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;
.super Ljava/lang/Object;
.source "DisputesDetailCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/disputes/DisputesDetailCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001BA\u0008\u0001\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0006\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a2\u0006\u0002\u0010\u000cJ$\u0010\r\u001a\u00020\u000e2\u001c\u0010\u000f\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00130\u0011j\u0002`\u00140\u0010R\u000e\u0010\u0008\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "mediumDateFormatter",
        "Ljava/text/DateFormat;",
        "mediumDateNoYearFormatter",
        "dateTimeFormatter",
        "locale",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljavax/inject/Provider;)V",
        "create",
        "Lcom/squareup/disputes/DisputesDetailCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/disputes/DisputesDetail$ScreenData;",
        "Lcom/squareup/disputes/DetailEvent;",
        "Lcom/squareup/disputes/DisputesDetailScreen;",
        "disputes_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dateTimeFormatter:Ljava/text/DateFormat;

.field private final locale:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mediumDateFormatter:Ljava/text/DateFormat;

.field private final mediumDateNoYearFormatter:Ljava/text/DateFormat;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljavax/inject/Provider;)V
    .locals 1
    .param p2    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumForm;
        .end annotation
    .end param
    .param p3    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumFormNoYear;
        .end annotation
    .end param
    .param p4    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumDateTime;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediumDateFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediumDateNoYearFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateTimeFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;->mediumDateFormatter:Ljava/text/DateFormat;

    iput-object p3, p0, Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;->mediumDateNoYearFormatter:Ljava/text/DateFormat;

    iput-object p4, p0, Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;->dateTimeFormatter:Ljava/text/DateFormat;

    iput-object p5, p0, Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;->locale:Ljavax/inject/Provider;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/disputes/DisputesDetailCoordinator;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/disputes/DisputesDetail$ScreenData;",
            "Lcom/squareup/disputes/DetailEvent;",
            ">;>;)",
            "Lcom/squareup/disputes/DisputesDetailCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    new-instance v0, Lcom/squareup/disputes/DisputesDetailCoordinator;

    .line 90
    iget-object v3, p0, Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, p0, Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;->mediumDateFormatter:Ljava/text/DateFormat;

    iget-object v5, p0, Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;->mediumDateNoYearFormatter:Ljava/text/DateFormat;

    .line 91
    iget-object v6, p0, Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;->dateTimeFormatter:Ljava/text/DateFormat;

    iget-object v7, p0, Lcom/squareup/disputes/DisputesDetailCoordinator$Factory;->locale:Ljavax/inject/Provider;

    move-object v1, v0

    move-object v2, p1

    .line 89
    invoke-direct/range {v1 .. v7}, Lcom/squareup/disputes/DisputesDetailCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljavax/inject/Provider;)V

    return-object v0
.end method
