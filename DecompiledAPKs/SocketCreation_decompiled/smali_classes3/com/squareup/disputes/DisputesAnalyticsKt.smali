.class public final Lcom/squareup/disputes/DisputesAnalyticsKt;
.super Ljava/lang/Object;
.source "DisputesAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\n\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\u001a\u000c\u0010\u000b\u001a\u00020\u000c*\u00020\rH\u0000\u001a\u0014\u0010\u000e\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0001H\u0000\u001a\u0014\u0010\u0010\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0001H\u0000\u001a\u0014\u0010\u0011\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0001H\u0000\u001a\u000c\u0010\u0012\u001a\u00020\u000c*\u00020\rH\u0000\u001a\u000c\u0010\u0013\u001a\u00020\u000c*\u00020\rH\u0000\u001a\u0014\u0010\u0014\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0001H\u0000\u001a\u001e\u0010\u0015\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u0016\u001a\u00020\u00012\u0008\u0008\u0002\u0010\u000f\u001a\u00020\u0001H\u0002\u001a\u000c\u0010\u0017\u001a\u00020\u000c*\u00020\rH\u0000\u001a\u0014\u0010\u0018\u001a\u00020\u000c*\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0001H\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "CHALLENGE",
        "",
        "GO_TO_WEB",
        "LEARN_MORE",
        "NOTIFICATION_POPUP_CANCEL",
        "NOTIFICATION_POPUP_VIEW",
        "REFUND_CUSTOMER",
        "SELECT_DISPUTE",
        "VIEW_DETAIL",
        "VIEW_REPORT",
        "VIEW_SUBMISSION",
        "canceledNotificationPopup",
        "",
        "Lcom/squareup/analytics/Analytics;",
        "clickedChallenge",
        "paymentToken",
        "clickedRefund",
        "goneToWeb",
        "learnedMore",
        "popupGoToDisputes",
        "selectedDispute",
        "send",
        "description",
        "viewedReport",
        "viewedSubmission",
        "disputes_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CHALLENGE:Ljava/lang/String; = "Disputes: Click Challenge"

.field public static final GO_TO_WEB:Ljava/lang/String; = "Disputes: Continue on Web"

.field public static final LEARN_MORE:Ljava/lang/String; = "Disputes: Learn More"

.field public static final NOTIFICATION_POPUP_CANCEL:Ljava/lang/String; = "Disputes Action Requested Modal: Cancel"

.field public static final NOTIFICATION_POPUP_VIEW:Ljava/lang/String; = "Disputes Action Requested Modal: View Disputes"

.field public static final REFUND_CUSTOMER:Ljava/lang/String; = "Disputes: Click Refund"

.field public static final SELECT_DISPUTE:Ljava/lang/String; = "Disputes: Select Dispute"

.field public static final VIEW_DETAIL:Ljava/lang/String; = "Disputes: Disputes Detail"

.field public static final VIEW_REPORT:Ljava/lang/String; = "Disputes: Disputes Report"

.field public static final VIEW_SUBMISSION:Ljava/lang/String; = "Disputes: View Challenge Submission"


# direct methods
.method public static final canceledNotificationPopup(Lcom/squareup/analytics/Analytics;)V
    .locals 3

    const-string v0, "$this$canceledNotificationPopup"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const-string v1, "Disputes Action Requested Modal: Cancel"

    const/4 v2, 0x2

    .line 22
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/disputes/DisputesAnalyticsKt;->send$default(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final clickedChallenge(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    const-string v0, "$this$clickedChallenge"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Disputes: Click Challenge"

    .line 46
    invoke-static {p0, v0, p1}, Lcom/squareup/disputes/DisputesAnalyticsKt;->send(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static final clickedRefund(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    const-string v0, "$this$clickedRefund"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Disputes: Click Refund"

    .line 42
    invoke-static {p0, v0, p1}, Lcom/squareup/disputes/DisputesAnalyticsKt;->send(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static final goneToWeb(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    const-string v0, "$this$goneToWeb"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Disputes: Continue on Web"

    .line 50
    invoke-static {p0, v0, p1}, Lcom/squareup/disputes/DisputesAnalyticsKt;->send(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static final learnedMore(Lcom/squareup/analytics/Analytics;)V
    .locals 3

    const-string v0, "$this$learnedMore"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const-string v1, "Disputes: Learn More"

    const/4 v2, 0x2

    .line 34
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/disputes/DisputesAnalyticsKt;->send$default(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final popupGoToDisputes(Lcom/squareup/analytics/Analytics;)V
    .locals 3

    const-string v0, "$this$popupGoToDisputes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const-string v1, "Disputes Action Requested Modal: View Disputes"

    const/4 v2, 0x2

    .line 18
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/disputes/DisputesAnalyticsKt;->send$default(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final selectedDispute(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    const-string v0, "$this$selectedDispute"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Disputes: Select Dispute"

    .line 30
    invoke-static {p0, v0, p1}, Lcom/squareup/disputes/DisputesAnalyticsKt;->send(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static final send(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 60
    new-instance v0, Lcom/squareup/disputes/DisputesAnalyticsEvent;

    invoke-direct {v0, p1, p2}, Lcom/squareup/disputes/DisputesAnalyticsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    .line 59
    invoke-interface {p0, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    return-void
.end method

.method static synthetic send$default(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const-string p2, ""

    .line 57
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/disputes/DisputesAnalyticsKt;->send(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static final viewedReport(Lcom/squareup/analytics/Analytics;)V
    .locals 3

    const-string v0, "$this$viewedReport"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const-string v1, "Disputes: Disputes Report"

    const/4 v2, 0x2

    .line 26
    invoke-static {p0, v1, v0, v2, v0}, Lcom/squareup/disputes/DisputesAnalyticsKt;->send$default(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    return-void
.end method

.method public static final viewedSubmission(Lcom/squareup/analytics/Analytics;Ljava/lang/String;)V
    .locals 1

    const-string v0, "$this$viewedSubmission"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "Disputes: View Challenge Submission"

    .line 38
    invoke-static {p0, v0, p1}, Lcom/squareup/disputes/DisputesAnalyticsKt;->send(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
