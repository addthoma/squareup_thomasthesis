.class public Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider;
.super Ljava/lang/Object;
.source "SupportedConnectV2ObjectTypesProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$Builder;
    }
.end annotation


# instance fields
.field public final objectTypesToSync:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogObjectType;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider;->objectTypesToSync:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/List;Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider$1;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1}, Lcom/squareup/features/cogs/SupportedConnectV2ObjectTypesProvider;-><init>(Ljava/util/List;)V

    return-void
.end method
