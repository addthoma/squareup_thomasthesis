.class public final Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;
.super Ljava/lang/Object;
.source "ChooseDateState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseDateState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseDateState.kt\ncom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,113:1\n180#2:114\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseDateState.kt\ncom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion\n*L\n57#1:114\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\tJ\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;",
        "",
        "()V",
        "fromByteString",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;",
        "bytes",
        "Lokio/ByteString;",
        "restoreFromSnapshot",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "startState",
        "Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;",
        "data",
        "Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromByteString(Lokio/ByteString;)Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;
    .locals 3

    const-string v0, "bytes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 58
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 60
    const-class v1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;

    .line 61
    invoke-static {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateStateKt;->access$readChooseDateScreenData(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;

    move-result-object p1

    .line 60
    invoke-direct {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;-><init>(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)V

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    goto :goto_0

    .line 63
    :cond_0
    const-class v1, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingCustomDate;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingCustomDate;

    .line 64
    invoke-static {p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateStateKt;->access$readCustomDateScreenData(Lokio/BufferedSource;)Lcom/squareup/invoices/workflow/edit/CustomDateInfo;

    move-result-object p1

    .line 63
    invoke-direct {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingCustomDate;-><init>(Lcom/squareup/invoices/workflow/edit/CustomDateInfo;)V

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    :goto_0
    return-object v0

    .line 66
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final restoreFromSnapshot(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;
    .locals 1

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    move-object v0, p0

    check-cast v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$Companion;->fromByteString(Lokio/ByteString;)Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState;

    move-result-object p1

    return-object p1
.end method

.method public final startState(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;
    .locals 1

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;

    invoke-direct {v0, p1}, Lcom/squareup/features/invoices/shared/edit/workflow/datepicker/ChooseDateState$SelectingDate;-><init>(Lcom/squareup/invoices/workflow/edit/ChooseDateInfo;)V

    return-object v0
.end method
