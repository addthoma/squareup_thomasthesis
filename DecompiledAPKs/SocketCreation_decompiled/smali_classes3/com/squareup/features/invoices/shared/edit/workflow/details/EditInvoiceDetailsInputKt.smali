.class public final Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInputKt;
.super Ljava/lang/Object;
.source "EditInvoiceDetailsInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0011\u0010\u0000\u001a\u00020\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "DEFAULT",
        "Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;",
        "getDEFAULT",
        "()Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final DEFAULT:Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 12
    new-instance v0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;

    .line 13
    new-instance v1, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;

    const-string v2, ""

    invoke-direct {v1, v2, v2, v2}, Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 12
    invoke-direct {v0, v1, v2, v2}, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;-><init>(Lcom/squareup/invoices/workflow/edit/EditInvoiceDetailsInfo;ZZ)V

    sput-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInputKt;->DEFAULT:Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;

    return-void
.end method

.method public static final getDEFAULT()Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInputKt;->DEFAULT:Lcom/squareup/features/invoices/shared/edit/workflow/details/EditInvoiceDetailsInput;

    return-object v0
.end method
