.class public final Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer$DefaultImpls;
.super Ljava/lang/Object;
.source "InvoiceSectionRenderer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultImpls"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static render(Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;Lcom/squareup/features/invoices/widgets/SectionElement;Lcom/squareup/features/invoices/widgets/EventHandler;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const-string v0, "$this$render"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parent"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    sget-object v0, Lcom/squareup/features/invoices/widgets/RenderDelegate;->INSTANCE:Lcom/squareup/features/invoices/widgets/RenderDelegate;

    invoke-virtual {v0, p1, p0, p2, p3}, Lcom/squareup/features/invoices/widgets/RenderDelegate;->render(Lcom/squareup/features/invoices/widgets/SectionElement;Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;Lcom/squareup/features/invoices/widgets/EventHandler;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method
