.class public final Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;
.super Landroid/widget/LinearLayout;
.source "InvoicesBigTextHeader.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInvoicesBigTextHeader.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InvoicesBigTextHeader.kt\ncom/squareup/features/invoices/widgets/InvoicesBigTextHeader\n*L\n1#1,78:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R$\u0010\t\u001a\u00020\u00082\u0006\u0010\u0007\u001a\u00020\u00088F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\rR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0010\u001a\u00020\u00082\u0006\u0010\u0007\u001a\u00020\u00088F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0011\u0010\u000b\"\u0004\u0008\u0012\u0010\rR\u000e\u0010\u0013\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "value",
        "",
        "subtitle",
        "getSubtitle",
        "()Ljava/lang/CharSequence;",
        "setSubtitle",
        "(Ljava/lang/CharSequence;)V",
        "subtitleView",
        "Landroid/widget/TextView;",
        "title",
        "getTitle",
        "setTitle",
        "titleView",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final subtitleView:Landroid/widget/TextView;

.field private final titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    new-instance p2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v1, -0x1

    invoke-direct {p2, v1, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    check-cast p2, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, p2}, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 p2, 0x1

    .line 48
    invoke-virtual {p0, p2}, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->setOrientation(I)V

    .line 49
    invoke-virtual {p0, p2}, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->setGravity(I)V

    .line 50
    sget v1, Lcom/squareup/noho/R$dimen;->noho_gap_12:I

    invoke-static {p0, v1}, Lcom/squareup/util/ViewResourceExtensionsKt;->setPaddingTopRes(Landroid/view/View;I)V

    .line 53
    new-instance v1, Lcom/squareup/marketfont/MarketTextView;

    invoke-direct {v1, p1}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    .line 54
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    check-cast v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 55
    move-object v2, v1

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/squareup/features/invoices/widgets/R$color;->invoices_big_text_header_title:I

    invoke-static {v2, v3}, Lcom/squareup/util/ViewResourceExtensionsKt;->setTextColorRes(Landroid/widget/TextView;I)V

    .line 56
    sget v3, Lcom/squareup/features/invoices/widgets/R$dimen;->big_text_header_title_size:I

    invoke-static {v2, v3}, Lcom/squareup/util/ViewResourceExtensionsKt;->setTextSizeRes(Landroid/widget/TextView;I)V

    .line 57
    invoke-virtual {v1}, Lcom/squareup/marketfont/MarketTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v1, v3, p2}, Lcom/squareup/marketfont/MarketTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 59
    iput-object v2, p0, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->titleView:Landroid/widget/TextView;

    check-cast v1, Landroid/view/View;

    .line 52
    invoke-virtual {p0, v1}, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->addView(Landroid/view/View;)V

    .line 63
    new-instance p2, Lcom/squareup/marketfont/MarketTextView;

    invoke-direct {p2, p1}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    .line 64
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    check-cast v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p2, v1}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    move-object v0, p2

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/squareup/features/invoices/widgets/R$color;->invoices_big_text_header_subtitle:I

    invoke-static {v0, v1}, Lcom/squareup/util/ViewResourceExtensionsKt;->setTextColorRes(Landroid/widget/TextView;I)V

    .line 66
    sget v1, Lcom/squareup/features/invoices/widgets/R$dimen;->big_text_header_subtitle_size:I

    invoke-static {v0, v1}, Lcom/squareup/util/ViewResourceExtensionsKt;->setTextSizeRes(Landroid/widget/TextView;I)V

    .line 68
    iput-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->subtitleView:Landroid/widget/TextView;

    check-cast p2, Landroid/view/View;

    .line 62
    invoke-virtual {p0, p2}, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->addView(Landroid/view/View;)V

    .line 72
    new-instance p2, Lcom/squareup/features/invoices/widgets/InvoicesDivider;

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p2, p1, v0, v1, v0}, Lcom/squareup/features/invoices/widgets/InvoicesDivider;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 73
    check-cast p2, Landroid/view/View;

    sget p1, Lcom/squareup/noho/R$dimen;->noho_gap_40:I

    invoke-static {p2, p1}, Lcom/squareup/util/ViewResourceExtensionsKt;->setMarginTopRes(Landroid/view/View;I)V

    .line 71
    invoke-virtual {p0, p2}, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->addView(Landroid/view/View;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 28
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final getSubtitle()Ljava/lang/CharSequence;
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "subtitleView.text"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    const-string/jumbo v1, "titleView.text"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/InvoicesBigTextHeader;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
