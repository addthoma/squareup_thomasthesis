.class final Lcom/squareup/features/invoices/widgets/RenderDelegate$render$$inlined$with$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InvoiceSectionRenderer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/features/invoices/widgets/RenderDelegate;->render(Lcom/squareup/features/invoices/widgets/SectionElement;Lcom/squareup/features/invoices/widgets/InvoiceSectionRenderer;Lcom/squareup/features/invoices/widgets/EventHandler;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/features/invoices/widgets/RenderDelegate$render$1$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $eventHandler$inlined:Lcom/squareup/features/invoices/widgets/EventHandler;

.field final synthetic $parent$inlined:Landroid/view/ViewGroup;

.field final synthetic $sectionElement$inlined:Lcom/squareup/features/invoices/widgets/SectionElement;


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/SectionElement;Lcom/squareup/features/invoices/widgets/EventHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$$inlined$with$lambda$1;->$parent$inlined:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$$inlined$with$lambda$1;->$sectionElement$inlined:Lcom/squareup/features/invoices/widgets/SectionElement;

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$$inlined$with$lambda$1;->$eventHandler$inlined:Lcom/squareup/features/invoices/widgets/EventHandler;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$$inlined$with$lambda$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 3

    .line 138
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$$inlined$with$lambda$1;->$eventHandler$inlined:Lcom/squareup/features/invoices/widgets/EventHandler;

    .line 139
    new-instance v1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/RenderDelegate$render$$inlined$with$lambda$1;->$sectionElement$inlined:Lcom/squareup/features/invoices/widgets/SectionElement;

    check-cast v2, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;

    invoke-virtual {v2}, Lcom/squareup/features/invoices/widgets/SectionElement$EditButtonRowData;->getEvent()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;-><init>(Ljava/lang/Object;)V

    check-cast v1, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;

    .line 138
    invoke-interface {v0, v1}, Lcom/squareup/features/invoices/widgets/EventHandler;->onEvent(Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent;)V

    return-void
.end method
