.class public interface abstract Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView;
.super Ljava/lang/Object;
.source "InvoiceSectionContainerView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView$Factory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0001\u000eJ\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0005H&J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\tH&J\u0012\u0010\n\u001a\u00020\u00032\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH&J\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\tH&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView;",
        "",
        "addViewToContainer",
        "",
        "view",
        "Landroid/view/View;",
        "asView",
        "setBottomDividerVisibleOrGone",
        "visible",
        "",
        "setHeaderTextAndVisibility",
        "text",
        "",
        "setTopDividerVisibleOrGone",
        "Factory",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract addViewToContainer(Landroid/view/View;)V
.end method

.method public abstract asView()Landroid/view/View;
.end method

.method public abstract setBottomDividerVisibleOrGone(Z)V
.end method

.method public abstract setHeaderTextAndVisibility(Ljava/lang/String;)V
.end method

.method public abstract setTopDividerVisibleOrGone(Z)V
.end method
