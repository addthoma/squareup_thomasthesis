.class public final Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactoryKt;
.super Ljava/lang/Object;
.source "BottomSheetDialogViewFactory.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBottomSheetDialogViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BottomSheetDialogViewFactory.kt\ncom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactoryKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,28:1\n1642#2,2:29\n*E\n*S KotlinDebug\n*F\n+ 1 BottomSheetDialogViewFactory.kt\ncom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactoryKt\n*L\n24#1,2:29\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0010\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a(\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t\u00a8\u0006\n"
    }
    d2 = {
        "setOn",
        "",
        "",
        "Lcom/squareup/features/invoices/widgets/SectionElement;",
        "container",
        "Landroid/view/ViewGroup;",
        "viewFactory",
        "Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;",
        "eventHandler",
        "Lcom/squareup/features/invoices/widgets/EventHandler;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final setOn(Ljava/util/List;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;Lcom/squareup/features/invoices/widgets/EventHandler;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/features/invoices/widgets/SectionElement;",
            ">;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;",
            "Lcom/squareup/features/invoices/widgets/EventHandler;",
            ")V"
        }
    .end annotation

    const-string v0, "$this$setOn"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "viewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eventHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 23
    invoke-interface {p2, p0, p1, p3}, Lcom/squareup/features/invoices/widgets/bottomsheet/BottomSheetDialogViewFactory;->create(Ljava/util/List;Landroid/view/ViewGroup;Lcom/squareup/features/invoices/widgets/EventHandler;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 29
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    .line 25
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method
