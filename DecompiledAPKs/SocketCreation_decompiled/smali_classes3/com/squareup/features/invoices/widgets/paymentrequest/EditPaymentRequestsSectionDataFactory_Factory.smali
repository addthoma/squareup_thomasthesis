.class public final Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;
.super Ljava/lang/Object;
.source "EditPaymentRequestsSectionDataFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p5, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;"
        }
    .end annotation

    .line 46
    new-instance v6, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;-><init>(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;
    .locals 5

    .line 40
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Clock;

    iget-object v3, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;->newInstance(Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory_Factory;->get()Lcom/squareup/features/invoices/widgets/paymentrequest/EditPaymentRequestsSectionDataFactory;

    move-result-object v0

    return-object v0
.end method
