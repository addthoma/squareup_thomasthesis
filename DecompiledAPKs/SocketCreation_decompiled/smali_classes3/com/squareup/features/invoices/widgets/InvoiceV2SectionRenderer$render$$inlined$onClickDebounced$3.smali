.class public final Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$onClickDebounced$3;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer;->render(Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;Landroid/view/ViewGroup;Lkotlin/jvm/functions/Function1;)Lcom/squareup/noho/NohoRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 InvoiceV2SectionRenderer.kt\ncom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer\n*L\n1#1,1322:1\n245#2,2:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $onEvent$inlined:Lkotlin/jvm/functions/Function1;

.field final synthetic $this_render$inlined:Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;


# direct methods
.method public constructor <init>(Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$onClickDebounced$3;->$this_render$inlined:Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$onClickDebounced$3;->$onEvent$inlined:Lkotlin/jvm/functions/Function1;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoRow;

    .line 1323
    iget-object p1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$onClickDebounced$3;->$onEvent$inlined:Lkotlin/jvm/functions/Function1;

    new-instance v0, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/InvoiceV2SectionRenderer$render$$inlined$onClickDebounced$3;->$this_render$inlined:Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;

    invoke-virtual {v1}, Lcom/squareup/features/invoices/widgets/SectionElement$RecurringRow;->getEvent()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/features/invoices/widgets/InvoiceSectionViewEvent$Simple;-><init>(Ljava/lang/Object;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
