.class public final Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;
.super Ljava/lang/Object;
.source "InvoiceTimelineDateFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p4, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;-><init>(Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;
    .locals 4

    .line 33
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Clock;

    iget-object v3, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;->newInstance(Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/util/Clock;Lcom/squareup/util/Res;)Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter_Factory;->get()Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineDateFormatter;

    move-result-object v0

    return-object v0
.end method
