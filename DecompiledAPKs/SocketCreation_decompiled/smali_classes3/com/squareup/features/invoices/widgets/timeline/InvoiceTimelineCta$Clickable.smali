.class public final Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;
.super Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;
.source "InvoiceTimelineCta.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Clickable"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B%\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tR\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;",
        "Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;",
        "defaultString",
        "",
        "cta",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;",
        "block",
        "Lkotlin/Function0;",
        "",
        "(Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;Lkotlin/jvm/functions/Function0;)V",
        "getBlock",
        "()Lkotlin/jvm/functions/Function0;",
        "getCta",
        "()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final block:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final cta:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "cta"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1, v0}, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta;-><init>(Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p2, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;->cta:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    iput-object p3, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;->block:Lkotlin/jvm/functions/Function0;

    return-void
.end method


# virtual methods
.method public final getBlock()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;->block:Lkotlin/jvm/functions/Function0;

    return-object v0
.end method

.method public final getCta()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/timeline/InvoiceTimelineCta$Clickable;->cta:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToAction;

    return-object v0
.end method
