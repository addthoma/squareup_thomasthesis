.class public final Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;
.super Landroid/widget/LinearLayout;
.source "RealInvoiceSectionContainerView.kt"

# interfaces
.implements Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealInvoiceSectionContainerView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealInvoiceSectionContainerView.kt\ncom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView\n*L\n1#1,66:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\tH\u0016J\u0008\u0010\u0011\u001a\u00020\u0000H\u0016J\u0008\u0010\u0012\u001a\u00020\u000fH\u0002J\u0010\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0006H\u0016J\u0012\u0010\u0015\u001a\u00020\u000f2\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0006H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0001X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;",
        "Landroid/widget/LinearLayout;",
        "Lcom/squareup/features/invoices/widgets/InvoiceSectionContainerView;",
        "context",
        "Landroid/content/Context;",
        "isV2Enabled",
        "",
        "(Landroid/content/Context;Z)V",
        "bottomDivider",
        "Landroid/view/View;",
        "container",
        "header",
        "Lcom/squareup/marketfont/MarketTextView;",
        "topDivider",
        "addViewToContainer",
        "",
        "view",
        "asView",
        "bindViews",
        "setBottomDividerVisibleOrGone",
        "visible",
        "setHeaderTextAndVisibility",
        "text",
        "",
        "setTopDividerVisibleOrGone",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private bottomDivider:Landroid/view/View;

.field private container:Landroid/widget/LinearLayout;

.field private header:Lcom/squareup/marketfont/MarketTextView;

.field private topDivider:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    if-eqz p2, :cond_0

    .line 30
    sget p2, Lcom/squareup/features/invoices/widgets/impl/R$layout;->invoice_container_view_v2:I

    goto :goto_0

    .line 31
    :cond_0
    sget p2, Lcom/squareup/features/invoices/widgets/impl/R$layout;->invoice_container_view:I

    .line 32
    :goto_0
    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {p1, p2, v0}, Landroid/widget/LinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 33
    invoke-direct {p0}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->bindViews()V

    const/4 p1, 0x1

    .line 34
    invoke-virtual {p0, p1}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->setOrientation(I)V

    .line 36
    new-instance p1, Landroid/animation/LayoutTransition;

    invoke-direct {p1}, Landroid/animation/LayoutTransition;-><init>()V

    const/4 p2, 0x4

    .line 37
    invoke-virtual {p1, p2}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    .line 36
    invoke-virtual {p0, p1}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void
.end method

.method private final bindViews()V
    .locals 2

    .line 60
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$id;->invoice_top_divider:I

    invoke-virtual {p0, v0}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.invoice_top_divider)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->topDivider:Landroid/view/View;

    .line 61
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$id;->invoice_bottom_divider:I

    invoke-virtual {p0, v0}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.invoice_bottom_divider)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->bottomDivider:Landroid/view/View;

    .line 62
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$id;->invoice_section_container:I

    invoke-virtual {p0, v0}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.invoice_section_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->container:Landroid/widget/LinearLayout;

    .line 63
    sget v0, Lcom/squareup/features/invoices/widgets/impl/R$id;->invoice_section_header:I

    invoke-virtual {p0, v0}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.invoice_section_header)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->header:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method


# virtual methods
.method public addViewToContainer(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->container:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    const-string v1, "container"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public bridge synthetic asView()Landroid/view/View;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->asView()Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public asView()Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;
    .locals 0

    return-object p0
.end method

.method public setBottomDividerVisibleOrGone(Z)V
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->bottomDivider:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "bottomDivider"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setHeaderTextAndVisibility(Ljava/lang/String;)V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->header:Lcom/squareup/marketfont/MarketTextView;

    if-nez v0, :cond_0

    const-string v1, "header"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/widget/TextView;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setTextAndVisibility(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTopDividerVisibleOrGone(Z)V
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/features/invoices/widgets/RealInvoiceSectionContainerView;->topDivider:Landroid/view/View;

    if-nez v0, :cond_0

    const-string/jumbo v1, "topDivider"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
