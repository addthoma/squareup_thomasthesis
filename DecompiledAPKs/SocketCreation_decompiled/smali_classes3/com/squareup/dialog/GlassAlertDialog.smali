.class public Lcom/squareup/dialog/GlassAlertDialog;
.super Landroid/app/AlertDialog;
.source "GlassAlertDialog.java"


# instance fields
.field private final util:Lcom/squareup/dialog/GlassDialogUtil;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 15
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 16
    new-instance v0, Lcom/squareup/dialog/GlassDialogUtil;

    invoke-direct {v0, p1}, Lcom/squareup/dialog/GlassDialogUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/dialog/GlassAlertDialog;->util:Lcom/squareup/dialog/GlassDialogUtil;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    .line 26
    new-instance p2, Lcom/squareup/dialog/GlassDialogUtil;

    invoke-direct {p2, p1}, Lcom/squareup/dialog/GlassDialogUtil;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/dialog/GlassAlertDialog;->util:Lcom/squareup/dialog/GlassDialogUtil;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V
    .locals 0

    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 21
    new-instance p2, Lcom/squareup/dialog/GlassDialogUtil;

    invoke-direct {p2, p1}, Lcom/squareup/dialog/GlassDialogUtil;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/dialog/GlassAlertDialog;->util:Lcom/squareup/dialog/GlassDialogUtil;

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/dialog/GlassAlertDialog;->util:Lcom/squareup/dialog/GlassDialogUtil;

    invoke-virtual {v0}, Lcom/squareup/dialog/GlassDialogUtil;->dismiss()V

    .line 36
    invoke-super {p0}, Landroid/app/AlertDialog;->dismiss()V

    .line 37
    invoke-static {}, Lcom/squareup/dialog/GlassDialogUtil;->createCancelEvent()Landroid/view/MotionEvent;

    move-result-object v0

    .line 38
    invoke-super {p0, v0}, Landroid/app/AlertDialog;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 39
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/dialog/GlassAlertDialog;->util:Lcom/squareup/dialog/GlassDialogUtil;

    invoke-virtual {v0}, Lcom/squareup/dialog/GlassDialogUtil;->isInputEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-super {p0, p1}, Landroid/app/AlertDialog;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public show()V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/dialog/GlassAlertDialog;->util:Lcom/squareup/dialog/GlassDialogUtil;

    invoke-virtual {v0}, Lcom/squareup/dialog/GlassDialogUtil;->show()V

    .line 31
    invoke-super {p0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method
