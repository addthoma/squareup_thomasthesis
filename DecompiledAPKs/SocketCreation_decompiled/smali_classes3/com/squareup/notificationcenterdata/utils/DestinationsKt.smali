.class public final Lcom/squareup/notificationcenterdata/utils/DestinationsKt;
.super Ljava/lang/Object;
.source "Destinations.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDestinations.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Destinations.kt\ncom/squareup/notificationcenterdata/utils/DestinationsKt\n*L\n1#1,81:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0015\u0010\u0005\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0004\"\u0018\u0010\u0007\u001a\u00020\u0001*\u00020\u00088BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\"\u0015\u0010\u000b\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u0004\u00a8\u0006\r"
    }
    d2 = {
        "browserDialogBody",
        "",
        "Lcom/squareup/notificationcenterdata/Notification$Destination;",
        "getBrowserDialogBody",
        "(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;",
        "clientActionString",
        "getClientActionString",
        "string",
        "Lcom/squareup/protos/client/ClientAction;",
        "getString",
        "(Lcom/squareup/protos/client/ClientAction;)Ljava/lang/String;",
        "urlString",
        "getUrlString",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getBrowserDialogBody(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$browserDialogBody"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;

    const-string v1, ""

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;->getOpenInBrowserDialogBody()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    move-object v1, p0

    :cond_0
    return-object v1
.end method

.method public static final getClientActionString(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$clientActionString"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;

    const-string v1, ""

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;->getClientAction()Lcom/squareup/protos/client/ClientAction;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getString(Lcom/squareup/protos/client/ClientAction;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 19
    :cond_0
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    if-eqz v0, :cond_1

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;->getClientAction()Lcom/squareup/protos/client/ClientAction;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getString(Lcom/squareup/protos/client/ClientAction;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 20
    :cond_1
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UnsupportedClientAction;

    if-eqz v0, :cond_2

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UnsupportedClientAction;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$UnsupportedClientAction;->getClientAction()Lcom/squareup/protos/client/ClientAction;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getString(Lcom/squareup/protos/client/ClientAction;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 21
    :cond_2
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedUnsupportedClientAction;

    if-eqz v0, :cond_3

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedUnsupportedClientAction;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedUnsupportedClientAction;->getClientAction()Lcom/squareup/protos/client/ClientAction;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/notificationcenterdata/utils/DestinationsKt;->getString(Lcom/squareup/protos/client/ClientAction;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 22
    :cond_3
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;

    if-eqz v0, :cond_4

    goto :goto_0

    .line 23
    :cond_4
    instance-of p0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$Nowhere;

    if-eqz p0, :cond_5

    :goto_0
    return-object v1

    :cond_5
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final getString(Lcom/squareup/protos/client/ClientAction;)Ljava/lang/String;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_checkout_applet:Lcom/squareup/protos/client/ClientAction$ViewCheckoutApplet;

    if-eqz v0, :cond_0

    const-string p0, "ViewCheckoutApplet"

    goto/16 :goto_0

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_orders_applet:Lcom/squareup/protos/client/ClientAction$ViewOrdersApplet;

    if-eqz v0, :cond_1

    const-string p0, "ViewOrdersApplet"

    goto/16 :goto_0

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_invoices_applet:Lcom/squareup/protos/client/ClientAction$ViewInvoicesApplet;

    if-eqz v0, :cond_2

    const-string p0, "ViewInvoicesApplet"

    goto/16 :goto_0

    .line 54
    :cond_2
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_all_invoices:Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    if-eqz v0, :cond_3

    const-string p0, "ViewAllInvoices"

    goto/16 :goto_0

    .line 55
    :cond_3
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_invoice:Lcom/squareup/protos/client/ClientAction$ViewInvoice;

    if-eqz v0, :cond_4

    const-string p0, "ViewInvoice"

    goto/16 :goto_0

    .line 56
    :cond_4
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_transactions_applet:Lcom/squareup/protos/client/ClientAction$ViewTransactionsApplet;

    if-eqz v0, :cond_5

    const-string p0, "ViewTransactionsApplet"

    goto/16 :goto_0

    .line 57
    :cond_5
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_reports_applet:Lcom/squareup/protos/client/ClientAction$ViewReportsApplet;

    if-eqz v0, :cond_6

    const-string p0, "ViewReportsApplet"

    goto/16 :goto_0

    .line 58
    :cond_6
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_sales_report:Lcom/squareup/protos/client/ClientAction$ViewSalesReport;

    if-eqz v0, :cond_7

    const-string p0, "ViewSalesReport"

    goto/16 :goto_0

    .line 59
    :cond_7
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_all_disputes:Lcom/squareup/protos/client/ClientAction$ViewAllDisputes;

    if-eqz v0, :cond_8

    const-string p0, "ViewAllDisputes"

    goto/16 :goto_0

    .line 60
    :cond_8
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_dispute:Lcom/squareup/protos/client/ClientAction$ViewDispute;

    if-eqz v0, :cond_9

    const-string p0, "ViewDispute"

    goto/16 :goto_0

    .line 61
    :cond_9
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_deposits_applet:Lcom/squareup/protos/client/ClientAction$ViewDepositsApplet;

    if-eqz v0, :cond_a

    const-string p0, "ViewDepositsApplet"

    goto/16 :goto_0

    .line 62
    :cond_a
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_all_deposits:Lcom/squareup/protos/client/ClientAction$ViewAllDeposits;

    if-eqz v0, :cond_b

    const-string p0, "ViewAllDeposits"

    goto/16 :goto_0

    .line 63
    :cond_b
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_customers_applet:Lcom/squareup/protos/client/ClientAction$ViewCustomersApplet;

    if-eqz v0, :cond_c

    const-string p0, "ViewCustomersApplet"

    goto/16 :goto_0

    .line 64
    :cond_c
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_customer_message_center:Lcom/squareup/protos/client/ClientAction$ViewCustomerMessageCenter;

    if-eqz v0, :cond_d

    const-string p0, "ViewCustomerMessageCenter"

    goto/16 :goto_0

    .line 65
    :cond_d
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_customer_conversation:Lcom/squareup/protos/client/ClientAction$ViewCustomerConversation;

    if-eqz v0, :cond_e

    const-string p0, "ViewCustomerConversation"

    goto/16 :goto_0

    .line 66
    :cond_e
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_items_applet:Lcom/squareup/protos/client/ClientAction$ViewItemsApplet;

    if-eqz v0, :cond_f

    const-string p0, "ViewItemsApplet"

    goto :goto_0

    .line 67
    :cond_f
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->create_item:Lcom/squareup/protos/client/ClientAction$CreateItem;

    if-eqz v0, :cond_10

    const-string p0, "CreateItem"

    goto :goto_0

    .line 68
    :cond_10
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_settings_applet:Lcom/squareup/protos/client/ClientAction$ViewSettingsApplet;

    if-eqz v0, :cond_11

    const-string p0, "ViewSettingsApplet"

    goto :goto_0

    .line 69
    :cond_11
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_bank_account:Lcom/squareup/protos/client/ClientAction$ViewBankAccount;

    if-eqz v0, :cond_12

    const-string p0, "ViewBankAccount"

    goto :goto_0

    .line 70
    :cond_12
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_support_applet:Lcom/squareup/protos/client/ClientAction$ViewSupportApplet;

    if-eqz v0, :cond_13

    const-string p0, "ViewSupportApplet"

    goto :goto_0

    .line 71
    :cond_13
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_support_message_center:Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    if-eqz v0, :cond_14

    const-string p0, "ViewSupportMessageCenter"

    goto :goto_0

    .line 72
    :cond_14
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_tutorials_and_tours:Lcom/squareup/protos/client/ClientAction$ViewTutorialsAndTours;

    if-eqz v0, :cond_15

    const-string p0, "ViewTutorialsAndTours"

    goto :goto_0

    .line 73
    :cond_15
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_deposits_settings:Lcom/squareup/protos/client/ClientAction$ViewDepositsSettings;

    if-eqz v0, :cond_16

    const-string p0, "ViewDepositsSettings"

    goto :goto_0

    .line 74
    :cond_16
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_overdue_invoices:Lcom/squareup/protos/client/ClientAction$ViewOverdueInvoices;

    if-eqz v0, :cond_17

    const-string p0, "ViewOverdueInvoices"

    goto :goto_0

    .line 75
    :cond_17
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->view_free_processing:Lcom/squareup/protos/client/ClientAction$ViewFreeProcessing;

    if-eqz v0, :cond_18

    const-string p0, "ViewFreeProcessing"

    goto :goto_0

    .line 76
    :cond_18
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    if-eqz v0, :cond_19

    const-string p0, "ActivateAccount"

    goto :goto_0

    .line 77
    :cond_19
    iget-object v0, p0, Lcom/squareup/protos/client/ClientAction;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    if-eqz v0, :cond_1a

    const-string p0, "FinishAccountSetup"

    goto :goto_0

    .line 78
    :cond_1a
    iget-object p0, p0, Lcom/squareup/protos/client/ClientAction;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    if-eqz p0, :cond_1b

    const-string p0, "LinkBankAccount"

    goto :goto_0

    :cond_1b
    const-string p0, ""

    :goto_0
    return-object p0
.end method

.method public static final getUrlString(Lcom/squareup/notificationcenterdata/Notification$Destination;)Ljava/lang/String;
    .locals 2

    const-string v0, "$this$urlString"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;

    const-string v1, ""

    if-eqz v0, :cond_0

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedSupportedClientAction;->getUrl()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 32
    :cond_0
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 33
    :cond_1
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UnsupportedClientAction;

    if-eqz v0, :cond_2

    goto :goto_0

    .line 34
    :cond_2
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedUnsupportedClientAction;

    if-eqz v0, :cond_3

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedUnsupportedClientAction;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$UrlBackedUnsupportedClientAction;->getUrl()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 35
    :cond_3
    instance-of v0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;

    if-eqz v0, :cond_4

    check-cast p0, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;

    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/Notification$Destination$ExternalUrl;->getUrl()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 36
    :cond_4
    instance-of p0, p0, Lcom/squareup/notificationcenterdata/Notification$Destination$Nowhere;

    if-eqz p0, :cond_5

    :goto_0
    return-object v1

    :cond_5
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
