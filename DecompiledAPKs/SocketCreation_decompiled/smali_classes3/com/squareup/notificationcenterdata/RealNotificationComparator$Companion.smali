.class final Lcom/squareup/notificationcenterdata/RealNotificationComparator$Companion;
.super Ljava/lang/Object;
.source "NotificationComparator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenterdata/RealNotificationComparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u0082\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R!\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R!\u0010\t\u001a\u0012\u0012\u0004\u0012\u00020\n0\u0004j\u0008\u0012\u0004\u0012\u00020\n`\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u0008R!\u0010\u000c\u001a\u0012\u0012\u0004\u0012\u00020\r0\u0004j\u0008\u0012\u0004\u0012\u00020\r`\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u0008R!\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u00100\u0004j\u0008\u0012\u0004\u0012\u00020\u0010`\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0008R\u0015\u0010\u0012\u001a\u00020\u0013*\u00020\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0015R\u0015\u0010\u0012\u001a\u00020\u0013*\u00020\r8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0016R\u0015\u0010\u0012\u001a\u00020\u0013*\u00020\u00108F\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0017\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/RealNotificationComparator$Companion;",
        "",
        "()V",
        "MESSAGE_TYPE_PRIORITY_COMPARATOR",
        "Ljava/util/Comparator;",
        "Lcom/squareup/communications/Message$Type;",
        "Lkotlin/Comparator;",
        "getMESSAGE_TYPE_PRIORITY_COMPARATOR",
        "()Ljava/util/Comparator;",
        "NOTIFICATION_COMPARATOR",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "getNOTIFICATION_COMPARATOR",
        "NOTIFICATION_DISPLAY_TYPE_COMPARATOR",
        "Lcom/squareup/notificationcenterdata/Notification$DisplayType;",
        "getNOTIFICATION_DISPLAY_TYPE_COMPARATOR",
        "READ_STATE_COMPARATOR",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "getREAD_STATE_COMPARATOR",
        "orderingValue",
        "",
        "getOrderingValue",
        "(Lcom/squareup/communications/Message$Type;)I",
        "(Lcom/squareup/notificationcenterdata/Notification$DisplayType;)I",
        "(Lcom/squareup/notificationcenterdata/Notification$State;)I",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/notificationcenterdata/RealNotificationComparator$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getMESSAGE_TYPE_PRIORITY_COMPARATOR()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/squareup/communications/Message$Type;",
            ">;"
        }
    .end annotation

    .line 40
    invoke-static {}, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->access$getMESSAGE_TYPE_PRIORITY_COMPARATOR$cp()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final getNOTIFICATION_COMPARATOR()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation

    .line 44
    invoke-static {}, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->access$getNOTIFICATION_COMPARATOR$cp()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final getNOTIFICATION_DISPLAY_TYPE_COMPARATOR()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/squareup/notificationcenterdata/Notification$DisplayType;",
            ">;"
        }
    .end annotation

    .line 34
    invoke-static {}, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->access$getNOTIFICATION_DISPLAY_TYPE_COMPARATOR$cp()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method

.method public final getOrderingValue(Lcom/squareup/communications/Message$Type;)I
    .locals 1

    const-string v0, "$this$orderingValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    sget-object v0, Lcom/squareup/communications/Message$Type$AlertHighPriority;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertHighPriority;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    .line 61
    :cond_0
    sget-object v0, Lcom/squareup/communications/Message$Type$AlertSetupPos;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertSetupPos;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x2

    goto :goto_0

    .line 62
    :cond_1
    sget-object v0, Lcom/squareup/communications/Message$Type$AlertResolveDispute;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertResolveDispute;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p1, 0x3

    goto :goto_0

    .line 63
    :cond_2
    sget-object v0, Lcom/squareup/communications/Message$Type$AlertToCompleteOrders;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertToCompleteOrders;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 p1, 0x4

    goto :goto_0

    .line 64
    :cond_3
    sget-object v0, Lcom/squareup/communications/Message$Type$AlertSupportCenter;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertSupportCenter;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 p1, 0x5

    goto :goto_0

    .line 65
    :cond_4
    sget-object v0, Lcom/squareup/communications/Message$Type$AlertCustomerComms;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertCustomerComms;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 p1, 0x6

    goto :goto_0

    .line 66
    :cond_5
    sget-object v0, Lcom/squareup/communications/Message$Type$MarketingToYourExistingCustomers;->INSTANCE:Lcom/squareup/communications/Message$Type$MarketingToYourExistingCustomers;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 p1, 0x7

    goto :goto_0

    .line 67
    :cond_6
    sget-object v0, Lcom/squareup/communications/Message$Type$MarketingToCustomersYouWantToAcquire;->INSTANCE:Lcom/squareup/communications/Message$Type$MarketingToCustomersYouWantToAcquire;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 p1, 0x8

    goto :goto_0

    .line 68
    :cond_7
    sget-object v0, Lcom/squareup/communications/Message$Type$NonUrgentBusinessNoticesForSellers;->INSTANCE:Lcom/squareup/communications/Message$Type$NonUrgentBusinessNoticesForSellers;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 p1, 0x9

    goto :goto_0

    .line 69
    :cond_8
    sget-object v0, Lcom/squareup/communications/Message$Type$Unsupported;->INSTANCE:Lcom/squareup/communications/Message$Type$Unsupported;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_9

    const/16 p1, 0x3e8

    :goto_0
    return p1

    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final getOrderingValue(Lcom/squareup/notificationcenterdata/Notification$DisplayType;)I
    .locals 1

    const-string v0, "$this$orderingValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    .line 55
    :cond_0
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 p1, 0x2

    :goto_0
    return p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public final getOrderingValue(Lcom/squareup/notificationcenterdata/Notification$State;)I
    .locals 3

    const-string v0, "$this$orderingValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    sget-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/notificationcenterdata/Notification$State;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x3

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eq p1, v2, :cond_2

    if-eq p1, v1, :cond_1

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 76
    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public final getREAD_STATE_COMPARATOR()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator<",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ">;"
        }
    .end annotation

    .line 42
    invoke-static {}, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->access$getREAD_STATE_COMPARATOR$cp()Ljava/util/Comparator;

    move-result-object v0

    return-object v0
.end method
