.class final Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DatabaseImpl.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->insertNotification(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/sqldelight/db/SqlPreparedStatement;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/sqldelight/db/SqlPreparedStatement;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $id:Ljava/lang/String;

.field final synthetic $source:Lcom/squareup/notificationcenterdata/Notification$Source;

.field final synthetic $state:Lcom/squareup/notificationcenterdata/Notification$State;

.field final synthetic this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;->this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

    iput-object p2, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;->$id:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;->$source:Lcom/squareup/notificationcenterdata/Notification$Source;

    iput-object p4, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;->$state:Lcom/squareup/notificationcenterdata/Notification$State;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lcom/squareup/sqldelight/db/SqlPreparedStatement;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;->invoke(Lcom/squareup/sqldelight/db/SqlPreparedStatement;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/sqldelight/db/SqlPreparedStatement;)V
    .locals 2

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;->$id:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-interface {p1, v1, v0}, Lcom/squareup/sqldelight/db/SqlPreparedStatement;->bindString(ILjava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;->this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

    invoke-static {v0}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->access$getDatabase$p(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;)Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;->getNotification_statesAdapter$impl_release()Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;->getSourceAdapter()Lcom/squareup/sqldelight/ColumnAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;->$source:Lcom/squareup/notificationcenterdata/Notification$Source;

    invoke-interface {v0, v1}, Lcom/squareup/sqldelight/ColumnAdapter;->encode(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x2

    invoke-interface {p1, v1, v0}, Lcom/squareup/sqldelight/db/SqlPreparedStatement;->bindString(ILjava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;->this$0:Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;

    invoke-static {v0}, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;->access$getDatabase$p(Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl;)Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;->getNotification_statesAdapter$impl_release()Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;->getStateAdapter()Lcom/squareup/sqldelight/ColumnAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/notificationcenterdata/impl/impl/NotificationStateQueriesImpl$insertNotification$1;->$state:Lcom/squareup/notificationcenterdata/Notification$State;

    invoke-interface {v0, v1}, Lcom/squareup/sqldelight/ColumnAdapter;->encode(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x3

    invoke-interface {p1, v1, v0}, Lcom/squareup/sqldelight/db/SqlPreparedStatement;->bindString(ILjava/lang/String;)V

    return-void
.end method
