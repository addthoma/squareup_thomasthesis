.class public final Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImplKt;
.super Ljava/lang/Object;
.source "DatabaseImpl.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\"\u0010\u0006\u001a\u00020\u0003*\u0008\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0000\"\u001e\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u000b"
    }
    d2 = {
        "schema",
        "Lcom/squareup/sqldelight/db/SqlDriver$Schema;",
        "Lkotlin/reflect/KClass;",
        "Lcom/squareup/notificationcenterdata/impl/Database;",
        "getSchema",
        "(Lkotlin/reflect/KClass;)Lcom/squareup/sqldelight/db/SqlDriver$Schema;",
        "newInstance",
        "driver",
        "Lcom/squareup/sqldelight/db/SqlDriver;",
        "notification_statesAdapter",
        "Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getSchema(Lkotlin/reflect/KClass;)Lcom/squareup/sqldelight/db/SqlDriver$Schema;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/KClass<",
            "Lcom/squareup/notificationcenterdata/impl/Database;",
            ">;)",
            "Lcom/squareup/sqldelight/db/SqlDriver$Schema;"
        }
    .end annotation

    const-string v0, "$this$schema"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    sget-object p0, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl$Schema;->INSTANCE:Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl$Schema;

    check-cast p0, Lcom/squareup/sqldelight/db/SqlDriver$Schema;

    return-object p0
.end method

.method public static final newInstance(Lkotlin/reflect/KClass;Lcom/squareup/sqldelight/db/SqlDriver;Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;)Lcom/squareup/notificationcenterdata/impl/Database;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/reflect/KClass<",
            "Lcom/squareup/notificationcenterdata/impl/Database;",
            ">;",
            "Lcom/squareup/sqldelight/db/SqlDriver;",
            "Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;",
            ")",
            "Lcom/squareup/notificationcenterdata/impl/Database;"
        }
    .end annotation

    const-string v0, "$this$newInstance"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "driver"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "notification_statesAdapter"

    invoke-static {p2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    new-instance p0, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;

    invoke-direct {p0, p1, p2}, Lcom/squareup/notificationcenterdata/impl/impl/DatabaseImpl;-><init>(Lcom/squareup/sqldelight/db/SqlDriver;Lcom/squareup/notificationcenterdata/db/Notification_states$Adapter;)V

    check-cast p0, Lcom/squareup/notificationcenterdata/impl/Database;

    return-object p0
.end method
