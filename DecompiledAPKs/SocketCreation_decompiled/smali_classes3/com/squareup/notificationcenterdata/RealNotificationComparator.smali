.class public final Lcom/squareup/notificationcenterdata/RealNotificationComparator;
.super Ljava/lang/Object;
.source "NotificationComparator.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenterdata/RealNotificationComparator$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/notificationcenterdata/Notification;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationComparator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationComparator.kt\ncom/squareup/notificationcenterdata/RealNotificationComparator\n*L\n1#1,80:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0018\u0000 \n2\u000c\u0012\u0004\u0012\u00020\u00020\u0001j\u0002`\u0003:\u0001\nB\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0004J)\u0010\u0005\u001a\u00020\u00062\u000e\u0010\u0007\u001a\n \u0008*\u0004\u0018\u00010\u00020\u00022\u000e\u0010\t\u001a\n \u0008*\u0004\u0018\u00010\u00020\u0002H\u0096\u0001\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/RealNotificationComparator;",
        "Ljava/util/Comparator;",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "Lcom/squareup/notificationcenterdata/NotificationComparator;",
        "()V",
        "compare",
        "",
        "p0",
        "kotlin.jvm.PlatformType",
        "p1",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/notificationcenterdata/RealNotificationComparator$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final MESSAGE_TYPE_PRIORITY_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/communications/Message$Type;",
            ">;"
        }
    .end annotation
.end field

.field private static final NOTIFICATION_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;"
        }
    .end annotation
.end field

.field private static final NOTIFICATION_DISPLAY_TYPE_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/notificationcenterdata/Notification$DisplayType;",
            ">;"
        }
    .end annotation
.end field

.field private static final READ_STATE_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final synthetic $$delegate_0:Ljava/util/Comparator;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/notificationcenterdata/RealNotificationComparator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->Companion:Lcom/squareup/notificationcenterdata/RealNotificationComparator$Companion;

    .line 34
    new-instance v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$compareBy$1;

    invoke-direct {v0}, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$compareBy$1;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    sput-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->NOTIFICATION_DISPLAY_TYPE_COMPARATOR:Ljava/util/Comparator;

    .line 40
    new-instance v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$compareBy$2;

    invoke-direct {v0}, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$compareBy$2;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    sput-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->MESSAGE_TYPE_PRIORITY_COMPARATOR:Ljava/util/Comparator;

    .line 42
    new-instance v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$compareBy$3;

    invoke-direct {v0}, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$compareBy$3;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    sput-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->READ_STATE_COMPARATOR:Ljava/util/Comparator;

    .line 45
    sget-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->NOTIFICATION_DISPLAY_TYPE_COMPARATOR:Ljava/util/Comparator;

    new-instance v1, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$compareBy$4;

    invoke-direct {v1, v0}, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$compareBy$4;-><init>(Ljava/util/Comparator;)V

    check-cast v1, Ljava/util/Comparator;

    .line 46
    sget-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->MESSAGE_TYPE_PRIORITY_COMPARATOR:Ljava/util/Comparator;

    new-instance v2, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$thenBy$1;

    invoke-direct {v2, v1, v0}, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$thenBy$1;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    check-cast v2, Ljava/util/Comparator;

    .line 47
    sget-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->READ_STATE_COMPARATOR:Ljava/util/Comparator;

    new-instance v1, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$thenBy$2;

    invoke-direct {v1, v2, v0}, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$thenBy$2;-><init>(Ljava/util/Comparator;Ljava/util/Comparator;)V

    check-cast v1, Ljava/util/Comparator;

    .line 48
    new-instance v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$thenByDescending$1;

    invoke-direct {v0, v1}, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$thenByDescending$1;-><init>(Ljava/util/Comparator;)V

    check-cast v0, Ljava/util/Comparator;

    .line 49
    new-instance v1, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$thenBy$3;

    invoke-direct {v1, v0}, Lcom/squareup/notificationcenterdata/RealNotificationComparator$$special$$inlined$thenBy$3;-><init>(Ljava/util/Comparator;)V

    check-cast v1, Ljava/util/Comparator;

    sput-object v1, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->NOTIFICATION_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->NOTIFICATION_COMPARATOR:Ljava/util/Comparator;

    iput-object v0, p0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->$$delegate_0:Ljava/util/Comparator;

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/squareup/notificationcenterdata/RealNotificationComparator$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->Companion:Lcom/squareup/notificationcenterdata/RealNotificationComparator$Companion;

    return-object v0
.end method

.method public static final synthetic access$getMESSAGE_TYPE_PRIORITY_COMPARATOR$cp()Ljava/util/Comparator;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->MESSAGE_TYPE_PRIORITY_COMPARATOR:Ljava/util/Comparator;

    return-object v0
.end method

.method public static final synthetic access$getNOTIFICATION_COMPARATOR$cp()Ljava/util/Comparator;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->NOTIFICATION_COMPARATOR:Ljava/util/Comparator;

    return-object v0
.end method

.method public static final synthetic access$getNOTIFICATION_DISPLAY_TYPE_COMPARATOR$cp()Ljava/util/Comparator;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->NOTIFICATION_DISPLAY_TYPE_COMPARATOR:Ljava/util/Comparator;

    return-object v0
.end method

.method public static final synthetic access$getREAD_STATE_COMPARATOR$cp()Ljava/util/Comparator;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->READ_STATE_COMPARATOR:Ljava/util/Comparator;

    return-object v0
.end method


# virtual methods
.method public compare(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenterdata/Notification;)I
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->$$delegate_0:Ljava/util/Comparator;

    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/notificationcenterdata/Notification;

    check-cast p2, Lcom/squareup/notificationcenterdata/Notification;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/notificationcenterdata/RealNotificationComparator;->compare(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenterdata/Notification;)I

    move-result p1

    return p1
.end method
