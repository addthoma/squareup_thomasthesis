.class public interface abstract Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;
.super Ljava/lang/Object;
.source "NotificationStateQueries.kt"

# interfaces
.implements Lcom/squareup/sqldelight/Transacter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u001e\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000c2\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J\u0016\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u000c2\u0006\u0010\u0008\u001a\u00020\tH&J\u001e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\t0\u000c2\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/notificationcenterdata/db/NotificationStateQueries;",
        "Lcom/squareup/sqldelight/Transacter;",
        "insertNotification",
        "",
        "id",
        "",
        "source",
        "Lcom/squareup/notificationcenterdata/Notification$Source;",
        "state",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "removeNotification",
        "selectIdWithSourceAndState",
        "Lcom/squareup/sqldelight/Query;",
        "selectIdWithState",
        "selectState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract insertNotification(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)V
.end method

.method public abstract removeNotification(Ljava/lang/String;)V
.end method

.method public abstract selectIdWithSourceAndState(Lcom/squareup/notificationcenterdata/Notification$Source;Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/sqldelight/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenterdata/Notification$Source;",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ")",
            "Lcom/squareup/sqldelight/Query<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract selectIdWithState(Lcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/sqldelight/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ")",
            "Lcom/squareup/sqldelight/Query<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract selectState(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;)Lcom/squareup/sqldelight/Query;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/notificationcenterdata/Notification$Source;",
            ")",
            "Lcom/squareup/sqldelight/Query<",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ">;"
        }
    .end annotation
.end method
