.class public final Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase_Factory;
.super Ljava/lang/Object;
.source "NotificationStateDatabase_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/sqldelight/db/SqlDriver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/sqldelight/db/SqlDriver;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/sqldelight/db/SqlDriver;",
            ">;)",
            "Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase_Factory;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/sqldelight/db/SqlDriver;)Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;

    invoke-direct {v0, p0}, Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;-><init>(Lcom/squareup/sqldelight/db/SqlDriver;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sqldelight/db/SqlDriver;

    invoke-static {v0}, Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase_Factory;->newInstance(Lcom/squareup/sqldelight/db/SqlDriver;)Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase_Factory;->get()Lcom/squareup/notificationcenterdata/db/NotificationStateDatabase;

    move-result-object v0

    return-object v0
.end method
