.class public final Lcom/squareup/money/PositiveNegativeFormatter_Factory;
.super Ljava/lang/Object;
.source "PositiveNegativeFormatter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/money/PositiveNegativeFormatter;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/money/PositiveNegativeFormatter_Factory;->baseFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/money/PositiveNegativeFormatter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)",
            "Lcom/squareup/money/PositiveNegativeFormatter_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/money/PositiveNegativeFormatter_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/money/PositiveNegativeFormatter_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/text/Formatter;)Lcom/squareup/money/PositiveNegativeFormatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/money/PositiveNegativeFormatter;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/money/PositiveNegativeFormatter;

    invoke-direct {v0, p0}, Lcom/squareup/money/PositiveNegativeFormatter;-><init>(Lcom/squareup/text/Formatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/money/PositiveNegativeFormatter;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/money/PositiveNegativeFormatter_Factory;->baseFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {v0}, Lcom/squareup/money/PositiveNegativeFormatter_Factory;->newInstance(Lcom/squareup/text/Formatter;)Lcom/squareup/money/PositiveNegativeFormatter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/money/PositiveNegativeFormatter_Factory;->get()Lcom/squareup/money/PositiveNegativeFormatter;

    move-result-object v0

    return-object v0
.end method
