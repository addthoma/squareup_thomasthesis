.class public final enum Lcom/squareup/money/CompactMoneyFormatter$Mode;
.super Ljava/lang/Enum;
.source "CompactMoneyFormatter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/money/CompactMoneyFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/money/CompactMoneyFormatter$Mode;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/money/CompactMoneyFormatter$Mode;",
        "",
        "(Ljava/lang/String;I)V",
        "minimumFractionDigitsWhenTruncated",
        "",
        "getMinimumFractionDigitsWhenTruncated",
        "()I",
        "DEFAULT",
        "SHORTER",
        "proto-utilities_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/money/CompactMoneyFormatter$Mode;

.field public static final enum DEFAULT:Lcom/squareup/money/CompactMoneyFormatter$Mode;

.field public static final enum SHORTER:Lcom/squareup/money/CompactMoneyFormatter$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/money/CompactMoneyFormatter$Mode;

    new-instance v1, Lcom/squareup/money/CompactMoneyFormatter$Mode;

    const/4 v2, 0x0

    const-string v3, "DEFAULT"

    invoke-direct {v1, v3, v2}, Lcom/squareup/money/CompactMoneyFormatter$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/money/CompactMoneyFormatter$Mode;->DEFAULT:Lcom/squareup/money/CompactMoneyFormatter$Mode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/money/CompactMoneyFormatter$Mode;

    const/4 v2, 0x1

    const-string v3, "SHORTER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/money/CompactMoneyFormatter$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/money/CompactMoneyFormatter$Mode;->SHORTER:Lcom/squareup/money/CompactMoneyFormatter$Mode;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/money/CompactMoneyFormatter$Mode;->$VALUES:[Lcom/squareup/money/CompactMoneyFormatter$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/money/CompactMoneyFormatter$Mode;
    .locals 1

    const-class v0, Lcom/squareup/money/CompactMoneyFormatter$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/money/CompactMoneyFormatter$Mode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/money/CompactMoneyFormatter$Mode;
    .locals 1

    sget-object v0, Lcom/squareup/money/CompactMoneyFormatter$Mode;->$VALUES:[Lcom/squareup/money/CompactMoneyFormatter$Mode;

    invoke-virtual {v0}, [Lcom/squareup/money/CompactMoneyFormatter$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/money/CompactMoneyFormatter$Mode;

    return-object v0
.end method


# virtual methods
.method public final getMinimumFractionDigitsWhenTruncated()I
    .locals 3

    .line 52
    sget-object v0, Lcom/squareup/money/CompactMoneyFormatter$Mode$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p0}, Lcom/squareup/money/CompactMoneyFormatter$Mode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    .line 54
    :cond_0
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    :cond_1
    :goto_0
    return v2
.end method
