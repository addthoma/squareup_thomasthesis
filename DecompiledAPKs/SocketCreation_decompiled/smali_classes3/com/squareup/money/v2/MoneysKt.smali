.class public final Lcom/squareup/money/v2/MoneysKt;
.super Ljava/lang/Object;
.source "Moneys.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0005\u001a\n\u0010\u0006\u001a\u00020\u0005*\u00020\u0004\u001a\n\u0010\u0007\u001a\u00020\u0008*\u00020\u0002\u001a\n\u0010\t\u001a\u00020\u0002*\u00020\u0008\u00a8\u0006\n"
    }
    d2 = {
        "isZero",
        "",
        "Lcom/squareup/protos/connect/v2/common/Money;",
        "toCurrency",
        "Lcom/squareup/protos/connect/v2/common/Currency;",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "toCurrencyCode",
        "toMoneyV1",
        "Lcom/squareup/protos/common/Money;",
        "toMoneyV2",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final isZero(Lcom/squareup/protos/connect/v2/common/Money;)Z
    .locals 4

    const-string v0, "$this$isZero"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long p0, v0, v2

    if-nez p0, :cond_1

    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x0

    :goto_1
    return p0
.end method

.method public static final toCurrency(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/connect/v2/common/Currency;
    .locals 1

    const-string v0, "$this$toCurrency"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->getValue()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/protos/connect/v2/common/Currency;->fromValue(I)Lcom/squareup/protos/connect/v2/common/Currency;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/protos/connect/v2/common/Currency;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/connect/v2/common/Currency;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public static final toCurrencyCode(Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    const-string v0, "$this$toCurrencyCode"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Currency;->getValue()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/protos/common/CurrencyCode;->fromValue(I)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Currency;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/protos/common/CurrencyCode;->valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public static final toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;
    .locals 2

    const-string v0, "$this$toMoneyV1"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance v0, Lcom/squareup/protos/common/Money$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/Money$Builder;-><init>()V

    .line 23
    iget-object v1, p0, Lcom/squareup/protos/connect/v2/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object v0

    .line 24
    iget-object p0, p0, Lcom/squareup/protos/connect/v2/common/Money;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    const-string v1, "currency"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/money/v2/MoneysKt;->toCurrencyCode(Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/common/Money$Builder;->currency_code(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money$Builder;

    move-result-object p0

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/common/Money$Builder;->build()Lcom/squareup/protos/common/Money;

    move-result-object p0

    const-string v0, "com.squareup.protos.comm\u2026ncyCode())\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toMoneyV2(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/connect/v2/common/Money;
    .locals 2

    const-string v0, "$this$toMoneyV2"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/squareup/protos/connect/v2/common/Money$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;-><init>()V

    .line 60
    iget-object v1, p0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->amount(Ljava/lang/Long;)Lcom/squareup/protos/connect/v2/common/Money$Builder;

    move-result-object v0

    .line 61
    iget-object p0, p0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v1, "currency_code"

    invoke-static {p0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/money/v2/MoneysKt;->toCurrency(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/connect/v2/common/Currency;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->currency(Lcom/squareup/protos/connect/v2/common/Currency;)Lcom/squareup/protos/connect/v2/common/Money$Builder;

    move-result-object p0

    .line 62
    invoke-virtual {p0}, Lcom/squareup/protos/connect/v2/common/Money$Builder;->build()Lcom/squareup/protos/connect/v2/common/Money;

    move-result-object p0

    const-string v0, "Money.Builder()\n      .a\u2026urrency())\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
