.class public final enum Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;
.super Ljava/lang/Enum;
.source "WholeUnitAmountScrubber.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/money/WholeUnitAmountScrubber;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ZeroState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

.field public static final enum BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

.field public static final enum BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

.field public static final enum NEVER_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

.field public static final enum NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 19
    new-instance v0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    const/4 v1, 0x0

    const-string v2, "BLANKABLE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    .line 21
    new-instance v0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    const/4 v2, 0x1

    const-string v3, "NOT_BLANKABLE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    .line 23
    new-instance v0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    const/4 v3, 0x2

    const-string v4, "BLANK_ON_ZERO"

    invoke-direct {v0, v4, v3}, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    .line 25
    new-instance v0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    const/4 v4, 0x3

    const-string v5, "NEVER_ZERO"

    invoke-direct {v0, v5, v4}, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NEVER_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    .line 17
    sget-object v5, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NOT_BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->NEVER_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->$VALUES:[Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;
    .locals 1

    .line 17
    const-class v0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->$VALUES:[Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v0}, [Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    return-object v0
.end method
