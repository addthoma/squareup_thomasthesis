.class public final synthetic Lcom/squareup/money/-$$Lambda$CurrencyMoneyModule$aMBV25vWYCTxVNqw5xZ4NOgLfeE;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/money/MoneyDigitsKeyListenerFactory;


# instance fields
.field private final synthetic f$0:Ljava/util/Locale;

.field private final synthetic f$1:Lcom/squareup/protos/common/CurrencyCode;


# direct methods
.method public synthetic constructor <init>(Ljava/util/Locale;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/money/-$$Lambda$CurrencyMoneyModule$aMBV25vWYCTxVNqw5xZ4NOgLfeE;->f$0:Ljava/util/Locale;

    iput-object p2, p0, Lcom/squareup/money/-$$Lambda$CurrencyMoneyModule$aMBV25vWYCTxVNqw5xZ4NOgLfeE;->f$1:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method


# virtual methods
.method public final getDigitsKeyListener(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;
    .locals 2

    iget-object v0, p0, Lcom/squareup/money/-$$Lambda$CurrencyMoneyModule$aMBV25vWYCTxVNqw5xZ4NOgLfeE;->f$0:Ljava/util/Locale;

    iget-object v1, p0, Lcom/squareup/money/-$$Lambda$CurrencyMoneyModule$aMBV25vWYCTxVNqw5xZ4NOgLfeE;->f$1:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, p1}, Lcom/squareup/money/CurrencyMoneyModule;->lambda$provideMoneyDigitsKeyListenerFactory$0(Ljava/util/Locale;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object p1

    return-object p1
.end method
