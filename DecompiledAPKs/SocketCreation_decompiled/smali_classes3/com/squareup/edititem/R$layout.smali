.class public final Lcom/squareup/edititem/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/edititem/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final discount_rule_row:I = 0x7f0d01df

.field public static final discount_rule_section_view:I = 0x7f0d01e0

.field public static final draggable_item_variation_row:I = 0x7f0d01e5

.field public static final draggable_service_variation_row:I = 0x7f0d01e6

.field public static final edit_discount_view:I = 0x7f0d01f6

.field public static final edit_discount_view_top:I = 0x7f0d01f7

.field public static final edit_discount_view_top_name:I = 0x7f0d01f8

.field public static final edit_discount_view_top_tag:I = 0x7f0d01f9

.field public static final edit_discount_view_top_value:I = 0x7f0d01fa

.field public static final edit_item_category_selection_list_row:I = 0x7f0d0201

.field public static final edit_item_category_selection_view:I = 0x7f0d0202

.field public static final edit_item_edit_details:I = 0x7f0d0203

.field public static final edit_item_item_option_and_values_row_content:I = 0x7f0d0204

.field public static final edit_item_label_top_view:I = 0x7f0d0205

.field public static final edit_item_label_view:I = 0x7f0d0206

.field public static final edit_item_main_view:I = 0x7f0d0207

.field public static final edit_item_main_view_single_variation_static_row:I = 0x7f0d0208

.field public static final edit_item_main_view_static_bottom:I = 0x7f0d0209

.field public static final edit_item_main_view_static_top:I = 0x7f0d020a

.field public static final edit_item_main_view_top_category:I = 0x7f0d020b

.field public static final edit_item_main_view_variation_input_fields:I = 0x7f0d020c

.field public static final edit_item_new_category_button:I = 0x7f0d020d

.field public static final edit_item_new_category_name_view:I = 0x7f0d020e

.field public static final edit_item_photo_view:I = 0x7f0d020f

.field public static final edit_item_prices_row:I = 0x7f0d0210

.field public static final edit_item_read_only_details:I = 0x7f0d0211

.field public static final edit_item_unit_selection_card:I = 0x7f0d0212

.field public static final edit_item_unit_selection_create_unit_row:I = 0x7f0d0213

.field public static final edit_item_unit_selection_recycler:I = 0x7f0d0214

.field public static final edit_item_unit_selection_sheet:I = 0x7f0d0215

.field public static final edit_item_variation:I = 0x7f0d0216

.field public static final edit_service_assigned_employees_view:I = 0x7f0d0221

.field public static final edit_service_main_view_single_variation_static_row:I = 0x7f0d0222

.field public static final edit_service_price_type_selection_view:I = 0x7f0d0223

.field public static final edit_service_variation:I = 0x7f0d0224

.field public static final legacy_item_variations_message_row:I = 0x7f0d0324

.field public static final stock_count_row:I = 0x7f0d0506


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
