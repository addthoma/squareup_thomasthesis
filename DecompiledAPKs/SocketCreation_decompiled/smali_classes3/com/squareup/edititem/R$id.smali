.class public final Lcom/squareup/edititem/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/edititem/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final assigned_employees:I = 0x7f0a01d0

.field public static final banner:I = 0x7f0a0217

.field public static final bookable_by_customers_online:I = 0x7f0a023c

.field public static final cancellation_fee_row:I = 0x7f0a02a0

.field public static final category_name:I = 0x7f0a02f7

.field public static final checkout_link:I = 0x7f0a031c

.field public static final checkout_link_container:I = 0x7f0a031d

.field public static final checkout_link_header:I = 0x7f0a031e

.field public static final checkout_link_settings_help_text:I = 0x7f0a0322

.field public static final checkout_link_share_help_text:I = 0x7f0a0323

.field public static final checkout_link_toggle:I = 0x7f0a0324

.field public static final chevron:I = 0x7f0a0329

.field public static final choose_photo:I = 0x7f0a0338

.field public static final color_grid:I = 0x7f0a0366

.field public static final container:I = 0x7f0a03a1

.field public static final create_variation_button:I = 0x7f0a03db

.field public static final dashboard_link:I = 0x7f0a0544

.field public static final date_range_section:I = 0x7f0a0554

.field public static final delete:I = 0x7f0a0560

.field public static final delete_button:I = 0x7f0a0561

.field public static final discount_amount:I = 0x7f0a05c4

.field public static final discount_bundle_list:I = 0x7f0a05c5

.field public static final discount_bundle_title:I = 0x7f0a05c6

.field public static final discount_delete:I = 0x7f0a05c7

.field public static final discount_icon:I = 0x7f0a05c8

.field public static final discount_modify_tax_basis:I = 0x7f0a05c9

.field public static final discount_name:I = 0x7f0a05ca

.field public static final discount_name_display:I = 0x7f0a05cb

.field public static final discount_options_section:I = 0x7f0a05cc

.field public static final discount_options_title:I = 0x7f0a05cd

.field public static final discount_percentage:I = 0x7f0a05ce

.field public static final discount_switch:I = 0x7f0a05cf

.field public static final discount_switch_amount:I = 0x7f0a05d0

.field public static final discount_switch_percentage:I = 0x7f0a05d1

.field public static final discount_tag_imageview:I = 0x7f0a05d2

.field public static final discount_text:I = 0x7f0a05d3

.field public static final discount_value_container:I = 0x7f0a05d4

.field public static final discounted_items_section:I = 0x7f0a05d5

.field public static final display_price_container:I = 0x7f0a05da

.field public static final display_price_row:I = 0x7f0a05db

.field public static final drag_handle:I = 0x7f0a05ef

.field public static final draggable_item_variation_row_drag_handle:I = 0x7f0a05f1

.field public static final draggable_item_variation_row_price:I = 0x7f0a05f2

.field public static final draggable_item_variation_row_price_sku:I = 0x7f0a05f3

.field public static final draggable_item_variation_row_sku:I = 0x7f0a05f4

.field public static final draggable_item_variation_row_stock_count:I = 0x7f0a05f5

.field public static final draggable_service_variation_row_duration:I = 0x7f0a05f6

.field public static final draggable_service_variation_row_price:I = 0x7f0a05f7

.field public static final draggable_variation_row_details_container:I = 0x7f0a05f8

.field public static final draggable_variation_row_name:I = 0x7f0a05f9

.field public static final draggable_variation_row_top_divider:I = 0x7f0a05fa

.field public static final edit_discount_container:I = 0x7f0a061b

.field public static final edit_discount_view:I = 0x7f0a061c

.field public static final edit_item_description:I = 0x7f0a0626

.field public static final edit_item_details_item_name:I = 0x7f0a0627

.field public static final edit_item_edit_details_section:I = 0x7f0a0628

.field public static final edit_item_item_option_and_values_row_phone_option_and_values_row:I = 0x7f0a0629

.field public static final edit_item_item_option_and_values_row_tablet_option_row:I = 0x7f0a062a

.field public static final edit_item_item_option_and_values_row_tablet_values_row:I = 0x7f0a062b

.field public static final edit_item_label_top_view:I = 0x7f0a062c

.field public static final edit_item_label_view:I = 0x7f0a062d

.field public static final edit_item_main_view_default_variation_container:I = 0x7f0a062e

.field public static final edit_item_main_view_errors_bar:I = 0x7f0a062f

.field public static final edit_item_main_view_item_name_and_category:I = 0x7f0a0630

.field public static final edit_item_main_view_manage_option_button:I = 0x7f0a0631

.field public static final edit_item_main_view_manage_option_help_text:I = 0x7f0a0632

.field public static final edit_item_main_view_option_section:I = 0x7f0a0633

.field public static final edit_item_main_view_options_container:I = 0x7f0a0634

.field public static final edit_item_main_view_price_input_field:I = 0x7f0a0635

.field public static final edit_item_main_view_price_sku_container:I = 0x7f0a0636

.field public static final edit_item_main_view_recycler_view:I = 0x7f0a0637

.field public static final edit_item_main_view_sku_input_field:I = 0x7f0a0638

.field public static final edit_item_main_view_stock_count_row:I = 0x7f0a0639

.field public static final edit_item_main_view_unit_type:I = 0x7f0a063a

.field public static final edit_item_main_view_unit_type_help_text:I = 0x7f0a063b

.field public static final edit_item_main_view_variation_overlay:I = 0x7f0a063c

.field public static final edit_item_price_help_text:I = 0x7f0a063d

.field public static final edit_item_read_only_details_section:I = 0x7f0a063e

.field public static final edit_item_save_button:I = 0x7f0a063f

.field public static final edit_item_unit_type_selection:I = 0x7f0a0640

.field public static final edit_item_variation_errors_bar:I = 0x7f0a0641

.field public static final edit_item_variation_help_text:I = 0x7f0a0642

.field public static final edit_item_variation_name_input_field:I = 0x7f0a0643

.field public static final edit_item_variation_name_readonly_field:I = 0x7f0a0644

.field public static final edit_item_variation_price_input_field:I = 0x7f0a0645

.field public static final edit_item_variation_remove_variation_button:I = 0x7f0a0646

.field public static final edit_item_variation_sku_input_field:I = 0x7f0a0647

.field public static final edit_item_variation_stock_count_row:I = 0x7f0a0648

.field public static final edit_item_variation_unit_type:I = 0x7f0a0649

.field public static final edit_item_variation_unit_type_help_text:I = 0x7f0a064a

.field public static final edit_item_variation_view:I = 0x7f0a064b

.field public static final edit_label_color_header:I = 0x7f0a064c

.field public static final edit_label_image_tile:I = 0x7f0a064d

.field public static final edit_label_text_tile:I = 0x7f0a064e

.field public static final edit_service_after_appointment_row:I = 0x7f0a0661

.field public static final edit_service_duration_extra_time_toggle:I = 0x7f0a0662

.field public static final edit_service_duration_row:I = 0x7f0a0663

.field public static final edit_service_final_duration:I = 0x7f0a0664

.field public static final edit_service_gap_duration:I = 0x7f0a0665

.field public static final edit_service_gap_time_toggle:I = 0x7f0a0666

.field public static final edit_service_initial_duration:I = 0x7f0a0667

.field public static final edit_service_main_view_default_variation_container:I = 0x7f0a0668

.field public static final edit_service_price_type_helper_text:I = 0x7f0a0669

.field public static final edit_service_variation_after_appointment_row:I = 0x7f0a066a

.field public static final edit_service_variation_assigned_employees:I = 0x7f0a066b

.field public static final edit_service_variation_bookable_by_customers_online:I = 0x7f0a066c

.field public static final edit_service_variation_cancellation_fee_row:I = 0x7f0a066d

.field public static final edit_service_variation_display_price_container:I = 0x7f0a066e

.field public static final edit_service_variation_display_price_row:I = 0x7f0a066f

.field public static final edit_service_variation_duration_extra_time_toggle:I = 0x7f0a0670

.field public static final edit_service_variation_duration_row:I = 0x7f0a0671

.field public static final edit_service_variation_extra_time_helper_text:I = 0x7f0a0672

.field public static final edit_service_variation_final_duration:I = 0x7f0a0673

.field public static final edit_service_variation_gap_duration:I = 0x7f0a0674

.field public static final edit_service_variation_gap_time_toggle:I = 0x7f0a0675

.field public static final edit_service_variation_initial_duration:I = 0x7f0a0676

.field public static final edit_service_variation_name_input_field:I = 0x7f0a0677

.field public static final edit_service_variation_price_input_field:I = 0x7f0a0678

.field public static final edit_service_variation_remove_variation_button:I = 0x7f0a0679

.field public static final edit_service_variation_unit_type:I = 0x7f0a067a

.field public static final edit_service_variaton_price_type_row:I = 0x7f0a067b

.field public static final employees_group:I = 0x7f0a06f1

.field public static final extra_time_helper_text:I = 0x7f0a073d

.field public static final fixed_button:I = 0x7f0a075f

.field public static final image_tile_help_text:I = 0x7f0a0826

.field public static final item_category:I = 0x7f0a08cf

.field public static final item_category_selection_list:I = 0x7f0a08d0

.field public static final item_editing_read_only_category:I = 0x7f0a08d3

.field public static final item_editing_read_only_description:I = 0x7f0a08d4

.field public static final item_editing_read_only_name:I = 0x7f0a08d5

.field public static final item_editing_save_spinner:I = 0x7f0a08d6

.field public static final item_editing_save_spinner_text:I = 0x7f0a08d7

.field public static final item_image_tile:I = 0x7f0a08d9

.field public static final item_text_tile:I = 0x7f0a08e2

.field public static final item_variation_message:I = 0x7f0a08e5

.field public static final items_section:I = 0x7f0a08ec

.field public static final modifier_list_container:I = 0x7f0a09e5

.field public static final modifier_list_header:I = 0x7f0a09e6

.field public static final name:I = 0x7f0a0a0c

.field public static final new_category_button:I = 0x7f0a0a1c

.field public static final new_category_name_view:I = 0x7f0a0a1d

.field public static final photo_action_view:I = 0x7f0a0c28

.field public static final photo_label_header:I = 0x7f0a0c29

.field public static final price:I = 0x7f0a0c4f

.field public static final price_row:I = 0x7f0a0c55

.field public static final price_type_group:I = 0x7f0a0c56

.field public static final price_type_row:I = 0x7f0a0c57

.field public static final pricing_rule_help_text:I = 0x7f0a0c58

.field public static final read_only_details_header:I = 0x7f0a0ce3

.field public static final rowKey:I = 0x7f0a0d9f

.field public static final rowValue:I = 0x7f0a0da0

.field public static final save_spinner_container:I = 0x7f0a0e06

.field public static final schedule_section:I = 0x7f0a0e09

.field public static final selectable_unit_list:I = 0x7f0a0e59

.field public static final stock_count_row_label:I = 0x7f0a0f3b

.field public static final stock_count_row_progress_spinner:I = 0x7f0a0f3c

.field public static final stock_count_row_stock_field:I = 0x7f0a0f3d

.field public static final stock_count_row_view_stock_count_field:I = 0x7f0a0f3e

.field public static final take_photo:I = 0x7f0a0f6e

.field public static final tax_basis_help_text:I = 0x7f0a0f75

.field public static final taxes_container:I = 0x7f0a0f7f

.field public static final taxes_header:I = 0x7f0a0f80

.field public static final tile_help_text:I = 0x7f0a0fdd

.field public static final unit_number_limit_help_text:I = 0x7f0a10b3

.field public static final unit_selection_create_unit_button:I = 0x7f0a10b4

.field public static final unit_selection_help_text:I = 0x7f0a10b5

.field public static final unit_type_row:I = 0x7f0a10b6

.field public static final variable_button:I = 0x7f0a10e3

.field public static final variation_header:I = 0x7f0a10e4


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
