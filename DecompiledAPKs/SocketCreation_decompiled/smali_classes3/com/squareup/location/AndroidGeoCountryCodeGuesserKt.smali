.class public final Lcom/squareup/location/AndroidGeoCountryCodeGuesserKt;
.super Ljava/lang/Object;
.source "AndroidGeoCountryCodeGuesser.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAndroidGeoCountryCodeGuesser.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AndroidGeoCountryCodeGuesser.kt\ncom/squareup/location/AndroidGeoCountryCodeGuesserKt\n*L\n1#1,94:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000e\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u0002H\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "toResult",
        "Lcom/squareup/location/CountryGuesser$Result;",
        "Lcom/squareup/CountryCode;",
        "impl-android-geo_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$toResult(Lcom/squareup/CountryCode;)Lcom/squareup/location/CountryGuesser$Result;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/location/AndroidGeoCountryCodeGuesserKt;->toResult(Lcom/squareup/CountryCode;)Lcom/squareup/location/CountryGuesser$Result;

    move-result-object p0

    return-object p0
.end method

.method private static final toResult(Lcom/squareup/CountryCode;)Lcom/squareup/location/CountryGuesser$Result;
    .locals 1

    if-eqz p0, :cond_0

    .line 93
    new-instance v0, Lcom/squareup/location/CountryGuesser$Result$Country;

    iget-boolean p0, p0, Lcom/squareup/CountryCode;->hasPayments:Z

    invoke-direct {v0, p0}, Lcom/squareup/location/CountryGuesser$Result$Country;-><init>(Z)V

    check-cast v0, Lcom/squareup/location/CountryGuesser$Result;

    goto :goto_0

    :cond_0
    sget-object p0, Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;->INSTANCE:Lcom/squareup/location/CountryGuesser$Result$NoSupportedCountryDetected;

    move-object v0, p0

    check-cast v0, Lcom/squareup/location/CountryGuesser$Result;

    :goto_0
    return-object v0
.end method
