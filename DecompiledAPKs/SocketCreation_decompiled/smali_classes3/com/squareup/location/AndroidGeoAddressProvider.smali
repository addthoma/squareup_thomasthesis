.class public final Lcom/squareup/location/AndroidGeoAddressProvider;
.super Ljava/lang/Object;
.source "AndroidGeoAddressProvider.kt"

# interfaces
.implements Lcom/squareup/core/location/providers/AddressProvider;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0007\u0018\u00002\u00020\u0001B\u0019\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0008\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J\u0018\u0010\r\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\n0\t2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J$\u0010\u0010\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\n0\t2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000c2\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u000fH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/location/AndroidGeoAddressProvider;",
        "Lcom/squareup/core/location/providers/AddressProvider;",
        "app",
        "Landroid/app/Application;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Landroid/app/Application;Lio/reactivex/Scheduler;)V",
        "geocoderScheduler",
        "getAddress",
        "Lio/reactivex/Single;",
        "Lcom/squareup/core/location/providers/AddressProvider$Result;",
        "location",
        "Landroid/location/Location;",
        "getAddressFromName",
        "name",
        "",
        "getAddressImpl",
        "locationName",
        "impl-android-geo_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final app:Landroid/app/Application;

.field private final geocoderScheduler:Lio/reactivex/Scheduler;

.field private final mainScheduler:Lio/reactivex/Scheduler;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "app"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/location/AndroidGeoAddressProvider;->app:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/location/AndroidGeoAddressProvider;->mainScheduler:Lio/reactivex/Scheduler;

    const-string p1, "geocoder-provider"

    .line 34
    invoke-static {p1}, Lcom/squareup/thread/executor/Executors;->newSingleThreadExecutor(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/Executor;

    invoke-static {p1}, Lio/reactivex/schedulers/Schedulers;->from(Ljava/util/concurrent/Executor;)Lio/reactivex/Scheduler;

    move-result-object p1

    const-string p2, "Schedulers.from(newSingl\u2026tor(\"geocoder-provider\"))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/location/AndroidGeoAddressProvider;->geocoderScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method private final getAddressImpl(Landroid/location/Location;Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/Location;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/core/location/providers/AddressProvider$Result;",
            ">;"
        }
    .end annotation

    .line 44
    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object p1, Lcom/squareup/core/location/providers/AddressProvider$Result$NotResolved;->INSTANCE:Lcom/squareup/core/location/providers/AddressProvider$Result$NotResolved;

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.just(NotResolved)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 46
    :cond_0
    new-instance v0, Landroid/location/Geocoder;

    iget-object v1, p0, Lcom/squareup/location/AndroidGeoAddressProvider;->app:Landroid/app/Application;

    check-cast v1, Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 47
    new-instance v1, Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;

    invoke-direct {v1, p1, v0, p2}, Lcom/squareup/location/AndroidGeoAddressProvider$getAddressImpl$1;-><init>(Landroid/location/Location;Landroid/location/Geocoder;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v1}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object p1

    .line 63
    invoke-virtual {p1}, Lio/reactivex/Single;->cache()Lio/reactivex/Single;

    move-result-object p1

    .line 64
    iget-object p2, p0, Lcom/squareup/location/AndroidGeoAddressProvider;->geocoderScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    .line 65
    iget-object p2, p0, Lcom/squareup/location/AndroidGeoAddressProvider;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.create<Result> { \u2026.observeOn(mainScheduler)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public getAddress(Landroid/location/Location;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/Location;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/core/location/providers/AddressProvider$Result;",
            ">;"
        }
    .end annotation

    const-string v0, "location"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, p1, v0}, Lcom/squareup/location/AndroidGeoAddressProvider;->getAddressImpl(Landroid/location/Location;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getAddressFromName(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/core/location/providers/AddressProvider$Result;",
            ">;"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, v0, p1}, Lcom/squareup/location/AndroidGeoAddressProvider;->getAddressImpl(Landroid/location/Location;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
