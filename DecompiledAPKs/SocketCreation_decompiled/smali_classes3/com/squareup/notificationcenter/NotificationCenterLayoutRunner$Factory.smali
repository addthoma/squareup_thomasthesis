.class public final Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$Factory;
.super Ljava/lang/Object;
.source "NotificationCenterLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$Factory;",
        "",
        "recyclerFactory",
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;",
        "(Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;)V",
        "create",
        "Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;",
        "view",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final recyclerFactory:Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;


# direct methods
.method public constructor <init>(Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "recyclerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$Factory;->recyclerFactory:Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;

    return-void
.end method


# virtual methods
.method public final create(Landroid/view/View;)Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance v0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;

    .line 54
    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$Factory;->recyclerFactory:Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;

    .line 52
    invoke-direct {v0, p1, v1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;)V

    return-object v0
.end method
