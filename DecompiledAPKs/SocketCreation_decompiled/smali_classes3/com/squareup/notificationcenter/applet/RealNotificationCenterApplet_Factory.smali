.class public final Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;
.super Ljava/lang/Object;
.source "RealNotificationCenterApplet_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/NotificationCenterScreens;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/NotificationCenterScreens;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p5, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;->arg4Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notificationcenter/NotificationCenterScreens;",
            ">;)",
            "Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;"
        }
    .end annotation

    .line 49
    new-instance v6, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;Lcom/squareup/notificationcenter/NotificationCenterScreens;)Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;",
            "Lcom/squareup/notificationcenter/NotificationCenterScreens;",
            ")",
            "Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;"
        }
    .end annotation

    .line 54
    new-instance v6, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;-><init>(Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;Lcom/squareup/notificationcenter/NotificationCenterScreens;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;
    .locals 5

    .line 42
    iget-object v0, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v2, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/reactivex/Scheduler;

    iget-object v3, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;

    iget-object v4, p0, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/notificationcenter/NotificationCenterScreens;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;->newInstance(Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lio/reactivex/Scheduler;Lcom/squareup/notificationcenterbadge/NotificationCenterBadge;Lcom/squareup/notificationcenter/NotificationCenterScreens;)Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet_Factory;->get()Lcom/squareup/notificationcenter/applet/RealNotificationCenterApplet;

    move-result-object v0

    return-object v0
.end method
