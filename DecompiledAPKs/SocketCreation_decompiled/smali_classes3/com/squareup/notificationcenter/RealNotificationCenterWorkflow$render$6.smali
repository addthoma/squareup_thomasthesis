.class final Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$6;
.super Lkotlin/jvm/internal/Lambda;
.source "RealNotificationCenterWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->render(Lkotlin/Unit;Lcom/squareup/notificationcenter/NotificationCenterState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/notificationcenter/NotificationCenterState;",
        "+",
        "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/notificationcenter/NotificationCenterState;",
        "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
        "output",
        "Lcom/squareup/notificationcenter/OpenInBrowserDialogOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/notificationcenter/NotificationCenterState;

.field final synthetic this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;Lcom/squareup/notificationcenter/NotificationCenterState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$6;->this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    iput-object p2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$6;->$state:Lcom/squareup/notificationcenter/NotificationCenterState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/notificationcenter/OpenInBrowserDialogOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenter/OpenInBrowserDialogOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/notificationcenter/NotificationCenterState;",
            "Lcom/squareup/notificationcenter/NotificationCenterOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    instance-of v0, p1, Lcom/squareup/notificationcenter/OpenInBrowserDialogOutput$Canceled;

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;

    .line 116
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$6;->$state:Lcom/squareup/notificationcenter/NotificationCenterState;

    move-object v1, v0

    check-cast v1, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    .line 117
    check-cast v0, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    invoke-virtual {v0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getSelectedExternalNotification()Lcom/squareup/notificationcenterdata/Notification;

    move-result-object v0

    .line 118
    iget-object v2, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$6;->this$0:Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;

    invoke-static {v2}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;->access$getAnalytics$p(Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow;)Lcom/squareup/notificationcenter/NotificationCenterAnalytics;

    move-result-object v2

    .line 115
    invoke-direct {p1, v1, v0, v2}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$CloseBrowserDialog;-><init>(Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterAnalytics;)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 120
    :cond_0
    instance-of p1, p1, Lcom/squareup/notificationcenter/OpenInBrowserDialogOutput$OpenedInBrowser;

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenExternalNotification;

    .line 121
    iget-object v0, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$6;->$state:Lcom/squareup/notificationcenter/NotificationCenterState;

    check-cast v0, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    invoke-virtual {v0}, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;->getSelectedExternalNotification()Lcom/squareup/notificationcenterdata/Notification;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$6;->$state:Lcom/squareup/notificationcenter/NotificationCenterState;

    check-cast v1, Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;

    .line 120
    invoke-direct {p1, v0, v1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$Action$OpenExternalNotification;-><init>(Lcom/squareup/notificationcenterdata/Notification;Lcom/squareup/notificationcenter/NotificationCenterState$ShowingOpenInBrowserDialogState;)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/notificationcenter/OpenInBrowserDialogOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/notificationcenter/RealNotificationCenterWorkflow$render$6;->invoke(Lcom/squareup/notificationcenter/OpenInBrowserDialogOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
