.class public final Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory_Factory;
.super Ljava/lang/Object;
.source "NotificationCenterRecyclerFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;)",
            "Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory_Factory;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/recycler/RecyclerFactory;)Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;-><init>(Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/recycler/RecyclerFactory;

    invoke-static {v0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory_Factory;->newInstance(Lcom/squareup/recycler/RecyclerFactory;)Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory_Factory;->get()Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;

    move-result-object v0

    return-object v0
.end method
