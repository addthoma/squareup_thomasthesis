.class public final Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;
.super Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;
.source "RealNotificationCenterAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AccountView"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0010\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0007H\u00c6\u0003J1\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00032\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0007H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\r\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;",
        "Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;",
        "has_unread_notification",
        "",
        "notification_count",
        "",
        "notifications",
        "",
        "last_notified_at",
        "(ZILjava/lang/String;Ljava/lang/String;)V",
        "getHas_unread_notification",
        "()Z",
        "getLast_notified_at",
        "()Ljava/lang/String;",
        "getNotification_count",
        "()I",
        "getNotifications",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final has_unread_notification:Z

.field private final last_notified_at:Ljava/lang/String;

.field private final notification_count:I

.field private final notifications:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string v0, "notifications"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "last_notified_at"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->NOTIFICATION_CENTER_ACCOUNT_VIEW:Lcom/squareup/analytics/RegisterActionName;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action;-><init>(Lcom/squareup/analytics/RegisterActionName;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->has_unread_notification:Z

    iput p2, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notification_count:I

    iput-object p3, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notifications:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->last_notified_at:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;ZILjava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-boolean p1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->has_unread_notification:Z

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget p2, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notification_count:I

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notifications:Ljava/lang/String;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->last_notified_at:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->copy(ZILjava/lang/String;Ljava/lang/String;)Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->has_unread_notification:Z

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notification_count:I

    return v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notifications:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->last_notified_at:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(ZILjava/lang/String;Ljava/lang/String;)Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;
    .locals 1

    const-string v0, "notifications"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "last_notified_at"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;-><init>(ZILjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->has_unread_notification:Z

    iget-boolean v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->has_unread_notification:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notification_count:I

    iget v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notification_count:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notifications:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notifications:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->last_notified_at:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->last_notified_at:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHas_unread_notification()Z
    .locals 1

    .line 173
    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->has_unread_notification:Z

    return v0
.end method

.method public final getLast_notified_at()Ljava/lang/String;
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->last_notified_at:Ljava/lang/String;

    return-object v0
.end method

.method public final getNotification_count()I
    .locals 1

    .line 174
    iget v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notification_count:I

    return v0
.end method

.method public final getNotifications()Ljava/lang/String;
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notifications:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->has_unread_notification:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notification_count:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notifications:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->last_notified_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AccountView(has_unread_notification="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->has_unread_notification:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", notification_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notification_count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", notifications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->notifications:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", last_notified_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/notificationcenter/NotificationCenterAnalyticsEvent$Action$AccountView;->last_notified_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
