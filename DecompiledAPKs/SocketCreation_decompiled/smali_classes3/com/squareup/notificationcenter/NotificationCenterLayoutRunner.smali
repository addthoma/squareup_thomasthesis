.class public final Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;
.super Ljava/lang/Object;
.source "NotificationCenterLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$Factory;,
        Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/notificationcenter/NotificationCenterScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationCenterLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationCenterLayoutRunner.kt\ncom/squareup/notificationcenter/NotificationCenterLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,258:1\n1103#2,7:259\n1103#2,7:266\n1360#3:273\n1429#3,3:274\n*E\n*S KotlinDebug\n*F\n+ 1 NotificationCenterLayoutRunner.kt\ncom/squareup/notificationcenter/NotificationCenterLayoutRunner\n*L\n127#1,7:259\n128#1,7:266\n168#1:273\n168#1,3:274\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002-.B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J0\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00182\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u00182\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001d0\u001cH\u0002J\u0018\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020\u00022\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010\"\u001a\u00020\u001d2\u0006\u0010#\u001a\u00020\u0002H\u0002J\u0010\u0010$\u001a\u00020\u001d2\u0006\u0010%\u001a\u00020&H\u0002J\u0010\u0010\'\u001a\u00020\u001d2\u0006\u0010(\u001a\u00020)H\u0002J*\u0010*\u001a\u00020\u001d2\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u00182\u0012\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u001a\u0012\u0004\u0012\u00020\u001d0\u001cH\u0002J$\u0010+\u001a\u00020\u001d2\u0006\u0010(\u001a\u00020)2\u0012\u0010,\u001a\u000e\u0012\u0004\u0012\u00020)\u0012\u0004\u0012\u00020\u001d0\u001cH\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/notificationcenter/NotificationCenterScreen;",
        "view",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;)V",
        "accountTab",
        "Lcom/squareup/noho/NohoLabel;",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "contentAnimator",
        "Lcom/squareup/widgets/SquareViewAnimator;",
        "errorDescription",
        "errorTitle",
        "loadingAnimator",
        "noNotificationsMessageView",
        "Lcom/squareup/noho/NohoMessageView;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
        "whatsNewTab",
        "createNotificationRowsFromNotifications",
        "",
        "notifications",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "onNotificationClicked",
        "Lkotlin/Function1;",
        "",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "screen",
        "updateErrorBanner",
        "showError",
        "",
        "updateNoNotificationsMessageView",
        "selectedTab",
        "Lcom/squareup/notificationcenter/NotificationCenterTab;",
        "updateRecycler",
        "updateTabs",
        "onTabSelected",
        "Binding",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountTab:Lcom/squareup/noho/NohoLabel;

.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final contentAnimator:Lcom/squareup/widgets/SquareViewAnimator;

.field private final errorDescription:Lcom/squareup/noho/NohoLabel;

.field private final errorTitle:Lcom/squareup/noho/NohoLabel;

.field private final loadingAnimator:Lcom/squareup/widgets/SquareViewAnimator;

.field private final noNotificationsMessageView:Lcom/squareup/noho/NohoMessageView;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;

.field private final view:Landroid/view/View;

.field private final whatsNewTab:Lcom/squareup/noho/NohoLabel;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->recyclerFactory:Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;

    .line 67
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_action_bar:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.n\u2026cation_center_action_bar)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 69
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_loading_animator:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.n\u2026_center_loading_animator)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->loadingAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 71
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_content_animator:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.n\u2026_center_content_animator)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->contentAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 73
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_no_notifications_message_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.n\u2026tifications_message_view)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoMessageView;

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->noNotificationsMessageView:Lcom/squareup/noho/NohoMessageView;

    .line 74
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_account_button:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.n\u2026on_center_account_button)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->accountTab:Lcom/squareup/noho/NohoLabel;

    .line 75
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_whats_new_button:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.n\u2026_center_whats_new_button)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->whatsNewTab:Lcom/squareup/noho/NohoLabel;

    .line 77
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->recyclerFactory:Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;

    iget-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_recycler_view:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string/jumbo v0, "view.findViewById(R.id.n\u2026ion_center_recycler_view)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, p2}, Lcom/squareup/notificationcenter/ui/NotificationCenterRecyclerFactory;->create(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    .line 78
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_error_title:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.n\u2026ation_center_error_title)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->errorTitle:Lcom/squareup/noho/NohoLabel;

    .line 80
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/notificationcenter/impl/R$id;->notification_center_error_description:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo p2, "view.findViewById(R.id.n\u2026center_error_description)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->errorDescription:Lcom/squareup/noho/NohoLabel;

    return-void
.end method

.method public static final synthetic access$createNotificationRowsFromNotifications(Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;Ljava/util/List;Lkotlin/jvm/functions/Function1;)Ljava/util/List;
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->createNotificationRowsFromNotifications(Ljava/util/List;Lkotlin/jvm/functions/Function1;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private final createNotificationRowsFromNotifications(Ljava/util/List;Lkotlin/jvm/functions/Function1;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/notificationcenterdata/Notification;",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenter/ui/NotificationCenterRow;",
            ">;"
        }
    .end annotation

    .line 168
    check-cast p1, Ljava/lang/Iterable;

    .line 273
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 274
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 275
    check-cast v1, Lcom/squareup/notificationcenterdata/Notification;

    .line 169
    invoke-virtual {v1}, Lcom/squareup/notificationcenterdata/Notification;->getDisplayType()Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    move-result-object v2

    .line 170
    sget-object v3, Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$WarningBanner;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v2, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$WarningBannerRow;

    .line 171
    invoke-virtual {v1}, Lcom/squareup/notificationcenterdata/Notification;->getTitle()Ljava/lang/String;

    move-result-object v5

    .line 172
    invoke-virtual {v1}, Lcom/squareup/notificationcenterdata/Notification;->getContent()Ljava/lang/String;

    move-result-object v6

    .line 173
    invoke-virtual {v1}, Lcom/squareup/notificationcenterdata/Notification;->getMessageType()Lcom/squareup/communications/Message$Type;

    move-result-object v3

    sget-object v4, Lcom/squareup/communications/Message$Type$AlertSetupPos;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertSetupPos;

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 174
    sget v3, Lcom/squareup/notificationcenter/impl/R$drawable;->icon_checklist_40:I

    goto :goto_1

    .line 176
    :cond_0
    sget v3, Lcom/squareup/notificationcenter/impl/R$drawable;->icon_triangle_warning_40:I

    :goto_1
    move v7, v3

    .line 178
    invoke-virtual {v1}, Lcom/squareup/notificationcenterdata/Notification;->getMessageType()Lcom/squareup/communications/Message$Type;

    move-result-object v3

    sget-object v4, Lcom/squareup/communications/Message$Type$AlertSetupPos;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertSetupPos;

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 179
    sget v3, Lcom/squareup/notificationcenter/impl/R$style;->WarningBannerRowTitleAppearance:I

    goto :goto_2

    .line 181
    :cond_1
    sget v3, Lcom/squareup/notificationcenter/impl/R$style;->WarningBannerRowTitleAlertAppearance:I

    :goto_2
    move v8, v3

    .line 183
    new-instance v3, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$createNotificationRowsFromNotifications$$inlined$map$lambda$1;

    invoke-direct {v3, v1, p2}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$createNotificationRowsFromNotifications$$inlined$map$lambda$1;-><init>(Lcom/squareup/notificationcenterdata/Notification;Lkotlin/jvm/functions/Function1;)V

    move-object v9, v3

    check-cast v9, Lkotlin/jvm/functions/Function1;

    move-object v4, v2

    .line 170
    invoke-direct/range {v4 .. v9}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$WarningBannerRow;-><init>(Ljava/lang/String;Ljava/lang/String;IILkotlin/jvm/functions/Function1;)V

    check-cast v2, Lcom/squareup/notificationcenter/ui/NotificationCenterRow;

    goto :goto_7

    .line 185
    :cond_2
    sget-object v3, Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 186
    invoke-virtual {v1}, Lcom/squareup/notificationcenterdata/Notification;->getState()Lcom/squareup/notificationcenterdata/Notification$State;

    move-result-object v2

    sget-object v3, Lcom/squareup/notificationcenterdata/Notification$State;->READ:Lcom/squareup/notificationcenterdata/Notification$State;

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    .line 187
    :goto_3
    new-instance v11, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;

    .line 188
    invoke-virtual {v1}, Lcom/squareup/notificationcenterdata/Notification;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 189
    invoke-virtual {v1}, Lcom/squareup/notificationcenterdata/Notification;->getContent()Ljava/lang/String;

    move-result-object v5

    if-eqz v2, :cond_4

    .line 191
    sget v3, Lcom/squareup/notificationcenter/impl/R$color;->read_notification_text_color:I

    goto :goto_4

    .line 193
    :cond_4
    sget v3, Lcom/squareup/notificationcenter/impl/R$color;->unread_notification_text_color:I

    :goto_4
    move v6, v3

    if-eqz v2, :cond_5

    .line 196
    sget v3, Lcom/squareup/notificationcenter/impl/R$drawable;->read_notification_dot:I

    goto :goto_5

    .line 198
    :cond_5
    sget v3, Lcom/squareup/notificationcenter/impl/R$drawable;->unread_notification_dot:I

    :goto_5
    move v7, v3

    if-eqz v2, :cond_6

    .line 201
    sget v2, Lcom/squareup/notificationcenter/impl/R$string;->read_notification_dot_content_description:I

    goto :goto_6

    .line 203
    :cond_6
    sget v2, Lcom/squareup/notificationcenter/impl/R$string;->unread_notification_dot_content_description:I

    :goto_6
    move v8, v2

    .line 205
    invoke-static {v1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunnerKt;->access$getShowOpenInBrowserIcon$p(Lcom/squareup/notificationcenterdata/Notification;)Z

    move-result v9

    .line 206
    new-instance v2, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$createNotificationRowsFromNotifications$$inlined$map$lambda$2;

    invoke-direct {v2, v1, p2}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$createNotificationRowsFromNotifications$$inlined$map$lambda$2;-><init>(Lcom/squareup/notificationcenterdata/Notification;Lkotlin/jvm/functions/Function1;)V

    move-object v10, v2

    check-cast v10, Lkotlin/jvm/functions/Function1;

    move-object v3, v11

    .line 187
    invoke-direct/range {v3 .. v10}, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$NotificationRow;-><init>(Ljava/lang/String;Ljava/lang/String;IIIZLkotlin/jvm/functions/Function1;)V

    move-object v2, v11

    check-cast v2, Lcom/squareup/notificationcenter/ui/NotificationCenterRow;

    .line 209
    :goto_7
    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 187
    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 276
    :cond_8
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    .line 210
    sget-object p1, Lcom/squareup/notificationcenter/ui/NotificationCenterRow$PaddingRow;->INSTANCE:Lcom/squareup/notificationcenter/ui/NotificationCenterRow$PaddingRow;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private final updateActionBar(Lcom/squareup/notificationcenter/NotificationCenterScreen;)V
    .locals 4

    .line 120
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 115
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 117
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/notificationcenter/impl/R$string;->notification_center_applet_name:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 118
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateActionBar$$inlined$apply$lambda$1;

    invoke-direct {v3, p1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/notificationcenter/NotificationCenterScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 120
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateErrorBanner(Z)V
    .locals 1

    .line 148
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->errorTitle:Lcom/squareup/noho/NohoLabel;

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 149
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->errorDescription:Lcom/squareup/noho/NohoLabel;

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final updateNoNotificationsMessageView(Lcom/squareup/notificationcenter/NotificationCenterTab;)V
    .locals 1

    .line 154
    sget-object v0, Lcom/squareup/notificationcenter/NotificationCenterTab$AccountTab;->INSTANCE:Lcom/squareup/notificationcenter/NotificationCenterTab$AccountTab;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->noNotificationsMessageView:Lcom/squareup/noho/NohoMessageView;

    sget v0, Lcom/squareup/notificationcenter/impl/R$string;->no_account_notifications_view_title:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 156
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->noNotificationsMessageView:Lcom/squareup/noho/NohoMessageView;

    sget v0, Lcom/squareup/notificationcenter/impl/R$string;->no_account_notifications_view_message:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    goto :goto_0

    .line 158
    :cond_0
    sget-object v0, Lcom/squareup/notificationcenter/NotificationCenterTab$WhatsNewTab;->INSTANCE:Lcom/squareup/notificationcenter/NotificationCenterTab$WhatsNewTab;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 159
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->noNotificationsMessageView:Lcom/squareup/noho/NohoMessageView;

    sget v0, Lcom/squareup/notificationcenter/impl/R$string;->no_whats_new_notifications_view_title:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 160
    iget-object p1, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->noNotificationsMessageView:Lcom/squareup/noho/NohoMessageView;

    sget v0, Lcom/squareup/notificationcenter/impl/R$string;->no_whats_new_notifications_view_message:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final updateRecycler(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/notificationcenterdata/Notification;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/notificationcenterdata/Notification;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 138
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    new-instance v1, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateRecycler$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateRecycler$1;-><init>(Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;Ljava/util/List;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final updateTabs(Lcom/squareup/notificationcenter/NotificationCenterTab;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/notificationcenter/NotificationCenterTab;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->accountTab:Lcom/squareup/noho/NohoLabel;

    check-cast v0, Landroid/view/View;

    .line 259
    new-instance v1, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateTabs$$inlined$onClickDebounced$1;

    invoke-direct {v1, p2}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateTabs$$inlined$onClickDebounced$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->whatsNewTab:Lcom/squareup/noho/NohoLabel;

    check-cast v0, Landroid/view/View;

    .line 266
    new-instance v1, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateTabs$$inlined$onClickDebounced$2;

    invoke-direct {v1, p2}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$updateTabs$$inlined$onClickDebounced$2;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->accountTab:Lcom/squareup/noho/NohoLabel;

    sget-object v0, Lcom/squareup/notificationcenter/NotificationCenterTab$AccountTab;->INSTANCE:Lcom/squareup/notificationcenter/NotificationCenterTab$AccountTab;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {p2, v0}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunnerKt;->access$setSelectionStyle(Lcom/squareup/noho/NohoLabel;Z)V

    .line 131
    iget-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->whatsNewTab:Lcom/squareup/noho/NohoLabel;

    sget-object v0, Lcom/squareup/notificationcenter/NotificationCenterTab$WhatsNewTab;->INSTANCE:Lcom/squareup/notificationcenter/NotificationCenterTab$WhatsNewTab;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    invoke-static {p2, p1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunnerKt;->access$setSelectionStyle(Lcom/squareup/noho/NohoLabel;Z)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/notificationcenter/NotificationCenterScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 2

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-direct {p0, p1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->updateActionBar(Lcom/squareup/notificationcenter/NotificationCenterScreen;)V

    .line 87
    iget-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->loadingAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    invoke-static {p1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunnerKt;->access$getLoadingAnimatorViewId$p(Lcom/squareup/notificationcenter/NotificationCenterScreen;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 88
    iget-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->contentAnimator:Lcom/squareup/widgets/SquareViewAnimator;

    invoke-static {p1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunnerKt;->access$getContentAnimatorViewId$p(Lcom/squareup/notificationcenter/NotificationCenterScreen;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 91
    instance-of p2, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    if-eqz p2, :cond_0

    .line 92
    move-object p2, p1

    check-cast p2, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->getNotifications()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->getOnNotificationClicked()Lkotlin/jvm/functions/Function1;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->updateRecycler(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V

    .line 93
    invoke-virtual {p2}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->getShowError()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->updateErrorBanner(Z)V

    .line 94
    invoke-virtual {p2}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingNotificationsScreen;->getOnTabSelected()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, v0, p2}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->updateTabs(Lcom/squareup/notificationcenter/NotificationCenterTab;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 96
    :cond_0
    instance-of p2, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;

    if-eqz p2, :cond_1

    .line 97
    move-object p2, p1

    check-cast p2, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->getNotifications()Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$showRendering$1;->INSTANCE:Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$showRendering$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, v0, v1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->updateRecycler(Ljava/util/List;Lkotlin/jvm/functions/Function1;)V

    .line 98
    invoke-virtual {p2}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->getShowError()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->updateErrorBanner(Z)V

    .line 99
    invoke-virtual {p2}, Lcom/squareup/notificationcenter/NotificationCenterScreen$ShowingOpenInBrowserDialogScreen;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object p2

    sget-object v0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$showRendering$2;->INSTANCE:Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$showRendering$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p0, p2, v0}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->updateTabs(Lcom/squareup/notificationcenter/NotificationCenterTab;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 101
    :cond_1
    instance-of p2, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;

    if-eqz p2, :cond_2

    .line 102
    move-object p2, p1

    check-cast p2, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->updateNoNotificationsMessageView(Lcom/squareup/notificationcenter/NotificationCenterTab;)V

    .line 103
    invoke-virtual {p2}, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;->getShowError()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->updateErrorBanner(Z)V

    .line 104
    invoke-virtual {p2}, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;->getSelectedTab()Lcom/squareup/notificationcenter/NotificationCenterTab;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/notificationcenter/NotificationCenterScreen$NoNotificationsScreen;->getOnTabSelected()Lkotlin/jvm/functions/Function1;

    move-result-object p2

    invoke-direct {p0, v0, p2}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->updateTabs(Lcom/squareup/notificationcenter/NotificationCenterTab;Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 106
    :cond_2
    instance-of p2, p1, Lcom/squareup/notificationcenter/NotificationCenterScreen$LoadingScreen;

    if-eqz p2, :cond_3

    const/4 p2, 0x0

    invoke-direct {p0, p2}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->updateErrorBanner(Z)V

    .line 109
    :cond_3
    :goto_0
    iget-object p2, p0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$showRendering$3;

    invoke-direct {v0, p1}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner$showRendering$3;-><init>(Lcom/squareup/notificationcenter/NotificationCenterScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 44
    check-cast p1, Lcom/squareup/notificationcenter/NotificationCenterScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/notificationcenter/NotificationCenterLayoutRunner;->showRendering(Lcom/squareup/notificationcenter/NotificationCenterScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
