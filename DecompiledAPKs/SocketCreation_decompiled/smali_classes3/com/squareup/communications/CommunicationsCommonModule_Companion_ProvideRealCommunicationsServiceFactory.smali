.class public final Lcom/squareup/communications/CommunicationsCommonModule_Companion_ProvideRealCommunicationsServiceFactory;
.super Ljava/lang/Object;
.source "CommunicationsCommonModule_Companion_ProvideRealCommunicationsServiceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/communications/service/CommunicationsService;",
        ">;"
    }
.end annotation


# instance fields
.field private final serviceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/communications/CommunicationsCommonModule_Companion_ProvideRealCommunicationsServiceFactory;->serviceCreatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/communications/CommunicationsCommonModule_Companion_ProvideRealCommunicationsServiceFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;)",
            "Lcom/squareup/communications/CommunicationsCommonModule_Companion_ProvideRealCommunicationsServiceFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/communications/CommunicationsCommonModule_Companion_ProvideRealCommunicationsServiceFactory;

    invoke-direct {v0, p0}, Lcom/squareup/communications/CommunicationsCommonModule_Companion_ProvideRealCommunicationsServiceFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideRealCommunicationsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/communications/service/CommunicationsService;
    .locals 1

    .line 38
    sget-object v0, Lcom/squareup/communications/CommunicationsCommonModule;->Companion:Lcom/squareup/communications/CommunicationsCommonModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/communications/CommunicationsCommonModule$Companion;->provideRealCommunicationsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/communications/service/CommunicationsService;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/communications/service/CommunicationsService;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/communications/service/CommunicationsService;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/communications/CommunicationsCommonModule_Companion_ProvideRealCommunicationsServiceFactory;->serviceCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ServiceCreator;

    invoke-static {v0}, Lcom/squareup/communications/CommunicationsCommonModule_Companion_ProvideRealCommunicationsServiceFactory;->provideRealCommunicationsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/communications/service/CommunicationsService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/communications/CommunicationsCommonModule_Companion_ProvideRealCommunicationsServiceFactory;->get()Lcom/squareup/communications/service/CommunicationsService;

    move-result-object v0

    return-object v0
.end method
