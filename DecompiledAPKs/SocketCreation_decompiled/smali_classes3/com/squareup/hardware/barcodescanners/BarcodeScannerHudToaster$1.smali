.class Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster$1;
.super Ljava/lang/Object;
.source "BarcodeScannerHudToaster.java"

# interfaces
.implements Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;-><init>(Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;

.field final synthetic val$analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method constructor <init>(Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 40
    iput-object p1, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster$1;->this$0:Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;

    iput-object p2, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster$1;->val$analytics:Lcom/squareup/analytics/Analytics;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public barcodeScannerConnected(Lcom/squareup/barcodescanners/BarcodeScanner;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster$1;->this$0:Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;

    invoke-virtual {v0}, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->showBarcodeScannerConnected()V

    .line 43
    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster$1;->val$analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;->forBarcodeScannerConnected(Lcom/squareup/barcodescanners/BarcodeScanner;)Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method public barcodeScannerDisconnected(Lcom/squareup/barcodescanners/BarcodeScanner;)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster$1;->this$0:Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;

    invoke-virtual {v0}, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster;->showBarcodeScannerDisconnected()V

    .line 48
    iget-object v0, p0, Lcom/squareup/hardware/barcodescanners/BarcodeScannerHudToaster$1;->val$analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;->forBarcodeScannerDisconnected(Lcom/squareup/barcodescanners/BarcodeScanner;)Lcom/squareup/hardware/barcodescanners/BarcodeScannerEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method
