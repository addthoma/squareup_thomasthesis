.class public Lcom/squareup/marin/widgets/MarinGlyphTextView;
.super Lcom/squareup/marketfont/MarketTextView;
.source "MarinGlyphTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/marin/widgets/MarinGlyphTextView$GlyphPosition;
    }
.end annotation


# static fields
.field public static final BOTTOM:I = 0x3

.field public static final LEFT:I = 0x0

.field public static final RIGHT:I = 0x2

.field public static final TOP:I = 0x1


# instance fields
.field private chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

.field private shadowColor:I

.field private shadowDx:I

.field private shadowDy:I

.field private shadowRadius:I

.field private final textColorStateList:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 56
    sget v0, Lcom/squareup/marin/R$attr;->marinGlyphTextViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 62
    sget-object v0, Lcom/squareup/marin/R$styleable;->MarinGlyphTextView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 64
    sget p2, Lcom/squareup/marin/R$styleable;->MarinGlyphTextView_glyphPosition:I

    invoke-virtual {p1, p2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    .line 65
    sget p3, Lcom/squareup/marin/R$styleable;->MarinGlyphTextView_android_textColor:I

    .line 66
    invoke-virtual {p1, p3}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p3

    .line 68
    sget v0, Lcom/squareup/marin/R$styleable;->MarinGlyphTextView_glyphShadowRadius:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->shadowRadius:I

    .line 69
    sget v0, Lcom/squareup/marin/R$styleable;->MarinGlyphTextView_glyphShadowDx:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->shadowDx:I

    .line 70
    sget v0, Lcom/squareup/marin/R$styleable;->MarinGlyphTextView_glyphShadowDy:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->shadowDy:I

    .line 71
    sget v0, Lcom/squareup/marin/R$styleable;->MarinGlyphTextView_glyphShadowColor:I

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->shadowColor:I

    .line 73
    sget v0, Lcom/squareup/marin/R$styleable;->MarinGlyphTextView_chevronColor:I

    .line 74
    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setChevronColor(Landroid/content/res/ColorStateList;)V

    .line 79
    :cond_0
    sget v0, Lcom/squareup/marin/R$styleable;->MarinGlyphTextView_chevronVisibility:I

    sget-object v1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 80
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ChevronVisibility;->ordinal()I

    move-result v1

    .line 79
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 81
    invoke-static {}, Lcom/squareup/marin/widgets/ChevronVisibility;->values()[Lcom/squareup/marin/widgets/ChevronVisibility;

    move-result-object v1

    aget-object v0, v1, v0

    .line 82
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    if-nez p3, :cond_1

    const/high16 p3, -0x10000

    .line 86
    invoke-static {p3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object p3

    .line 88
    :cond_1
    iput-object p3, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->textColorStateList:Landroid/content/res/ColorStateList;

    .line 89
    sget v0, Lcom/squareup/marin/R$styleable;->MarinGlyphTextView_glyph:I

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->ALL_GLYPHS:Ljava/util/List;

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/util/Views;->getEnum(Landroid/content/res/TypedArray;ILjava/util/List;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz v0, :cond_2

    .line 91
    invoke-virtual {p0, v0, p2, p3}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;ILandroid/content/res/ColorStateList;)V

    .line 93
    :cond_2
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private refreshChevron()V
    .locals 3

    .line 154
    sget-object v0, Lcom/squareup/marin/widgets/MarinGlyphTextView$1;->$SwitchMap$com$squareup$marin$widgets$ChevronVisibility:[I

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ChevronVisibility;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-eq v0, v1, :cond_3

    if-eq v0, v2, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 162
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_text_selector_medium_gray:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 163
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setChevronColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 166
    :cond_0
    invoke-virtual {p0, v2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->removeGlyph(I)V

    goto :goto_0

    .line 170
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized chevronVisibility attribute "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_text_selector_medium_gray:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setChevronColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 156
    :cond_3
    invoke-virtual {p0, v2}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->removeGlyph(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final removeAllGlyphs()V
    .locals 1

    const/4 v0, 0x0

    .line 150
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final removeGlyph(I)V
    .locals 1

    const/4 v0, 0x0

    .line 146
    invoke-virtual {p0, v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;I)V

    return-void
.end method

.method public setChevronColor(Landroid/content/res/ColorStateList;)V
    .locals 2

    .line 142
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RIGHT_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;ILandroid/content/res/ColorStateList;)V

    return-void
.end method

.method public setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 138
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->refreshChevron()V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 0

    .line 97
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketTextView;->setEnabled(Z)V

    .line 98
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->refreshChevron()V

    return-void
.end method

.method public final setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;I)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->textColorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, p1, p2, v0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;ILandroid/content/res/ColorStateList;)V

    return-void
.end method

.method public final setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;ILandroid/content/res/ColorStateList;)V
    .locals 8

    .line 116
    iget v4, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->shadowColor:I

    iget v5, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->shadowDx:I

    iget v6, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->shadowDy:I

    iget v7, p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;->shadowRadius:I

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v7}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;ILandroid/content/res/ColorStateList;IIII)V

    return-void
.end method

.method public final setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;ILandroid/content/res/ColorStateList;IIII)V
    .locals 2

    if-eqz p1, :cond_1

    .line 124
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p1

    invoke-virtual {p1, p3}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorStateList(Landroid/content/res/ColorStateList;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object p1

    const/4 p3, -0x1

    if-eq p4, p3, :cond_0

    int-to-float p3, p5

    int-to-float p5, p6

    .line 126
    invoke-virtual {p1, p7, p3, p5, p4}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->shadow(IFFI)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    .line 128
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 131
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object p3

    .line 132
    aput-object p1, p3, p2

    const/4 p1, 0x0

    .line 133
    aget-object p1, p3, p1

    const/4 p2, 0x1

    aget-object p2, p3, p2

    const/4 p4, 0x2

    aget-object p4, p3, p4

    const/4 p5, 0x3

    aget-object p3, p3, p5

    invoke-virtual {p0, p1, p2, p4, p3}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1

    .line 104
    sget-object v0, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    if-ne p2, v0, :cond_0

    instance-of v0, p1, Landroid/text/SpannedString;

    if-eqz v0, :cond_0

    .line 105
    sget-object p2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    .line 107
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    return-void
.end method
