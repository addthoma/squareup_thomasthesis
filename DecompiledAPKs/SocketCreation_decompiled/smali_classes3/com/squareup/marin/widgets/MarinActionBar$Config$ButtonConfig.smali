.class public Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;
.super Ljava/lang/Object;
.source "MarinActionBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/widgets/MarinActionBar$Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ButtonConfig"
.end annotation


# instance fields
.field public final backgroundColor:I

.field public final command:Ljava/lang/Runnable;

.field public final enabled:Z

.field public final glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field public final text:Ljava/lang/CharSequence;

.field public final textColor:I

.field public final tooltip:Ljava/lang/CharSequence;

.field public final visible:Z


# direct methods
.method public constructor <init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)V
    .locals 1

    .line 505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 506
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$000(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 507
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$100(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->text:Ljava/lang/CharSequence;

    .line 508
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$200(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)I

    move-result v0

    iput v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->textColor:I

    .line 509
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$300(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->tooltip:Ljava/lang/CharSequence;

    .line 510
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$400(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->command:Ljava/lang/Runnable;

    .line 511
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$500(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->enabled:Z

    .line 512
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$600(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->visible:Z

    .line 513
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;->access$700(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$ButtonConfigBuilder;)I

    move-result p1

    iput p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$ButtonConfig;->backgroundColor:I

    return-void
.end method
