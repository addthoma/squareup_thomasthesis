.class public Lcom/squareup/marin/widgets/MarinSwitch;
.super Landroid/widget/CompoundButton;
.source "MarinSwitch.java"

# interfaces
.implements Lcom/squareup/widgets/list/AnimatableSwitch;


# static fields
.field private static final CHECKED_STATE_SET:[I

.field private static final TOUCH_MODE_DOWN:I = 0x1

.field private static final TOUCH_MODE_DRAGGING:I = 0x2

.field private static final TOUCH_MODE_IDLE:I


# instance fields
.field private animator:Landroid/animation/ValueAnimator;

.field private ballBackgroundPaint:Landroid/graphics/Paint;

.field private final ballDimen:I

.field private ballDynamicFillRadius:F

.field private ballFillPaint:Landroid/graphics/Paint;

.field private final ballInnerRadius:I

.field private final ballInnerRadiusWithStroke:I

.field private ballPosition:F

.field private ballPositionAnimated:F

.field private ballStrokePaint:Landroid/graphics/Paint;

.field private currentTouchState:I

.field private focusOffset:I

.field private final focusRadius:I

.field private focusedBallBackgroundPaint:Landroid/graphics/Paint;

.field private focusedBallColor:I

.field private focusedBallStrokeColor:I

.field private focusedBallStrokePaint:Landroid/graphics/Paint;

.field private mMinFlingVelocity:I

.field private mTouchSlop:I

.field private mTouchX:F

.field private mTouchY:F

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private offBallColor:I

.field private offBallStrokeColor:I

.field private offTrackColor:I

.field private onBallColor:I

.field private onBallStrokeColor:I

.field private onTrackColor:I

.field private final strokeWidth:I

.field private switchBottom:I

.field private switchLeft:I

.field private switchRight:I

.field private switchTop:I

.field private switchWidth:I

.field private trackPaint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 84
    sget v1, Lcom/squareup/widgets/R$attr;->state_checked:I

    const/4 v2, 0x0

    aput v1, v0, v2

    sput-object v0, Lcom/squareup/marin/widgets/MarinSwitch;->CHECKED_STATE_SET:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 90
    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinSwitch;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 94
    invoke-direct {p0, p1, p2}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/high16 p2, 0x7fc00000    # Float.NaN

    .line 52
    iput p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPositionAnimated:F

    .line 96
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 97
    sget v0, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->setMinimumHeight(I)V

    .line 98
    sget v0, Lcom/squareup/marin/R$dimen;->marin_lollipop_switch_width:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 99
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->setMinWidth(I)V

    .line 100
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->setMaxWidth(I)V

    const/4 v0, 0x1

    .line 102
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->setClickable(Z)V

    .line 103
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->setFocusable(Z)V

    .line 105
    sget v1, Lcom/squareup/marin/R$dimen;->marin_lollipop_switch_stroke:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->strokeWidth:I

    .line 106
    sget v1, Lcom/squareup/marin/R$dimen;->marin_lollipop_focus_radius:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusRadius:I

    .line 107
    sget v1, Lcom/squareup/marin/R$dimen;->marin_lollipop_focus_offset:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusOffset:I

    .line 108
    sget v1, Lcom/squareup/marin/R$dimen;->marin_lollipop_ball_radius_inner:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballInnerRadius:I

    .line 109
    sget v1, Lcom/squareup/marin/R$dimen;->marin_lollipop_ball_radius_inner_with_stroke:I

    .line 110
    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballInnerRadiusWithStroke:I

    .line 111
    sget v1, Lcom/squareup/marin/R$dimen;->marin_lollipop_ball_size:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballDimen:I

    const/4 v1, 0x0

    .line 113
    invoke-virtual {p0, v1}, Lcom/squareup/marin/widgets/MarinSwitch;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v1, 0x0

    .line 114
    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/squareup/marin/widgets/MarinSwitch;->setPadding(IIII)V

    .line 116
    sget v1, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->offTrackColor:I

    .line 117
    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->onTrackColor:I

    .line 119
    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->offBallColor:I

    .line 120
    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->onBallColor:I

    .line 122
    sget v1, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->offBallStrokeColor:I

    .line 123
    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->onBallStrokeColor:I

    .line 125
    sget v1, Lcom/squareup/marin/R$color;->marin_ultra_light_gray:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallColor:I

    .line 126
    sget v1, Lcom/squareup/marin/R$color;->marin_ultra_light_gray_pressed:I

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallStrokeColor:I

    .line 128
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    .line 129
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result p2

    iput p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mTouchSlop:I

    .line 130
    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result p1

    iput p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mMinFlingVelocity:I

    .line 132
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->trackPaint:Landroid/graphics/Paint;

    .line 133
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->trackPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setFlags(I)V

    .line 134
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->trackPaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 135
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->trackPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->isChecked()Z

    move-result p2

    if-eqz p2, :cond_0

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->onTrackColor:I

    goto :goto_0

    :cond_0
    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->offTrackColor:I

    :goto_0
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 136
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->trackPaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->strokeWidth:I

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 137
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->trackPaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 139
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballFillPaint:Landroid/graphics/Paint;

    .line 140
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballFillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setFlags(I)V

    .line 141
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballFillPaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->onBallColor:I

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 142
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballFillPaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 144
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballBackgroundPaint:Landroid/graphics/Paint;

    .line 145
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setFlags(I)V

    .line 146
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballBackgroundPaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->offBallColor:I

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 147
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballBackgroundPaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 149
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballStrokePaint:Landroid/graphics/Paint;

    .line 150
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setFlags(I)V

    .line 151
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballStrokePaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->strokeWidth:I

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 152
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->isChecked()Z

    move-result p2

    if-eqz p2, :cond_1

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->onBallStrokeColor:I

    goto :goto_1

    :cond_1
    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->offBallStrokeColor:I

    :goto_1
    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 153
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballStrokePaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 155
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallBackgroundPaint:Landroid/graphics/Paint;

    .line 156
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setFlags(I)V

    .line 157
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallBackgroundPaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallColor:I

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 158
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallBackgroundPaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 160
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallStrokePaint:Landroid/graphics/Paint;

    .line 161
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setFlags(I)V

    .line 162
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallStrokePaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->strokeWidth:I

    int-to-float p2, p2

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 163
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallStrokePaint:Landroid/graphics/Paint;

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallStrokeColor:I

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 164
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallStrokePaint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    return-void
.end method

.method static synthetic access$002(Lcom/squareup/marin/widgets/MarinSwitch;F)F
    .locals 0

    .line 37
    iput p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPositionAnimated:F

    return p1
.end method

.method static synthetic access$102(Lcom/squareup/marin/widgets/MarinSwitch;F)F
    .locals 0

    .line 37
    iput p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    return p1
.end method

.method static synthetic access$200(Lcom/squareup/marin/widgets/MarinSwitch;)I
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getThumbScrollRange()I

    move-result p0

    return p0
.end method

.method private cancelSuperTouch(Landroid/view/MotionEvent;)V
    .locals 1

    .line 262
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object p1

    const/4 v0, 0x3

    .line 263
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    .line 264
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 265
    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    return-void
.end method

.method private getTargetCheckedState()Z
    .locals 2

    .line 332
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getThumbScrollRange()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private getThumbScrollRange()I
    .locals 2

    .line 412
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchWidth:I

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballDimen:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusOffset:I

    sub-int/2addr v0, v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private hitThumb(FF)Z
    .locals 8

    .line 186
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchBottom:I

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchTop:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 187
    iget v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballDimen:I

    div-int/lit8 v2, v1, 0x2

    sub-int v2, v0, v2

    iget v3, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusOffset:I

    sub-int/2addr v2, v3

    iget v4, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mTouchSlop:I

    sub-int/2addr v2, v4

    .line 188
    div-int/lit8 v5, v1, 0x2

    add-int/2addr v0, v5

    add-int/2addr v0, v3

    add-int/2addr v0, v4

    .line 190
    iget v5, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchLeft:I

    add-int/2addr v5, v3

    int-to-float v5, v5

    iget v6, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    add-float/2addr v5, v6

    div-int/lit8 v6, v1, 0x2

    int-to-float v6, v6

    add-float/2addr v5, v6

    float-to-int v5, v5

    sub-int v6, v5, v3

    .line 191
    div-int/lit8 v7, v1, 0x2

    sub-int/2addr v6, v7

    sub-int/2addr v6, v4

    .line 192
    div-int/lit8 v1, v1, 0x2

    add-int/2addr v5, v1

    add-int/2addr v5, v3

    add-int/2addr v5, v4

    int-to-float v1, v6

    cmpl-float v1, p1, v1

    if-lez v1, :cond_0

    int-to-float v1, v5

    cmpg-float p1, p1, v1

    if-gez p1, :cond_0

    int-to-float p1, v2

    cmpl-float p1, p2, p1

    if-lez p1, :cond_0

    int-to-float p1, v0

    cmpg-float p1, p2, p1

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private setBallForPosition()V
    .locals 4

    .line 381
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getThumbScrollRange()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    const v1, 0x3e19999a    # 0.15f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 385
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballStrokePaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->offTrackColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    :cond_0
    const v1, 0x3f59999a    # 0.85f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 388
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballStrokePaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->onTrackColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 390
    :cond_1
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballStrokePaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->offBallStrokeColor:I

    iget v3, p0, Lcom/squareup/marin/widgets/MarinSwitch;->onBallStrokeColor:I

    invoke-static {v0, v2, v3}, Lcom/squareup/util/Colors;->evaluateColor(FII)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 393
    :goto_0
    iget v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballInnerRadius:I

    int-to-float v1, v1

    mul-float v0, v0, v1

    iput v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballDynamicFillRadius:F

    return-void
.end method

.method private setTrackForPosition()V
    .locals 4

    .line 397
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getThumbScrollRange()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    const v1, 0x3e19999a    # 0.15f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 401
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->trackPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->offTrackColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    :cond_0
    const v1, 0x3f59999a    # 0.85f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    .line 404
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->trackPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->onTrackColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 407
    :cond_1
    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->trackPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->offTrackColor:I

    iget v3, p0, Lcom/squareup/marin/widgets/MarinSwitch;->onTrackColor:I

    invoke-static {v0, v2, v3}, Lcom/squareup/util/Colors;->evaluateColor(FII)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    :goto_0
    return-void
.end method

.method private stopDrag(Landroid/view/MotionEvent;)V
    .locals 4

    const/4 v0, 0x0

    .line 274
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->currentTouchState:I

    .line 276
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 278
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinSwitch;->cancelSuperTouch(Landroid/view/MotionEvent;)V

    if-eqz v1, :cond_3

    .line 282
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    invoke-virtual {p1, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 283
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result p1

    .line 284
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mMinFlingVelocity:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    const/4 v1, 0x0

    cmpl-float p1, p1, v1

    if-lez p1, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    .line 287
    :cond_1
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getTargetCheckedState()Z

    move-result v0

    .line 289
    :cond_2
    :goto_1
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->setChecked(Z)V

    goto :goto_2

    .line 291
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->isChecked()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinSwitch;->setChecked(Z)V

    :goto_2
    return-void
.end method

.method private updateCheckedState(ZZ)V
    .locals 4

    .line 297
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->isChecked()Z

    move-result v0

    if-eq v0, p1, :cond_0

    return-void

    .line 301
    :cond_0
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    .line 302
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getThumbScrollRange()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    move p1, v1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 305
    :goto_0
    iput p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    .line 306
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->invalidate()V

    cmpl-float v3, v0, p1

    if-eqz v3, :cond_4

    cmpg-float v3, v0, v2

    if-ltz v3, :cond_4

    cmpg-float v3, p1, v2

    if-ltz v3, :cond_4

    cmpl-float v2, v1, v2

    if-nez v2, :cond_2

    goto :goto_1

    :cond_2
    if-nez p2, :cond_3

    return-void

    :cond_3
    const/4 p2, 0x2

    new-array p2, p2, [F

    const/4 v2, 0x0

    div-float/2addr v0, v1

    aput v0, p2, v2

    const/4 v0, 0x1

    div-float/2addr p1, v1

    aput p1, p2, v0

    .line 318
    invoke-static {p2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->animator:Landroid/animation/ValueAnimator;

    .line 319
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->animator:Landroid/animation/ValueAnimator;

    new-instance p2, Lcom/squareup/marin/widgets/MarinSwitch$1;

    invoke-direct {p2, p0}, Lcom/squareup/marin/widgets/MarinSwitch$1;-><init>(Lcom/squareup/marin/widgets/MarinSwitch;)V

    invoke-virtual {p1, p2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 327
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const/high16 v0, 0x10e0000

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p2

    int-to-long v0, p2

    invoke-virtual {p1, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 328
    iget-object p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->animator:Landroid/animation/ValueAnimator;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    :cond_4
    :goto_1
    return-void
.end method


# virtual methods
.method protected drawBall(Landroid/graphics/Canvas;II)V
    .locals 2

    .line 374
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->setBallForPosition()V

    int-to-float p3, p3

    int-to-float p2, p2

    .line 375
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballInnerRadius:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p3, p2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 376
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballDynamicFillRadius:F

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballFillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p3, p2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 377
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballInnerRadiusWithStroke:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p3, p2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected drawBallFocus(Landroid/graphics/Canvas;II)V
    .locals 2

    int-to-float p3, p3

    int-to-float p2, p2

    .line 369
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusRadius:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p3, p2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 370
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusRadius:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusedBallStrokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p3, p2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected drawTrack(Landroid/graphics/Canvas;I)V
    .locals 9

    .line 363
    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->setTrackForPosition()V

    .line 364
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchLeft:I

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusOffset:I

    add-int/2addr v0, v1

    iget v2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->strokeWidth:I

    add-int/2addr v0, v2

    int-to-float v4, v0

    int-to-float v7, p2

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchRight:I

    sub-int/2addr p2, v2

    sub-int/2addr p2, v1

    int-to-float v6, p2

    iget-object v8, p0, Lcom/squareup/marin/widgets/MarinSwitch;->trackPaint:Landroid/graphics/Paint;

    move-object v3, p1

    move v5, v7

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected drawableStateChanged()V
    .locals 0

    .line 426
    invoke-super {p0}, Landroid/widget/CompoundButton;->drawableStateChanged()V

    .line 427
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->invalidate()V

    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 1

    add-int/lit8 p1, p1, 0x1

    .line 417
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onCreateDrawableState(I)[I

    move-result-object p1

    .line 418
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    sget-object v0, Lcom/squareup/marin/widgets/MarinSwitch;->CHECKED_STATE_SET:[I

    invoke-static {p1, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->mergeDrawableStates([I[I)[I

    :cond_0
    return-object p1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .line 347
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onDraw(Landroid/graphics/Canvas;)V

    .line 348
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 349
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchLeft:I

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchTop:I

    iget v2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchRight:I

    iget v3, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchBottom:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 350
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchBottom:I

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchTop:I

    sub-int/2addr v0, v1

    const/4 v1, 0x2

    div-int/2addr v0, v1

    .line 351
    iget v2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchLeft:I

    iget v3, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusOffset:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget v3, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballDimen:I

    div-int/2addr v3, v1

    int-to-float v3, v3

    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 353
    iget v3, p0, Lcom/squareup/marin/widgets/MarinSwitch;->currentTouchState:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    if-ne v3, v1, :cond_1

    .line 354
    :cond_0
    invoke-virtual {p0, p1, v0, v2}, Lcom/squareup/marin/widgets/MarinSwitch;->drawBallFocus(Landroid/graphics/Canvas;II)V

    .line 356
    :cond_1
    invoke-virtual {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->drawTrack(Landroid/graphics/Canvas;I)V

    .line 357
    invoke-virtual {p0, p1, v0, v2}, Lcom/squareup/marin/widgets/MarinSwitch;->drawBall(Landroid/graphics/Canvas;II)V

    .line 359
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .line 168
    invoke-super {p0, p1, p2}, Landroid/widget/CompoundButton;->onMeasure(II)V

    .line 170
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getPaddingLeft()I

    move-result p1

    iput p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchLeft:I

    .line 171
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getPaddingTop()I

    move-result p1

    iput p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchTop:I

    .line 172
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getMeasuredWidth()I

    move-result p1

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getPaddingRight()I

    move-result p2

    sub-int/2addr p1, p2

    iput p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchRight:I

    .line 173
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getMeasuredHeight()I

    move-result p1

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getPaddingBottom()I

    move-result p2

    sub-int/2addr p1, p2

    iput p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchBottom:I

    .line 174
    iget p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchRight:I

    iget p2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchLeft:I

    sub-int/2addr p1, p2

    iput p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->switchWidth:I

    .line 177
    iget p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPositionAnimated:F

    invoke-static {p1}, Ljava/lang/Float;->isNaN(F)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 178
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->isChecked()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getThumbScrollRange()I

    move-result p1

    int-to-float p1, p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    goto :goto_1

    .line 180
    :cond_1
    iget p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPositionAnimated:F

    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getThumbScrollRange()I

    move-result p2

    int-to-float p2, p2

    mul-float p1, p1, p2

    iput p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    :goto_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .line 198
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 199
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    const/4 v1, 0x1

    if-eqz v0, :cond_7

    const/4 v2, 0x2

    if-eq v0, v1, :cond_5

    if-eq v0, v2, :cond_0

    const/4 v3, 0x3

    if-eq v0, v3, :cond_5

    goto/16 :goto_0

    .line 213
    :cond_0
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->currentTouchState:I

    if-eqz v0, :cond_8

    if-eq v0, v1, :cond_3

    if-eq v0, v2, :cond_1

    goto/16 :goto_0

    .line 232
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result p1

    .line 233
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mTouchX:F

    sub-float v0, p1, v0

    const/4 v2, 0x0

    .line 234
    iget v3, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    add-float/2addr v3, v0

    invoke-direct {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getThumbScrollRange()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 235
    iget v2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_2

    .line 236
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->ballPosition:F

    .line 237
    iput p1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mTouchX:F

    .line 238
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->invalidate()V

    :cond_2
    return v1

    .line 219
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 220
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 221
    iget v4, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mTouchX:F

    sub-float v4, v0, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mTouchSlop:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-gtz v4, :cond_4

    iget v4, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mTouchY:F

    sub-float v4, v3, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mTouchSlop:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_8

    .line 222
    :cond_4
    iput v2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->currentTouchState:I

    .line 223
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    invoke-interface {p1, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 224
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mTouchX:F

    .line 225
    iput v3, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mTouchY:F

    return v1

    .line 248
    :cond_5
    iget v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->currentTouchState:I

    if-ne v0, v2, :cond_6

    .line 249
    invoke-direct {p0, p1}, Lcom/squareup/marin/widgets/MarinSwitch;->stopDrag(Landroid/view/MotionEvent;)V

    return v1

    :cond_6
    const/4 v0, 0x0

    .line 252
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->currentTouchState:I

    .line 253
    iget-object v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0

    .line 202
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 203
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 204
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-direct {p0, v0, v2}, Lcom/squareup/marin/widgets/MarinSwitch;->hitThumb(FF)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 205
    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->currentTouchState:I

    .line 206
    iput v0, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mTouchX:F

    .line 207
    iput v2, p0, Lcom/squareup/marin/widgets/MarinSwitch;->mTouchY:F

    .line 258
    :cond_8
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public removePadding()V
    .locals 2

    .line 437
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->getMinWidth()I

    move-result v0

    iget v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusOffset:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    .line 438
    iput v1, p0, Lcom/squareup/marin/widgets/MarinSwitch;->focusOffset:I

    .line 439
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->setMinWidth(I)V

    .line 440
    invoke-virtual {p0, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->setMaxWidth(I)V

    .line 442
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinSwitch;->requestLayout()V

    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .line 336
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const/4 v0, 0x1

    .line 337
    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->updateCheckedState(ZZ)V

    return-void
.end method

.method public setCheckedNoAnimation(Z)V
    .locals 1

    .line 341
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    const/4 v0, 0x0

    .line 342
    invoke-direct {p0, p1, v0}, Lcom/squareup/marin/widgets/MarinSwitch;->updateCheckedState(ZZ)V

    return-void
.end method
