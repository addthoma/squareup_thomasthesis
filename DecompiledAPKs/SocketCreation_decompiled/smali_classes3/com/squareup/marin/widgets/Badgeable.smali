.class public interface abstract Lcom/squareup/marin/widgets/Badgeable;
.super Ljava/lang/Object;
.source "Badgeable.java"

# interfaces
.implements Lcom/squareup/mortar/HasContext;


# virtual methods
.method public abstract hideBadge()V
.end method

.method public abstract showBadge(Ljava/lang/CharSequence;)V
.end method

.method public abstract showFatalPriorityBadge()V
.end method

.method public abstract showHighPriorityBadge(Ljava/lang/CharSequence;)V
.end method
