.class public Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;
.super Ljava/lang/Object;
.source "MarinActionBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/marin/widgets/MarinActionBar$Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CustomViewConfig"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;
    }
.end annotation


# instance fields
.field public final command:Ljava/lang/Runnable;

.field public final customViewFactory:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

.field public final enabled:Z

.field public final tooltip:Ljava/lang/CharSequence;

.field public final visible:Z


# direct methods
.method public constructor <init>(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)V
    .locals 1

    .line 529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->access$800(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->tooltip:Ljava/lang/CharSequence;

    .line 531
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->access$900(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->command:Ljava/lang/Runnable;

    .line 532
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->access$1000(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->enabled:Z

    .line 533
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->access$1100(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->visible:Z

    .line 534
    invoke-static {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;->access$1200(Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder$CustomViewConfigBuilder;)Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig;->customViewFactory:Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

    return-void
.end method
