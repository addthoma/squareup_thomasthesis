.class public Lcom/squareup/marin/widgets/HorizontalRuleView;
.super Landroid/view/View;
.source "HorizontalRuleView.java"

# interfaces
.implements Lcom/squareup/marin/widgets/HorizontalRule;


# static fields
.field public static final DIVIDER_HEIGHT:I = 0x1


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 19
    invoke-virtual {p0, p2, p3}, Lcom/squareup/marin/widgets/HorizontalRuleView;->setPadding(II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 0

    return-void
.end method

.method public drawSelf()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getRulePosition()I
    .locals 3

    .line 37
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/HorizontalRuleView;->getTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/HorizontalRuleView;->getTop()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/marin/widgets/HorizontalRuleView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public setPadding(II)V
    .locals 1

    const/4 v0, 0x0

    .line 23
    invoke-virtual {p0, v0, p1, v0, p2}, Lcom/squareup/marin/widgets/HorizontalRuleView;->setPadding(IIII)V

    add-int/2addr p1, p2

    add-int/lit8 p1, p1, 0x1

    .line 24
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/HorizontalRuleView;->setMinimumHeight(I)V

    return-void
.end method
