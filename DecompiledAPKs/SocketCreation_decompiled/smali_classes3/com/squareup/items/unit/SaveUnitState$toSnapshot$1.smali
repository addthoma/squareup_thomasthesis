.class final Lcom/squareup/items/unit/SaveUnitState$toSnapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SaveUnitState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/SaveUnitState;->toSnapshot()Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "sink",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/unit/SaveUnitState;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/SaveUnitState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/SaveUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/SaveUnitState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/SaveUnitState$toSnapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "sink"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/items/unit/SaveUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/SaveUnitState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "this::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 65
    iget-object v0, p0, Lcom/squareup/items/unit/SaveUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/SaveUnitState;

    .line 66
    instance-of v1, v0, Lcom/squareup/items/unit/SaveUnitState$Saving;

    if-eqz v1, :cond_0

    sget-object v0, Lcom/squareup/items/unit/SaveUnitState;->Companion:Lcom/squareup/items/unit/SaveUnitState$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/SaveUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v1, Lcom/squareup/items/unit/SaveUnitState$Saving;

    invoke-static {v0, p1, v1}, Lcom/squareup/items/unit/SaveUnitState$Companion;->access$writeSaving(Lcom/squareup/items/unit/SaveUnitState$Companion;Lokio/BufferedSink;Lcom/squareup/items/unit/SaveUnitState$Saving;)V

    goto :goto_0

    .line 67
    :cond_0
    instance-of v1, v0, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    if-eqz v1, :cond_1

    sget-object v0, Lcom/squareup/items/unit/SaveUnitState;->Companion:Lcom/squareup/items/unit/SaveUnitState$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/SaveUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v1, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;

    invoke-virtual {v1}, Lcom/squareup/items/unit/SaveUnitState$FetchingCountOfAffectedVariations;->getIncomingSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/items/unit/SaveUnitState$Companion;->access$writeSaving(Lcom/squareup/items/unit/SaveUnitState$Companion;Lokio/BufferedSink;Lcom/squareup/items/unit/SaveUnitState$Saving;)V

    goto :goto_0

    .line 68
    :cond_1
    instance-of v1, v0, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    if-eqz v1, :cond_2

    .line 69
    check-cast v0, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    invoke-virtual {v0}, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;->getCount()I

    move-result v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 70
    sget-object v0, Lcom/squareup/items/unit/SaveUnitState;->Companion:Lcom/squareup/items/unit/SaveUnitState$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/SaveUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v1, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;

    invoke-virtual {v1}, Lcom/squareup/items/unit/SaveUnitState$DisplayCountOfAffectedVariations;->getIncomingSaving()Lcom/squareup/items/unit/SaveUnitState$Saving;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/items/unit/SaveUnitState$Companion;->access$writeSaving(Lcom/squareup/items/unit/SaveUnitState$Companion;Lokio/BufferedSink;Lcom/squareup/items/unit/SaveUnitState$Saving;)V

    goto :goto_0

    .line 72
    :cond_2
    instance-of v0, v0, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/items/unit/SaveUnitState;->Companion:Lcom/squareup/items/unit/SaveUnitState$Companion;

    iget-object v1, p0, Lcom/squareup/items/unit/SaveUnitState$toSnapshot$1;->this$0:Lcom/squareup/items/unit/SaveUnitState;

    check-cast v1, Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;

    invoke-static {v0, p1, v1}, Lcom/squareup/items/unit/SaveUnitState$Companion;->access$writeUnitSaveFailed(Lcom/squareup/items/unit/SaveUnitState$Companion;Lokio/BufferedSink;Lcom/squareup/items/unit/SaveUnitState$UnitSaveFailed;)V

    :cond_3
    :goto_0
    return-void
.end method
