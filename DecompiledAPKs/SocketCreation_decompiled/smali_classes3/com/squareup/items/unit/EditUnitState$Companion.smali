.class public final Lcom/squareup/items/unit/EditUnitState$Companion;
.super Ljava/lang/Object;
.source "EditUnitState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/unit/EditUnitState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditUnitState.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditUnitState.kt\ncom/squareup/items/unit/EditUnitState$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,235:1\n180#2:236\n148#2:237\n165#2:238\n56#3:239\n*E\n*S KotlinDebug\n*F\n+ 1 EditUnitState.kt\ncom/squareup/items/unit/EditUnitState$Companion\n*L\n128#1:236\n128#1:237\n128#1:238\n128#1:239\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/items/unit/EditUnitState$Companion;",
        "",
        "()V",
        "fromSnapshot",
        "Lcom/squareup/items/unit/EditUnitState;",
        "byteString",
        "Lokio/ByteString;",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 126
    invoke-direct {p0}, Lcom/squareup/items/unit/EditUnitState$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromSnapshot(Lokio/ByteString;)Lcom/squareup/items/unit/EditUnitState;
    .locals 9

    const-string v0, "byteString"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 129
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 130
    const-class v1, Lcom/squareup/items/unit/EditUnitState$Saving;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v3

    .line 132
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v4

    .line 133
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v5

    .line 134
    sget-object v0, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v6

    .line 237
    const-class v0, Lcom/squareup/items/unit/SaveUnitAction;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    const-string v1, "T::class.java.enumConstants[readInt()]"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v7, v0

    check-cast v7, Lcom/squareup/items/unit/SaveUnitAction;

    .line 136
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v8

    .line 130
    new-instance p1, Lcom/squareup/items/unit/EditUnitState$Saving;

    move-object v2, p1

    invoke-direct/range {v2 .. v8}, Lcom/squareup/items/unit/EditUnitState$Saving;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/items/unit/SaveUnitAction;Z)V

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    goto/16 :goto_1

    .line 138
    :cond_0
    const-class v1, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 238
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    .line 239
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v4, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v3, v4}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v3

    if-eqz v3, :cond_1

    check-cast v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 142
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v4

    .line 143
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v5

    .line 140
    new-instance v6, Lcom/squareup/items/unit/LocalizedStandardUnit;

    invoke-direct {v6, v3, v4, v5}, Lcom/squareup/items/unit/LocalizedStandardUnit;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 239
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.protos.connect.v2.common.MeasurementUnit"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 238
    :cond_2
    check-cast v1, Ljava/util/List;

    .line 138
    new-instance p1, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;

    invoke-direct {p1, v1}, Lcom/squareup/items/unit/EditUnitState$StandardUnitsList;-><init>(Ljava/util/List;)V

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    goto/16 :goto_1

    .line 148
    :cond_3
    const-class v1, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 149
    new-instance v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 150
    sget-object v1, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    .line 151
    new-instance v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    sget-object v3, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v3, p1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 152
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    .line 149
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    goto/16 :goto_1

    .line 155
    :cond_4
    const-class v1, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 156
    new-instance v0, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;

    .line 157
    sget-object v1, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    .line 158
    sget-object v2, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v2, p1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    .line 159
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    .line 156
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/items/unit/EditUnitState$EditsDiscardAttempted;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    goto :goto_1

    .line 163
    :cond_5
    const-class v1, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v0, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;

    .line 164
    sget-object v1, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v1, p1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    .line 165
    sget-object v2, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper;->Companion:Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;

    invoke-virtual {v2, p1}, Lcom/squareup/items/unit/CatalogMeasurementUnitSerializationHelper$Companion;->readCatalogMeasurementUnit(Lokio/BufferedSource;)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    .line 166
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    .line 163
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    goto :goto_1

    .line 169
    :cond_6
    const-class v1, Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v0, Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;

    .line 170
    invoke-static {p1}, Lcom/squareup/items/unit/EditUnitStateKt;->access$readRequiresInternet(Lokio/BufferedSource;)Lcom/squareup/items/unit/EditUnitState$RequiresInternet;

    move-result-object p1

    .line 169
    invoke-direct {v0, p1}, Lcom/squareup/items/unit/EditUnitState$WarnInternetUnavailable;-><init>(Lcom/squareup/items/unit/EditUnitState$RequiresInternet;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    goto :goto_1

    .line 173
    :cond_7
    const-class v1, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v0, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;

    .line 174
    invoke-static {p1}, Lcom/squareup/items/unit/EditUnitStateKt;->access$readRequiresInternet(Lokio/BufferedSource;)Lcom/squareup/items/unit/EditUnitState$RequiresInternet;

    move-result-object p1

    .line 173
    invoke-direct {v0, p1}, Lcom/squareup/items/unit/EditUnitState$CheckInternetAvailability;-><init>(Lcom/squareup/items/unit/EditUnitState$RequiresInternet;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/items/unit/EditUnitState;

    :goto_1
    return-object p1

    .line 177
    :cond_8
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
