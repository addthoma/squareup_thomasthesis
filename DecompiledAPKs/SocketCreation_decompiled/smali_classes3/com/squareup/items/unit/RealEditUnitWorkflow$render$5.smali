.class final Lcom/squareup/items/unit/RealEditUnitWorkflow$render$5;
.super Lkotlin/jvm/internal/Lambda;
.source "EditUnitWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealEditUnitWorkflow;->render(Lcom/squareup/items/unit/EditUnitInput;Lcom/squareup/items/unit/EditUnitState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/unit/EditUnitState;",
        "+",
        "Lcom/squareup/items/unit/EditUnitResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/unit/EditUnitState;",
        "Lcom/squareup/items/unit/EditUnitResult;",
        "event",
        "Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/items/unit/EditUnitState;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/EditUnitState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$5;->$state:Lcom/squareup/items/unit/EditUnitState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/unit/EditUnitState;",
            "Lcom/squareup/items/unit/EditUnitResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 275
    instance-of p1, p1, Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen$Event$Confirm;

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 276
    new-instance v0, Lcom/squareup/items/unit/EditUnitState$EditUnit;

    .line 277
    iget-object v1, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$5;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v1, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;

    invoke-virtual {v1}, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;->getOriginalUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v1

    .line 278
    new-instance v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    iget-object v3, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$5;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v3, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;

    invoke-virtual {v3}, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;->getUpdatedUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)V

    .line 279
    iget-object v3, p0, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$5;->$state:Lcom/squareup/items/unit/EditUnitState;

    check-cast v3, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;

    invoke-virtual {v3}, Lcom/squareup/items/unit/EditUnitState$DuplicateCustomUnitName;->isDefaultUnit()Z

    move-result v3

    .line 276
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/items/unit/EditUnitState$EditUnit;-><init>(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;Z)V

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 275
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 116
    check-cast p1, Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/RealEditUnitWorkflow$render$5;->invoke(Lcom/squareup/items/unit/DuplicateCustomUnitNameScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
