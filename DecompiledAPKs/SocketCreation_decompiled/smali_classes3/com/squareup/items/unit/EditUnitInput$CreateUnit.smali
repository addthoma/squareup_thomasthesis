.class public final Lcom/squareup/items/unit/EditUnitInput$CreateUnit;
.super Lcom/squareup/items/unit/EditUnitInput;
.source "EditUnitWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/unit/EditUnitInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateUnit"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u000f\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0008H\u00c6\u0003J-\u0010\u0013\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u0008H\u00d6\u0001R\u0014\u0010\u0007\u001a\u00020\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u001a\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/items/unit/EditUnitInput$CreateUnit;",
        "Lcom/squareup/items/unit/EditUnitInput;",
        "unitsInUse",
        "",
        "Lcom/squareup/items/unit/SelectableUnit;",
        "unitCreationSource",
        "Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;",
        "prepopulatedName",
        "",
        "(Ljava/util/Set;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;Ljava/lang/String;)V",
        "getPrepopulatedName",
        "()Ljava/lang/String;",
        "getUnitCreationSource",
        "()Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;",
        "getUnitsInUse",
        "()Ljava/util/Set;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "edit-unit_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final prepopulatedName:Ljava/lang/String;

.field private final unitCreationSource:Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

.field private final unitsInUse:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;Ljava/lang/String;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;",
            "Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "unitsInUse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unitCreationSource"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "prepopulatedName"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v1, p0

    move-object v4, p1

    move-object v5, p3

    move-object v6, p2

    .line 80
    invoke-direct/range {v1 .. v8}, Lcom/squareup/items/unit/EditUnitInput;-><init>(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Ljava/util/Set;Ljava/lang/String;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->unitsInUse:Ljava/util/Set;

    iput-object p2, p0, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->unitCreationSource:Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    iput-object p3, p0, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->prepopulatedName:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/Set;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const-string p3, ""

    .line 79
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;-><init>(Ljava/util/Set;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/unit/EditUnitInput$CreateUnit;Ljava/util/Set;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/items/unit/EditUnitInput$CreateUnit;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getUnitsInUse()Ljava/util/Set;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getUnitCreationSource()Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getPrepopulatedName()Ljava/lang/String;

    move-result-object p3

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->copy(Ljava/util/Set;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;Ljava/lang/String;)Lcom/squareup/items/unit/EditUnitInput$CreateUnit;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getUnitsInUse()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getUnitCreationSource()Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getPrepopulatedName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final copy(Ljava/util/Set;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;Ljava/lang/String;)Lcom/squareup/items/unit/EditUnitInput$CreateUnit;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;",
            "Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/items/unit/EditUnitInput$CreateUnit;"
        }
    .end annotation

    const-string/jumbo v0, "unitsInUse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unitCreationSource"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "prepopulatedName"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;-><init>(Ljava/util/Set;Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;Ljava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getUnitsInUse()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getUnitsInUse()Ljava/util/Set;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getUnitCreationSource()Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getUnitCreationSource()Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getPrepopulatedName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getPrepopulatedName()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getPrepopulatedName()Ljava/lang/String;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->prepopulatedName:Ljava/lang/String;

    return-object v0
.end method

.method public getUnitCreationSource()Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->unitCreationSource:Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    return-object v0
.end method

.method public getUnitsInUse()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/items/unit/SelectableUnit;",
            ">;"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->unitsInUse:Ljava/util/Set;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getUnitsInUse()Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getUnitCreationSource()Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getPrepopulatedName()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CreateUnit(unitsInUse="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getUnitsInUse()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", unitCreationSource="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getUnitCreationSource()Lcom/squareup/items/unit/EditUnitEventLogger$UnitCreationSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", prepopulatedName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/items/unit/EditUnitInput$CreateUnit;->getPrepopulatedName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
