.class final Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations$run$1;
.super Ljava/lang/Object;
.source "SaveUnitWorkflow.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogOnlineTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations;->run()Lkotlinx/coroutines/flow/Flow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogOnlineTask<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "",
        "kotlin.jvm.PlatformType",
        "online",
        "Lcom/squareup/shared/catalog/Catalog$Online;",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations$run$1;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/shared/catalog/Catalog$Online;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Online;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "online"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations$run$1;->this$0:Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations;

    invoke-static {v0}, Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations;->access$getUnitId$p(Lcom/squareup/items/unit/RealSaveUnitWorkflow$FetchCountOfAffectedVariations;)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x64

    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Online;->countVariationsWithAssignedUnit(Ljava/lang/String;I)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    return-object p1
.end method
