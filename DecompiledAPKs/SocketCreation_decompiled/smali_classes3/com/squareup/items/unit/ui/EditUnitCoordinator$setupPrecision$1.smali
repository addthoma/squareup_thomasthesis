.class final Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupPrecision$1;
.super Ljava/lang/Object;
.source "EditUnitCoordinator.kt"

# interfaces
.implements Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/ui/EditUnitCoordinator;->setupPrecision(Lcom/squareup/items/unit/EditUnitScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "<anonymous parameter 2>",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/items/unit/EditUnitScreen;

.field final synthetic this$0:Lcom/squareup/items/unit/ui/EditUnitCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/ui/EditUnitCoordinator;Lcom/squareup/items/unit/EditUnitScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupPrecision$1;->this$0:Lcom/squareup/items/unit/ui/EditUnitCoordinator;

    iput-object p2, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupPrecision$1;->$screen:Lcom/squareup/items/unit/EditUnitScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/noho/NohoCheckableGroup;II)V
    .locals 0

    .line 146
    iget-object p1, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupPrecision$1;->$screen:Lcom/squareup/items/unit/EditUnitScreen;

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitScreen;->getState()Lcom/squareup/items/unit/EditUnitState$EditUnit;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/items/unit/EditUnitState$EditUnit;->getCurrentUnitInEditing()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    move-result-object p1

    iget-object p3, p0, Lcom/squareup/items/unit/ui/EditUnitCoordinator$setupPrecision$1;->this$0:Lcom/squareup/items/unit/ui/EditUnitCoordinator;

    invoke-static {p3, p2}, Lcom/squareup/items/unit/ui/EditUnitCoordinator;->access$getPrecisionIndexById(Lcom/squareup/items/unit/ui/EditUnitCoordinator;I)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;->setPrecision(I)Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit$Builder;

    return-void
.end method
