.class final Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory$create$1;
.super Ljava/lang/Object;
.source "UnitUnsavedChangesAlertDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u001c\u0010\u0003\u001a\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/unit/UnitUnsavedChangesAlertScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 3

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory$create$1;->$context:Landroid/content/Context;

    .line 30
    new-instance v1, Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory$create$1$1;

    invoke-direct {v1, p1}, Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory$create$1$1;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v1, Ljava/lang/Runnable;

    .line 31
    new-instance v2, Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory$create$1$2;

    invoke-direct {v2, p1}, Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory$create$1$2;-><init>(Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v2, Ljava/lang/Runnable;

    .line 28
    invoke-static {v0, v1, v2}, Lcom/squareup/ui/ConfirmDiscardChangesAlertDialogFactory;->createConfirmDiscardChangesAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 19
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/items/unit/ui/UnitUnsavedChangesAlertDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
