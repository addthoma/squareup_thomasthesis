.class final Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StandardUnitsListCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->createRecycler(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$UnitFamilyLabel;",
        "Lcom/squareup/noho/NohoLabel;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "",
        "item",
        "Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$UnitFamilyLabel;",
        "row",
        "Lcom/squareup/noho/NohoLabel;",
        "invoke",
        "com/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$1$2"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$UnitFamilyLabel;

    check-cast p3, Lcom/squareup/noho/NohoLabel;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$1;->invoke(ILcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$UnitFamilyLabel;Lcom/squareup/noho/NohoLabel;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$UnitFamilyLabel;Lcom/squareup/noho/NohoLabel;)V
    .locals 2

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "row"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    iget-object p1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;

    invoke-virtual {p2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$StandardUnitsListItem$UnitFamilyLabel;->getUnitFamily()Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;

    invoke-static {v0}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->access$getRes$p(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;)Lcom/squareup/util/Res;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->access$displayValue(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$UnitFamily;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object p1, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;

    invoke-static {p1}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->access$getRes$p(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;)Lcom/squareup/util/Res;

    move-result-object p1

    sget p2, Lcom/squareup/noho/R$dimen;->noho_row_section_header_upper_gap:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p1

    .line 179
    iget-object p2, p0, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;

    invoke-static {p2}, Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;->access$getRes$p(Lcom/squareup/items/unit/ui/StandardUnitsListCoordinator;)Lcom/squareup/util/Res;

    move-result-object p2

    sget v0, Lcom/squareup/noho/R$dimen;->noho_row_section_header_lower_gap:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result p2

    .line 180
    invoke-virtual {p3}, Lcom/squareup/noho/NohoLabel;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p3}, Lcom/squareup/noho/NohoLabel;->getPaddingRight()I

    move-result v1

    invoke-virtual {p3, v0, p1, v1, p2}, Lcom/squareup/noho/NohoLabel;->setPadding(IIII)V

    return-void
.end method
