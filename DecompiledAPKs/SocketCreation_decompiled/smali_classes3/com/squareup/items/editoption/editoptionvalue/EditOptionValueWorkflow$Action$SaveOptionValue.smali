.class public final Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$SaveOptionValue;
.super Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;
.source "EditOptionValueWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SaveOptionValue"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u0004*\u0012\u0012\u0008\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00080\u0005H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$SaveOptionValue;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueState;",
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$SaveOptionValue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 62
    new-instance v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$SaveOptionValue;

    invoke-direct {v0}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$SaveOptionValue;-><init>()V

    sput-object v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$SaveOptionValue;->INSTANCE:Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action$SaveOptionValue;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, v0}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;",
            "-",
            "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    new-instance v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput$OptionValueChanged;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;

    invoke-virtual {v1}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;

    invoke-virtual {v2}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValue;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueOutput$OptionValueChanged;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method
