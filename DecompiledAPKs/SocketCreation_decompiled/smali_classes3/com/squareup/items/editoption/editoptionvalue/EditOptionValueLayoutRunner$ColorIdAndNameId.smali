.class final Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;
.super Ljava/lang/Object;
.source "EditOptionValueLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ColorIdAndNameId"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0082\u0008\u0018\u00002\u00020\u0001B\u0019\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\n\u001a\u00020\u0003H\u00c6\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u000f\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;",
        "",
        "colorResId",
        "",
        "colorNameStringId",
        "(II)V",
        "getColorNameStringId",
        "()I",
        "getColorResId",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final colorNameStringId:I

.field private final colorResId:I


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorResId:I

    iput p2, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorNameStringId:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;IIILjava/lang/Object;)Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget p1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorResId:I

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget p2, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorNameStringId:I

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->copy(II)Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorResId:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorNameStringId:I

    return v0
.end method

.method public final copy(II)Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;
    .locals 1

    new-instance v0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;-><init>(II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;

    iget v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorResId:I

    iget v1, p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorResId:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorNameStringId:I

    iget p1, p1, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorNameStringId:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getColorNameStringId()I
    .locals 1

    .line 126
    iget v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorNameStringId:I

    return v0
.end method

.method public final getColorResId()I
    .locals 1

    .line 125
    iget v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorResId:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorResId:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorNameStringId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ColorIdAndNameId(colorResId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorResId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", colorNameStringId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/items/editoption/editoptionvalue/EditOptionValueLayoutRunner$ColorIdAndNameId;->colorNameStringId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
