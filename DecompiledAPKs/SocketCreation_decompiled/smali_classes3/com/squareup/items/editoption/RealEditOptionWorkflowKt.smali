.class public final Lcom/squareup/items/editoption/RealEditOptionWorkflowKt;
.super Ljava/lang/Object;
.source "RealEditOptionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEditOptionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEditOptionWorkflow.kt\ncom/squareup/items/editoption/RealEditOptionWorkflowKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,355:1\n1360#2:356\n1429#2,3:357\n*E\n*S KotlinDebug\n*F\n+ 1 RealEditOptionWorkflow.kt\ncom/squareup/items/editoption/RealEditOptionWorkflowKt\n*L\n339#1:356\n339#1,3:357\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\u001a\u001c\u0010\u0000\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0003H\u0002\u001a\u001c\u0010\u0005\u001a\u00020\u0001*\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0003H\u0002\u001a\u000c\u0010\u0007\u001a\u00020\u0008*\u00020\u0001H\u0002\u00a8\u0006\t"
    }
    d2 = {
        "addNewValue",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "valueId",
        "",
        "name",
        "updateValueName",
        "updatedName",
        "validForSave",
        "",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$addNewValue(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/items/editoption/RealEditOptionWorkflowKt;->addNewValue(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateValueName(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/squareup/items/editoption/RealEditOptionWorkflowKt;->updateValueName(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$validForSave(Lcom/squareup/cogs/itemoptions/ItemOption;)Z
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/items/editoption/RealEditOptionWorkflowKt;->validForSave(Lcom/squareup/cogs/itemoptions/ItemOption;)Z

    move-result p0

    return p0
.end method

.method private static final addNewValue(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 8

    .line 343
    invoke-virtual {p0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 344
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v5

    .line 345
    sget-object v0, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->Companion:Lcom/squareup/cogs/itemoptions/ItemOptionValue$Companion;

    .line 347
    invoke-virtual {p0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v1

    .line 345
    invoke-virtual {v0, p2, v1, p1}, Lcom/squareup/cogs/itemoptions/ItemOptionValue$Companion;->from(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object p1

    invoke-interface {v5, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x7

    const/4 v7, 0x0

    move-object v1, p0

    .line 342
    invoke-static/range {v1 .. v7}, Lcom/squareup/cogs/itemoptions/ItemOption;->copy$default(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object p0

    return-object p0
.end method

.method private static final updateValueName(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 10

    .line 339
    invoke-virtual {p0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 356
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 357
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 358
    move-object v3, v2

    check-cast v3, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 339
    invoke-virtual {v3}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getValueId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v6, p2

    goto :goto_1

    :cond_0
    invoke-virtual {v3}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v2

    move-object v6, v2

    :goto_1
    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->copy$default(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 359
    :cond_1
    move-object v7, v1

    check-cast v7, Ljava/util/List;

    const/4 v8, 0x7

    const/4 v9, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v3, p0

    .line 338
    invoke-static/range {v3 .. v9}, Lcom/squareup/cogs/itemoptions/ItemOption;->copy$default(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object p0

    return-object p0
.end method

.method private static final validForSave(Lcom/squareup/cogs/itemoptions/ItemOption;)Z
    .locals 2

    .line 353
    invoke-virtual {p0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/util/Collection;

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result p0

    xor-int/2addr p0, v1

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method
