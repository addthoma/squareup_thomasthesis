.class public abstract Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;
.super Ljava/lang/Object;
.source "RealEditOptionWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/editoption/RealEditOptionWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;,
        Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Exit;,
        Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;,
        Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateDisplayName;,
        Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;,
        Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;,
        Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$RemoveValue;,
        Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$ReorderValue;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/editoption/EditOptionState;",
        "Lcom/squareup/items/editoption/EditOptionOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00080\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0008\u0005\u0006\u0007\u0008\t\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004\u0082\u0001\u0008\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/editoption/EditOptionState;",
        "Lcom/squareup/items/editoption/EditOptionOutput;",
        "()V",
        "Exit",
        "NewValueStarted",
        "RemoveValue",
        "ReorderValue",
        "Save",
        "UpdateDisplayName",
        "UpdateName",
        "UpdateValueName",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Save;",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$Exit;",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateName;",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateDisplayName;",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$UpdateValueName;",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$NewValueStarted;",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$RemoveValue;",
        "Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action$ReorderValue;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 216
    invoke-direct {p0}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/items/editoption/EditOptionOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/items/editoption/EditOptionState;",
            ">;)",
            "Lcom/squareup/items/editoption/EditOptionOutput;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/editoption/EditOptionOutput;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 216
    invoke-virtual {p0, p1}, Lcom/squareup/items/editoption/RealEditOptionWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/items/editoption/EditOptionOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/editoption/EditOptionState;",
            "-",
            "Lcom/squareup/items/editoption/EditOptionOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
