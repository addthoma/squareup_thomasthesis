.class public final Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory;
.super Ljava/lang/Object;
.source "WarnVariationsToDeleteDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWarnVariationsToDeleteDialogFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WarnVariationsToDeleteDialogFactory.kt\ncom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory\n*L\n1#1,136:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B)\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0016\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0005H\u0002J \u0010\u0011\u001a\n \u0013*\u0004\u0018\u00010\u00120\u00122\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u001c\u0010\u0016\u001a\n \u0013*\u0004\u0018\u00010\u00120\u0012*\u00020\u00172\u0006\u0010\u000c\u001a\u00020\rH\u0002J\u001c\u0010\u0018\u001a\n \u0013*\u0004\u0018\u00010\u00120\u0012*\u00020\u00172\u0006\u0010\u000c\u001a\u00020\rH\u0002J\u000c\u0010\u0019\u001a\u00020\u0015*\u00020\u0017H\u0002R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lio/reactivex/Observable;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "Landroid/app/AlertDialog;",
        "screen",
        "getDeleteConfirmation",
        "",
        "kotlin.jvm.PlatformType",
        "deletedCount",
        "",
        "getConfirm",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;",
        "getMessage",
        "getTitleId",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory;Landroid/content/Context;Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteScreen;)Landroid/app/AlertDialog;
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteScreen;)Landroid/app/AlertDialog;

    move-result-object p0

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteScreen;)Landroid/app/AlertDialog;
    .locals 3

    .line 32
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 33
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setVerticalButtonOrientation()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 34
    sget v1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 35
    sget v1, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 36
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteScreen;->getChangeDescription()Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory;->getConfirm(Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory$createDialog$1;

    invoke-direct {v2, p2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory$createDialog$1;-><init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 39
    sget v1, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory$createDialog$2;

    invoke-direct {v2, p2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory$createDialog$2;-><init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteScreen;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 42
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteScreen;->getChangeDescription()Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory;->getMessage(Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 43
    invoke-virtual {p2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteScreen;->getChangeDescription()Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory;->getTitleId(Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 p2, 0x0

    .line 44
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026(false)\n        .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getConfirm(Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 1

    .line 121
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;->getDeletedVariationCount()I

    move-result p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory;->getDeleteConfirmation(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 122
    :cond_0
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;->getDeletedVariationCount()I

    move-result p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory;->getDeleteConfirmation(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 123
    :cond_1
    instance-of p1, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$AddedAndDeletedValuesDescription;

    if-eqz p1, :cond_2

    .line 124
    sget p1, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_confirm_delete_and_create:I

    .line 123
    invoke-virtual {p2, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final getDeleteConfirmation(Landroid/content/Context;I)Ljava/lang/CharSequence;
    .locals 1

    const/4 v0, 0x1

    if-le p2, v0, :cond_0

    .line 130
    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_confirm_delete_plural:I

    goto :goto_0

    .line 132
    :cond_0
    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_confirm_delete_singular:I

    .line 128
    :goto_0
    invoke-static {p1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v0, "deleted_count"

    .line 133
    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 134
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private final getMessage(Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4

    .line 55
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;

    const/4 v1, 0x0

    const-string v2, "deleted_count"

    const/4 v3, 0x1

    if-eqz v0, :cond_5

    .line 56
    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;->getOptionSetDisplayName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    const-string v0, "option_set_name"

    if-eqz v1, :cond_3

    .line 59
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;->getDeletedVariationCount()I

    move-result v1

    if-le v1, v3, :cond_2

    .line 60
    sget v1, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_message_remove_option_set_no_display_plural:I

    goto :goto_0

    .line 62
    :cond_2
    sget v1, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_message_remove_option_set_no_display_singular:I

    .line 57
    :goto_0
    invoke-static {p2, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 64
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;->getOptionSetName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 65
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;->getDeletedVariationCount()I

    move-result p1

    invoke-virtual {p2, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 66
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto/16 :goto_4

    .line 70
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;->getDeletedVariationCount()I

    move-result v1

    if-le v1, v3, :cond_4

    .line 71
    sget v1, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_message_remove_option_set_plural:I

    goto :goto_1

    .line 73
    :cond_4
    sget v1, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_message_remove_option_set_singular:I

    .line 68
    :goto_1
    invoke-static {p2, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 75
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;->getOptionSetName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 76
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;->getOptionSetDisplayName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "option_set_display_name"

    invoke-virtual {p2, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 77
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;->getDeletedVariationCount()I

    move-result p1

    invoke-virtual {p2, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto/16 :goto_4

    .line 81
    :cond_5
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;

    if-eqz v0, :cond_b

    .line 82
    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;->getDeletedValueCount()I

    move-result v0

    if-le v0, v3, :cond_6

    .line 84
    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_message_remove_multiple_values_plural_variations:I

    .line 83
    invoke-static {p2, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 85
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;->getDeletedValueCount()I

    move-result v0

    const-string v1, "deleted_value_count"

    invoke-virtual {p2, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 86
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;->getDeletedVariationCount()I

    move-result p1

    invoke-virtual {p2, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 87
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto/16 :goto_4

    .line 89
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;->getSingularOptionValueName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_8

    :cond_7
    const/4 v1, 0x1

    :cond_8
    xor-int/lit8 v0, v1, 0x1

    if-eqz v0, :cond_a

    .line 92
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;->getDeletedVariationCount()I

    move-result v0

    if-le v0, v3, :cond_9

    .line 93
    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_message_remove_single_value_plural_variations:I

    goto :goto_2

    .line 95
    :cond_9
    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_message_remove_single_value_single_variation:I

    .line 91
    :goto_2
    invoke-static {p2, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 96
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;->getSingularOptionValueName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string/jumbo v1, "value_name"

    invoke-virtual {p2, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 97
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;->getDeletedVariationCount()I

    move-result p1

    invoke-virtual {p2, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 98
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_4

    .line 89
    :cond_a
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Check failed."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 101
    :cond_b
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$AddedAndDeletedValuesDescription;

    if-eqz v0, :cond_f

    .line 103
    check-cast p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$AddedAndDeletedValuesDescription;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$AddedAndDeletedValuesDescription;->getDeletedVariationCount()I

    move-result v0

    if-le v0, v3, :cond_d

    .line 104
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$AddedAndDeletedValuesDescription;->getCreatedVariationCount()I

    move-result v0

    if-le v0, v3, :cond_c

    .line 105
    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_message_plural_delete_plural_add:I

    goto :goto_3

    .line 107
    :cond_c
    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_message_plural_delete_singular_add:I

    goto :goto_3

    .line 109
    :cond_d
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$AddedAndDeletedValuesDescription;->getCreatedVariationCount()I

    move-result v0

    if-le v0, v3, :cond_e

    .line 110
    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_message_singular_delete_plural_add:I

    goto :goto_3

    .line 112
    :cond_e
    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_message_singular_delete_singular_add:I

    .line 102
    :goto_3
    invoke-static {p2, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 114
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$AddedAndDeletedValuesDescription;->getDeletedVariationCount()I

    move-result v0

    invoke-virtual {p2, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 115
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$AddedAndDeletedValuesDescription;->getCreatedVariationCount()I

    move-result p1

    const-string v0, "created_count"

    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 116
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    :goto_4
    return-object p1

    :cond_f
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method private final getTitleId(Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription;)I
    .locals 1

    .line 49
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$RemovedOptionSetDescription;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$DeletedValuesDescription;

    if-eqz v0, :cond_1

    .line 50
    :goto_0
    sget p1, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_title_delete:I

    goto :goto_1

    .line 51
    :cond_1
    instance-of p1, p1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/VariationChangeDescription$AddedAndDeletedValuesDescription;

    if-eqz p1, :cond_2

    sget p1, Lcom/squareup/items/assignitemoptions/impl/R$string;->warn_variations_to_delete_confirm_delete_and_create:I

    :goto_1
    return p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory;->screens:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    .line 23
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 25
    new-instance v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory$create$1;-><init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screens\n        .take(1)\u2026 screen.unwrapV2Screen) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
