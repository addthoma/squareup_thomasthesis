.class public final Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "ReviewVariationsToDeleteViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "reviewVariationsToDeleteLayoutRunnerFactory",
        "Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$Factory;",
        "(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$Factory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "reviewVariationsToDeleteLayoutRunnerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 10
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 11
    const-class v2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    .line 12
    sget v3, Lcom/squareup/items/assignitemoptions/impl/R$layout;->review_variations_to_delete:I

    .line 13
    new-instance v4, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory$1;

    invoke-direct {v4, p1}, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory$1;-><init>(Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteLayoutRunner$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 10
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lkotlin/reflect/KClass;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 15
    sget-object p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 16
    const-class v1, Lcom/squareup/items/assignitemoptions/changeoptionvalues/WarnVariationsToDeleteScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    .line 17
    sget-object v2, Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/changeoptionvalues/ReviewVariationsToDeleteViewFactory$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 15
    invoke-virtual {p1, v1, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 9
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
