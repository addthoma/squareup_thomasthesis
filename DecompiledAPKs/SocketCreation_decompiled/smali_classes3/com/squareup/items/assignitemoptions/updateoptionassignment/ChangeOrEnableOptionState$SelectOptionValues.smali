.class public final Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;
.super Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;
.source "ChangeOrEnableOptionState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectOptionValues"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\t\u0010\n\u001a\u00020\u000bH\u00d6\u0001J\u0013\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u000bH\u00d6\u0001J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\u0019\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000bH\u00d6\u0001R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;",
        "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;",
        "initialValueSelections",
        "",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
        "(Ljava/util/List;)V",
        "getInitialValueSelections",
        "()Ljava/util/List;",
        "component1",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final initialValueSelections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues$Creator;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues$Creator;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
            ">;)V"
        }
    .end annotation

    const-string v0, "initialValueSelections"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 13
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->initialValueSelections:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->initialValueSelections:Ljava/util/List;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->copy(Ljava/util/List;)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->initialValueSelections:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/util/List;)Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
            ">;)",
            "Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;"
        }
    .end annotation

    const-string v0, "initialValueSelections"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->initialValueSelections:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->initialValueSelections:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getInitialValueSelections()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
            ">;"
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->initialValueSelections:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->initialValueSelections:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SelectOptionValues(initialValueSelections="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->initialValueSelections:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/updateoptionassignment/ChangeOrEnableOptionState$SelectOptionValues;->initialValueSelections:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_0
    return-void
.end method
