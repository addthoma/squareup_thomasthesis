.class public final Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;
.super Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action;
.source "RealAssignOptionToItemWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Save"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAssignOptionToItemWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAssignOptionToItemWorkflow.kt\ncom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,143:1\n1360#2:144\n1429#2,3:145\n*E\n*S KotlinDebug\n*F\n+ 1 RealAssignOptionToItemWorkflow.kt\ncom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save\n*L\n137#1:144\n137#1,3:145\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003H\u00c6\u0003J\u0013\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\u0018\u0010\u0011\u001a\u00020\u0012*\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0013H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;",
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action;",
        "assignmentEngine",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V",
        "getAssignmentEngine",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;",
        "Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V
    .locals 1

    const-string v0, "assignmentEngine"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 133
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;->copy(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllVariations()Ljava/util/List;

    move-result-object v0

    const-string v1, "assignmentEngine.allVariations"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllItemOptions()Ljava/util/List;

    move-result-object v1

    const-string v2, "assignmentEngine.allItemOptions"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 144
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 145
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 146
    check-cast v3, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    const-string v4, "it"

    .line 137
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 147
    :cond_0
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 137
    invoke-static {v2}, Lkotlin/collections/MapsKt;->toMap(Ljava/lang/Iterable;)Ljava/util/Map;

    move-result-object v1

    .line 135
    new-instance v2, Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput$AssignmentUpdated;

    invoke-direct {v2, v0, v1}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemOutput$AssignmentUpdated;-><init>(Ljava/util/List;Ljava/util/Map;)V

    invoke-virtual {p1, v2}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final copy(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;
    .locals 1

    const-string v0, "assignmentEngine"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;-><init>(Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Save(assignmentEngine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/RealAssignOptionToItemWorkflow$Action$Save;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
