.class final Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$renderEditOptionWorkflow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CreateOptionForItemWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;->renderEditOptionWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/items/editoption/EditOptionWorkflow;Lcom/squareup/items/editoption/EditOptionProps;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/editoption/EditOptionOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
        "+",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
        "editOptionOutput",
        "Lcom/squareup/items/editoption/EditOptionOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

.field final synthetic $isIncrementAssignment:Z


# direct methods
.method constructor <init>(ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V
    .locals 0

    iput-boolean p1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$renderEditOptionWorkflow$1;->$isIncrementAssignment:Z

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$renderEditOptionWorkflow$1;->$assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/editoption/EditOptionOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/editoption/EditOptionOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "editOptionOutput"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    instance-of v0, p1, Lcom/squareup/items/editoption/EditOptionOutput$Discard;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$Cancel;->INSTANCE:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$Cancel;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 122
    :cond_0
    instance-of v0, p1, Lcom/squareup/items/editoption/EditOptionOutput$Save;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;

    .line 123
    check-cast p1, Lcom/squareup/items/editoption/EditOptionOutput$Save;

    invoke-virtual {p1}, Lcom/squareup/items/editoption/EditOptionOutput$Save;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object p1

    .line 124
    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$renderEditOptionWorkflow$1;->$isIncrementAssignment:Z

    .line 125
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$renderEditOptionWorkflow$1;->$assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    .line 122
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/items/editoption/EditOptionOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$renderEditOptionWorkflow$1;->invoke(Lcom/squareup/items/editoption/EditOptionOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
