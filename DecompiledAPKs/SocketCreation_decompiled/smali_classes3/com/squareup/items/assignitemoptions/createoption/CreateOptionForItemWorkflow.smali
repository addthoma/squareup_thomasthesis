.class public final Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "CreateOptionForItemWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCreateOptionForItemWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CreateOptionForItemWorkflow.kt\ncom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow\n+ 2 SnapshotParcels.kt\ncom/squareup/container/SnapshotParcelsKt\n*L\n1#1,206:1\n32#2,12:207\n*E\n*S KotlinDebug\n*F\n+ 1 CreateOptionForItemWorkflow.kt\ncom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow\n*L\n46#1,12:207\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002<\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0001:\u0001 B\u0017\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u001a\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00022\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016JN\u0010\u0014\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00032\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0017H\u0016J^\u0010\u0018\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u00172\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u0003H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "editOptionWorkflow",
        "Lcom/squareup/items/editoption/EditOptionWorkflow;",
        "selectVariationsToCreateWorkflow",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;",
        "(Lcom/squareup/items/editoption/EditOptionWorkflow;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;)V",
        "initialState",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateOption;",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "renderEditOptionWorkflow",
        "editOptionProps",
        "Lcom/squareup/items/editoption/EditOptionProps;",
        "isIncrementAssignment",
        "",
        "assignmentEngine",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "snapshotState",
        "Action",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final editOptionWorkflow:Lcom/squareup/items/editoption/EditOptionWorkflow;

.field private final selectVariationsToCreateWorkflow:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/items/editoption/EditOptionWorkflow;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "editOptionWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectVariationsToCreateWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;->editOptionWorkflow:Lcom/squareup/items/editoption/EditOptionWorkflow;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;->selectVariationsToCreateWorkflow:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;

    return-void
.end method

.method private final renderEditOptionWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/items/editoption/EditOptionWorkflow;Lcom/squareup/items/editoption/EditOptionProps;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
            ">;",
            "Lcom/squareup/items/editoption/EditOptionWorkflow;",
            "Lcom/squareup/items/editoption/EditOptionProps;",
            "Z",
            "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 117
    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    .line 119
    new-instance p2, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$renderEditOptionWorkflow$1;

    invoke-direct {p2, p4, p5}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$renderEditOptionWorkflow$1;-><init>(ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    move-object v4, p2

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p1

    move-object v2, p3

    .line 116
    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1
.end method


# virtual methods
.method public initialState(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateOption;
    .locals 2

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_4

    .line 207
    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p2

    const/4 v0, 0x0

    if-lez p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    const/4 v1, 0x0

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    move-object p1, v1

    :goto_1
    if-eqz p1, :cond_3

    .line 212
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object p2

    const-string v1, "Parcel.obtain()"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 213
    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 214
    array-length v1, p1

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 215
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 216
    const-class p1, Lcom/squareup/workflow/Snapshot;

    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_2
    const-string p1, "parcel.readParcelable<T>\u2026class.java.classLoader)!!"

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    invoke-virtual {p2}, Landroid/os/Parcel;->recycle()V

    .line 218
    :cond_3
    check-cast v1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateOption;

    if-eqz v1, :cond_4

    goto :goto_2

    .line 46
    :cond_4
    sget-object v1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateOption;->INSTANCE:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateOption;

    :goto_2
    return-object v1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;->initialState(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateOption;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;

    check-cast p2, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;->render(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object v6, p0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "props"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "state"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    move-object/from16 v3, p3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    instance-of v2, v1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateOption;

    if-eqz v2, :cond_0

    .line 55
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;->allItemOptionNames()Ljava/util/Set;

    move-result-object v10

    .line 56
    new-instance v1, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;

    const/4 v8, 0x0

    .line 57
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;->getOptionName()Ljava/lang/String;

    move-result-object v9

    const/4 v11, 0x1

    const/4 v12, 0x0

    move-object v7, v1

    .line 56
    invoke-direct/range {v7 .. v12}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 62
    iget-object v2, v6, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;->editOptionWorkflow:Lcom/squareup/items/editoption/EditOptionWorkflow;

    .line 63
    move-object v4, v1

    check-cast v4, Lcom/squareup/items/editoption/EditOptionProps;

    .line 64
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;->isIncrementAssignment()Z

    move-result v5

    .line 65
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v7

    move-object v0, p0

    move-object/from16 v1, p3

    move-object v3, v4

    move v4, v5

    move-object v5, v7

    .line 60
    invoke-direct/range {v0 .. v5}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;->renderEditOptionWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/items/editoption/EditOptionWorkflow;Lcom/squareup/items/editoption/EditOptionProps;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Ljava/util/Map;

    move-result-object v0

    goto/16 :goto_0

    .line 68
    :cond_0
    instance-of v2, v1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$UpdateNewOption;

    if-eqz v2, :cond_1

    .line 69
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;->allItemOptionNames()Ljava/util/Set;

    move-result-object v10

    .line 70
    new-instance v2, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;

    .line 71
    check-cast v1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$UpdateNewOption;

    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$UpdateNewOption;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v11, 0x2

    const/4 v12, 0x0

    move-object v7, v2

    .line 70
    invoke-direct/range {v7 .. v12}, Lcom/squareup/items/editoption/EditOptionProps$CreateNewOption;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;Ljava/lang/String;Ljava/util/Set;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 76
    iget-object v4, v6, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;->editOptionWorkflow:Lcom/squareup/items/editoption/EditOptionWorkflow;

    .line 77
    move-object v5, v2

    check-cast v5, Lcom/squareup/items/editoption/EditOptionProps;

    .line 78
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;->isIncrementAssignment()Z

    move-result v7

    .line 79
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v8

    move-object v0, p0

    move-object/from16 v1, p3

    move-object v2, v4

    move-object v3, v5

    move v4, v7

    move-object v5, v8

    .line 74
    invoke-direct/range {v0 .. v5}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;->renderEditOptionWorkflow(Lcom/squareup/workflow/RenderContext;Lcom/squareup/items/editoption/EditOptionWorkflow;Lcom/squareup/items/editoption/EditOptionProps;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 82
    :cond_1
    instance-of v2, v1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;

    if-eqz v2, :cond_2

    .line 84
    move-object v2, v1

    check-cast v2, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;

    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->getCreatedOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 86
    iget-object v4, v6, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;->selectVariationsToCreateWorkflow:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateWorkflow;

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 87
    new-instance v5, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;

    .line 88
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;->getItemName()Ljava/lang/String;

    move-result-object v8

    .line 89
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v9

    .line 90
    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;->getNumberOfExistingVariations()I

    move-result v10

    const/4 v12, 0x0

    const/16 v13, 0x10

    const/4 v14, 0x0

    move-object v7, v5

    .line 87
    invoke-direct/range {v7 .. v14}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateProps;-><init>(Ljava/lang/String;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILcom/squareup/cogs/itemoptions/ItemOptionValue;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v10, 0x0

    .line 93
    new-instance v2, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$render$1;

    invoke-direct {v2, v1, v0}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$render$1;-><init>(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemProps;)V

    move-object v11, v2

    check-cast v11, Lkotlin/jvm/functions/Function1;

    const/4 v12, 0x4

    const/4 v13, 0x0

    move-object/from16 v7, p3

    move-object v8, v4

    move-object v9, v5

    .line 85
    invoke-static/range {v7 .. v13}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0
.end method

.method public snapshotState(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    check-cast p1, Landroid/os/Parcelable;

    invoke-static {p1}, Lcom/squareup/container/SnapshotParcelsKt;->toSnapshot(Landroid/os/Parcelable;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow;->snapshotState(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
