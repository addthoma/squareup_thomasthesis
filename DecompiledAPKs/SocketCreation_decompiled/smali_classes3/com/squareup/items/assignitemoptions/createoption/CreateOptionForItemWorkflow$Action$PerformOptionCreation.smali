.class public final Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;
.super Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action;
.source "CreateOptionForItemWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PerformOptionCreation"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCreateOptionForItemWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CreateOptionForItemWorkflow.kt\ncom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,206:1\n1360#2:207\n1429#2,3:208\n*E\n*S KotlinDebug\n*F\n+ 1 CreateOptionForItemWorkflow.kt\ncom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation\n*L\n165#1:207\n165#1,3:208\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0011\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00052\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\u0018\u0010\u0019\u001a\u00020\u001a*\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\u001bH\u0016R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action;",
        "optionToCreate",
        "Lcom/squareup/cogs/itemoptions/ItemOption;",
        "isIncrementalAssignment",
        "",
        "assignmentEngine",
        "Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "(Lcom/squareup/cogs/itemoptions/ItemOption;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V",
        "getAssignmentEngine",
        "()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;",
        "()Z",
        "getOptionToCreate",
        "()Lcom/squareup/cogs/itemoptions/ItemOption;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

.field private final isIncrementalAssignment:Z

.field private final optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOption;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V
    .locals 1

    const-string v0, "optionToCreate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 160
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    iput-boolean p2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->isIncrementalAssignment:Z

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;Lcom/squareup/cogs/itemoptions/ItemOption;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->isIncrementalAssignment:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->copy(Lcom/squareup/cogs/itemoptions/ItemOption;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->toCatalogItemOption()Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->addNewItemOption(Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    .line 164
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v1

    .line 165
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 207
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 208
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 209
    check-cast v4, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 165
    invoke-virtual {v4}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getIdPair()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 210
    :cond_0
    check-cast v3, Ljava/util/List;

    .line 163
    invoke-virtual {v0, v1, v3}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->addOrReplaceIntendedValues(Ljava/lang/String;Ljava/util/List;)V

    .line 168
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->isIncrementalAssignment:Z

    if-eqz v0, :cond_3

    .line 169
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 172
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v0}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 173
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    .line 174
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getIdPair()Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;

    move-result-object v1

    .line 175
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 173
    invoke-virtual {v0, v1, v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->updateExistingVariationsAndCreateNewVariations(Lcom/squareup/shared/catalog/engines/itemoptionassignment/OptionValueIdPair;Ljava/util/List;)V

    .line 177
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->cleanUpIntendedOptionsAndValues()V

    .line 178
    sget-object v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput$Created;->INSTANCE:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput$Created;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_1

    .line 180
    :cond_1
    new-instance v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;

    .line 181
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    .line 182
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;->getAllVariations()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 180
    invoke-direct {v0, v1, v2}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemState$CreateVariations;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;I)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_1

    .line 169
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Check failed."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 185
    :cond_3
    sget-object v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput$Created;->INSTANCE:Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput$Created;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method public final component1()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->isIncrementalAssignment:Z

    return v0
.end method

.method public final component3()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final copy(Lcom/squareup/cogs/itemoptions/ItemOption;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;
    .locals 1

    const-string v0, "optionToCreate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "assignmentEngine"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;-><init>(Lcom/squareup/cogs/itemoptions/ItemOption;ZLcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->isIncrementalAssignment:Z

    iget-boolean v1, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->isIncrementalAssignment:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    return-object v0
.end method

.method public final getOptionToCreate()Lcom/squareup/cogs/itemoptions/ItemOption;
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->isIncrementalAssignment:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isIncrementalAssignment()Z
    .locals 1

    .line 158
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->isIncrementalAssignment:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PerformOptionCreation(optionToCreate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->optionToCreate:Lcom/squareup/cogs/itemoptions/ItemOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isIncrementalAssignment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->isIncrementalAssignment:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", assignmentEngine="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemWorkflow$Action$PerformOptionCreation;->assignmentEngine:Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
