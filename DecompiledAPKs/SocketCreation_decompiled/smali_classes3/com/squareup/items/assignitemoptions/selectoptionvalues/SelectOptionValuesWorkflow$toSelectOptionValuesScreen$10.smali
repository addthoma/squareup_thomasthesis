.class final Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionValuesWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow;->toSelectOptionValuesScreen(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState$SelectValues;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;Lcom/squareup/workflow/Sink;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesWorkflow.kt\ncom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,468:1\n1360#2:469\n1429#2,3:470\n704#2:473\n777#2,2:474\n1360#2:476\n1429#2,3:477\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionValuesWorkflow.kt\ncom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10\n*L\n449#1:469\n449#1,3:470\n451#1:473\n451#1,2:474\n452#1:476\n452#1,3:477\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $actionSink:Lcom/squareup/workflow/Sink;

.field final synthetic $props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10;->$actionSink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10;->$props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 56
    invoke-virtual {p0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 9

    .line 446
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10;->$actionSink:Lcom/squareup/workflow/Sink;

    if-eqz v0, :cond_4

    .line 447
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10;->$props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v3

    .line 448
    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10;->$props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    invoke-virtual {v1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getOptionValueSelections()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 469
    new-instance v2, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v1, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 470
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 471
    check-cast v5, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    .line 449
    invoke-virtual {v5}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->getOptionValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 472
    :cond_0
    move-object v1, v2

    check-cast v1, Ljava/util/List;

    .line 450
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10;->$props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getOptionValueSelections()Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 473
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    check-cast v5, Ljava/util/Collection;

    .line 474
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    move-object v7, v6

    check-cast v7, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    .line 451
    invoke-virtual {v7}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 475
    :cond_2
    check-cast v5, Ljava/util/List;

    check-cast v5, Ljava/lang/Iterable;

    .line 476
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v5, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 477
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 478
    check-cast v5, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    .line 452
    invoke-virtual {v5}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->getOptionValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 479
    :cond_3
    move-object v5, v2

    check-cast v5, Ljava/util/List;

    .line 453
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10;->$props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->getAssignmentEngine()Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;

    move-result-object v6

    .line 454
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$toSelectOptionValuesScreen$10;->$props:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;

    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesProps;->isIncrementalUpdate()Z

    move-result v7

    .line 446
    new-instance v8, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$Save;

    move-object v2, v8

    move-object v4, v1

    invoke-direct/range {v2 .. v7}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$Save;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/squareup/shared/catalog/engines/itemoptionassignment/ItemOptionAssignmentEngine;Z)V

    invoke-interface {v0, v8}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :cond_4
    return-void
.end method
