.class public final Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;
.super Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action;
.source "SelectOptionValuesWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SaveConfirmed"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesWorkflow.kt\ncom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed\n*L\n1#1,468:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\u000f\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0003J-\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00032\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0005H\u00d6\u0001J\u0018\u0010\u0019\u001a\u00020\u001a*\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\u001bH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action;",
        "isRemoveOptionSet",
        "",
        "optionId",
        "",
        "originalOptionValues",
        "",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "(ZLjava/lang/String;Ljava/util/List;)V",
        "()Z",
        "getOptionId",
        "()Ljava/lang/String;",
        "getOriginalOptionValues",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isRemoveOptionSet:Z

.field private final optionId:Ljava/lang/String;

.field private final originalOptionValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;)V"
        }
    .end annotation

    const-string v0, "optionId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalOptionValues"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 299
    invoke-direct {p0, v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->isRemoveOptionSet:Z

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->optionId:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->originalOptionValues:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;ZLjava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-boolean p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->isRemoveOptionSet:Z

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->optionId:Ljava/lang/String;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->originalOptionValues:Ljava/util/List;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->copy(ZLjava/lang/String;Ljava/util/List;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 301
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->isRemoveOptionSet:Z

    if-eqz v0, :cond_0

    .line 302
    sget-object v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput$RemovedOptionSet;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput$RemovedOptionSet;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void

    .line 305
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;->getNewValueInEditing()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 306
    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Updater;->getNextState()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesState;->newValueIsDuplicate()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Shouldn\'t have made it to WarnChanges with duplicate new value"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 312
    :cond_2
    :goto_0
    sget-object v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput$Updated;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesOutput$Updated;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method

.method public final component1()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->isRemoveOptionSet:Z

    return v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->optionId:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->originalOptionValues:Ljava/util/List;

    return-object v0
.end method

.method public final copy(ZLjava/lang/String;Ljava/util/List;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;)",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;"
        }
    .end annotation

    const-string v0, "optionId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "originalOptionValues"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;-><init>(ZLjava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->isRemoveOptionSet:Z

    iget-boolean v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->isRemoveOptionSet:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->optionId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->optionId:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->originalOptionValues:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->originalOptionValues:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getOptionId()Ljava/lang/String;
    .locals 1

    .line 297
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->optionId:Ljava/lang/String;

    return-object v0
.end method

.method public final getOriginalOptionValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    .line 298
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->originalOptionValues:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->isRemoveOptionSet:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->optionId:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->originalOptionValues:Ljava/util/List;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    return v0
.end method

.method public final isRemoveOptionSet()Z
    .locals 1

    .line 296
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->isRemoveOptionSet:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SaveConfirmed(isRemoveOptionSet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->isRemoveOptionSet:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", optionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->optionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", originalOptionValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesWorkflow$Action$SaveConfirmed;->originalOptionValues:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
