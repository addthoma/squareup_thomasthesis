.class public final Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;
.super Ljava/lang/Object;
.source "SelectOptionValuesScreen.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\u0013\u0010\u0015\u001a\u00020\u00052\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0096\u0002J\t\u0010\u0018\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\nH\u00d6\u0001J\u0019\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0008R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;",
        "Landroid/os/Parcelable;",
        "optionValue",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "isSelected",
        "",
        "isDuplicate",
        "(Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)V",
        "()Z",
        "name",
        "",
        "getName",
        "()Ljava/lang/String;",
        "getOptionValue",
        "()Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "component1",
        "component2",
        "component3",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final isDuplicate:Z

.field private final isSelected:Z

.field private final name:Ljava/lang/String;

.field private final optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection$Creator;

    invoke-direct {v0}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection$Creator;-><init>()V

    sput-object v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)V
    .locals 1

    const-string v0, "optionValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    iput-boolean p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected:Z

    iput-boolean p3, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isDuplicate:Z

    .line 22
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {p1}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getName()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->name:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 20
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZILjava/lang/Object;)Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-boolean p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected:Z

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-boolean p3, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isDuplicate:Z

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->copy(Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isDuplicate:Z

    return v0
.end method

.method public final copy(Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;
    .locals 1

    const-string v0, "optionValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;ZZ)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 25
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 27
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    iget-object v2, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected:Z

    iget-boolean v2, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected:Z

    if-ne v0, v2, :cond_0

    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isDuplicate:Z

    iget-boolean p1, p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isDuplicate:Z

    if-ne v0, p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final getOptionValue()Lcom/squareup/cogs/itemoptions/ItemOptionValue;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isDuplicate:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public final isDuplicate()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isDuplicate:Z

    return v0
.end method

.method public final isSelected()Z
    .locals 1

    .line 19
    iget-boolean v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OptionValueSelection(optionValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isDuplicate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isDuplicate:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->optionValue:Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-boolean p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isDuplicate:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
