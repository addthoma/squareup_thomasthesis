.class public final Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;
.super Ljava/lang/Object;
.source "SelectOptionValuesLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$Factory;,
        Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSelectOptionValuesLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SelectOptionValuesLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 7 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,387:1\n1360#2:388\n1429#2,3:389\n1847#2,3:392\n49#3:395\n50#3,3:401\n53#3:454\n599#4,4:396\n601#4:400\n310#5,6:404\n310#5,6:410\n310#5,6:416\n310#5,3:422\n313#5,3:431\n310#5,6:434\n310#5,6:440\n310#5,6:446\n35#6,6:425\n43#7,2:452\n*E\n*S KotlinDebug\n*F\n+ 1 SelectOptionValuesLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner\n*L\n261#1:388\n261#1,3:389\n302#1,3:392\n67#1:395\n67#1,3:401\n67#1:454\n67#1,4:396\n67#1:400\n67#1,6:404\n67#1,6:410\n67#1,6:416\n67#1,3:422\n67#1,3:431\n67#1,6:434\n67#1,6:440\n67#1,6:446\n67#1,6:425\n67#1,2:452\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u001f B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J$\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\r0\u00132\u0006\u0010\u0014\u001a\u00020\u00022\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0013H\u0002J$\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\r0\u00132\u0006\u0010\u0014\u001a\u00020\u00022\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0013H\u0002J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00192\u0006\u0010\u0014\u001a\u00020\u0002H\u0002J\u0010\u0010\u001d\u001a\u00020\u00192\u0006\u0010\u0014\u001a\u00020\u0002H\u0002J\u0010\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u0014\u001a\u00020\u0002H\u0002R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n \n*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n \n*\u0004\u0018\u00010\u00110\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;",
        "view",
        "Landroid/view/View;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "kotlin.jvm.PlatformType",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "searchBar",
        "Lcom/squareup/noho/NohoEditRow;",
        "getFullScreenRows",
        "",
        "rendering",
        "optionValueRows",
        "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$OptionValueSelectionRow;",
        "getSearchResultRows",
        "showRendering",
        "",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "updateOptionValueList",
        "updateSearchBar",
        "Factory",
        "SelectOptionValuesRow",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final searchBar:Lcom/squareup/noho/NohoEditRow;

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->view:Landroid/view/View;

    .line 64
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 65
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$id;->option_value_search_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoEditRow;

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->searchBar:Lcom/squareup/noho/NohoEditRow;

    .line 66
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/items/assignitemoptions/impl/R$id;->select_option_value_recycler:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 67
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 395
    sget-object v0, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 396
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 397
    new-instance v0, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v0}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 401
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 402
    invoke-virtual {p2}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 68
    sget-object p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 405
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 74
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$2$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$2$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 405
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 404
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 411
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 91
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$3$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$3$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 101
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$3$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$3$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 410
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 417
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 105
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$4$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$4$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 120
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$4$2;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$4$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p2, v1}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 416
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 423
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$4;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 124
    sget v1, Lcom/squareup/items/assignitemoptions/impl/R$layout;->select_option_values_add_option_row:I

    .line 425
    new-instance v2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$create$1;

    invoke-direct {v2, v1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$create$1;-><init>(I)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 423
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 422
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 435
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$5;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$5;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 159
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$6$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$6$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 435
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 434
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 441
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$6;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$6;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 174
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$7$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$7$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 441
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 440
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 447
    new-instance p2, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$7;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$$special$$inlined$row$7;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 191
    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$8$1;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$recycler$1$8$1;

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 447
    check-cast p2, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 446
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 452
    new-instance p2, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {p2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v1, 0x0

    .line 211
    invoke-virtual {p2, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    check-cast p2, Lcom/squareup/cycler/ExtensionSpec;

    .line 452
    invoke-virtual {v0, p2}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 399
    invoke-virtual {v0, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 396
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final getFullScreenRows(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$OptionValueSelectionRow;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow;",
            ">;"
        }
    .end annotation

    .line 302
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 303
    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$SectionHeader;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getOptionName()Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$SectionHeader;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getAreAllOptionsShown()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_3

    .line 305
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getOptionValueSelections()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 392
    instance-of v4, v1, Ljava/util/Collection;

    if-eqz v4, :cond_1

    move-object v4, v1

    check-cast v4, Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 393
    :cond_1
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    .line 305
    invoke-virtual {v4}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected()Z

    move-result v4

    xor-int/2addr v4, v3

    if-eqz v4, :cond_2

    const/4 v1, 0x0

    .line 307
    :goto_0
    new-instance v4, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$AllOptionValuesSelectionRow;

    new-instance v5, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$getFullScreenRows$$inlined$apply$lambda$1;

    invoke-direct {v5, v1, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$getFullScreenRows$$inlined$apply$lambda$1;-><init>(ZLcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;Ljava/util/List;)V

    check-cast v5, Lkotlin/jvm/functions/Function1;

    invoke-direct {v4, v1, v5}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$AllOptionValuesSelectionRow;-><init>(ZLkotlin/jvm/functions/Function1;)V

    .line 306
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 314
    :cond_3
    check-cast p2, Ljava/util/Collection;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 318
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getHasReachedOptionValueNumberLimit()Z

    move-result p2

    if-eqz p2, :cond_4

    .line 319
    sget-object p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$OptionValueNumberLimitHelpTextRow;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$OptionValueNumberLimitHelpTextRow;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 322
    :cond_4
    new-instance p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$AddNewOptionValueRow;

    .line 323
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getNewValueInEditing()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    .line 324
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getShouldHighlightDuplicateNewValueName()Z

    move-result v4

    .line 325
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getShouldFocusOnNewValueRowAndShowKeyboard()Z

    move-result v5

    .line 326
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getOnNewOptionValuePromoted()Lkotlin/jvm/functions/Function0;

    move-result-object v6

    .line 322
    invoke-direct {p2, v1, v4, v5, v6}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$AddNewOptionValueRow;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;ZZLkotlin/jvm/functions/Function0;)V

    .line 321
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 331
    :goto_1
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getRemoveButtonState()Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    move-result-object p2

    sget-object v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;->HIDE_DELETE_BUTTON:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    if-eq p2, v1, :cond_6

    .line 333
    new-instance p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$RemoveOptionSetButtonRow;

    .line 334
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getRemoveButtonState()Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    move-result-object v1

    sget-object v4, Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;->SHOW_CONFIRM_DELETE_BUTTON:Lcom/squareup/items/assignitemoptions/selectoptionvalues/RemoveButtonState;

    if-ne v1, v4, :cond_5

    const/4 v2, 0x1

    .line 335
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getOnRemoveOptionSet()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    .line 333
    invoke-direct {p2, v2, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$RemoveOptionSetButtonRow;-><init>(ZLkotlin/jvm/functions/Function0;)V

    .line 332
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    return-object v0
.end method

.method private final getSearchResultRows(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$OptionValueSelectionRow;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow;",
            ">;"
        }
    .end annotation

    .line 288
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getHasReachedOptionValueNumberLimit()Z

    move-result v0

    if-nez v0, :cond_4

    .line 289
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getSearchText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const/4 v0, 0x1

    if-eqz p2, :cond_1

    invoke-static {p2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p2, 0x1

    :goto_1
    xor-int/2addr p2, v0

    if-eqz p2, :cond_3

    .line 290
    new-instance p2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$CreateFromSearchButtonRow;

    .line 291
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getSearchText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 292
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getOnNewValueFromCreateButton()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    .line 290
    invoke-direct {p2, v0, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$CreateFromSearchButtonRow;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function0;)V

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    goto :goto_2

    .line 289
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Check failed."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_4
    :goto_2
    return-object p2
.end method

.method private final updateActionBar(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;)V
    .locals 6

    .line 244
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 227
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 229
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getOptionName()Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    .line 230
    new-instance v2, Lcom/squareup/resources/PhraseModel;

    sget v3, Lcom/squareup/items/assignitemoptions/impl/R$string;->combined_name_and_display_name:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 231
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getOptionName()Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "option_name"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v2

    .line 232
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getOptionName()Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "option_display_name"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v2

    check-cast v2, Lcom/squareup/resources/TextModel;

    goto :goto_0

    .line 234
    :cond_0
    new-instance v2, Lcom/squareup/resources/FixedText;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getOptionName()Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;->getName()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v2, v3}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    .line 228
    :goto_0
    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 236
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$updateActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$updateActionBar$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 238
    sget-object v2, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 239
    new-instance v3, Lcom/squareup/util/ViewString$ResourceString;

    sget v4, Lcom/squareup/items/assignitemoptions/impl/R$string;->select_option_values_action_bar_primary_button_text:I

    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v3, Lcom/squareup/resources/TextModel;

    .line 240
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->isDoneButtonEnabled()Z

    move-result v4

    .line 241
    new-instance v5, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$updateActionBar$2;

    invoke-direct {v5, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$updateActionBar$2;-><init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;)V

    check-cast v5, Lkotlin/jvm/functions/Function0;

    .line 237
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 244
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateOptionValueList(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;)V
    .locals 8

    .line 261
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getOptionValueSelections()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 388
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 389
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 390
    check-cast v2, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;

    .line 262
    new-instance v3, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$OptionValueSelectionRow;

    .line 263
    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->getName()Ljava/lang/String;

    move-result-object v4

    .line 264
    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isSelected()Z

    move-result v5

    .line 266
    invoke-virtual {v2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;->isDuplicate()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getShouldHighlightDuplicateNewValueName()Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x1

    goto :goto_1

    :cond_0
    const/4 v6, 0x0

    .line 267
    :goto_1
    new-instance v7, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$updateOptionValueList$$inlined$map$lambda$1;

    invoke-direct {v7, v2, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$updateOptionValueList$$inlined$map$lambda$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/OptionValueSelection;Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 262
    invoke-direct {v3, v4, v5, v6, v7}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$SelectOptionValuesRow$OptionValueSelectionRow;-><init>(Ljava/lang/String;ZZLkotlin/jvm/functions/Function1;)V

    .line 271
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 391
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 274
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->showingSearchResults()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 275
    invoke-direct {p0, p1, v1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->getSearchResultRows(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    goto :goto_2

    .line 277
    :cond_2
    invoke-direct {p0, p1, v1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->getFullScreenRows(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 279
    :goto_2
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->recycler:Lcom/squareup/cycler/Recycler;

    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$updateOptionValueList$1;

    invoke-direct {v1, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$updateOptionValueList$1;-><init>(Ljava/util/List;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final updateSearchBar(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;)V
    .locals 5

    .line 254
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->searchBar:Lcom/squareup/noho/NohoEditRow;

    const-string v1, "searchBar"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 251
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->view:Landroid/view/View;

    sget v3, Lcom/squareup/items/assignitemoptions/impl/R$string;->select_option_values_search_bar_hint_format:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 252
    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getOptionName()Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/ItemOptionName;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    const-string v4, "option"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 253
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 254
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditRow;->setHint(Ljava/lang/CharSequence;)V

    .line 255
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->searchBar:Lcom/squareup/noho/NohoEditRow;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;->getSearchText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->updateActionBar(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;)V

    .line 219
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->updateSearchBar(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;)V

    .line 220
    invoke-direct {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->updateOptionValueList(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;)V

    .line 221
    iget-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner$showRendering$1;-><init>(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 59
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesLayoutRunner;->showRendering(Lcom/squareup/items/assignitemoptions/selectoptionvalues/SelectOptionValuesScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
