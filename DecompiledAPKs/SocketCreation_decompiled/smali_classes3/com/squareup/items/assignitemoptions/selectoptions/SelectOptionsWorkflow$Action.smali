.class public abstract Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;
.super Ljava/lang/Object;
.source "SelectOptionsWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Cancel;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Complete;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Next;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Search;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Create;,
        Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$SelectMoreOption;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00080\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0008\u0005\u0006\u0007\u0008\t\n\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004\u0082\u0001\u0008\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
        "()V",
        "Cancel",
        "Complete",
        "Create",
        "MoveAssignedOption",
        "Next",
        "Search",
        "SelectMoreOption",
        "TapOption",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Cancel;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Complete;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Next;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Search;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$TapOption;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$MoveAssignedOption;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Create;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$SelectMoreOption;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 319
    invoke-direct {p0}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
            ">;)",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 319
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 319
    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
            "-",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 319
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Updater;)V

    return-void
.end method
