.class final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->render(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "newOptionName",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $optionsInUse:Ljava/util/List;

.field final synthetic $props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/Sink;Ljava/util/List;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$3;->$sink:Lcom/squareup/workflow/Sink;

    iput-object p2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$3;->$optionsInUse:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$3;->$props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$3;->invoke(Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/String;)V
    .locals 4

    .line 118
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$3;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Create;

    .line 120
    iget-object v2, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$3;->$optionsInUse:Ljava/util/List;

    .line 121
    iget-object v3, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$3;->$props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    invoke-virtual {v3}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->getMaxOptionsPerItem()I

    move-result v3

    .line 118
    invoke-direct {v1, p1, v2, v3}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Create;-><init>(Ljava/lang/String;Ljava/util/List;I)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
