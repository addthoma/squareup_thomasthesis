.class final Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$9;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectOptionsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow;->render(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
        "+",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
        "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
        "createOptionForItemOutput",
        "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;


# direct methods
.method constructor <init>(Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$9;->$props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsState;",
            "Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "createOptionForItemOutput"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    instance-of v0, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput$Canceled;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$SelectMoreOption;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$SelectMoreOption;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_1

    .line 194
    :cond_0
    instance-of p1, p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput$Created;

    if-eqz p1, :cond_2

    .line 195
    iget-object p1, p0, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$9;->$props:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;

    invoke-virtual {p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsProps;->isIncrementalAssignment()Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Complete;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$Complete;

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$SelectMoreOption;->INSTANCE:Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action$SelectMoreOption;

    :goto_0
    check-cast p1, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$Action;

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    :goto_1
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 58
    check-cast p1, Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectoptions/SelectOptionsWorkflow$render$9;->invoke(Lcom/squareup/items/assignitemoptions/createoption/CreateOptionForItemOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
