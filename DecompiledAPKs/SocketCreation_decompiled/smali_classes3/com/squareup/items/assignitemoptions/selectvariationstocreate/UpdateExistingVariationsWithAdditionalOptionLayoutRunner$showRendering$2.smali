.class final Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$2;
.super Lkotlin/jvm/internal/Lambda;
.source "UpdateExistingVariationsWithAdditionalOptionLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner;->showRendering(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Update<",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$OptionValueRow;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUpdateExistingVariationsWithAdditionalOptionLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UpdateExistingVariationsWithAdditionalOptionLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$2\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,100:1\n1360#2:101\n1429#2,3:102\n*E\n*S KotlinDebug\n*F\n+ 1 UpdateExistingVariationsWithAdditionalOptionLayoutRunner.kt\ncom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$2\n*L\n68#1:101\n68#1,3:102\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/Update;",
        "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$OptionValueRow;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rendering:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;


# direct methods
.method constructor <init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$2;->$rendering:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/cycler/Update;

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$2;->invoke(Lcom/squareup/cycler/Update;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/Update;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Update<",
            "Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$OptionValueRow;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$2;->$rendering:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;->getOptionValueSelections()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 101
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 102
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 103
    check-cast v2, Lcom/squareup/cogs/itemoptions/ItemOptionValue;

    .line 69
    new-instance v3, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$OptionValueRow;

    .line 70
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getName()Ljava/lang/String;

    move-result-object v4

    .line 71
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getValueId()Ljava/lang/String;

    move-result-object v5

    .line 72
    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOptionValue;->getValueId()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$2;->$rendering:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;

    invoke-virtual {v7}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionScreen;->getSelectedValueId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    new-instance v7, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$2$$special$$inlined$map$lambda$1;

    invoke-direct {v7, v2, p0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$2$$special$$inlined$map$lambda$1;-><init>(Lcom/squareup/cogs/itemoptions/ItemOptionValue;Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$showRendering$2;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 69
    invoke-direct {v3, v4, v5, v6, v7}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/UpdateExistingVariationsWithAdditionalOptionLayoutRunner$OptionValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/functions/Function1;)V

    .line 76
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 104
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 78
    invoke-static {v1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setData(Lcom/squareup/cycler/DataSource;)V

    return-void
.end method
