.class final Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$showRendering$rows$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectVariationsToCreateLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner;->showRendering(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Boolean;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "isChecked",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rendering:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;


# direct methods
.method constructor <init>(Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$showRendering$rows$1;->$rendering:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$showRendering$rows$1;->invoke(Z)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Z)V
    .locals 1

    .line 192
    iget-object v0, p0, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateLayoutRunner$showRendering$rows$1;->$rendering:Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;

    invoke-virtual {v0}, Lcom/squareup/items/assignitemoptions/selectvariationstocreate/SelectVariationsToCreateScreen;->getOnAllCombinationsSelectionChanged()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
