.class public final synthetic Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 3

    invoke-static {}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->values()[Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->WELCOME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->APPLETS_DRAWER:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_APPLET:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION_SAD_PATH_SEARCH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_ENTER_NAME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_ENTER_PRICE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_SAVE_ITEM:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->EDIT_CATEGORY:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ADJUST_INVENTORY:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->TUTORIAL_COMPLETE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialPhoneHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->SKIP:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1

    return-void
.end method
