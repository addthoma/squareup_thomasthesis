.class public final enum Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;
.super Ljava/lang/Enum;
.source "CreateItemTutorialHandler.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/tutorial/CreateItemTutorialHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0019\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;",
        "",
        "(Ljava/lang/String;I)V",
        "WELCOME",
        "SKIP",
        "ITEM_DIALOG_ENTER_NAME",
        "ITEM_DIALOG_ENTER_PRICE",
        "ITEM_DIALOG_SAVE_ITEM",
        "EDIT_CATEGORY",
        "ADJUST_INVENTORY",
        "ITEM_DIALOG_SAD_PATH",
        "TUTORIAL_COMPLETE",
        "APPLETS_DRAWER",
        "ITEMS_APPLET",
        "ALL_ITEMS_SECTION",
        "ALL_ITEMS_SECTION_SAD_PATH",
        "ALL_ITEMS_SECTION_SAD_PATH_SEARCH",
        "ITEMS_GRID",
        "ITEMS_GRID_SAD_PATH",
        "IN_EDIT_MODE",
        "IN_EDIT_MODE_SAD_PATH",
        "ITEM_LIST_SCREEN",
        "ITEM_DRAG_AND_DROP",
        "ITEM_DRAG_AND_DROP_SAD_PATH",
        "DONE_EDITING",
        "DONE_EDITING_SAD_PATH",
        "items-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ADJUST_INVENTORY:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ALL_ITEMS_SECTION:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ALL_ITEMS_SECTION_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ALL_ITEMS_SECTION_SAD_PATH_SEARCH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum APPLETS_DRAWER:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum DONE_EDITING:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum DONE_EDITING_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum EDIT_CATEGORY:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum IN_EDIT_MODE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum IN_EDIT_MODE_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ITEMS_APPLET:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ITEMS_GRID:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ITEMS_GRID_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ITEM_DIALOG_ENTER_NAME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ITEM_DIALOG_ENTER_PRICE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ITEM_DIALOG_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ITEM_DIALOG_SAVE_ITEM:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ITEM_DRAG_AND_DROP:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ITEM_DRAG_AND_DROP_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum ITEM_LIST_SCREEN:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum SKIP:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum TUTORIAL_COMPLETE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

.field public static final enum WELCOME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x17

    new-array v0, v0, [Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/4 v2, 0x0

    const-string v3, "WELCOME"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->WELCOME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/4 v2, 0x1

    const-string v3, "SKIP"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->SKIP:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/4 v2, 0x2

    const-string v3, "ITEM_DIALOG_ENTER_NAME"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_ENTER_NAME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/4 v2, 0x3

    const-string v3, "ITEM_DIALOG_ENTER_PRICE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_ENTER_PRICE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/4 v2, 0x4

    const-string v3, "ITEM_DIALOG_SAVE_ITEM"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_SAVE_ITEM:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/4 v2, 0x5

    const-string v3, "EDIT_CATEGORY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->EDIT_CATEGORY:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/4 v2, 0x6

    const-string v3, "ADJUST_INVENTORY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ADJUST_INVENTORY:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/4 v2, 0x7

    const-string v3, "ITEM_DIALOG_SAD_PATH"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0x8

    const-string v3, "TUTORIAL_COMPLETE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->TUTORIAL_COMPLETE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0x9

    const-string v3, "APPLETS_DRAWER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->APPLETS_DRAWER:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0xa

    const-string v3, "ITEMS_APPLET"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_APPLET:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0xb

    const-string v3, "ALL_ITEMS_SECTION"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0xc

    const-string v3, "ALL_ITEMS_SECTION_SAD_PATH"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0xd

    const-string v3, "ALL_ITEMS_SECTION_SAD_PATH_SEARCH"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ALL_ITEMS_SECTION_SAD_PATH_SEARCH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0xe

    const-string v3, "ITEMS_GRID"

    invoke-direct {v1, v3, v2}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_GRID:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const-string v2, "ITEMS_GRID_SAD_PATH"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_GRID_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const-string v2, "IN_EDIT_MODE"

    const/16 v3, 0x10

    invoke-direct {v1, v2, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->IN_EDIT_MODE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const-string v2, "IN_EDIT_MODE_SAD_PATH"

    const/16 v3, 0x11

    invoke-direct {v1, v2, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->IN_EDIT_MODE_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const-string v2, "ITEM_LIST_SCREEN"

    const/16 v3, 0x12

    invoke-direct {v1, v2, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_LIST_SCREEN:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const-string v2, "ITEM_DRAG_AND_DROP"

    const/16 v3, 0x13

    invoke-direct {v1, v2, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DRAG_AND_DROP:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const-string v2, "ITEM_DRAG_AND_DROP_SAD_PATH"

    const/16 v3, 0x14

    invoke-direct {v1, v2, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DRAG_AND_DROP_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const-string v2, "DONE_EDITING"

    const/16 v3, 0x15

    invoke-direct {v1, v2, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->DONE_EDITING:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const-string v2, "DONE_EDITING_SAD_PATH"

    const/16 v3, 0x16

    invoke-direct {v1, v2, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->DONE_EDITING_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->$VALUES:[Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;
    .locals 1

    const-class v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    return-object p0
.end method

.method public static values()[Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;
    .locals 1

    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->$VALUES:[Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v0}, [Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    return-object v0
.end method
