.class public final Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;
.super Lcom/squareup/items/tutorial/CreateItemTutorialHandler;
.source "CreateItemTutorialTabletHandler.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0018\u0000  2\u00020\u0001:\u0001 Ba\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0008\u0008\u0001\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0002\u0010\u0018J\u001e\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001a2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;",
        "Lcom/squareup/items/tutorial/CreateItemTutorialHandler;",
        "orderEntryAppletGateway",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "orderEntryScreenState",
        "Lcom/squareup/orderentry/OrderEntryScreenState;",
        "orderEntryPages",
        "Lcom/squareup/orderentry/pages/OrderEntryPages;",
        "appletsDrawerRunner",
        "Lcom/squareup/applet/AppletsDrawerRunner;",
        "flow",
        "Lflow/Flow;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "tutorialApi",
        "Lcom/squareup/register/tutorial/TutorialApi;",
        "dialogFactory",
        "Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;",
        "tooltipFactory",
        "Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;",
        "preferences",
        "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
        "(Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/main/Home;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/applet/AppletsDrawerRunner;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V",
        "tileTag",
        "",
        "handleEvent",
        "Lcom/squareup/tutorialv2/TutorialState;",
        "name",
        "value",
        "",
        "Companion",
        "items-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$Companion;

.field public static final DO_ACTIVATE_EDIT_MODE:Ljava/lang/String; = "Activate Edit Mode"

.field public static final DO_CLOSE_APPLETS_DRAWER:Ljava/lang/String; = "Close Applets Drawer"

.field public static final EDIT_MODE:Ljava/lang/String; = "In Edit Mode"


# instance fields
.field private final appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

.field private final home:Lcom/squareup/ui/main/Home;

.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private tileTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->Companion:Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/main/Home;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/orderentry/pages/OrderEntryPages;Lcom/squareup/applet/AppletsDrawerRunner;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V
    .locals 13
    .param p11    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v7, p0

    move-object v8, p1

    move-object v9, p2

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    move-object/from16 v12, p5

    const-string v0, "orderEntryAppletGateway"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "home"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryScreenState"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryPages"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appletsDrawerRunner"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    move-object/from16 v3, p6

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    move-object/from16 v5, p7

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tutorialApi"

    move-object/from16 v4, p8

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dialogFactory"

    move-object/from16 v1, p9

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tooltipFactory"

    move-object/from16 v2, p10

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "preferences"

    move-object/from16 v6, p11

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 82
    invoke-direct/range {v0 .. v6}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;Lflow/Flow;Lcom/squareup/register/tutorial/TutorialApi;Lcom/squareup/analytics/Analytics;Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)V

    iput-object v8, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iput-object v9, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->home:Lcom/squareup/ui/main/Home;

    iput-object v10, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    iput-object v11, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    iput-object v12, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    const-string v0, ""

    .line 91
    iput-object v0, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->tileTag:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;
    .locals 18

    move-object/from16 v7, p0

    move-object/from16 v0, p1

    move-object/from16 v3, p2

    const-string v1, "CREATE_ITEM_TUTORIAL"

    .line 97
    invoke-static {v1}, Ltimber/log/Timber;->tag(Ljava/lang/String;)Ltimber/log/Timber$Tree;

    move-result-object v1

    .line 98
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleEvent (Tablet) -- State: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, ", Name: \'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\', Value: \'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v4, 0x27

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v4}, Ltimber/log/Timber$Tree;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 100
    move-object v2, v1

    check-cast v2, Lcom/squareup/tutorialv2/TutorialState;

    .line 101
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v4

    sget-object v5, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v4}, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ordinal()I

    move-result v4

    aget v4, v5, v4

    const-string v5, "Item Changes Discarded"

    const-string v6, "Close Applets Drawer"

    const-string v8, "TutorialV2DialogScreen primary tapped"

    const-string v9, "TutorialV2DialogScreen secondary tapped"

    const-string v10, "Edit Discount Dismissed"

    const-string v11, "Edit Discount Shown"

    const-string v13, "Item List Screen Dismissed"

    const-string v14, "Favorites Grid Done Editing Tapped"

    const-string v15, "Favorites Tab Selected"

    const-string v12, "Item List Screen Shown"

    const-string v1, "Activate Edit Mode"

    move-object/from16 v16, v2

    const-string v2, "In Edit Mode"

    move-object/from16 v17, v2

    const-string v2, "Favorites Grid Tiles Loaded"

    packed-switch v4, :pswitch_data_0

    goto/16 :goto_7

    .line 445
    :pswitch_0
    invoke-virtual/range {p0 .. p1}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleSkip(Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    :pswitch_1
    if-nez v0, :cond_0

    goto/16 :goto_7

    .line 424
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    goto/16 :goto_7

    .line 426
    :sswitch_0
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 427
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->DONE_EDITING:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    const/4 v0, 0x0

    .line 428
    invoke-virtual {v7, v2, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    .line 429
    iget-object v0, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->startEditing()V

    goto/16 :goto_8

    .line 433
    :sswitch_1
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_0

    :sswitch_2
    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_0

    :sswitch_3
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_0

    :sswitch_4
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 440
    :goto_0
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->DONE_EDITING:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    const/4 v0, 0x0

    .line 441
    invoke-virtual {v7, v2, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    :pswitch_2
    if-nez v0, :cond_1

    goto/16 :goto_7

    .line 394
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_1

    goto/16 :goto_7

    .line 396
    :sswitch_5
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 397
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DRAG_AND_DROP:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    const/4 v0, 0x0

    .line 398
    invoke-virtual {v7, v2, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    .line 399
    iget-object v0, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->startEditing()V

    goto/16 :goto_8

    .line 403
    :sswitch_6
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 404
    instance-of v0, v3, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-eqz v0, :cond_17

    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne v3, v0, :cond_17

    .line 405
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DRAG_AND_DROP:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    const/4 v0, 0x0

    .line 406
    invoke-virtual {v7, v2, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    .line 411
    :sswitch_7
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_1

    :sswitch_8
    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_1

    :sswitch_9
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_1

    :sswitch_a
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 418
    :goto_1
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DRAG_AND_DROP:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    const/4 v0, 0x0

    .line 419
    invoke-virtual {v7, v2, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    :pswitch_3
    if-nez v0, :cond_2

    goto/16 :goto_7

    .line 353
    :cond_2
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    goto/16 :goto_7

    .line 384
    :sswitch_b
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 385
    instance-of v0, v3, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-eqz v0, :cond_17

    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-ne v3, v0, :cond_17

    .line 386
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_GRID:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    const/4 v0, 0x0

    .line 387
    invoke-virtual {v7, v2, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    .line 355
    :sswitch_c
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 356
    iget-object v0, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->appletsDrawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletsDrawerRunner;->toggleDrawer()V

    goto/16 :goto_7

    :sswitch_d
    const-string v1, "Price Entry Screen Dismissed"

    .line 360
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_2

    :sswitch_e
    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_2

    :sswitch_f
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_2

    :sswitch_10
    const-string v1, "Leaving SelectMethodScreen"

    .line 375
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_3

    :sswitch_11
    const-string v1, "Applets Drawer Closed"

    .line 360
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_2

    :sswitch_12
    const-string v1, "Finished with receipt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 369
    :goto_2
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_GRID:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    const/4 v0, 0x0

    .line 370
    invoke-virtual {v7, v2, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    :sswitch_13
    const-string v1, "Leaving SelectMethodScreen, card amount in range"

    .line 375
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 377
    :goto_3
    instance-of v0, v3, Lcom/squareup/tenderpayment/TenderPaymentResult;

    if-eqz v0, :cond_17

    sget-object v0, Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;->INSTANCE:Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 378
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_GRID:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    const/4 v0, 0x0

    .line 379
    invoke-virtual {v7, v2, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    :pswitch_4
    if-nez v0, :cond_3

    goto/16 :goto_7

    .line 328
    :cond_3
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v2, -0x45ba1ef6

    if-eq v1, v2, :cond_6

    const v2, -0x2a30eac4

    if-eq v1, v2, :cond_5

    const v2, -0x234e9f5f

    if-eq v1, v2, :cond_4

    goto/16 :goto_7

    .line 330
    :cond_4
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 331
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_SHOW_MODAL_DONE:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 332
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getFlow()Lflow/Flow;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getDialogFactory()Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;->completeModal()Lcom/squareup/tutorialv2/TutorialV2DialogScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 333
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->markAsCompleted()V

    goto/16 :goto_7

    .line 337
    :cond_5
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 338
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    .line 339
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->WELCOME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 340
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_DONE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 341
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getTutorialApi()Lcom/squareup/register/tutorial/TutorialApi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/register/tutorial/TutorialApi;->forceStartFirstPaymentTutorial()V

    goto/16 :goto_8

    .line 345
    :cond_6
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 346
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState;->FINISHED:Lcom/squareup/tutorialv2/TutorialState;

    .line 347
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->WELCOME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 348
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_DONE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    goto/16 :goto_8

    :pswitch_5
    if-nez v0, :cond_7

    goto/16 :goto_7

    .line 306
    :cond_7
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const v4, -0x7b9e65ef

    if-eq v1, v4, :cond_a

    const v4, -0x234e9f5f

    if-eq v1, v4, :cond_9

    const v2, -0x1d5a872e

    if-eq v1, v2, :cond_8

    goto/16 :goto_7

    .line 321
    :cond_8
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 322
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->DONE_EDITING_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 323
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto/16 :goto_8

    .line 314
    :cond_9
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 315
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->CREATE_ITEM_TUTORIAL_DONE_EDITING:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 316
    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->TUTORIAL_COMPLETE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v1}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 317
    invoke-virtual/range {p0 .. p2}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    .line 308
    :cond_a
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 309
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v1

    invoke-virtual {v7, v1, v0, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 310
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getTooltipFactory()Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->tapDoneEditing()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    :pswitch_6
    if-nez v0, :cond_b

    goto/16 :goto_7

    .line 268
    :cond_b
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_3

    goto/16 :goto_7

    .line 283
    :sswitch_14
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 284
    instance-of v0, v3, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-eqz v0, :cond_17

    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-eq v3, v0, :cond_17

    .line 285
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DRAG_AND_DROP_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 286
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto/16 :goto_8

    :sswitch_15
    const-string v1, "Favorites Grid Drag and Drop Item"

    .line 276
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 277
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->DONE_EDITING:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 278
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CREATE_ITEM_TUTORIAL_DRAG_ITEM:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 279
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getTooltipFactory()Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->tapDoneEditing()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    .line 299
    :sswitch_16
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 300
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DRAG_AND_DROP_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 301
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto/16 :goto_8

    .line 291
    :sswitch_17
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 292
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DRAG_AND_DROP_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 293
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v0

    invoke-virtual {v7, v0, v1, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 294
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->showSkipModal()V

    .line 295
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto/16 :goto_8

    .line 270
    :sswitch_18
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 271
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v1

    invoke-virtual {v7, v1, v0, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 272
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getTooltipFactory()Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    move-result-object v0

    iget-object v1, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->tileTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->dragAndDropItem(Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    .line 265
    :pswitch_7
    invoke-virtual/range {p0 .. p2}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleItemDialogSadPath(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    .line 263
    :pswitch_8
    invoke-virtual/range {p0 .. p2}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleAdjustInventory(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    .line 261
    :pswitch_9
    invoke-virtual/range {p0 .. p2}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEditCategory(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    .line 255
    :pswitch_a
    sget-object v4, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->IN_EDIT_MODE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    new-instance v1, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$handleEvent$1;

    invoke-direct {v1, v7}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler$handleEvent$1;-><init>(Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;)V

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function0;

    const-string v5, "In Edit Mode"

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleItemSaveEvent(Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Lkotlin/jvm/functions/Function0;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    .line 253
    :pswitch_b
    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->IN_EDIT_MODE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-object/from16 v4, v17

    invoke-virtual {v7, v0, v3, v1, v4}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleItemPriceEvent(Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    :pswitch_c
    move-object/from16 v4, v17

    .line 251
    sget-object v1, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->IN_EDIT_MODE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0, v3, v1, v4}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleItemNameEvent(Ljava/lang/String;Ljava/lang/Object;Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    :pswitch_d
    move-object/from16 v4, v17

    if-nez v0, :cond_c

    goto/16 :goto_7

    .line 223
    :cond_c
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_4

    goto/16 :goto_7

    :sswitch_19
    const-string v1, "Item Create Clicked"

    .line 231
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 232
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_DIALOG_ENTER_NAME:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    goto/16 :goto_7

    .line 236
    :sswitch_1a
    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_4

    .line 246
    :sswitch_1b
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 247
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto/16 :goto_8

    .line 225
    :sswitch_1c
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 226
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v1

    invoke-virtual {v7, v1, v0, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 227
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getTooltipFactory()Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->selectCreateItemTablet()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    .line 236
    :sswitch_1d
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_4

    :sswitch_1e
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 241
    :goto_4
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->IN_EDIT_MODE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    const/4 v0, 0x0

    .line 242
    invoke-virtual {v7, v4, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    :pswitch_e
    move-object/from16 v4, v17

    if-nez v0, :cond_d

    goto/16 :goto_7

    .line 213
    :cond_d
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v3, 0x7049e72c

    if-eq v2, v3, :cond_e

    goto/16 :goto_7

    .line 215
    :cond_e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 216
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->IN_EDIT_MODE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    const/4 v0, 0x0

    .line 217
    invoke-virtual {v7, v4, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    .line 218
    iget-object v0, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->startEditing()V

    goto/16 :goto_8

    :pswitch_f
    move-object/from16 v4, v17

    if-nez v0, :cond_f

    goto/16 :goto_7

    .line 188
    :cond_f
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    const v5, -0x46355f22

    if-eq v2, v5, :cond_13

    const v5, -0x234e9f5f

    if-eq v2, v5, :cond_12

    const v1, 0x52993c29

    if-eq v2, v1, :cond_10

    goto/16 :goto_7

    :cond_10
    const-string v1, "Favorites Grid Empty Tile Selected"

    .line 197
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 198
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEM_LIST_SCREEN:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    if-eqz v3, :cond_11

    .line 200
    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    iput-object v0, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->tileTag:Ljava/lang/String;

    goto/16 :goto_7

    :cond_11
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_12
    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 205
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->IN_EDIT_MODE_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 206
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v0

    invoke-virtual {v7, v0, v1, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 207
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->showSkipModal()V

    .line 208
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto/16 :goto_8

    .line 190
    :cond_13
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 191
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_ADD_ITEM_TO_GRID:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 192
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v1

    invoke-virtual {v7, v1, v0, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 193
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getTooltipFactory()Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->tapEmtpyTile()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    :pswitch_10
    move-object/from16 v4, v17

    if-nez v0, :cond_14

    goto/16 :goto_7

    .line 140
    :cond_14
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_5

    goto/16 :goto_7

    .line 168
    :sswitch_1f
    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 169
    instance-of v0, v3, Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-eqz v0, :cond_17

    sget-object v0, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    if-eq v3, v0, :cond_17

    .line 170
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_GRID_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    goto/16 :goto_7

    .line 175
    :sswitch_20
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_5

    :sswitch_21
    const-string v1, "Shown SelectMethodScreen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_5

    :sswitch_22
    const-string v1, "Favorites Grid Edit Mode Entered"

    .line 154
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 155
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->IN_EDIT_MODE:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    const/4 v0, 0x0

    .line 156
    invoke-virtual {v7, v4, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    .line 175
    :sswitch_23
    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_5

    :sswitch_24
    const-string v1, "Applets Drawer Opened"

    .line 160
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 161
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_GRID_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 162
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_GRID_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v6, v1}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 163
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->showSkipModal()V

    .line 164
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto/16 :goto_8

    :sswitch_25
    const-string v1, "Price Entry Screen Shown"

    .line 175
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 182
    :goto_5
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_GRID_SAD_PATH:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 183
    sget-object v2, Lcom/squareup/tutorialv2/TutorialState;->CLEAR:Lcom/squareup/tutorialv2/TutorialState;

    goto/16 :goto_8

    .line 142
    :sswitch_26
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 143
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v1

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->CREATE_ITEM_TUTORIAL_ITEM_SETUP:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 147
    iget-object v1, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->orderEntryPages:Lcom/squareup/orderentry/pages/OrderEntryPages;

    sget-object v2, Lcom/squareup/orderentry/pages/OrderEntryPageKey;->FAV1:Lcom/squareup/orderentry/pages/OrderEntryPageKey;

    invoke-virtual {v1, v2}, Lcom/squareup/orderentry/pages/OrderEntryPages;->setCurrentPage(Lcom/squareup/orderentry/pages/OrderEntryPageKey;)V

    .line 149
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getState()Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    move-result-object v1

    invoke-virtual {v7, v1, v0, v3}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->saveAndAdvance(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;Ljava/lang/String;Ljava/lang/Object;)V

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getTooltipFactory()Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTooltipFactory;->enterEditMode()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto/16 :goto_8

    :pswitch_11
    if-nez v0, :cond_15

    goto/16 :goto_7

    .line 103
    :cond_15
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_6

    goto/16 :goto_7

    :sswitch_27
    const-string v1, "Leaving Device or Feature Tour"

    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_6

    .line 117
    :sswitch_28
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 118
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->logStartTutorialEvent()V

    .line 119
    sget-object v0, Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;->ITEMS_GRID:Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;

    invoke-virtual {v7, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->setState(Lcom/squareup/items/tutorial/CreateItemTutorialHandler$State;)V

    .line 121
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getStartedFromSupportApplet()Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x0

    .line 124
    :try_start_0
    invoke-virtual {v7, v2, v0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->handleEvent(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_8

    :cond_16
    const/4 v0, 0x0

    .line 128
    iget-object v1, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    sget-object v2, Lcom/squareup/orderentry/OrderEntryMode;->FAVORITES:Lcom/squareup/orderentry/OrderEntryMode;

    invoke-interface {v1, v2}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->historyFactoryForMode(Lcom/squareup/orderentry/OrderEntryMode;)Lcom/squareup/ui/main/HistoryFactory;

    move-result-object v1

    .line 129
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getFlow()Lflow/Flow;

    move-result-object v2

    iget-object v3, v7, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->home:Lcom/squareup/ui/main/Home;

    invoke-interface {v1, v3, v0}, Lcom/squareup/ui/main/HistoryFactory;->createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object v0

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-virtual {v2, v0, v1}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    goto :goto_7

    :sswitch_29
    const-string v1, "Start Tutorial"

    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_6

    .line 134
    :sswitch_2a
    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 135
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->skipTutorial()Lcom/squareup/tutorialv2/TutorialState;

    move-result-object v2

    goto :goto_8

    .line 105
    :sswitch_2b
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    goto :goto_6

    :sswitch_2c
    const-string v1, "Dismissed EnterPasscodeToUnlockScreen"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 112
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->logModalShowEvent()V

    .line 113
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getFlow()Lflow/Flow;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/items/tutorial/CreateItemTutorialTabletHandler;->getDialogFactory()Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/items/tutorial/CreateItemTutorialDialogFactory;->welcomeModal()Lcom/squareup/tutorialv2/TutorialV2DialogScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_17
    :goto_7
    move-object/from16 v2, v16

    :goto_8
    return-object v2

    :catchall_0
    move-exception v0

    move-object v1, v0

    .line 124
    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        -0x7d15e1c0 -> :sswitch_4
        -0x7b9e65ef -> :sswitch_3
        0x290f6fea -> :sswitch_2
        0x4c63e0b3 -> :sswitch_1
        0x7049e72c -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x7d15e1c0 -> :sswitch_a
        -0x7b9e65ef -> :sswitch_9
        0x290f6fea -> :sswitch_8
        0x4c63e0b3 -> :sswitch_7
        0x57cb5a4f -> :sswitch_6
        0x7049e72c -> :sswitch_5
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        -0x74bd0c1d -> :sswitch_13
        -0x607d3e54 -> :sswitch_12
        -0x54a1fd2c -> :sswitch_11
        -0x43d36e0b -> :sswitch_10
        0x2090c228 -> :sswitch_f
        0x290f6fea -> :sswitch_e
        0x2e4d34da -> :sswitch_d
        0x47051f20 -> :sswitch_c
        0x57cb5a4f -> :sswitch_b
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        -0x7b9e65ef -> :sswitch_18
        -0x234e9f5f -> :sswitch_17
        -0x1d5a872e -> :sswitch_16
        0x3c2270e0 -> :sswitch_15
        0x57cb5a4f -> :sswitch_14
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        -0x7d15e1c0 -> :sswitch_1e
        -0x7b9e65ef -> :sswitch_1d
        -0x1d5a872e -> :sswitch_1c
        0x2090c228 -> :sswitch_1b
        0x290f6fea -> :sswitch_1a
        0x5b502190 -> :sswitch_19
    .end sparse-switch

    :sswitch_data_5
    .sparse-switch
        -0x7b9e65ef -> :sswitch_26
        -0x5414ca3e -> :sswitch_25
        -0x3ff4162f -> :sswitch_24
        -0x1d5a872e -> :sswitch_23
        0x1a28f3ff -> :sswitch_22
        0x1fbd2f78 -> :sswitch_21
        0x2090c228 -> :sswitch_20
        0x57cb5a4f -> :sswitch_1f
    .end sparse-switch

    :sswitch_data_6
    .sparse-switch
        -0x7fe47a36 -> :sswitch_2c
        -0x7b9e65ef -> :sswitch_2b
        -0x45ba1ef6 -> :sswitch_2a
        -0x305cba64 -> :sswitch_29
        -0x2a30eac4 -> :sswitch_28
        -0x154ab8ff -> :sswitch_27
    .end sparse-switch
.end method
