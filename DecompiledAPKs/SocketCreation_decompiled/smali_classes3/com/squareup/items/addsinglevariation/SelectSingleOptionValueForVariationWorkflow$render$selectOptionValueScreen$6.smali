.class final Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$6;
.super Lkotlin/jvm/internal/Lambda;
.source "SelectSingleOptionValueForVariationWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow;->render(Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic $props:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;

.field final synthetic $state:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/RenderContext;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$6;->$context:Lcom/squareup/workflow/RenderContext;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$6;->$props:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;

    iput-object p3, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$6;->$state:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$6;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 5

    .line 87
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$6;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    .line 88
    new-instance v1, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;

    .line 89
    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$6;->$props:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;

    invoke-virtual {v2}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cogs/itemoptions/ItemOption;->getId()Ljava/lang/String;

    move-result-object v2

    .line 90
    iget-object v3, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$6;->$state:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;

    invoke-virtual {v3}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationState;->getSearchText()Ljava/lang/String;

    move-result-object v3

    .line 91
    iget-object v4, p0, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$render$selectOptionValueScreen$6;->$props:Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;

    invoke-virtual {v4}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationProps;->getOption()Lcom/squareup/cogs/itemoptions/ItemOption;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/cogs/itemoptions/ItemOption;->getAllValues()Ljava/util/List;

    move-result-object v4

    .line 88
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/items/addsinglevariation/SelectSingleOptionValueForVariationWorkflow$Action$PromoteNewValue;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 87
    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
