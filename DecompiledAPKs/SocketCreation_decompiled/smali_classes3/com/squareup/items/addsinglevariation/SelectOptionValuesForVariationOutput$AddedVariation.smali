.class public final Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;
.super Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;
.source "SelectOptionValuesForVariationOutput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddedVariation"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J#\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;",
        "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;",
        "newVariation",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "newOptionValues",
        "",
        "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
        "(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;)V",
        "getNewOptionValues",
        "()Ljava/util/List;",
        "getNewVariation",
        "()Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final newOptionValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation
.end field

.field private final newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;


# direct methods
.method public constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newVariation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newOptionValues"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 12
    invoke-direct {p0, v0}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newOptionValues:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 11
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p2

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newOptionValues:Ljava/util/List;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->copy(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/shared/catalog/models/CatalogItemVariation;
    .locals 1

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newOptionValues:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;)",
            "Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;"
        }
    .end annotation

    const-string v0, "newVariation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newOptionValues"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;

    invoke-direct {v0, p1, p2}, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newOptionValues:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newOptionValues:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNewOptionValues()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cogs/itemoptions/ItemOptionValue;",
            ">;"
        }
    .end annotation

    .line 11
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newOptionValues:Ljava/util/List;

    return-object v0
.end method

.method public final getNewVariation()Lcom/squareup/shared/catalog/models/CatalogItemVariation;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newOptionValues:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AddedVariation(newVariation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", newOptionValues="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/SelectOptionValuesForVariationOutput$AddedVariation;->newOptionValues:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
