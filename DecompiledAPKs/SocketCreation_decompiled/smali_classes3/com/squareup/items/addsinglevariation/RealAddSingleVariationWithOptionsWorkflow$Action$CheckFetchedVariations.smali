.class public final Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;
.super Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action;
.source "RealAddSingleVariationWithOptionsWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckFetchedVariations"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAddSingleVariationWithOptionsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAddSingleVariationWithOptionsWorkflow.kt\ncom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,206:1\n704#2:207\n777#2,2:208\n1360#2:210\n1429#2,3:211\n1550#2,3:214\n*E\n*S KotlinDebug\n*F\n+ 1 RealAddSingleVariationWithOptionsWorkflow.kt\ncom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations\n*L\n122#1:207\n122#1,2:208\n123#1:210\n123#1,3:211\n127#1,3:214\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B/\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0003\u00a2\u0006\u0002\u0010\tJ\u000f\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000f\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003H\u00c6\u0003J\u000f\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0003H\u00c6\u0003J9\u0010\u0011\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00032\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0003H\u00c6\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0008H\u00d6\u0001J\u0018\u0010\u0019\u001a\u00020\u001a*\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d0\u001bH\u0016R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u000bR\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000b\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;",
        "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action;",
        "fetchedVariations",
        "",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
        "localVariations",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "locallyDeletedVariationTokens",
        "",
        "(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V",
        "getFetchedVariations",
        "()Ljava/util/List;",
        "getLocalVariations",
        "getLocallyDeletedVariationTokens",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;",
        "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final fetchedVariations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field private final localVariations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field private final locallyDeletedVariationTokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const-string v0, "fetchedVariations"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localVariations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locallyDeletedVariationTokens"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 119
    invoke-direct {p0, v0}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->fetchedVariations:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->localVariations:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;Ljava/util/List;Ljava/util/List;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->fetchedVariations:Ljava/util/List;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->localVariations:Ljava/util/List;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->copy(Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState;",
            "-",
            "Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->localVariations:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 207
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 208
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 122
    invoke-virtual {v5}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 209
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 210
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 211
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 212
    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 123
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getMerchantCatalogObjectToken()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 213
    :cond_3
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 124
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->toMutableSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 125
    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 127
    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->fetchedVariations:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 214
    instance-of v2, v1, Ljava/util/Collection;

    if-eqz v2, :cond_4

    move-object v2, v1

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_3

    .line 215
    :cond_4
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;

    .line 128
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;->getKey()Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;

    move-result-object v2

    const-string v5, "it.key"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/connectv2/models/ModelObjectKey;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    xor-int/2addr v2, v3

    if-eqz v2, :cond_5

    const/4 v4, 0x1

    :cond_6
    :goto_3
    if-eqz v4, :cond_7

    .line 132
    sget-object v0, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput$AbortedWithRemoteVariations;->INSTANCE:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsOutput$AbortedWithRemoteVariations;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_4

    .line 134
    :cond_7
    sget-object v0, Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState$AddSingleVariation;->INSTANCE:Lcom/squareup/items/addsinglevariation/AddSingleVariationWithOptionsState$AddSingleVariation;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    :goto_4
    return-void
.end method

.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->fetchedVariations:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->localVariations:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;"
        }
    .end annotation

    const-string v0, "fetchedVariations"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localVariations"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locallyDeletedVariationTokens"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->fetchedVariations:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->fetchedVariations:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->localVariations:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->localVariations:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getFetchedVariations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogConnectV2ItemVariation;",
            ">;"
        }
    .end annotation

    .line 116
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->fetchedVariations:Ljava/util/List;

    return-object v0
.end method

.method public final getLocalVariations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->localVariations:Ljava/util/List;

    return-object v0
.end method

.method public final getLocallyDeletedVariationTokens()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->fetchedVariations:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->localVariations:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CheckFetchedVariations(fetchedVariations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->fetchedVariations:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", localVariations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->localVariations:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", locallyDeletedVariationTokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/items/addsinglevariation/RealAddSingleVariationWithOptionsWorkflow$Action$CheckFetchedVariations;->locallyDeletedVariationTokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
