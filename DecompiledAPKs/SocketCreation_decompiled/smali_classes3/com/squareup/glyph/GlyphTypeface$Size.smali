.class final enum Lcom/squareup/glyph/GlyphTypeface$Size;
.super Ljava/lang/Enum;
.source "GlyphTypeface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/glyph/GlyphTypeface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Size"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/glyph/GlyphTypeface$Size;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/glyph/GlyphTypeface$Size;

.field public static final enum APPLET:Lcom/squareup/glyph/GlyphTypeface$Size;

.field public static final enum CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

.field public static final enum COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

.field public static final enum COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

.field public static final enum HUD:Lcom/squareup/glyph/GlyphTypeface$Size;


# instance fields
.field final dimenDp:F


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 134
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Size;

    const/4 v1, 0x0

    const-string v2, "APPLET"

    const/high16 v3, 0x41c40000    # 24.5f

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/glyph/GlyphTypeface$Size;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Size;->APPLET:Lcom/squareup/glyph/GlyphTypeface$Size;

    .line 136
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Size;

    const/4 v2, 0x1

    const-string v3, "CIRCLE"

    const/high16 v4, 0x42f50000    # 122.5f

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/glyph/GlyphTypeface$Size;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    .line 138
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Size;

    const/4 v3, 0x2

    const-string v4, "COMPOUND_24"

    const/high16 v5, 0x41c00000    # 24.0f

    invoke-direct {v0, v4, v3, v5}, Lcom/squareup/glyph/GlyphTypeface$Size;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    .line 140
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Size;

    const/4 v4, 0x3

    const-string v5, "COMPOUND_26"

    const/high16 v6, 0x41d00000    # 26.0f

    invoke-direct {v0, v5, v4, v6}, Lcom/squareup/glyph/GlyphTypeface$Size;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    .line 142
    new-instance v0, Lcom/squareup/glyph/GlyphTypeface$Size;

    const/4 v5, 0x4

    const-string v6, "HUD"

    const/high16 v7, 0x43000000    # 128.0f

    invoke-direct {v0, v6, v5, v7}, Lcom/squareup/glyph/GlyphTypeface$Size;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/glyph/GlyphTypeface$Size;

    .line 132
    sget-object v6, Lcom/squareup/glyph/GlyphTypeface$Size;->APPLET:Lcom/squareup/glyph/GlyphTypeface$Size;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Size;->CIRCLE:Lcom/squareup/glyph/GlyphTypeface$Size;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_24:Lcom/squareup/glyph/GlyphTypeface$Size;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Size;->COMPOUND_26:Lcom/squareup/glyph/GlyphTypeface$Size;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Size;->HUD:Lcom/squareup/glyph/GlyphTypeface$Size;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/glyph/GlyphTypeface$Size;->$VALUES:[Lcom/squareup/glyph/GlyphTypeface$Size;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IF)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)V"
        }
    .end annotation

    .line 146
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 147
    iput p3, p0, Lcom/squareup/glyph/GlyphTypeface$Size;->dimenDp:F

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/glyph/GlyphTypeface$Size;
    .locals 1

    .line 132
    const-class v0, Lcom/squareup/glyph/GlyphTypeface$Size;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/glyph/GlyphTypeface$Size;

    return-object p0
.end method

.method public static values()[Lcom/squareup/glyph/GlyphTypeface$Size;
    .locals 1

    .line 132
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Size;->$VALUES:[Lcom/squareup/glyph/GlyphTypeface$Size;

    invoke-virtual {v0}, [Lcom/squareup/glyph/GlyphTypeface$Size;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/glyph/GlyphTypeface$Size;

    return-object v0
.end method
