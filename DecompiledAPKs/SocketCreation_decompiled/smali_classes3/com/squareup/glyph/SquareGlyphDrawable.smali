.class public final Lcom/squareup/glyph/SquareGlyphDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "SquareGlyphDrawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/glyph/SquareGlyphDrawable$Builder;
    }
.end annotation


# static fields
.field private static final DEFAULT_TEXT_SIZE:F = NaNf

.field public static final NO_SHADOW:I = -0x1

.field private static final TEXT_DRAW_MAX_SIZE_DP:F = 150.0f


# instance fields
.field private final colorStateList:Landroid/content/res/ColorStateList;

.field private glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private final glyphBounds:Landroid/graphics/Rect;

.field private final glyphPaint:Landroid/text/TextPaint;

.field private final glyphPath:Landroid/graphics/Path;

.field private text:Ljava/lang/String;

.field private final usePathToDraw:Z


# direct methods
.method private constructor <init>(Landroid/content/res/Resources;Lcom/squareup/glyph/GlyphTypeface$Glyph;FLandroid/graphics/Rect;Landroid/content/res/ColorStateList;ZIFFI)V
    .locals 3

    .line 36
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 37
    iput-object p2, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 38
    invoke-virtual {p2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->getCharacterAsString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->text:Ljava/lang/String;

    .line 39
    iput-object p5, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->colorStateList:Landroid/content/res/ColorStateList;

    .line 40
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPath:Landroid/graphics/Path;

    .line 42
    invoke-virtual {p2, p1}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->initGlyph(Landroid/content/res/Resources;)V

    .line 44
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    .line 45
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const/4 v0, 0x0

    if-eqz p6, :cond_0

    const/high16 p6, 0x7fc00000    # Float.NaN

    cmpl-float p6, p3, p6

    if-eqz p6, :cond_0

    .line 47
    iget-object p6, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    invoke-virtual {p6, p3}, Landroid/text/TextPaint;->setTextSize(F)V

    if-nez p4, :cond_1

    .line 50
    new-instance p3, Landroid/graphics/Rect;

    invoke-direct {p3}, Landroid/graphics/Rect;-><init>()V

    .line 51
    iget-object p4, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    invoke-virtual {p2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->getCharacterAsString()Ljava/lang/String;

    move-result-object p6

    invoke-virtual {p4, p6, v0, v1, p3}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 52
    new-instance p4, Landroid/graphics/Rect;

    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result p6

    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result p3

    invoke-direct {p4, v0, v0, p6, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    .line 55
    :cond_0
    iget-object p3, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    invoke-virtual {p2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->getFixedTextSize()I

    move-result p6

    int-to-float p6, p6

    invoke-virtual {p3, p6}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 57
    :cond_1
    :goto_0
    iget-object p3, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    invoke-virtual {p5}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result p5

    invoke-virtual {p3, p5}, Landroid/text/TextPaint;->setColor(I)V

    .line 58
    iget-object p3, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    invoke-static {p1}, Lcom/squareup/glyph/GlyphTypeface;->getGlyphFont(Landroid/content/res/Resources;)Landroid/graphics/Typeface;

    move-result-object p5

    invoke-virtual {p3, p5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    if-eqz p4, :cond_2

    .line 61
    iput-object p4, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphBounds:Landroid/graphics/Rect;

    goto :goto_1

    .line 63
    :cond_2
    new-instance p3, Landroid/graphics/Rect;

    invoke-virtual {p2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->getFixedGlyphWidth()I

    move-result p4

    invoke-virtual {p2}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->getFixedGlyphHeight()I

    move-result p2

    invoke-direct {p3, v0, v0, p4, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object p3, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphBounds:Landroid/graphics/Rect;

    :goto_1
    const/4 p2, -0x1

    if-eq p7, p2, :cond_3

    .line 67
    iget-object p2, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    int-to-float p3, p7

    invoke-virtual {p2, p3, p8, p9, p10}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 69
    :cond_3
    iget-object p2, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphBounds:Landroid/graphics/Rect;

    invoke-virtual {p0, p2}, Lcom/squareup/glyph/SquareGlyphDrawable;->setBounds(Landroid/graphics/Rect;)V

    const/high16 p2, 0x43160000    # 150.0f

    .line 72
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    iget p1, p1, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->dpToPxRounded(FI)I

    move-result p1

    .line 73
    iget-object p2, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphBounds:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result p2

    if-gt p2, p1, :cond_4

    iget-object p2, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphBounds:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result p2

    if-le p2, p1, :cond_5

    :cond_4
    const/4 v0, 0x1

    :cond_5
    iput-boolean v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->usePathToDraw:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/res/Resources;Lcom/squareup/glyph/GlyphTypeface$Glyph;FLandroid/graphics/Rect;Landroid/content/res/ColorStateList;ZIFFILcom/squareup/glyph/SquareGlyphDrawable$1;)V
    .locals 0

    .line 19
    invoke-direct/range {p0 .. p10}, Lcom/squareup/glyph/SquareGlyphDrawable;-><init>(Landroid/content/res/Resources;Lcom/squareup/glyph/GlyphTypeface$Glyph;FLandroid/graphics/Rect;Landroid/content/res/ColorStateList;ZIFFI)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 10

    .line 85
    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 86
    iget v1, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 89
    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget-object v3, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->getTextSize()F

    move-result v3

    add-float/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    add-float/2addr v2, v0

    float-to-int v0, v2

    .line 94
    iget-boolean v2, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->usePathToDraw:Z

    if-eqz v2, :cond_0

    .line 95
    iget-object v3, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->text:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    int-to-float v7, v1

    int-to-float v8, v0

    iget-object v9, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPath:Landroid/graphics/Path;

    invoke-virtual/range {v3 .. v9}, Landroid/text/TextPaint;->getTextPath(Ljava/lang/String;IIFFLandroid/graphics/Path;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 97
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPath:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 99
    :cond_0
    iget-object v2, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->text:Ljava/lang/String;

    int-to-float v1, v1

    int-to-float v0, v0

    iget-object v3, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v2, v1, v0, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :goto_0
    return-void
.end method

.method public getGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 134
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphBounds:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public isStateful()Z
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->colorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    return v0
.end method

.method protected onStateChange([I)Z
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getColor()I

    move-result v0

    .line 123
    iget-object v1, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->colorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v2

    invoke-virtual {v1, p1, v2}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result p1

    if-eq p1, v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 127
    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphDrawable;->invalidateSelf()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public setAlpha(I)V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 105
    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphDrawable;->invalidateSelf()V

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/glyph/SquareGlyphDrawable;->glyphPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 110
    invoke-virtual {p0}, Lcom/squareup/glyph/SquareGlyphDrawable;->invalidateSelf()V

    return-void
.end method
