.class final Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EGiftCardRegisteredCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003j\u0002`\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
        "Lcom/squareup/egiftcard/activation/EGiftCardRegisteredScreen;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$attach$1;->this$0:Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$attach$1;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$attach$1;->this$0:Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator$attach$1;->$view:Landroid/view/View;

    iget-object v2, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v2, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-static {v0, v1, v2, p1}, Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;->access$update(Lcom/squareup/egiftcard/activation/EGiftCardRegisteredCoordinator;Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method
