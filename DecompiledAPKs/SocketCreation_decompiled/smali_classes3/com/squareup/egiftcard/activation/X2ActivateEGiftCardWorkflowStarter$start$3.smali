.class final Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$3;
.super Ljava/lang/Object;
.source "X2ActivateEGiftCardWorkflowStarter.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter;->start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;",
        "+",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0006*\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/legacy/Workflow;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Workflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$3;->$workflow:Lcom/squareup/workflow/legacy/Workflow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 30
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$3;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;",
            "+",
            "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;

    .line 59
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/X2ActivateEGiftCardWorkflowStarter$start$3;->$workflow:Lcom/squareup/workflow/legacy/Workflow;

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEventWithState;->getEvent()Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/workflow/legacy/Workflow;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
