.class public final Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$$inlined$forEach$lambda$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;->update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingAmount;Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 ChooseAmountCoordinator.kt\ncom/squareup/egiftcard/activation/ChooseAmountCoordinator\n*L\n1#1,1322:1\n60#2:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0019\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0007"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release",
        "com/squareup/egiftcard/activation/ChooseAmountCoordinator$$special$$inlined$onClickDebounced$1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $money$inlined:Lcom/squareup/protos/common/Money;

.field final synthetic $workflowInput$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$$inlined$forEach$lambda$1;->$money$inlined:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$$inlined$forEach$lambda$1;->this$0:Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator;

    iput-object p3, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$$inlined$forEach$lambda$1;->$workflowInput$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Lcom/squareup/noho/NohoButton;

    .line 1323
    iget-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$$inlined$forEach$lambda$1;->$workflowInput$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$AmountSelected;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$update$$inlined$forEach$lambda$1;->$money$inlined:Lcom/squareup/protos/common/Money;

    invoke-direct {v0, v1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$AmountSelected;-><init>(Lcom/squareup/protos/common/Money;)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
