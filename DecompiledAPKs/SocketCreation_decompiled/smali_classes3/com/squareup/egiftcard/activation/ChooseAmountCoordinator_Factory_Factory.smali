.class public final Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "ChooseAmountCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator_Factory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)",
            "Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator_Factory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/text/Formatter;)Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;

    invoke-direct {v0, p0}, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;-><init>(Lcom/squareup/text/Formatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator_Factory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {v0}, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator_Factory_Factory;->newInstance(Lcom/squareup/text/Formatter;)Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator_Factory_Factory;->get()Lcom/squareup/egiftcard/activation/ChooseAmountCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
