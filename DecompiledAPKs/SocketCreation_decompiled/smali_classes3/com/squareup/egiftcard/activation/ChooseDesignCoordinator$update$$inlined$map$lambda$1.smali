.class final Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$update$$inlined$map$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseDesignCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator;->update(Landroid/view/View;Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$ChoosingDesign;Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/egiftcard/activation/ChooseDesignCoordinator$update$3$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/protos/client/giftcards/EGiftTheme;

.field final synthetic $workflowInput$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;


# direct methods
.method constructor <init>(Lcom/squareup/protos/client/giftcards/EGiftTheme;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$update$$inlined$map$lambda$1;->$it:Lcom/squareup/protos/client/giftcards/EGiftTheme;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$update$$inlined$map$lambda$1;->$workflowInput$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$update$$inlined$map$lambda$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 91
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$update$$inlined$map$lambda$1;->$workflowInput$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$DesignSelected;

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ChooseDesignCoordinator$update$$inlined$map$lambda$1;->$it:Lcom/squareup/protos/client/giftcards/EGiftTheme;

    iget-object v2, v2, Lcom/squareup/protos/client/giftcards/EGiftTheme;->read_only_token:Ljava/lang/String;

    const-string v3, "it.read_only_token"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardEvent$DesignSelected;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
