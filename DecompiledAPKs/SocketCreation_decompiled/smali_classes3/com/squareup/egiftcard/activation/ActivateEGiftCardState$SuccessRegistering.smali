.class public final Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;
.super Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;
.source "ActivateEGiftCardState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SuccessRegistering"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000f\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\tH\u00c6\u0003J1\u0010\u0017\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\u0013\u0010\u001a\u001a\u00020\t2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001J\u0019\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;",
        "Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;",
        "config",
        "Lcom/squareup/egiftcard/activation/EGiftCardConfig;",
        "eGiftCard",
        "Lcom/squareup/protos/client/giftcards/GiftCard;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "autoClose",
        "",
        "(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/common/Money;Z)V",
        "getAmount",
        "()Lcom/squareup/protos/common/Money;",
        "getAutoClose",
        "()Z",
        "getConfig",
        "()Lcom/squareup/egiftcard/activation/EGiftCardConfig;",
        "getEGiftCard",
        "()Lcom/squareup/protos/client/giftcards/GiftCard;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "giftcard-activation_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final amount:Lcom/squareup/protos/common/Money;

.field private final autoClose:Z

.field private final config:Lcom/squareup/egiftcard/activation/EGiftCardConfig;

.field private final eGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering$Creator;

    invoke-direct {v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering$Creator;-><init>()V

    sput-object v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/common/Money;Z)V
    .locals 1

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eGiftCard"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, v0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->config:Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    iput-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->eGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    iput-object p3, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->amount:Lcom/squareup/protos/common/Money;

    iput-boolean p4, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->autoClose:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/common/Money;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 59
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;-><init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/common/Money;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;Lcom/squareup/egiftcard/activation/EGiftCardConfig;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/common/Money;ZILjava/lang/Object;)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object p1

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->eGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->amount:Lcom/squareup/protos/common/Money;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-boolean p4, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->autoClose:Z

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->copy(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/egiftcard/activation/EGiftCardConfig;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/giftcards/GiftCard;
    .locals 1

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->eGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->autoClose:Z

    return v0
.end method

.method public final copy(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/common/Money;Z)Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;
    .locals 1

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "eGiftCard"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;-><init>(Lcom/squareup/egiftcard/activation/EGiftCardConfig;Lcom/squareup/protos/client/giftcards/GiftCard;Lcom/squareup/protos/common/Money;Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->eGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    iget-object v1, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->eGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->amount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->amount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->autoClose:Z

    iget-boolean p1, p1, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->autoClose:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->amount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getAutoClose()Z
    .locals 1

    .line 59
    iget-boolean v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->autoClose:Z

    return v0
.end method

.method public getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->config:Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    return-object v0
.end method

.method public final getEGiftCard()Lcom/squareup/protos/client/giftcards/GiftCard;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->eGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->eGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->autoClose:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SuccessRegistering(config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->getConfig()Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", eGiftCard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->eGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", autoClose="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->autoClose:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->config:Lcom/squareup/egiftcard/activation/EGiftCardConfig;

    const/4 v0, 0x0

    invoke-interface {p2, p1, v0}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->eGiftCard:Lcom/squareup/protos/client/giftcards/GiftCard;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-boolean p2, p0, Lcom/squareup/egiftcard/activation/ActivateEGiftCardState$SuccessRegistering;->autoClose:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
