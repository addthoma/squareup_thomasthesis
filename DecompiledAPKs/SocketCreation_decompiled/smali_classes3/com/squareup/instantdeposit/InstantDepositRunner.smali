.class public interface abstract Lcom/squareup/instantdeposit/InstantDepositRunner;
.super Ljava/lang/Object;
.source "InstantDepositRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/instantdeposit/InstantDepositRunner$InstantDepositState;,
        Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;,
        Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\u0008f\u0018\u0000 \u00102\u00020\u0001:\u0003\u000f\u0010\u0011J\u0016\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u0004H&J\u000e\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\'J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\'J\u0008\u0010\u000e\u001a\u00020\u000cH&J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u000bH\'\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/instantdeposit/InstantDepositRunner;",
        "",
        "checkIfEligibleForInstantDeposit",
        "Lio/reactivex/Single;",
        "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
        "includeRecentActivity",
        "",
        "instantDepositHint",
        "",
        "snapshot",
        "onInstantDepositMade",
        "Lio/reactivex/Observable;",
        "",
        "sendInstantDeposit",
        "setIsConfirmingInstantTransfer",
        "InstantDepositState",
        "NOT_SUPPORTED",
        "Snapshot",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final NOT_SUPPORTED:Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;->$$INSTANCE:Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;

    sput-object v0, Lcom/squareup/instantdeposit/InstantDepositRunner;->NOT_SUPPORTED:Lcom/squareup/instantdeposit/InstantDepositRunner$NOT_SUPPORTED;

    return-void
.end method


# virtual methods
.method public abstract checkIfEligibleForInstantDeposit(Z)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation
.end method

.method public abstract instantDepositHint(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Ljava/lang/CharSequence;
.end method

.method public abstract onInstantDepositMade()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end method

.method public abstract sendInstantDeposit()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setIsConfirmingInstantTransfer()V
.end method

.method public abstract snapshot()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            ">;"
        }
    .end annotation
.end method
