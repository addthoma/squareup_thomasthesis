.class final Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion$fromLoyaltyEvent$rewardSubtext$1;
.super Lkotlin/jvm/internal/Lambda;
.source "LoyaltyRewardProgress.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion;->fromLoyaltyEvent(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/util/Res;Ljava/util/Locale;ZLcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)Lcom/squareup/loyalty/LoyaltyRewardProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Integer;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "rewards",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion$fromLoyaltyEvent$rewardSubtext$1;->$res:Lcom/squareup/util/Res;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion$fromLoyaltyEvent$rewardSubtext$1;->invoke(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(I)Ljava/lang/String;
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyRewardProgress$Companion$fromLoyaltyEvent$rewardSubtext$1;->$res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/loyalty/R$string;->loyalty_rewards_amount:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "rewards"

    .line 74
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 75
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
