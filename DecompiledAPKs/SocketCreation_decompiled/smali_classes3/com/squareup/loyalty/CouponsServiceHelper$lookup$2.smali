.class final Lcom/squareup/loyalty/CouponsServiceHelper$lookup$2;
.super Ljava/lang/Object;
.source "CouponsServiceHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyalty/CouponsServiceHelper;->lookup(Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0014\u0010\u0003\u001a\u0010\u0012\u000c\u0012\n \u0002*\u0004\u0018\u00010\u00010\u00010\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
        "kotlin.jvm.PlatformType",
        "received",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/loyalty/CouponsServiceHelper$lookup$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/loyalty/CouponsServiceHelper$lookup$2;

    invoke-direct {v0}, Lcom/squareup/loyalty/CouponsServiceHelper$lookup$2;-><init>()V

    sput-object v0, Lcom/squareup/loyalty/CouponsServiceHelper$lookup$2;->INSTANCE:Lcom/squareup/loyalty/CouponsServiceHelper$lookup$2;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
            ">;)",
            "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;"
        }
    .end annotation

    const-string v0, "received"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 40
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    invoke-virtual {v0}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->newBuilder()Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;

    move-result-object v0

    .line 41
    sget-object v1, Lcom/squareup/loyalty/CouponsServiceHelper;->Companion:Lcom/squareup/loyalty/CouponsServiceHelper$Companion;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->coupon:Ljava/util/List;

    const-string v2, "received.response.coupon"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Lcom/squareup/loyalty/CouponsServiceHelper$Companion;->getSupportedCoupons(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;->coupon(Ljava/util/List;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;

    move-result-object p1

    .line 42
    invoke-virtual {p1}, Lcom/squareup/protos/client/coupons/LookupCouponsResponse$Builder;->build()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    move-result-object p1

    goto :goto_0

    .line 44
    :cond_0
    invoke-static {}, Lcom/squareup/loyalty/CouponsServiceHelper;->access$getEMPTY_LOOKUP_COUPONS_RESPONSE$cp()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/loyalty/CouponsServiceHelper$lookup$2;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    move-result-object p1

    return-object p1
.end method
