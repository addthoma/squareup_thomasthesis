.class public final Lcom/squareup/loyalty/ui/ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "RewardAdapter.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/loyalty/ui/ViewHolder;",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;",
        "itemView",
        "Lcom/squareup/ui/account/view/SmartLineRow;",
        "(Lcom/squareup/ui/account/view/SmartLineRow;)V",
        "edges",
        "",
        "getEdges",
        "()I",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final edges:I


# direct methods
.method public constructor <init>(Lcom/squareup/ui/account/view/SmartLineRow;)V
    .locals 1

    const-string v0, "itemView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    check-cast p1, Landroid/view/View;

    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    const/16 p1, 0xa

    .line 21
    iput p1, p0, Lcom/squareup/loyalty/ui/ViewHolder;->edges:I

    return-void
.end method


# virtual methods
.method public getEdges()I
    .locals 1

    .line 21
    iget v0, p0, Lcom/squareup/loyalty/ui/ViewHolder;->edges:I

    return v0
.end method

.method public getPadding(Landroid/graphics/Rect;)V
    .locals 1

    const-string v0, "outRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges$DefaultImpls;->getPadding(Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;Landroid/graphics/Rect;)V

    return-void
.end method
