.class final Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3;
.super Ljava/lang/Object;
.source "LoyaltyPointsRowSubtitleRenderer.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->getSubtitle(Lcom/squareup/payment/Transaction;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;",
        "Lrx/Observable<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u000e\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Observable;",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "kotlin.jvm.PlatformType",
        "contactToken",
        "",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;


# direct methods
.method constructor <init>(Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3;->this$0:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3;->call(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
            ">;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3;->this$0:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;

    invoke-static {v0}, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;->access$getLoyaltyServiceHelper$p(Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer;)Lcom/squareup/loyalty/LoyaltyServiceHelper;

    move-result-object v0

    const-string v1, "contactToken"

    .line 38
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->getLoyaltyStatusForContactToken(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    .line 39
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    .line 40
    sget-object v0, Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3$1;->INSTANCE:Lcom/squareup/loyalty/LoyaltyPointsRowSubtitleRenderer$getSubtitle$withCustomer$3$1;

    check-cast v0, Lrx/functions/Func1;

    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
