.class public final Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;
.super Lcom/squareup/loyalty/MaybeLoyaltyEvent;
.source "MaybeLoyaltyEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/MaybeLoyaltyEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoyaltyEvent"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$EventContext;,
        Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0017\n\u0002\u0010\u0000\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u000245B\u008b\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000c\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u000c\u0012\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0012\u0012\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0012\u0012\u0006\u0010\u0016\u001a\u00020\u000f\u0012\u0006\u0010\u0017\u001a\u00020\u000f\u0012\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019\u00a2\u0006\u0002\u0010\u001aJ\t\u0010\u001e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0012H\u00c6\u0003J\t\u0010 \u001a\u00020\u000cH\u00c6\u0003J\u000b\u0010!\u001a\u0004\u0018\u00010\u0012H\u00c6\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\u0012H\u00c6\u0003J\t\u0010#\u001a\u00020\u000fH\u00c6\u0003J\t\u0010$\u001a\u00020\u000fH\u00c6\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u0019H\u00c6\u0003J\t\u0010&\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\'\u001a\u00020\u0006H\u00c6\u0003J\t\u0010(\u001a\u00020\u0008H\u00c6\u0003J\t\u0010)\u001a\u00020\nH\u00c6\u0003J\t\u0010*\u001a\u00020\u000cH\u00c6\u0003J\t\u0010+\u001a\u00020\u000cH\u00c6\u0003J\t\u0010,\u001a\u00020\u000fH\u00c6\u0003J\t\u0010-\u001a\u00020\u000fH\u00c6\u0003J\u00af\u0001\u0010.\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\r\u001a\u00020\u000c2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u000c2\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00122\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00122\u0008\u0008\u0002\u0010\u0016\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0017\u001a\u00020\u000f2\n\u0008\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00c6\u0001J\u0013\u0010/\u001a\u00020\u000c2\u0008\u00100\u001a\u0004\u0018\u000101H\u00d6\u0003J\t\u00102\u001a\u00020\u000fH\u00d6\u0001J\t\u00103\u001a\u00020\u0012H\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0010\u0010\u0007\u001a\u00020\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u00020\u000c8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u00198\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u000c8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u00020\u000c8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u00020\u000f8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u00128\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u00128\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u00020\u000f8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u00020\u000f8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001cR\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u00020\u000f8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\n8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u00020\u00128\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent;",
        "tenderIdPair",
        "Lcom/squareup/protos/client/IdPair;",
        "billIdPair",
        "tenderType",
        "Lcom/squareup/protos/client/bills/Tender$Type;",
        "cart",
        "Lcom/squareup/protos/client/bills/Cart;",
        "type",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;",
        "isEnrolledWithPhone",
        "",
        "isNewlyEnrolled",
        "pointsEarnedInTransaction",
        "",
        "pointsEarnedTotal",
        "unitToken",
        "",
        "didLinkNewCardToExistingLoyalty",
        "obfuscatedPhoneNumber",
        "phoneToken",
        "newEligibleRewardTiers",
        "totalEligibleRewardTiers",
        "earlyComputedReason",
        "Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;",
        "(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;ZZIILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;IILcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)V",
        "getBillIdPair",
        "()Lcom/squareup/protos/client/IdPair;",
        "getTenderIdPair",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component16",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "EventContext",
        "Type",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final billIdPair:Lcom/squareup/protos/client/IdPair;

.field public final cart:Lcom/squareup/protos/client/bills/Cart;

.field public final didLinkNewCardToExistingLoyalty:Z

.field public final earlyComputedReason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

.field public final isEnrolledWithPhone:Z

.field public final isNewlyEnrolled:Z

.field public final newEligibleRewardTiers:I

.field public final obfuscatedPhoneNumber:Ljava/lang/String;

.field public final phoneToken:Ljava/lang/String;

.field public final pointsEarnedInTransaction:I

.field public final pointsEarnedTotal:I

.field private final tenderIdPair:Lcom/squareup/protos/client/IdPair;

.field public final tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

.field public final totalEligibleRewardTiers:I

.field public final type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

.field public final unitToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;ZZIILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;IILcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)V
    .locals 8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p10

    const-string v7, "tenderIdPair"

    invoke-static {p1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "billIdPair"

    invoke-static {p2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "tenderType"

    invoke-static {p3, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v7, "cart"

    invoke-static {p4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v7, "type"

    invoke-static {p5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v7, "unitToken"

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    .line 87
    invoke-direct {p0, p1, p2, v7}, Lcom/squareup/loyalty/MaybeLoyaltyEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v1, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    iput-object v2, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    iput-object v3, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    iput-object v4, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    iput-object v5, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    move v1, p6

    iput-boolean v1, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isEnrolledWithPhone:Z

    move v1, p7

    iput-boolean v1, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isNewlyEnrolled:Z

    move/from16 v1, p8

    iput v1, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    move/from16 v1, p9

    iput v1, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedTotal:I

    iput-object v6, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->unitToken:Ljava/lang/String;

    move/from16 v1, p11

    iput-boolean v1, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->didLinkNewCardToExistingLoyalty:Z

    move-object/from16 v1, p12

    iput-object v1, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->obfuscatedPhoneNumber:Ljava/lang/String;

    move-object/from16 v1, p13

    iput-object v1, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->phoneToken:Ljava/lang/String;

    move/from16 v1, p14

    iput v1, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->newEligibleRewardTiers:I

    move/from16 v1, p15

    iput v1, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->totalEligibleRewardTiers:I

    move-object/from16 v1, p16

    iput-object v1, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->earlyComputedReason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;ZZIILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;IILcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;ILjava/lang/Object;)Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p17

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    goto :goto_1

    :cond_1
    move-object/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isEnrolledWithPhone:Z

    goto :goto_5

    :cond_5
    move/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isNewlyEnrolled:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget v9, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    goto :goto_7

    :cond_7
    move/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget v10, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedTotal:I

    goto :goto_8

    :cond_8
    move/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->unitToken:Ljava/lang/String;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-boolean v12, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->didLinkNewCardToExistingLoyalty:Z

    goto :goto_a

    :cond_a
    move/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget-object v13, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->obfuscatedPhoneNumber:Ljava/lang/String;

    goto :goto_b

    :cond_b
    move-object/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-object v14, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->phoneToken:Ljava/lang/String;

    goto :goto_c

    :cond_c
    move-object/from16 v14, p13

    :goto_c
    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget v15, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->newEligibleRewardTiers:I

    goto :goto_d

    :cond_d
    move/from16 v15, p14

    :goto_d
    move/from16 p14, v15

    and-int/lit16 v15, v1, 0x4000

    if-eqz v15, :cond_e

    iget v15, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->totalEligibleRewardTiers:I

    goto :goto_e

    :cond_e
    move/from16 v15, p15

    :goto_e
    const v16, 0x8000

    and-int v1, v1, v16

    if-eqz v1, :cond_f

    iget-object v1, v0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->earlyComputedReason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    goto :goto_f

    :cond_f
    move-object/from16 v1, p16

    :goto_f
    move-object/from16 p1, v2

    move-object/from16 p2, v3

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v10

    move-object/from16 p10, v11

    move/from16 p11, v12

    move-object/from16 p12, v13

    move-object/from16 p13, v14

    move/from16 p15, v15

    move-object/from16 p16, v1

    invoke-virtual/range {p0 .. p16}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->copy(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;ZZIILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;IILcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/IdPair;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0
.end method

.method public final component10()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->unitToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component11()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->didLinkNewCardToExistingLoyalty:Z

    return v0
.end method

.method public final component12()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->obfuscatedPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final component13()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->phoneToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component14()I
    .locals 1

    iget v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->newEligibleRewardTiers:I

    return v0
.end method

.method public final component15()I
    .locals 1

    iget v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->totalEligibleRewardTiers:I

    return v0
.end method

.method public final component16()Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->earlyComputedReason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    return-object v0
.end method

.method public final component2()Lcom/squareup/protos/client/IdPair;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/client/bills/Tender$Type;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    return-object v0
.end method

.method public final component4()Lcom/squareup/protos/client/bills/Cart;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object v0
.end method

.method public final component5()Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;
    .locals 1

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isEnrolledWithPhone:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isNewlyEnrolled:Z

    return v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    return v0
.end method

.method public final component9()I
    .locals 1

    iget v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedTotal:I

    return v0
.end method

.method public final copy(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;ZZIILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;IILcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;
    .locals 19

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    move/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move/from16 v14, p14

    move/from16 v15, p15

    move-object/from16 v16, p16

    const-string v0, "tenderIdPair"

    move-object/from16 v17, v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billIdPair"

    move-object/from16 v1, p2

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderType"

    move-object/from16 v1, p3

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cart"

    move-object/from16 v1, p4

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    move-object/from16 v1, p5

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unitToken"

    move-object/from16 v1, p10

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v18, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/bills/Tender$Type;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;ZZIILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;IILcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;)V

    return-object v18
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    iget-object v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    iget-object v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isEnrolledWithPhone:Z

    iget-boolean v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isEnrolledWithPhone:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isNewlyEnrolled:Z

    iget-boolean v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isNewlyEnrolled:Z

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    iget v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedTotal:I

    iget v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedTotal:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->unitToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->unitToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->didLinkNewCardToExistingLoyalty:Z

    iget-boolean v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->didLinkNewCardToExistingLoyalty:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->obfuscatedPhoneNumber:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->obfuscatedPhoneNumber:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->phoneToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->phoneToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->newEligibleRewardTiers:I

    iget v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->newEligibleRewardTiers:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->totalEligibleRewardTiers:I

    iget v1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->totalEligibleRewardTiers:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->earlyComputedReason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    iget-object p1, p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->earlyComputedReason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getBillIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->billIdPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public getTenderIdPair()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderIdPair:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isEnrolledWithPhone:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :cond_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isNewlyEnrolled:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedTotal:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->unitToken:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->didLinkNewCardToExistingLoyalty:Z

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :cond_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->obfuscatedPhoneNumber:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_9
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->phoneToken:Ljava/lang/String;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_a
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->newEligibleRewardTiers:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->totalEligibleRewardTiers:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->earlyComputedReason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_b
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "LoyaltyEvent(tenderIdPair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", billIdPair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->getBillIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tenderType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->tenderType:Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", cart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->cart:Lcom/squareup/protos/client/bills/Cart;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->type:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isEnrolledWithPhone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isEnrolledWithPhone:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isNewlyEnrolled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->isNewlyEnrolled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", pointsEarnedInTransaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedInTransaction:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", pointsEarnedTotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->pointsEarnedTotal:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", unitToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->unitToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", didLinkNewCardToExistingLoyalty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->didLinkNewCardToExistingLoyalty:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", obfuscatedPhoneNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->obfuscatedPhoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", phoneToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->phoneToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", newEligibleRewardTiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->newEligibleRewardTiers:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", totalEligibleRewardTiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->totalEligibleRewardTiers:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", earlyComputedReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->earlyComputedReason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
