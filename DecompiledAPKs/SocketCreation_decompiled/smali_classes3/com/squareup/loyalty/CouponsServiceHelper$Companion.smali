.class public final Lcom/squareup/loyalty/CouponsServiceHelper$Companion;
.super Ljava/lang/Object;
.source "CouponsServiceHelper.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/loyalty/CouponsServiceHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/loyalty/CouponsServiceHelper$Companion;",
        "",
        "()V",
        "EMPTY_LOOKUP_COUPONS_RESPONSE",
        "Lcom/squareup/protos/client/coupons/LookupCouponsResponse;",
        "kotlin.jvm.PlatformType",
        "getSupportedCoupons",
        "",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "coupons",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 58
    invoke-direct {p0}, Lcom/squareup/loyalty/CouponsServiceHelper$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getSupportedCoupons(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation

    const-string v0, "coupons"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 71
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/coupons/Coupon;

    .line 72
    iget-object v2, v1, Lcom/squareup/protos/client/coupons/Coupon;->condition:Lcom/squareup/protos/client/coupons/CouponCondition;

    if-eqz v2, :cond_1

    goto :goto_0

    .line 78
    :cond_1
    iget-object v2, v1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    if-eqz v2, :cond_9

    .line 79
    iget-object v2, v1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v2, v2, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/squareup/loyalty/CouponsServiceHelper$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v2}, Lcom/squareup/protos/client/coupons/Scope;->ordinal()I

    move-result v2

    aget v2, v3, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_7

    const/4 v4, 0x2

    if-eq v2, v4, :cond_3

    goto :goto_0

    .line 94
    :cond_3
    iget-object v2, v1, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    sget-object v4, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->CATEGORY:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    if-eq v2, v4, :cond_4

    iget-object v2, v1, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    sget-object v4, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->VARIATION:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    if-eq v2, v4, :cond_4

    goto :goto_0

    .line 99
    :cond_4
    iget-object v2, v1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v2, v2, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v3, :cond_5

    goto :goto_0

    .line 105
    :cond_5
    iget-object v2, v1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v2, v2, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/coupons/ItemConstraint;

    iget-object v2, v2, Lcom/squareup/protos/client/coupons/ItemConstraint;->quantity:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 106
    iget-object v2, v1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v2, v2, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/coupons/ItemConstraint;

    iget-object v2, v2, Lcom/squareup/protos/client/coupons/ItemConstraint;->quantity:Ljava/lang/Integer;

    if-nez v2, :cond_6

    goto :goto_0

    :cond_6
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v3, :cond_9

    goto :goto_0

    .line 81
    :cond_7
    iget-object v2, v1, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    sget-object v3, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->UNKNOWN:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    if-eq v2, v3, :cond_8

    goto :goto_0

    .line 86
    :cond_8
    iget-object v2, v1, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    iget-object v2, v2, Lcom/squareup/protos/client/coupons/CouponReward;->item_constraint:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_9

    goto :goto_0

    .line 119
    :cond_9
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 122
    :cond_a
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
