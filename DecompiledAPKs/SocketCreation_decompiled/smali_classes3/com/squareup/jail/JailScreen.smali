.class public final Lcom/squareup/jail/JailScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "JailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/jail/JailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/jail/JailScreen$Component;,
        Lcom/squareup/jail/JailScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/jail/JailScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/jail/JailScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11
    new-instance v0, Lcom/squareup/jail/JailScreen;

    invoke-direct {v0}, Lcom/squareup/jail/JailScreen;-><init>()V

    sput-object v0, Lcom/squareup/jail/JailScreen;->INSTANCE:Lcom/squareup/jail/JailScreen;

    .line 26
    sget-object v0, Lcom/squareup/jail/JailScreen;->INSTANCE:Lcom/squareup/jail/JailScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/jail/JailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 29
    sget v0, Lcom/squareup/jail/R$layout;->jail_view:I

    return v0
.end method
