.class final Lcom/squareup/jail/CogsJailKeeper$backgroundSync$1;
.super Ljava/lang/Object;
.source "CogsJailKeeper.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/jail/CogsJailKeeper;->backgroundSync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/shared/catalog/sync/SyncResult<",
        "Ljava/lang/Void;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004 \u0005*\u0012\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "Ljava/lang/Void;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/jail/CogsJailKeeper;


# direct methods
.method constructor <init>(Lcom/squareup/jail/CogsJailKeeper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/jail/CogsJailKeeper$backgroundSync$1;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/shared/catalog/sync/SyncResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 136
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper$backgroundSync$1;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    invoke-static {v0}, Lcom/squareup/jail/CogsJailKeeper;->access$getAuthenticator$p(Lcom/squareup/jail/CogsJailKeeper;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {v0}, Lcom/squareup/account/LegacyAuthenticator;->isLoggedIn()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "JailKeeper aborting fees read. No user is logged in!"

    .line 137
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 142
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/sync/SyncResult;->get()Ljava/lang/Object;

    .line 145
    iget-object v0, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/squareup/jail/CogsJailKeeper$backgroundSync$1;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    iget-object p1, p1, Lcom/squareup/shared/catalog/sync/SyncResult;->error:Lcom/squareup/shared/catalog/sync/SyncError;

    const-string v1, "result.error"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/jail/CogsJailKeeper;->access$logOrThrowSyncError(Lcom/squareup/jail/CogsJailKeeper;Lcom/squareup/shared/catalog/sync/SyncError;)V

    .line 148
    :cond_1
    iget-object p1, p0, Lcom/squareup/jail/CogsJailKeeper$backgroundSync$1;->this$0:Lcom/squareup/jail/CogsJailKeeper;

    invoke-static {p1}, Lcom/squareup/jail/CogsJailKeeper;->access$reloadData(Lcom/squareup/jail/CogsJailKeeper;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 72
    check-cast p1, Lcom/squareup/shared/catalog/sync/SyncResult;

    invoke-virtual {p0, p1}, Lcom/squareup/jail/CogsJailKeeper$backgroundSync$1;->accept(Lcom/squareup/shared/catalog/sync/SyncResult;)V

    return-void
.end method
