.class public final Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;
.super Lcom/squareup/customreport/data/EmployeeFiltersSelection;
.source "EmployeeFiltersSelection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/customreport/data/EmployeeFiltersSelection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SomeEmployeesSelection"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection$Creator;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\t\u0010\n\u001a\u00020\u000bH\u00d6\u0001J\u0013\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u000bH\u00d6\u0001J\t\u0010\u0011\u001a\u00020\u0012H\u00d6\u0001J\u0019\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000bH\u00d6\u0001R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;",
        "Lcom/squareup/customreport/data/EmployeeFiltersSelection;",
        "employeeFilters",
        "",
        "Lcom/squareup/customreport/data/EmployeeFilter;",
        "(Ljava/util/Set;)V",
        "getEmployeeFilters",
        "()Ljava/util/Set;",
        "component1",
        "copy",
        "describeContents",
        "",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final employeeFilters:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/customreport/data/EmployeeFilter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection$Creator;

    invoke-direct {v0}, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection$Creator;-><init>()V

    sput-object v0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "+",
            "Lcom/squareup/customreport/data/EmployeeFilter;",
            ">;)V"
        }
    .end annotation

    const-string v0, "employeeFilters"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0, v0}, Lcom/squareup/customreport/data/EmployeeFiltersSelection;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->employeeFilters:Ljava/util/Set;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;Ljava/util/Set;ILjava/lang/Object;)Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->employeeFilters:Ljava/util/Set;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->copy(Ljava/util/Set;)Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/customreport/data/EmployeeFilter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->employeeFilters:Ljava/util/Set;

    return-object v0
.end method

.method public final copy(Ljava/util/Set;)Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "+",
            "Lcom/squareup/customreport/data/EmployeeFilter;",
            ">;)",
            "Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;"
        }
    .end annotation

    const-string v0, "employeeFilters"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    invoke-direct {v0, p1}, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;-><init>(Ljava/util/Set;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;

    iget-object v0, p0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->employeeFilters:Ljava/util/Set;

    iget-object p1, p1, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->employeeFilters:Ljava/util/Set;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEmployeeFilters()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/customreport/data/EmployeeFilter;",
            ">;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->employeeFilters:Ljava/util/Set;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->employeeFilters:Ljava/util/Set;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SomeEmployeesSelection(employeeFilters="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->employeeFilters:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/customreport/data/EmployeeFiltersSelection$SomeEmployeesSelection;->employeeFilters:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/customreport/data/EmployeeFilter;

    invoke-virtual {p1, v1, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_0

    :cond_0
    return-void
.end method
