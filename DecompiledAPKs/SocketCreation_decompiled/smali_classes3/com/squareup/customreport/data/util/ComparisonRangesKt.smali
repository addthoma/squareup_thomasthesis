.class public final Lcom/squareup/customreport/data/util/ComparisonRangesKt;
.super Ljava/lang/Object;
.source "ComparisonRanges.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0001\u00a8\u0006\u0004"
    }
    d2 = {
        "date",
        "Lorg/threeten/bp/LocalDate;",
        "Lcom/squareup/customreport/data/ComparisonRange;",
        "localDate",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final date(Lcom/squareup/customreport/data/ComparisonRange;Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;
    .locals 2

    const-string v0, "$this$date"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localDate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$Yesterday;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$Yesterday;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->yesterday(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    goto/16 :goto_2

    .line 36
    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousWeek;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousWeek;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_0
    invoke-static {p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->oneWeekAgo(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    goto/16 :goto_2

    .line 37
    :cond_2
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousDay;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousDay;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->yesterday(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    goto :goto_2

    .line 38
    :cond_3
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousMonth;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousMonth;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->oneMonthAgo(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    goto :goto_2

    .line 39
    :cond_4
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousThreeMonths;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousThreeMonths;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-wide/16 v0, 0x3

    invoke-virtual {p1, v0, v1}, Lorg/threeten/bp/LocalDate;->minusMonths(J)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    const-string p0, "localDate.minusMonths(3)"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 40
    :cond_5
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameWeekPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameWeekPreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-wide/16 v0, 0x34

    invoke-virtual {p1, v0, v1}, Lorg/threeten/bp/LocalDate;->minusWeeks(J)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    const-string p0, "localDate.minusWeeks(52)"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 41
    :cond_6
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameDayPreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    goto :goto_1

    .line 42
    :cond_7
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameMonthPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameMonthPreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    goto :goto_1

    .line 43
    :cond_8
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$SameThreeMonthsPreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$SameThreeMonthsPreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto :goto_1

    .line 44
    :cond_9
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$PreviousYear;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$PreviousYear;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    :goto_1
    invoke-static {p1}, Lcom/squareup/utilities/threeten/LocalDatesKt;->oneYearAgo(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p1

    goto :goto_2

    .line 45
    :cond_a
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$NoComparison;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$NoComparison;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_b

    :goto_2
    return-object p1

    :cond_b
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method
