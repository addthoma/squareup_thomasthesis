.class public final Lcom/squareup/customreport/data/util/ReportConfigsKt;
.super Ljava/lang/Object;
.source "ReportConfigs.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u001a\n\u0010\u0007\u001a\u00020\u0002*\u00020\u0002\u001a\u0016\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\t*\u00020\u0002\u001a\u0012\u0010\u000b\u001a\u00020\u0002*\u00020\u00022\u0006\u0010\u000c\u001a\u00020\r\"\u0015\u0010\u0000\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0015\u0010\u0005\u001a\u00020\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0004\u00a8\u0006\u000e"
    }
    d2 = {
        "endDateTime",
        "Lorg/threeten/bp/LocalDateTime;",
        "Lcom/squareup/customreport/data/ReportConfig;",
        "getEndDateTime",
        "(Lcom/squareup/customreport/data/ReportConfig;)Lorg/threeten/bp/LocalDateTime;",
        "startDateTime",
        "getStartDateTime",
        "comparisonConfig",
        "comparisonDateRange",
        "Lkotlin/Pair;",
        "Lorg/threeten/bp/LocalDate;",
        "withAllDay",
        "allDay",
        "",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final comparisonConfig(Lcom/squareup/customreport/data/ReportConfig;)Lcom/squareup/customreport/data/ReportConfig;
    .locals 14

    const-string v0, "$this$comparisonConfig"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-static {p0}, Lcom/squareup/customreport/data/util/ReportConfigsKt;->comparisonDateRange(Lcom/squareup/customreport/data/ReportConfig;)Lkotlin/Pair;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lorg/threeten/bp/LocalDate;

    .line 64
    sget-object v0, Lcom/squareup/customreport/data/ComparisonRange$NoComparison;->INSTANCE:Lcom/squareup/customreport/data/ComparisonRange$NoComparison;

    move-object v11, v0

    check-cast v11, Lcom/squareup/customreport/data/ComparisonRange;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v12, 0xfc

    const/4 v13, 0x0

    move-object v2, p0

    invoke-static/range {v2 .. v13}, Lcom/squareup/customreport/data/ReportConfig;->copy$default(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;ILjava/lang/Object;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p0

    return-object p0
.end method

.method public static final comparisonDateRange(Lcom/squareup/customreport/data/ReportConfig;)Lkotlin/Pair;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/customreport/data/ReportConfig;",
            ")",
            "Lkotlin/Pair<",
            "Lorg/threeten/bp/LocalDate;",
            "Lorg/threeten/bp/LocalDate;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$comparisonDateRange"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/customreport/data/util/ComparisonRangesKt;->date(Lcom/squareup/customreport/data/ComparisonRange;Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getComparisonRange()Lcom/squareup/customreport/data/ComparisonRange;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object p0

    invoke-static {v1, p0}, Lcom/squareup/customreport/data/util/ComparisonRangesKt;->date(Lcom/squareup/customreport/data/ComparisonRange;Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/LocalDate;

    move-result-object p0

    invoke-static {v0, p0}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p0

    return-object p0
.end method

.method public static final getEndDateTime(Lcom/squareup/customreport/data/ReportConfig;)Lorg/threeten/bp/LocalDateTime;
    .locals 1

    const-string v0, "$this$endDateTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getEndTime()Lorg/threeten/bp/LocalTime;

    move-result-object p0

    invoke-static {v0, p0}, Lorg/threeten/bp/LocalDateTime;->of(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalDateTime;

    move-result-object p0

    const-string v0, "LocalDateTime.of(endDate, endTime)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final getStartDateTime(Lcom/squareup/customreport/data/ReportConfig;)Lorg/threeten/bp/LocalDateTime;
    .locals 1

    const-string v0, "$this$startDateTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/customreport/data/ReportConfig;->getStartTime()Lorg/threeten/bp/LocalTime;

    move-result-object p0

    invoke-static {v0, p0}, Lorg/threeten/bp/LocalDateTime;->of(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalDateTime;

    move-result-object p0

    const-string v0, "LocalDateTime.of(startDate, startTime)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final withAllDay(Lcom/squareup/customreport/data/ReportConfig;Z)Lcom/squareup/customreport/data/ReportConfig;
    .locals 13

    const-string v0, "$this$withAllDay"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    .line 43
    sget-object v0, Lorg/threeten/bp/LocalTime;->MIN:Lorg/threeten/bp/LocalTime;

    goto :goto_0

    .line 45
    :cond_0
    sget-object v0, Lcom/squareup/customreport/data/ReportConfig;->Companion:Lcom/squareup/customreport/data/ReportConfig$Companion;

    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig$Companion;->getDEFAULT_START_TIME()Lorg/threeten/bp/LocalTime;

    move-result-object v0

    :goto_0
    move-object v4, v0

    const-string v0, "if (allDay) {\n      Loca\u2026.DEFAULT_START_TIME\n    }"

    .line 42
    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_1

    .line 48
    sget-object v0, Lorg/threeten/bp/LocalTime;->MAX:Lorg/threeten/bp/LocalTime;

    goto :goto_1

    .line 50
    :cond_1
    sget-object v0, Lcom/squareup/customreport/data/ReportConfig;->Companion:Lcom/squareup/customreport/data/ReportConfig$Companion;

    invoke-virtual {v0}, Lcom/squareup/customreport/data/ReportConfig$Companion;->getDEFAULT_END_TIME()Lorg/threeten/bp/LocalTime;

    move-result-object v0

    :goto_1
    move-object v5, v0

    const-string v0, "if (allDay) {\n      Loca\u2026ig.DEFAULT_END_TIME\n    }"

    .line 47
    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x1e3

    const/4 v12, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v1, p0

    move v6, p1

    .line 40
    invoke-static/range {v1 .. v12}, Lcom/squareup/customreport/data/ReportConfig;->copy$default(Lcom/squareup/customreport/data/ReportConfig;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;Lorg/threeten/bp/LocalTime;ZZLcom/squareup/customreport/data/EmployeeFiltersSelection;Lcom/squareup/customreport/data/RangeSelection;Lcom/squareup/customreport/data/ComparisonRange;ILjava/lang/Object;)Lcom/squareup/customreport/data/ReportConfig;

    move-result-object p0

    return-object p0
.end method
