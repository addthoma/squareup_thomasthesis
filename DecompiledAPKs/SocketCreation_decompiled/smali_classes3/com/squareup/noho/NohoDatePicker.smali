.class public final Lcom/squareup/noho/NohoDatePicker;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "NohoDatePicker.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoDatePicker$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoDatePicker.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoDatePicker.kt\ncom/squareup/noho/NohoDatePicker\n+ 2 View.kt\nandroidx/core/view/ViewKt\n+ 3 Delegates.kt\ncom/squareup/util/DelegatesKt\n+ 4 Delegates.kt\nkotlin/properties/Delegates\n*L\n1#1,169:1\n253#2,2:170\n45#3:172\n45#3:176\n33#4,3:173\n33#4,3:177\n*E\n*S KotlinDebug\n*F\n+ 1 NohoDatePicker.kt\ncom/squareup/noho/NohoDatePicker\n*L\n47#1,2:170\n31#1:172\n32#1:176\n31#1,3:173\n32#1,3:177\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0013\u0018\u0000 72\u00020\u0001:\u00017B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0018\u0010,\u001a\u00020\u00072\u0006\u0010-\u001a\u00020\u00072\u0006\u0010.\u001a\u00020\u0007H\u0002J\u0017\u0010/\u001a\u0004\u0018\u00010\u001f2\u0006\u00100\u001a\u00020\nH\u0002\u00a2\u0006\u0002\u00101J\u0008\u00102\u001a\u00020\u001fH\u0002J\u0008\u00103\u001a\u00020\u001fH\u0002J\u0008\u00104\u001a\u00020\u001fH\u0002J \u00105\u001a\u00020\u001f2\u0006\u0010.\u001a\u00020\u00072\u0006\u0010-\u001a\u00020\u00072\u0006\u00106\u001a\u00020\u0007H\u0002R$\u0010\u000b\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n8F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R+\u0010\u0013\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\n8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0016\u0010\u0017\u001a\u0004\u0008\u0014\u0010\r\"\u0004\u0008\u0015\u0010\u000fR+\u0010\u0018\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\n8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u001b\u0010\u0017\u001a\u0004\u0008\u0019\u0010\r\"\u0004\u0008\u001a\u0010\u000fR\u000e\u0010\u001c\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R.\u0010\u001d\u001a\u0016\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u001f\u0018\u00010\u001eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008 \u0010!\"\u0004\u0008\"\u0010#R$\u0010&\u001a\u00020%2\u0006\u0010$\u001a\u00020%@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\'\u0010(\"\u0004\u0008)\u0010*R\u000e\u0010+\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00068"
    }
    d2 = {
        "Lcom/squareup/noho/NohoDatePicker;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "date",
        "Lorg/threeten/bp/LocalDate;",
        "currentDate",
        "getCurrentDate",
        "()Lorg/threeten/bp/LocalDate;",
        "setCurrentDate",
        "(Lorg/threeten/bp/LocalDate;)V",
        "dayPicker",
        "Lcom/squareup/noho/NohoNumberPicker;",
        "<set-?>",
        "maxDate",
        "getMaxDate",
        "setMaxDate",
        "maxDate$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "minDate",
        "getMinDate",
        "setMinDate",
        "minDate$delegate",
        "monthPicker",
        "onValueChangeListener",
        "Lkotlin/Function2;",
        "",
        "getOnValueChangeListener",
        "()Lkotlin/jvm/functions/Function2;",
        "setOnValueChangeListener",
        "(Lkotlin/jvm/functions/Function2;)V",
        "value",
        "",
        "yearEnabled",
        "getYearEnabled",
        "()Z",
        "setYearEnabled",
        "(Z)V",
        "yearPicker",
        "getDaysInMonth",
        "month",
        "year",
        "notifyChange",
        "newValue",
        "(Lorg/threeten/bp/LocalDate;)Lkotlin/Unit;",
        "recalculateMaxPickerValues",
        "setupFields",
        "setupListeners",
        "updatePickers",
        "day",
        "Companion",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/squareup/noho/NohoDatePicker$Companion;

.field private static final DEFAULT_MAX_DATE:Lorg/threeten/bp/LocalDate;

.field private static final DEFAULT_MIN_DATE:Lorg/threeten/bp/LocalDate;


# instance fields
.field private final dayPicker:Lcom/squareup/noho/NohoNumberPicker;

.field private final maxDate$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final minDate$delegate:Lkotlin/properties/ReadWriteProperty;

.field private final monthPicker:Lcom/squareup/noho/NohoNumberPicker;

.field private onValueChangeListener:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/noho/NohoDatePicker;",
            "-",
            "Lorg/threeten/bp/LocalDate;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private yearEnabled:Z

.field private final yearPicker:Lcom/squareup/noho/NohoNumberPicker;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/noho/NohoDatePicker;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "minDate"

    const-string v5, "getMinDate()Lorg/threeten/bp/LocalDate;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v3, "maxDate"

    const-string v4, "getMaxDate()Lorg/threeten/bp/LocalDate;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/noho/NohoDatePicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/squareup/noho/NohoDatePicker$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/noho/NohoDatePicker$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/noho/NohoDatePicker;->Companion:Lcom/squareup/noho/NohoDatePicker$Companion;

    .line 163
    sget-object v0, Lorg/threeten/bp/Month;->JANUARY:Lorg/threeten/bp/Month;

    const/16 v1, 0x76c

    invoke-static {v1, v0, v2}, Lorg/threeten/bp/LocalDate;->of(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-string v1, "LocalDate.of(1900, Month.JANUARY, 1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/noho/NohoDatePicker;->DEFAULT_MIN_DATE:Lorg/threeten/bp/LocalDate;

    .line 166
    sget-object v0, Lorg/threeten/bp/Month;->DECEMBER:Lorg/threeten/bp/Month;

    const/16 v1, 0x833

    const/16 v2, 0x1f

    invoke-static {v1, v0, v2}, Lorg/threeten/bp/LocalDate;->of(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-string v1, "LocalDate.of(2099, Month.DECEMBER, 31)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/noho/NohoDatePicker;->DEFAULT_MAX_DATE:Lorg/threeten/bp/LocalDate;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    sget-object p1, Lcom/squareup/noho/NohoDatePicker;->DEFAULT_MIN_DATE:Lorg/threeten/bp/LocalDate;

    .line 172
    sget-object p2, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    .line 173
    new-instance p2, Lcom/squareup/noho/NohoDatePicker$$special$$inlined$observable$1;

    invoke-direct {p2, p1, p1, p0}, Lcom/squareup/noho/NohoDatePicker$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/NohoDatePicker;)V

    check-cast p2, Lkotlin/properties/ReadWriteProperty;

    .line 172
    iput-object p2, p0, Lcom/squareup/noho/NohoDatePicker;->minDate$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 32
    sget-object p1, Lcom/squareup/noho/NohoDatePicker;->DEFAULT_MAX_DATE:Lorg/threeten/bp/LocalDate;

    .line 176
    sget-object p2, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    .line 177
    new-instance p2, Lcom/squareup/noho/NohoDatePicker$$special$$inlined$observable$2;

    invoke-direct {p2, p1, p1, p0}, Lcom/squareup/noho/NohoDatePicker$$special$$inlined$observable$2;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/NohoDatePicker;)V

    check-cast p2, Lkotlin/properties/ReadWriteProperty;

    .line 176
    iput-object p2, p0, Lcom/squareup/noho/NohoDatePicker;->maxDate$delegate:Lkotlin/properties/ReadWriteProperty;

    const/4 p1, 0x1

    .line 43
    iput-boolean p1, p0, Lcom/squareup/noho/NohoDatePicker;->yearEnabled:Z

    .line 65
    sget p2, Lcom/squareup/noho/R$layout;->noho_date_picker_contents:I

    move-object p3, p0

    check-cast p3, Landroid/view/ViewGroup;

    invoke-static {p2, p3, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 67
    sget p1, Lcom/squareup/noho/R$id;->month_picker:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoNumberPicker;

    iput-object p1, p0, Lcom/squareup/noho/NohoDatePicker;->monthPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 68
    sget p1, Lcom/squareup/noho/R$id;->day_picker:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoNumberPicker;

    iput-object p1, p0, Lcom/squareup/noho/NohoDatePicker;->dayPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 69
    sget p1, Lcom/squareup/noho/R$id;->year_picker:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoNumberPicker;

    iput-object p1, p0, Lcom/squareup/noho/NohoDatePicker;->yearPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 72
    invoke-direct {p0}, Lcom/squareup/noho/NohoDatePicker;->setupFields()V

    .line 75
    invoke-static {}, Lorg/threeten/bp/LocalDate;->now()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    .line 76
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result p2

    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result p3

    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result p1

    invoke-direct {p0, p2, p3, p1}, Lcom/squareup/noho/NohoDatePicker;->updatePickers(III)V

    .line 79
    invoke-direct {p0}, Lcom/squareup/noho/NohoDatePicker;->setupListeners()V

    .line 80
    invoke-direct {p0}, Lcom/squareup/noho/NohoDatePicker;->recalculateMaxPickerValues()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 23
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 24
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoDatePicker;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getDEFAULT_MAX_DATE$cp()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/noho/NohoDatePicker;->DEFAULT_MAX_DATE:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public static final synthetic access$getDEFAULT_MIN_DATE$cp()Lorg/threeten/bp/LocalDate;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/noho/NohoDatePicker;->DEFAULT_MIN_DATE:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public static final synthetic access$notifyChange(Lcom/squareup/noho/NohoDatePicker;Lorg/threeten/bp/LocalDate;)Lkotlin/Unit;
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoDatePicker;->notifyChange(Lorg/threeten/bp/LocalDate;)Lkotlin/Unit;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$recalculateMaxPickerValues(Lcom/squareup/noho/NohoDatePicker;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/noho/NohoDatePicker;->recalculateMaxPickerValues()V

    return-void
.end method

.method private final getDaysInMonth(II)I
    .locals 0

    .line 157
    invoke-static {p2, p1}, Lorg/threeten/bp/YearMonth;->of(II)Lorg/threeten/bp/YearMonth;

    move-result-object p1

    invoke-virtual {p1}, Lorg/threeten/bp/YearMonth;->lengthOfMonth()I

    move-result p1

    return p1
.end method

.method private final notifyChange(Lorg/threeten/bp/LocalDate;)Lkotlin/Unit;
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->onValueChangeListener:Lkotlin/jvm/functions/Function2;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0, p1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final recalculateMaxPickerValues()V
    .locals 3

    .line 128
    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->monthPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 129
    iget-object v1, p0, Lcom/squareup/noho/NohoDatePicker;->yearPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result v1

    goto :goto_0

    :cond_0
    sget-object v1, Lorg/threeten/bp/Month;->JANUARY:Lorg/threeten/bp/Month;

    invoke-virtual {v1}, Lorg/threeten/bp/Month;->getValue()I

    move-result v1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 130
    iget-object v1, p0, Lcom/squareup/noho/NohoDatePicker;->yearPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result v1

    goto :goto_1

    :cond_1
    sget-object v1, Lorg/threeten/bp/Month;->DECEMBER:Lorg/threeten/bp/Month;

    invoke-virtual {v1}, Lorg/threeten/bp/Month;->getValue()I

    move-result v1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 133
    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->dayPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 134
    iget-object v1, p0, Lcom/squareup/noho/NohoDatePicker;->yearPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/squareup/noho/NohoDatePicker;->monthPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 136
    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x1

    .line 134
    :goto_2
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 140
    iget-object v1, p0, Lcom/squareup/noho/NohoDatePicker;->yearPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/squareup/noho/NohoDatePicker;->monthPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 142
    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result v1

    goto :goto_3

    .line 144
    :cond_3
    iget-object v1, p0, Lcom/squareup/noho/NohoDatePicker;->monthPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/noho/NohoDatePicker;->yearPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/squareup/noho/NohoDatePicker;->getDaysInMonth(II)I

    move-result v1

    .line 140
    :goto_3
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 148
    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->yearPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 149
    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 150
    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    return-void
.end method

.method private final setupFields()V
    .locals 3

    .line 84
    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->monthPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 85
    sget-object v1, Lorg/threeten/bp/Month;->JANUARY:Lorg/threeten/bp/Month;

    invoke-virtual {v1}, Lorg/threeten/bp/Month;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 86
    sget-object v1, Lorg/threeten/bp/Month;->DECEMBER:Lorg/threeten/bp/Month;

    invoke-virtual {v1}, Lorg/threeten/bp/Month;->getValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 88
    new-instance v1, Ljava/text/DateFormatSymbols;

    invoke-direct {v1}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v1

    const-string v2, "DateFormatSymbols().months"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    new-instance v2, Lcom/squareup/noho/NohoDatePicker$setupFields$1$1;

    invoke-direct {v2, v1}, Lcom/squareup/noho/NohoDatePicker$setupFields$1$1;-><init>([Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/noho/NohoNumberPicker$Formatter;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoNumberPicker;->setFormatter(Lcom/squareup/noho/NohoNumberPicker$Formatter;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->dayPicker:Lcom/squareup/noho/NohoNumberPicker;

    const/4 v1, 0x1

    .line 94
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    const/16 v1, 0x1f

    .line 95
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    .line 98
    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->yearPicker:Lcom/squareup/noho/NohoNumberPicker;

    .line 99
    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMinDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMinValue(I)V

    .line 100
    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getMaxDate()Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setMaxValue(I)V

    const-string v1, "8888"

    .line 102
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoNumberPicker;->setWidestFormattedValue(Ljava/lang/String;)V

    return-void
.end method

.method private final setupListeners()V
    .locals 2

    .line 107
    new-instance v0, Lcom/squareup/noho/NohoDatePicker$setupListeners$listener$1;

    invoke-direct {v0, p0}, Lcom/squareup/noho/NohoDatePicker$setupListeners$listener$1;-><init>(Lcom/squareup/noho/NohoDatePicker;)V

    check-cast v0, Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;

    .line 112
    iget-object v1, p0, Lcom/squareup/noho/NohoDatePicker;->yearPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoNumberPicker;->setOnValueChangedListener(Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V

    .line 113
    iget-object v1, p0, Lcom/squareup/noho/NohoDatePicker;->monthPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoNumberPicker;->setOnValueChangedListener(Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V

    .line 114
    iget-object v1, p0, Lcom/squareup/noho/NohoDatePicker;->dayPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoNumberPicker;->setOnValueChangedListener(Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V

    return-void
.end method

.method private final updatePickers(III)V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->monthPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    .line 123
    iget-object p2, p0, Lcom/squareup/noho/NohoDatePicker;->dayPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {p2, p3}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    .line 124
    iget-object p2, p0, Lcom/squareup/noho/NohoDatePicker;->yearPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoNumberPicker;->setValue(I)V

    return-void
.end method


# virtual methods
.method public final getCurrentDate()Lorg/threeten/bp/LocalDate;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->yearPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/noho/NohoDatePicker;->monthPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/noho/NohoDatePicker;->dayPicker:Lcom/squareup/noho/NohoNumberPicker;

    invoke-virtual {v2}, Lcom/squareup/noho/NohoNumberPicker;->getValue()I

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-string v1, "LocalDate.of(yearPicker.\u2026r.value, dayPicker.value)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getMaxDate()Lorg/threeten/bp/LocalDate;
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->maxDate$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoDatePicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getMinDate()Lorg/threeten/bp/LocalDate;
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->minDate$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoDatePicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public final getOnValueChangeListener()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/squareup/noho/NohoDatePicker;",
            "Lorg/threeten/bp/LocalDate;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->onValueChangeListener:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getYearEnabled()Z
    .locals 1

    .line 43
    iget-boolean v0, p0, Lcom/squareup/noho/NohoDatePicker;->yearEnabled:Z

    return v0
.end method

.method public final setCurrentDate(Lorg/threeten/bp/LocalDate;)V
    .locals 2

    const-string v0, "date"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v0

    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getMonthValue()I

    move-result v1

    invoke-virtual {p1}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result p1

    invoke-direct {p0, v0, v1, p1}, Lcom/squareup/noho/NohoDatePicker;->updatePickers(III)V

    .line 40
    invoke-direct {p0}, Lcom/squareup/noho/NohoDatePicker;->recalculateMaxPickerValues()V

    return-void
.end method

.method public final setMaxDate(Lorg/threeten/bp/LocalDate;)V
    .locals 3

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->maxDate$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoDatePicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setMinDate(Lorg/threeten/bp/LocalDate;)V
    .locals 3

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->minDate$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoDatePicker;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public final setOnValueChangeListener(Lkotlin/jvm/functions/Function2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/noho/NohoDatePicker;",
            "-",
            "Lorg/threeten/bp/LocalDate;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 62
    iput-object p1, p0, Lcom/squareup/noho/NohoDatePicker;->onValueChangeListener:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final setYearEnabled(Z)V
    .locals 3

    .line 45
    iget-boolean v0, p0, Lcom/squareup/noho/NohoDatePicker;->yearEnabled:Z

    if-ne p1, v0, :cond_0

    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoDatePicker;->yearPicker:Lcom/squareup/noho/NohoNumberPicker;

    check-cast v0, Landroid/view/View;

    const/16 v1, 0x8

    .line 170
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_1

    .line 49
    sget v0, Lcom/squareup/noho/R$layout;->noho_date_picker_contents:I

    goto :goto_0

    .line 51
    :cond_1
    sget v0, Lcom/squareup/noho/R$layout;->noho_date_picker_contents_no_year:I

    .line 54
    :goto_0
    new-instance v1, Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoDatePicker;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;)V

    check-cast v1, Landroid/view/ViewGroup;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 55
    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {v1}, Landroidx/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;)V

    .line 56
    new-instance v1, Landroidx/constraintlayout/widget/ConstraintSet;

    invoke-direct {v1}, Landroidx/constraintlayout/widget/ConstraintSet;-><init>()V

    .line 57
    invoke-virtual {v1, v0}, Landroidx/constraintlayout/widget/ConstraintSet;->clone(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 58
    move-object v0, p0

    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v1, v0}, Landroidx/constraintlayout/widget/ConstraintSet;->applyTo(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 59
    iput-boolean p1, p0, Lcom/squareup/noho/NohoDatePicker;->yearEnabled:Z

    return-void
.end method
