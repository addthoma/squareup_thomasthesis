.class final Lcom/squareup/noho/NohoRow$TextIconFeature;
.super Lcom/squareup/noho/NohoRow$FeatureProperty;
.source "NohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TextIconFeature"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/noho/NohoRow$FeatureProperty<",
        "Lcom/squareup/noho/NohoRow$Icon$TextIcon;",
        "Landroidx/appcompat/widget/AppCompatTextView;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRow$TextIconFeature\n*L\n1#1,1059:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u00c2\u0002\u0018\u00002\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0008\u001a\u00020\tH\u0014J\u001a\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00032\u0008\u0010\r\u001a\u0004\u0018\u00010\u0002H\u0014J\u0012\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\r\u001a\u0004\u0018\u00010\u0002H\u0014J\u0018\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\tH\u0014\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/noho/NohoRow$TextIconFeature;",
        "Lcom/squareup/noho/NohoRow$FeatureProperty;",
        "Lcom/squareup/noho/NohoRow$Icon$TextIcon;",
        "Landroidx/appcompat/widget/AppCompatTextView;",
        "()V",
        "create",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "styleId",
        "",
        "doSetValue",
        "",
        "view",
        "value",
        "shouldShowFor",
        "",
        "updateStyleId",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/noho/NohoRow$TextIconFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 951
    new-instance v0, Lcom/squareup/noho/NohoRow$TextIconFeature;

    invoke-direct {v0}, Lcom/squareup/noho/NohoRow$TextIconFeature;-><init>()V

    sput-object v0, Lcom/squareup/noho/NohoRow$TextIconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$TextIconFeature;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .line 953
    sget v1, Lcom/squareup/noho/R$id;->textIcon:I

    .line 954
    sget v2, Lcom/squareup/noho/R$dimen;->noho_row_gap_size:I

    .line 957
    sget v5, Lcom/squareup/noho/R$style;->TextAppearance_Widget_Noho_Row_TextIcon:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    .line 952
    invoke-direct/range {v0 .. v6}, Lcom/squareup/noho/NohoRow$FeatureProperty;-><init>(IIIIILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lcom/squareup/noho/NohoRow;I)Landroid/view/View;
    .locals 0

    .line 951
    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$TextIconFeature;->create(Lcom/squareup/noho/NohoRow;I)Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method protected create(Lcom/squareup/noho/NohoRow;I)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 2

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 964
    new-instance v0, Landroidx/appcompat/widget/AppCompatTextView;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/appcompat/widget/AppCompatTextView;-><init>(Landroid/content/Context;)V

    .line 965
    move-object v1, v0

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1, p2}, Landroidx/core/widget/TextViewCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    .line 966
    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object p2

    check-cast p2, Landroid/graphics/Paint;

    invoke-static {p2}, Lcom/squareup/marketfont/MarketUtils;->configureOptimalPaintFlags(Landroid/graphics/Paint;)V

    .line 968
    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getMinHeight()I

    move-result p1

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/AppCompatTextView;->setMinWidth(I)V

    const/16 p1, 0x11

    .line 969
    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/AppCompatTextView;->setGravity(I)V

    .line 970
    new-instance p1, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    const/4 p2, -0x2

    const/4 v1, 0x0

    invoke-direct {p1, p2, v1}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast p1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, p1}, Landroidx/appcompat/widget/AppCompatTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public bridge synthetic doSetValue(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .line 951
    check-cast p1, Landroidx/appcompat/widget/AppCompatTextView;

    check-cast p2, Lcom/squareup/noho/NohoRow$Icon$TextIcon;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$TextIconFeature;->doSetValue(Landroidx/appcompat/widget/AppCompatTextView;Lcom/squareup/noho/NohoRow$Icon$TextIcon;)V

    return-void
.end method

.method protected doSetValue(Landroidx/appcompat/widget/AppCompatTextView;Lcom/squareup/noho/NohoRow$Icon$TextIcon;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_2

    .line 983
    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->getText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatTextView;->setText(Ljava/lang/CharSequence;)V

    .line 984
    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->getShapeId()I

    move-result v0

    if-nez v0, :cond_0

    .line 985
    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->getBackground()I

    move-result p2

    invoke-virtual {p1, p2}, Landroidx/appcompat/widget/AppCompatTextView;->setBackgroundColor(I)V

    .line 986
    new-instance p2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v1, 0x0

    invoke-direct {p2, v0, v1}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast p2, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, p2}, Landroidx/appcompat/widget/AppCompatTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 988
    :cond_0
    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->getShapeId()I

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v3, v2, v3}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable$default(Landroid/content/Context;ILandroid/content/res/Resources$Theme;ILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 989
    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->getBackground()I

    move-result v1

    if-eqz v1, :cond_1

    .line 990
    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->getBackground()I

    move-result p2

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 991
    sget-object p2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 993
    :cond_1
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatTextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 995
    new-instance p2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-direct {p2, v1, v0}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast p2, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1, p2}, Landroidx/appcompat/widget/AppCompatTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    return-void

    .line 982
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "doSetValue called for null textIcon!"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method protected shouldShowFor(Lcom/squareup/noho/NohoRow$Icon$TextIcon;)Z
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic shouldShowFor(Ljava/lang/Object;)Z
    .locals 0

    .line 951
    check-cast p1, Lcom/squareup/noho/NohoRow$Icon$TextIcon;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow$TextIconFeature;->shouldShowFor(Lcom/squareup/noho/NohoRow$Icon$TextIcon;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic updateStyleId(Landroid/view/View;I)V
    .locals 0

    .line 951
    check-cast p1, Landroidx/appcompat/widget/AppCompatTextView;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$TextIconFeature;->updateStyleId(Landroidx/appcompat/widget/AppCompatTextView;I)V

    return-void
.end method

.method protected updateStyleId(Landroidx/appcompat/widget/AppCompatTextView;I)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 978
    check-cast p1, Landroid/widget/TextView;

    invoke-static {p1, p2}, Landroidx/core/widget/TextViewCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    return-void
.end method
