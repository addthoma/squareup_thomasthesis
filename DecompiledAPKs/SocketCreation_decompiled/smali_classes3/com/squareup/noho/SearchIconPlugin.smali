.class public final Lcom/squareup/noho/SearchIconPlugin;
.super Lcom/squareup/noho/IconPlugin;
.source "NohoEditRowPlugins.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/noho/SearchIconPlugin;",
        "Lcom/squareup/noho/IconPlugin;",
        "context",
        "Landroid/content/Context;",
        "tintColor",
        "",
        "(Landroid/content/Context;I)V",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 7

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    sget-object v3, Lcom/squareup/noho/NohoEditRow$Side;->START:Lcom/squareup/noho/NohoEditRow$Side;

    sget v4, Lcom/squareup/vectoricons/R$drawable;->icon_magnifying_glass:I

    .line 130
    sget v5, Lcom/squareup/noho/R$dimen;->noho_edit_search_margin:I

    move-object v1, p0

    move-object v2, p1

    move v6, p2

    .line 128
    invoke-direct/range {v1 .. v6}, Lcom/squareup/noho/IconPlugin;-><init>(Landroid/content/Context;Lcom/squareup/noho/NohoEditRow$Side;III)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 126
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/SearchIconPlugin;-><init>(Landroid/content/Context;I)V

    return-void
.end method
