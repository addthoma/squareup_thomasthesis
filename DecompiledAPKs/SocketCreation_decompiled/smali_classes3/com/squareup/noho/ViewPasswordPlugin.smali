.class public final Lcom/squareup/noho/ViewPasswordPlugin;
.super Lcom/squareup/noho/IconPlugin;
.source "NohoEditRowPlugins.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0008\u0010\u0010\u001a\u00020\u0008H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u00020\u00088VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\t\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/noho/ViewPasswordPlugin;",
        "Lcom/squareup/noho/IconPlugin;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "editText",
        "Lcom/squareup/noho/NohoEditRow;",
        "isClickable",
        "",
        "()Z",
        "attach",
        "",
        "description",
        "",
        "editDescription",
        "",
        "onClick",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private editText:Lcom/squareup/noho/NohoEditRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    sget-object v2, Lcom/squareup/noho/NohoEditRow$Side;->END:Lcom/squareup/noho/NohoEditRow$Side;

    .line 192
    invoke-static {p1}, Lcom/squareup/noho/NohoEditRowPluginsKt;->passwordIcon(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 193
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/noho/R$dimen;->noho_edit_default_margin:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v1, p0

    .line 190
    invoke-direct/range {v1 .. v7}, Lcom/squareup/noho/IconPlugin;-><init>(Lcom/squareup/noho/NohoEditRow$Side;Landroid/graphics/drawable/Drawable;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public attach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    invoke-super {p0, p1}, Lcom/squareup/noho/IconPlugin;->attach(Lcom/squareup/noho/NohoEditRow;)V

    .line 199
    iput-object p1, p0, Lcom/squareup/noho/ViewPasswordPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    return-void
.end method

.method public description(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    const-string v0, "editDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    iget-object p1, p0, Lcom/squareup/noho/ViewPasswordPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez p1, :cond_0

    const-string v0, "editText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/noho/R$string;->noho_editrow_plugin_view_password:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, "editText.resources.getSt\u2026row_plugin_view_password)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public isClickable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onClick()Z
    .locals 3

    .line 206
    iget-object v0, p0, Lcom/squareup/noho/ViewPasswordPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    const-string v1, "editText"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/squareup/noho/ViewPasswordPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/noho/NohoEditRow;->getTransformationMethod()Landroid/text/method/TransformationMethod;

    move-result-object v1

    instance-of v1, v1, Landroid/text/method/PasswordTransformationMethod;

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    goto :goto_0

    .line 207
    :cond_2
    new-instance v1, Landroid/text/method/PasswordTransformationMethod;

    invoke-direct {v1}, Landroid/text/method/PasswordTransformationMethod;-><init>()V

    check-cast v1, Landroid/text/method/TransformationMethod;

    .line 206
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    const/4 v0, 0x1

    return v0
.end method
