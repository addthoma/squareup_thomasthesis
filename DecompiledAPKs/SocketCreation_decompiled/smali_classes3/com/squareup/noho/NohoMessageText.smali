.class public Lcom/squareup/noho/NohoMessageText;
.super Lcom/squareup/widgets/SelectableEditText;
.source "NohoMessageText.java"


# static fields
.field private static final SCROLL_MARGIN_BOTTOM_PX:I = 0x3


# instance fields
.field private final edgeController:Lcom/squareup/noho/NohoEdgeController;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 37
    sget v0, Lcom/squareup/noho/R$attr;->nohoMessageTextStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/noho/NohoMessageText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/widgets/SelectableEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    new-instance p1, Lcom/squareup/noho/NohoEdgeController;

    const/4 v0, 0x0

    invoke-direct {p1, p0, p2, p3, v0}, Lcom/squareup/noho/NohoEdgeController;-><init>(Landroid/view/View;Landroid/util/AttributeSet;II)V

    iput-object p1, p0, Lcom/squareup/noho/NohoMessageText;->edgeController:Lcom/squareup/noho/NohoEdgeController;

    const/4 p1, 0x0

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoMessageText;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 p1, 0x1

    .line 47
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoMessageText;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private adjustRect(Landroid/graphics/Rect;)V
    .locals 1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/noho/NohoMessageText;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    return-void
.end method


# virtual methods
.method public getFocusedRect(Landroid/graphics/Rect;)V
    .locals 0

    .line 77
    invoke-super {p0, p1}, Lcom/squareup/widgets/SelectableEditText;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoMessageText;->adjustRect(Landroid/graphics/Rect;)V

    return-void
.end method

.method public getOffsetForPosition(FF)I
    .locals 0

    .line 67
    invoke-super {p0, p1, p2}, Lcom/squareup/widgets/SelectableEditText;->getOffsetForPosition(FF)I

    move-result p1

    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/noho/NohoMessageText;->edgeController:Lcom/squareup/noho/NohoEdgeController;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEdgeController;->onDraw(Landroid/graphics/Canvas;)V

    .line 58
    invoke-super {p0, p1}, Lcom/squareup/widgets/SelectableEditText;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 51
    invoke-super/range {p0 .. p5}, Lcom/squareup/widgets/SelectableEditText;->onLayout(ZIIII)V

    const p1, 0x800033

    .line 52
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoMessageText;->setGravity(I)V

    return-void
.end method

.method public requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z
    .locals 0

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoMessageText;->adjustRect(Landroid/graphics/Rect;)V

    .line 73
    invoke-super {p0, p1, p2}, Lcom/squareup/widgets/SelectableEditText;->requestRectangleOnScreen(Landroid/graphics/Rect;Z)Z

    move-result p1

    return p1
.end method
