.class final Lcom/squareup/noho/NohoInputBox$validatedPlugin$2;
.super Lkotlin/jvm/internal/Lambda;
.source "NohoInputBox.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NohoInputBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/noho/HideablePlugin;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/noho/HideablePlugin;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/noho/NohoInputBox;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoInputBox;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/noho/NohoInputBox$validatedPlugin$2;->this$0:Lcom/squareup/noho/NohoInputBox;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/noho/HideablePlugin;
    .locals 3

    .line 142
    iget-object v0, p0, Lcom/squareup/noho/NohoInputBox$validatedPlugin$2;->this$0:Lcom/squareup/noho/NohoInputBox;

    .line 143
    sget v1, Lcom/squareup/vectoricons/R$drawable;->circle_check_24:I

    .line 144
    sget v2, Lcom/squareup/noho/R$color;->noho_edit_background_underline_validated:I

    .line 142
    invoke-static {v0, v1, v2}, Lcom/squareup/noho/NohoInputBox;->access$createStatusPlugin(Lcom/squareup/noho/NohoInputBox;II)Lcom/squareup/noho/HideablePlugin;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 55
    invoke-virtual {p0}, Lcom/squareup/noho/NohoInputBox$validatedPlugin$2;->invoke()Lcom/squareup/noho/HideablePlugin;

    move-result-object v0

    return-object v0
.end method
