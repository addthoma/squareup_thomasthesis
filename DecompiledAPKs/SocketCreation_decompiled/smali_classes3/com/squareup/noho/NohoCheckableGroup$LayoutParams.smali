.class public Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;
.super Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;
.source "NohoCheckableGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoCheckableGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .line 457
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(II)V

    return-void
.end method

.method public constructor <init>(IIF)V
    .locals 0

    .line 462
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(IIF)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 452
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .line 467
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 0

    .line 472
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method protected setBaseAttributes(Landroid/content/res/TypedArray;II)V
    .locals 2

    .line 486
    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    const/4 v1, -0x2

    if-eqz v0, :cond_0

    const-string v0, "layout_width"

    .line 487
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getLayoutDimension(ILjava/lang/String;)I

    move-result p2

    iput p2, p0, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;->width:I

    goto :goto_0

    .line 489
    :cond_0
    iput v1, p0, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;->width:I

    .line 492
    :goto_0
    invoke-virtual {p1, p3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result p2

    if-eqz p2, :cond_1

    const-string p2, "layout_height"

    .line 493
    invoke-virtual {p1, p3, p2}, Landroid/content/res/TypedArray;->getLayoutDimension(ILjava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;->height:I

    goto :goto_1

    .line 495
    :cond_1
    iput v1, p0, Lcom/squareup/noho/NohoCheckableGroup$LayoutParams;->height:I

    :goto_1
    return-void
.end method
