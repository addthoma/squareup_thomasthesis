.class final Lcom/squareup/noho/NohoTimePicker$setupListeners$$inlined$with$lambda$1;
.super Ljava/lang/Object;
.source "NohoTimePicker.kt"

# interfaces
.implements Lcom/squareup/noho/NohoNumberPicker$Formatter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NohoTimePicker;->setupListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoTimePicker.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoTimePicker.kt\ncom/squareup/noho/NohoTimePicker$setupListeners$1$1\n*L\n1#1,154:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "kotlin.jvm.PlatformType",
        "hour",
        "",
        "format",
        "com/squareup/noho/NohoTimePicker$setupListeners$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $listener$inlined:Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;

.field final synthetic this$0:Lcom/squareup/noho/NohoTimePicker;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoTimePicker;Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/noho/NohoTimePicker$setupListeners$$inlined$with$lambda$1;->this$0:Lcom/squareup/noho/NohoTimePicker;

    iput-object p2, p0, Lcom/squareup/noho/NohoTimePicker$setupListeners$$inlined$with$lambda$1;->$listener$inlined:Lcom/squareup/noho/NohoNumberPicker$OnValueChangeListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final format(I)Ljava/lang/String;
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/noho/NohoTimePicker$setupListeners$$inlined$with$lambda$1;->this$0:Lcom/squareup/noho/NohoTimePicker;

    invoke-static {v0}, Lcom/squareup/noho/NohoTimePicker;->access$getUse24Hour$p(Lcom/squareup/noho/NohoTimePicker;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v1

    array-length p1, v0

    invoke-static {v0, p1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    const-string v0, "%02d"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "java.lang.String.format(this, *args)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    const/16 p1, 0xc

    .line 78
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 79
    :cond_1
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method
