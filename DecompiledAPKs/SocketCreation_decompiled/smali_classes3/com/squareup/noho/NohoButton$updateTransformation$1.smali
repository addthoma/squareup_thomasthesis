.class public final Lcom/squareup/noho/NohoButton$updateTransformation$1;
.super Ljava/lang/Object;
.source "NohoButton.kt"

# interfaces
.implements Landroid/text/method/TransformationMethod;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NohoButton;->updateTransformation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoButton.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoButton.kt\ncom/squareup/noho/NohoButton$updateTransformation$1\n*L\n1#1,219:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00003\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u001c\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016J6\u0010\u0007\u001a\u00020\u00082\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0008\u0010\t\u001a\u0004\u0018\u00010\u00032\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016\u00a8\u0006\u0010"
    }
    d2 = {
        "com/squareup/noho/NohoButton$updateTransformation$1",
        "Landroid/text/method/TransformationMethod;",
        "getTransformation",
        "",
        "source",
        "view",
        "Landroid/view/View;",
        "onFocusChanged",
        "",
        "sourceText",
        "focused",
        "",
        "direction",
        "",
        "previouslyFocusedRect",
        "Landroid/graphics/Rect;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $currentSubText:Ljava/lang/CharSequence;

.field final synthetic this$0:Lcom/squareup/noho/NohoButton;


# direct methods
.method constructor <init>(Lcom/squareup/noho/NohoButton;Ljava/lang/CharSequence;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")V"
        }
    .end annotation

    .line 173
    iput-object p1, p0, Lcom/squareup/noho/NohoButton$updateTransformation$1;->this$0:Lcom/squareup/noho/NohoButton;

    iput-object p2, p0, Lcom/squareup/noho/NohoButton$updateTransformation$1;->$currentSubText:Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;
    .locals 4

    .line 186
    new-instance p2, Landroid/text/SpannableStringBuilder;

    invoke-direct {p2}, Landroid/text/SpannableStringBuilder;-><init>()V

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    .line 187
    check-cast p1, Ljava/lang/CharSequence;

    :goto_0
    invoke-virtual {p2, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/16 p1, 0xa

    .line 188
    invoke-virtual {p2, p1}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 189
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p1

    .line 190
    iget-object v0, p0, Lcom/squareup/noho/NohoButton$updateTransformation$1;->$currentSubText:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 191
    invoke-virtual {p2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 192
    iget-object v1, p0, Lcom/squareup/noho/NohoButton$updateTransformation$1;->this$0:Lcom/squareup/noho/NohoButton;

    invoke-static {v1}, Lcom/squareup/noho/NohoButton;->access$getMainSpan$p(Lcom/squareup/noho/NohoButton;)Lcom/squareup/noho/TextAppearanceSpanCompat;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x21

    invoke-virtual {p2, v1, v2, p1, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 193
    iget-object v1, p0, Lcom/squareup/noho/NohoButton$updateTransformation$1;->this$0:Lcom/squareup/noho/NohoButton;

    invoke-static {v1}, Lcom/squareup/noho/NohoButton;->access$getSubTextSpan$p(Lcom/squareup/noho/NohoButton;)Lcom/squareup/noho/TextAppearanceSpanCompat;

    move-result-object v1

    invoke-virtual {p2, v1, p1, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 186
    check-cast p2, Ljava/lang/CharSequence;

    return-object p2
.end method

.method public onFocusChanged(Landroid/view/View;Ljava/lang/CharSequence;ZILandroid/graphics/Rect;)V
    .locals 0

    return-void
.end method
