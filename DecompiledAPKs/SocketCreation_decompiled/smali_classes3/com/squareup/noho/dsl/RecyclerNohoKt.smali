.class public final Lcom/squareup/noho/dsl/RecyclerNohoKt;
.super Ljava/lang/Object;
.source "RecyclerNoho.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerNoho.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 2 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n*L\n1#1,173:1\n61#1,3:183\n65#1:197\n114#1,5:218\n120#1:234\n147#1,5:246\n153#1:262\n342#2,5:174\n344#2,4:179\n328#2:186\n342#2,5:187\n344#2,4:192\n329#2:196\n328#2:198\n342#2,5:199\n344#2,4:204\n329#2:208\n342#2,5:209\n344#2,4:214\n328#2:223\n342#2,5:224\n344#2,4:229\n329#2:233\n328#2:235\n342#2,5:236\n344#2,4:241\n329#2:245\n328#2:251\n342#2,5:252\n344#2,4:257\n329#2:261\n328#2:263\n342#2,5:264\n344#2,4:269\n329#2:273\n328#2:274\n342#2,5:275\n344#2,4:280\n329#2:284\n*E\n*S KotlinDebug\n*F\n+ 1 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n*L\n48#1,3:183\n48#1:197\n100#1,5:218\n100#1:234\n133#1,5:246\n133#1:262\n32#1,5:174\n32#1,4:179\n48#1:186\n48#1,5:187\n48#1,4:192\n48#1:196\n61#1:198\n61#1,5:199\n61#1,4:204\n61#1:208\n81#1,5:209\n81#1,4:214\n100#1:223\n100#1,5:224\n100#1,4:229\n100#1:233\n114#1:235\n114#1,5:236\n114#1,4:241\n114#1:245\n133#1:251\n133#1,5:252\n133#1,4:257\n133#1:261\n147#1:263\n147#1,5:264\n147#1,4:269\n147#1:273\n166#1:274\n166#1,5:275\n166#1,4:280\n166#1:284\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001aM\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u001a\u0008\u0004\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00010\tH\u0087\u0008\u001aS\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072 \u0008\u0008\u0010\u0008\u001a\u001a\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00010\u000bH\u0087\u0008\u001aS\u0010\r\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u000e2 \u0008\u0008\u0010\u0008\u001a\u001a\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00010\u000bH\u0087\u0008\u001aE\u0010\u0010\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052\u001a\u0008\u0004\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00010\tH\u0087\u0008\u001aK\u0010\u0010\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052 \u0008\u0008\u0010\u0008\u001a\u001a\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00010\u000bH\u0087\u0008\u001aV\u0010\u0010\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052+\u0008\u0004\u0010\u0012\u001a%\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u00110\u0014\u0012\u0004\u0012\u00020\u00010\u0013\u00a2\u0006\u0002\u0008\u0015H\u0087\u0008\u001aM\u0010\u0010\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00162\u001a\u0008\u0004\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00010\tH\u0087\u0008\u001aS\u0010\u0010\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00162 \u0008\u0008\u0010\u0008\u001a\u001a\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00010\u000bH\u0087\u0008\u001a^\u0010\u0010\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u0002*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00162+\u0008\u0004\u0010\u0012\u001a%\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u00170\u0014\u0012\u0004\u0012\u00020\u00010\u0013\u00a2\u0006\u0002\u0008\u0015H\u0087\u0008\u00a8\u0006\u0018"
    }
    d2 = {
        "nohoButton",
        "",
        "I",
        "",
        "S",
        "Lcom/squareup/cycler/Recycler$Config;",
        "type",
        "Lcom/squareup/noho/NohoButtonType;",
        "bindBlock",
        "Lkotlin/Function2;",
        "Lcom/squareup/noho/NohoButton;",
        "Lkotlin/Function3;",
        "",
        "nohoLabel",
        "Lcom/squareup/noho/NohoLabel$Type;",
        "Lcom/squareup/noho/NohoLabel;",
        "nohoRow",
        "Lcom/squareup/noho/NohoRow;",
        "specBlock",
        "Lkotlin/Function1;",
        "Lcom/squareup/cycler/BinderRowSpec;",
        "Lkotlin/ExtensionFunctionType;",
        "Lcom/squareup/noho/CheckType;",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic nohoButton(Lcom/squareup/cycler/Recycler$Config;Lcom/squareup/noho/NohoButtonType;Lkotlin/jvm/functions/Function2;)V
    .locals 2
    .annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;>(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lcom/squareup/noho/NohoButtonType;",
            "Lkotlin/jvm/functions/Function2<",
            "-TS;-",
            "Lcom/squareup/noho/NohoButton;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$nohoButton"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bindBlock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    new-instance v0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoButton$1;

    invoke-direct {v0, p2}, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoButton$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    .line 247
    new-instance p2, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoButton$2;

    invoke-direct {p2, p1}, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoButton$2;-><init>(Lcom/squareup/noho/NohoButtonType;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 253
    new-instance p1, Lcom/squareup/cycler/BinderRowSpec;

    .line 257
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoButton$$inlined$nohoButton$1;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoButton$$inlined$nohoButton$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 253
    invoke-direct {p1, v1, p2}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 251
    invoke-virtual {p1, v0}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 256
    check-cast p1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 252
    invoke-virtual {p0, p1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public static final synthetic nohoButton(Lcom/squareup/cycler/Recycler$Config;Lcom/squareup/noho/NohoButtonType;Lkotlin/jvm/functions/Function3;)V
    .locals 2
    .annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;>(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lcom/squareup/noho/NohoButtonType;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-TS;-",
            "Lcom/squareup/noho/NohoButton;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$nohoButton"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bindBlock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    new-instance v0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoButton$2;

    invoke-direct {v0, p1}, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoButton$2;-><init>(Lcom/squareup/noho/NohoButtonType;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 265
    new-instance p1, Lcom/squareup/cycler/BinderRowSpec;

    .line 269
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoButton$$inlined$row$2;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoButton$$inlined$row$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 265
    invoke-direct {p1, v1, v0}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 263
    invoke-virtual {p1, p2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 268
    check-cast p1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 264
    invoke-virtual {p0, p1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public static final synthetic nohoLabel(Lcom/squareup/cycler/Recycler$Config;Lcom/squareup/noho/NohoLabel$Type;Lkotlin/jvm/functions/Function3;)V
    .locals 2
    .annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;>(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lcom/squareup/noho/NohoLabel$Type;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-TS;-",
            "Lcom/squareup/noho/NohoLabel;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$nohoLabel"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bindBlock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    new-instance v0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoLabel$1;

    invoke-direct {v0, p1}, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoLabel$1;-><init>(Lcom/squareup/noho/NohoLabel$Type;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 276
    new-instance p1, Lcom/squareup/cycler/BinderRowSpec;

    .line 280
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoLabel$$inlined$row$1;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoLabel$$inlined$row$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 276
    invoke-direct {p1, v1, v0}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 274
    invoke-virtual {p1, p2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 279
    check-cast p1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 275
    invoke-virtual {p0, p1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public static final synthetic nohoRow(Lcom/squareup/cycler/Recycler$Config;Lcom/squareup/noho/CheckType;Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;>(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lcom/squareup/noho/CheckType;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/BinderRowSpec<",
            "TI;TS;",
            "Lcom/squareup/noho/NohoCheckableRow;",
            ">;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$nohoRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "specBlock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    new-instance v0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$4;

    invoke-direct {v0, p1}, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$4;-><init>(Lcom/squareup/noho/CheckType;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 210
    new-instance p1, Lcom/squareup/cycler/BinderRowSpec;

    .line 214
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$$inlined$row$4;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$$inlined$row$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 210
    invoke-direct {p1, v1, v0}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 213
    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast p1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 209
    invoke-virtual {p0, p1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public static final synthetic nohoRow(Lcom/squareup/cycler/Recycler$Config;Lcom/squareup/noho/CheckType;Lkotlin/jvm/functions/Function2;)V
    .locals 2
    .annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;>(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lcom/squareup/noho/CheckType;",
            "Lkotlin/jvm/functions/Function2<",
            "-TS;-",
            "Lcom/squareup/noho/NohoCheckableRow;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$nohoRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bindBlock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    new-instance v0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$5;

    invoke-direct {v0, p2}, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$5;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    .line 219
    new-instance p2, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$6;

    invoke-direct {p2, p1}, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$6;-><init>(Lcom/squareup/noho/CheckType;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 225
    new-instance p1, Lcom/squareup/cycler/BinderRowSpec;

    .line 229
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$$inlined$nohoRow$2;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$$inlined$nohoRow$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 225
    invoke-direct {p1, v1, p2}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 223
    invoke-virtual {p1, v0}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 228
    check-cast p1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 224
    invoke-virtual {p0, p1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public static final synthetic nohoRow(Lcom/squareup/cycler/Recycler$Config;Lcom/squareup/noho/CheckType;Lkotlin/jvm/functions/Function3;)V
    .locals 2
    .annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;>(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lcom/squareup/noho/CheckType;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-TS;-",
            "Lcom/squareup/noho/NohoCheckableRow;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$nohoRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bindBlock"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    new-instance v0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$6;

    invoke-direct {v0, p1}, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$6;-><init>(Lcom/squareup/noho/CheckType;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 237
    new-instance p1, Lcom/squareup/cycler/BinderRowSpec;

    .line 241
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$$inlined$row$6;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$$inlined$row$6;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 237
    invoke-direct {p1, v1, v0}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 235
    invoke-virtual {p1, p2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 240
    check-cast p1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 236
    invoke-virtual {p0, p1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public static final synthetic nohoRow(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;>(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cycler/BinderRowSpec<",
            "TI;TS;",
            "Lcom/squareup/noho/NohoRow;",
            ">;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$nohoRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "specBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    sget-object v0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$1;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 175
    new-instance v1, Lcom/squareup/cycler/BinderRowSpec;

    .line 179
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v2, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$$inlined$row$1;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$$inlined$row$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 175
    invoke-direct {v1, v2, v0}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 178
    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 174
    invoke-virtual {p0, v1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public static final synthetic nohoRow(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function2;)V
    .locals 3
    .annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;>(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/jvm/functions/Function2<",
            "-TS;-",
            "Lcom/squareup/noho/NohoRow;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$nohoRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bindBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$2;

    invoke-direct {v0, p1}, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$2;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    .line 184
    sget-object p1, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$3;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$3;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    .line 188
    new-instance v1, Lcom/squareup/cycler/BinderRowSpec;

    .line 192
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v2, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$$inlined$nohoRow$1;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$$inlined$nohoRow$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 188
    invoke-direct {v1, v2, p1}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 186
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 191
    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 187
    invoke-virtual {p0, v1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method

.method public static final synthetic nohoRow(Lcom/squareup/cycler/Recycler$Config;Lkotlin/jvm/functions/Function3;)V
    .locals 3
    .annotation runtime Lcom/squareup/cycler/RecyclerApiMarker;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "S::TI;>(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "TI;>;",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-TS;-",
            "Lcom/squareup/noho/NohoRow;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$nohoRow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bindBlock"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    sget-object v0, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$3;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$3;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 200
    new-instance v1, Lcom/squareup/cycler/BinderRowSpec;

    .line 204
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v2, Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$$inlined$row$3;->INSTANCE:Lcom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$$inlined$row$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 200
    invoke-direct {v1, v2, v0}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 198
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 203
    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 199
    invoke-virtual {p0, v1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    return-void
.end method
