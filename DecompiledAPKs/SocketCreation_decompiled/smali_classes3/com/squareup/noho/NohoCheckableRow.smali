.class public final Lcom/squareup/noho/NohoCheckableRow;
.super Lcom/squareup/noho/NohoRow;
.source "NohoCheckableRow.kt"

# interfaces
.implements Lcom/squareup/noho/ListenableCheckable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/noho/NohoRow;",
        "Lcom/squareup/noho/ListenableCheckable<",
        "Lcom/squareup/noho/NohoCheckableRow;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoCheckableRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoCheckableRow.kt\ncom/squareup/noho/NohoCheckableRow\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 Delegates.kt\nkotlin/properties/Delegates\n+ 4 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,205:1\n1261#2:206\n1103#2,7:210\n33#3,3:207\n37#4,6:217\n*E\n*S KotlinDebug\n*F\n+ 1 NohoCheckableRow.kt\ncom/squareup/noho/NohoCheckableRow\n*L\n24#1:206\n65#1,7:210\n40#1,3:207\n66#1,6:217\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000]\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0015\n\u0002\u0008\u0008*\u0001\r\u0018\u00002\u00020\u00012\u0008\u0012\u0004\u0012\u00020\u00000\u0002B%\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0003\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0008\u0010)\u001a\u00020\u000bH\u0016J\u0010\u0010*\u001a\u00020+2\u0006\u0010,\u001a\u00020\u0008H\u0014J\u0010\u0010-\u001a\u00020\u00182\u0006\u0010.\u001a\u00020\u000bH\u0016J\u0010\u0010/\u001a\u00020\u00182\u0006\u00100\u001a\u00020\u000bH\u0016J\u0008\u00101\u001a\u00020\u0018H\u0016J\u0006\u00102\u001a\u00020\u0018R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000eR+\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u000b8F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0014\u0010\u0015\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R:\u0010\u0016\u001a\"\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u0018\u0018\u00010\u0017j\n\u0012\u0004\u0012\u00020\u0000\u0018\u0001`\u0019X\u0096\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u001a\u0010\u001b\"\u0004\u0008\u001c\u0010\u001dR\u001a\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00180\u001fX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\"\u001a\u00020!2\u0006\u0010 \u001a\u00020!@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008#\u0010$\"\u0004\u0008%\u0010&R\u0010\u0010\'\u001a\u0004\u0018\u00010(X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/noho/NohoCheckableRow;",
        "Lcom/squareup/noho/NohoRow;",
        "Lcom/squareup/noho/ListenableCheckable;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "_checked",
        "",
        "checkableArrageable",
        "com/squareup/noho/NohoCheckableRow$checkableArrageable$1",
        "Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;",
        "<set-?>",
        "isCheckableEnabled",
        "()Z",
        "setCheckableEnabled",
        "(Z)V",
        "isCheckableEnabled$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "onCheckedChange",
        "Lkotlin/Function2;",
        "",
        "Lcom/squareup/noho/OnCheckedChange;",
        "getOnCheckedChange",
        "()Lkotlin/jvm/functions/Function2;",
        "setOnCheckedChange",
        "(Lkotlin/jvm/functions/Function2;)V",
        "setChildCheckedState",
        "Lkotlin/Function1;",
        "value",
        "Lcom/squareup/noho/CheckType;",
        "type",
        "getType",
        "()Lcom/squareup/noho/CheckType;",
        "setType",
        "(Lcom/squareup/noho/CheckType;)V",
        "view",
        "Landroid/view/View;",
        "isChecked",
        "onCreateDrawableState",
        "",
        "extraSpace",
        "setChecked",
        "checked",
        "setEnabled",
        "enabled",
        "toggle",
        "updateView",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private _checked:Z

.field private final checkableArrageable:Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;

.field private final isCheckableEnabled$delegate:Lkotlin/properties/ReadWriteProperty;

.field private onCheckedChange:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/noho/NohoCheckableRow;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private setChildCheckedState:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private type:Lcom/squareup/noho/CheckType;

.field private view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/noho/NohoCheckableRow;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "isCheckableEnabled"

    const-string v4, "isCheckableEnabled()Z"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/noho/NohoCheckableRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    sget-object v0, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v0, p1}, Lcom/squareup/util/VectorFriendly;->ensureContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 24
    invoke-direct {p0, v0, p2, p3}, Lcom/squareup/noho/NohoRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    sget-object v0, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 207
    new-instance v2, Lcom/squareup/noho/NohoCheckableRow$$special$$inlined$observable$1;

    invoke-direct {v2, v1, v1, p0}, Lcom/squareup/noho/NohoCheckableRow$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/noho/NohoCheckableRow;)V

    check-cast v2, Lkotlin/properties/ReadWriteProperty;

    .line 209
    iput-object v2, p0, Lcom/squareup/noho/NohoCheckableRow;->isCheckableEnabled$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 44
    sget-object v1, Lcom/squareup/noho/CheckType$RADIO;->INSTANCE:Lcom/squareup/noho/CheckType$RADIO;

    check-cast v1, Lcom/squareup/noho/CheckType;

    iput-object v1, p0, Lcom/squareup/noho/NohoCheckableRow;->type:Lcom/squareup/noho/CheckType;

    .line 53
    new-instance v1, Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;

    invoke-direct {v1, p1}, Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/squareup/noho/NohoCheckableRow;->checkableArrageable:Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;

    .line 64
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableRow;->getRightArrangeableProviders()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/noho/NohoCheckableRow;->checkableArrageable:Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;

    invoke-interface {v1, v0, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 210
    new-instance v0, Lcom/squareup/noho/NohoCheckableRow$$special$$inlined$onClickDebounced$1;

    invoke-direct {v0}, Lcom/squareup/noho/NohoCheckableRow$$special$$inlined$onClickDebounced$1;-><init>()V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoCheckableRow:[I

    const-string v1, "R.styleable.NohoCheckableRow"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    sget v1, Lcom/squareup/noho/R$style;->Widget_Noho_CheckableRow:I

    .line 217
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string p2, "a"

    .line 219
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    sget p2, Lcom/squareup/noho/R$styleable;->NohoCheckableRow_sqCheckType:I

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p2

    .line 73
    invoke-static {}, Lcom/squareup/noho/NohoCheckableRowKt;->access$getCHECK_TYPES_FROM_XML$p()[Lcom/squareup/noho/CheckType;

    move-result-object p3

    aget-object p2, p3, p2

    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoCheckableRow;->setType(Lcom/squareup/noho/CheckType;)V

    .line 74
    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 22
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    .line 23
    sget p3, Lcom/squareup/noho/R$attr;->sqRowStyle:I

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoCheckableRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getView$p(Lcom/squareup/noho/NohoCheckableRow;)Landroid/view/View;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/noho/NohoCheckableRow;->view:Landroid/view/View;

    return-object p0
.end method

.method public static final synthetic access$mergeDrawableStates$s2666181([I[I)[I
    .locals 0

    .line 19
    invoke-static {p0, p1}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setView$p(Lcom/squareup/noho/NohoCheckableRow;Landroid/view/View;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/squareup/noho/NohoCheckableRow;->view:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public getOnCheckedChange()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "Lcom/squareup/noho/NohoCheckableRow;",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getType()Lcom/squareup/noho/CheckType;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableRow;->type:Lcom/squareup/noho/CheckType;

    return-object v0
.end method

.method public final isCheckableEnabled()Z
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableRow;->isCheckableEnabled$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoCheckableRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isChecked()Z
    .locals 1

    .line 77
    iget-boolean v0, p0, Lcom/squareup/noho/NohoCheckableRow;->_checked:Z

    return v0
.end method

.method public onCheckedChange(Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/noho/NohoCheckableRow;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "callback"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-static {p0, p1}, Lcom/squareup/noho/ListenableCheckable$DefaultImpls;->onCheckedChange(Lcom/squareup/noho/ListenableCheckable;Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 1

    add-int/lit8 p1, p1, 0x1

    .line 95
    invoke-super {p0, p1}, Lcom/squareup/noho/NohoRow;->onCreateDrawableState(I)[I

    move-result-object p1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableRow;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-static {}, Lcom/squareup/noho/NohoCheckableRowKt;->getCHECKED_STATE_SET()[I

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->access$mergeDrawableStates$s2666181([I[I)[I

    :cond_0
    const-string v0, "super.onCreateDrawableSt\u2026ET)\n          }\n        }"

    .line 96
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final setCheckableEnabled(Z)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableRow;->isCheckableEnabled$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/noho/NohoCheckableRow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .line 86
    iget-boolean v0, p0, Lcom/squareup/noho/NohoCheckableRow;->_checked:Z

    if-eq v0, p1, :cond_1

    .line 87
    iput-boolean p1, p0, Lcom/squareup/noho/NohoCheckableRow;->_checked:Z

    .line 88
    iget-object p1, p0, Lcom/squareup/noho/NohoCheckableRow;->setChildCheckedState:Lkotlin/jvm/functions/Function1;

    if-nez p1, :cond_0

    const-string v0, "setChildCheckedState"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lcom/squareup/noho/NohoCheckableRow;->_checked:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableRow;->refreshDrawableState()V

    .line 90
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableRow;->getOnCheckedChange()Lkotlin/jvm/functions/Function2;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableRow;->isChecked()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p1, p0, v0}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;

    :cond_1
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0

    .line 104
    invoke-super {p0, p1}, Lcom/squareup/noho/NohoRow;->setEnabled(Z)V

    .line 106
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setCheckableEnabled(Z)V

    return-void
.end method

.method public setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/noho/NohoCheckableRow;",
            "-",
            "Ljava/lang/Boolean;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 27
    iput-object p1, p0, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public final setType(Lcom/squareup/noho/CheckType;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableRow;->type:Lcom/squareup/noho/CheckType;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableRow;->view:Landroid/view/View;

    if-nez v0, :cond_1

    .line 47
    :cond_0
    iput-object p1, p0, Lcom/squareup/noho/NohoCheckableRow;->type:Lcom/squareup/noho/CheckType;

    .line 48
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableRow;->updateView()V

    :cond_1
    return-void
.end method

.method public toggle()V
    .locals 2

    .line 80
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableRow;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/squareup/noho/NohoCheckableRow;->type:Lcom/squareup/noho/CheckType;

    invoke-virtual {v1}, Lcom/squareup/noho/CheckType;->getCanDeselect()Z

    move-result v1

    or-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableRow;->isChecked()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public final updateView()V
    .locals 4

    .line 114
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableRow;->view:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoCheckableRow;->removeView(Landroid/view/View;)V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableRow;->type:Lcom/squareup/noho/CheckType;

    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableRow;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lcom/squareup/noho/NohoCheckableRow$updateView$result$1;

    invoke-direct {v2, p0}, Lcom/squareup/noho/NohoCheckableRow$updateView$result$1;-><init>(Lcom/squareup/noho/NohoCheckableRow;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/CheckType;->createView(Landroid/content/Context;Lkotlin/jvm/functions/Function1;)Lkotlin/Pair;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 117
    new-instance v2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v3, v3}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v2, 0x0

    .line 118
    invoke-virtual {v1, v2}, Landroid/view/View;->setFocusable(Z)V

    .line 119
    iget-boolean v3, p0, Lcom/squareup/noho/NohoCheckableRow;->_checked:Z

    invoke-virtual {p0, v3}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 120
    iget-object v3, p0, Lcom/squareup/noho/NohoCheckableRow;->checkableArrageable:Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;

    invoke-virtual {v3}, Lcom/squareup/noho/NohoCheckableRow$checkableArrageable$1;->getId()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setId(I)V

    .line 121
    invoke-virtual {v1, v2}, Landroid/view/View;->setSaveEnabled(Z)V

    .line 122
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableRow;->isCheckableEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 123
    invoke-virtual {p0, v1}, Lcom/squareup/noho/NohoCheckableRow;->addView(Landroid/view/View;)V

    .line 116
    iput-object v1, p0, Lcom/squareup/noho/NohoCheckableRow;->view:Landroid/view/View;

    .line 125
    invoke-virtual {v0}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/jvm/functions/Function1;

    iput-object v0, p0, Lcom/squareup/noho/NohoCheckableRow;->setChildCheckedState:Lkotlin/jvm/functions/Function1;

    .line 127
    iget-object v0, p0, Lcom/squareup/noho/NohoCheckableRow;->setChildCheckedState:Lkotlin/jvm/functions/Function1;

    if-nez v0, :cond_1

    const-string v1, "setChildCheckedState"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/squareup/noho/NohoCheckableRow;->isChecked()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
