.class final Lcom/squareup/noho/NohoRow$IconFeature;
.super Lcom/squareup/noho/NohoRow$FeatureProperty;
.source "NohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "IconFeature"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/noho/NohoRow$FeatureProperty<",
        "Lcom/squareup/noho/NohoRow$Icon;",
        "Landroidx/appcompat/widget/AppCompatImageView;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRow$IconFeature\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,1059:1\n60#2,6:1060\n*E\n*S KotlinDebug\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRow$IconFeature\n*L\n864#1,6:1060\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u00c2\u0002\u0018\u00002\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0001\u0010\u0008\u001a\u00020\tH\u0014J&\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0006\u001a\u00020\u00072\u0014\u0010\u000c\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0004\u0012\u00020\u00030\rH\u0014J\u0012\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0002H\u0014J6\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\u00072\u0014\u0010\u000c\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0002\u0012\u0004\u0012\u00020\u00030\rH\u0014\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/noho/NohoRow$IconFeature;",
        "Lcom/squareup/noho/NohoRow$FeatureProperty;",
        "Lcom/squareup/noho/NohoRow$Icon;",
        "Landroidx/appcompat/widget/AppCompatImageView;",
        "()V",
        "create",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "styleId",
        "",
        "doSetValue",
        "",
        "instance",
        "Lcom/squareup/noho/NohoRow$FeatureInstance;",
        "shouldShowFor",
        "",
        "value",
        "updateStyleId",
        "view",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/noho/NohoRow$IconFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 832
    new-instance v0, Lcom/squareup/noho/NohoRow$IconFeature;

    invoke-direct {v0}, Lcom/squareup/noho/NohoRow$IconFeature;-><init>()V

    sput-object v0, Lcom/squareup/noho/NohoRow$IconFeature;->INSTANCE:Lcom/squareup/noho/NohoRow$IconFeature;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    .line 834
    sget v1, Lcom/squareup/noho/R$id;->icon:I

    .line 835
    sget v2, Lcom/squareup/noho/R$dimen;->noho_row_gap_size:I

    .line 836
    sget v3, Lcom/squareup/noho/R$attr;->sqIconStyle:I

    .line 837
    sget v4, Lcom/squareup/noho/R$styleable;->NohoRow_sqIconStyle:I

    .line 838
    sget v5, Lcom/squareup/noho/R$style;->Widget_Noho_Row_Icon:I

    const/4 v6, 0x0

    move-object v0, p0

    .line 833
    invoke-direct/range {v0 .. v6}, Lcom/squareup/noho/NohoRow$FeatureProperty;-><init>(IIIIILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic create(Lcom/squareup/noho/NohoRow;I)Landroid/view/View;
    .locals 0

    .line 832
    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$IconFeature;->create(Lcom/squareup/noho/NohoRow;I)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    return-object p1
.end method

.method protected create(Lcom/squareup/noho/NohoRow;I)Landroidx/appcompat/widget/AppCompatImageView;
    .locals 2

    const-string p2, "row"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 843
    new-instance p2, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;)V

    .line 844
    new-instance v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 845
    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getMinHeight()I

    move-result p1

    const/4 v1, 0x0

    .line 844
    invoke-direct {v0, p1, v1}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    check-cast v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p2, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object p2
.end method

.method protected doSetValue(Lcom/squareup/noho/NohoRow;Lcom/squareup/noho/NohoRow$FeatureInstance;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoRow;",
            "Lcom/squareup/noho/NohoRow$FeatureInstance<",
            "Lcom/squareup/noho/NohoRow$Icon;",
            "Landroidx/appcompat/widget/AppCompatImageView;",
            ">;)V"
        }
    .end annotation

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "instance"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 861
    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getView()Landroid/view/View;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast p1, Landroidx/appcompat/widget/AppCompatImageView;

    .line 862
    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v0, Lcom/squareup/noho/NohoRow$Icon;

    .line 864
    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "view.context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getStyleId()I

    move-result p2

    invoke-static {v1, p2}, Lcom/squareup/util/StyledAttributesKt;->styleAsTheme(Landroid/content/Context;I)Landroid/content/res/Resources$Theme;

    move-result-object p2

    sget-object v1, Lcom/squareup/noho/R$styleable;->NohoRow_Icon:[I

    const-string v3, "R.styleable.NohoRow_Icon"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1060
    invoke-virtual {p2, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object p2

    :try_start_0
    const-string v1, "a"

    .line 1062
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 866
    instance-of v1, v0, Lcom/squareup/noho/NohoRow$Icon$BoxedIcon;

    if-eqz v1, :cond_2

    .line 867
    move-object v1, v0

    check-cast v1, Lcom/squareup/noho/NohoRow$Icon$BoxedIcon;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoRow$Icon$BoxedIcon;->getBackground()I

    move-result v1

    invoke-virtual {p1, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setBackgroundColor(I)V

    .line 868
    check-cast v0, Lcom/squareup/noho/NohoRow$Icon$BoxedIcon;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow$Icon$BoxedIcon;->getId()I

    move-result v0

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 869
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 870
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setSupportImageTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 872
    sget v0, Lcom/squareup/noho/R$styleable;->NohoRow_Icon_android_colorForegroundInverse:I

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setSupportImageTintList(Landroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 874
    :cond_2
    instance-of v1, v0, Lcom/squareup/noho/NohoRow$Icon$PictureIcon;

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    .line 875
    move-object v3, v1

    check-cast v3, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v3}, Landroidx/appcompat/widget/AppCompatImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 876
    check-cast v0, Lcom/squareup/noho/NohoRow$Icon$PictureIcon;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow$Icon$PictureIcon;->getProvider()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    invoke-virtual {p1}, Landroidx/appcompat/widget/AppCompatImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 877
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 878
    move-object v0, v1

    check-cast v0, Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setSupportImageTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 879
    check-cast v1, Landroid/content/res/ColorStateList;

    invoke-virtual {p1, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setSupportImageTintList(Landroid/content/res/ColorStateList;)V

    .line 882
    :cond_3
    :goto_0
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1064
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method protected shouldShowFor(Lcom/squareup/noho/NohoRow$Icon;)Z
    .locals 0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic shouldShowFor(Ljava/lang/Object;)Z
    .locals 0

    .line 832
    check-cast p1, Lcom/squareup/noho/NohoRow$Icon;

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow$IconFeature;->shouldShowFor(Lcom/squareup/noho/NohoRow$Icon;)Z

    move-result p1

    return p1
.end method

.method public bridge synthetic updateStyleId(Landroid/view/View;ILcom/squareup/noho/NohoRow;Lcom/squareup/noho/NohoRow$FeatureInstance;)V
    .locals 0

    .line 832
    check-cast p1, Landroidx/appcompat/widget/AppCompatImageView;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/noho/NohoRow$IconFeature;->updateStyleId(Landroidx/appcompat/widget/AppCompatImageView;ILcom/squareup/noho/NohoRow;Lcom/squareup/noho/NohoRow$FeatureInstance;)V

    return-void
.end method

.method protected updateStyleId(Landroidx/appcompat/widget/AppCompatImageView;ILcom/squareup/noho/NohoRow;Lcom/squareup/noho/NohoRow$FeatureInstance;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/appcompat/widget/AppCompatImageView;",
            "I",
            "Lcom/squareup/noho/NohoRow;",
            "Lcom/squareup/noho/NohoRow$FeatureInstance<",
            "Lcom/squareup/noho/NohoRow$Icon;",
            "Landroidx/appcompat/widget/AppCompatImageView;",
            ">;)V"
        }
    .end annotation

    const-string/jumbo p2, "view"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "row"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "instance"

    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 857
    invoke-virtual {p0, p3, p4}, Lcom/squareup/noho/NohoRow$IconFeature;->doSetValue(Lcom/squareup/noho/NohoRow;Lcom/squareup/noho/NohoRow$FeatureInstance;)V

    return-void
.end method
