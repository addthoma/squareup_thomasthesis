.class public final Lcom/squareup/noho/StateListVectorDrawable;
.super Landroid/graphics/drawable/StateListDrawable;
.source "StateListVectorDrawable.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/StateListVectorDrawable$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \r2\u00020\u0001:\u0001\rB\u0005\u00a2\u0006\u0002\u0010\u0002J.\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u000c\u0010\u000b\u001a\u0008\u0018\u00010\u000cR\u00020\u0006H\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/noho/StateListVectorDrawable;",
        "Landroid/graphics/drawable/StateListDrawable;",
        "()V",
        "inflate",
        "",
        "resources",
        "Landroid/content/res/Resources;",
        "parser",
        "Lorg/xmlpull/v1/XmlPullParser;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "theme",
        "Landroid/content/res/Resources$Theme;",
        "Companion",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/noho/StateListVectorDrawable$Companion;

.field private static final DEFAULT_STYLE:Ljava/lang/String; = "defaultStyle"

.field private static final DRAWABLE:Ljava/lang/String; = "drawable"

.field private static final ITEM:Ljava/lang/String; = "item"

.field private static final ITEM_ATTRIBUTES:[I

.field private static final ITEM_DEFAULT_STYLE:I

.field private static final ITEM_DRAWABLE:I

.field private static final ITEM_STYLE:I

.field private static final NO_ID:I = 0x0

.field public static final SELECTOR:Ljava/lang/String; = "selector"

.field private static final STYLE:Ljava/lang/String; = "vectorStyle"

.field public static final VECTOR:Ljava/lang/String; = "vector"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/noho/StateListVectorDrawable$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/noho/StateListVectorDrawable$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/noho/StateListVectorDrawable;->Companion:Lcom/squareup/noho/StateListVectorDrawable$Companion;

    .line 64
    sget-object v0, Lcom/squareup/noho/R$styleable;->StateListVectorDrawableItem:[I

    sput-object v0, Lcom/squareup/noho/StateListVectorDrawable;->ITEM_ATTRIBUTES:[I

    .line 65
    sget v0, Lcom/squareup/noho/R$styleable;->StateListVectorDrawableItem_vectorStyle:I

    sput v0, Lcom/squareup/noho/StateListVectorDrawable;->ITEM_STYLE:I

    .line 66
    sget v0, Lcom/squareup/noho/R$styleable;->StateListVectorDrawableItem_defaultStyle:I

    sput v0, Lcom/squareup/noho/StateListVectorDrawable;->ITEM_DEFAULT_STYLE:I

    .line 67
    sget v0, Lcom/squareup/noho/R$styleable;->StateListVectorDrawableItem_android_drawable:I

    sput v0, Lcom/squareup/noho/StateListVectorDrawable;->ITEM_DRAWABLE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    return-void
.end method

.method public static final synthetic access$getITEM_ATTRIBUTES$cp()[I
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/noho/StateListVectorDrawable;->ITEM_ATTRIBUTES:[I

    return-object v0
.end method

.method public static final synthetic access$getITEM_DEFAULT_STYLE$cp()I
    .locals 1

    .line 41
    sget v0, Lcom/squareup/noho/StateListVectorDrawable;->ITEM_DEFAULT_STYLE:I

    return v0
.end method

.method public static final synthetic access$getITEM_DRAWABLE$cp()I
    .locals 1

    .line 41
    sget v0, Lcom/squareup/noho/StateListVectorDrawable;->ITEM_DRAWABLE:I

    return v0
.end method

.method public static final synthetic access$getITEM_STYLE$cp()I
    .locals 1

    .line 41
    sget v0, Lcom/squareup/noho/StateListVectorDrawable;->ITEM_STYLE:I

    return v0
.end method

.method public static final create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/noho/StateListVectorDrawable;->Companion:Lcom/squareup/noho/StateListVectorDrawable$Companion;

    invoke-virtual {v0, p0, p1, p2}, Lcom/squareup/noho/StateListVectorDrawable$Companion;->create(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
    .locals 7

    const-string v0, "resources"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parser"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget-object v1, Lcom/squareup/noho/StateListVectorDrawable;->Companion:Lcom/squareup/noho/StateListVectorDrawable$Companion;

    move-object v2, p0

    check-cast v2, Landroid/graphics/drawable/StateListDrawable;

    if-eqz p4, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object p4

    const-string v0, "resources.newTheme()"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    move-object v6, p4

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v1 .. v6}, Lcom/squareup/noho/StateListVectorDrawable$Companion;->inflate$noho_release(Landroid/graphics/drawable/StateListDrawable;Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    return-void
.end method
