.class public final Lcom/squareup/noho/NohoNestedScrollView;
.super Landroidx/core/widget/NestedScrollView;
.source "NohoNestedScrollView.kt"

# interfaces
.implements Lcom/squareup/noho/utils/ScrollViewUtils$NeedsMeasurementFix;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoNestedScrollView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoNestedScrollView.kt\ncom/squareup/noho/NohoNestedScrollView\n*L\n1#1,60:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0008\u0018\u00002\u00020\u00012\u00020\u0002B#\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0018\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00082\u0006\u0010\u000f\u001a\u00020\u0008H\u0014J(\u0010\u0010\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u00082\u0006\u0010\u0012\u001a\u00020\u00082\u0006\u0010\u0013\u001a\u00020\u00082\u0006\u0010\u0014\u001a\u00020\u0008H\u0014R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/noho/NohoNestedScrollView;",
        "Landroidx/core/widget/NestedScrollView;",
        "Lcom/squareup/noho/utils/ScrollViewUtils$NeedsMeasurementFix;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "scrollViewUtils",
        "Lcom/squareup/noho/utils/ScrollViewUtils;",
        "onMeasure",
        "",
        "widthMeasureSpec",
        "heightMeasureSpec",
        "onSizeChanged",
        "w",
        "h",
        "oldw",
        "oldh",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final scrollViewUtils:Lcom/squareup/noho/utils/ScrollViewUtils;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoNestedScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0, p1, p2, p3}, Landroidx/core/widget/NestedScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    new-instance v0, Lcom/squareup/noho/utils/ScrollViewUtils;

    invoke-direct {v0}, Lcom/squareup/noho/utils/ScrollViewUtils;-><init>()V

    iput-object v0, p0, Lcom/squareup/noho/NohoNestedScrollView;->scrollViewUtils:Lcom/squareup/noho/utils/ScrollViewUtils;

    .line 23
    iget-object v0, p0, Lcom/squareup/noho/NohoNestedScrollView;->scrollViewUtils:Lcom/squareup/noho/utils/ScrollViewUtils;

    move-object v1, p0

    check-cast v1, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/squareup/noho/utils/ScrollViewUtils;->setPadding(Landroid/widget/FrameLayout;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 18
    sget p3, Lcom/squareup/noho/R$attr;->nohoScrollViewStyle:I

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoNestedScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 7

    .line 30
    invoke-super {p0, p1, p2}, Landroidx/core/widget/NestedScrollView;->onMeasure(II)V

    .line 32
    invoke-static {p0, p2}, Lcom/squareup/noho/utils/ScrollViewUtilsKt;->needsAdjustment(Lcom/squareup/noho/utils/ScrollViewUtils$NeedsMeasurementFix;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 33
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoNestedScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, v0

    move v3, p1

    move v5, p2

    .line 36
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/noho/NohoNestedScrollView;->measureChildWithMargins(Landroid/view/View;IIII)V

    const-string p2, "child"

    .line 39
    invoke-static {v0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p0, v0, p1}, Lcom/squareup/noho/utils/ScrollViewUtilsKt;->onMeasureAdjustment(Landroid/widget/FrameLayout;Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .line 51
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNestedScrollView;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 52
    move-object v1, p0

    check-cast v1, Landroid/view/View;

    invoke-static {v1, v0}, Lcom/squareup/util/Views;->isDescendant(Landroid/view/View;Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/squareup/noho/NohoNestedScrollView;->clearFocus()V

    .line 57
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroidx/core/widget/NestedScrollView;->onSizeChanged(IIII)V

    return-void
.end method
