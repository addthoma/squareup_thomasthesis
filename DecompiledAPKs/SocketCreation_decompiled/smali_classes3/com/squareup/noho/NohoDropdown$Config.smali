.class public final Lcom/squareup/noho/NohoDropdown$Config;
.super Ljava/lang/Object;
.source "NohoDropdown.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoDropdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoDropdown.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoDropdown.kt\ncom/squareup/noho/NohoDropdown$Config\n*L\n1#1,226:1\n212#1,7:227\n193#1:234\n212#1,7:235\n194#1:242\n212#1,7:243\n206#1,13:250\n207#1,12:263\n211#1,8:275\n211#1,8:283\n*E\n*S KotlinDebug\n*F\n+ 1 NohoDropdown.kt\ncom/squareup/noho/NohoDropdown$Config\n*L\n193#1,7:227\n206#1,7:243\n175#1,8:275\n178#1,8:283\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0004\u0018\u0000*\u0004\u0008\u0000\u0010\u00012\u00020\u0002B\u001b\u0008\u0000\u0012\u0008\u0008\u0001\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0016\u001a\u00020\u0017H\u0001Jg\u0010\u0018\u001a$\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000ej\u0008\u0012\u0004\u0012\u00028\u0000`\u00112\u0008\u0008\u0001\u0010\u0019\u001a\u00020\u000420\u0008\u0006\u0010\u001a\u001a*\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u001c0\u001bj\u0008\u0012\u0004\u0012\u00028\u0000`\u001dH\u0081\u0008J0\u0010\u001e\u001a\u00020\u001c2(\u0010\u001a\u001a$\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000ej\u0008\u0012\u0004\u0012\u00028\u0000`\u0011JE\u0010\u001e\u001a\u00020\u001c2\u0008\u0008\u0003\u0010\u0019\u001a\u00020\u000420\u0008\u0006\u0010\u001a\u001a*\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u001c0\u001bj\u0008\u0012\u0004\u0012\u00028\u0000`\u001dH\u0086\u0008J0\u0010\u001f\u001a\u00020\u001c2(\u0010\u001a\u001a$\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000ej\u0008\u0012\u0004\u0012\u00028\u0000`\u0011JE\u0010\u001f\u001a\u00020\u001c2\u0008\u0008\u0003\u0010\u0019\u001a\u00020\u000420\u0008\u0006\u0010\u001a\u001a*\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u001c0\u001bj\u0008\u0012\u0004\u0012\u00028\u0000`\u001dH\u0086\u0008R \u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR0\u0010\r\u001a$\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000ej\u0008\u0012\u0004\u0012\u00028\u0000`\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0013R0\u0010\u0015\u001a$\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000ej\u0008\u0012\u0004\u0012\u00028\u0000`\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/noho/NohoDropdown$Config;",
        "T",
        "",
        "selectedViewLayoutId",
        "",
        "menuViewLayoutId",
        "(Lcom/squareup/noho/NohoDropdown;II)V",
        "data",
        "",
        "getData",
        "()Ljava/util/List;",
        "setData",
        "(Ljava/util/List;)V",
        "dropdownViewsBlock",
        "Lkotlin/Function3;",
        "Landroid/content/Context;",
        "Landroid/view/View;",
        "Lcom/squareup/noho/CreateViewBlock;",
        "getMenuViewLayoutId",
        "()I",
        "getSelectedViewLayoutId",
        "selectedViewsBlock",
        "createAdapter",
        "Landroid/widget/SpinnerAdapter;",
        "createLayoutAndConfigureBlock",
        "layoutId",
        "block",
        "Lkotlin/Function4;",
        "",
        "Lcom/squareup/noho/ConfigureViewBlock;",
        "dropdownViews",
        "selectedView",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+TT;>;"
        }
    .end annotation
.end field

.field private dropdownViewsBlock:Lkotlin/jvm/functions/Function3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-TT;-",
            "Landroid/content/Context;",
            "+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final menuViewLayoutId:I

.field private final selectedViewLayoutId:I

.field private selectedViewsBlock:Lkotlin/jvm/functions/Function3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-TT;-",
            "Landroid/content/Context;",
            "+",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/noho/NohoDropdown;


# direct methods
.method public constructor <init>(Lcom/squareup/noho/NohoDropdown;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 168
    iput-object p1, p0, Lcom/squareup/noho/NohoDropdown$Config;->this$0:Lcom/squareup/noho/NohoDropdown;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/squareup/noho/NohoDropdown$Config;->selectedViewLayoutId:I

    iput p3, p0, Lcom/squareup/noho/NohoDropdown$Config;->menuViewLayoutId:I

    .line 175
    iget p1, p0, Lcom/squareup/noho/NohoDropdown$Config;->selectedViewLayoutId:I

    .line 276
    new-instance p2, Lcom/squareup/noho/NohoDropdown$Config$$special$$inlined$createLayoutAndConfigureBlock$1;

    invoke-direct {p2, p1}, Lcom/squareup/noho/NohoDropdown$Config$$special$$inlined$createLayoutAndConfigureBlock$1;-><init>(I)V

    check-cast p2, Lkotlin/jvm/functions/Function3;

    .line 282
    iput-object p2, p0, Lcom/squareup/noho/NohoDropdown$Config;->selectedViewsBlock:Lkotlin/jvm/functions/Function3;

    .line 178
    iget p1, p0, Lcom/squareup/noho/NohoDropdown$Config;->menuViewLayoutId:I

    .line 284
    new-instance p2, Lcom/squareup/noho/NohoDropdown$Config$$special$$inlined$createLayoutAndConfigureBlock$2;

    invoke-direct {p2, p1}, Lcom/squareup/noho/NohoDropdown$Config$$special$$inlined$createLayoutAndConfigureBlock$2;-><init>(I)V

    check-cast p2, Lkotlin/jvm/functions/Function3;

    .line 290
    iput-object p2, p0, Lcom/squareup/noho/NohoDropdown$Config;->dropdownViewsBlock:Lkotlin/jvm/functions/Function3;

    return-void
.end method

.method public static synthetic createLayoutAndConfigureBlock$default(Lcom/squareup/noho/NohoDropdown$Config;ILkotlin/jvm/functions/Function4;ILjava/lang/Object;)Lkotlin/jvm/functions/Function3;
    .locals 0

    and-int/lit8 p0, p3, 0x2

    if-eqz p0, :cond_0

    .line 211
    sget-object p0, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$1;->INSTANCE:Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$1;

    move-object p2, p0

    check-cast p2, Lkotlin/jvm/functions/Function4;

    :cond_0
    const-string p0, "block"

    invoke-static {p2, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 268
    new-instance p0, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$2;

    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$2;-><init>(ILkotlin/jvm/functions/Function4;)V

    check-cast p0, Lkotlin/jvm/functions/Function3;

    return-object p0
.end method

.method public static synthetic dropdownViews$default(Lcom/squareup/noho/NohoDropdown$Config;ILkotlin/jvm/functions/Function4;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/squareup/noho/NohoDropdown$Config;->getMenuViewLayoutId()I

    move-result p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    .line 204
    sget-object p2, Lcom/squareup/noho/NohoDropdown$Config$dropdownViews$1;->INSTANCE:Lcom/squareup/noho/NohoDropdown$Config$dropdownViews$1;

    check-cast p2, Lkotlin/jvm/functions/Function4;

    :cond_1
    const-string p3, "block"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    new-instance p3, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$2;

    invoke-direct {p3, p1, p2}, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$2;-><init>(ILkotlin/jvm/functions/Function4;)V

    check-cast p3, Lkotlin/jvm/functions/Function3;

    .line 250
    invoke-virtual {p0, p3}, Lcom/squareup/noho/NohoDropdown$Config;->dropdownViews(Lkotlin/jvm/functions/Function3;)V

    return-void
.end method

.method public static synthetic selectedView$default(Lcom/squareup/noho/NohoDropdown$Config;ILkotlin/jvm/functions/Function4;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    .line 190
    invoke-virtual {p0}, Lcom/squareup/noho/NohoDropdown$Config;->getSelectedViewLayoutId()I

    move-result p1

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    .line 191
    sget-object p2, Lcom/squareup/noho/NohoDropdown$Config$selectedView$1;->INSTANCE:Lcom/squareup/noho/NohoDropdown$Config$selectedView$1;

    check-cast p2, Lkotlin/jvm/functions/Function4;

    :cond_1
    const-string p3, "block"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 235
    new-instance p3, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$2;

    invoke-direct {p3, p1, p2}, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$2;-><init>(ILkotlin/jvm/functions/Function4;)V

    check-cast p3, Lkotlin/jvm/functions/Function3;

    .line 234
    invoke-virtual {p0, p3}, Lcom/squareup/noho/NohoDropdown$Config;->selectedView(Lkotlin/jvm/functions/Function3;)V

    return-void
.end method


# virtual methods
.method public final createAdapter()Landroid/widget/SpinnerAdapter;
    .locals 5

    .line 221
    new-instance v0, Lcom/squareup/noho/NohoDropdown$AndroidSpinnerAdapter;

    iget-object v1, p0, Lcom/squareup/noho/NohoDropdown$Config;->this$0:Lcom/squareup/noho/NohoDropdown;

    iget-object v2, p0, Lcom/squareup/noho/NohoDropdown$Config;->data:Ljava/util/List;

    if-nez v2, :cond_0

    const-string v3, "data"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v3, p0, Lcom/squareup/noho/NohoDropdown$Config;->selectedViewsBlock:Lkotlin/jvm/functions/Function3;

    iget-object v4, p0, Lcom/squareup/noho/NohoDropdown$Config;->dropdownViewsBlock:Lkotlin/jvm/functions/Function3;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/noho/NohoDropdown$AndroidSpinnerAdapter;-><init>(Lcom/squareup/noho/NohoDropdown;Ljava/util/List;Lkotlin/jvm/functions/Function3;Lkotlin/jvm/functions/Function3;)V

    check-cast v0, Landroid/widget/SpinnerAdapter;

    return-object v0
.end method

.method public final createLayoutAndConfigureBlock(ILkotlin/jvm/functions/Function4;)Lkotlin/jvm/functions/Function3;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lkotlin/jvm/functions/Function4<",
            "-",
            "Ljava/lang/Integer;",
            "-TT;-",
            "Landroid/view/View;",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)",
            "Lkotlin/jvm/functions/Function3<",
            "Ljava/lang/Integer;",
            "TT;",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    new-instance v0, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$2;

    invoke-direct {v0, p1, p2}, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$2;-><init>(ILkotlin/jvm/functions/Function4;)V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    return-object v0
.end method

.method public final dropdownViews(ILkotlin/jvm/functions/Function4;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lkotlin/jvm/functions/Function4<",
            "-",
            "Ljava/lang/Integer;",
            "-TT;-",
            "Landroid/view/View;",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    new-instance v0, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$2;

    invoke-direct {v0, p1, p2}, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$2;-><init>(ILkotlin/jvm/functions/Function4;)V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    .line 206
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoDropdown$Config;->dropdownViews(Lkotlin/jvm/functions/Function3;)V

    return-void
.end method

.method public final dropdownViews(Lkotlin/jvm/functions/Function3;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-TT;-",
            "Landroid/content/Context;",
            "+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 181
    iput-object p1, p0, Lcom/squareup/noho/NohoDropdown$Config;->dropdownViewsBlock:Lkotlin/jvm/functions/Function3;

    return-void
.end method

.method public final getData()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 172
    iget-object v0, p0, Lcom/squareup/noho/NohoDropdown$Config;->data:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v1, "data"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final getMenuViewLayoutId()I
    .locals 1

    .line 170
    iget v0, p0, Lcom/squareup/noho/NohoDropdown$Config;->menuViewLayoutId:I

    return v0
.end method

.method public final getSelectedViewLayoutId()I
    .locals 1

    .line 169
    iget v0, p0, Lcom/squareup/noho/NohoDropdown$Config;->selectedViewLayoutId:I

    return v0
.end method

.method public final selectedView(ILkotlin/jvm/functions/Function4;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lkotlin/jvm/functions/Function4<",
            "-",
            "Ljava/lang/Integer;",
            "-TT;-",
            "Landroid/view/View;",
            "-",
            "Landroid/view/View;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    new-instance v0, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$2;

    invoke-direct {v0, p1, p2}, Lcom/squareup/noho/NohoDropdown$Config$createLayoutAndConfigureBlock$2;-><init>(ILkotlin/jvm/functions/Function4;)V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    .line 193
    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoDropdown$Config;->selectedView(Lkotlin/jvm/functions/Function3;)V

    return-void
.end method

.method public final selectedView(Lkotlin/jvm/functions/Function3;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-TT;-",
            "Landroid/content/Context;",
            "+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 180
    iput-object p1, p0, Lcom/squareup/noho/NohoDropdown$Config;->selectedViewsBlock:Lkotlin/jvm/functions/Function3;

    return-void
.end method

.method public final setData(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TT;>;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    iput-object p1, p0, Lcom/squareup/noho/NohoDropdown$Config;->data:Ljava/util/List;

    return-void
.end method
