.class public Lcom/squareup/noho/EdgePainter;
.super Ljava/lang/Object;
.source "EdgePainter.java"


# instance fields
.field private drawInside:Z

.field private final edgePaint:Landroid/graphics/Paint;

.field private edgeWidth:I

.field private final hostView:Landroid/view/View;

.field private leftInset:I

.field private rightInset:I

.field private sidesToPaint:I


# direct methods
.method public constructor <init>(Landroid/view/View;II)V
    .locals 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 33
    iput v0, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    .line 36
    iput-object p1, p0, Lcom/squareup/noho/EdgePainter;->hostView:Landroid/view/View;

    .line 37
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    .line 38
    iget-object p1, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 39
    iput p2, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    const/4 p1, 0x1

    .line 40
    iput-boolean p1, p0, Lcom/squareup/noho/EdgePainter;->drawInside:Z

    .line 41
    iput v0, p0, Lcom/squareup/noho/EdgePainter;->rightInset:I

    .line 42
    iput v0, p0, Lcom/squareup/noho/EdgePainter;->leftInset:I

    return-void
.end method


# virtual methods
.method public addEdges(I)V
    .locals 1

    .line 101
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    or-int/2addr p1, v0

    iput p1, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    return-void
.end method

.method public clearEdges()V
    .locals 1

    const/4 v0, 0x0

    .line 105
    iput v0, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    return-void
.end method

.method public clearEmphasis()V
    .locals 2

    .line 93
    iget-object v0, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    return-void
.end method

.method public drawChildEdges(Landroid/graphics/Canvas;Landroid/view/View;)V
    .locals 13

    .line 169
    iget-object v0, p0, Lcom/squareup/noho/EdgePainter;->hostView:Landroid/view/View;

    if-eq p2, v0, :cond_a

    .line 173
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_9

    .line 176
    check-cast v0, Landroid/view/ViewGroup;

    .line 179
    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 183
    :cond_0
    invoke-static {p2, v0}, Lcom/squareup/noho/Views;->getLeftRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result v1

    .line 184
    invoke-static {p2, v0}, Lcom/squareup/noho/Views;->getTopRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result v2

    .line 185
    invoke-static {p2, v0}, Lcom/squareup/noho/Views;->getRightRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result v3

    .line 186
    invoke-static {p2, v0}, Lcom/squareup/noho/Views;->getBottomRelativeToHost(Landroid/view/View;Landroid/view/ViewGroup;)I

    move-result p2

    .line 188
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    const/4 v4, 0x1

    invoke-static {v0, v4}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 192
    iget-boolean v0, p0, Lcom/squareup/noho/EdgePainter;->drawInside:Z

    if-eqz v0, :cond_1

    .line 194
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    add-int/2addr v0, v1

    move v4, v0

    move v0, v1

    goto :goto_0

    .line 196
    :cond_1
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    sub-int v0, v1, v0

    move v4, v1

    :goto_0
    int-to-float v6, v0

    int-to-float v7, v2

    int-to-float v8, v4

    int-to-float v9, p2

    .line 199
    iget-object v10, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    move-object v5, p1

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 202
    :cond_2
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    const/4 v4, 0x2

    invoke-static {v0, v4}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 205
    iget-boolean v0, p0, Lcom/squareup/noho/EdgePainter;->drawInside:Z

    if-eqz v0, :cond_3

    .line 207
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    add-int/2addr v0, v2

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 209
    :cond_3
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    sub-int v0, v2, v0

    move v4, v2

    .line 212
    :goto_1
    iget v5, p0, Lcom/squareup/noho/EdgePainter;->leftInset:I

    add-int/2addr v5, v1

    int-to-float v7, v5

    int-to-float v8, v0

    iget v0, p0, Lcom/squareup/noho/EdgePainter;->rightInset:I

    sub-int v0, v3, v0

    int-to-float v9, v0

    int-to-float v10, v4

    iget-object v11, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    move-object v6, p1

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 216
    :cond_4
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    const/4 v4, 0x4

    invoke-static {v0, v4}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 219
    iget-boolean v0, p0, Lcom/squareup/noho/EdgePainter;->drawInside:Z

    if-eqz v0, :cond_5

    .line 220
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    sub-int v0, v3, v0

    move v4, v3

    goto :goto_2

    .line 224
    :cond_5
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    add-int/2addr v0, v3

    move v4, v0

    move v0, v3

    :goto_2
    int-to-float v6, v0

    int-to-float v7, v2

    int-to-float v8, v4

    int-to-float v9, p2

    .line 226
    iget-object v10, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    move-object v5, p1

    invoke-virtual/range {v5 .. v10}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 229
    :cond_6
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    const/16 v2, 0x8

    invoke-static {v0, v2}, Lcom/squareup/noho/Edges;->hasEdge(II)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 232
    iget-boolean v0, p0, Lcom/squareup/noho/EdgePainter;->drawInside:Z

    if-eqz v0, :cond_7

    .line 233
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    sub-int v0, p2, v0

    move v12, v0

    move v0, p2

    move p2, v12

    goto :goto_3

    .line 237
    :cond_7
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    add-int/2addr v0, p2

    .line 239
    :goto_3
    iget v2, p0, Lcom/squareup/noho/EdgePainter;->leftInset:I

    add-int/2addr v1, v2

    int-to-float v5, v1

    int-to-float v6, p2

    iget p2, p0, Lcom/squareup/noho/EdgePainter;->rightInset:I

    sub-int/2addr v3, p2

    int-to-float v7, v3

    int-to-float v8, v0

    iget-object v9, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    move-object v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_8
    return-void

    .line 174
    :cond_9
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "hostView must be a ViewGroup if it allegedly has children."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 170
    :cond_a
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "It\'s gonna look bad, call `drawEdges()` instead!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public drawEdgeInside()V
    .locals 1

    const/4 v0, 0x1

    .line 66
    iput-boolean v0, p0, Lcom/squareup/noho/EdgePainter;->drawInside:Z

    return-void
.end method

.method public drawEdgeOutside()V
    .locals 1

    const/4 v0, 0x0

    .line 59
    iput-boolean v0, p0, Lcom/squareup/noho/EdgePainter;->drawInside:Z

    return-void
.end method

.method public drawEdges(Landroid/graphics/Canvas;)V
    .locals 9

    .line 121
    iget-boolean v0, p0, Lcom/squareup/noho/EdgePainter;->drawInside:Z

    if-eqz v0, :cond_5

    .line 126
    iget-object v0, p0, Lcom/squareup/noho/EdgePainter;->hostView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/squareup/noho/EdgePainter;->hostView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 132
    iget-object v1, p0, Lcom/squareup/noho/EdgePainter;->hostView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 141
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 142
    iget-object v2, p0, Lcom/squareup/noho/EdgePainter;->hostView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/squareup/noho/EdgePainter;->hostView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getScrollY()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 144
    iget v2, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 145
    iget v2, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    int-to-float v6, v2

    int-to-float v7, v1

    iget-object v8, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 148
    :cond_1
    iget v2, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_2

    .line 149
    iget v2, p0, Lcom/squareup/noho/EdgePainter;->leftInset:I

    int-to-float v4, v2

    const/4 v5, 0x0

    iget v2, p0, Lcom/squareup/noho/EdgePainter;->rightInset:I

    sub-int v2, v0, v2

    int-to-float v6, v2

    iget v2, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    int-to-float v7, v2

    iget-object v8, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 152
    :cond_2
    iget v2, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_3

    .line 153
    iget v2, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    sub-int v2, v0, v2

    int-to-float v4, v2

    const/4 v5, 0x0

    int-to-float v6, v0

    int-to-float v7, v1

    iget-object v8, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 156
    :cond_3
    iget v2, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_4

    .line 157
    iget v2, p0, Lcom/squareup/noho/EdgePainter;->leftInset:I

    int-to-float v4, v2

    iget v2, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    sub-int v2, v1, v2

    int-to-float v5, v2

    iget v2, p0, Lcom/squareup/noho/EdgePainter;->rightInset:I

    sub-int/2addr v0, v2

    int-to-float v6, v0

    int-to-float v7, v1

    iget-object v8, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    move-object v3, p1

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 160
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    .line 122
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Can only drawEdges on the inside!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getColor()I
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    return v0
.end method

.method public getEdgeWidth()I
    .locals 1

    .line 85
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    return v0
.end method

.method public getEdges()I
    .locals 1

    .line 109
    iget v0, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    return v0
.end method

.method public setColor(I)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public setEdgeWidth(I)V
    .locals 0

    .line 89
    iput p1, p0, Lcom/squareup/noho/EdgePainter;->edgeWidth:I

    return-void
.end method

.method public setEdges(I)V
    .locals 0

    .line 113
    iput p1, p0, Lcom/squareup/noho/EdgePainter;->sidesToPaint:I

    return-void
.end method

.method public setHorizontalInsets(II)V
    .locals 0

    .line 73
    iput p1, p0, Lcom/squareup/noho/EdgePainter;->leftInset:I

    .line 74
    iput p2, p0, Lcom/squareup/noho/EdgePainter;->rightInset:I

    return-void
.end method

.method public setMultiply()V
    .locals 3

    .line 97
    iget-object v0, p0, Lcom/squareup/noho/EdgePainter;->edgePaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    return-void
.end method

.method public setRightInset(I)V
    .locals 0

    .line 81
    iput p1, p0, Lcom/squareup/noho/EdgePainter;->rightInset:I

    return-void
.end method
