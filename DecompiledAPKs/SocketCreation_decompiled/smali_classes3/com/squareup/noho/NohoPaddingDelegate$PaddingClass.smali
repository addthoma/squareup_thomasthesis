.class final enum Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;
.super Ljava/lang/Enum;
.source "NohoPaddingDelegate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoPaddingDelegate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "PaddingClass"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

.field public static final enum CARD:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

.field public static final enum CARD_ALERT:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

.field public static final enum DETAIL:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

.field public static final enum MASTER:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

.field public static final enum SHEET:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

.field public static final enum SHEET_ALERT:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

.field public static final enum SHEET_PAYMENT_FLOW:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

.field public static final enum SHEET_PAYMENT_FLOW_ALERT:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;


# instance fields
.field private final contentWidth:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

.field private final horizontalRes:I

.field private final verticalRes:I


# direct methods
.method static constructor <clinit>()V
    .locals 18

    .line 86
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->AS_IS:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    const/4 v2, 0x0

    const-string v3, "MASTER"

    invoke-direct {v0, v3, v2, v2, v1}, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;-><init>(Ljava/lang/String;IILcom/squareup/noho/NohoPaddingDelegate$ContentWidth;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->MASTER:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    .line 90
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    sget v1, Lcom/squareup/noho/R$dimen;->noho_gutter_detail:I

    sget-object v3, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->AS_IS:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    const/4 v4, 0x1

    const-string v5, "DETAIL"

    invoke-direct {v0, v5, v4, v1, v3}, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;-><init>(Ljava/lang/String;IILcom/squareup/noho/NohoPaddingDelegate$ContentWidth;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->DETAIL:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    .line 95
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    sget v9, Lcom/squareup/noho/R$dimen;->noho_gutter_card_vertical:I

    sget v10, Lcom/squareup/noho/R$dimen;->noho_gutter_card_horizontal:I

    sget-object v11, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->AS_IS:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    const-string v7, "CARD"

    const/4 v8, 0x2

    move-object v6, v0

    invoke-direct/range {v6 .. v11}, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;-><init>(Ljava/lang/String;IIILcom/squareup/noho/NohoPaddingDelegate$ContentWidth;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->CARD:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    .line 100
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    sget v15, Lcom/squareup/noho/R$dimen;->noho_gutter_card_alert_vertical:I

    sget v16, Lcom/squareup/noho/R$dimen;->noho_gutter_card_alert_horizontal:I

    sget-object v17, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->AS_IS:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    const-string v13, "CARD_ALERT"

    const/4 v14, 0x3

    move-object v12, v0

    invoke-direct/range {v12 .. v17}, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;-><init>(Ljava/lang/String;IIILcom/squareup/noho/NohoPaddingDelegate$ContentWidth;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->CARD_ALERT:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    .line 106
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    sget v8, Lcom/squareup/noho/R$dimen;->noho_gutter_sheet_vertical:I

    sget v9, Lcom/squareup/noho/R$dimen;->noho_gutter_sheet_horizontal:I

    sget-object v10, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->SCREEN_MIN_DIMEN_DESIRED:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    const-string v6, "SHEET"

    const/4 v7, 0x4

    move-object v5, v0

    invoke-direct/range {v5 .. v10}, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;-><init>(Ljava/lang/String;IIILcom/squareup/noho/NohoPaddingDelegate$ContentWidth;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->SHEET:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    .line 111
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    sget v14, Lcom/squareup/noho/R$dimen;->noho_gutter_sheet_alert_vertical:I

    sget v15, Lcom/squareup/noho/R$dimen;->noho_gutter_sheet_alert_horizontal:I

    sget-object v16, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->SCREEN_MIN_DIMEN_MINUS_DIMEN_DESIRED:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    const-string v12, "SHEET_ALERT"

    const/4 v13, 0x5

    move-object v11, v0

    invoke-direct/range {v11 .. v16}, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;-><init>(Ljava/lang/String;IIILcom/squareup/noho/NohoPaddingDelegate$ContentWidth;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->SHEET_ALERT:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    .line 117
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    sget v8, Lcom/squareup/noho/R$dimen;->noho_gutter_sheet_vertical_payment_flow:I

    sget-object v10, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->SCREEN_MIN_DIMEN_DESIRED:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    const-string v6, "SHEET_PAYMENT_FLOW"

    const/4 v7, 0x6

    const/4 v9, 0x0

    move-object v5, v0

    invoke-direct/range {v5 .. v10}, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;-><init>(Ljava/lang/String;IIILcom/squareup/noho/NohoPaddingDelegate$ContentWidth;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->SHEET_PAYMENT_FLOW:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    .line 122
    new-instance v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    sget-object v16, Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;->SCREEN_MIN_DIMEN_MINUS_DIMEN_DESIRED:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    const-string v12, "SHEET_PAYMENT_FLOW_ALERT"

    const/4 v13, 0x7

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v16}, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;-><init>(Ljava/lang/String;IIILcom/squareup/noho/NohoPaddingDelegate$ContentWidth;)V

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->SHEET_PAYMENT_FLOW_ALERT:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    .line 85
    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->MASTER:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->DETAIL:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->CARD:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->CARD_ALERT:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->SHEET:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->SHEET_ALERT:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->SHEET_PAYMENT_FLOW:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->SHEET_PAYMENT_FLOW_ALERT:Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->$VALUES:[Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILcom/squareup/noho/NohoPaddingDelegate$ContentWidth;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;",
            ")V"
        }
    .end annotation

    .line 137
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 138
    iput p3, p0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->verticalRes:I

    .line 139
    iput p4, p0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->horizontalRes:I

    .line 140
    iput-object p5, p0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->contentWidth:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/squareup/noho/NohoPaddingDelegate$ContentWidth;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p3

    move-object v5, p4

    .line 133
    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;-><init>(Ljava/lang/String;IIILcom/squareup/noho/NohoPaddingDelegate$ContentWidth;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)I
    .locals 0

    .line 85
    iget p0, p0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->horizontalRes:I

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;
    .locals 0

    .line 85
    iget-object p0, p0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->contentWidth:Lcom/squareup/noho/NohoPaddingDelegate$ContentWidth;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;)I
    .locals 0

    .line 85
    iget p0, p0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->verticalRes:I

    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;
    .locals 1

    .line 85
    const-class v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    return-object p0
.end method

.method public static values()[Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;
    .locals 1

    .line 85
    sget-object v0, Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->$VALUES:[Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    invoke-virtual {v0}, [Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/noho/NohoPaddingDelegate$PaddingClass;

    return-object v0
.end method
