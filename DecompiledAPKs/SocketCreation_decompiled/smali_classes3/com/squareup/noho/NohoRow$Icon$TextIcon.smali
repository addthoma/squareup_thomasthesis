.class public final Lcom/squareup/noho/NohoRow$Icon$TextIcon;
.super Lcom/squareup/noho/NohoRow$Icon;
.source "NohoRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoRow$Icon;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TextIcon"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0005H\u00c6\u0003J\'\u0010\u0010\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0003\u0010\u0006\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u00d6\u0003J\t\u0010\u0015\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/noho/NohoRow$Icon$TextIcon;",
        "Lcom/squareup/noho/NohoRow$Icon;",
        "text",
        "",
        "background",
        "",
        "shapeId",
        "(Ljava/lang/String;II)V",
        "getBackground",
        "()I",
        "getShapeId",
        "getText",
        "()Ljava/lang/String;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final background:I

.field private final shapeId:I

.field private final text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 901
    invoke-direct {p0, v0}, Lcom/squareup/noho/NohoRow$Icon;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->text:Ljava/lang/String;

    iput p2, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->background:I

    iput p3, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->shapeId:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 900
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/noho/NohoRow$Icon$TextIcon;Ljava/lang/String;IIILjava/lang/Object;)Lcom/squareup/noho/NohoRow$Icon$TextIcon;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->text:Ljava/lang/String;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->background:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->shapeId:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->copy(Ljava/lang/String;II)Lcom/squareup/noho/NohoRow$Icon$TextIcon;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->text:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->background:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->shapeId:I

    return v0
.end method

.method public final copy(Ljava/lang/String;II)Lcom/squareup/noho/NohoRow$Icon$TextIcon;
    .locals 1

    const-string v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/noho/NohoRow$Icon$TextIcon;-><init>(Ljava/lang/String;II)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/noho/NohoRow$Icon$TextIcon;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/noho/NohoRow$Icon$TextIcon;

    iget-object v0, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->text:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->text:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->background:I

    iget v1, p1, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->background:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->shapeId:I

    iget p1, p1, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->shapeId:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBackground()I
    .locals 1

    .line 899
    iget v0, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->background:I

    return v0
.end method

.method public final getShapeId()I
    .locals 1

    .line 900
    iget v0, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->shapeId:I

    return v0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .line 898
    iget-object v0, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->text:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->text:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->background:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->shapeId:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TextIcon(text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", background="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->background:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", shapeId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/noho/NohoRow$Icon$TextIcon;->shapeId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
