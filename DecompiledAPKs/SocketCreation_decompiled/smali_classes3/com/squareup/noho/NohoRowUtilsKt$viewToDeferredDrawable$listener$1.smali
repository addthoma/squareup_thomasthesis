.class public final Lcom/squareup/noho/NohoRowUtilsKt$viewToDeferredDrawable$listener$1;
.super Ljava/lang/Object;
.source "NohoRowUtils.kt"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/noho/NohoRowUtilsKt;->viewToDeferredDrawable(Landroid/view/View;)Landroid/graphics/drawable/Drawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u00012\u00020\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0002J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0010\u0010\n\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\tH\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "com/squareup/noho/NohoRowUtilsKt$viewToDeferredDrawable$listener$1",
        "Landroid/view/ViewTreeObserver$OnPreDrawListener;",
        "Landroid/view/View$OnAttachStateChangeListener;",
        "disconnect",
        "",
        "onPreDraw",
        "",
        "onViewAttachedToWindow",
        "view",
        "Landroid/view/View;",
        "onViewDetachedFromWindow",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic $wrapper:Lcom/squareup/noho/DrawableWrapper;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/squareup/noho/DrawableWrapper;)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/squareup/noho/NohoRowUtilsKt$viewToDeferredDrawable$listener$1;->$view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/noho/NohoRowUtilsKt$viewToDeferredDrawable$listener$1;->$wrapper:Lcom/squareup/noho/DrawableWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final disconnect()V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/noho/NohoRowUtilsKt$viewToDeferredDrawable$listener$1;->$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    move-object v1, p0

    check-cast v1, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/noho/NohoRowUtilsKt$viewToDeferredDrawable$listener$1;->$view:Landroid/view/View;

    move-object v1, p0

    check-cast v1, Landroid/view/View$OnAttachStateChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/noho/NohoRowUtilsKt$viewToDeferredDrawable$listener$1;->$view:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/noho/NohoRowUtilsKt;->access$viewToDrawable(Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/squareup/noho/NohoRowUtilsKt$viewToDeferredDrawable$listener$1;->$wrapper:Lcom/squareup/noho/DrawableWrapper;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/DrawableWrapper;->setDelegate(Landroid/graphics/drawable/Drawable;)V

    .line 75
    invoke-direct {p0}, Lcom/squareup/noho/NohoRowUtilsKt$viewToDeferredDrawable$listener$1;->disconnect()V

    const/4 v0, 0x1

    return v0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-direct {p0}, Lcom/squareup/noho/NohoRowUtilsKt$viewToDeferredDrawable$listener$1;->disconnect()V

    return-void
.end method
