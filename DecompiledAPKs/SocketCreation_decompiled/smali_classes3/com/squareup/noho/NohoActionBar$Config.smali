.class public final Lcom/squareup/noho/NohoActionBar$Config;
.super Ljava/lang/Object;
.source "NohoActionBar.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoActionBar$Config$Builder;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001+BG\u0008\u0000\u0012\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0010\u0008\u0002\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n\u0012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000c\u00a2\u0006\u0002\u0010\rJ\u0006\u0010\u0018\u001a\u00020\u0019J\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0003H\u00c0\u0003\u00a2\u0006\u0002\u0008\u001bJ\u0016\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005H\u00c0\u0003\u00a2\u0006\u0002\u0008\u001dJ\u000e\u0010\u001e\u001a\u00020\u0008H\u00c0\u0003\u00a2\u0006\u0002\u0008\u001fJ\u0010\u0010 \u001a\u0004\u0018\u00010\nH\u00c0\u0003\u00a2\u0006\u0002\u0008!J\u0010\u0010\"\u001a\u0004\u0018\u00010\u000cH\u00c0\u0003\u00a2\u0006\u0002\u0008#JI\u0010$\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0010\u0008\u0002\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00c6\u0001J\u0013\u0010%\u001a\u00020\u00082\u0008\u0010&\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\'\u001a\u00020(H\u00d6\u0001J\t\u0010)\u001a\u00020*H\u00d6\u0001R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0014\u0010\u0007\u001a\u00020\u0008X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u001c\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0016\u0010\t\u001a\u0004\u0018\u00010\nX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/noho/NohoActionBar$Config;",
        "",
        "leftBadge",
        "Lcom/squareup/noho/Badge;",
        "title",
        "Lcom/squareup/resources/TextModel;",
        "",
        "centeredTitle",
        "",
        "upButton",
        "Lcom/squareup/noho/NohoActionBar$UpButtonConfig;",
        "action",
        "Lcom/squareup/noho/NohoActionBar$ActionConfig;",
        "(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;)V",
        "getAction$noho_release",
        "()Lcom/squareup/noho/NohoActionBar$ActionConfig;",
        "getCenteredTitle$noho_release",
        "()Z",
        "getLeftBadge$noho_release",
        "()Lcom/squareup/noho/Badge;",
        "getTitle$noho_release",
        "()Lcom/squareup/resources/TextModel;",
        "getUpButton$noho_release",
        "()Lcom/squareup/noho/NohoActionBar$UpButtonConfig;",
        "buildUpon",
        "Lcom/squareup/noho/NohoActionBar$Config$Builder;",
        "component1",
        "component1$noho_release",
        "component2",
        "component2$noho_release",
        "component3",
        "component3$noho_release",
        "component4",
        "component4$noho_release",
        "component5",
        "component5$noho_release",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Builder",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

.field private final centeredTitle:Z

.field private final leftBadge:Lcom/squareup/noho/Badge;

.field private final title:Lcom/squareup/resources/TextModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/squareup/noho/NohoActionBar$Config;-><init>(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/Badge;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;Z",
            "Lcom/squareup/noho/NohoActionBar$UpButtonConfig;",
            "Lcom/squareup/noho/NohoActionBar$ActionConfig;",
            ")V"
        }
    .end annotation

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/NohoActionBar$Config;->leftBadge:Lcom/squareup/noho/Badge;

    iput-object p2, p0, Lcom/squareup/noho/NohoActionBar$Config;->title:Lcom/squareup/resources/TextModel;

    iput-boolean p3, p0, Lcom/squareup/noho/NohoActionBar$Config;->centeredTitle:Z

    iput-object p4, p0, Lcom/squareup/noho/NohoActionBar$Config;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    iput-object p5, p0, Lcom/squareup/noho/NohoActionBar$Config;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 3

    and-int/lit8 p7, p6, 0x1

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    .line 211
    move-object p1, v0

    check-cast p1, Lcom/squareup/noho/Badge;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    .line 212
    move-object p2, v0

    check-cast p2, Lcom/squareup/resources/TextModel;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    const/4 p3, 0x0

    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move v1, p3

    :goto_0
    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    .line 214
    move-object p4, v0

    check-cast p4, Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    :cond_3
    move-object v2, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    .line 215
    move-object p5, v0

    check-cast p5, Lcom/squareup/noho/NohoActionBar$ActionConfig;

    :cond_4
    move-object v0, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v1

    move-object p6, v2

    move-object p7, v0

    invoke-direct/range {p2 .. p7}, Lcom/squareup/noho/NohoActionBar$Config;-><init>(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/noho/NohoActionBar$Config;Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/noho/NohoActionBar$Config;->leftBadge:Lcom/squareup/noho/Badge;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/noho/NohoActionBar$Config;->title:Lcom/squareup/resources/TextModel;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-boolean p3, p0, Lcom/squareup/noho/NohoActionBar$Config;->centeredTitle:Z

    :cond_2
    move v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/noho/NohoActionBar$Config;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/noho/NohoActionBar$Config;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move p5, v0

    move-object p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/noho/NohoActionBar$Config;->copy(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;)Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;
    .locals 7

    .line 217
    new-instance v6, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    iget-object v1, p0, Lcom/squareup/noho/NohoActionBar$Config;->leftBadge:Lcom/squareup/noho/Badge;

    iget-object v2, p0, Lcom/squareup/noho/NohoActionBar$Config;->title:Lcom/squareup/resources/TextModel;

    iget-boolean v3, p0, Lcom/squareup/noho/NohoActionBar$Config;->centeredTitle:Z

    iget-object v4, p0, Lcom/squareup/noho/NohoActionBar$Config;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    iget-object v5, p0, Lcom/squareup/noho/NohoActionBar$Config;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;)V

    return-object v6
.end method

.method public final component1$noho_release()Lcom/squareup/noho/Badge;
    .locals 1

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->leftBadge:Lcom/squareup/noho/Badge;

    return-object v0
.end method

.method public final component2$noho_release()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->title:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final component3$noho_release()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->centeredTitle:Z

    return v0
.end method

.method public final component4$noho_release()Lcom/squareup/noho/NohoActionBar$UpButtonConfig;
    .locals 1

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    return-object v0
.end method

.method public final component5$noho_release()Lcom/squareup/noho/NohoActionBar$ActionConfig;
    .locals 1

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    return-object v0
.end method

.method public final copy(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;)Lcom/squareup/noho/NohoActionBar$Config;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/Badge;",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;Z",
            "Lcom/squareup/noho/NohoActionBar$UpButtonConfig;",
            "Lcom/squareup/noho/NohoActionBar$ActionConfig;",
            ")",
            "Lcom/squareup/noho/NohoActionBar$Config;"
        }
    .end annotation

    new-instance v6, Lcom/squareup/noho/NohoActionBar$Config;

    move-object v0, v6

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoActionBar$Config;-><init>(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;)V

    return-object v6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/noho/NohoActionBar$Config;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/noho/NohoActionBar$Config;

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->leftBadge:Lcom/squareup/noho/Badge;

    iget-object v1, p1, Lcom/squareup/noho/NohoActionBar$Config;->leftBadge:Lcom/squareup/noho/Badge;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->title:Lcom/squareup/resources/TextModel;

    iget-object v1, p1, Lcom/squareup/noho/NohoActionBar$Config;->title:Lcom/squareup/resources/TextModel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->centeredTitle:Z

    iget-boolean v1, p1, Lcom/squareup/noho/NohoActionBar$Config;->centeredTitle:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    iget-object v1, p1, Lcom/squareup/noho/NohoActionBar$Config;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    iget-object p1, p1, Lcom/squareup/noho/NohoActionBar$Config;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAction$noho_release()Lcom/squareup/noho/NohoActionBar$ActionConfig;
    .locals 1

    .line 215
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    return-object v0
.end method

.method public final getCenteredTitle$noho_release()Z
    .locals 1

    .line 213
    iget-boolean v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->centeredTitle:Z

    return v0
.end method

.method public final getLeftBadge$noho_release()Lcom/squareup/noho/Badge;
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->leftBadge:Lcom/squareup/noho/Badge;

    return-object v0
.end method

.method public final getTitle$noho_release()Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 212
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->title:Lcom/squareup/resources/TextModel;

    return-object v0
.end method

.method public final getUpButton$noho_release()Lcom/squareup/noho/NohoActionBar$UpButtonConfig;
    .locals 1

    .line 214
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar$Config;->leftBadge:Lcom/squareup/noho/Badge;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/noho/NohoActionBar$Config;->title:Lcom/squareup/resources/TextModel;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/noho/NohoActionBar$Config;->centeredTitle:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/noho/NohoActionBar$Config;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/noho/NohoActionBar$Config;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Config(leftBadge="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/noho/NohoActionBar$Config;->leftBadge:Lcom/squareup/noho/Badge;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/noho/NohoActionBar$Config;->title:Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", centeredTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/noho/NohoActionBar$Config;->centeredTitle:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", upButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/noho/NohoActionBar$Config;->upButton:Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/noho/NohoActionBar$Config;->action:Lcom/squareup/noho/NohoActionBar$ActionConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
