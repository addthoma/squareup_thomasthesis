.class public Lcom/squareup/noho/NohoActionBar;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "NohoActionBar.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/noho/NohoActionBar$Config;,
        Lcom/squareup/noho/NohoActionBar$UpButtonConfig;,
        Lcom/squareup/noho/NohoActionBar$ActionConfig;,
        Lcom/squareup/noho/NohoActionBar$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoActionBar.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoActionBar.kt\ncom/squareup/noho/NohoActionBar\n+ 2 Views.kt\ncom/squareup/util/Views\n+ 3 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,418:1\n1103#2,7:419\n1103#2,7:426\n1103#2,7:433\n1261#2:440\n37#3,6:441\n*E\n*S KotlinDebug\n*F\n+ 1 NohoActionBar.kt\ncom/squareup/noho/NohoActionBar\n*L\n133#1,7:419\n144#1,7:426\n152#1,7:433\n70#1:440\n90#1,6:441\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0016\u0018\u0000 +2\u00020\u0001:\u0004*+,-B#\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0008\u0010\u001b\u001a\u00020\u001cH\u0002J$\u0010\u000f\u001a\u00020\u001c2\u0019\u0008\u0004\u0010\u001d\u001a\u0013\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u00020\u001c0\u001e\u00a2\u0006\u0002\u0008 H\u0086\u0008J\u0010\u0010!\u001a\u00020\u001c2\u0006\u0010\"\u001a\u00020#H\u0016J\u0010\u0010$\u001a\u00020\u001c2\u0006\u0010\"\u001a\u00020#H\u0014J\u0010\u0010%\u001a\u00020\u001c2\u0006\u0010\u000f\u001a\u00020\u000eH\u0002J \u0010&\u001a\u00020\u001c*\u00020\u000c2\u0008\u0008\u0001\u0010\'\u001a\u00020\u00072\u0008\u0010(\u001a\u0004\u0018\u00010)H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\r\u001a\u00020\u000e@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/noho/NohoActionBar;",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "actionButton",
        "Lcom/squareup/noho/NohoButton;",
        "actionIcon",
        "Landroid/widget/ImageView;",
        "value",
        "Lcom/squareup/noho/NohoActionBar$Config;",
        "config",
        "getConfig",
        "()Lcom/squareup/noho/NohoActionBar$Config;",
        "setConfig",
        "(Lcom/squareup/noho/NohoActionBar$Config;)V",
        "edgePainter",
        "Lcom/squareup/noho/EdgePainter;",
        "spacer",
        "Landroid/widget/Space;",
        "title",
        "Landroid/widget/TextView;",
        "upButton",
        "bindViews",
        "",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/noho/NohoActionBar$Config$Builder;",
        "Lkotlin/ExtensionFunctionType;",
        "dispatchDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "onDraw",
        "updateConfig",
        "setImageAndBadge",
        "iconRes",
        "badge",
        "Lcom/squareup/noho/Badge;",
        "ActionConfig",
        "Companion",
        "Config",
        "UpButtonConfig",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/noho/NohoActionBar$Companion;


# instance fields
.field private actionButton:Lcom/squareup/noho/NohoButton;

.field private actionIcon:Landroid/widget/ImageView;

.field private config:Lcom/squareup/noho/NohoActionBar$Config;

.field private final edgePainter:Lcom/squareup/noho/EdgePainter;

.field private spacer:Landroid/widget/Space;

.field private title:Landroid/widget/TextView;

.field private upButton:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/noho/NohoActionBar$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/noho/NohoActionBar$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/noho/NohoActionBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 440
    sget-object v0, Lcom/squareup/util/VectorFriendly;->INSTANCE:Lcom/squareup/util/VectorFriendly;

    invoke-virtual {v0, p1}, Lcom/squareup/util/VectorFriendly;->ensureContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 70
    invoke-direct {p0, v0, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x1f

    const/4 v8, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/noho/NohoActionBar$Config;-><init>(Lcom/squareup/noho/Badge;Lcom/squareup/resources/TextModel;ZLcom/squareup/noho/NohoActionBar$UpButtonConfig;Lcom/squareup/noho/NohoActionBar$ActionConfig;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object v0, p0, Lcom/squareup/noho/NohoActionBar;->config:Lcom/squareup/noho/NohoActionBar$Config;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/noho/NohoActionBar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/noho/R$layout;->noho_action_bar_contents:I

    move-object v2, p0

    check-cast v2, Landroid/view/ViewGroup;

    invoke-static {v0, v1, v2}, Landroidx/constraintlayout/widget/ConstraintLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 88
    invoke-direct {p0}, Lcom/squareup/noho/NohoActionBar;->bindViews()V

    .line 92
    sget-object v0, Lcom/squareup/noho/R$styleable;->NohoActionBar:[I

    const-string v1, "R.styleable.NohoActionBar"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    sget v1, Lcom/squareup/noho/R$style;->Widget_Noho_TopBar:I

    .line 441
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    :try_start_0
    const-string p2, "a"

    .line 443
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    sget p2, Lcom/squareup/noho/R$styleable;->NohoActionBar_sqEdgeWidth:I

    const/4 p3, -0x1

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p2

    .line 97
    sget v0, Lcom/squareup/noho/R$styleable;->NohoActionBar_sqEdgeColor:I

    invoke-virtual {p1, v0, p3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p3

    .line 98
    new-instance v0, Lcom/squareup/noho/EdgePainter;

    move-object v1, p0

    check-cast v1, Landroid/view/View;

    invoke-direct {v0, v1, p2, p3}, Lcom/squareup/noho/EdgePainter;-><init>(Landroid/view/View;II)V

    const/16 p2, 0x8

    .line 99
    invoke-virtual {v0, p2}, Lcom/squareup/noho/EdgePainter;->setEdges(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 445
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 442
    iput-object v0, p0, Lcom/squareup/noho/NohoActionBar;->edgePainter:Lcom/squareup/noho/EdgePainter;

    return-void

    :catchall_0
    move-exception p2

    .line 445
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    .line 69
    sget p3, Lcom/squareup/noho/R$attr;->nohoActionBarStyle:I

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoActionBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final bindViews()V
    .locals 2

    .line 172
    sget v0, Lcom/squareup/noho/R$id;->up_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/noho/NohoActionBar;->upButton:Landroid/widget/ImageView;

    .line 173
    sget v0, Lcom/squareup/noho/R$id;->spacer:I

    invoke-virtual {p0, v0}, Lcom/squareup/noho/NohoActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "findViewById(R.id.spacer)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Space;

    iput-object v0, p0, Lcom/squareup/noho/NohoActionBar;->spacer:Landroid/widget/Space;

    .line 174
    sget v0, Lcom/squareup/noho/R$id;->title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/noho/NohoActionBar;->title:Landroid/widget/TextView;

    .line 175
    sget v0, Lcom/squareup/noho/R$id;->action_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionButton:Lcom/squareup/noho/NohoButton;

    .line 176
    sget v0, Lcom/squareup/noho/R$id;->action_icon:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionIcon:Landroid/widget/ImageView;

    .line 177
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->upButton:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    const-string/jumbo v1, "upButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    const-string v1, "actionIcon"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/widgets/CheatSheet;->setup(Landroid/view/View;)V

    return-void
.end method

.method public static final findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p0

    return-object p0
.end method

.method private final setImageAndBadge(Landroid/widget/ImageView;ILcom/squareup/noho/Badge;)V
    .locals 4

    const-string v0, "context"

    if-eqz p3, :cond_0

    .line 164
    new-instance v1, Lcom/squareup/noho/NotificationDrawable;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p3}, Lcom/squareup/noho/Badge;->getType()Lcom/squareup/noho/Badge$Type;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/Badge$Type;->getDefStyleAttr()I

    move-result v0

    invoke-virtual {p3}, Lcom/squareup/noho/Badge;->getType()Lcom/squareup/noho/Badge$Type;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/noho/Badge$Type;->getDefStyleRes()I

    move-result v3

    invoke-direct {v1, v2, p2, v0, v3}, Lcom/squareup/noho/NotificationDrawable;-><init>(Landroid/content/Context;III)V

    .line 165
    invoke-virtual {p3}, Lcom/squareup/noho/Badge;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/noho/NotificationDrawable;->setText(Ljava/lang/String;)V

    .line 163
    check-cast v1, Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 167
    :cond_0
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p3, p2, v1, v0, v1}, Lcom/squareup/noho/ContextExtensions;->getCustomDrawable$default(Landroid/content/Context;ILandroid/content/res/Resources$Theme;ILjava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 168
    :goto_0
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private final updateConfig(Lcom/squareup/noho/NohoActionBar$Config;)V
    .locals 6

    .line 123
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->title:Landroid/widget/TextView;

    const-string/jumbo v1, "title"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config;->getTitle$noho_release()Lcom/squareup/resources/TextModel;

    move-result-object v2

    const-string v3, "context"

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoActionBar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->title:Landroid/widget/TextView;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config;->getCenteredTitle$noho_release()Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0x11

    goto :goto_1

    :cond_3
    const v1, 0x800013

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 130
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->spacer:Landroid/widget/Space;

    if-nez v0, :cond_4

    const-string v1, "spacer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config;->getCenteredTitle$noho_release()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 132
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->upButton:Landroid/widget/ImageView;

    const-string/jumbo v1, "upButton"

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config;->getUpButton$noho_release()Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    move-result-object v4

    const/4 v5, 0x0

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    goto :goto_2

    :cond_6
    const/4 v4, 0x0

    :goto_2
    invoke-static {v0, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 133
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config;->getUpButton$noho_release()Lcom/squareup/noho/NohoActionBar$UpButtonConfig;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 134
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$UpButtonConfig;->getIconRes()I

    move-result v4

    if-eqz v4, :cond_7

    goto :goto_3

    :cond_7
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_b

    .line 135
    iget-object v2, p0, Lcom/squareup/noho/NohoActionBar;->upButton:Landroid/widget/ImageView;

    if-nez v2, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$UpButtonConfig;->getIconRes()I

    move-result v4

    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config;->getLeftBadge$noho_release()Lcom/squareup/noho/Badge;

    move-result-object v5

    invoke-direct {p0, v2, v4, v5}, Lcom/squareup/noho/NohoActionBar;->setImageAndBadge(Landroid/widget/ImageView;ILcom/squareup/noho/Badge;)V

    .line 136
    iget-object v2, p0, Lcom/squareup/noho/NohoActionBar;->upButton:Landroid/widget/ImageView;

    if-nez v2, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$UpButtonConfig;->getTooltip()Lcom/squareup/resources/TextModel;

    move-result-object v4

    invoke-virtual {p0}, Lcom/squareup/noho/NohoActionBar;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v2, p0, Lcom/squareup/noho/NohoActionBar;->upButton:Landroid/widget/ImageView;

    if-nez v2, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast v2, Landroid/view/View;

    .line 419
    new-instance v1, Lcom/squareup/noho/NohoActionBar$$special$$inlined$onClickDebounced$1;

    invoke-direct {v1, v0}, Lcom/squareup/noho/NohoActionBar$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/noho/NohoActionBar$UpButtonConfig;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4

    .line 134
    :cond_b
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "A valid icon resource is required."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 140
    :cond_c
    :goto_4
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionButton:Lcom/squareup/noho/NohoButton;

    const-string v1, "actionButton"

    if-nez v0, :cond_d

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config;->getAction$noho_release()Lcom/squareup/noho/NohoActionBar$ActionConfig;

    move-result-object v2

    instance-of v2, v2, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionButtonConfig;

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 141
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionIcon:Landroid/widget/ImageView;

    const-string v2, "actionIcon"

    if-nez v0, :cond_e

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config;->getAction$noho_release()Lcom/squareup/noho/NohoActionBar$ActionConfig;

    move-result-object v4

    instance-of v4, v4, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;

    invoke-static {v0, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 142
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config;->getAction$noho_release()Lcom/squareup/noho/NohoActionBar$ActionConfig;

    move-result-object v0

    .line 143
    instance-of v4, v0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionButtonConfig;

    if-eqz v4, :cond_13

    .line 144
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config;->getAction$noho_release()Lcom/squareup/noho/NohoActionBar$ActionConfig;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionButtonConfig;

    .line 145
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_f

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionButtonConfig;->getStyle()Lcom/squareup/noho/NohoActionButtonStyle;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/squareup/noho/NohoActionButtonStyleKt;->applyFrom(Lcom/squareup/noho/NohoButton;Lcom/squareup/noho/NohoActionButtonStyle;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_10

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionButtonConfig;->getLabel()Lcom/squareup/resources/TextModel;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/noho/NohoActionBar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_11

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    check-cast v0, Landroid/view/View;

    .line 426
    new-instance v2, Lcom/squareup/noho/NohoActionBar$$special$$inlined$onClickDebounced$2;

    invoke-direct {v2, p1}, Lcom/squareup/noho/NohoActionBar$$special$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionButtonConfig;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez v0, :cond_12

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionButtonConfig;->isEnabled()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    goto :goto_5

    .line 151
    :cond_13
    instance-of v0, v0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;

    if-eqz v0, :cond_18

    .line 152
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config;->getAction$noho_release()Lcom/squareup/noho/NohoActionBar$ActionConfig;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;

    .line 153
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_14

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_14
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->getIconRes()I

    move-result v1

    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->getBadge()Lcom/squareup/noho/Badge;

    move-result-object v4

    invoke-direct {p0, v0, v1, v4}, Lcom/squareup/noho/NohoActionBar;->setImageAndBadge(Landroid/widget/ImageView;ILcom/squareup/noho/Badge;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_15

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_15
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->getTooltip()Lcom/squareup/resources/TextModel;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/noho/NohoActionBar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v4}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_16

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_16
    check-cast v0, Landroid/view/View;

    .line 433
    new-instance v1, Lcom/squareup/noho/NohoActionBar$$special$$inlined$onClickDebounced$3;

    invoke-direct {v1, p1}, Lcom/squareup/noho/NohoActionBar$$special$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_17

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_17
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;->isEnabled()Z

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    :cond_18
    :goto_5
    return-void
.end method


# virtual methods
.method public final config(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/noho/NohoActionBar$Config$Builder;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 351
    invoke-virtual {p0}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    invoke-super {p0, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->edgePainter:Lcom/squareup/noho/EdgePainter;

    iget-object v1, p0, Lcom/squareup/noho/NohoActionBar;->upButton:Landroid/widget/ImageView;

    if-nez v1, :cond_0

    const-string/jumbo v2, "upButton"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v1, Landroid/view/View;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/noho/EdgePainter;->drawChildEdges(Landroid/graphics/Canvas;Landroid/view/View;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->config:Lcom/squareup/noho/NohoActionBar$Config;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config;->getAction$noho_release()Lcom/squareup/noho/NohoActionBar$ActionConfig;

    move-result-object v0

    .line 109
    instance-of v1, v0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionButtonConfig;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast v0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionButtonConfig;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionButtonConfig;->getStyle()Lcom/squareup/noho/NohoActionButtonStyle;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionButtonStyle;->getShowsBottomEdge()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/squareup/noho/NohoActionBar;->actionButton:Lcom/squareup/noho/NohoButton;

    if-nez v2, :cond_1

    const-string v0, "actionButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v2, Landroid/view/View;

    goto :goto_0

    .line 110
    :cond_2
    instance-of v0, v0, Lcom/squareup/noho/NohoActionBar$ActionConfig$ActionIconConfig;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->actionIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_3

    const-string v1, "actionIcon"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    move-object v2, v0

    check-cast v2, Landroid/view/View;

    :cond_4
    :goto_0
    if-eqz v2, :cond_5

    .line 114
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0, p1, v2}, Lcom/squareup/noho/EdgePainter;->drawChildEdges(Landroid/graphics/Canvas;Landroid/view/View;)V

    :cond_5
    return-void
.end method

.method public final getConfig()Lcom/squareup/noho/NohoActionBar$Config;
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->config:Lcom/squareup/noho/NohoActionBar$Config;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/noho/NohoActionBar;->edgePainter:Lcom/squareup/noho/EdgePainter;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/EdgePainter;->drawEdges(Landroid/graphics/Canvas;)V

    .line 119
    invoke-super {p0, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public final setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V
    .locals 1

    const-string/jumbo v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    iput-object p1, p0, Lcom/squareup/noho/NohoActionBar;->config:Lcom/squareup/noho/NohoActionBar$Config;

    .line 83
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoActionBar;->updateConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method
