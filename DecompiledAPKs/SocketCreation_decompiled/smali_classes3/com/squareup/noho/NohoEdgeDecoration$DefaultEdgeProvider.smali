.class public final Lcom/squareup/noho/NohoEdgeDecoration$DefaultEdgeProvider;
.super Ljava/lang/Object;
.source "NohoEdgeDecoration.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoEdgeDecoration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DefaultEdgeProvider"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u001a\u0010\t\u001a\u00020\n2\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u000b\u001a\u00020\nH\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/noho/NohoEdgeDecoration$DefaultEdgeProvider;",
        "Lcom/squareup/noho/NohoEdgeDecoration$EdgeProvider;",
        "()V",
        "dividerPadding",
        "",
        "vh",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "padding",
        "Landroid/graphics/Rect;",
        "edges",
        "",
        "index",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/noho/NohoEdgeDecoration$DefaultEdgeProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 74
    new-instance v0, Lcom/squareup/noho/NohoEdgeDecoration$DefaultEdgeProvider;

    invoke-direct {v0}, Lcom/squareup/noho/NohoEdgeDecoration$DefaultEdgeProvider;-><init>()V

    sput-object v0, Lcom/squareup/noho/NohoEdgeDecoration$DefaultEdgeProvider;->INSTANCE:Lcom/squareup/noho/NohoEdgeDecoration$DefaultEdgeProvider;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dividerPadding(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;Landroid/graphics/Rect;)V
    .locals 1

    const-string/jumbo v0, "vh"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "padding"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    instance-of v0, p1, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;

    if-eqz p1, :cond_1

    invoke-interface {p1, p2}, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;->getPadding(Landroid/graphics/Rect;)V

    :cond_1
    return-void
.end method

.method public edges(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)I
    .locals 0

    .line 77
    instance-of p2, p1, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;

    if-eqz p2, :cond_1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;

    invoke-interface {p1}, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;->getEdges()I

    move-result p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.noho.NohoEdgeDecoration.ShowsEdges"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 78
    :cond_1
    instance-of p1, p1, Lcom/squareup/noho/NohoEdgeDecoration$AllEdges;

    if-eqz p1, :cond_2

    const/16 p1, 0xf

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
