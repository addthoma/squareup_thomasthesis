.class public abstract Lcom/squareup/noho/CheckableGroup;
.super Ljava/lang/Object;
.source "CheckableGroups.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K::",
        "Landroid/widget/Checkable;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckableGroups.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckableGroups.kt\ncom/squareup/noho/CheckableGroup\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,285:1\n94#1,8:286\n94#1,8:294\n94#1,8:302\n108#1,2:310\n94#1,4:312\n110#1,2:316\n99#1,3:318\n113#1:321\n94#1,8:322\n11416#2,2:330\n*E\n*S KotlinDebug\n*F\n+ 1 CheckableGroups.kt\ncom/squareup/noho/CheckableGroup\n*L\n109#1,8:286\n125#1,8:294\n134#1,8:302\n145#1,2:310\n145#1,4:312\n145#1,2:316\n145#1,3:318\n145#1:321\n157#1,8:322\n86#1,2:330\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010%\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0004\u0008\u0001\u0010\u00032\u00020\u0004BA\u0008\u0002\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006\u0012*\u0010\u0007\u001a\u0016\u0012\u0012\u0008\u0001\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\t0\u0008\"\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\t\u00a2\u0006\u0002\u0010\nJ\u001b\u0010\u0019\u001a\u00020\u00112\u0006\u0010\u001a\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00028\u0001\u00a2\u0006\u0002\u0010\u001cJ\u001a\u0010\u0019\u001a\u00020\u00112\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\tJ\u0015\u0010\u001e\u001a\u00020\u00112\u0006\u0010\u001a\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u001fJ.\u0010 \u001a\u00020\u00112#\u0010!\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0000\u0012\u0004\u0012\u00020\u00110\"\u00a2\u0006\u0002\u0008#H\u0082\u0008J\u001c\u0010$\u001a\u00020\u00112\u0014\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0010j\u0004\u0018\u0001`\u0012J\u001d\u0010%\u001a\u00020\u00112\u0006\u0010\u001a\u001a\u00028\u00002\u0006\u0010&\u001a\u00020\u0018H\u0002\u00a2\u0006\u0002\u0010\'J\u001d\u0010(\u001a\u00020\u00112\u0006\u0010)\u001a\u00028\u00002\u0006\u0010&\u001a\u00020\u0018H\u0016\u00a2\u0006\u0002\u0010\'J\u0015\u0010*\u001a\u00020\u00112\u0006\u0010\u001a\u001a\u00028\u0000H\u0004\u00a2\u0006\u0002\u0010\u001fJ.\u0010+\u001a\u00020\u00112#\u0010!\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0000\u0012\u0004\u0012\u00020\u00110\"\u00a2\u0006\u0002\u0008#H\u0082\u0008R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u000cX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u001c\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0010j\u0004\u0018\u0001`\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0014X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u0082\u0001\u0002,-\u00a8\u0006."
    }
    d2 = {
        "Lcom/squareup/noho/CheckableGroup;",
        "K",
        "Landroid/widget/Checkable;",
        "V",
        "",
        "attacher",
        "Lcom/squareup/noho/ListenerAttacher;",
        "initialItems",
        "",
        "Lkotlin/Pair;",
        "(Lcom/squareup/noho/ListenerAttacher;[Lkotlin/Pair;)V",
        "items",
        "",
        "getItems$noho_release",
        "()Ljava/util/Map;",
        "listener",
        "Lkotlin/Function0;",
        "",
        "Lcom/squareup/noho/CheckableGroupListener;",
        "selectedItems",
        "",
        "getSelectedItems$noho_release",
        "()Ljava/util/List;",
        "updating",
        "",
        "attach",
        "item",
        "value",
        "(Landroid/widget/Checkable;Ljava/lang/Object;)V",
        "itemAndType",
        "detach",
        "(Landroid/widget/Checkable;)V",
        "ifNotUpdating",
        "block",
        "Lkotlin/Function1;",
        "Lkotlin/ExtensionFunctionType;",
        "onChange",
        "onItemChange",
        "checked",
        "(Landroid/widget/Checkable;Z)V",
        "onItemChanging",
        "checkable",
        "select",
        "update",
        "Lcom/squareup/noho/CheckGroup;",
        "Lcom/squareup/noho/RadioGroup;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final attacher:Lcom/squareup/noho/ListenerAttacher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/noho/ListenerAttacher<",
            "TK;>;"
        }
    .end annotation
.end field

.field private final items:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field private listener:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TK;>;"
        }
    .end annotation
.end field

.field private updating:Z


# direct methods
.method private varargs constructor <init>(Lcom/squareup/noho/ListenerAttacher;[Lkotlin/Pair;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/ListenerAttacher<",
            "TK;>;[",
            "Lkotlin/Pair<",
            "+TK;+TV;>;)V"
        }
    .end annotation

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/noho/CheckableGroup;->attacher:Lcom/squareup/noho/ListenerAttacher;

    .line 74
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast p1, Ljava/util/Map;

    iput-object p1, p0, Lcom/squareup/noho/CheckableGroup;->items:Ljava/util/Map;

    .line 77
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/noho/CheckableGroup;->selectedItems:Ljava/util/List;

    .line 330
    array-length p1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    aget-object v1, p2, v0

    move-object v2, p0

    check-cast v2, Lcom/squareup/noho/CheckableGroup;

    .line 86
    invoke-virtual {v2, v1}, Lcom/squareup/noho/CheckableGroup;->attach(Lkotlin/Pair;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public varargs synthetic constructor <init>(Lcom/squareup/noho/ListenerAttacher;[Lkotlin/Pair;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/CheckableGroup;-><init>(Lcom/squareup/noho/ListenerAttacher;[Lkotlin/Pair;)V

    return-void
.end method

.method public static final synthetic access$getUpdating$p(Lcom/squareup/noho/CheckableGroup;)Z
    .locals 0

    .line 68
    iget-boolean p0, p0, Lcom/squareup/noho/CheckableGroup;->updating:Z

    return p0
.end method

.method public static final synthetic access$onItemChange(Lcom/squareup/noho/CheckableGroup;Landroid/widget/Checkable;Z)V
    .locals 0

    .line 68
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/CheckableGroup;->onItemChange(Landroid/widget/Checkable;Z)V

    return-void
.end method

.method public static final synthetic access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V
    .locals 0

    .line 68
    iput-boolean p1, p0, Lcom/squareup/noho/CheckableGroup;->updating:Z

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/noho/CheckableGroup;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/noho/CheckableGroup;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final ifNotUpdating(Lkotlin/jvm/functions/Function1;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/noho/CheckableGroup<",
            "TK;TV;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 108
    invoke-static {p0}, Lcom/squareup/noho/CheckableGroup;->access$getUpdating$p(Lcom/squareup/noho/CheckableGroup;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    invoke-static {p0}, Lcom/squareup/noho/CheckableGroup;->access$getUpdating$p(Lcom/squareup/noho/CheckableGroup;)Z

    move-result v0

    const/4 v1, 0x1

    .line 288
    :try_start_0
    invoke-static {p0, v1}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    .line 289
    move-object v2, p0

    check-cast v2, Lcom/squareup/noho/CheckableGroup;

    .line 110
    invoke-interface {p1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 291
    invoke-static {p0, v0}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    goto :goto_0

    :catchall_0
    move-exception p1

    .line 292
    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 291
    invoke-static {p0, v0}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    throw p1

    :cond_0
    :goto_0
    return-void
.end method

.method private final onItemChange(Landroid/widget/Checkable;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)V"
        }
    .end annotation

    .line 310
    invoke-static {p0}, Lcom/squareup/noho/CheckableGroup;->access$getUpdating$p(Lcom/squareup/noho/CheckableGroup;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 312
    invoke-static {p0}, Lcom/squareup/noho/CheckableGroup;->access$getUpdating$p(Lcom/squareup/noho/CheckableGroup;)Z

    move-result v0

    const/4 v1, 0x1

    .line 314
    :try_start_0
    invoke-static {p0, v1}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    .line 315
    move-object v1, p0

    check-cast v1, Lcom/squareup/noho/CheckableGroup;

    .line 146
    invoke-virtual {v1, p1, p2}, Lcom/squareup/noho/CheckableGroup;->onItemChanging(Landroid/widget/Checkable;Z)V

    if-eqz p2, :cond_0

    .line 148
    iget-object p2, v1, Lcom/squareup/noho/CheckableGroup;->selectedItems:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 150
    :cond_0
    iget-object p2, v1, Lcom/squareup/noho/CheckableGroup;->selectedItems:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 152
    :goto_0
    iget-object p1, v1, Lcom/squareup/noho/CheckableGroup;->listener:Lkotlin/jvm/functions/Function0;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318
    :cond_1
    invoke-static {p0, v0}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    goto :goto_1

    :catchall_0
    move-exception p1

    invoke-static {p0, v0}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    throw p1

    :cond_2
    :goto_1
    return-void
.end method

.method private final update(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/noho/CheckableGroup<",
            "TK;TV;>;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 94
    invoke-static {p0}, Lcom/squareup/noho/CheckableGroup;->access$getUpdating$p(Lcom/squareup/noho/CheckableGroup;)Z

    move-result v0

    const/4 v1, 0x1

    .line 96
    :try_start_0
    invoke-static {p0, v1}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    .line 97
    invoke-interface {p1, p0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 99
    invoke-static {p0, v0}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    return-void

    :catchall_0
    move-exception p1

    .line 100
    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyStart(I)V

    .line 99
    invoke-static {p0, v0}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    invoke-static {v1}, Lkotlin/jvm/internal/InlineMarker;->finallyEnd(I)V

    throw p1
.end method


# virtual methods
.method public final attach(Landroid/widget/Checkable;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/noho/CheckableGroup;->items:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    if-eqz v0, :cond_2

    .line 122
    iget-object v0, p0, Lcom/squareup/noho/CheckableGroup;->items:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    iget-object p2, p0, Lcom/squareup/noho/CheckableGroup;->attacher:Lcom/squareup/noho/ListenerAttacher;

    new-instance v0, Lcom/squareup/noho/CheckableGroup$attach$2;

    move-object v2, p0

    check-cast v2, Lcom/squareup/noho/CheckableGroup;

    invoke-direct {v0, v2}, Lcom/squareup/noho/CheckableGroup$attach$2;-><init>(Lcom/squareup/noho/CheckableGroup;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p2, p1, v0}, Lcom/squareup/noho/ListenerAttacher;->attach(Ljava/lang/Object;Lkotlin/jvm/functions/Function2;)V

    .line 124
    invoke-interface {p1}, Landroid/widget/Checkable;->isChecked()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 294
    invoke-static {p0}, Lcom/squareup/noho/CheckableGroup;->access$getUpdating$p(Lcom/squareup/noho/CheckableGroup;)Z

    move-result p2

    .line 296
    :try_start_0
    invoke-static {p0, v1}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    .line 297
    move-object v0, p0

    check-cast v0, Lcom/squareup/noho/CheckableGroup;

    .line 126
    invoke-virtual {v0, p1, v1}, Lcom/squareup/noho/CheckableGroup;->onItemChanging(Landroid/widget/Checkable;Z)V

    .line 127
    iget-object v1, v0, Lcom/squareup/noho/CheckableGroup;->selectedItems:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    iget-object p1, v0, Lcom/squareup/noho/CheckableGroup;->listener:Lkotlin/jvm/functions/Function0;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    :cond_0
    invoke-static {p0, p2}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    goto :goto_0

    :catchall_0
    move-exception p1

    invoke-static {p0, p2}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    throw p1

    :cond_1
    :goto_0
    return-void

    .line 121
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " ("

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ") is already included in the CheckableGroup."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method public final attach(Lkotlin/Pair;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+TK;+TV;>;)V"
        }
    .end annotation

    const-string v0, "itemAndType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Checkable;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    .line 117
    invoke-virtual {p0, v0, p1}, Lcom/squareup/noho/CheckableGroup;->attach(Landroid/widget/Checkable;Ljava/lang/Object;)V

    return-void
.end method

.method public detach(Landroid/widget/Checkable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 302
    invoke-static {p0}, Lcom/squareup/noho/CheckableGroup;->access$getUpdating$p(Lcom/squareup/noho/CheckableGroup;)Z

    move-result v0

    const/4 v1, 0x1

    .line 304
    :try_start_0
    invoke-static {p0, v1}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    .line 305
    move-object v1, p0

    check-cast v1, Lcom/squareup/noho/CheckableGroup;

    .line 135
    iget-object v2, v1, Lcom/squareup/noho/CheckableGroup;->attacher:Lcom/squareup/noho/ListenerAttacher;

    invoke-virtual {v2, p1}, Lcom/squareup/noho/ListenerAttacher;->detach(Ljava/lang/Object;)V

    .line 136
    iget-object v2, v1, Lcom/squareup/noho/CheckableGroup;->items:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v2, v1, Lcom/squareup/noho/CheckableGroup;->selectedItems:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, v1, Lcom/squareup/noho/CheckableGroup;->listener:Lkotlin/jvm/functions/Function0;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    :cond_0
    invoke-static {p0, v0}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    return-void

    :catchall_0
    move-exception p1

    invoke-static {p0, v0}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    throw p1
.end method

.method public final getItems$noho_release()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "TK;TV;>;"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/squareup/noho/CheckableGroup;->items:Ljava/util/Map;

    return-object v0
.end method

.method public final getSelectedItems$noho_release()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TK;>;"
        }
    .end annotation

    .line 77
    iget-object v0, p0, Lcom/squareup/noho/CheckableGroup;->selectedItems:Ljava/util/List;

    return-object v0
.end method

.method public final onChange(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 168
    iput-object p1, p0, Lcom/squareup/noho/CheckableGroup;->listener:Lkotlin/jvm/functions/Function0;

    return-void
.end method

.method public onItemChanging(Landroid/widget/Checkable;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)V"
        }
    .end annotation

    const-string p2, "checkable"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method protected final select(Landroid/widget/Checkable;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)V"
        }
    .end annotation

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 322
    invoke-static {p0}, Lcom/squareup/noho/CheckableGroup;->access$getUpdating$p(Lcom/squareup/noho/CheckableGroup;)Z

    move-result v0

    const/4 v1, 0x1

    .line 324
    :try_start_0
    invoke-static {p0, v1}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    .line 325
    move-object v2, p0

    check-cast v2, Lcom/squareup/noho/CheckableGroup;

    .line 158
    invoke-interface {p1}, Landroid/widget/Checkable;->isChecked()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    .line 159
    :goto_0
    invoke-interface {p1, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 160
    iget-object v1, v2, Lcom/squareup/noho/CheckableGroup;->selectedItems:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_1

    .line 162
    iget-object p1, v2, Lcom/squareup/noho/CheckableGroup;->listener:Lkotlin/jvm/functions/Function0;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    :cond_1
    invoke-static {p0, v0}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    return-void

    :catchall_0
    move-exception p1

    invoke-static {p0, v0}, Lcom/squareup/noho/CheckableGroup;->access$setUpdating$p(Lcom/squareup/noho/CheckableGroup;Z)V

    throw p1
.end method
