.class public abstract Lcom/squareup/noho/NohoRow$FeatureProperty;
.super Ljava/lang/Object;
.source "NohoRow.kt"

# interfaces
.implements Lcom/squareup/noho/NohoRow$ArrangeableProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/noho/NohoRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "FeatureProperty"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/noho/NohoRow$ArrangeableProvider;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRow$FeatureProperty\n+ 2 StyledAttributes.kt\ncom/squareup/util/StyledAttributesKt\n*L\n1#1,1059:1\n37#2,6:1060\n*E\n*S KotlinDebug\n*F\n+ 1 NohoRow.kt\ncom/squareup/noho/NohoRow$FeatureProperty\n*L\n547#1,6:1060\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0008$\u0018\u0000*\u0004\u0008\u0000\u0010\u0001*\u0008\u0008\u0001\u0010\u0002*\u00020\u00032\u00020\u0004B=\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\u0008\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\u0006\u0012\u0008\u0008\u0001\u0010\n\u001a\u00020\u0006\u0012\u0006\u0010\u000b\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u000cJ\u001f\u0010\u000e\u001a\u00028\u00012\u0006\u0010\u000f\u001a\u00020\u00102\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u0006H$\u00a2\u0006\u0002\u0010\u0012J&\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00142\u0006\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0002J\u0018\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0019\u001a\u00020\u0003H\u0004J\u001d\u0010\u001a\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00028\u00012\u0006\u0010\u001b\u001a\u00028\u0000H\u0014\u00a2\u0006\u0002\u0010\u001cJ$\u0010\u001a\u001a\u00020\u00182\u0006\u0010\u000f\u001a\u00020\u00102\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0014H\u0014J\u0012\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u000e\u0010 \u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u0010J\"\u0010!\u001a\u00028\u00002\u0006\u0010\"\u001a\u00020\u00102\n\u0010#\u001a\u0006\u0012\u0002\u0008\u00030$H\u0086\u0002\u00a2\u0006\u0002\u0010%J\u0018\u0010&\u001a\u00020\u00182\u0006\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016J\u001c\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u00142\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u001c\u0010\'\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u00142\u0006\u0010\u000f\u001a\u00020\u0010J\u0015\u0010(\u001a\u00028\u00002\u0006\u0010\u001b\u001a\u00028\u0000H\u0014\u00a2\u0006\u0002\u0010)J\u0018\u0010*\u001a\u00020\u00182\u0006\u0010\u000f\u001a\u00020\u00102\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u0006J*\u0010+\u001a\u00020\u00182\u0006\u0010\"\u001a\u00020\u00102\n\u0010#\u001a\u0006\u0012\u0002\u0008\u00030$2\u0006\u0010,\u001a\u00028\u0000H\u0086\u0002\u00a2\u0006\u0002\u0010-J\u0015\u0010.\u001a\u00020/2\u0006\u0010\u001b\u001a\u00028\u0000H$\u00a2\u0006\u0002\u00100J\u001f\u00101\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00028\u00012\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u0006H\u0014\u00a2\u0006\u0002\u00102J;\u00101\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00028\u00012\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u00102\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0014H\u0014\u00a2\u0006\u0002\u00103R\u000e\u0010\n\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00028\u0000X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\rR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/noho/NohoRow$FeatureProperty;",
        "T",
        "V",
        "Landroid/view/View;",
        "Lcom/squareup/noho/NohoRow$ArrangeableProvider;",
        "id",
        "",
        "marginId",
        "styleAttrId",
        "styleableId",
        "defStyleRes",
        "defaultValue",
        "(IIIIILjava/lang/Object;)V",
        "Ljava/lang/Object;",
        "create",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "styleId",
        "(Lcom/squareup/noho/NohoRow;I)Landroid/view/View;",
        "createInstance",
        "Lcom/squareup/noho/NohoRow$FeatureInstance;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "destroy",
        "",
        "view",
        "doSetValue",
        "value",
        "(Landroid/view/View;Ljava/lang/Object;)V",
        "instance",
        "get",
        "Lcom/squareup/noho/NohoRow$Arrangeable;",
        "getStyleId",
        "getValue",
        "thisRef",
        "property",
        "Lkotlin/reflect/KProperty;",
        "(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;)Ljava/lang/Object;",
        "initializeInstance",
        "instanceOrNull",
        "normalize",
        "(Ljava/lang/Object;)Ljava/lang/Object;",
        "setStyleId",
        "setValue",
        "valueToSet",
        "(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V",
        "shouldShowFor",
        "",
        "(Ljava/lang/Object;)Z",
        "updateStyleId",
        "(Landroid/view/View;I)V",
        "(Landroid/view/View;ILcom/squareup/noho/NohoRow;Lcom/squareup/noho/NohoRow$FeatureInstance;)V",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final defStyleRes:I

.field private final defaultValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final id:I

.field private final marginId:I

.field private final styleAttrId:I

.field private final styleableId:I


# direct methods
.method public constructor <init>(IIIIILjava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIIITT;)V"
        }
    .end annotation

    .line 497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->id:I

    iput p2, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->marginId:I

    iput p3, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->styleAttrId:I

    iput p4, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->styleableId:I

    iput p5, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->defStyleRes:I

    iput-object p6, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->defaultValue:Ljava/lang/Object;

    return-void
.end method

.method public static final synthetic access$getDefStyleRes$p(Lcom/squareup/noho/NohoRow$FeatureProperty;)I
    .locals 0

    .line 497
    iget p0, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->defStyleRes:I

    return p0
.end method

.method public static final synthetic access$getStyleableId$p(Lcom/squareup/noho/NohoRow$FeatureProperty;)I
    .locals 0

    .line 497
    iget p0, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->styleableId:I

    return p0
.end method

.method private final createInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)Lcom/squareup/noho/NohoRow$FeatureInstance;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoRow;",
            "Landroid/util/AttributeSet;",
            ")",
            "Lcom/squareup/noho/NohoRow$FeatureInstance<",
            "TT;TV;>;"
        }
    .end annotation

    .line 543
    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "row.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->marginId:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 544
    iget v2, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->styleableId:I

    if-nez v2, :cond_0

    .line 545
    iget p2, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->defStyleRes:I

    goto :goto_0

    .line 547
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v1, Lcom/squareup/noho/R$styleable;->NohoRow:[I

    const-string v3, "R.styleable.NohoRow"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow;->getDefStyleAttr()I

    move-result v3

    const/4 v4, 0x0

    .line 1060
    invoke-virtual {v2, p2, v1, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    :try_start_0
    const-string v1, "a"

    .line 1062
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 548
    invoke-static {p0}, Lcom/squareup/noho/NohoRow$FeatureProperty;->access$getStyleableId$p(Lcom/squareup/noho/NohoRow$FeatureProperty;)I

    move-result v1

    invoke-static {p0}, Lcom/squareup/noho/NohoRow$FeatureProperty;->access$getDefStyleRes$p(Lcom/squareup/noho/NohoRow$FeatureProperty;)I

    move-result v2

    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1064
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    move p2, v1

    .line 551
    :goto_0
    new-instance v1, Lcom/squareup/noho/NohoRow$FeatureInstance;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->defaultValue:Ljava/lang/Object;

    invoke-direct {v1, v2, v3, p2, v0}, Lcom/squareup/noho/NohoRow$FeatureInstance;-><init>(Landroid/view/View;Ljava/lang/Object;II)V

    .line 552
    iget p2, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->id:I

    invoke-virtual {p1, p2, v1}, Lcom/squareup/noho/NohoRow;->setTag(ILjava/lang/Object;)V

    return-object v1

    :catchall_0
    move-exception p1

    .line 1064
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    throw p1
.end method

.method private final instance(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$FeatureInstance;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoRow;",
            ")",
            "Lcom/squareup/noho/NohoRow$FeatureInstance<",
            "TT;TV;>;"
        }
    .end annotation

    .line 540
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow$FeatureProperty;->instanceOrNull(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$FeatureInstance;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/noho/NohoRow$FeatureProperty;->createInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)Lcom/squareup/noho/NohoRow$FeatureInstance;

    move-result-object v0

    :goto_0
    return-object v0
.end method


# virtual methods
.method protected abstract create(Lcom/squareup/noho/NohoRow;I)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoRow;",
            "I)TV;"
        }
    .end annotation
.end method

.method protected final destroy(Lcom/squareup/noho/NohoRow;Landroid/view/View;)V
    .locals 1

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 624
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->removeView(Landroid/view/View;)V

    return-void
.end method

.method protected doSetValue(Landroid/view/View;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TT;)V"
        }
    .end annotation

    const-string/jumbo p2, "view"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 622
    new-instance p1, Lkotlin/NotImplementedError;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "An operation is not implemented: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Please override doSetValue"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method protected doSetValue(Lcom/squareup/noho/NohoRow;Lcom/squareup/noho/NohoRow$FeatureInstance;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoRow;",
            "Lcom/squareup/noho/NohoRow$FeatureInstance<",
            "TT;TV;>;)V"
        }
    .end annotation

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "instance"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 618
    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getView()Landroid/view/View;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getValue()Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$FeatureProperty;->doSetValue(Landroid/view/View;Ljava/lang/Object;)V

    return-void
.end method

.method public get(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$Arrangeable;
    .locals 1

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 532
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow$FeatureProperty;->instanceOrNull(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$FeatureInstance;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow$FeatureInstance;->get(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$Arrangeable;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final getStyleId(Lcom/squareup/noho/NohoRow;)I
    .locals 1

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 506
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow$FeatureProperty;->instanceOrNull(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$FeatureInstance;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getStyleId()I

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final getValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoRow;",
            "Lkotlin/reflect/KProperty<",
            "*>;)TT;"
        }
    .end annotation

    const-string/jumbo v0, "thisRef"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "property"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 559
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow$FeatureProperty;->instanceOrNull(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$FeatureInstance;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getValue()Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->defaultValue:Ljava/lang/Object;

    :goto_0
    return-object p1
.end method

.method public final initializeInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)V
    .locals 4

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p2, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 596
    invoke-interface {p2}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_2

    .line 597
    invoke-interface {p2, v0}, Landroid/util/AttributeSet;->getAttributeNameResource(I)I

    move-result v2

    iget v3, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->styleAttrId:I

    if-ne v2, v3, :cond_1

    .line 598
    invoke-direct {p0, p1, p2}, Lcom/squareup/noho/NohoRow$FeatureProperty;->createInstance(Lcom/squareup/noho/NohoRow;Landroid/util/AttributeSet;)Lcom/squareup/noho/NohoRow$FeatureInstance;

    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final instanceOrNull(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$FeatureInstance;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoRow;",
            ")",
            "Lcom/squareup/noho/NohoRow$FeatureInstance<",
            "TT;TV;>;"
        }
    .end annotation

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 536
    iget v0, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->id:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->getTag(I)Ljava/lang/Object;

    move-result-object p1

    instance-of v0, p1, Lcom/squareup/noho/NohoRow$FeatureInstance;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/noho/NohoRow$FeatureInstance;

    return-object p1
.end method

.method protected normalize(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    return-object p1
.end method

.method public final setStyleId(Lcom/squareup/noho/NohoRow;I)V
    .locals 2

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 508
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoRow$FeatureProperty;->instance(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$FeatureInstance;

    move-result-object v0

    .line 509
    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoRow$FeatureInstance;->setStyleId(I)V

    .line 510
    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 512
    invoke-virtual {p0, v1, p2, p1, v0}, Lcom/squareup/noho/NohoRow$FeatureProperty;->updateStyleId(Landroid/view/View;ILcom/squareup/noho/NohoRow;Lcom/squareup/noho/NohoRow$FeatureInstance;)V

    :cond_0
    return-void
.end method

.method public final setValue(Lcom/squareup/noho/NohoRow;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoRow;",
            "Lkotlin/reflect/KProperty<",
            "*>;TT;)V"
        }
    .end annotation

    const-string/jumbo v0, "thisRef"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "property"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 566
    invoke-virtual {p0, p3}, Lcom/squareup/noho/NohoRow$FeatureProperty;->normalize(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .line 567
    invoke-virtual {p0, p2}, Lcom/squareup/noho/NohoRow$FeatureProperty;->shouldShowFor(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_1

    .line 569
    invoke-direct {p0, p1}, Lcom/squareup/noho/NohoRow$FeatureProperty;->instance(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$FeatureInstance;

    move-result-object p3

    .line 570
    invoke-virtual {p3}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getStyleId()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/noho/NohoRow$FeatureProperty;->create(Lcom/squareup/noho/NohoRow;I)Landroid/view/View;

    move-result-object v0

    .line 571
    iget v1, p0, Lcom/squareup/noho/NohoRow$FeatureProperty;->id:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    const/4 v1, 0x0

    .line 572
    invoke-virtual {v0, v1}, Landroid/view/View;->setSaveEnabled(Z)V

    .line 573
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->addView(Landroid/view/View;)V

    .line 574
    invoke-static {p1}, Lcom/squareup/noho/NohoRow;->access$invalidateConstraints(Lcom/squareup/noho/NohoRow;)V

    .line 570
    :goto_0
    invoke-virtual {p3, v0}, Lcom/squareup/noho/NohoRow$FeatureInstance;->setView(Landroid/view/View;)V

    .line 576
    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow$FeatureInstance;->setValue(Ljava/lang/Object;)V

    .line 577
    invoke-virtual {p0, p1, p3}, Lcom/squareup/noho/NohoRow$FeatureProperty;->doSetValue(Lcom/squareup/noho/NohoRow;Lcom/squareup/noho/NohoRow$FeatureInstance;)V

    goto :goto_1

    .line 578
    :cond_1
    invoke-virtual {p0, p1}, Lcom/squareup/noho/NohoRow$FeatureProperty;->instanceOrNull(Lcom/squareup/noho/NohoRow;)Lcom/squareup/noho/NohoRow$FeatureInstance;

    move-result-object p3

    if-eqz p3, :cond_3

    .line 579
    invoke-virtual {p3}, Lcom/squareup/noho/NohoRow$FeatureInstance;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 580
    invoke-virtual {p0, p1, v0}, Lcom/squareup/noho/NohoRow$FeatureProperty;->destroy(Lcom/squareup/noho/NohoRow;Landroid/view/View;)V

    .line 581
    invoke-static {p1}, Lcom/squareup/noho/NohoRow;->access$invalidateConstraints(Lcom/squareup/noho/NohoRow;)V

    :cond_2
    const/4 p1, 0x0

    .line 583
    check-cast p1, Landroid/view/View;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoRow$FeatureInstance;->setView(Landroid/view/View;)V

    .line 584
    invoke-virtual {p3, p2}, Lcom/squareup/noho/NohoRow$FeatureInstance;->setValue(Ljava/lang/Object;)V

    :cond_3
    :goto_1
    return-void
.end method

.method protected abstract shouldShowFor(Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation
.end method

.method protected updateStyleId(Landroid/view/View;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;I)V"
        }
    .end annotation

    const-string/jumbo p2, "view"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method protected updateStyleId(Landroid/view/View;ILcom/squareup/noho/NohoRow;Lcom/squareup/noho/NohoRow$FeatureInstance;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;I",
            "Lcom/squareup/noho/NohoRow;",
            "Lcom/squareup/noho/NohoRow$FeatureInstance<",
            "TT;TV;>;)V"
        }
    .end annotation

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "row"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "instance"

    invoke-static {p4, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 526
    invoke-virtual {p0, p1, p2}, Lcom/squareup/noho/NohoRow$FeatureProperty;->updateStyleId(Landroid/view/View;I)V

    return-void
.end method
