.class public final Lcom/squareup/noho/FlexibleLabelPlugin;
.super Ljava/lang/Object;
.source "NohoEditRowLabelPlugin.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEditRow$Plugin;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNohoEditRowLabelPlugin.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NohoEditRowLabelPlugin.kt\ncom/squareup/noho/FlexibleLabelPlugin\n*L\n1#1,172:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0017\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0010\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0018\u0010 \u001a\u00020\u001a2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$H\u0016R\u000e\u0010\u000b\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u00020\u000f8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u0010R\u0010\u0010\u0011\u001a\u00020\u00078\u0002X\u0083\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0010\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0018\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/noho/FlexibleLabelPlugin;",
        "Lcom/squareup/noho/NohoEditRow$Plugin;",
        "context",
        "Landroid/content/Context;",
        "text",
        "",
        "appearanceId",
        "",
        "maxWidthPercentage",
        "",
        "(Landroid/content/Context;Ljava/lang/String;IF)V",
        "displayText",
        "editText",
        "Lcom/squareup/noho/NohoEditRow;",
        "isClickable",
        "",
        "()Z",
        "marginWithText",
        "paint",
        "Landroid/text/TextPaint;",
        "getText",
        "()Ljava/lang/String;",
        "watcher",
        "com/squareup/noho/FlexibleLabelPlugin$watcher$1",
        "Lcom/squareup/noho/FlexibleLabelPlugin$watcher$1;",
        "attach",
        "",
        "detach",
        "measure",
        "Lcom/squareup/noho/NohoEditRow$PluginSize;",
        "editRect",
        "Landroid/graphics/Rect;",
        "onDraw",
        "canvas",
        "Landroid/graphics/Canvas;",
        "drawingInfo",
        "Lcom/squareup/noho/NohoEditRow$DrawingInfo;",
        "noho_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private displayText:Ljava/lang/String;

.field private editText:Lcom/squareup/noho/NohoEditRow;

.field private final marginWithText:I

.field private final maxWidthPercentage:F

.field private final paint:Landroid/text/TextPaint;

.field private final text:Ljava/lang/String;

.field private final watcher:Lcom/squareup/noho/FlexibleLabelPlugin$watcher$1;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IF)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "text"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->text:Ljava/lang/String;

    iput p4, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->maxWidthPercentage:F

    .line 105
    invoke-static {p1, p3}, Lcom/squareup/noho/CanvasExtensionsKt;->createTextPaintFromTextAppearance(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->paint:Landroid/text/TextPaint;

    .line 107
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/noho/R$dimen;->noho_edit_label_margin_with_text:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->marginWithText:I

    .line 109
    new-instance p1, Lcom/squareup/noho/FlexibleLabelPlugin$watcher$1;

    invoke-direct {p1, p0}, Lcom/squareup/noho/FlexibleLabelPlugin$watcher$1;-><init>(Lcom/squareup/noho/FlexibleLabelPlugin;)V

    iput-object p1, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->watcher:Lcom/squareup/noho/FlexibleLabelPlugin$watcher$1;

    const-string p1, ""

    .line 157
    iput-object p1, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->displayText:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getEditText$p(Lcom/squareup/noho/FlexibleLabelPlugin;)Lcom/squareup/noho/NohoEditRow;
    .locals 1

    .line 98
    iget-object p0, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez p0, :cond_0

    const-string v0, "editText"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setEditText$p(Lcom/squareup/noho/FlexibleLabelPlugin;Lcom/squareup/noho/NohoEditRow;)V
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    return-void
.end method


# virtual methods
.method public attach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    iput-object p1, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    .line 121
    sget-object v0, Lcom/squareup/noho/NohoEditRow$Side;->END:Lcom/squareup/noho/NohoEditRow$Side;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setAlignment(Lcom/squareup/noho/NohoEditRow$Side;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->watcher:Lcom/squareup/noho/FlexibleLabelPlugin$watcher$1;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public description(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    const-string v0, "editDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->description(Lcom/squareup/noho/NohoEditRow$Plugin;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public detach(Lcom/squareup/noho/NohoEditRow;)V
    .locals 1

    const-string v0, "editText"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->watcher:Lcom/squareup/noho/FlexibleLabelPlugin$watcher$1;

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 127
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->detach(Lcom/squareup/noho/NohoEditRow$Plugin;Lcom/squareup/noho/NohoEditRow;)V

    return-void
.end method

.method public focusChanged()V
    .locals 0

    .line 98
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->focusChanged(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    return-void
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->text:Ljava/lang/String;

    return-object v0
.end method

.method public isClickable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public measure(Landroid/graphics/Rect;)Lcom/squareup/noho/NohoEditRow$PluginSize;
    .locals 5

    const-string v0, "editRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result p1

    .line 135
    iget-object v0, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    const-string v1, "editText"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getOriginalPaddingLeft()I

    move-result v0

    sub-int/2addr p1, v0

    .line 136
    iget-object v0, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditRow;->getOriginalPaddingRight()I

    move-result v0

    sub-int/2addr p1, v0

    .line 137
    iget v0, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->marginWithText:I

    sub-int/2addr p1, v0

    int-to-float v0, p1

    .line 138
    iget v2, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->maxWidthPercentage:F

    mul-float v0, v0, v2

    float-to-int v0, v0

    .line 140
    iget-object v2, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v2, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    if-eqz v2, :cond_4

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v2, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v2}, Lcom/squareup/noho/NohoEditRow;->getHint()Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v2, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v2}, Lcom/squareup/noho/NohoEditRow;->getText()Landroid/text/Editable;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    :goto_2
    if-eqz v2, :cond_8

    .line 141
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_8
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_9

    goto :goto_4

    :cond_9
    const-string v2, ""

    .line 142
    :goto_4
    iget-object v4, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->editText:Lcom/squareup/noho/NohoEditRow;

    if-nez v4, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v4}, Lcom/squareup/noho/NohoEditRow;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    const-string v4, "editText.paint"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/squareup/noho/CanvasExtensionsKt;->textWidth(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v1

    sub-int/2addr p1, v1

    .line 146
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 149
    iget-object v0, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->paint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->text:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/noho/CanvasExtensionsKt;->textWidth(Landroid/text/TextPaint;Ljava/lang/String;)I

    move-result v0

    if-gt v0, p1, :cond_b

    iget-object p1, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->text:Ljava/lang/String;

    goto :goto_5

    .line 150
    :cond_b
    iget-object v0, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->text:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->paint:Landroid/text/TextPaint;

    int-to-float p1, p1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v1, p1, v2}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 149
    :goto_5
    iput-object p1, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->displayText:Ljava/lang/String;

    .line 154
    new-instance p1, Lcom/squareup/noho/NohoEditRow$PluginSize;

    sget-object v0, Lcom/squareup/noho/NohoEditRow$Side;->START:Lcom/squareup/noho/NohoEditRow$Side;

    iget v1, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->marginWithText:I

    invoke-direct {p1, v0, v3, v1}, Lcom/squareup/noho/NohoEditRow$PluginSize;-><init>(Lcom/squareup/noho/NohoEditRow$Side;II)V

    return-object p1
.end method

.method public onClick()Z
    .locals 1

    .line 98
    invoke-static {p0}, Lcom/squareup/noho/NohoEditRow$Plugin$DefaultImpls;->onClick(Lcom/squareup/noho/NohoEditRow$Plugin;)Z

    move-result v0

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;Lcom/squareup/noho/NohoEditRow$DrawingInfo;)V
    .locals 3

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "drawingInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getPluginRect()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    .line 168
    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditRow$DrawingInfo;->getPluginRect()Landroid/graphics/Rect;

    move-result-object p2

    invoke-virtual {p2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result p2

    .line 169
    iget-object v1, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->displayText:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/noho/FlexibleLabelPlugin;->paint:Landroid/text/TextPaint;

    invoke-static {p1, v1, v0, p2, v2}, Lcom/squareup/noho/CanvasExtensionsKt;->drawTextCenteredAt(Landroid/graphics/Canvas;Ljava/lang/String;FFLandroid/text/TextPaint;)V

    return-void
.end method
