.class public final Lcom/squareup/core/location/providers/LocationManagerKt;
.super Ljava/lang/Object;
.source "LocationManager.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLocationManager.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LocationManager.kt\ncom/squareup/core/location/providers/LocationManagerKt\n*L\n1#1,78:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u001a\u0012\u0010\u0007\u001a\u00020\u0008*\u00020\t2\u0006\u0010\n\u001a\u00020\u0002\u001a\u0014\u0010\u000b\u001a\u00020\u0008*\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0002\u001a\u0014\u0010\u000c\u001a\u00020\u0008*\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0003\u001a\u0012\u0010\r\u001a\u00020\u0008*\u00020\t2\u0006\u0010\n\u001a\u00020\u0002\u001a\u0014\u0010\u000e\u001a\u00020\u0008*\u00020\t2\u0006\u0010\n\u001a\u00020\u0003H\u0002\u001a\u0014\u0010\u000f\u001a\u00020\u0008*\u00020\t2\u0006\u0010\n\u001a\u00020\u0003H\u0003\"\u001a\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\"\u0016\u0010\u0004\u001a\n \u0006*\u0004\u0018\u00010\u00050\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "listeners",
        "",
        "Lcom/squareup/core/location/providers/NMEAEventListener;",
        "",
        "nmeaThreadExecutor",
        "Ljava/util/concurrent/ExecutorService;",
        "kotlin.jvm.PlatformType",
        "addNmeaListener",
        "",
        "Landroid/location/LocationManager;",
        "listener",
        "addNmeaListener21",
        "addNmeaListener24",
        "removeNmeaListener",
        "removeNmeaListener21",
        "removeNmeaListener24",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final listeners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/core/location/providers/NMEAEventListener;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final nmeaThreadExecutor:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    sput-object v0, Lcom/squareup/core/location/providers/LocationManagerKt;->listeners:Ljava/util/Map;

    const-string v0, "NMEAListenerThread"

    .line 14
    invoke-static {v0}, Lcom/squareup/thread/executor/Executors;->newSingleThreadExecutor(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/squareup/core/location/providers/LocationManagerKt;->nmeaThreadExecutor:Ljava/util/concurrent/ExecutorService;

    return-void
.end method

.method public static final synthetic access$getNmeaThreadExecutor$p()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .line 1
    sget-object v0, Lcom/squareup/core/location/providers/LocationManagerKt;->nmeaThreadExecutor:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public static final addNmeaListener(Landroid/location/LocationManager;Lcom/squareup/core/location/providers/NMEAEventListener;)V
    .locals 2

    const-string v0, "$this$addNmeaListener"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    invoke-static {p0, p1}, Lcom/squareup/core/location/providers/LocationManagerKt;->addNmeaListener24(Landroid/location/LocationManager;Lcom/squareup/core/location/providers/NMEAEventListener;)V

    goto :goto_0

    .line 25
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/core/location/providers/LocationManagerKt;->addNmeaListener21(Landroid/location/LocationManager;Lcom/squareup/core/location/providers/NMEAEventListener;)V

    :goto_0
    return-void
.end method

.method private static final addNmeaListener21(Landroid/location/LocationManager;Lcom/squareup/core/location/providers/NMEAEventListener;)V
    .locals 5

    .line 30
    new-instance v0, Lcom/squareup/core/location/providers/LocationManagerKt$addNmeaListener21$realListener$1;

    invoke-direct {v0, p1}, Lcom/squareup/core/location/providers/LocationManagerKt$addNmeaListener21$realListener$1;-><init>(Lcom/squareup/core/location/providers/NMEAEventListener;)V

    check-cast v0, Landroid/location/GpsStatus$NmeaListener;

    .line 35
    sget-object v1, Lcom/squareup/core/location/providers/LocationManagerKt;->listeners:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    const-class p1, Landroid/location/LocationManager;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    const-class v3, Landroid/location/GpsStatus$NmeaListener;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "addNmeaListener"

    invoke-virtual {p1, v3, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object p1

    const-string v2, "LocationManager::class.j\u2026NmeaListener::class.java)"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v4

    .line 39
    invoke-virtual {p1, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static final addNmeaListener24(Landroid/location/LocationManager;Lcom/squareup/core/location/providers/NMEAEventListener;)V
    .locals 2

    .line 45
    new-instance v0, Lcom/squareup/core/location/providers/LocationManagerKt$addNmeaListener24$realListener$1;

    invoke-direct {v0, p1}, Lcom/squareup/core/location/providers/LocationManagerKt$addNmeaListener24$realListener$1;-><init>(Lcom/squareup/core/location/providers/NMEAEventListener;)V

    check-cast v0, Landroid/location/OnNmeaMessageListener;

    .line 50
    sget-object v1, Lcom/squareup/core/location/providers/LocationManagerKt;->listeners:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    invoke-virtual {p0, v0}, Landroid/location/LocationManager;->addNmeaListener(Landroid/location/OnNmeaMessageListener;)Z

    return-void
.end method

.method public static final removeNmeaListener(Landroid/location/LocationManager;Lcom/squareup/core/location/providers/NMEAEventListener;)V
    .locals 2

    const-string v0, "$this$removeNmeaListener"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    sget-object v0, Lcom/squareup/core/location/providers/LocationManagerKt;->listeners:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 63
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    invoke-static {p0, p1}, Lcom/squareup/core/location/providers/LocationManagerKt;->removeNmeaListener24(Landroid/location/LocationManager;Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :cond_0
    invoke-static {p0, p1}, Lcom/squareup/core/location/providers/LocationManagerKt;->removeNmeaListener21(Landroid/location/LocationManager;Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private static final removeNmeaListener21(Landroid/location/LocationManager;Ljava/lang/Object;)V
    .locals 5

    .line 70
    const-class v0, Landroid/location/LocationManager;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    const-class v3, Landroid/location/GpsStatus$NmeaListener;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "removeNmeaListener"

    invoke-virtual {v0, v3, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const-string v2, "LocationManager::class.j\u2026NmeaListener::class.java)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v4

    .line 71
    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static final removeNmeaListener24(Landroid/location/LocationManager;Ljava/lang/Object;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 76
    check-cast p1, Landroid/location/OnNmeaMessageListener;

    invoke-virtual {p0, p1}, Landroid/location/LocationManager;->removeNmeaListener(Landroid/location/OnNmeaMessageListener;)V

    return-void

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type android.location.OnNmeaMessageListener"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
