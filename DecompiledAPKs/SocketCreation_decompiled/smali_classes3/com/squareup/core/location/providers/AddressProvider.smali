.class public interface abstract Lcom/squareup/core/location/providers/AddressProvider;
.super Ljava/lang/Object;
.source "AddressProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/core/location/providers/AddressProvider$Result;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\nJ\u0018\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0018\u0010\u0007\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00040\u00032\u0006\u0010\u0008\u001a\u00020\tH&\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/core/location/providers/AddressProvider;",
        "",
        "getAddress",
        "Lio/reactivex/Single;",
        "Lcom/squareup/core/location/providers/AddressProvider$Result;",
        "location",
        "Landroid/location/Location;",
        "getAddressFromName",
        "name",
        "",
        "Result",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getAddress(Landroid/location/Location;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/Location;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/core/location/providers/AddressProvider$Result;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAddressFromName(Ljava/lang/String;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Lcom/squareup/core/location/providers/AddressProvider$Result;",
            ">;"
        }
    .end annotation
.end method
