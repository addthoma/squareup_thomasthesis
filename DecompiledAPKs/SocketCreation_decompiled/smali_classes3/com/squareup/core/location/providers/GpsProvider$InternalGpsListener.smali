.class Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;
.super Ljava/lang/Object;
.source "GpsProvider.java"

# interfaces
.implements Landroid/location/LocationListener;
.implements Lcom/squareup/core/location/providers/NMEAEventListener$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/core/location/providers/GpsProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalGpsListener"
.end annotation


# static fields
.field private static final FUTURE_THRESHOLD_MS:I = 0xea60


# instance fields
.field private nmeaTime:Ljava/util/Calendar;

.field final synthetic this$0:Lcom/squareup/core/location/providers/GpsProvider;


# direct methods
.method private constructor <init>(Lcom/squareup/core/location/providers/GpsProvider;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->this$0:Lcom/squareup/core/location/providers/GpsProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/core/location/providers/GpsProvider;Lcom/squareup/core/location/providers/GpsProvider$1;)V
    .locals 0

    .line 87
    invoke-direct {p0, p1}, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;-><init>(Lcom/squareup/core/location/providers/GpsProvider;)V

    return-void
.end method

.method private inTheFuture(Landroid/location/Location;)Z
    .locals 6

    .line 113
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iget-object p1, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->this$0:Lcom/squareup/core/location/providers/GpsProvider;

    .line 114
    invoke-static {p1}, Lcom/squareup/core/location/providers/GpsProvider;->access$500(Lcom/squareup/core/location/providers/GpsProvider;)Lcom/squareup/util/Clock;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v2

    const-wide/32 v4, 0xea60

    add-long/2addr v2, v4

    cmp-long p1, v0, v2

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public gotNMEADate(Ljava/util/Calendar;)V
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->nmeaTime:Ljava/util/Calendar;

    return-void
.end method

.method public onLocationChanged(Landroid/location/Location;)V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 93
    invoke-static {p1}, Lcom/squareup/core/location/Locations;->getLocationString(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "GPS location update: %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    invoke-direct {p0, p1}, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->inTheFuture(Landroid/location/Location;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->nmeaTime:Ljava/util/Calendar;

    if-eqz v1, :cond_1

    new-array v2, v0, [Ljava/lang/Object;

    .line 95
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    aput-object v1, v2, v3

    const-string v1, "GPS fix from the future, adjust to NMEA: %s"

    invoke-static {v1, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    iget-object v1, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->this$0:Lcom/squareup/core/location/providers/GpsProvider;

    invoke-static {v1}, Lcom/squareup/core/location/providers/GpsProvider;->access$100(Lcom/squareup/core/location/providers/GpsProvider;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->this$0:Lcom/squareup/core/location/providers/GpsProvider;

    invoke-static {v1}, Lcom/squareup/core/location/providers/GpsProvider;->access$300(Lcom/squareup/core/location/providers/GpsProvider;)Lcom/squareup/analytics/Analytics;

    move-result-object v1

    new-instance v3, Lcom/squareup/core/location/providers/GpsProvider$WrongLocationTimestampEvent;

    invoke-direct {v3, v2}, Lcom/squareup/core/location/providers/GpsProvider$WrongLocationTimestampEvent;-><init>(Lcom/squareup/core/location/providers/GpsProvider$1;)V

    invoke-interface {v1, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 98
    iget-object v1, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->this$0:Lcom/squareup/core/location/providers/GpsProvider;

    invoke-static {v1, v0}, Lcom/squareup/core/location/providers/GpsProvider;->access$102(Lcom/squareup/core/location/providers/GpsProvider;Z)Z

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->nmeaTime:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setTime(J)V

    .line 105
    iput-object v2, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->nmeaTime:Ljava/util/Calendar;

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->this$0:Lcom/squareup/core/location/providers/GpsProvider;

    invoke-static {v0}, Lcom/squareup/core/location/providers/GpsProvider;->access$400(Lcom/squareup/core/location/providers/GpsProvider;)Landroid/location/LocationListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/location/LocationListener;->onLocationChanged(Landroid/location/Location;)V

    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->this$0:Lcom/squareup/core/location/providers/GpsProvider;

    invoke-static {v0}, Lcom/squareup/core/location/providers/GpsProvider;->access$400(Lcom/squareup/core/location/providers/GpsProvider;)Landroid/location/LocationListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/location/LocationListener;->onProviderDisabled(Ljava/lang/String;)V

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->this$0:Lcom/squareup/core/location/providers/GpsProvider;

    invoke-static {v0}, Lcom/squareup/core/location/providers/GpsProvider;->access$400(Lcom/squareup/core/location/providers/GpsProvider;)Landroid/location/LocationListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/location/LocationListener;->onProviderEnabled(Ljava/lang/String;)V

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/core/location/providers/GpsProvider$InternalGpsListener;->this$0:Lcom/squareup/core/location/providers/GpsProvider;

    invoke-static {v0}, Lcom/squareup/core/location/providers/GpsProvider;->access$400(Lcom/squareup/core/location/providers/GpsProvider;)Landroid/location/LocationListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/location/LocationListener;->onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V

    return-void
.end method
