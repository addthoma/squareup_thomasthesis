.class Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;
.super Ljava/lang/Object;
.source "BaseLocationMonitor.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/core/location/monitors/BaseLocationMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalLocationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;


# direct methods
.method private constructor <init>(Lcom/squareup/core/location/monitors/BaseLocationMonitor;)V
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/core/location/monitors/BaseLocationMonitor;Lcom/squareup/core/location/monitors/BaseLocationMonitor$1;)V
    .locals 0

    .line 141
    invoke-direct {p0, p1}, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;-><init>(Lcom/squareup/core/location/monitors/BaseLocationMonitor;)V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 7

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 143
    invoke-static {p1}, Lcom/squareup/core/location/Locations;->getLocationString(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "received location: %s"

    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v4, 0x2

    if-eqz v1, :cond_5

    const/4 v5, -0x1

    .line 147
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v6, "network"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x1

    goto :goto_0

    :sswitch_1
    const-string v6, "fused"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x3

    goto :goto_0

    :sswitch_2
    const-string v6, "gps"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    :sswitch_3
    const-string v6, "passive"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x2

    :cond_0
    :goto_0
    if-eqz v5, :cond_4

    if-eq v5, v0, :cond_3

    if-eq v5, v4, :cond_2

    if-eq v5, v2, :cond_1

    .line 161
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Location from unknown provider: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-void

    .line 158
    :cond_1
    iget-object v1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    invoke-static {v1, p1}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->access$402(Lcom/squareup/core/location/monitors/BaseLocationMonitor;Landroid/location/Location;)Landroid/location/Location;

    goto :goto_1

    .line 155
    :cond_2
    iget-object v1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    invoke-static {v1, p1}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->access$302(Lcom/squareup/core/location/monitors/BaseLocationMonitor;Landroid/location/Location;)Landroid/location/Location;

    goto :goto_1

    .line 152
    :cond_3
    iget-object v1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    invoke-static {v1, p1}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->access$202(Lcom/squareup/core/location/monitors/BaseLocationMonitor;Landroid/location/Location;)Landroid/location/Location;

    goto :goto_1

    .line 149
    :cond_4
    iget-object v1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    invoke-static {v1, p1}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->access$102(Lcom/squareup/core/location/monitors/BaseLocationMonitor;Landroid/location/Location;)Landroid/location/Location;

    .line 168
    :cond_5
    :goto_1
    iget-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    iget-object v1, p1, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->locationComparer:Lcom/squareup/core/location/comparer/LocationComparer;

    const/4 v5, 0x5

    new-array v5, v5, [Landroid/location/Location;

    iget-object v6, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    iget-object v6, v6, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->currentLocation:Landroid/location/Location;

    aput-object v6, v5, v3

    iget-object v3, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    .line 169
    invoke-static {v3}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->access$100(Lcom/squareup/core/location/monitors/BaseLocationMonitor;)Landroid/location/Location;

    move-result-object v3

    aput-object v3, v5, v0

    iget-object v0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    invoke-static {v0}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->access$200(Lcom/squareup/core/location/monitors/BaseLocationMonitor;)Landroid/location/Location;

    move-result-object v0

    aput-object v0, v5, v4

    iget-object v0, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    invoke-static {v0}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->access$300(Lcom/squareup/core/location/monitors/BaseLocationMonitor;)Landroid/location/Location;

    move-result-object v0

    aput-object v0, v5, v2

    const/4 v0, 0x4

    iget-object v2, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    .line 170
    invoke-static {v2}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->access$400(Lcom/squareup/core/location/monitors/BaseLocationMonitor;)Landroid/location/Location;

    move-result-object v2

    aput-object v2, v5, v0

    .line 169
    invoke-virtual {v1, v5}, Lcom/squareup/core/location/comparer/LocationComparer;->getBestLocation([Landroid/location/Location;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->currentLocation:Landroid/location/Location;

    .line 173
    iget-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    iget-object p1, p1, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->currentLocation:Landroid/location/Location;

    if-eqz p1, :cond_7

    .line 174
    iget-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    invoke-virtual {p1}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->isSingleShot()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 175
    iget-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    invoke-virtual {p1}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->removeUpdates()V

    .line 178
    :cond_6
    iget-object p1, p0, Lcom/squareup/core/location/monitors/BaseLocationMonitor$InternalLocationListener;->this$0:Lcom/squareup/core/location/monitors/BaseLocationMonitor;

    iget-object v0, p1, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->currentLocation:Landroid/location/Location;

    invoke-virtual {p1, v0}, Lcom/squareup/core/location/monitors/BaseLocationMonitor;->notifyLocationChanged(Landroid/location/Location;)V

    :cond_7
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x2f3590d9 -> :sswitch_3
        0x190aa -> :sswitch_2
        0x5d44923 -> :sswitch_1
        0x6de15a2e -> :sswitch_0
    .end sparse-switch
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onProviderDisabled: %s"

    .line 194
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "onProviderEnabled: %s"

    .line 190
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 2

    if-nez p2, :cond_0

    const-string p3, "Out of Service"

    goto :goto_0

    :cond_0
    const-string p3, "Available"

    :goto_0
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    const-string p3, "Temporarily Unavailable"

    :cond_1
    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p3, p2, v1

    aput-object p1, p2, v0

    const-string p1, "status changed: %s for provider %s"

    .line 186
    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
