.class public final Lcom/squareup/marketfont/MarketSpanKt;
.super Ljava/lang/Object;
.source "MarketSpan.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMarketSpan.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MarketSpan.kt\ncom/squareup/marketfont/MarketSpanKt\n*L\n1#1,19:1\n18#1:20\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u001a%\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u0087\u0008\u00a8\u0006\u0008"
    }
    d2 = {
        "marketSpanFor",
        "Lcom/squareup/fonts/FontSpan;",
        "context",
        "Landroid/content/Context;",
        "weight",
        "Lcom/squareup/marketfont/MarketFont$Weight;",
        "typefaceStyle",
        "",
        "marketfont_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final marketSpanFor(Landroid/content/Context;)Lcom/squareup/fonts/FontSpan;
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x6

    invoke-static {p0, v0, v1, v2, v0}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor$default(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;IILjava/lang/Object;)Lcom/squareup/fonts/FontSpan;

    move-result-object p0

    return-object p0
.end method

.method public static final marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {p0, p1, v0, v1, v2}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor$default(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;IILjava/lang/Object;)Lcom/squareup/fonts/FontSpan;

    move-result-object p0

    return-object p0
.end method

.method public static final marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;I)Lcom/squareup/fonts/FontSpan;
    .locals 1

    const-string v0, "context"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "weight"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/squareup/fonts/FontSpan;

    invoke-static {p1, p2}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result p1

    invoke-direct {v0, p0, p1}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public static synthetic marketSpanFor$default(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;IILjava/lang/Object;)Lcom/squareup/fonts/FontSpan;
    .locals 0

    and-int/lit8 p4, p3, 0x2

    if-eqz p4, :cond_0

    .line 16
    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    const-string p4, "Weight.DEFAULT"

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    and-int/lit8 p3, p3, 0x4

    if-eqz p3, :cond_1

    const/4 p2, 0x0

    :cond_1
    const-string p3, "context"

    .line 17
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo p3, "weight"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    new-instance p3, Lcom/squareup/fonts/FontSpan;

    invoke-static {p1, p2}, Lcom/squareup/marketfont/MarketFont$Weight;->resourceIdFor(Lcom/squareup/marketfont/MarketFont$Weight;I)I

    move-result p1

    invoke-direct {p3, p0, p1}, Lcom/squareup/fonts/FontSpan;-><init>(Landroid/content/Context;I)V

    return-object p3
.end method
