.class Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "BarcodeNotFoundScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/BarcodeNotFoundScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/BarcodeNotFoundView;",
        ">;"
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/ui/BarcodeNotFoundScreen;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 39
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->flow:Lflow/Flow;

    .line 41
    iput-object p2, p0, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public synthetic lambda$onLoad$0$BarcodeNotFoundScreen$Presenter()V
    .locals 4

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/BarcodeNotFoundScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$BarcodeNotFoundScreen$Presenter(Landroid/view/View;)V
    .locals 0

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 45
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/BarcodeNotFoundScreen;

    iput-object p1, p0, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->screen:Lcom/squareup/ui/BarcodeNotFoundScreen;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/BarcodeNotFoundView;

    invoke-virtual {p1}, Lcom/squareup/ui/BarcodeNotFoundView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    .line 50
    new-instance v0, Lcom/squareup/ui/-$$Lambda$BarcodeNotFoundScreen$Presenter$m5_M8H9XCrsxWiipWPESdYz4QGw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/-$$Lambda$BarcodeNotFoundScreen$Presenter$m5_M8H9XCrsxWiipWPESdYz4QGw;-><init>(Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 51
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->sku_not_found_actionbar:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/BarcodeNotFoundView;

    iget-object v0, p0, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->sku_not_found_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->screen:Lcom/squareup/ui/BarcodeNotFoundScreen;

    .line 53
    invoke-static {v1}, Lcom/squareup/ui/BarcodeNotFoundScreen;->access$000(Lcom/squareup/ui/BarcodeNotFoundScreen;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "sku"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 52
    invoke-virtual {p1, v0}, Lcom/squareup/ui/BarcodeNotFoundView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 55
    invoke-virtual {p0}, Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/BarcodeNotFoundView;

    new-instance v0, Lcom/squareup/ui/-$$Lambda$BarcodeNotFoundScreen$Presenter$VtGYh_L9O9gtN91OkuJ0-itwh3I;

    invoke-direct {v0, p0}, Lcom/squareup/ui/-$$Lambda$BarcodeNotFoundScreen$Presenter$VtGYh_L9O9gtN91OkuJ0-itwh3I;-><init>(Lcom/squareup/ui/BarcodeNotFoundScreen$Presenter;)V

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/BarcodeNotFoundView;->setOnClickListenerOnOkButton(Landroid/view/View$OnClickListener;)V

    return-void
.end method
