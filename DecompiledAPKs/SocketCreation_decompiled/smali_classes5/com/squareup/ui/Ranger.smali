.class public final Lcom/squareup/ui/Ranger;
.super Ljava/lang/Object;
.source "Ranger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/Ranger$Builder;,
        Lcom/squareup/ui/Ranger$Range;,
        Lcom/squareup/ui/Ranger$RowType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Enum;",
        ":",
        "Lcom/squareup/ui/Ranger$RowType<",
        "TH;>;H:",
        "Ljava/lang/Enum;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final count:I

.field private final ranges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/Ranger$Range<",
            "TR;>;>;"
        }
    .end annotation
.end field

.field private final rangesByRowType:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "TR;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/Ranger$Range<",
            "TR;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/Map;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/Ranger$Range<",
            "TR;>;>;",
            "Ljava/util/Map<",
            "TR;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/Ranger$Range<",
            "TR;>;>;>;I)V"
        }
    .end annotation

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/Ranger;->ranges:Ljava/util/List;

    .line 116
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/Ranger;->rangesByRowType:Ljava/util/Map;

    .line 117
    iput p3, p0, Lcom/squareup/ui/Ranger;->count:I

    return-void
.end method

.method private static checkBounds(II)V
    .locals 3

    if-ltz p0, :cond_0

    if-ge p0, p1, :cond_0

    return-void

    .line 201
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attempted to use index "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, " on size "

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public containsRowType(Ljava/lang/Enum;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)Z"
        }
    .end annotation

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/Ranger;->rangesByRowType:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public count()I
    .locals 1

    .line 196
    iget v0, p0, Lcom/squareup/ui/Ranger;->count:I

    return v0
.end method

.method public getHolderType(I)Ljava/lang/Enum;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TH;"
        }
    .end annotation

    .line 136
    invoke-virtual {p0, p1}, Lcom/squareup/ui/Ranger;->getRowType(I)Ljava/lang/Enum;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/Ranger$RowType;

    invoke-interface {p1}, Lcom/squareup/ui/Ranger$RowType;->getHolderType()Ljava/lang/Enum;

    move-result-object p1

    return-object p1
.end method

.method public getRowType(I)Ljava/lang/Enum;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TR;"
        }
    .end annotation

    .line 124
    iget v0, p0, Lcom/squareup/ui/Ranger;->count:I

    invoke-static {p1, v0}, Lcom/squareup/ui/Ranger;->checkBounds(II)V

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/Ranger;->ranges:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/Ranger$Range;

    .line 127
    iget v2, v1, Lcom/squareup/ui/Ranger$Range;->endExclusive:I

    if-ge p1, v2, :cond_0

    .line 128
    iget-object p1, v1, Lcom/squareup/ui/Ranger$Range;->rangeType:Ljava/lang/Enum;

    return-object p1

    .line 131
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v1, v2

    const/4 p1, 0x1

    iget-object v2, p0, Lcom/squareup/ui/Ranger;->ranges:Ljava/util/List;

    aput-object v2, v1, p1

    const-string p1, "Could not find index %d in ranges %s"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getRowTypeCountUpTo(Ljava/lang/Enum;I)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;I)I"
        }
    .end annotation

    .line 156
    iget v0, p0, Lcom/squareup/ui/Ranger;->count:I

    invoke-static {p2, v0}, Lcom/squareup/ui/Ranger;->checkBounds(II)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/Ranger;->rangesByRowType:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 160
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/Ranger$Range;

    .line 161
    iget v2, v1, Lcom/squareup/ui/Ranger$Range;->startInclusive:I

    if-gt v2, p2, :cond_0

    add-int/lit8 v2, p2, 0x1

    .line 162
    iget v3, v1, Lcom/squareup/ui/Ranger$Range;->endExclusive:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget v1, v1, Lcom/squareup/ui/Ranger$Range;->startInclusive:I

    sub-int/2addr v2, v1

    add-int/2addr v0, v2

    goto :goto_0

    :cond_0
    return v0
.end method

.method public indexOfRowType(Ljava/lang/Enum;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;I)I"
        }
    .end annotation

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/Ranger;->rangesByRowType:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    if-eqz p1, :cond_1

    .line 177
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/Ranger$Range;

    .line 178
    iget v1, v0, Lcom/squareup/ui/Ranger$Range;->endExclusive:I

    if-ge p2, v1, :cond_0

    .line 179
    iget p1, v0, Lcom/squareup/ui/Ranger$Range;->startInclusive:I

    invoke-static {p2, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    return p1

    :cond_1
    const/4 p1, -0x1

    return p1
.end method
