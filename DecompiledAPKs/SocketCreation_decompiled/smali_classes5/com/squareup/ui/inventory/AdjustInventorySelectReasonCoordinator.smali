.class public Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AdjustInventorySelectReasonCoordinator.java"


# instance fields
.field private final adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

.field private helpText:Lcom/squareup/widgets/MessageView;

.field private reasonList:Landroid/view/ViewGroup;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/inventory/AdjustInventoryController;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->res:Lcom/squareup/util/Res;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 87
    sget v0, Lcom/squareup/adjustinventory/R$id;->reason_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->reasonList:Landroid/view/ViewGroup;

    .line 88
    sget v0, Lcom/squareup/adjustinventory/R$id;->adjust_inventory_select_reason_help_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->helpText:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private getActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 4

    .line 78
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 79
    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 80
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/inventory/-$$Lambda$261IpqP_Zi1NK4Eim_FGD3ndFwM;

    invoke-direct {v3, v2}, Lcom/squareup/ui/inventory/-$$Lambda$261IpqP_Zi1NK4Eim_FGD3ndFwM;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    .line 81
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 83
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic lambda$6BCJadWfRcX9uaLFwabN-JI8hL8(Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySelectReasonScreenData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->populateReasonRows(Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySelectReasonScreenData;)V

    return-void
.end method

.method private populateReasonRows(Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySelectReasonScreenData;)V
    .locals 3

    .line 62
    iget-object p1, p1, Lcom/squareup/ui/inventory/AdjustInventoryController$AdjustInventorySelectReasonScreenData;->reasons:Ljava/util/List;

    .line 63
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    .line 64
    iget-object v1, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->reasonList:Landroid/view/ViewGroup;

    invoke-static {v1}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForLayout(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v1

    .line 65
    iget v2, v0, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;->labelResId:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(I)V

    .line 66
    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->setInventoryReasonOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;)V

    .line 67
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->reasonList:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setInventoryReasonOnClickListener(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;)V
    .locals 1

    .line 73
    new-instance v0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySelectReasonCoordinator$kkxfD_8fQG_j8kSTI9wYVgEVqfk;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySelectReasonCoordinator$kkxfD_8fQG_j8kSTI9wYVgEVqfk;-><init>(Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;)V

    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    .line 39
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->bindViews(Landroid/view/View;)V

    .line 42
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->getActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->helpText:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/adjustinventory/R$string;->adjust_stock_help_text:I

    const-string v3, "dashboard"

    .line 46
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->dashboard_items_library_url:I

    .line 47
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->square_dashboard:I

    .line 48
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/inventory/-$$Lambda$261IpqP_Zi1NK4Eim_FGD3ndFwM;

    invoke-direct {v1, v0}, Lcom/squareup/ui/inventory/-$$Lambda$261IpqP_Zi1NK4Eim_FGD3ndFwM;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryController;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 55
    new-instance v0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySelectReasonCoordinator$DcIfrh2HHNyHKqKCB6nnrosll3w;

    invoke-direct {v0, p0}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySelectReasonCoordinator$DcIfrh2HHNyHKqKCB6nnrosll3w;-><init>(Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$AdjustInventorySelectReasonCoordinator()Lrx/Subscription;
    .locals 2

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    invoke-virtual {v0}, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventorySelectReasonScreenData()Lrx/Observable;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lrx/Observable;->first()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySelectReasonCoordinator$6BCJadWfRcX9uaLFwabN-JI8hL8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySelectReasonCoordinator$6BCJadWfRcX9uaLFwabN-JI8hL8;-><init>(Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;)V

    .line 58
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$setInventoryReasonOnClickListener$1$AdjustInventorySelectReasonCoordinator(Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;Landroid/view/View;)V
    .locals 0

    .line 73
    iget-object p2, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;->adjustInventoryController:Lcom/squareup/ui/inventory/AdjustInventoryController;

    .line 74
    invoke-virtual {p2, p1}, Lcom/squareup/ui/inventory/AdjustInventoryController;->adjustInventoryReasonRowClicked(Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;)V

    return-void
.end method
