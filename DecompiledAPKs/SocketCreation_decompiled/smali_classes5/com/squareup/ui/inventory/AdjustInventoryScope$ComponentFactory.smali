.class public Lcom/squareup/ui/inventory/AdjustInventoryScope$ComponentFactory;
.super Ljava/lang/Object;
.source "AdjustInventoryScope.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/inventory/AdjustInventoryScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 0

    .line 77
    const-class p2, Lcom/squareup/ui/inventory/AdjustInventoryScope$ParentComponent;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/inventory/AdjustInventoryScope$ParentComponent;

    invoke-interface {p1}, Lcom/squareup/ui/inventory/AdjustInventoryScope$ParentComponent;->adjustInventoryComponent()Lcom/squareup/ui/inventory/AdjustInventoryScope$Component;

    move-result-object p1

    return-object p1
.end method
