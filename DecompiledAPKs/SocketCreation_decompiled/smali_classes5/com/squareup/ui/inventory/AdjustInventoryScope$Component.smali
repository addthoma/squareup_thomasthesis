.class public interface abstract Lcom/squareup/ui/inventory/AdjustInventoryScope$Component;
.super Ljava/lang/Object;
.source "AdjustInventoryScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/inventory/AdjustInventoryScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract adjustInventoryController()Lcom/squareup/ui/inventory/AdjustInventoryController;
.end method

.method public abstract getAdjustInventoryHost()Lcom/squareup/ui/inventory/AdjustInventoryHost;
.end method

.method public abstract getAdjustInventorySelectReasonCoordinator()Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;
.end method

.method public abstract getAdjustInventorySpecifyNumberCoordinator()Lcom/squareup/ui/inventory/AdjustInventorySpecifyNumberCoordinator;
.end method
