.class public final Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;
.super Lcom/squareup/ui/inventory/InAdjustInventoryScope;
.source "AdjustInventorySelectReasonScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySelectReasonScreen$dU0LgxQkdXYk4qWMevvitvWPvYw;->INSTANCE:Lcom/squareup/ui/inventory/-$$Lambda$AdjustInventorySelectReasonScreen$dU0LgxQkdXYk4qWMevvitvWPvYw;

    .line 35
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/ui/inventory/InAdjustInventoryScope;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;
    .locals 1

    .line 36
    const-class v0, Lcom/squareup/ui/inventory/AdjustInventoryScope;

    .line 37
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/inventory/AdjustInventoryScope;

    .line 38
    new-instance v0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;-><init>(Lcom/squareup/ui/inventory/AdjustInventoryScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;->adjustInventoryScope:Lcom/squareup/ui/inventory/AdjustInventoryScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 13
    invoke-virtual {p0, p1}, Lcom/squareup/ui/inventory/AdjustInventorySelectReasonScreen;->provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;
    .locals 1

    .line 26
    const-class v0, Lcom/squareup/ui/inventory/AdjustInventoryScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/inventory/AdjustInventoryScope$Component;

    .line 27
    invoke-interface {p1}, Lcom/squareup/ui/inventory/AdjustInventoryScope$Component;->getAdjustInventorySelectReasonCoordinator()Lcom/squareup/ui/inventory/AdjustInventorySelectReasonCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 22
    sget v0, Lcom/squareup/adjustinventory/R$layout;->adjust_inventory_select_reason:I

    return v0
.end method
