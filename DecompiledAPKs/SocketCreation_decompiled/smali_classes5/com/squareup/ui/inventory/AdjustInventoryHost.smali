.class public interface abstract Lcom/squareup/ui/inventory/AdjustInventoryHost;
.super Ljava/lang/Object;
.source "AdjustInventoryHost.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/inventory/AdjustInventoryHost$InventoryAdjustmentUpdate;,
        Lcom/squareup/ui/inventory/AdjustInventoryHost$InventoryAssignmentRequest;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001:\u0002\u0011\u0012J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&J\u0008\u0010\n\u001a\u00020\u000bH&J*\u0010\u000c\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0008\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u0007H&\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/inventory/AdjustInventoryHost;",
        "",
        "deferInitialStockAdjustment",
        "",
        "variationId",
        "",
        "receivedStockCount",
        "Ljava/math/BigDecimal;",
        "receivedUnitCost",
        "Lcom/squareup/protos/common/Money;",
        "shouldDeferInitialStockAdjustment",
        "",
        "updateInventoryStockCountAfterAdjustment",
        "reason",
        "Lcom/squareup/protos/client/InventoryAdjustmentReason;",
        "adjustment",
        "newStockCountAfterAdjustment",
        "InventoryAdjustmentUpdate",
        "InventoryAssignmentRequest",
        "adjust-inventory_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract deferInitialStockAdjustment(Ljava/lang/String;Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V
.end method

.method public abstract shouldDeferInitialStockAdjustment()Z
.end method

.method public abstract updateInventoryStockCountAfterAdjustment(Ljava/lang/String;Lcom/squareup/protos/client/InventoryAdjustmentReason;Ljava/math/BigDecimal;Ljava/math/BigDecimal;)V
.end method
