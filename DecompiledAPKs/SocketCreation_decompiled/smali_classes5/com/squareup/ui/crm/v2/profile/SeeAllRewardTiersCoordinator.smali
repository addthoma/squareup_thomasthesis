.class public final Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SeeAllRewardTiersCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B7\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0008\u0010\u0015\u001a\u00020\u0016H\u0002J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0010\u0010\u001c\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u001eH\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;",
        "res",
        "Lcom/squareup/util/Res;",
        "rewardAdapterHelper",
        "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "rewardRecyclerViewHelper",
        "Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;",
        "(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "rewardTiersAdapter",
        "Lcom/squareup/loyalty/ui/RewardAdapter;",
        "applyMultipleCoupons",
        "",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "detach",
        "getSpendablePoints",
        "",
        "statusPoints",
        "crm-screens_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final res:Lcom/squareup/util/Res;

.field private final rewardAdapterHelper:Lcom/squareup/loyalty/ui/RewardAdapterHelper;

.field private final rewardRecyclerViewHelper:Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

.field private final rewardTiersAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

.field private final runner:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rewardAdapterHelper"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rewardRecyclerViewHelper"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->rewardAdapterHelper:Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->features:Lcom/squareup/settings/server/Features;

    iput-object p6, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->rewardRecyclerViewHelper:Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

    .line 40
    new-instance p1, Lcom/squareup/loyalty/ui/RewardAdapter;

    invoke-direct {p1}, Lcom/squareup/loyalty/ui/RewardAdapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->rewardTiersAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

    return-void
.end method

.method public static final synthetic access$getRewardAdapterHelper$p(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)Lcom/squareup/loyalty/ui/RewardAdapterHelper;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->rewardAdapterHelper:Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    return-object p0
.end method

.method public static final synthetic access$getRewardRecyclerViewHelper$p(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->rewardRecyclerViewHelper:Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

    return-object p0
.end method

.method public static final synthetic access$getRewardTiersAdapter$p(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)Lcom/squareup/loyalty/ui/RewardAdapter;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->rewardTiersAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;
    .locals 0

    .line 28
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$getSpendablePoints(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;I)I
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->getSpendablePoints(I)I

    move-result p0

    return p0
.end method

.method private final applyMultipleCoupons()Z
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 105
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 106
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_see_all_loyalty_tiers_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method private final getSpendablePoints(I)I
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->canRedeemPoints()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 45
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->bindViews(Landroid/view/View;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 47
    :cond_0
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 49
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 50
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmscreens/R$string;->crm_loyalty_program_section_all_loyalty_rewards:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 48
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 52
    new-instance v2, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$1;-><init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 53
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 55
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$2;-><init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->applyMultipleCoupons()Z

    move-result v0

    const-string v1, "recyclerView"

    if-eqz v0, :cond_3

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->rewardRecyclerViewHelper:Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v2}, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;->createRecyclerFor(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;->loyaltyStatus()Lrx/Observable;

    move-result-object v1

    .line 62
    sget-object v2, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$3;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_2

    new-instance v3, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$sam$rx_functions_Func1$0;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$sam$rx_functions_Func1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v2, v3

    :cond_2
    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    .line 63
    new-instance v2, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$4;-><init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)V

    check-cast v2, Lrx/functions/Func1;

    invoke-virtual {v1, v2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v1

    const-string v2, "runner.loyaltyStatus()\n \u2026            )\n          }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    new-instance v2, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$5;

    invoke-direct {v2, v0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$5;-><init>(Lcom/squareup/cycler/Recycler;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, p1, v2}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    goto :goto_0

    .line 77
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->rewardTiersAdapter:Lcom/squareup/loyalty/ui/RewardAdapter;

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v1, Lcom/squareup/noho/NohoEdgeDecoration;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string/jumbo v3, "view.resources"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;->loyaltyStatus()Lrx/Observable;

    move-result-object v0

    .line 82
    sget-object v1, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$6;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$6;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    if-eqz v1, :cond_6

    new-instance v2, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$sam$rx_functions_Func1$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$sam$rx_functions_Func1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v1, v2

    :cond_6
    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 83
    new-instance v1, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$7;-><init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)V

    check-cast v1, Lrx/functions/Func1;

    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    const-string v1, "runner.loyaltyStatus()\n \u2026rewardTier) }\n          }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    new-instance v1, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator$attach$8;-><init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    :goto_0
    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    return-void
.end method
