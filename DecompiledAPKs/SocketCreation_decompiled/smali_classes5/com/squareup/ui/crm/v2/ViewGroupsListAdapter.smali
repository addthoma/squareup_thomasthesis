.class Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "ViewGroupsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;,
        Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$HeaderViewHolder;,
        Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;,
        Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field static final VIEW_TYPE_GROUP:I = 0x1

.field static final VIEW_TYPE_HEADER:I


# instance fields
.field private coordinator:Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;",
            ">;",
            "Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;",
            ")V"
        }
    .end annotation

    .line 83
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->items:Ljava/util/List;

    .line 85
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->coordinator:Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;)Ljava/util/List;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->items:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;)Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;
    .locals 0

    .line 18
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->coordinator:Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;

    return-object p0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;

    iget p1, p1, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;->type:I

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->onBindViewHolder(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;I)V
    .locals 0

    .line 110
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;->bind(I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$AbstractViewHolder;
    .locals 1

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 103
    new-instance p2, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 105
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string/jumbo p2, "viewType"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 101
    :cond_1
    new-instance p2, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$HeaderViewHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$HeaderViewHolder;-><init>(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;Landroid/view/ViewGroup;)V

    return-object p2
.end method

.method updateItems(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$Item;",
            ">;)V"
        }
    .end annotation

    .line 89
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->items:Ljava/util/List;

    .line 90
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->notifyDataSetChanged()V

    return-void
.end method
