.class public final Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;
.super Ljava/lang/Object;
.source "ConversationDetailCoordinatorV2_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;",
        ">;"
    }
.end annotation


# instance fields
.field private final billHistoryFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/BillHistoryFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final customerConversationTokenHolderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/BillHistoryFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->customerConversationTokenHolderProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->billHistoryFlowProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->resProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->rolodexProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/BillHistoryFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;Lcom/squareup/util/Device;Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;
    .locals 8

    .line 63
    new-instance v7, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;-><init>(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;Lcom/squareup/util/Device;Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->customerConversationTokenHolderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->billHistoryFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/crm/flow/BillHistoryFlow;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->rolodexProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/crm/RolodexServiceHelper;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->newInstance(Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;Lcom/squareup/util/Device;Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2_Factory;->get()Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;

    move-result-object v0

    return-object v0
.end method
