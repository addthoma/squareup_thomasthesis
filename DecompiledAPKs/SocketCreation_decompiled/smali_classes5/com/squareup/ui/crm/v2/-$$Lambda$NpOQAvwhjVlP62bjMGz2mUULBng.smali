.class public final synthetic Lcom/squareup/ui/crm/v2/-$$Lambda$NpOQAvwhjVlP62bjMGz2mUULBng;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final synthetic f$0:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/-$$Lambda$NpOQAvwhjVlP62bjMGz2mUULBng;->f$0:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/-$$Lambda$NpOQAvwhjVlP62bjMGz2mUULBng;->f$0:Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;

    check-cast p1, Ljava/lang/String;

    invoke-interface {v0, p1}, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;->getAppointmentSummaryData(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    check-cast p1, Lio/reactivex/ObservableSource;

    return-object p1
.end method
