.class public interface abstract Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;
.super Ljava/lang/Object;
.source "LoyaltySectionBottomDialogScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract canViewExpiringPoints()Z
.end method

.method public abstract enterAdjustPointsFlow()V
.end method

.method public abstract isForTransferringLoyalty()Z
.end method

.method public abstract showChooseContactForTransfer()V
.end method

.method public abstract showDeleteLoyaltyAccount()V
.end method

.method public abstract showSendLoyaltyStatusScreen()V
.end method

.method public abstract showTransferLoyaltyAccount(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Contact;)V
.end method

.method public abstract showUpdateLoyaltyPhoneScreen()V
.end method

.method public abstract viewExpiringPoints()V
.end method
