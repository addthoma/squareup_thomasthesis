.class Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;
.super Lmortar/ViewPresenter;
.source "ChooseEnumAttributeScreenV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;",
        ">;"
    }
.end annotation


# static fields
.field private static final SELECTIONS_KEY:Ljava/lang/String; = "SELECTIONS"


# instance fields
.field private final definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Runner;

.field private final selections:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Runner;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 61
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 59
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->selections:Ljava/util/HashSet;

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Runner;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    .line 65
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Runner;->getEnumAttribute()Lcom/squareup/ui/crm/rows/EnumAttribute;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/ui/crm/rows/EnumAttribute;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    return-void
.end method


# virtual methods
.method getAttributeFromSelections()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 4

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->selections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 121
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 122
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 123
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->selections:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 124
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    invoke-static {v1}, Lcom/squareup/crm/util/RolodexContactHelper;->toAttributeBuilder(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;-><init>()V

    .line 130
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->enum_values(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object v0

    .line 129
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$0$ChooseEnumAttributeScreenV2$Presenter()V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Runner;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->getAttributeFromSelections()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Runner;->closeChooseEnumAttributeScreen(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V

    return-void
.end method

.method onEnumReselected(I)V
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->selections:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method onEnumSelected(I)V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_allow_multiple:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->selections:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->selections:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 69
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;

    .line 71
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v3, v3, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_allow_multiple:Ljava/lang/Boolean;

    .line 72
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmupdatecustomer/R$string;->crm_choose_many:I

    .line 73
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmupdatecustomer/R$string;->crm_choose_one:I

    .line 74
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 72
    :goto_0
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ChooseEnumAttributeScreenV2$Presenter$GIbwXrG9yIjgGpiLE_0jiq1XL7Y;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ChooseEnumAttributeScreenV2$Presenter$GIbwXrG9yIjgGpiLE_0jiq1XL7Y;-><init>(Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;)V

    .line 75
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 77
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 71
    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 79
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_allow_multiple:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->setSingleChoiceMode(Z)V

    .line 81
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->addEnumOptions(Ljava/util/List;)V

    if-nez p1, :cond_1

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->selections:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Runner;->getEnumAttribute()Lcom/squareup/ui/crm/rows/EnumAttribute;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/ui/crm/rows/EnumAttribute;->selections:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 87
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->selections:Ljava/util/HashSet;

    const-string v2, "SELECTIONS"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :goto_1
    const/4 p1, 0x0

    .line 90
    :goto_2
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_3

    .line 91
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->selections:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->definition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 92
    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeViewV2;->setCheckedOption(I)V

    :cond_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    :cond_3
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 98
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ChooseEnumAttributeScreenV2$Presenter;->selections:Ljava/util/HashSet;

    const-string v1, "SELECTIONS"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-void
.end method
