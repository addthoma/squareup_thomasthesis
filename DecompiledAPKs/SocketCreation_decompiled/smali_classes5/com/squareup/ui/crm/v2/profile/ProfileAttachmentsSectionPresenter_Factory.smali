.class public final Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;
.super Ljava/lang/Object;
.source "ProfileAttachmentsSectionPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final attachmentLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexAttachmentLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final timeFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexAttachmentLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->attachmentLoaderProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->dateFormatterProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->timeFormatterProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p7, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p8, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p9, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexAttachmentLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;"
        }
    .end annotation

    .line 71
    new-instance v10, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexAttachmentLoader;Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;
    .locals 11

    .line 78
    new-instance v10, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexAttachmentLoader;Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;
    .locals 10

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->attachmentLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/crm/RolodexAttachmentLoader;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->dateFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->timeFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/analytics/Analytics;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexAttachmentLoader;Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/util/Res;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/util/Device;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter_Factory;->get()Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;

    move-result-object v0

    return-object v0
.end method
