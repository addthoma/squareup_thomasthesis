.class public Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;
.super Ljava/lang/Object;
.source "ManageCouponsAndRewardsFlow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Runner;


# instance fields
.field private active:Z

.field private couponItems:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;",
            ">;>;"
        }
    .end annotation
.end field

.field private crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

.field private final flow:Lflow/Flow;

.field private loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final result:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->create(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->couponItems:Lrx/subjects/BehaviorSubject;

    .line 52
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 53
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->result:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->flow:Lflow/Flow;

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private complete()V
    .locals 2

    const/4 v0, 0x0

    .line 91
    iput-boolean v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->active:Z

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->couponItems:Lrx/subjects/BehaviorSubject;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public cancelManageCouponsAndRewards()V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 110
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->complete()V

    return-void
.end method

.method public closeVoidingCouponsScreen()V
    .locals 4

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public confirmVoid()V
    .locals 5

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method public couponClicked(Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;)V
    .locals 1

    .line 114
    iget-boolean v0, p1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->checked:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p1, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->checked:Z

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->couponItems:Lrx/subjects/BehaviorSubject;

    invoke-virtual {p1}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public getConfirmCopy()Ljava/lang/String;
    .locals 3

    .line 130
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->getCouponsToBeVoided()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_coupons_and_rewards_void_confirm_single:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 134
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_coupons_and_rewards_void_confirm_plural:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "number"

    .line 135
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCouponsToBeVoided()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 146
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->couponItems:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v1}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;

    .line 147
    iget-boolean v3, v2, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->checked:Z

    if-eqz v3, :cond_0

    .line 148
    iget-object v2, v2, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    iget-object v2, v2, Lcom/squareup/protos/client/coupons/Coupon;->coupon_id:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getHoldsCoupons()Lcom/squareup/checkout/HoldsCoupons;
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 76
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$0$ManageCouponsAndRewardsFlow(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)V
    .locals 4

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 67
    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->coupon:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/coupons/Coupon;

    .line 69
    new-instance v2, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;-><init>(Lcom/squareup/protos/client/coupons/Coupon;Z)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->couponItems:Lrx/subjects/BehaviorSubject;

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public manageCouponsAndRewardsScreenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->couponItems:Lrx/subjects/BehaviorSubject;

    sget-object v1, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$d4M4QSOMYYUCEm1bcB73Tq1nSw0;->INSTANCE:Lcom/squareup/ui/crm/v2/flow/-$$Lambda$d4M4QSOMYYUCEm1bcB73Tq1nSw0;

    invoke-virtual {v0, v1}, Lrx/subjects/BehaviorSubject;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 61
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$ManageCouponsAndRewardsFlow$rm1kbya-hB5NNMSQuDRn9W5SRuo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/flow/-$$Lambda$ManageCouponsAndRewardsFlow$rm1kbya-hB5NNMSQuDRn9W5SRuo;-><init>(Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;)V

    .line 64
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 62
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 8

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "active"

    .line 166
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->active:Z

    .line 167
    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->active:Z

    if-eqz v0, :cond_3

    .line 170
    sget-object v0, Lcom/squareup/protos/client/coupons/Coupon;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "coupons"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProtos(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    const-string v1, "couponsChecked"

    .line 172
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    .line 173
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 174
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 175
    new-instance v4, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    goto :goto_1

    :cond_1
    const/4 v7, 0x0

    :goto_1
    invoke-direct {v4, v5, v7}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;-><init>(Lcom/squareup/protos/client/coupons/Coupon;Z)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 177
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->couponItems:Lrx/subjects/BehaviorSubject;

    invoke-virtual {p1, v1}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :cond_3
    return-void
.end method

.method public onResult()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->result:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 5

    .line 182
    iget-boolean v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->active:Z

    if-eqz v0, :cond_1

    const-string v1, "active"

    .line 183
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 185
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 186
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 187
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->couponItems:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v2}, Lrx/subjects/BehaviorSubject;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;

    .line 188
    iget-object v4, v3, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    iget-boolean v3, v3, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$CouponItem;->checked:Z

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v2, "couponsChecked"

    .line 192
    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 193
    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Ljava/util/List;)[B

    move-result-object v0

    const-string v1, "coupons"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    :cond_1
    return-void
.end method

.method public showConfirmVoid()V
    .locals 3

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/loyalty/ConfirmVoidCouponDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showFirstScreen(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)V
    .locals 2

    const/4 v0, 0x1

    .line 83
    iput-boolean v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->active:Z

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public success()V
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->result:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 156
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->complete()V

    return-void
.end method
