.class public Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "ChooseCustomerToSaveScreenV2.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Component;,
        Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Presenter;,
        Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2$Runner;
    }
.end annotation


# instance fields
.field private final parentPath:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2;->parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CHOOSE_CUSTOMER:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomerToSaveScreenV2;->parentPath:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 60
    sget v0, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_choose_customer_to_save_card_view:I

    return v0
.end method
