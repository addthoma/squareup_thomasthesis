.class public final Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;
.super Ljava/lang/Object;
.source "ViewCustomerCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerSummaryDataRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final cardOnFileViewDataRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final errorBarPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final frequentItemsSectionDataRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesSectionDataRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final notesSectionDataRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final personalInformationViewDataRendererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final viewCustomerConfigurationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 76
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 77
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 78
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 79
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 80
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 81
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->clockProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->errorBarPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->personalInformationViewDataRendererProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->cardOnFileViewDataRendererProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->notesSectionDataRendererProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->viewCustomerConfigurationProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->invoicesSectionDataRendererProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->buyerSummaryDataRendererProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->frequentItemsSectionDataRendererProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 112
    new-instance v17, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v17
.end method

.method public static newInstance(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/util/Device;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Clock;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;)Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;
    .locals 18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 126
    new-instance v17, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;-><init>(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/util/Device;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Clock;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;)V

    return-object v17
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;
    .locals 18

    move-object/from16 v0, p0

    .line 96
    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/util/Clock;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->errorBarPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->personalInformationViewDataRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->cardOnFileViewDataRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->notesSectionDataRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->viewCustomerConfigurationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->invoicesSectionDataRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->buyerSummaryDataRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;

    iget-object v1, v0, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->frequentItemsSectionDataRendererProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;

    invoke-static/range {v2 .. v17}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->newInstance(Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/util/Device;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Clock;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/ui/crm/v2/profile/PersonalInformationViewDataRenderer;Lcom/squareup/ui/crm/v2/profile/CardOnFileViewDataRenderer;Lcom/squareup/ui/crm/v2/profile/NotesSectionDataRenderer;Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;Lcom/squareup/ui/crm/v2/profile/InvoicesSectionDataRenderer;Lcom/squareup/ui/crm/v2/profile/BuyerSummaryDataRenderer;Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionDataRenderer;)Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator_Factory;->get()Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator;

    move-result-object v0

    return-object v0
.end method
