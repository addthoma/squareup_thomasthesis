.class public final synthetic Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

.field private final synthetic f$1:Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;

.field private final synthetic f$2:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

.field private final synthetic f$3:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

.field private final synthetic f$4:I


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;->f$0:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;->f$1:Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;

    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;->f$2:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;->f$3:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iput p5, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;->f$4:I

    return-void
.end method


# virtual methods
.method public final invoke()Ljava/lang/Object;
    .locals 5

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;->f$0:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;->f$1:Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;->f$2:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;->f$3:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    iget v4, p0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ContactEditView$ZCqMUVLi-u0w_q-tFhSz8mgY_E8;->f$4:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/ui/crm/v2/profile/ContactEditView;->lambda$addAttributeRow$8$ContactEditView(Lcom/squareup/ui/crm/rows/EditEnumAttributeRow;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;I)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method
