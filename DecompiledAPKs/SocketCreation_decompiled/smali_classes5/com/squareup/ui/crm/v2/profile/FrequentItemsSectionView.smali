.class public Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;
.super Landroid/widget/LinearLayout;
.source "FrequentItemsSectionView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$ViewData;,
        Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$Component;
    }
.end annotation


# instance fields
.field formatter:Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;

.field private onViewAllClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private rows:Landroid/widget/LinearLayout;

.field private sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

.field private viewAllButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const-class p2, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$Component;->inject(Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;)V

    .line 43
    new-instance p2, Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->res:Lcom/squareup/util/Res;

    invoke-direct {p2, v0}, Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;-><init>(Lcom/squareup/util/Res;)V

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->formatter:Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;

    .line 45
    sget p2, Lcom/squareup/crmviewcustomer/R$layout;->crm_frequent_items_section_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->setOrientation(I)V

    return-void
.end method

.method private bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/FrequentItem;)V
    .locals 1

    .line 102
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/FrequentItem;->display_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->formatter:Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;->getFrequentItemText(Lcom/squareup/protos/client/rolodex/FrequentItem;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private bindViews()V
    .locals 3

    .line 83
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_frequent_items_section_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    .line 84
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_frequent_items_section_rows:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->rows:Landroid/widget/LinearLayout;

    .line 85
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_frequent_items_section_view_all_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->viewAllButton:Landroid/view/View;

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_frequent_items_heading_uppercase:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setTitle(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->viewAllButton:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->onViewAllClicked:Lrx/Observable;

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 0

    .line 50
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->bindViews()V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 56
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onViewAllClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->onViewAllClicked:Lrx/Observable;

    return-object v0
.end method

.method public setViewData(Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$ViewData;)V
    .locals 4

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->viewAllButton:Landroid/view/View;

    iget-boolean v1, p1, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$ViewData;->showAll:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$ViewData;->frequentItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setDividerVisible(Z)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->sectionHeader:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    const/4 v1, 0x0

    sget-object v3, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->GONE:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    invoke-virtual {v0, v1, v3}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setActionButton(Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;)V

    .line 73
    iget-object p1, p1, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView$ViewData;->frequentItems:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/FrequentItem;

    .line 74
    sget v1, Lcom/squareup/crmviewcustomer/R$layout;->crm_frequent_item_section_row:I

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-static {v1, v3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 75
    sget-object v3, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v1, v3}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 76
    invoke-virtual {v1, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 77
    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/FrequentItem;)V

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/FrequentItemsSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method setVisible(Z)V
    .locals 0

    .line 60
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
