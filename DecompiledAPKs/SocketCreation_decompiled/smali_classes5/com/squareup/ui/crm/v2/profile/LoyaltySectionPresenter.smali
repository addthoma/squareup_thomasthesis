.class public final Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;
.super Lmortar/ViewPresenter;
.source "LoyaltySectionPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLoyaltySectionPresenter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LoyaltySectionPresenter.kt\ncom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter\n+ 2 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n+ 3 RxKotlin.kt\ncom/squareup/util/rx2/RxKotlinKt\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,325:1\n19#2:326\n19#2:328\n19#2:329\n505#3:327\n250#4,2:330\n*E\n*S KotlinDebug\n*F\n+ 1 LoyaltySectionPresenter.kt\ncom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter\n*L\n90#1:326\n109#1:328\n139#1:329\n90#1:327\n273#1,2:330\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0098\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 72\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u00017BY\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0002\u0010\u0017J\u0006\u0010\u0018\u001a\u00020\u0019J\u0018\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0018\u0010 \u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010!\u001a\u00020\"H\u0002J \u0010#\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\u0019H\u0002J\u0018\u0010\'\u001a\u00020\u001b2\u0006\u0010(\u001a\u00020\u00022\u0006\u0010)\u001a\u00020*H\u0002J \u0010+\u001a\u00020\u001b2\u0006\u0010(\u001a\u00020\u00022\u0006\u0010)\u001a\u00020*2\u0006\u0010&\u001a\u00020\u0019H\u0002J\u0010\u0010,\u001a\u00020\u001b2\u0006\u0010(\u001a\u00020\u0002H\u0002J\u0012\u0010-\u001a\u00020\u001b2\u0008\u0010.\u001a\u0004\u0018\u00010/H\u0014J\u0018\u00100\u001a\u00020\u001b2\u0006\u0010(\u001a\u00020\u00022\u0006\u00101\u001a\u000202H\u0002J\u0013\u00103\u001a\u0004\u0018\u000104*\u000205H\u0002\u00a2\u0006\u0002\u00106R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00068"
    }
    d2 = {
        "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;",
        "Lmortar/ViewPresenter;",
        "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;",
        "phoneHelper",
        "Lcom/squareup/text/PhoneNumberHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "runner",
        "Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;",
        "loyaltySettings",
        "Lcom/squareup/loyalty/LoyaltySettings;",
        "pointsTermsFormatter",
        "Lcom/squareup/loyalty/PointsTermsFormatter;",
        "rewardAdapterHelper",
        "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
        "dateFormat",
        "Ljava/text/DateFormat;",
        "locale",
        "Ljava/util/Locale;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "rewardRecyclerViewHelper",
        "Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;",
        "(Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;)V",
        "applyMultipleCoupons",
        "",
        "bindLifetimePointsRow",
        "",
        "row",
        "Lcom/squareup/ui/crm/rows/ProfileLineRow;",
        "lifetimePoints",
        "",
        "bindMembersSinceRow",
        "enrolledAt",
        "Lcom/squareup/protos/common/time/DateTime;",
        "bindPhoneRow",
        "mapping",
        "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
        "writeEnabled",
        "bindPointsRow",
        "view",
        "status",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;",
        "handleLoyaltyAccount",
        "handleWritableNullState",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "updateView",
        "data",
        "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;",
        "spendablePoints",
        "",
        "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;",
        "(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;)Ljava/lang/Integer;",
        "Companion",
        "crm-view-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$Companion;

# The value of this static final field might be set in the static constructor
.field private static final REWARD_TIERS_MAX:I = 0x3


# instance fields
.field private final dateFormat:Ljava/text/DateFormat;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final locale:Ljava/util/Locale;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final rewardAdapterHelper:Lcom/squareup/loyalty/ui/RewardAdapterHelper;

.field private final rewardRecyclerViewHelper:Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

.field private final runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->Companion:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$Companion;

    const/4 v0, 0x3

    .line 322
    sput v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->REWARD_TIERS_MAX:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;)V
    .locals 1
    .param p7    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/LongForm;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "phoneHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "runner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySettings"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pointsTermsFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rewardAdapterHelper"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormat"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rewardRecyclerViewHelper"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    iput-object p6, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->rewardAdapterHelper:Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    iput-object p7, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->dateFormat:Ljava/text/DateFormat;

    iput-object p8, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->locale:Ljava/util/Locale;

    iput-object p9, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->features:Lcom/squareup/settings/server/Features;

    iput-object p10, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->rewardRecyclerViewHelper:Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

    return-void
.end method

.method public static final synthetic access$getLoyaltySettings$p(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/loyalty/LoyaltySettings;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    return-object p0
.end method

.method public static final synthetic access$getREWARD_TIERS_MAX$cp()I
    .locals 1

    .line 42
    sget v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->REWARD_TIERS_MAX:I

    return v0
.end method

.method public static final synthetic access$getRewardAdapterHelper$p(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/loyalty/ui/RewardAdapterHelper;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->rewardAdapterHelper:Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    return-object p0
.end method

.method public static final synthetic access$getRewardRecyclerViewHelper$p(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->rewardRecyclerViewHelper:Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;
    .locals 0

    .line 42
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    return-object p0
.end method

.method public static final synthetic access$getView(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;
    .locals 0

    .line 42
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    return-object p0
.end method

.method public static final synthetic access$spendablePoints(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;)Ljava/lang/Integer;
    .locals 0

    .line 42
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->spendablePoints(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$updateView(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->updateView(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;)V

    return-void
.end method

.method private final bindLifetimePointsRow(Lcom/squareup/ui/crm/rows/ProfileLineRow;J)V
    .locals 4

    .line 248
    new-instance v0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    .line 249
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_lifetime_points:I

    invoke-virtual {v1, v2}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v1

    .line 250
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 252
    sget v3, Lcom/squareup/crmscreens/R$string;->crm_points_earned_format:I

    .line 250
    invoke-virtual {v2, p2, p3, v3}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(JI)Ljava/lang/String;

    move-result-object p2

    .line 254
    sget-object p3, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    .line 248
    invoke-direct {v0, v1, p2, p3}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    .line 247
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/rows/ProfileLineRow;->setInfo(Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;)V

    return-void
.end method

.method private final bindMembersSinceRow(Lcom/squareup/ui/crm/rows/ProfileLineRow;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 4

    .line 235
    new-instance v0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    .line 236
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_member_since:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 237
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->dateFormat:Ljava/text/DateFormat;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->locale:Ljava/util/Locale;

    invoke-static {p2, v3}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    const-string v2, "dateFormat.format(ProtoT\u2026Date(enrolledAt, locale))"

    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 238
    sget-object v2, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    .line 235
    invoke-direct {v0, v1, p2, v2}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    .line 234
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/rows/ProfileLineRow;->setInfo(Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;)V

    return-void
.end method

.method private final bindPhoneRow(Lcom/squareup/ui/crm/rows/ProfileLineRow;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Z)V
    .locals 4

    if-eqz p3, :cond_0

    .line 198
    sget-object v0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT_LINK:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    goto :goto_0

    .line 200
    :cond_0
    sget-object v0, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->TEXT:Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    .line 203
    :goto_0
    new-instance v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;

    .line 204
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_program_phone:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 205
    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    iget-object p2, p2, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->raw_id:Ljava/lang/String;

    invoke-interface {v3, p2}, Lcom/squareup/text/PhoneNumberHelper;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-nez p2, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 203
    :cond_1
    invoke-direct {v1, v2, p2, v0}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;)V

    .line 202
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/rows/ProfileLineRow;->setInfo(Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;)V

    .line 209
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    .line 210
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->getView()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    const-string/jumbo v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$bindPhoneRow$1;

    invoke-direct {v0, p0, p3}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$bindPhoneRow$1;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;Z)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, p2, v0}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    return-void
.end method

.method private final bindPointsRow(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;)V
    .locals 2

    .line 218
    invoke-virtual {p2}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->lifetimePoints()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCurrentPoints()I

    move-result v0

    if-nez v0, :cond_1

    const/4 p2, 0x0

    .line 219
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setPoints(Ljava/lang/String;)V

    goto :goto_1

    .line 222
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 223
    invoke-virtual {p2}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCurrentPoints()I

    move-result p2

    .line 224
    sget v1, Lcom/squareup/crmscreens/R$string;->crm_points_earned_format:I

    .line 222
    invoke-virtual {v0, p2, v1}, Lcom/squareup/loyalty/PointsTermsFormatter;->points(II)Ljava/lang/String;

    move-result-object p2

    .line 221
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setPoints(Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method private final handleLoyaltyAccount(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;Z)V
    .locals 5

    .line 264
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->clearRows()V

    if-eqz p3, :cond_0

    .line 267
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->enableHeaderAction()V

    goto :goto_0

    .line 269
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->disableHeaderAction()V

    .line 276
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getResponse()Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->mappings:Ljava/util/List;

    const-string/jumbo v1, "view.addRow()"

    if-eqz v0, :cond_4

    check-cast v0, Ljava/lang/Iterable;

    .line 330
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 274
    iget-object v3, v3, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    sget-object v4, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;->TYPE_PHONE:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    if-ne v3, v4, :cond_2

    const/4 v3, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_1

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    .line 331
    :goto_2
    check-cast v2, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    if-eqz v2, :cond_4

    .line 277
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->addRow()Lcom/squareup/ui/crm/rows/ProfileLineRow;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v2, p3}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->bindPhoneRow(Lcom/squareup/ui/crm/rows/ProfileLineRow;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Z)V

    .line 281
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->bindPointsRow(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;)V

    .line 285
    invoke-virtual {p2}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getResponse()Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    move-result-object p3

    iget-object p3, p3, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    iget-object p3, p3, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->enrolled_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz p3, :cond_5

    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->addRow()Lcom/squareup/ui/crm/rows/ProfileLineRow;

    move-result-object v0

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, p3}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->bindMembersSinceRow(Lcom/squareup/ui/crm/rows/ProfileLineRow;Lcom/squareup/protos/common/time/DateTime;)V

    .line 288
    :cond_5
    invoke-virtual {p2}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->lifetimePoints()Ljava/lang/Integer;

    move-result-object p2

    if-eqz p2, :cond_6

    .line 289
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result p2

    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->addRow()Lcom/squareup/ui/crm/rows/ProfileLineRow;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    int-to-long p2, p2

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->bindLifetimePointsRow(Lcom/squareup/ui/crm/rows/ProfileLineRow;J)V

    :cond_6
    return-void
.end method

.method private final handleWritableNullState(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;)V
    .locals 1

    .line 310
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->clearRows()V

    .line 311
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->enableNullStateHeader()V

    const/4 v0, 0x0

    .line 312
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setSeeAllRewardsVisibility(Z)V

    .line 313
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setRewardTiersItems(Ljava/util/List;)V

    .line 314
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setRewardTiersMultiRedemption(Ljava/util/List;)V

    const/4 v0, 0x0

    .line 315
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setPoints(Ljava/lang/String;)V

    return-void
.end method

.method private final spendablePoints(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;)Ljava/lang/Integer;
    .locals 1

    .line 296
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->getLoyaltyStatusResponse()Lcom/squareup/loyalty/LoyaltyStatusResponse;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    if-eqz v0, :cond_2

    .line 298
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->getLoyaltyStatusResponse()Lcom/squareup/loyalty/LoyaltyStatusResponse;

    move-result-object v0

    check-cast v0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    invoke-virtual {v0}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->lifetimePoints()Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_2

    .line 300
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->getCanRedeemPoints()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 301
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->getLoyaltyStatusResponse()Lcom/squareup/loyalty/LoyaltyStatusResponse;

    move-result-object p1

    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    invoke-virtual {p1}, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;->getCurrentPoints()I

    move-result p1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    .line 300
    :goto_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    return-object p1
.end method

.method private final updateView(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;)V
    .locals 5

    .line 173
    instance-of v0, p2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$Loading;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setVisible(Z)V

    goto :goto_0

    .line 175
    :cond_0
    instance-of v0, p2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    move-object v3, p2

    check-cast v3, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;

    invoke-virtual {v3}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->getLoyaltyStatusResponse()Lcom/squareup/loyalty/LoyaltyStatusResponse;

    move-result-object v4

    instance-of v4, v4, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    if-eqz v4, :cond_1

    .line 176
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setVisible(Z)V

    .line 177
    invoke-virtual {v3}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->getLoyaltyStatusResponse()Lcom/squareup/loyalty/LoyaltyStatusResponse;

    move-result-object p2

    check-cast p2, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;

    invoke-virtual {v3}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->getWriteEnabled()Z

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->handleLoyaltyAccount(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$HasLoyalty;Z)V

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_2

    .line 180
    check-cast p2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;

    invoke-virtual {p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->getLoyaltyStatusResponse()Lcom/squareup/loyalty/LoyaltyStatusResponse;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/loyalty/LoyaltyStatusResponse$GotStatus$NoLoyalty;

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;->getWriteEnabled()Z

    move-result p2

    if-eqz p2, :cond_2

    .line 182
    invoke-virtual {p1, v2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setVisible(Z)V

    .line 183
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->handleWritableNullState(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;)V

    goto :goto_0

    .line 187
    :cond_2
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->setVisible(Z)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final applyMultipleCoupons()Z
    .locals 2

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 57
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ui/crm/flow/CrmScopeTypeKt;->isInvoicePath(Lcom/squareup/ui/crm/flow/CrmScopeType;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 69
    :cond_0
    sget-object p1, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->loyaltyLoadingState()Lrx/Observable;

    move-result-object v0

    const-string v1, "runner.loyaltyLoadingState()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->runner:Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;->loyaltyStatus()Lrx/Observable;

    move-result-object v1

    const-string v2, "runner.loyaltyStatus()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$viewData$1;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$viewData$1;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "runner.loyaltyStatus().t\u2026{ it.toPointsResponse() }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p1, v0, v1}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    .line 73
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$viewData$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$viewData$2;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string/jumbo v0, "viewData"

    .line 86
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    const-string/jumbo v1, "view"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$1;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0, v2}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 89
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->onHeaderActionClicked()Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v2, "view.onHeaderActionClicked()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 326
    const-class v2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;

    invoke-virtual {p1, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "ofType(T::class.java)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/ObservableSource;

    .line 327
    new-instance v4, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$$inlined$withLatestFrom$1;

    invoke-direct {v4}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$$inlined$withLatestFrom$1;-><init>()V

    check-cast v4, Lio/reactivex/functions/BiFunction;

    invoke-virtual {v0, v2, v4}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v2, "withLatestFrom(other, Bi\u2026 combiner.invoke(t, u) })"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    sget-object v2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$3;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$3;

    check-cast v2, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v2, "view.onHeaderActionClick\u2026ilter { it.writeEnabled }"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroid/view/View;

    new-instance v4, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$4;

    invoke-direct {v4, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$4;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2, v4}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;->onSeeAllRewardsClicked()Lio/reactivex/Observable;

    move-result-object v0

    const-string/jumbo v2, "view.onSeeAllRewardsClicked()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Landroid/view/View;

    new-instance v4, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$5;

    invoke-direct {v4, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$5;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2, v4}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 107
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->applyMultipleCoupons()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 328
    const-class v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$6;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$6;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string/jumbo v0, "viewData\n          .ofTy\u2026   .orEmpty()\n          }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$7;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$7;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    goto :goto_0

    .line 329
    :cond_1
    const-class v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$8;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string/jumbo v0, "viewData\n          .ofTy\u2026(emptyList())\n          }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 152
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionView;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$9;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$9;-><init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    :goto_0
    return-void
.end method
