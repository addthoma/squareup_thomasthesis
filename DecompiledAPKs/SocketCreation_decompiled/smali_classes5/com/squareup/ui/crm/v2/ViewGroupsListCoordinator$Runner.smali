.class public interface abstract Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator$Runner;
.super Ljava/lang/Object;
.source "ViewGroupsListCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeViewGroupsListScreen()V
.end method

.method public abstract gotoCreateManualGroupScreen(Z)V
.end method

.method public abstract showGroup(Lcom/squareup/protos/client/rolodex/Group;)V
.end method
