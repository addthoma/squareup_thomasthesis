.class public Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "NoCustomerSelectedDetailCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;,
        Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$Runner;
    }
.end annotation


# instance fields
.field private row1:Lcom/squareup/widgets/MessageView;

.field private row2:Lcom/squareup/widgets/MessageView;

.field private final runner:Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$Runner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 40
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;->runner:Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$Runner;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 63
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_no_customer_selected_row1:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;->row1:Lcom/squareup/widgets/MessageView;

    .line 64
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_no_customer_selected_row2:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;->row2:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public static synthetic lambda$A2yanugfdu2RUZD5yVNcFEtaJ5E(Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;->update(Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;)V

    return-void
.end method

.method private update(Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;)V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;->row1:Lcom/squareup/widgets/MessageView;

    iget-object v1, p1, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;->row1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;->row2:Lcom/squareup/widgets/MessageView;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$ScreenData;->row2:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 45
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;->bindViews(Landroid/view/View;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;->runner:Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$Wpar8A1CcBoz44Ak00ZiR8DYdAc;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$Wpar8A1CcBoz44Ak00ZiR8DYdAc;-><init>(Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$Runner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 52
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$NoCustomerSelectedDetailCoordinator$M8xGd4_H4oSpNr4-OgO7aC_hSHQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$NoCustomerSelectedDetailCoordinator$M8xGd4_H4oSpNr4-OgO7aC_hSHQ;-><init>(Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 0

    .line 59
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$attach$0$NoCustomerSelectedDetailCoordinator()Lrx/Subscription;
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;->runner:Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator$Runner;->noCustomerSelectedScreenData()Lrx/Observable;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$NoCustomerSelectedDetailCoordinator$A2yanugfdu2RUZD5yVNcFEtaJ5E;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$NoCustomerSelectedDetailCoordinator$A2yanugfdu2RUZD5yVNcFEtaJ5E;-><init>(Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;)V

    .line 55
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method
