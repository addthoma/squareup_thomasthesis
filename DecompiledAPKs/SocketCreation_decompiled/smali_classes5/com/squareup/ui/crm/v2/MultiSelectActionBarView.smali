.class public Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;
.super Landroid/widget/LinearLayout;
.source "MultiSelectActionBarView.java"


# instance fields
.field private dropDownClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private xButton:Landroid/view/View;

.field private xClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    sget p2, Lcom/squareup/crm/applet/R$layout;->crm_v2_multiselect_actionbar:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 25
    sget p1, Lcom/squareup/crm/applet/R$id;->crm_multiselect_dropdown_container:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 26
    sget p2, Lcom/squareup/crm/applet/R$id;->crm_multiselect_cancel:I

    invoke-static {p0, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;->xButton:Landroid/view/View;

    .line 27
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;->xButton:Landroid/view/View;

    invoke-static {p2}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;->xClicked:Lrx/Observable;

    .line 28
    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;->dropDownClicked:Lrx/Observable;

    return-void
.end method


# virtual methods
.method public onDropDownClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;->dropDownClicked:Lrx/Observable;

    return-object v0
.end method

.method public onXClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/MultiSelectActionBarView;->xClicked:Lrx/Observable;

    return-object v0
.end method
