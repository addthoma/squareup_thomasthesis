.class public abstract Lcom/squareup/ui/crm/applet/ConversationDetailScope$Module;
.super Ljava/lang/Object;
.source "ConversationDetailScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/ConversationDetailScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract createConversationDetailCoordinatorV2Runner(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAbstractBillHistoryController(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideAddCouponScreenRunner(Lcom/squareup/ui/crm/applet/CustomersAppletScopeRunner;)Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBillHistoryController(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTenderWithCustomerInfoCache(Lcom/squareup/ui/activity/billhistory/RealTenderWithCustomerInfoCache;)Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTransactionsHistoryRefundHelper(Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper;)Lcom/squareup/ui/activity/TransactionsHistoryRefundHelper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
