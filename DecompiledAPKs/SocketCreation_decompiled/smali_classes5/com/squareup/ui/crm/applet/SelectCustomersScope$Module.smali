.class public abstract Lcom/squareup/ui/crm/applet/SelectCustomersScope$Module;
.super Ljava/lang/Object;
.source "SelectCustomersScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/SelectCustomersScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract addCustomersToGroupController(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;)Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract addingCustomersToGroupController(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;)Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract createGroupScreenController(Lcom/squareup/ui/crm/flow/CreateGroupFlow;)Lcom/squareup/ui/crm/cards/CreateGroupScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract deleteCustomersConfirmationScreen(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;)Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract deletingCustomersScreenController(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;)Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract holdsCustomer(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/crm/HoldsCustomer;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract mergeCustomersController(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;)Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract mergingCustomersController(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;)Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract selectLoyaltyPhoneRunner(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;)Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
