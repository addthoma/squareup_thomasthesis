.class public final Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;
.super Ljava/lang/Object;
.source "CustomersDeepLinkHandler.kt"

# interfaces
.implements Lcom/squareup/deeplinks/DeepLinkHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCustomersDeepLinkHandler.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CustomersDeepLinkHandler.kt\ncom/squareup/ui/crm/applet/CustomersDeepLinkHandler\n*L\n1#1,78:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;",
        "Lcom/squareup/deeplinks/DeepLinkHandler;",
        "customersApplet",
        "Lcom/squareup/ui/crm/applet/CustomersApplet;",
        "customerConversationTokenHolder",
        "Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;",
        "(Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;)V",
        "handleExternal",
        "Lcom/squareup/deeplinks/DeepLinkResult;",
        "uri",
        "Landroid/net/Uri;",
        "CustomersHistoryFactory",
        "crm-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final customerConversationTokenHolder:Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

.field private final customersApplet:Lcom/squareup/ui/crm/applet/CustomersApplet;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/applet/CustomersApplet;Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "customersApplet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "customerConversationTokenHolder"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;->customersApplet:Lcom/squareup/ui/crm/applet/CustomersApplet;

    iput-object p2, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;->customerConversationTokenHolder:Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

    return-void
.end method


# virtual methods
.method public handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 9

    const-string/jumbo v0, "uri"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-static {p1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandlerKt;->access$isCustomers$p(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    goto/16 :goto_2

    .line 26
    :cond_0
    invoke-static {p1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandlerKt;->access$getNavigateToTopCustomers$p(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;->customersApplet:Lcom/squareup/ui/crm/applet/CustomersApplet;

    check-cast v0, Lcom/squareup/ui/main/HistoryFactory;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    goto/16 :goto_2

    .line 27
    :cond_1
    invoke-static {p1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandlerKt;->access$getNavigateToFeedback$p(Landroid/net/Uri;)Z

    move-result v0

    const-string v1, "MessageListScreenV2.INSTANCE"

    const-string v2, "AllCustomersMasterScreen.INSTANCE"

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v0, :cond_4

    invoke-static {p1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandlerKt;->access$getConversationToken$p(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_4

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;->customerConversationTokenHolder:Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;

    invoke-static {p1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandlerKt;->access$getConversationToken$p(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/squareup/ui/crm/applet/CustomerConversationTokenHolder;->setConversationToken(Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/squareup/deeplinks/DeepLinkResult;

    .line 30
    new-instance v6, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;

    const/4 v7, 0x3

    new-array v7, v7, [Lcom/squareup/container/LayoutScreen;

    .line 32
    sget-object v8, Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;

    invoke-static {v8, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v8, Lcom/squareup/container/LayoutScreen;

    aput-object v8, v7, v5

    .line 33
    sget-object v2, Lcom/squareup/ui/crm/v2/MessageListScreenV2;->INSTANCE:Lcom/squareup/ui/crm/v2/MessageListScreenV2;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/container/LayoutScreen;

    aput-object v2, v7, v4

    .line 34
    new-instance v1, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;

    invoke-static {p1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandlerKt;->access$getConversationToken$p(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/container/LayoutScreen;

    aput-object v1, v7, v3

    .line 31
    invoke-static {v7}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    .line 30
    invoke-direct {v6, p1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;-><init>(Ljava/util/List;)V

    check-cast v6, Lcom/squareup/ui/main/HistoryFactory;

    .line 29
    invoke-direct {v0, v6}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    move-object p1, v0

    goto :goto_2

    .line 39
    :cond_4
    invoke-static {p1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandlerKt;->access$getNavigateToFeedback$p(Landroid/net/Uri;)Z

    move-result p1

    if-eqz p1, :cond_5

    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    .line 40
    new-instance v0, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;

    new-array v3, v3, [Lcom/squareup/container/LayoutScreen;

    .line 42
    sget-object v6, Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/AllCustomersMasterScreen;

    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v6, Lcom/squareup/container/LayoutScreen;

    aput-object v6, v3, v5

    .line 43
    sget-object v2, Lcom/squareup/ui/crm/v2/MessageListScreenV2;->INSTANCE:Lcom/squareup/ui/crm/v2/MessageListScreenV2;

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/container/LayoutScreen;

    aput-object v2, v3, v4

    .line 41
    invoke-static {v3}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 40
    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler$CustomersHistoryFactory;-><init>(Ljava/util/List;)V

    check-cast v0, Lcom/squareup/ui/main/HistoryFactory;

    .line 39
    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    goto :goto_2

    .line 47
    :cond_5
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    :goto_2
    return-object p1
.end method
