.class public interface abstract Lcom/squareup/ui/crm/applet/ConversationDetailScope$Component;
.super Ljava/lang/Object;
.source "ConversationDetailScope.java"

# interfaces
.implements Lcom/squareup/ui/activity/IssueRefundScope$ParentComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/crm/applet/ConversationDetailScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/applet/ConversationDetailScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract addCouponScreen()Lcom/squareup/ui/crm/cards/AddCouponScreen$Component;
.end method

.method public abstract billHistoryFlow()Lcom/squareup/ui/crm/flow/BillHistoryFlow;
.end method

.method public abstract billHistoryScreen()Lcom/squareup/ui/crm/cards/BillHistoryScreen$Component;
.end method

.method public abstract conversationDetailScreenV2()Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2$Component;
.end method

.method public abstract issueReceiptScreen()Lcom/squareup/ui/activity/IssueReceiptScreen$Component;
.end method

.method public abstract selectGiftReceiptTenderScreen()Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen$Component;
.end method

.method public abstract selectReceiptTenderScreen()Lcom/squareup/ui/activity/SelectReceiptTenderScreen$Component;
.end method

.method public abstract selectRefundTenderScreen()Lcom/squareup/ui/activity/SelectRefundTenderScreen$Component;
.end method
