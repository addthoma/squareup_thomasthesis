.class public final Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;
.super Ljava/lang/Object;
.source "ChooseCustomerScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final chooseCustomerFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumberHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final recentRolodexContactLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexContactLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final updateCustomerFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p3, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p4, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->phoneNumberHelperProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p5, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->chooseCustomerFlowProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p6, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->updateCustomerFlowProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p7, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p8, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p9, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p10, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->rolodexContactLoaderProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p11, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->recentRolodexContactLoaderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/ChooseCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;)",
            "Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;"
        }
    .end annotation

    .line 84
    new-instance v12, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexContactLoader;)Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;
    .locals 13

    .line 92
    new-instance v12, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexContactLoader;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;
    .locals 12

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->phoneNumberHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->chooseCustomerFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/crm/ChooseCustomerFlow;

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->updateCustomerFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->rolodexContactLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/crm/RolodexContactLoader;

    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->recentRolodexContactLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/crm/RolodexContactLoader;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/util/Res;Ljava/util/Locale;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/crm/RolodexContactLoader;Lcom/squareup/crm/RolodexContactLoader;)Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner_Factory;->get()Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    move-result-object v0

    return-object v0
.end method
