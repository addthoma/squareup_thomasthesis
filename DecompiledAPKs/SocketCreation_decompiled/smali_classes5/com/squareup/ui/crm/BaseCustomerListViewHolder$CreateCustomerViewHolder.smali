.class public final Lcom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder;
.super Lcom/squareup/ui/crm/BaseCustomerListViewHolder;
.source "BaseCustomerListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/BaseCustomerListViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreateCustomerViewHolder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBaseCustomerListViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BaseCustomerListViewHolder.kt\ncom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,93:1\n1103#2,7:94\n*E\n*S KotlinDebug\n*F\n+ 1 BaseCustomerListViewHolder.kt\ncom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder\n*L\n66#1,7:94\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder;",
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "button",
        "Landroid/widget/Button;",
        "bind",
        "",
        "item",
        "Lcom/squareup/ui/crm/BaseCustomerListItem;",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final button:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 61
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder;-><init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 62
    sget v0, Lcom/squareup/crmchoosecustomer/R$id;->crm_create_new_customer:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.crm_create_new_customer)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder;->button:Landroid/widget/Button;

    return-void
.end method


# virtual methods
.method public bind(Lcom/squareup/ui/crm/BaseCustomerListItem;)V
    .locals 2

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder;->button:Landroid/widget/Button;

    move-object v1, p1

    check-cast v1, Lcom/squareup/ui/crm/BaseCustomerListItem$CreateCustomerItem;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/BaseCustomerListItem$CreateCustomerItem;->getButtonText()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder;->button:Landroid/widget/Button;

    check-cast v0, Landroid/view/View;

    .line 94
    new-instance v1, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder$bind$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$CreateCustomerViewHolder$bind$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/crm/BaseCustomerListItem;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
