.class public final Lcom/squareup/ui/crm/ChooseCustomerFlowKt;
.super Ljava/lang/Object;
.source "ChooseCustomerFlow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u001a\u000e\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u0002H\u0000\u001a\u000e\u0010\u0003\u001a\u00020\u0002*\u0004\u0018\u00010\u0001H\u0000\u00a8\u0006\u0004"
    }
    d2 = {
        "toChooseCustomerResultKey",
        "Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;",
        "",
        "toInt",
        "crm-choose-customer_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toChooseCustomerResultKey(I)Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;
    .locals 1

    if-ltz p0, :cond_0

    .line 132
    invoke-static {}, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->values()[Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    move-result-object v0

    aget-object p0, v0, p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final toInt(Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;)I
    .locals 0

    if-eqz p0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->ordinal()I

    move-result p0

    goto :goto_0

    :cond_0
    const/4 p0, -0x1

    :goto_0
    return p0
.end method
