.class public final Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$3;
.super Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;
.source "ChooseCustomerCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0007H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/ui/crm/ChooseCustomerCoordinator$attach$3",
        "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;",
        "onScrolled",
        "",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "dx",
        "",
        "dy",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/ChooseCustomerCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$3;->this$0:Lcom/squareup/ui/crm/ChooseCustomerCoordinator;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrolled(Landroidx/recyclerview/widget/RecyclerView;II)V
    .locals 1

    const-string v0, "recyclerView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-super {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;->onScrolled(Landroidx/recyclerview/widget/RecyclerView;II)V

    .line 70
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object p1

    if-eqz p1, :cond_1

    check-cast p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 71
    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->getItemCount()I

    move-result p2

    .line 72
    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result p3

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$3;->this$0:Lcom/squareup/ui/crm/ChooseCustomerCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->access$getRunner$p(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;

    move-result-object v0

    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;->setContactListScrollPosition(I)V

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$3;->this$0:Lcom/squareup/ui/crm/ChooseCustomerCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->access$getRunner$p(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;

    move-result-object p1

    add-int/lit8 p3, p3, 0x32

    if-le p3, p2, :cond_0

    const/4 p2, 0x1

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;->onNearEndOfListChanged(Ljava/lang/Boolean;)V

    return-void

    .line 70
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
