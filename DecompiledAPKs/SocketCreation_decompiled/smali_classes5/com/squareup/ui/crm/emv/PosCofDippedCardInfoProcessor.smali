.class public Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;
.super Ljava/lang/Object;
.source "PosCofDippedCardInfoProcessor.java"

# interfaces
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;
.implements Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;
.implements Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$EmptyNfcListenerOverrider;,
        Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;
    }
.end annotation


# static fields
.field private static final FAKE_CHARGE_AMOUNT_IN_CENTS:I = 0x1


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final clock:Lcom/squareup/util/Clock;

.field private final dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private hasStartedPayment:Z

.field private final nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final onCardInserted:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onDipResult:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

.field private final readerSessionIds:Lcom/squareup/log/ReaderSessionIds;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;Lcom/squareup/log/ReaderSessionIds;Lcom/squareup/cardreader/PaymentCounter;Lcom/squareup/util/Clock;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/cardreader/DippedCardTracker;Lcom/squareup/cardreader/CardReaderHub;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->onCardInserted:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 62
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->onDipResult:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v0, 0x0

    .line 63
    iput-boolean v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->hasStartedPayment:Z

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 70
    iput-object p2, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    .line 71
    iput-object p3, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    .line 72
    iput-object p4, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    .line 73
    iput-object p5, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->clock:Lcom/squareup/util/Clock;

    .line 74
    iput-object p6, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    .line 75
    iput-object p7, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 76
    iput-object p8, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    .line 77
    iput-object p9, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    .line 78
    iput-object p10, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;Lmortar/MortarScope;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->registerScopedListeners(Lmortar/MortarScope;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;)Lcom/squareup/cardreader/dipper/ActiveCardReader;
    .locals 0

    .line 45
    iget-object p0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    return-object p0
.end method

.method private cancelPaymentOnActiveCardReader(Lcom/squareup/cardreader/CardReader;)V
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->reset()V

    .line 158
    invoke-direct {p0}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->unsetCardReaderListeners()V

    if-nez p1, :cond_0

    .line 160
    iget-object p1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object p1

    :cond_0
    if-nez p1, :cond_1

    return-void

    .line 165
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->hasStartedPayment:Z

    if-eqz v0, :cond_2

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderSessionIds;->onEmvPaymentTerminated(Lcom/squareup/cardreader/CardReaderId;)V

    const/4 v0, 0x0

    .line 167
    iput-boolean v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->hasStartedPayment:Z

    .line 169
    :cond_2
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isInPayment()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 170
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->cancelPayment()V

    .line 172
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->unsetActiveCardReader(Lcom/squareup/cardreader/CardReaderId;)Z

    return-void
.end method

.method private registerScopedListeners(Lmortar/MortarScope;)V
    .locals 3

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/ui/NfcProcessor;->registerNfcAuthDelegate(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcAuthDelegate;)V

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    new-instance v1, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$EmptyNfcListenerOverrider;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$EmptyNfcListenerOverrider;-><init>(Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$1;)V

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/NfcProcessor;->registerNfcListener(Lmortar/MortarScope;Lcom/squareup/ui/NfcProcessor$NfcListenerOverrider;)V

    return-void
.end method

.method private setCardReaderListeners()V
    .locals 2

    .line 107
    new-instance v0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;-><init>(Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;)V

    .line 108
    iget-object v1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/CardReaderListeners;->setEmvListener(Lcom/squareup/cardreader/EmvListener;)V

    .line 109
    iget-object v1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/CardReaderListeners;->setPaymentCompletionListener(Lcom/squareup/cardreader/PaymentCompletionListener;)V

    .line 110
    iget-object v1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/CardReaderListeners;->setPinRequestListener(Lcom/squareup/cardreader/PinRequestListener;)V

    return-void
.end method

.method private startPayment()V
    .locals 5

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->hasActiveCardReader()Z

    move-result v0

    const-string v1, "Tried to start payment without an active card reader"

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    .line 132
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    iget-object v1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->nameFetchInfo:Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/emv/CardholderNameProcessor$NameFetchInfo;->setDipPaymentStarted()V

    const/4 v1, 0x1

    .line 138
    iput-boolean v1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->hasStartedPayment:Z

    .line 139
    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    .line 140
    iget-object v2, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    invoke-virtual {v2, v1}, Lcom/squareup/log/ReaderSessionIds;->onEmvPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 142
    iget-object v2, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->paymentCounter:Lcom/squareup/cardreader/PaymentCounter;

    invoke-virtual {v2, v1}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    const-wide/16 v1, 0x1

    .line 143
    iget-object v3, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v3}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v3

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/CardReader;->startPayment(JJ)V

    return-void

    .line 133
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private unsetCardReaderListeners()V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getEmvListener()Lcom/squareup/cardreader/EmvListener;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetEmvListener()V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getPaymentCompletionListener()Lcom/squareup/cardreader/PaymentCompletionListener;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetPaymentCompletionListener()V

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getPinRequestListener()Lcom/squareup/cardreader/PinRequestListener;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$Listener;

    if-eqz v0, :cond_2

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->unsetPinRequestListener()V

    :cond_2
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    const/4 v0, 0x0

    .line 98
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cancel(Lcom/squareup/cardreader/CardReader;)V

    return-void
.end method

.method cancel(Lcom/squareup/cardreader/CardReader;)V
    .locals 1

    .line 102
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cancelPaymentOnActiveCardReader(Lcom/squareup/cardreader/CardReader;)V

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->onDipResult:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->canceled()Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method notifyDipFailure()V
    .locals 2

    .line 219
    invoke-virtual {p0}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cancel()V

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->onDipResult:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->failure()Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method notifyDipSuccess(Lcom/squareup/cardreader/CardInfo;[BLcom/squareup/protos/client/bills/CardData$ReaderType;)V
    .locals 1

    .line 214
    invoke-virtual {p0}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cancel()V

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->onDipResult:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {p1, p2, p3}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->success(Lcom/squareup/cardreader/CardInfo;[BLcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onCardInserted()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->onCardInserted:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onDipResult()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;",
            ">;"
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->onDipResult:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onNfcAuthorizationRequestReceived(Lcom/squareup/ui/NfcAuthData;)V
    .locals 1

    .line 208
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "We should not be configured such that onNfcAuthorizationRequestReceived will be called."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public processDip()V
    .locals 2

    .line 90
    invoke-direct {p0}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->setCardReaderListeners()V

    .line 91
    invoke-direct {p0}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->startPayment()V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReader()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cardreader/DippedCardTracker;->onEmvTransactionCompleted(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 176
    iget-object p1, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->onCardInserted:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 2

    .line 180
    iget-boolean v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->hasStartedPayment:Z

    if-eqz v0, :cond_0

    .line 181
    invoke-virtual {p0}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->notifyDipFailure()V

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->cancel(Lcom/squareup/cardreader/CardReader;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;->dippedCardTracker:Lcom/squareup/cardreader/DippedCardTracker;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/DippedCardTracker;->onCardRemoved(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 188
    new-instance v0, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor$1;-><init>(Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;)V

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
