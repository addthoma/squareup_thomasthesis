.class public Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;
.super Ljava/lang/Object;
.source "CofDippedCardInfoProcessor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DipResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;
    }
.end annotation


# instance fields
.field public final cardInfo:Lcom/squareup/cardreader/CardInfo;

.field public final encyptedCardData:[B

.field public final readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

.field public final state:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/CardInfo;[BLcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    .line 62
    iput-object p2, p0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->encyptedCardData:[B

    .line 63
    iput-object p3, p0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 64
    iput-object p4, p0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->state:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    return-void
.end method

.method public static canceled()Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;
    .locals 3

    .line 89
    new-instance v0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;

    sget-object v1, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->CANCELED:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v2, v2, v1}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;-><init>(Lcom/squareup/cardreader/CardInfo;[BLcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;)V

    return-object v0
.end method

.method public static failure()Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;
    .locals 3

    .line 85
    new-instance v0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;

    sget-object v1, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->FAILURE:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v2, v2, v1}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;-><init>(Lcom/squareup/cardreader/CardInfo;[BLcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;)V

    return-object v0
.end method

.method public static success(Lcom/squareup/cardreader/CardInfo;[BLcom/squareup/protos/client/bills/CardData$ReaderType;)Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;
    .locals 2

    .line 81
    new-instance v0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;

    sget-object v1, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->SUCCESS:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;-><init>(Lcom/squareup/cardreader/CardInfo;[BLcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;)V

    return-object v0
.end method


# virtual methods
.method public isCancel()Z
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->state:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    sget-object v1, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->CANCELED:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isFailure()Z
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->state:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    sget-object v1, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->FAILURE:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isSuccess()Z
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;->state:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    sget-object v1, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;->SUCCESS:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
