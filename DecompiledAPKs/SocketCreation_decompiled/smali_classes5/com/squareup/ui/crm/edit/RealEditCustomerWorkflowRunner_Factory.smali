.class public final Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "RealEditCustomerWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/EditCustomerCompoundViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/EditCustomerCompoundViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/EditCustomerCompoundViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/crm/edit/EditCustomerCompoundViewFactory;Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;-><init>(Lcom/squareup/ui/crm/edit/EditCustomerCompoundViewFactory;Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;Lcom/squareup/ui/main/PosContainer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;
    .locals 3

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/edit/EditCustomerCompoundViewFactory;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;->newInstance(Lcom/squareup/ui/crm/edit/EditCustomerCompoundViewFactory;Lcom/squareup/ui/crm/edit/EditCustomerWorkflow;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner_Factory;->get()Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
