.class final Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$6;
.super Lkotlin/jvm/internal/Lambda;
.source "RealEditCustomerWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->render(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/ui/crm/edit/EditCustomerState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "+",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/crm/edit/EditCustomerState;",
        "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
        "newValue",
        "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/crm/edit/EditCustomerState;

.field final synthetic this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/ui/crm/edit/EditCustomerState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$6;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$6;->$state:Lcom/squareup/ui/crm/edit/EditCustomerState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/crm/edit/EditCustomerState;",
            "Lcom/squareup/ui/crm/edit/EditCustomerOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "newValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$6;->$state:Lcom/squareup/ui/crm/edit/EditCustomerState;

    check-cast v0, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/edit/EditCustomerState$Editing$EditingGroups;->getAttribute()Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;->getSelectedGroups()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;->copy(Ljava/util/List;)Lcom/squareup/crm/model/ContactAttribute$GroupsAttribute;

    move-result-object p1

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$6;->this$0:Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    check-cast p1, Lcom/squareup/crm/model/ContactAttribute;

    invoke-static {v0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;->access$updateAttributeAction(Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;Lcom/squareup/crm/model/ContactAttribute;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 85
    check-cast p1, Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow$render$6;->invoke(Lcom/squareup/crm/groups/choose/ChooseGroupsOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
