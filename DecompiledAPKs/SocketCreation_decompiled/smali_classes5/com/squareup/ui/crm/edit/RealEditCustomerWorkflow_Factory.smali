.class public final Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealEditCustomerWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/address/workflow/AddressWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexMerchantLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/address/workflow/AddressWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexMerchantLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 41
    iput-object p5, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 42
    iput-object p6, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 43
    iput-object p7, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/address/workflow/AddressWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexMerchantLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;"
        }
    .end annotation

    .line 56
    new-instance v8, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;Lcom/squareup/address/workflow/AddressWorkflow;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/crm/RolodexMerchantLoader;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;",
            "Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;",
            "Lcom/squareup/address/workflow/AddressWorkflow;",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            "Lcom/squareup/crm/RolodexMerchantLoader;",
            "Lcom/squareup/util/Res;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;)",
            "Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;"
        }
    .end annotation

    .line 62
    new-instance v8, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;-><init>(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;Lcom/squareup/address/workflow/AddressWorkflow;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/crm/RolodexMerchantLoader;Lcom/squareup/util/Res;Ljavax/inject/Provider;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;
    .locals 8

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/address/workflow/AddressWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/crm/RolodexGroupLoader;

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/crm/RolodexMerchantLoader;

    iget-object v0, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Res;

    iget-object v7, p0, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->newInstance(Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumWorkflow;Lcom/squareup/crm/groups/choose/ChooseGroupsWorkflow;Lcom/squareup/address/workflow/AddressWorkflow;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/crm/RolodexMerchantLoader;Lcom/squareup/util/Res;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow_Factory;->get()Lcom/squareup/ui/crm/edit/RealEditCustomerWorkflow;

    move-result-object v0

    return-object v0
.end method
