.class final Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4$1;
.super Ljava/lang/Object;
.source "ChooseCustomerScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4;->call(Lcom/squareup/crm/RolodexContactLoader;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func2<",
        "TT1;TT2;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012b\u0010\u0002\u001a^\u0012(\u0012&\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00040\u0004 \u0006*.\u0012(\u0012&\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00040\u0004\u0018\u00010\u00030\u00032b\u0010\u0007\u001a^\u0012(\u0012&\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00080\u0008 \u0006*.\u0012(\u0012&\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000c\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0018\u00010\u00080\u0008\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "progress",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/datafetch/Rx1AbstractLoader$Progress;",
        "Lcom/squareup/crm/RolodexContactLoader$Input;",
        "kotlin.jvm.PlatformType",
        "failure",
        "Lcom/squareup/datafetch/Rx1AbstractLoader$Failure;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4$1;

    invoke-direct {v0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4$1;->INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 65
    check-cast p1, Lcom/squareup/util/Optional;

    check-cast p2, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$onEnterScope$4$1;->call(Lcom/squareup/util/Optional;Lcom/squareup/util/Optional;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/util/Optional;Lcom/squareup/util/Optional;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "+",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Progress<",
            "Lcom/squareup/crm/RolodexContactLoader$Input;",
            ">;>;",
            "Lcom/squareup/util/Optional<",
            "+",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Failure<",
            "Lcom/squareup/crm/RolodexContactLoader$Input;",
            ">;>;)Z"
        }
    .end annotation

    .line 152
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p2}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
