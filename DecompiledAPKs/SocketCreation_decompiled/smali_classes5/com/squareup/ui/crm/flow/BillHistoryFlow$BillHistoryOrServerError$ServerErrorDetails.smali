.class public Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;
.super Ljava/lang/Object;
.source "BillHistoryFlow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ServerErrorDetails"
.end annotation


# instance fields
.field public final message:Ljava/lang/String;

.field public final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;->title:Ljava/lang/String;

    .line 300
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;->message:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public toWarningStrings()Lcom/squareup/widgets/warning/WarningStrings;
    .locals 3

    .line 304
    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;->message:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
