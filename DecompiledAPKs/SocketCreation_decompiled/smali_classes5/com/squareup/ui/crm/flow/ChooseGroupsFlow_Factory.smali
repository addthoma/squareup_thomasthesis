.class public final Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;
.super Ljava/lang/Object;
.source "ChooseGroupsFlow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;",
        ">;"
    }
.end annotation


# instance fields
.field private final createGroupFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/CreateGroupFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/CreateGroupFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;->createGroupFlowProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/CreateGroupFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;)",
            "Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateGroupFlow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;-><init>(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateGroupFlow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;->createGroupFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/flow/CreateGroupFlow;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;->x2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;->newInstance(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateGroupFlow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ChooseGroupsFlow_Factory;->get()Lcom/squareup/ui/crm/flow/ChooseGroupsFlow;

    move-result-object v0

    return-object v0
.end method
