.class final Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;
.super Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;
.source "ViewCustomerProfileRunner.java"


# instance fields
.field private final attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

.field private deleteContactDisposable:Lio/reactivex/disposables/Disposable;

.field private final eventLoader:Lcom/squareup/crm/RolodexEventLoader;

.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;


# direct methods
.method constructor <init>(Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/crm/RolodexEventLoader;Lcom/squareup/crm/RolodexAttachmentLoader;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;Lcom/squareup/ui/crm/flow/MergeCustomersFlow;Lcom/squareup/invoices/InvoicesCustomerLoader;Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;)V
    .locals 29
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v12, p0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p6

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    move-object/from16 v9, p12

    move-object/from16 v10, p13

    move-object/from16 v20, p14

    move-object/from16 v11, p15

    move-object/from16 v14, p16

    move-object/from16 v13, p17

    move-object/from16 v15, p18

    move-object/from16 v19, p19

    move-object/from16 v18, p20

    move-object/from16 v12, p21

    move-object/from16 v16, p22

    move-object/from16 v17, p23

    move-object/from16 v21, p24

    move-object/from16 v22, p25

    move-object/from16 v23, p26

    move-object/from16 v24, p27

    move-object/from16 v25, p28

    move-object/from16 v26, p29

    move-object/from16 v27, p30

    move-object/from16 v28, p31

    .line 119
    invoke-direct/range {v0 .. v28}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;-><init>(Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lcom/squareup/ui/crm/flow/MergeCustomersFlow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;Lcom/squareup/invoices/InvoicesCustomerLoader;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;)V

    const/4 v0, 0x0

    move-object/from16 v1, p0

    .line 84
    iput-object v0, v1, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->deleteContactDisposable:Lio/reactivex/disposables/Disposable;

    move-object/from16 v0, p1

    .line 148
    iput-object v0, v1, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    move-object/from16 v0, p4

    .line 149
    iput-object v0, v1, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    move-object/from16 v0, p5

    .line 150
    iput-object v0, v1, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    return-void
.end method

.method private closeReviewCustomerScreen(Z)V
    .locals 3

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v0, v1, :cond_0

    .line 210
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/crm/v2/ViewCustomerDetailScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    goto :goto_0

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_RETAIL_HOME_APPLET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v0, v1, :cond_1

    .line 213
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/container/CalculatedKey;

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ViewCustomerProfileRunner$f1vDDq3Sa8yeHDJJ3jrIFWtgSdQ;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$ViewCustomerProfileRunner$f1vDDq3Sa8yeHDJJ3jrIFWtgSdQ;

    const-string v2, "goBackFrom[ViewCustomerDetailScreen]"

    invoke-direct {v0, v2, v1}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 217
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v0, v1, :cond_3

    if-eqz p1, :cond_2

    .line 223
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->closeCard(Lflow/Flow;Ljava/lang/Class;)V

    goto :goto_0

    .line 225
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    goto :goto_0

    .line 227
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_PROFILE_IN_APPLET_CARD:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne p1, v0, :cond_4

    .line 228
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    :cond_4
    :goto_0
    return-void
.end method

.method private inCustomersApplet()Z
    .locals 1

    .line 393
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-static {v0}, Lcom/squareup/ui/crm/flow/CrmScopeTypeKt;->inCustomersApplet(Lcom/squareup/ui/crm/flow/CrmScopeType;)Z

    move-result v0

    return v0
.end method

.method static synthetic lambda$closeReviewCustomerScreen$2(Lflow/History;)Lcom/squareup/container/Command;
    .locals 1

    .line 214
    invoke-virtual {p0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflow/History$Builder;->pop(I)Lflow/History$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    .line 215
    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p0, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$4(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-void
.end method


# virtual methods
.method public addThisCustomerToSale()V
    .locals 3

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, v2}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    const/4 v0, 0x1

    .line 239
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->closeReviewCustomerScreen(Z)V

    return-void
.end method

.method protected applyCouponToCartAndAddCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 1

    .line 345
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    invoke-interface {v0, p2}, Lcom/squareup/checkout/HoldsCoupons;->apply(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 346
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->inCustomersApplet()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 347
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->newSaleWithCustomerFromApplet()V

    goto :goto_0

    .line 348
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->isContactAddedToHoldsCustomer(Lcom/squareup/protos/client/rolodex/Contact;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 349
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    const/4 p1, 0x0

    .line 350
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->closeReviewCustomerScreen(Z)V

    goto :goto_0

    .line 352
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->addThisCustomerToSale()V

    :goto_0
    return-void
.end method

.method public canStartNewSaleWithCustomerFromApplet()Z
    .locals 1

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->hasOrderEntryApplet()Z

    move-result v0

    return v0
.end method

.method public cancelDeleteSingleCustomer()V
    .locals 4

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public closeCustomerActivityScreen()V
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_ACTIVITY_LIST:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/CustomerActivityScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeSendMessageScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 270
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->sendMessageScreenMessage:Ljava/lang/String;

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/SendMessageScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeSendMessageScreen(Lcom/squareup/protos/client/dialogue/Conversation;)V
    .locals 1

    const/4 p1, 0x0

    .line 275
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->sendMessageScreenMessage:Ljava/lang/String;

    .line 276
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    invoke-virtual {p1}, Lcom/squareup/crm/RolodexEventLoader;->refresh()V

    .line 277
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/crm/cards/SendMessageScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public deleteSingleCustomer()V
    .locals 2

    .line 358
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->deleteContactDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 359
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexServiceHelper;->deleteContact(Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ViewCustomerProfileRunner$ZD66hdaKKSnXHb_XCMfJXHcSAOA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ViewCustomerProfileRunner$ZD66hdaKKSnXHb_XCMfJXHcSAOA;-><init>(Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;)V

    .line 362
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->deleteContactDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public goBackFromViewCustomer()V
    .locals 2

    .line 193
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->inCustomersApplet()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_IN_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v0, v1, :cond_0

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    :cond_0
    const/4 v0, 0x0

    .line 201
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->closeReviewCustomerScreen(Z)V

    return-void
.end method

.method public isCustomerAddedToSale(Ljava/lang/String;)Z
    .locals 1

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    invoke-interface {v0}, Lcom/squareup/payment/crm/HoldsCustomer;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    .line 260
    invoke-interface {v0}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic lambda$deleteSingleCustomer$5$ViewCustomerProfileRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 362
    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$ViewCustomerProfileRunner$60J_yGMoiRXHo-FVHSmkUqyXQ5U;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ViewCustomerProfileRunner$60J_yGMoiRXHo-FVHSmkUqyXQ5U;-><init>(Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;)V

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ViewCustomerProfileRunner$ivxGQI9tkhpkuoNLsExmmXkcm0A;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$ViewCustomerProfileRunner$ivxGQI9tkhpkuoNLsExmmXkcm0A;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$0$ViewCustomerProfileRunner(Lflow/History$Builder;)Lflow/History$Builder;
    .locals 2

    .line 173
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_VIEW_CUSTOMER_ADDED_TO_SALE_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 174
    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isOnPostTransactionMonitor()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->pipTenderScope()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    :cond_0
    return-object p1
.end method

.method public synthetic lambda$null$3$ViewCustomerProfileRunner(Lcom/squareup/protos/client/rolodex/DeleteContactResponse;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 364
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    invoke-interface {p1}, Lcom/squareup/payment/crm/HoldsCustomer;->hasCustomer()Z

    move-result p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    .line 365
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->contactToken()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 366
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->contactToken()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    invoke-interface {v1}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 367
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    invoke-interface {p1, v0, v0, v0}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 369
    :cond_0
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 373
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/squareup/ui/crm/v2/CustomersAppletDetailScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$ViewCustomerProfileRunner(Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;)V
    .locals 4

    .line 167
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->setLoadedContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    const/4 v2, 0x2

    new-array v2, v2, [Lkotlin/jvm/functions/Function1;

    .line 170
    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;->getHistoryFunc()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    const/4 v3, 0x0

    aput-object p1, v2, v3

    new-instance p1, Lcom/squareup/ui/crm/flow/-$$Lambda$ViewCustomerProfileRunner$lRToa-Enq0KTbWqqdF3qcAuM7p4;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ViewCustomerProfileRunner$lRToa-Enq0KTbWqqdF3qcAuM7p4;-><init>(Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;)V

    const/4 v3, 0x1

    aput-object p1, v2, v3

    .line 169
    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public newSaleWithCustomerFromApplet()V
    .locals 3

    .line 323
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->inCustomersApplet()Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 326
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, v2}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 329
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->activateApplet()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 155
    invoke-super {p0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->onEnterScope(Lmortar/MortarScope;)V

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_DISABLE_BUYER_PROFILE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->attachmentLoader:Lcom/squareup/crm/RolodexAttachmentLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->addCardOnFileFlow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    .line 165
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onResult()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$ViewCustomerProfileRunner$24fyiOkT-uKDh0FoNMJxHzbB5YY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$ViewCustomerProfileRunner$24fyiOkT-uKDh0FoNMJxHzbB5YY;-><init>(Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;)V

    .line 166
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 164
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 181
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object p1, p1, Lcom/squareup/ui/crm/flow/CrmScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->setBaseContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 185
    invoke-super {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->onExitScope()V

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->deleteContactDisposable:Lio/reactivex/disposables/Disposable;

    if-eqz v0, :cond_0

    .line 187
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    :cond_0
    return-void
.end method

.method public removeThisCustomer()V
    .locals 3

    .line 245
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->inCustomersApplet()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    const/4 v2, 0x0

    invoke-interface {v0, v2, v2, v2}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->dismissSaveCustomer()Z

    .line 250
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_INVOICE_EDIT_INVOICE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v0, v2, :cond_1

    .line 251
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    sget-object v2, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_ESTIMATE_EDIT_ESTIMATE:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v0, v2, :cond_0

    goto :goto_0

    .line 254
    :cond_0
    invoke-direct {p0, v1}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->closeReviewCustomerScreen(Z)V

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 252
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->closeReviewCustomerScreen(Z)V

    :goto_1
    return-void
.end method

.method scopeSupportsProfileAttachments()Z
    .locals 2

    .line 385
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->inCustomersApplet()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_INVOICE_IN_DETAIL:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public showConflictingContact()V
    .locals 3

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_INVOICE_IN_DETAIL:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 294
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->conflictingLoyaltyContact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v1, v2}, Lcom/squareup/ui/crm/flow/CrmScope;->newReadOnlyCrmScopeCopy(Lcom/squareup/ui/crm/flow/CrmScope;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showDeleteSingleCustomerScreen()V
    .locals 3

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showImagePreviewScreen(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;)V
    .locals 5

    .line 318
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewScreen;

    new-instance v2, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    const/4 v4, 0x0

    invoke-direct {v2, v3, p1, p2, v4}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;Landroid/net/Uri;)V

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsImagePreviewScreen;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showInvoiceReadOnlyScreen(Ljava/lang/String;)V
    .locals 3

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_INVOICE_LINKING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, p1, v2}, Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen;-><init>(Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showMergeContacts()V
    .locals 3

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->mergeCustomersFlow:Lcom/squareup/ui/crm/flow/MergeCustomersFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public showOverflowBottomSheet(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;)V
    .locals 5

    .line 313
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog;

    new-instance v2, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    const/4 v4, 0x0

    invoke-direct {v2, v3, p1, p2, v4}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;Landroid/net/Uri;)V

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showProfileAttachmentsScreen()V
    .locals 6

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen;

    new-instance v2, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v4, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 303
    invoke-virtual {v4}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/rolodex/Contact;

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5, v5}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;Landroid/net/Uri;)V

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsScreen;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    .line 302
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showTransferLoyaltyAccount(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 3

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v2, v2, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-static {v1, v2, p2, p1}, Lcom/squareup/ui/crm/flow/CrmScope;->newTransferLoyaltyCustomerCard(Lcom/squareup/ui/crm/flow/CrmScope;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p1

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showUploadFileBottomSheet()V
    .locals 6

    .line 307
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog;

    new-instance v2, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v4, p0, Lcom/squareup/ui/crm/flow/ViewCustomerProfileRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 308
    invoke-virtual {v4}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/rolodex/Contact;

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5, v5}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;Landroid/net/Uri;)V

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    .line 307
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
