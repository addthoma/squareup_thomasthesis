.class public interface abstract Lcom/squareup/ui/crm/flow/CrmScope$ParentOrderEntryComponent;
.super Ljava/lang/Object;
.source "CrmScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/CrmScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ParentOrderEntryComponent"
.end annotation


# virtual methods
.method public abstract addCustomerToSaleTicket(Lcom/squareup/ui/crm/flow/CrmScope$AddInTransModule;)Lcom/squareup/ui/crm/flow/CrmScope$AddInTransC;
.end method

.method public abstract viewCustomerAddedToSale(Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileWithRefundHelperModule;)Lcom/squareup/ui/crm/flow/CrmScope$ViewInTransC;
.end method
