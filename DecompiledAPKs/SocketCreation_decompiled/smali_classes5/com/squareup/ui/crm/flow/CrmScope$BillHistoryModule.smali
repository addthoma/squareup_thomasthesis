.class public abstract Lcom/squareup/ui/crm/flow/CrmScope$BillHistoryModule;
.super Ljava/lang/Object;
.source "CrmScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/CrmScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BillHistoryModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 641
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAbstractBillHistoryRunner(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBillHistoryRunner(Lcom/squareup/ui/crm/flow/BillHistoryFlow;)Lcom/squareup/ui/crm/cards/BillHistoryScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTenderWithCustomerInfoCache(Lcom/squareup/ui/activity/billhistory/RealTenderWithCustomerInfoCache;)Lcom/squareup/ui/activity/billhistory/TenderWithCustomerInfoCache;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
