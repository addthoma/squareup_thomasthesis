.class Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "AddCustomerToSaleRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->showCreateCustomerScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)V
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$1;->this$0:Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 9

    .line 275
    sget-object v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$2;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$1;->this$0:Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$1;->this$0:Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$1;->this$0:Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;

    iget-object v2, v1, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$1;->this$0:Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;

    iget-object v3, v1, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v4, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->ADD_TO_SALE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    sget-object v5, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->CREATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$1;->this$0:Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;

    iget-object v1, v1, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v1, v1, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    .line 288
    invoke-static {v1}, Lcom/squareup/ui/crm/flow/CrmScopeTypeKt;->isInvoicePath(Lcom/squareup/ui/crm/flow/CrmScopeType;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;->REQUIRE_FIRST_LAST_EMAIL:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;->REQUIRE_AT_LEAST_ONE_FIELD:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    :goto_0
    move-object v6, v1

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$1;->this$0:Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;

    iget-object v1, v1, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v7, v1, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$1;->this$0:Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;

    .line 292
    invoke-static {v1}, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->access$000(Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;)Ljava/lang/String;

    move-result-object v1

    const/4 v8, 0x0

    invoke-static {v1, v8}, Lcom/squareup/crm/util/RolodexContactHelper;->newContactFromSearchTermOrGroup(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v8

    .line 284
    invoke-interface/range {v2 .. v8}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;->getFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_1

    .line 280
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner$1;->this$0:Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/AddCustomerToSaleRunner;->addCardOnFileFlow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    new-instance v1, Lcom/squareup/protos/client/rolodex/Contact$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->showFirstScreen(Lcom/squareup/protos/client/rolodex/Contact;)V

    :goto_1
    return-void
.end method
