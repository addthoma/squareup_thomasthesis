.class public abstract Lcom/squareup/ui/crm/flow/InCrmScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InCrmScope.java"


# instance fields
.field public final crmPath:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method protected constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/InCrmScope;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 2

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/InCrmScope;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v1}, Lcom/squareup/ui/main/RegisterTreeKey;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-super {p0}, Lcom/squareup/ui/main/RegisterTreeKey;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getParentKey()Ljava/lang/Object;
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/InCrmScope;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method
