.class public final Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper_Factory;
.super Ljava/lang/Object;
.source "RealCrmScopeTransactionsHistoryHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final refundBillHistoryWrapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper_Factory;->refundBillHistoryWrapperProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;Lcom/squareup/util/Res;)Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper;-><init>(Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;Lcom/squareup/util/Res;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper_Factory;->refundBillHistoryWrapperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-static {v0, v1}, Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper_Factory;->newInstance(Lcom/squareup/ui/crm/flow/RefundBillHistoryWrapper;Lcom/squareup/util/Res;)Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper_Factory;->get()Lcom/squareup/ui/crm/flow/RealCrmScopeTransactionsHistoryHelper;

    move-result-object v0

    return-object v0
.end method
