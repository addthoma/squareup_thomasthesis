.class public Lcom/squareup/ui/crm/flow/UpdateFilterFlow;
.super Ljava/lang/Object;
.source "UpdateFilterFlow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/UpdateFilterFlow$SharedScope;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private filter:Lcom/squareup/protos/client/rolodex/Filter;

.field private final flow:Lflow/Flow;

.field private final onResult:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field

.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->flow:Lflow/Flow;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public commitEditFilterCard()V
    .locals 4

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/EditFilterScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    if-eqz v0, :cond_0

    .line 102
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->analytics:Lcom/squareup/analytics/Analytics;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    aput-object v0, v2, v3

    const/4 v0, 0x1

    const-string v3, "Select Filter Value"

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/squareup/ui/crm/v2/CrmDynamicEvent;->createFilterEvent([Ljava/lang/String;)Lcom/squareup/ui/crm/v2/CrmDynamicEvent;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_0
    return-void
.end method

.method public dismissEditFilterCard()V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/EditFilterScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public getFilter()Lcom/squareup/protos/client/rolodex/Filter;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 62
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isFilterBeingCreated()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "parentKey"

    .line 73
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 74
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "filter"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Filter;

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    return-void
.end method

.method public onResult()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const-string v1, "parentKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "filter"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method public setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    return-void
.end method

.method public showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 1

    .line 108
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 109
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->filter:Lcom/squareup/protos/client/rolodex/Filter;

    .line 110
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateFilterFlow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/EditFilterScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/cards/EditFilterScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p2, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
