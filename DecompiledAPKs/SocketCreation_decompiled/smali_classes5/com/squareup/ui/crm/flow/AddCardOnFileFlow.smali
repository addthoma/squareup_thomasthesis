.class public Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;
.super Ljava/lang/Object;
.source "AddCardOnFileFlow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Runner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/AddCardOnFileFlow$SharedScope;
    }
.end annotation


# instance fields
.field private final bus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

.field private final cofDippedCardInfoProcessor:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;

.field private contact:Lcom/squareup/protos/client/rolodex/Contact;

.field private final crmDipSupported:Z

.field private crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

.field private final flow:Lflow/Flow;

.field private final giftCards:Lcom/squareup/giftcard/GiftCards;

.field private holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final onCancel:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onDipFailure:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private onDipSuccess:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;",
            ">;"
        }
    .end annotation
.end field

.field private final onResult:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;",
            ">;"
        }
    .end annotation
.end field

.field private final onSwipeFailure:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
            ">;"
        }
    .end annotation
.end field

.field private final onSwipeSuccess:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
            ">;"
        }
    .end annotation
.end field

.field private saveCardState:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

.field private final skipPostal:Z

.field private final x2CrmDipSupported:Z

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/giftcard/GiftCards;Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 75
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onCancel:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 79
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onSwipeSuccess:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 80
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onSwipeFailure:Lcom/jakewharton/rxrelay/PublishRelay;

    const/4 v0, 0x0

    .line 83
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 84
    new-instance v0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-direct {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->saveCardState:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    .line 98
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->flow:Lflow/Flow;

    .line 99
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 100
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 101
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->bus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    .line 102
    iput-object p5, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->giftCards:Lcom/squareup/giftcard/GiftCards;

    .line 103
    iput-object p6, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->cofDippedCardInfoProcessor:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;

    .line 104
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_DIP_CRM:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p7, p1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmDipSupported:Z

    .line 105
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->CARD_ON_FILE_DIP_CRM_X2:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p7, p1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2CrmDipSupported:Z

    .line 106
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->COLLECT_COF_POSTAL_CODE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p7, p1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->skipPostal:Z

    .line 108
    invoke-interface {p6}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;->onDipResult()Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/crm/flow/-$$Lambda$t0soJ35vF_MyOwKdb1qMFquyQuY;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$t0soJ35vF_MyOwKdb1qMFquyQuY;

    .line 109
    invoke-virtual {p1, p2}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$55GL-czDCaebl81FBvDqKnsMlWU;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$55GL-czDCaebl81FBvDqKnsMlWU;-><init>(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)V

    .line 110
    invoke-virtual {p1, p2}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onDipSuccess:Lrx/Observable;

    .line 112
    invoke-interface {p6}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;->onDipResult()Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/crm/flow/-$$Lambda$-A9WTeOOPeyNn5nS-Qo8JdrJ0qk;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$-A9WTeOOPeyNn5nS-Qo8JdrJ0qk;

    .line 113
    invoke-virtual {p1, p2}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$7lkgSVPh5dioj5dDE8ilVzvQHAA;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$7lkgSVPh5dioj5dDE8ilVzvQHAA;

    .line 114
    invoke-virtual {p1, p2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$55GL-czDCaebl81FBvDqKnsMlWU;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$55GL-czDCaebl81FBvDqKnsMlWU;-><init>(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)V

    .line 115
    invoke-virtual {p1, p2}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onDipFailure:Lrx/Observable;

    return-void
.end method

.method private closeDippedCardSpinnerScreen()V
    .locals 4

    .line 330
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->isCrmDipSupported()Z

    move-result v0

    const-string v1, "dip is not supported"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 332
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method private filterForCustomerSaveCardScreen(Lrx/Observable;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;)",
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation

    .line 246
    const-class v0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->filterForScreen(Lrx/Observable;Ljava/lang/Class;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method private filterForDippedCardSpinnerScreen(Lrx/Observable;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;)",
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation

    .line 250
    const-class v0, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->filterForScreen(Lrx/Observable;Ljava/lang/Class;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method private getContact()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 400
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    return-object v0

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v0, :cond_1

    return-object v0

    .line 405
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    if-eqz v0, :cond_2

    .line 409
    invoke-interface {v0}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method private initializeCard(Lcom/squareup/payment/BillPayment;)V
    .locals 2

    if-nez p1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 204
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/payment/BillPayment;->getCard()Lcom/squareup/Card;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    .line 205
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->giftCards:Lcom/squareup/giftcard/GiftCards;

    invoke-virtual {v1, v0}, Lcom/squareup/giftcard/GiftCards;->isPossiblyGiftCard(Lcom/squareup/Card;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 208
    invoke-virtual {v0}, Lcom/squareup/Card;->toBuilder()Lcom/squareup/Card$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    .line 209
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object v0

    .line 210
    invoke-virtual {v0}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object v0

    .line 212
    :cond_1
    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->setCard(Lcom/squareup/Card;Lcom/squareup/payment/BillPayment;)V

    return-void
.end method

.method private isPosCrmDipSupported()Z
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmDipSupported:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private isX2CrmDipSupported()Z
    .locals 1

    .line 344
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->badIsHodorCheck()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2CrmDipSupported:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public static synthetic lambda$-Qva3YREmc0BtWrvcJLmw9CQ5D0(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lrx/Observable;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->filterForCustomerSaveCardScreen(Lrx/Observable;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$55GL-czDCaebl81FBvDqKnsMlWU(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lrx/Observable;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->waitForDippedCardSpinnerScreenToNotBeTop(Lrx/Observable;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic lambda$X2MI1LUJJ98FxEupMvc3lYrSaX0(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lrx/Observable;)Lrx/Observable;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->filterForDippedCardSpinnerScreen(Lrx/Observable;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$filterForScreen$5(Ljava/lang/Class;Ljava/lang/Object;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Boolean;
    .locals 0

    .line 258
    invoke-virtual {p0, p2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$filterForScreen$6(Ljava/lang/Object;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$finishSaveCardFlow$7(ZLflow/History$Builder;)Lflow/History$Builder;
    .locals 1

    .line 295
    const-class v0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    if-nez p0, :cond_0

    .line 298
    const-class p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;

    invoke-static {p1, p0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    .line 300
    :cond_0
    const-class p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;

    invoke-static {p1, p0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    .line 301
    const-class p0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;

    invoke-static {p1, p0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    return-object p1
.end method

.method static synthetic lambda$new$0(Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;)Lkotlin/Unit;
    .locals 0

    .line 114
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method static synthetic lambda$waitForDippedCardSpinnerScreenToNotBeTop$1(Ljava/lang/Object;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Boolean;
    .locals 0

    .line 127
    instance-of p0, p1, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen;

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$waitForDippedCardSpinnerScreenToNotBeTop$2(Ljava/lang/Object;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method private processDip()V
    .locals 3

    .line 323
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->isCrmDipSupported()Z

    move-result v0

    const-string v1, "dip is not supported"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->saveCardState:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->clearCard()V

    .line 326
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private setContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 379
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0, p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->saveCustomerContact(Lcom/squareup/protos/client/rolodex/Contact;)Z

    goto :goto_0

    .line 382
    :cond_0
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    :goto_0
    return-void
.end method

.method private waitForDippedCardSpinnerScreenToNotBeTop(Lrx/Observable;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;)",
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    .line 126
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$Z07InZnNRdYXOGF9N4w8BFu88Ds;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$Z07InZnNRdYXOGF9N4w8BFu88Ds;

    .line 127
    invoke-static {v0}, Lcom/squareup/util/RxTuples;->expandPairForFunc(Lrx/functions/Func2;)Lrx/functions/Func1;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$Va4uB1bmPj8OtY99EXjlvRrDUTw;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$Va4uB1bmPj8OtY99EXjlvRrDUTw;

    .line 128
    invoke-static {v0}, Lcom/squareup/util/RxTuples;->expandPairForFunc(Lrx/functions/Func2;)Lrx/functions/Func1;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    .line 129
    invoke-virtual {p1}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public cancelDippedCardSpinnerScreen()V
    .locals 2

    .line 318
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->isCrmDipSupported()Z

    move-result v0

    const-string v1, "dip is not supported"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->cofDippedCardInfoProcessor:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;

    invoke-interface {v0}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;->cancel()V

    return-void
.end method

.method public cancelSaveCardToCustomerScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 364
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 365
    new-instance v0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-direct {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->saveCardState:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    .line 367
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onCancel:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public closeCustomerEmailScreen()V
    .locals 2

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeSaveCardSpinner()V
    .locals 2

    .line 314
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeVerifyPostalCodeScreen()V
    .locals 2

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method filterForScreen(Lrx/Observable;Ljava/lang/Class;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;",
            "Ljava/lang/Class;",
            ")",
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    .line 256
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$vPmIg28J74RsZnDqgCWroEsXeKo;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$vPmIg28J74RsZnDqgCWroEsXeKo;-><init>(Ljava/lang/Class;)V

    .line 258
    invoke-static {v0}, Lcom/squareup/util/RxTuples;->expandPairForFunc(Lrx/functions/Func2;)Lrx/functions/Func1;

    move-result-object p2

    .line 257
    invoke-virtual {p1, p2}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$QLuHsDuDBWVdsm1331tKYyJczq0;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$QLuHsDuDBWVdsm1331tKYyJczq0;

    .line 259
    invoke-static {p2}, Lcom/squareup/util/RxTuples;->expandPairForFunc(Lrx/functions/Func2;)Lrx/functions/Func1;

    move-result-object p2

    invoke-virtual {p1, p2}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public finishSaveCardFlow(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 5

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->saveCardState:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 294
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v2, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->saveCardState:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    new-instance v4, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$JUGjlauTW2dcMuC9k5jc9tMMFkY;

    invoke-direct {v4, v0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$JUGjlauTW2dcMuC9k5jc9tMMFkY;-><init>(Z)V

    invoke-direct {v2, p1, v3, v4}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;-><init>(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/ui/crm/flow/SaveCardSharedState;Lkotlin/jvm/functions/Function1;)V

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    const/4 p1, 0x0

    .line 305
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 306
    new-instance p1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-direct {p1}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->saveCardState:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    return-void
.end method

.method public getContactForSaveCardScreen()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 352
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    return-object v0
.end method

.method public getContactForVerifyPostalCodeScreen()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 288
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 174
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    return-object v0
.end method

.method public getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;
    .locals 1

    .line 356
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->getStateForSaveCard()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object v0

    return-object v0

    .line 359
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->saveCardState:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    return-object v0
.end method

.method public isCrmDipSupported()Z
    .locals 1

    .line 348
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->isPosCrmDipSupported()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->isX2CrmDipSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public synthetic lambda$onEnterScope$3$AddCardOnFileFlow(Lkotlin/Unit;)V
    .locals 0

    .line 189
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->processDip()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$4$AddCardOnFileFlow(Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;)V
    .locals 0

    .line 194
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->closeDippedCardSpinnerScreen()V

    return-void
.end method

.method public onCancel()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onCancel:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 178
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->billPayment:Lcom/squareup/payment/BillPayment;

    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->initializeCard(Lcom/squareup/payment/BillPayment;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->bus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$0QbiAt1b6r5CMPI2h9Srop6mR0Y;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$0QbiAt1b6r5CMPI2h9Srop6mR0Y;-><init>(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->bus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->failedSwipes()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$7Ml3KKJOKBZhuJxVIZskPegibiw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$7Ml3KKJOKBZhuJxVIZskPegibiw;-><init>(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 186
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->isCrmDipSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->cofDippedCardInfoProcessor:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;

    invoke-interface {v0}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;->onCardInserted()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$-Qva3YREmc0BtWrvcJLmw9CQ5D0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$-Qva3YREmc0BtWrvcJLmw9CQ5D0;-><init>(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)V

    .line 188
    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$vHIk5FBHtyrXo3YlcglebAzl6CM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$vHIk5FBHtyrXo3YlcglebAzl6CM;-><init>(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)V

    .line 189
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 187
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->cofDippedCardInfoProcessor:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;

    invoke-interface {v0}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;->onDipResult()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$Kl2odX8_kLEqRBHWva7NXNjF-hw;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$Kl2odX8_kLEqRBHWva7NXNjF-hw;

    .line 192
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$X2MI1LUJJ98FxEupMvc3lYrSaX0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$X2MI1LUJJ98FxEupMvc3lYrSaX0;-><init>(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)V

    .line 193
    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$kDVbSpqxIdVJ_82Zb3kJGNcRJsg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$kDVbSpqxIdVJ_82Zb3kJGNcRJsg;-><init>(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)V

    .line 194
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 191
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->cofDippedCardInfoProcessor:Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;

    invoke-interface {v0, p1}, Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;->register(Lmortar/MortarScope;)V

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onFailedDip()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 221
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->isCrmDipSupported()Z

    move-result v0

    const-string v1, "dip is not supported"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onDipFailure:Lrx/Observable;

    return-object v0
.end method

.method public onFailedSwipe()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
            ">;"
        }
    .end annotation

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onSwipeFailure:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$-Qva3YREmc0BtWrvcJLmw9CQ5D0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$-Qva3YREmc0BtWrvcJLmw9CQ5D0;-><init>(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)V

    .line 241
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Lrx/Observable;->share()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 264
    :cond_0
    sget-object v0, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "contact"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public onResult()Lrx/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/flow/AddCardOnFileFlowResult;",
            ">;"
        }
    .end annotation

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onResult:Lcom/jakewharton/rxrelay/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 163
    invoke-interface {v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->onCardOnFileResult()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    .line 162
    invoke-static {v0, v1}, Lrx/Observable;->merge(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 268
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "contact"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method public onSuccessfulDip()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor$DipResult;",
            ">;"
        }
    .end annotation

    .line 216
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->isCrmDipSupported()Z

    move-result v0

    const-string v1, "dip is not supported"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onDipSuccess:Lrx/Observable;

    return-object v0
.end method

.method public onSuccessfulSwipe()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
            ">;"
        }
    .end annotation

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onSwipeSuccess:Lcom/jakewharton/rxrelay/PublishRelay;

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$-Qva3YREmc0BtWrvcJLmw9CQ5D0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AddCardOnFileFlow$-Qva3YREmc0BtWrvcJLmw9CQ5D0;-><init>(Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;)V

    .line 235
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    .line 236
    invoke-virtual {v0}, Lrx/Observable;->share()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method onSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onSwipeFailure:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method onSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onSwipeSuccess:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method setCard(Lcom/squareup/Card;Lcom/squareup/payment/BillPayment;)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez p2, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-ne v2, v3, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    const-string v1, "card and billPayment must have matching nullity"

    .line 387
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 390
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0, p1, p2}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->saveCustomerCardOnFile(Lcom/squareup/Card;Lcom/squareup/payment/BillPayment;)Z

    goto :goto_3

    :cond_3
    if-nez p1, :cond_4

    .line 392
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->saveCardState:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->clearCard()V

    goto :goto_3

    .line 394
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->saveCardState:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {p2}, Lcom/squareup/payment/BillPayment;->getCardData()Lcom/squareup/protos/client/bills/CardData;

    move-result-object v1

    .line 395
    invoke-static {p1, p2}, Lcom/squareup/ui/crm/cards/AddCardHelpers;->getEntryMethod(Lcom/squareup/Card;Lcom/squareup/payment/BillPayment;)Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object p2

    .line 394
    invoke-virtual {v0, p1, v1, p2}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->setCard(Lcom/squareup/Card;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    :goto_3
    return-void
.end method

.method public showCustomerEmailScreen()V
    .locals 3

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->isBranReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-interface {v0, v1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->displayCardOnFileAuth(Lcom/squareup/ui/main/RegisterTreeKey;)Z

    goto :goto_0

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method showFirstScreen(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 2

    .line 138
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 139
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->enteringSaveCardOnFile()Z

    return-void
.end method

.method showFirstScreen(Lflow/History$Builder;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 150
    invoke-direct {p0, p2}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 151
    new-instance p2, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {p2, v0}, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 152
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object v0, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-virtual {p2, p1, v0}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    .line 153
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {p1}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->enteringSaveCardOnFile()Z

    return-void
.end method

.method public showSaveCardSpinnerScreen()V
    .locals 3

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showVerifyZipCodeOrSaveCardSpinnerScreen()V
    .locals 3

    .line 276
    iget-boolean v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->skipPostal:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->saveCardState:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_1

    .line 277
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method
