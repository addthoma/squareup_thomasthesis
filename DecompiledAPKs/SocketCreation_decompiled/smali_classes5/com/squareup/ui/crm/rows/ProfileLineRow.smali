.class public Lcom/squareup/ui/crm/rows/ProfileLineRow;
.super Landroid/widget/LinearLayout;
.source "ProfileLineRow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0016\u0018\u00002\u00020\u0001:\u0001\u0011B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000cH\u0002J\u0010\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/crm/rows/ProfileLineRow;",
        "Landroid/widget/LinearLayout;",
        "context",
        "Landroid/content/Context;",
        "(Landroid/content/Context;)V",
        "infoView",
        "Lcom/squareup/marketfont/MarketTextView;",
        "titleView",
        "Landroid/widget/TextView;",
        "linkInfoAndRemoveUnderLines",
        "",
        "info",
        "",
        "uriScheme",
        "setInfo",
        "line",
        "Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;",
        "ViewData",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final infoView:Lcom/squareup/marketfont/MarketTextView;

.field private final titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 30
    sget v0, Lcom/squareup/crm/R$layout;->crm_v2_profile_line_data_row:I

    move-object v1, p0

    check-cast v1, Landroid/view/ViewGroup;

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 31
    move-object p1, p0

    check-cast p1, Landroid/view/View;

    sget v0, Lcom/squareup/crm/R$id;->crm_line_data_row_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow;->titleView:Landroid/widget/TextView;

    .line 32
    sget v0, Lcom/squareup/crm/R$id;->crm_line_data_row_info:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow;->infoView:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method private final linkInfoAndRemoveUnderLines(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow;->infoView:Lcom/squareup/marketfont/MarketTextView;

    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow;->infoView:Lcom/squareup/marketfont/MarketTextView;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    const/16 v0, 0x10

    .line 70
    invoke-static {p1, v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object p1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow;->infoView:Lcom/squareup/marketfont/MarketTextView;

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0, p1, p2}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow;->infoView:Lcom/squareup/marketfont/MarketTextView;

    check-cast p1, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/text/NoUnderlineURLSpan;->removeLinkUnderlines(Landroid/widget/TextView;)V

    return-void
.end method


# virtual methods
.method public setInfo(Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;)V
    .locals 2

    const-string v0, "line"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow;->titleView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->getTitle()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 38
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->getType()Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/rows/ProfileLineRow$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData$Type;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 54
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->getInfo()Ljava/lang/String;

    move-result-object p1

    const-string v0, "geo:0,0?q="

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/rows/ProfileLineRow;->linkInfoAndRemoveUnderLines(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 52
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->getInfo()Ljava/lang/String;

    move-result-object p1

    const-string v0, "mailto:"

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/rows/ProfileLineRow;->linkInfoAndRemoveUnderLines(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->getInfo()Ljava/lang/String;

    move-result-object p1

    const-string v0, "tel:"

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/rows/ProfileLineRow;->linkInfoAndRemoveUnderLines(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 45
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow;->infoView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->getInfo()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow;->infoView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/ProfileLineRow;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/crm/R$color;->profile_row_text_link:I

    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 47
    iget-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow;->infoView:Lcom/squareup/marketfont/MarketTextView;

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    goto :goto_0

    .line 40
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow;->infoView:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/ProfileLineRow$ViewData;->getInfo()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    iget-object p1, p0, Lcom/squareup/ui/crm/rows/ProfileLineRow;->infoView:Lcom/squareup/marketfont/MarketTextView;

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    :goto_0
    return-void
.end method
