.class public Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;
.super Lcom/squareup/widgets/list/ToggleButtonRow;
.source "EditBooleanAttributeRow.java"

# interfaces
.implements Lcom/squareup/ui/crm/rows/HasAttribute;


# instance fields
.field private attributeDefinition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 3

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 35
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;->attributeDefinition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->toAttributeBuilder(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;-><init>()V

    const/4 v2, 0x1

    .line 37
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->boolean_(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    move-result-object v1

    .line 38
    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    return-object v0
.end method

.method public showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 1

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;->attributeDefinition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    .line 23
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 24
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->boolean_:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;->setChecked(Z)V

    .line 25
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v0, 0x8

    :goto_1
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;->setVisibility(I)V

    .line 26
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/rows/EditBooleanAttributeRow;->setButtonEnabled(Z)V

    return-void
.end method
