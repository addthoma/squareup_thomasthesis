.class public final Lcom/squareup/ui/crm/ConversationPresenter_Factory;
.super Ljava/lang/Object;
.source "ConversationPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/ConversationPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final arg10Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg11Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg12Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg13Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/coupon/AddCouponState;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/coupon/AddCouponState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;)V"
        }
    .end annotation

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 61
    iput-object p2, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 62
    iput-object p3, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 63
    iput-object p4, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 64
    iput-object p5, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 65
    iput-object p6, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 66
    iput-object p7, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 67
    iput-object p8, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 68
    iput-object p9, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg8Provider:Ljavax/inject/Provider;

    .line 69
    iput-object p10, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg9Provider:Ljavax/inject/Provider;

    .line 70
    iput-object p11, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg10Provider:Ljavax/inject/Provider;

    .line 71
    iput-object p12, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg11Provider:Ljavax/inject/Provider;

    .line 72
    iput-object p13, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg12Provider:Ljavax/inject/Provider;

    .line 73
    iput-object p14, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg13Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/ConversationPresenter_Factory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/coupon/AddCouponState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;)",
            "Lcom/squareup/ui/crm/ConversationPresenter_Factory;"
        }
    .end annotation

    .line 89
    new-instance v15, Lcom/squareup/ui/crm/ConversationPresenter_Factory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/crm/ConversationPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/crm/coupon/AddCouponState;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/crm/DialogueServiceHelper;Lcom/squareup/ui/ErrorsBarPresenter;Ljava/util/Locale;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/settings/server/Features;Ljava/text/DateFormat;)Lcom/squareup/ui/crm/ConversationPresenter;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/ui/crm/coupon/AddCouponState;",
            "Ljava/text/DateFormat;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/crm/DialogueServiceHelper;",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Ljava/util/Locale;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            "Lcom/squareup/server/bills/BillListServiceHelper;",
            "Lcom/squareup/settings/server/Features;",
            "Ljava/text/DateFormat;",
            ")",
            "Lcom/squareup/ui/crm/ConversationPresenter;"
        }
    .end annotation

    .line 96
    new-instance v15, Lcom/squareup/ui/crm/ConversationPresenter;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/ui/crm/ConversationPresenter;-><init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/crm/coupon/AddCouponState;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/crm/DialogueServiceHelper;Lcom/squareup/ui/ErrorsBarPresenter;Ljava/util/Locale;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/settings/server/Features;Ljava/text/DateFormat;)V

    return-object v15
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/ConversationPresenter;
    .locals 15

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/crm/coupon/AddCouponState;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/crm/DialogueServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/text/Formatter;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg10Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg11Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/server/bills/BillListServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg12Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->arg13Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Ljava/text/DateFormat;

    invoke-static/range {v1 .. v14}, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->newInstance(Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/crm/coupon/AddCouponState;Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/crm/DialogueServiceHelper;Lcom/squareup/ui/ErrorsBarPresenter;Ljava/util/Locale;Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/server/bills/BillListServiceHelper;Lcom/squareup/settings/server/Features;Ljava/text/DateFormat;)Lcom/squareup/ui/crm/ConversationPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/crm/ConversationPresenter_Factory;->get()Lcom/squareup/ui/crm/ConversationPresenter;

    move-result-object v0

    return-object v0
.end method
