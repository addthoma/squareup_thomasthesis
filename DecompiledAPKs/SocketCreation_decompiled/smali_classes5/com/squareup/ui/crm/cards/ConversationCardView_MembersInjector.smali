.class public final Lcom/squareup/ui/crm/cards/ConversationCardView_MembersInjector;
.super Ljava/lang/Object;
.source "ConversationCardView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/crm/cards/ConversationCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;",
            ">;)V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ConversationCardView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/crm/cards/ConversationCardView;",
            ">;"
        }
    .end annotation

    .line 22
    new-instance v0, Lcom/squareup/ui/crm/cards/ConversationCardView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/ConversationCardView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/crm/cards/ConversationCardView;Ljava/lang/Object;)V
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ConversationCardView;->presenter:Lcom/squareup/ui/crm/cards/ConversationCardScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/crm/cards/ConversationCardView;)V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ConversationCardView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/ConversationCardView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/ConversationCardView;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/crm/cards/ConversationCardView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/ConversationCardView_MembersInjector;->injectMembers(Lcom/squareup/ui/crm/cards/ConversationCardView;)V

    return-void
.end method
