.class public final Lcom/squareup/ui/crm/cards/AddCouponView_MembersInjector;
.super Ljava/lang/Object;
.source "AddCouponView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/crm/cards/AddCouponView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final wholeUnitMoneyHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/WholeUnitMoneyHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/WholeUnitMoneyHelper;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddCouponView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/AddCouponView_MembersInjector;->wholeUnitMoneyHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/WholeUnitMoneyHelper;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/crm/cards/AddCouponView;",
            ">;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/ui/crm/cards/AddCouponView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/AddCouponView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/crm/cards/AddCouponView;Ljava/lang/Object;)V
    .locals 0

    .line 37
    check-cast p1, Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->presenter:Lcom/squareup/ui/crm/cards/AddCouponScreen$Presenter;

    return-void
.end method

.method public static injectWholeUnitMoneyHelper(Lcom/squareup/ui/crm/cards/AddCouponView;Lcom/squareup/money/WholeUnitMoneyHelper;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddCouponView;->wholeUnitMoneyHelper:Lcom/squareup/money/WholeUnitMoneyHelper;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/crm/cards/AddCouponView;)V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/AddCouponView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/AddCouponView;Ljava/lang/Object;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCouponView_MembersInjector;->wholeUnitMoneyHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/WholeUnitMoneyHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/AddCouponView_MembersInjector;->injectWholeUnitMoneyHelper(Lcom/squareup/ui/crm/cards/AddCouponView;Lcom/squareup/money/WholeUnitMoneyHelper;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/crm/cards/AddCouponView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/AddCouponView_MembersInjector;->injectMembers(Lcom/squareup/ui/crm/cards/AddCouponView;)V

    return-void
.end method
