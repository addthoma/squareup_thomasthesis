.class Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$1;
.super Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;
.source "CustomerInvoiceCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->setUpRecyclerView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;

.field final synthetic val$layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;Landroidx/recyclerview/widget/LinearLayoutManager;)V
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$1;->this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$1;->val$layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrolled(Landroidx/recyclerview/widget/RecyclerView;II)V
    .locals 3

    .line 134
    invoke-super {p0, p1, p2, p3}, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;->onScrolled(Landroidx/recyclerview/widget/RecyclerView;II)V

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$1;->val$layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->getItemCount()I

    move-result p1

    .line 136
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$1;->val$layoutManager:Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p2}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result p2

    const/4 p3, 0x1

    sub-int/2addr p1, p3

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$1;->this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->access$000(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    const/4 v1, 0x0

    if-lt p2, p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$1;->this$0:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->access$100(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    add-int/lit8 p2, p2, 0x32

    if-lt p2, p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 p3, 0x0

    :goto_1
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
