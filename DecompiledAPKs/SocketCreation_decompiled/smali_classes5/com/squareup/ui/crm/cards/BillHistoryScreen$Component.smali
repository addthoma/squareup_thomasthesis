.class public interface abstract Lcom/squareup/ui/crm/cards/BillHistoryScreen$Component;
.super Ljava/lang/Object;
.source "BillHistoryScreen.java"

# interfaces
.implements Lcom/squareup/ui/activity/billhistory/BillHistoryView$Component;
.implements Lcom/squareup/ui/ErrorsBarView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/BillHistoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/crm/cards/BillHistoryCardView;)V
.end method
