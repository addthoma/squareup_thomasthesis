.class public Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;
.super Landroid/widget/LinearLayout;
.source "VisitFrequencyFilterContentView.java"

# interfaces
.implements Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;


# instance fields
.field private minimumVisitsHeader:Landroid/widget/TextView;

.field private minimumVisitsRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

.field presenter:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private timePeriodHeader:Landroid/widget/TextView;

.field private timePeriodList:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const-class p2, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;->inject(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;)V

    return-void
.end method


# virtual methods
.method addRow()Lcom/squareup/ui/crm/rows/CheckableRow;
    .locals 2

    .line 79
    sget v0, Lcom/squareup/crm/R$layout;->crm_single_select_row:I

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->timePeriodList:Landroid/widget/LinearLayout;

    .line 80
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/rows/CheckableRow;

    .line 81
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->timePeriodList:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method clearRows()V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->timePeriodList:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method public filter()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->filter()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public isValid()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->isValid()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onFinishInflate$0$VisitFrequencyFilterContentView(I)V
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->onMinimumVisitsChanged(I)V

    return-void
.end method

.method minimumVisitsRow()Lcom/squareup/register/widgets/list/EditQuantityRow;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->minimumVisitsRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->dropView(Ljava/lang/Object;)V

    .line 53
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 36
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 38
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_minimum_visits_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->minimumVisitsHeader:Landroid/widget/TextView;

    .line 39
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_minimum_visits:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/list/EditQuantityRow;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->minimumVisitsRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    .line 40
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_time_period_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->timePeriodHeader:Landroid/widget/TextView;

    .line 41
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_time_period_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->timePeriodList:Landroid/widget/LinearLayout;

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->minimumVisitsRow:Lcom/squareup/register/widgets/list/EditQuantityRow;

    new-instance v1, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentView$Dxhtp5DR6AmNkn2f_BDyhE7HheI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentView$Dxhtp5DR6AmNkn2f_BDyhE7HheI;-><init>(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setOnQuantityChangedListener(Lcom/squareup/register/widgets/list/EditQuantityRow$OnQuantityChangedListener;)V

    return-void
.end method

.method public setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->minimumVisitsHeader:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments_field_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->timePeriodHeader:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_field_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V

    return-void
.end method
