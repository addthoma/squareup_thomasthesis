.class public Lcom/squareup/ui/crm/cards/lookup/CustomerLookupHelper;
.super Ljava/lang/Object;
.source "CustomerLookupHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static contactLoaderSearchTerm(Lrx/Observable;Lrx/Observable;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;)",
            "Lrx/Observable<",
            "Lcom/squareup/crm/RolodexContactLoader$SearchTerm;",
            ">;"
        }
    .end annotation

    .line 19
    sget-object v0, Lcom/squareup/ui/crm/cards/lookup/-$$Lambda$CustomerLookupHelper$Ercq-aJog12h46hMj55M-UIpbvY;->INSTANCE:Lcom/squareup/ui/crm/cards/lookup/-$$Lambda$CustomerLookupHelper$Ercq-aJog12h46hMj55M-UIpbvY;

    .line 21
    invoke-virtual {p0, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/lookup/-$$Lambda$CustomerLookupHelper$6a_RlUhACylFF-hnAqAYSPoxQCs;->INSTANCE:Lcom/squareup/ui/crm/cards/lookup/-$$Lambda$CustomerLookupHelper$6a_RlUhACylFF-hnAqAYSPoxQCs;

    .line 22
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/lookup/-$$Lambda$CustomerLookupHelper$xT4tKgpga5JHcrucFeSEFA-V8ms;->INSTANCE:Lcom/squareup/ui/crm/cards/lookup/-$$Lambda$CustomerLookupHelper$xT4tKgpga5JHcrucFeSEFA-V8ms;

    .line 24
    invoke-virtual {p1, p0, v1}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p0

    sget-object p1, Lcom/squareup/ui/crm/cards/lookup/-$$Lambda$CustomerLookupHelper$uQMlDSuaAj0FLyOJaTR6-0jd4JA;->INSTANCE:Lcom/squareup/ui/crm/cards/lookup/-$$Lambda$CustomerLookupHelper$uQMlDSuaAj0FLyOJaTR6-0jd4JA;

    .line 27
    invoke-virtual {p0, p1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p0

    .line 19
    invoke-static {v0, p0}, Lrx/Observable;->merge(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$contactLoaderSearchTerm$0(Ljava/lang/String;)Lcom/squareup/crm/RolodexContactLoader$SearchTerm;
    .locals 2

    .line 21
    new-instance v0, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method

.method static synthetic lambda$contactLoaderSearchTerm$1(Lcom/squareup/crm/RolodexContactLoader$SearchTerm;)Ljava/lang/Boolean;
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 22
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$contactLoaderSearchTerm$2(Lkotlin/Unit;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    return-object p1
.end method

.method static synthetic lambda$contactLoaderSearchTerm$3(Ljava/lang/String;)Lcom/squareup/crm/RolodexContactLoader$SearchTerm;
    .locals 2

    .line 27
    new-instance v0, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method
