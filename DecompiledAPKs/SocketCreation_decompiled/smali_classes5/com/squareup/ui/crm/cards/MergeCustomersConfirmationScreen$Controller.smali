.class public interface abstract Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;
.super Ljava/lang/Object;
.source "MergeCustomersConfirmationScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract cancelMergeCustomersConfirmationScreen()V
.end method

.method public abstract getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;
.end method

.method public abstract showMergingCustomersScreen()V
.end method
