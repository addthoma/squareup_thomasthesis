.class public interface abstract Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Controller;
.super Ljava/lang/Object;
.source "DeletingCustomersScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/DeletingCustomersScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract closeDeletingCustomersScreen()V
.end method

.method public abstract getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;
.end method

.method public abstract success()V
.end method
