.class public interface abstract Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;
.super Ljava/lang/Object;
.source "SendMessageScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SendMessageScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeSendMessageScreen()V
.end method

.method public abstract closeSendMessageScreen(Lcom/squareup/protos/client/dialogue/Conversation;)V
.end method

.method public abstract getContactForSendMessageScreen()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract getMessageForSendMessageScreen()Ljava/lang/String;
.end method

.method public abstract showAddCouponScreen(Ljava/lang/String;)V
.end method
