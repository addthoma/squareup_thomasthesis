.class Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;
.super Lmortar/Presenter;
.source "MergingDuplicatesPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;",
        ">;"
    }
.end annotation


# static fields
.field static final AUTO_CLOSE_SECONDS:J = 0x3L


# instance fields
.field private final autoClose:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final controller:Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Controller;

.field private final mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

.field private final mergeSuccess:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/MergeProposalLoader;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 43
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 39
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->mergeSuccess:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->controller:Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Controller;

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->res:Lcom/squareup/util/Res;

    .line 46
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 47
    iput-object p6, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

    .line 49
    new-instance p1, Lcom/squareup/util/RxWatchdog;

    invoke-direct {p1, p4, p5}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    return-void
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;)Lmortar/bundler/BundleService;
    .locals 0

    .line 53
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->extractBundleService(Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$0$MergingDuplicatesPresenter(Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->mergeSuccess:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$1$MergingDuplicatesPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->mergeSuccess:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$3$MergingDuplicatesPresenter(Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;Ljava/lang/Boolean;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 79
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;->hideProgress()V

    .line 81
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 82
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 83
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/applet/R$string;->crm_duplicates_merged_format:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->controller:Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Controller;

    .line 84
    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Controller;->getExpectedDuplicateCount()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "number"

    invoke-virtual {p2, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 85
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p2

    .line 83
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;->showText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->controller:Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Controller;->success()V

    goto :goto_0

    .line 89
    :cond_0
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 90
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/applet/R$string;->crm_duplicates_merged_error:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;->showText(Ljava/lang/CharSequence;)V

    .line 93
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->mergeProposalLoader:Lcom/squareup/crm/MergeProposalLoader;

    invoke-virtual {p1}, Lcom/squareup/crm/MergeProposalLoader;->refresh()V

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    const-wide/16 v0, 0x3

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public synthetic lambda$null$5$MergingDuplicatesPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->controller:Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Controller;->closeMergingDuplicatesScreen()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$MergingDuplicatesPresenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 62
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$pyfrY22GIi--iEqWVXGoy9zDgN0;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$pyfrY22GIi--iEqWVXGoy9zDgN0;-><init>(Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$HIYF50tqdL2RRNFiaKR3d_w2ZpI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$HIYF50tqdL2RRNFiaKR3d_w2ZpI;-><init>(Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onLoad$4$MergingDuplicatesPresenter(Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->mergeSuccess:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$XEIqkG-bsMicr-YdWhtXrK5K3AU;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$XEIqkG-bsMicr-YdWhtXrK5K3AU;-><init>(Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;)V

    .line 78
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$MergingDuplicatesPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$wG8DWIPMPM-OUaCp5L0_ryK0Z9c;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$wG8DWIPMPM-OUaCp5L0_ryK0Z9c;-><init>(Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;)V

    .line 100
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 57
    invoke-super {p0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 61
    invoke-interface {v0}, Lcom/squareup/crm/RolodexServiceHelper;->runMergeAllJob()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$d6A9RvZ712WJ-FbNpLG6t9GasbA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$d6A9RvZ712WJ-FbNpLG6t9GasbA;-><init>(Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;)V

    .line 62
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSuccess(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 60
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 70
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    .line 71
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_merging_duplicates:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;->showText(Ljava/lang/CharSequence;)V

    .line 76
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$FT3sXwGJ7NBQCQspwTaojnoxnPs;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$FT3sXwGJ7NBQCQspwTaojnoxnPs;-><init>(Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 98
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/MergingDuplicatesDialog;->getView()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$JQ8RbnEhMV97bkCG6KuzDDTz6K8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergingDuplicatesPresenter$JQ8RbnEhMV97bkCG6KuzDDTz6K8;-><init>(Lcom/squareup/ui/crm/cards/MergingDuplicatesPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
