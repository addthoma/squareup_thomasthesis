.class public Lcom/squareup/ui/crm/cards/ChooseGroupsView;
.super Landroid/widget/LinearLayout;
.source "ChooseGroupsView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private container:Landroid/widget/LinearLayout;

.field private createNew:Landroid/widget/Button;

.field private groupTokenToScrollTo:Ljava/lang/String;

.field private message:Lcom/squareup/marketfont/MarketTextView;

.field presenter:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;

.field private scrollView:Landroid/widget/ScrollView;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const-class p2, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Component;->inject(Lcom/squareup/ui/crm/cards/ChooseGroupsView;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->shortAnimTimeMs:I

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 133
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 134
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_groups_scroll_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->scrollView:Landroid/widget/ScrollView;

    .line 135
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_groups_create_new:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->createNew:Landroid/widget/Button;

    .line 136
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_groups_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->container:Landroid/widget/LinearLayout;

    .line 137
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_groups_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->message:Lcom/squareup/marketfont/MarketTextView;

    .line 138
    sget v0, Lcom/squareup/crmupdatecustomer/R$id;->crm_groups_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->progressBar:Landroid/widget/ProgressBar;

    return-void
.end method

.method private newGroupRow(Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/ui/CheckBoxListRow;
    .locals 4

    .line 111
    new-instance v0, Lcom/squareup/ui/CheckBoxListRow;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/CheckBoxListRow;-><init>(Landroid/content/Context;)V

    .line 113
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->presenter:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;

    invoke-virtual {v2, p1}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->isChosen(Lcom/squareup/protos/client/rolodex/Group;)Z

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/squareup/ui/CheckBoxListRow;->showItem(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 114
    new-instance v1, Lcom/squareup/ui/crm/cards/ChooseGroupsView$2;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/ui/crm/cards/ChooseGroupsView$2;-><init>(Lcom/squareup/ui/crm/cards/ChooseGroupsView;Lcom/squareup/ui/CheckBoxListRow;Lcom/squareup/protos/client/rolodex/Group;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/CheckBoxListRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method


# virtual methods
.method public synthetic lambda$refresh$0$ChooseGroupsView(Landroid/view/View;)V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->scrollView:Landroid/widget/ScrollView;

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result p1

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 58
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->presenter:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->presenter:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 64
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 48
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->bindViews()V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->createNew:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/crm/cards/ChooseGroupsView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/ChooseGroupsView$1;-><init>(Lcom/squareup/ui/crm/cards/ChooseGroupsView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method refresh()V
    .locals 4

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->presenter:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->getAllManualGroups()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Group;

    .line 70
    invoke-direct {p0, v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->newGroupRow(Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/ui/CheckBoxListRow;

    move-result-object v2

    .line 71
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 73
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->groupTokenToScrollTo:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 74
    iput-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->groupTokenToScrollTo:Ljava/lang/String;

    .line 77
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsView$vtEBMy6Q8hbcCjmJ0q1m-YNLIrY;

    invoke-direct {v1, p0, v2}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseGroupsView$vtEBMy6Q8hbcCjmJ0q1m-YNLIrY;-><init>(Lcom/squareup/ui/crm/cards/ChooseGroupsView;Landroid/view/View;)V

    invoke-virtual {p0, v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setActionBarUpButtonEnabled(Z)V
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    return-void
.end method

.method setGroupTokenToScrollTo(Ljava/lang/String;)V
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->groupTokenToScrollTo:Ljava/lang/String;

    return-void
.end method

.method setMessage(Ljava/lang/String;)V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->message:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showGroups(Z)V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->container:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showMessage(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 100
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->message:Lcom/squareup/marketfont/MarketTextView;

    iget v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 102
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->message:Lcom/squareup/marketfont/MarketTextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->progressBar:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->shortAnimTimeMs:I

    invoke-static {p1, v0, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;II)V

    goto :goto_0

    .line 86
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->progressBar:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method
