.class final Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo$1;
.super Ljava/lang/Object;
.source "BirthdayInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;
    .locals 7

    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 55
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v0, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    :cond_0
    const/4 v6, 0x0

    .line 58
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    if-eqz p1, :cond_1

    const/4 v5, 0x1

    .line 59
    :cond_1
    new-instance p1, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;

    move-object v0, p1

    move v4, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;-><init>(IIIZZ)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 52
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;
    .locals 0

    .line 63
    new-array p1, p1, [Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 52
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo$1;->newArray(I)[Lcom/squareup/ui/crm/cards/birthday/BirthdayInfo;

    move-result-object p1

    return-object p1
.end method
