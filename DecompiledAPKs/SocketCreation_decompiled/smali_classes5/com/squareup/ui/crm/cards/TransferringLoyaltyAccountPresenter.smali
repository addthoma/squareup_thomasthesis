.class public Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;
.super Lmortar/Presenter;
.source "TransferringLoyaltyAccountPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;",
        ">;"
    }
.end annotation


# static fields
.field static final AUTO_CLOSE_SECONDS:J = 0x3L

.field static final MIN_LATENCY_SECONDS:J = 0x1L


# instance fields
.field private final autoClose:Lcom/squareup/util/RxWatchdog;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/RxWatchdog<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final received:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 45
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 41
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->received:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->runner:Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;

    .line 47
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->res:Lcom/squareup/util/Res;

    .line 48
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    .line 49
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 51
    new-instance p1, Lcom/squareup/util/RxWatchdog;

    invoke-direct {p1, p4, p5}, Lcom/squareup/util/RxWatchdog;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Ljava/lang/Long;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p0
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;)Lmortar/bundler/BundleService;
    .locals 0

    .line 55
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->extractBundleService(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$TransferringLoyaltyAccountPresenter(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 83
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 84
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_transferring_success:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;->showText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->runner:Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;

    const/4 p2, 0x1

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;->closeTransferringLoyaltyAccountScreen(Z)V

    return-void
.end method

.method public synthetic lambda$null$2$TransferringLoyaltyAccountPresenter(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 89
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;->showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 90
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_transferring_failure:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;->showText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public synthetic lambda$null$3$TransferringLoyaltyAccountPresenter(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 80
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;->hideProgress()V

    .line 81
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$vFTXb8lVJOyvgDzsdXOzAPkBIe4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$vFTXb8lVJOyvgDzsdXOzAPkBIe4;-><init>(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$5rt-dC-K5YgzhpuldEyCFYb4HSo;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$5rt-dC-K5YgzhpuldEyCFYb4HSo;-><init>(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-virtual {p1, p2, v1, v2, v0}, Lcom/squareup/util/RxWatchdog;->restart(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public synthetic lambda$null$5$TransferringLoyaltyAccountPresenter(Lkotlin/Unit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->runner:Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->received:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 100
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 99
    invoke-interface {p1, v0}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;->closeTransferringLoyaltyAccountScreen(Z)V

    return-void
.end method

.method public synthetic lambda$onLoad$4$TransferringLoyaltyAccountPresenter(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->received:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$JXDmX-5768Qfh4h5SE5rnKoAQ2o;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$JXDmX-5768Qfh4h5SE5rnKoAQ2o;-><init>(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$TransferringLoyaltyAccountPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->autoClose:Lcom/squareup/util/RxWatchdog;

    invoke-virtual {v0}, Lcom/squareup/util/RxWatchdog;->timeout()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$48J_GlLlDE4_XJP75MAGOVsTDjY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$48J_GlLlDE4_XJP75MAGOVsTDjY;-><init>(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;)V

    .line 99
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    .line 59
    invoke-super {p0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->runner:Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;

    .line 63
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;->getSourceLoyaltyAccountToken()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->runner:Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;

    .line 64
    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;->transferLoyaltyTargetContactToken()Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-virtual {v0, v1, v2}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->transferLoyaltyAccount(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v3, 0x1

    .line 67
    invoke-static {v3, v4, v1, v2}, Lio/reactivex/Observable;->timer(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$qOgV-c-pS_qEPA3MuT2yvtSkWZY;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$qOgV-c-pS_qEPA3MuT2yvtSkWZY;

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->zipWith(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->received:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 68
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 62
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 72
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_transferring_account:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;->showText(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;->getView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$tY45jfbqKbAAc0NAeskJSYByJVY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$tY45jfbqKbAAc0NAeskJSYByJVY;-><init>(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 98
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountDialog;->getView()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$nPfLfjOOkZeJzL3TJV0X4WS8UXY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$TransferringLoyaltyAccountPresenter$nPfLfjOOkZeJzL3TJV0X4WS8UXY;-><init>(Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
