.class Lcom/squareup/ui/crm/cards/ChooseGroupsView$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ChooseGroupsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/ChooseGroupsView;->newGroupRow(Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/ui/CheckBoxListRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/ChooseGroupsView;

.field final synthetic val$group:Lcom/squareup/protos/client/rolodex/Group;

.field final synthetic val$row:Lcom/squareup/ui/CheckBoxListRow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/ChooseGroupsView;Lcom/squareup/ui/CheckBoxListRow;Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView$2;->this$0:Lcom/squareup/ui/crm/cards/ChooseGroupsView;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView$2;->val$row:Lcom/squareup/ui/CheckBoxListRow;

    iput-object p3, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView$2;->val$group:Lcom/squareup/protos/client/rolodex/Group;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView$2;->val$row:Lcom/squareup/ui/CheckBoxListRow;

    invoke-virtual {p1}, Lcom/squareup/ui/CheckBoxListRow;->toggle()V

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView$2;->this$0:Lcom/squareup/ui/crm/cards/ChooseGroupsView;

    iget-object p1, p1, Lcom/squareup/ui/crm/cards/ChooseGroupsView;->presenter:Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView$2;->val$group:Lcom/squareup/protos/client/rolodex/Group;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseGroupsView$2;->val$row:Lcom/squareup/ui/CheckBoxListRow;

    invoke-virtual {v1}, Lcom/squareup/ui/CheckBoxListRow;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/crm/cards/ChooseGroupsScreen$Presenter;->toggleChosen(Lcom/squareup/protos/client/rolodex/Group;Z)V

    return-void
.end method
