.class Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "CreateNoteScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CreateNoteScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/CreateNoteView;",
        ">;"
    }
.end annotation


# static fields
.field private static final KEY_UNIQUE_KEY:Ljava/lang/String; = "uniqueKey"


# instance fields
.field private final busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private createNoteDisposable:Lio/reactivex/disposables/Disposable;

.field private final errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final locale:Ljava/util/Locale;

.field private final reminderDateFormatter:Ljava/text/DateFormat;

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final runner:Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final timeFormatter:Ljava/text/DateFormat;

.field private uniqueKey:Ljava/util/UUID;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .param p9    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 110
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, 0x0

    .line 102
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 103
    invoke-static {}, Lio/reactivex/disposables/Disposables;->empty()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->createNoteDisposable:Lio/reactivex/disposables/Disposable;

    .line 111
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;

    .line 112
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 113
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 114
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 115
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 116
    iput-object p6, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->reminderDateFormatter:Ljava/text/DateFormat;

    .line 117
    iput-object p7, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 118
    iput-object p8, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->locale:Ljava/util/Locale;

    .line 119
    iput-object p9, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    const/4 p1, 0x1

    .line 121
    invoke-virtual {p2, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    return-void
.end method

.method private formatReminderTimestamp(Lcom/squareup/protos/client/rolodex/Reminder;)Ljava/lang/String;
    .locals 3

    if-eqz p1, :cond_1

    .line 192
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Reminder;->scheduled_at:Lcom/squareup/protos/common/time/DateTime;

    if-nez v0, :cond_0

    goto :goto_0

    .line 195
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Reminder;->scheduled_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->locale:Ljava/util/Locale;

    invoke-static {p1, v0}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p1

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/R$string;->date_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->reminderDateFormatter:Ljava/text/DateFormat;

    .line 197
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "date"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 198
    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    const-string/jumbo v1, "time"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 199
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 193
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmscreens/R$string;->crm_reminder_none:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$null$4(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 158
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private noteMaxChars()Ljava/lang/CharSequence;
    .locals 3

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_note_char_max:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmscreens/R$integer;->crm_note_max_length:I

    .line 239
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result v1

    const-string v2, "max_chars"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 240
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public synthetic lambda$null$11$CreateNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateNoteView;Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 226
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 227
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;

    .line 228
    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;->getContactForCreateNoteScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/UpsertNoteResponse;->note:Lcom/squareup/protos/client/rolodex/Note;

    invoke-static {p1, p2}, Lcom/squareup/crm/util/RolodexContactHelper;->withAddedNote(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Note;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    .line 229
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;

    invoke-interface {p2, p1}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;->closeCreateNoteScreen(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public synthetic lambda$null$12$CreateNoteScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 232
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 233
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_note_saving_error:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p1, v1, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$2$CreateNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateNoteView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 148
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/CreateNoteView;->setActionBarUpButtonEnabled(Z)V

    .line 149
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/CreateNoteView;->setEnabled(Z)V

    .line 150
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/CreateNoteView;->showProgress(Z)V

    return-void
.end method

.method public synthetic lambda$null$5$CreateNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateNoteView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 162
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/CreateNoteView;->setActionBarPrimaryButtonEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$null$7$CreateNoteScreen$Presenter(Lkotlin/Unit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 171
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;->getReminder()Lcom/squareup/protos/client/rolodex/Reminder;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;->showReminderScreen(Lcom/squareup/protos/client/rolodex/Reminder;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$CreateNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateNoteView;)V
    .locals 0

    .line 136
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 137
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;->closeCreateNoteScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$CreateNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateNoteView;)V
    .locals 1

    .line 140
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CreateNoteView;->getNote()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->onSavePressed(Lcom/squareup/ui/crm/cards/CreateNoteView;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$onLoad$3$CreateNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateNoteView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$o4XKkuYP_OYENhBJwDgFu9dGivw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$o4XKkuYP_OYENhBJwDgFu9dGivw;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateNoteView;)V

    .line 146
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$CreateNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateNoteView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 157
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CreateNoteView;->noteIsBlank()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$I_zUbuTrm4fYJYaY7Sh5ePIrUJo;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$I_zUbuTrm4fYJYaY7Sh5ePIrUJo;

    .line 155
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$Q1OVl0tS_ZnpCpssa4AP6MPRnwE;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$Q1OVl0tS_ZnpCpssa4AP6MPRnwE;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateNoteView;)V

    .line 160
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$8$CreateNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateNoteView;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 170
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CreateNoteView;->onReminderClicked()Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$FTltAK3g5xRjmf9wQlWrOD3yPH4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$FTltAK3g5xRjmf9wQlWrOD3yPH4;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;)V

    .line 171
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onSavePressed$10$CreateNoteScreen$Presenter()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onSavePressed$13$CreateNoteScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateNoteView;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 223
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$0IIjk9t9_MiKNyNVLtYccnfR9wA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$0IIjk9t9_MiKNyNVLtYccnfR9wA;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateNoteView;)V

    new-instance p1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$AE0eCWTaGPnLu3UwvAmBaPR4qRg;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$AE0eCWTaGPnLu3UwvAmBaPR4qRg;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;)V

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onSavePressed$9$CreateNoteScreen$Presenter(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 216
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 217
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->createNoteDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 126
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 130
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 131
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/CreateNoteView;

    .line 133
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmscreens/R$string;->crm_create_note_title:I

    .line 134
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$Edl5pvVtqJBR0MCi2x1b17cWR3M;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$Edl5pvVtqJBR0MCi2x1b17cWR3M;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateNoteView;)V

    .line 135
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->save:I

    .line 139
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$qRXLl-RPfxNFxkzUp7Fa_Q1Gg0s;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$qRXLl-RPfxNFxkzUp7Fa_Q1Gg0s;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateNoteView;)V

    .line 140
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 141
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 133
    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/CreateNoteView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 144
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$Iptn9_cNO-721jDd2jNLCPle6PM;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$Iptn9_cNO-721jDd2jNLCPle6PM;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateNoteView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 154
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$gxHUFO1GVb3pjWJnwxhN0iSo7jA;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$gxHUFO1GVb3pjWJnwxhN0iSo7jA;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateNoteView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 165
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CRM_REMINDERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;->getReminder()Lcom/squareup/protos/client/rolodex/Reminder;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->formatReminderTimestamp(Lcom/squareup/protos/client/rolodex/Reminder;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/CreateNoteView;->showReminder(Ljava/lang/String;)V

    .line 169
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$ZsjJ8WSOi-9NkYir7m8kIiDtS8s;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$ZsjJ8WSOi-9NkYir7m8kIiDtS8s;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateNoteView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 175
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-interface {v1}, Lcom/squareup/crm/RolodexServiceHelper;->getCurrentEmployee()Lcom/squareup/protos/client/Employee;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 177
    iget-object v1, v1, Lcom/squareup/protos/client/Employee;->read_only_full_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/CreateNoteView;->showMessageLeft(Ljava/lang/CharSequence;)V

    .line 178
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->noteMaxChars()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/CreateNoteView;->showMessageRight(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 180
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->noteMaxChars()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/CreateNoteView;->showMessageLeft(Ljava/lang/CharSequence;)V

    :goto_0
    if-nez p1, :cond_2

    .line 184
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    .line 185
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/CreateNoteView;->setInitialFocus()V

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "uniqueKey"

    .line 187
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    :goto_1
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "uniqueKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method onSavePressed(Lcom/squareup/ui/crm/cards/CreateNoteView;Ljava/lang/String;)V
    .locals 4

    .line 208
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;->getContactForCreateNoteScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    .line 211
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;->getReminder()Lcom/squareup/protos/client/rolodex/Reminder;

    move-result-object v1

    .line 213
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->createNoteDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v2}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 214
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    invoke-interface {v2, v0, p2, v1, v3}, Lcom/squareup/crm/RolodexServiceHelper;->createNote(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Reminder;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$u33nbBPySEMpCj2IMJ8h4bHVKwI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$u33nbBPySEMpCj2IMJ8h4bHVKwI;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;)V

    .line 215
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$UIQZHAAoqEWB-ZEixcqCLvpJxEo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$UIQZHAAoqEWB-ZEixcqCLvpJxEo;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;)V

    .line 219
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$REY2SZtkzx5xaqqJVk0m9O9Yh6Q;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateNoteScreen$Presenter$REY2SZtkzx5xaqqJVk0m9O9Yh6Q;-><init>(Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateNoteView;)V

    .line 223
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CreateNoteScreen$Presenter;->createNoteDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method
