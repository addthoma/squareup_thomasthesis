.class public Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;
.super Landroid/widget/LinearLayout;
.source "SingleTextFilterContentView.java"

# interfaces
.implements Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;


# instance fields
.field private editText:Lcom/squareup/ui/XableEditText;

.field private final onTextChanged:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->onTextChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 31
    const-class p2, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;->inject(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->onTextChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method


# virtual methods
.method public filter()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->filter()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public isValid()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->isValid()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->dropView(Ljava/lang/Object;)V

    .line 53
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 35
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 37
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_text_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->editText:Lcom/squareup/ui/XableEditText;

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->editText:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView$1;-><init>(Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method onTextChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->onTextChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentPresenter;->setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V

    return-void
.end method

.method setInputType(I)V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->editText:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SelectableEditText;->setInputType(I)V

    return-void
.end method

.method showHint(Ljava/lang/String;)V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->editText:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method showText(Ljava/lang/String;)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;->editText:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
