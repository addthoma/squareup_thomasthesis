.class Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ChooseFilterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ChooseFilterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/ChooseFilterCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final controller:Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;

.field private final errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final filterHelper:Lcom/squareup/crm/filters/FilterHelper;

.field private final filterLoader:Lcom/squareup/crm/FilterTemplateLoader;

.field private numberOfVisibleFilters:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/crm/FilterTemplateLoader;Lcom/squareup/crm/filters/FilterHelper;Lcom/squareup/ui/ErrorsBarPresenter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 79
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, 0x0

    .line 75
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->numberOfVisibleFilters:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 80
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;

    .line 81
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 82
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->filterLoader:Lcom/squareup/crm/FilterTemplateLoader;

    .line 83
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->filterHelper:Lcom/squareup/crm/filters/FilterHelper;

    .line 84
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    return-void
.end method

.method private bind(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)V
    .locals 2

    .line 142
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 143
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->VISIBLE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    .line 145
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 147
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$uLdpMp_mx_gVaSYz2_FqyIbiNpI;

    invoke-direct {v1, p0, p3, v0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$uLdpMp_mx_gVaSYz2_FqyIbiNpI;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFilterCardView;Ljava/lang/String;Lcom/squareup/ui/account/view/SmartLineRow;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 156
    new-instance p3, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$-kK8bBPJXx48gHh4af0hnxha6Is;

    invoke-direct {p3, p0, p1, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$-kK8bBPJXx48gHh4af0hnxha6Is;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Filter;)V

    invoke-static {p1, p3}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 126
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->showFilterContainer(Z)V

    .line 127
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->showProgress(Z)V

    return-void
.end method

.method static synthetic lambda$null$6(Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 136
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p0

    if-nez p0, :cond_0

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$8(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 149
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    .line 150
    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    .line 149
    invoke-virtual {p0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/16 p0, 0x8

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x0

    :goto_1
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$bind$10$ChooseFilterScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;Ljava/lang/String;Lcom/squareup/ui/account/view/SmartLineRow;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 148
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->searchText()Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$-s3QoVLth9vCKUMam8TSXOSV5ec;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$-s3QoVLth9vCKUMam8TSXOSV5ec;-><init>(Ljava/lang/String;)V

    .line 149
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x0

    .line 151
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/util/rx2/Rx2TransformersKt;->distinctUntilChangedWithFirstValueToSkip(Ljava/lang/Object;)Lio/reactivex/ObservableTransformer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$u1uofvrmLpEeAnS9Q4n8yl8Z3d8;

    invoke-direct {p2, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$u1uofvrmLpEeAnS9Q4n8yl8Z3d8;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;)V

    .line 152
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance p2, Lcom/squareup/ui/crm/cards/-$$Lambda$DFH_IVSgwbBKxq7_MC2nt3O2qGY;

    invoke-direct {p2, p3}, Lcom/squareup/ui/crm/cards/-$$Lambda$DFH_IVSgwbBKxq7_MC2nt3O2qGY;-><init>(Lcom/squareup/ui/account/view/SmartLineRow;)V

    .line 154
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$bind$12$ChooseFilterScreen$Presenter(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Filter;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 157
    invoke-static {p1}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$w60T0ZgveODgD1IIVmtKh-BT_rQ;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$w60T0ZgveODgD1IIVmtKh-BT_rQ;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;Lcom/squareup/protos/client/rolodex/Filter;)V

    .line 158
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$0$ChooseFilterScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 101
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->removeAllFilterRows()V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->numberOfVisibleFilters:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 104
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter;

    .line 105
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->addFilterRow()Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v1

    invoke-direct {p0, v1, v0, p1}, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->bind(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/Filter;Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$11$ChooseFilterScreen$Presenter(Lcom/squareup/protos/client/rolodex/Filter;Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 158
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;

    invoke-interface {p2, p1}, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;->showEditFilterScreen(Lcom/squareup/protos/client/rolodex/Filter;)V

    return-void
.end method

.method public synthetic lambda$null$2$ChooseFilterScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 113
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    const-string v0, ""

    if-eqz p2, :cond_0

    .line 114
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->removeAllFilterRows()V

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_failed_to_load_filters:I

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 117
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->errorsBar:Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$9$ChooseFilterScreen$Presenter(Ljava/lang/Integer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->numberOfVisibleFilters:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 153
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, -0x1

    :goto_0
    add-int/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    .line 152
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$ChooseFilterScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->filterLoader:Lcom/squareup/crm/FilterTemplateLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/FilterTemplateLoader;->results()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->filterHelper:Lcom/squareup/crm/filters/FilterHelper;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;

    .line 99
    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;->getCurrentFilters()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/crm/filters/FilterHelper;->excludeFilterTemplates(Ljava/util/List;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$aUiK7QJjWMoB1SPWVaSfx7KvFzA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$aUiK7QJjWMoB1SPWVaSfx7KvFzA;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)V

    .line 100
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$ChooseFilterScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->filterLoader:Lcom/squareup/crm/FilterTemplateLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/FilterTemplateLoader;->failure()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$HTrq53ADfgdndIsr_THTWushJ2I;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$HTrq53ADfgdndIsr_THTWushJ2I;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)V

    .line 112
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$5$ChooseFilterScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->filterLoader:Lcom/squareup/crm/FilterTemplateLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/FilterTemplateLoader;->progress()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 124
    invoke-static {v1}, Lcom/squareup/util/rx2/Rx2TransformersKt;->distinctUntilChangedWithFirstValueToSkip(Ljava/lang/Object;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$TQHDqf8iXVhWbXxhCLv40PGhkRk;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$TQHDqf8iXVhWbXxhCLv40PGhkRk;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)V

    .line 125
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$7$ChooseFilterScreen$Presenter(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)Lio/reactivex/disposables/Disposable;
    .locals 4

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->filterLoader:Lcom/squareup/crm/FilterTemplateLoader;

    .line 133
    invoke-virtual {v0}, Lcom/squareup/crm/FilterTemplateLoader;->results()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->numberOfVisibleFilters:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 135
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->searchText()Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$SCa6p00t75jHt1gkQlI9SIMvXrQ;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$SCa6p00t75jHt1gkQlI9SIMvXrQ;

    .line 132
    invoke-static {v0, v1, v2, v3}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function3;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 137
    invoke-static {v1}, Lcom/squareup/util/rx2/Rx2TransformersKt;->distinctUntilChangedWithFirstValueToSkip(Ljava/lang/Object;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$xF-Sqh8G4djgIjSa6sT6JZaUYy4;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$xF-Sqh8G4djgIjSa6sT6JZaUYy4;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)V

    .line 138
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 88
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;

    .line 90
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/ChooseFilterCardView;->actionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 92
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crm/applet/R$string;->crm_choose_filters_title:I

    .line 93
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 92
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$IaEx4UTk3V2tqnrJTuLJfo_S8cg;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$IaEx4UTk3V2tqnrJTuLJfo_S8cg;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 97
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$p3vgeX60JfX9325OsymjPhdSobc;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$p3vgeX60JfX9325OsymjPhdSobc;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 110
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$_8DKR72MUuA4uIe6JpSie_gVrD0;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$_8DKR72MUuA4uIe6JpSie_gVrD0;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 122
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$IWv3krXUBqoaYbMeErCyBgpBEao;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$IWv3krXUBqoaYbMeErCyBgpBEao;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 131
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$RrhWHr_BrCcYwV0AkoZQpNxIurA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ChooseFilterScreen$Presenter$RrhWHr_BrCcYwV0AkoZQpNxIurA;-><init>(Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Presenter;Lcom/squareup/ui/crm/cards/ChooseFilterCardView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
