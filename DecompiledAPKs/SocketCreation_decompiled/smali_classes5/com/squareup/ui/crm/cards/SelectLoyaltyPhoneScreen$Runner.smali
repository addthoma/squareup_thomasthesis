.class public interface abstract Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;
.super Ljava/lang/Object;
.source "SelectLoyaltyPhoneScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeSelectLoyaltyPhoneScreen()V
.end method

.method public abstract getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;
.end method

.method public abstract getLoyaltyAccounts()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTargetLoyaltyAccountMapping()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ">;"
        }
    .end annotation
.end method

.method public abstract selectLoyaltyPhoneScreenBusy()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setTargetLoyaltyAccountMapping(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract showMergeCustomersScreen()V
.end method
