.class Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;
.super Lmortar/ViewPresenter;
.source "MergeProposalListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;",
        ">;"
    }
.end annotation


# static fields
.field static final ITEM_TYPE_MERGE_PROPOSAL:I = 0x1

.field static final ITEM_TYPE_PROGRESS_ERROR:I = 0x2

.field static final NEXT_PAGE_SIZE:I = 0x19


# instance fields
.field private final activeItemSubs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lio/reactivex/disposables/Disposable;",
            ">;"
        }
    .end annotation
.end field

.field private latestResults:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results<",
            "Lkotlin/Unit;",
            "Lcom/squareup/protos/client/rolodex/MergeProposal;",
            ">;"
        }
    .end annotation
.end field

.field private final loader:Lcom/squareup/crm/MergeProposalLoader;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final onActiveItemsChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onUncheckedItemsChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private showError:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private showProgress:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final showProgressErrorItem:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final uncheckedItemPositions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/crm/MergeProposalLoader;Lio/reactivex/Scheduler;Lcom/squareup/crm/RolodexServiceHelper;)V
    .locals 1
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 61
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 47
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->showProgressErrorItem:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 51
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->onUncheckedItemsChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 52
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->uncheckedItemPositions:Ljava/util/Set;

    .line 54
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->onActiveItemsChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 55
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->activeItemSubs:Ljava/util/Map;

    .line 57
    sget-object v0, Lcom/squareup/crm/MergeProposalLoader;->EMPTY_RESULTS:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->latestResults:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->loader:Lcom/squareup/crm/MergeProposalLoader;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 64
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    return-void
.end method

.method private isItemActive(I)Z
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->activeItemSubs:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private isItemChecked(I)Z
    .locals 1

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->uncheckedItemPositions:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method static synthetic lambda$null$10(Lkotlin/Unit;)Ljava/lang/Integer;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/16 p0, 0x19

    .line 149
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$17(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;Lkotlin/Unit;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 189
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->isChecked()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$5(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x0

    .line 113
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    const/4 v2, 0x1

    .line 114
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    .line 116
    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItems()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v2, :cond_1

    .line 118
    invoke-virtual {v1}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->hasItems()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119
    invoke-virtual {v1}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItemCount()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->notifyItemRangeRemoved(II)V

    .line 121
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->hasItems()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 122
    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItemCount()I

    move-result p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->notifyItemRangeInserted(II)V

    goto :goto_0

    .line 125
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItemCount()I

    move-result p1

    invoke-virtual {v1}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItemCount()I

    move-result v0

    sub-int/2addr p1, v0

    if-lez p1, :cond_2

    .line 127
    invoke-virtual {v1}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItemCount()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->notifyItemRangeInserted(II)V

    :cond_2
    :goto_0
    return-void
.end method

.method static synthetic lambda$null$9(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;)Lio/reactivex/ObservableSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 148
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->onNearEndOfList()Lio/reactivex/Observable;

    move-result-object p0

    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 72
    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;->isFirstPage()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$1(Lcom/squareup/datafetch/AbstractLoader$LoaderState$Failure;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 78
    invoke-virtual {p0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Failure;->isFirstPage()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$2(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 84
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_1

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private setItemActive(ILio/reactivex/disposables/Disposable;)V
    .locals 3

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    .line 253
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->activeItemSubs:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v0, v1

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->activeItemSubs:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 256
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->activeItemSubs:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/disposables/Disposable;

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 257
    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 258
    invoke-interface {p1}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 260
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->onActiveItemsChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private setItemChecked(IZ)V
    .locals 0

    if-eqz p2, :cond_0

    .line 240
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->uncheckedItemPositions:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 242
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->uncheckedItemPositions:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 244
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->onUncheckedItemsChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method bind(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;I)V
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->latestResults:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    invoke-virtual {v0, p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/MergeProposal;

    .line 168
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->show(Lcom/squareup/protos/client/rolodex/MergeProposal;)V

    .line 171
    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$K3dAfsBMDe5TnrAHTCwjWoPlZcc;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$K3dAfsBMDe5TnrAHTCwjWoPlZcc;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;ILcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 178
    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$4iLhvZrGSvRrAdUeo9F1oXXQ0rQ;

    invoke-direct {v1, p0, p2, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$4iLhvZrGSvRrAdUeo9F1oXXQ0rQ;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;ILcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 186
    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$vOIztvg2hm9vIHhzMl8E_Z8WMtg;

    invoke-direct {v1, p0, p1, p2, v0}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$vOIztvg2hm9vIHhzMl8E_Z8WMtg;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;ILcom/squareup/protos/client/rolodex/MergeProposal;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method bind(Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;)V
    .locals 1

    .line 216
    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$fdRmIRmDSVRxwx_64W-OwcujTow;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$fdRmIRmDSVRxwx_64W-OwcujTow;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 217
    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$E_W-1gvbpcswqIhp0-ChHaPLF6E;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$E_W-1gvbpcswqIhp0-ChHaPLF6E;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method getItemCount()I
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->latestResults:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    invoke-virtual {v0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItemCount()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->showProgressErrorItem:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method getItemViewType(I)I
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->latestResults:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    invoke-virtual {v0}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItemCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    :goto_0
    return p1
.end method

.method isBusy()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->onActiveItemsChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 222
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$GP-qDnC7vhB1Uq_efgU_MNHv63Y;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$GP-qDnC7vhB1Uq_efgU_MNHv63Y;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;)V

    .line 223
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 224
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$bind$13$MergeProposalListPresenter(ILcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->onUncheckedItemsChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 173
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$b6dwlZqz5Hmhj53fiipl5bFLTMs;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$b6dwlZqz5Hmhj53fiipl5bFLTMs;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;I)V

    .line 174
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$oOB2Z7FBxx0ckPEJVSpQKAQGRYo;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$oOB2Z7FBxx0ckPEJVSpQKAQGRYo;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;)V

    .line 175
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$bind$15$MergeProposalListPresenter(ILcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->onActiveItemsChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 180
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$qsFMX7mMkWGsnCjQ265MZT76mMA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$qsFMX7mMkWGsnCjQ265MZT76mMA;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;I)V

    .line 181
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 182
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$slhL9Q8JKYDnmkj6uWjUiaQg6eU;

    invoke-direct {v0, p2}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$slhL9Q8JKYDnmkj6uWjUiaQg6eU;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;)V

    .line 183
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$bind$20$MergeProposalListPresenter(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;ILcom/squareup/protos/client/rolodex/MergeProposal;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 187
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->onTitleClicked()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$BbeUvGLwWdTcILVKNSF_gq2aXvg;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$BbeUvGLwWdTcILVKNSF_gq2aXvg;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;I)V

    .line 188
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$xR6AxyzPWGRJxRQ7JqZiHfR5aXk;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$xR6AxyzPWGRJxRQ7JqZiHfR5aXk;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;)V

    .line 189
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$rzGhYFoHDiCQvHmSCiYF254MydU;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$rzGhYFoHDiCQvHmSCiYF254MydU;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;ILcom/squareup/protos/client/rolodex/MergeProposal;)V

    .line 190
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$bind$21$MergeProposalListPresenter(Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->showProgress:Lio/reactivex/Observable;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$irEqJYsGE1XhxbOmT58uTwzv74Y;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$irEqJYsGE1XhxbOmT58uTwzv74Y;-><init>(Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$bind$22$MergeProposalListPresenter(Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->showError:Lio/reactivex/Observable;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$vFxPZPc49wHAWVt9Fe0i6qQYZX8;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$vFxPZPc49wHAWVt9Fe0i6qQYZX8;-><init>(Lcom/squareup/ui/crm/cards/dedupe/ProgressErrorItemView;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$isBusy$23$MergeProposalListPresenter(Lkotlin/Unit;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 223
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->activeItemSubs:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$12$MergeProposalListPresenter(ILkotlin/Unit;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 174
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->isItemChecked(I)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$14$MergeProposalListPresenter(ILkotlin/Unit;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 181
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->isItemActive(I)Z

    move-result p2

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->isItemChecked(I)Z

    move-result p1

    xor-int/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$16$MergeProposalListPresenter(ILkotlin/Unit;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 188
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->isItemActive(I)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public synthetic lambda$null$18$MergeProposalListPresenter(ILjava/lang/Boolean;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 202
    instance-of p3, p3, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-nez p3, :cond_0

    .line 204
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->setItemChecked(IZ)V

    :cond_0
    const/4 p2, 0x0

    .line 206
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->setItemActive(ILio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public synthetic lambda$null$19$MergeProposalListPresenter(ILcom/squareup/protos/client/rolodex/MergeProposal;Ljava/lang/Boolean;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 191
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->setItemChecked(IZ)V

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 195
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-interface {v0, p2, v1}, Lcom/squareup/crm/RolodexServiceHelper;->dismissMergeProposal(Lcom/squareup/protos/client/rolodex/MergeProposal;Z)Lio/reactivex/Single;

    move-result-object p2

    .line 196
    invoke-virtual {p2}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p2

    .line 197
    invoke-virtual {p2}, Lio/reactivex/Observable;->publish()Lio/reactivex/observables/ConnectableObservable;

    move-result-object p2

    .line 200
    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$DI3EQbfNg4mg31k17D39HawQhU4;

    invoke-direct {v0, p0, p1, p3}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$DI3EQbfNg4mg31k17D39HawQhU4;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;ILjava/lang/Boolean;)V

    .line 201
    invoke-virtual {p2, v0}, Lio/reactivex/observables/ConnectableObservable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p3

    .line 200
    invoke-direct {p0, p1, p3}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->setItemActive(ILio/reactivex/disposables/Disposable;)V

    .line 211
    invoke-virtual {p2}, Lio/reactivex/observables/ConnectableObservable;->connect()Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public synthetic lambda$null$7$MergeProposalListPresenter(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 137
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    const/4 v0, 0x1

    if-eqz p2, :cond_0

    .line 138
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->latestResults:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItemCount()I

    move-result p2

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->notifyItemRangeInserted(II)V

    goto :goto_0

    .line 140
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->latestResults:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItemCount()I

    move-result p2

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;->notifyItemRangeRemoved(II)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$3$MergeProposalListPresenter(Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 90
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->latestResults:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    return-void
.end method

.method public synthetic lambda$onEnterScope$4$MergeProposalListPresenter()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->activeItemSubs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/disposables/Disposable;

    .line 96
    invoke-interface {v1}, Lio/reactivex/disposables/Disposable;->dispose()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$onLoad$11$MergeProposalListPresenter(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)Lio/reactivex/disposables/Disposable;
    .locals 4

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->loader:Lcom/squareup/crm/MergeProposalLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/MergeProposalLoader;->results()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$3BY4cg_FVxK7OwdcUQU3rOda8Zg;->INSTANCE:Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$3BY4cg_FVxK7OwdcUQU3rOda8Zg;

    .line 147
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$KzhF0s97E42-bgcP1hwtDaHc220;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$KzhF0s97E42-bgcP1hwtDaHc220;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)V

    .line 148
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$1TiRD6k3yO0bYefMB43Kd9o1u74;->INSTANCE:Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$1TiRD6k3yO0bYefMB43Kd9o1u74;

    .line 149
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->mainScheduler:Lio/reactivex/Scheduler;

    const-wide/16 v2, 0x1

    .line 152
    invoke-virtual {p1, v2, v3, v0, v1}, Lio/reactivex/Observable;->delay(JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->loader:Lcom/squareup/crm/MergeProposalLoader;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$YrJsJbxn-3jrBT4BMLZsmhL2rR0;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$YrJsJbxn-3jrBT4BMLZsmhL2rR0;-><init>(Lcom/squareup/crm/MergeProposalLoader;)V

    .line 153
    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$MergeProposalListPresenter(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->loader:Lcom/squareup/crm/MergeProposalLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/MergeProposalLoader;->results()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/crm/MergeProposalLoader;->EMPTY_RESULTS:Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;

    .line 108
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 111
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->buffer(II)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$nL4XeS4_qYHlepGQQxDg9XERYx8;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$nL4XeS4_qYHlepGQQxDg9XERYx8;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)V

    .line 112
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$8$MergeProposalListPresenter(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->showProgressErrorItem:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    .line 135
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/rx2/Rx2TransformersKt;->distinctUntilChangedWithFirstValueToSkip(Ljava/lang/Object;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$GMVN8TY_2UcqYlqk3movoTrdJFQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$GMVN8TY_2UcqYlqk3movoTrdJFQ;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)V

    .line 136
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$uncheckedCount$24$MergeProposalListPresenter(Lkotlin/Unit;)Ljava/lang/Integer;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 230
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->uncheckedItemPositions:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 68
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->loader:Lcom/squareup/crm/MergeProposalLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/MergeProposalLoader;->state()Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;

    .line 71
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$P10A2cweKjJnuPeJkrjIZYByjww;->INSTANCE:Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$P10A2cweKjJnuPeJkrjIZYByjww;

    .line 72
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x0

    .line 73
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->showProgress:Lio/reactivex/Observable;

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->loader:Lcom/squareup/crm/MergeProposalLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/MergeProposalLoader;->state()Lio/reactivex/Observable;

    move-result-object v0

    const-class v2, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Failure;

    .line 77
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v2, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$u5c_WU3_ywMZVgOErc89JfGmRBE;->INSTANCE:Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$u5c_WU3_ywMZVgOErc89JfGmRBE;

    .line 78
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 79
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->showError:Lio/reactivex/Observable;

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->showProgress:Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->showError:Lio/reactivex/Observable;

    sget-object v2, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$RqADA1Ul3IQB0vclrEYZ1StCB0Q;->INSTANCE:Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$RqADA1Ul3IQB0vclrEYZ1StCB0Q;

    .line 84
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->showProgressErrorItem:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 85
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 83
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->loader:Lcom/squareup/crm/MergeProposalLoader;

    .line 89
    invoke-virtual {v0}, Lcom/squareup/crm/MergeProposalLoader;->results()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$M0ySoVM_WKU9V_D4npIkwXeEulU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$M0ySoVM_WKU9V_D4npIkwXeEulU;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;)V

    .line 90
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 88
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 93
    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$ZMwiDEdeYApSK9xJO7Bc6R-sC1g;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$ZMwiDEdeYApSK9xJO7Bc6R-sC1g;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;)V

    .line 94
    invoke-static {v0}, Lio/reactivex/disposables/Disposables;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 93
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 102
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 103
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;

    .line 106
    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$pfnOGf2mGmP2nKzZ265Gz74rfJ4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$pfnOGf2mGmP2nKzZ265Gz74rfJ4;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 133
    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$uw4xhJ7I-GjX-pEoD6E1TqpCrxk;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$uw4xhJ7I-GjX-pEoD6E1TqpCrxk;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 145
    new-instance v0, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$etvqBT91jfto4peotm4vCZUVR0g;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$etvqBT91jfto4peotm4vCZUVR0g;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method uncheckedCount()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;->onUncheckedItemsChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 229
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$0oY6ERojJsnmPtOAZ6iftJMGCRA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/dedupe/-$$Lambda$MergeProposalListPresenter$0oY6ERojJsnmPtOAZ6iftJMGCRA;-><init>(Lcom/squareup/ui/crm/cards/dedupe/MergeProposalListPresenter;)V

    .line 230
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 231
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
