.class public Lcom/squareup/ui/crm/cards/ProfileAttachmentsDeleteFileDialog$Factory;
.super Ljava/lang/Object;
.source "ProfileAttachmentsDeleteFileDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ProfileAttachmentsDeleteFileDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 46
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->deleteAttachment(Landroid/content/DialogInterface;)V

    return-void
.end method

.method static synthetic lambda$create$1(Landroid/content/Context;Landroid/content/DialogInterface;I)V
    .locals 1

    .line 49
    sget p2, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_delete_cancelled:I

    const/4 v0, 0x0

    invoke-static {p0, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p0

    .line 50
    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    .line 51
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 30
    const-class v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;

    .line 31
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;

    .line 32
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;->runner()Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->getContactName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->getContactName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_delete_file_fallback_name:I

    .line 35
    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 38
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_delete_file_message:I

    invoke-static {v2, v3}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "customer_name"

    .line 39
    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 40
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 42
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_delete_file_title:I

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v2

    .line 44
    invoke-virtual {v2, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/profileattachments/R$string;->crm_profile_attachments_delete:I

    new-instance v3, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsDeleteFileDialog$Factory$i5ViaUisyIOTLWa0itn6QgQbXEo;

    invoke-direct {v3, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsDeleteFileDialog$Factory$i5ViaUisyIOTLWa0itn6QgQbXEo;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    .line 45
    invoke-virtual {v1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsDeleteFileDialog$Factory$Z94Tz5FGqws7Z4pWUCYkBvoNHwo;

    invoke-direct {v2, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsDeleteFileDialog$Factory$Z94Tz5FGqws7Z4pWUCYkBvoNHwo;-><init>(Landroid/content/Context;)V

    .line 48
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 53
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 55
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 56
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
