.class public Lcom/squareup/ui/crm/cards/UpdateGroup2View;
.super Landroid/widget/LinearLayout;
.source "UpdateGroup2View.java"


# instance fields
.field private addFilter:Lcom/squareup/marketfont/MarketButton;

.field private deleteGroup:Lcom/squareup/ui/ConfirmButton;

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private filterList:Landroid/widget/LinearLayout;

.field private groupName:Lcom/squareup/ui/XableEditText;

.field private final onDeleteGroupConfirmed:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onGroupNameChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/view/View;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->onGroupNameChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 38
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->onDeleteGroupConfirmed:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 48
    const-class p2, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Component;->inject(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->shortAnimTimeMs:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->onGroupNameChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method


# virtual methods
.method actionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 91
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method addFilterRow()Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 2

    .line 131
    sget v0, Lcom/squareup/widgets/pos/R$layout;->smart_line_row:I

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->filterList:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 132
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->filterList:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method disableAddFilter()V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->addFilter:Lcom/squareup/marketfont/MarketButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    return-void
.end method

.method getGroupName()Ljava/lang/String;
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->groupName:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method groupName()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 95
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2View$Q9ADhJw7ZXITmaeZsY-X5_-mVTo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2View$Q9ADhJw7ZXITmaeZsY-X5_-mVTo;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)V

    invoke-static {v0}, Lio/reactivex/Observable;->defer(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$groupName$1$UpdateGroup2View()Lio/reactivex/ObservableSource;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->onGroupNameChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->getGroupName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onFinishInflate$0$UpdateGroup2View()V
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->onDeleteGroupConfirmed:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method onAddFilterClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->addFilter:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 73
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->presenter:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method onDeleteGroupClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->onDeleteGroupConfirmed:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->presenter:Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/UpdateGroup2Screen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 79
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 53
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 54
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_group_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->groupName:Lcom/squareup/ui/XableEditText;

    .line 55
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_filter_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->filterList:Landroid/widget/LinearLayout;

    .line 56
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_add_filter:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->addFilter:Lcom/squareup/marketfont/MarketButton;

    .line 57
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_delete_group:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->deleteGroup:Lcom/squareup/ui/ConfirmButton;

    .line 58
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->progressBar:Landroid/view/View;

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->groupName:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/UpdateGroup2View$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/UpdateGroup2View$1;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_DELETE_GROUPS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->deleteGroup:Lcom/squareup/ui/ConfirmButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->deleteGroup:Lcom/squareup/ui/ConfirmButton;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2View$glLcKc998fsrbxZgP6mBhSRFX_w;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$UpdateGroup2View$glLcKc998fsrbxZgP6mBhSRFX_w;-><init>(Lcom/squareup/ui/crm/cards/UpdateGroup2View;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    :cond_0
    return-void
.end method

.method setDeleteGroupEnabled(Z)V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->deleteGroup:Lcom/squareup/ui/ConfirmButton;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ConfirmButton;->setEnabled(Z)V

    return-void
.end method

.method setGroupName(Ljava/lang/String;)V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->groupName:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showAddFilter()V
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->addFilter:Lcom/squareup/marketfont/MarketButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    return-void
.end method

.method showFilters()V
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->filterList:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method showProgressBar(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 86
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/UpdateGroup2View;->progressBar:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method
