.class Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;
.super Lmortar/ViewPresenter;
.source "GroupEditPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/group/GroupEditView;",
        ">;"
    }
.end annotation


# static fields
.field private static final KEY_GROUP:Ljava/lang/String; = "group"


# instance fields
.field private final group:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 22
    invoke-static {}, Lcom/squareup/crm/util/RolodexGroupHelper;->newGroupBuilder()Lcom/squareup/protos/client/rolodex/Group$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Group$Builder;->build()Lcom/squareup/protos/client/rolodex/Group;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method private updateView(Lcom/squareup/ui/crm/cards/group/GroupEditView;)V
    .locals 1

    .line 62
    invoke-static {p1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetachNow(Landroid/view/View;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Group;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Group;->display_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/group/GroupEditView;->setGroupName(Ljava/lang/String;)V

    .line 66
    new-instance v0, Lcom/squareup/ui/crm/cards/group/-$$Lambda$GroupEditPresenter$yP2DEDd3OUnsCFfgBIMOd6D6MlA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/group/-$$Lambda$GroupEditPresenter$yP2DEDd3OUnsCFfgBIMOd6D6MlA;-><init>(Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;Lcom/squareup/ui/crm/cards/group/GroupEditView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method


# virtual methods
.method group()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public synthetic lambda$null$0$GroupEditPresenter(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 69
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/crm/util/RolodexGroupHelper;->withGroupName(Lcom/squareup/protos/client/rolodex/Group;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Group;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$updateView$1$GroupEditPresenter(Lcom/squareup/ui/crm/cards/group/GroupEditView;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 67
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/group/GroupEditView;->groupName()Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/group/-$$Lambda$GroupEditPresenter$rid_6pDvBCr5pwnKEO9YJvcLjZ8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/group/-$$Lambda$GroupEditPresenter$rid_6pDvBCr5pwnKEO9YJvcLjZ8;-><init>(Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;)V

    .line 68
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 29
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/group/GroupEditView;

    if-eqz p1, :cond_0

    const-string v1, "group"

    .line 33
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object p1

    if-eqz p1, :cond_0

    .line 36
    :try_start_0
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v2, Lcom/squareup/protos/client/rolodex/Group;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v2, p1}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    :catch_0
    :cond_0
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->updateView(Lcom/squareup/ui/crm/cards/group/GroupEditView;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 48
    sget-object v0, Lcom/squareup/protos/client/rolodex/Group;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object v0

    const-string v1, "group"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method setGroup(Lcom/squareup/ui/crm/cards/group/GroupEditView;Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 1

    .line 56
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->group:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->updateView(Lcom/squareup/ui/crm/cards/group/GroupEditView;)V

    return-void
.end method
