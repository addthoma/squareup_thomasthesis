.class Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "EditFilterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/EditFilterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/EditFilterCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final controller:Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/ui/crm/cards/EditFilterScreen;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 104
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 105
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;

    .line 106
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 107
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private inflateContentView(Lcom/squareup/ui/crm/cards/EditFilterCardView;)Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;
    .locals 2

    .line 164
    sget-object v0, Lcom/squareup/ui/crm/cards/EditFilterScreen$1;->$SwitchMap$com$squareup$protos$client$rolodex$Filter$Type:[I

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;

    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;->getFilter()Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Filter;->type:Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 p1, 0x0

    return-object p1

    .line 201
    :pswitch_0
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->inflateMultiOptionFilterContent()Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

    move-result-object p1

    return-object p1

    .line 196
    :pswitch_1
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_LOCATION_FILTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->inflateMultiOptionFilterContent()Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

    move-result-object p1

    goto :goto_0

    .line 198
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->inflateEmptyFilterContent()Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;

    move-result-object p1

    :goto_0
    return-object p1

    .line 193
    :pswitch_2
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->inflateMultiOptionFilterContent()Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

    move-result-object p1

    return-object p1

    .line 190
    :pswitch_3
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->inflateSingleTextFilterContent()Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;

    move-result-object p1

    return-object p1

    .line 185
    :pswitch_4
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->inflateEmptyFilterContent()Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;

    move-result-object p1

    return-object p1

    .line 182
    :pswitch_5
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->inflateSingleOptionFilterContent()Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;

    move-result-object p1

    return-object p1

    .line 171
    :pswitch_6
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_VISIT_FREQUENCY_FILTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->inflateVisitFrequencyFilterContent()Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;

    move-result-object p1

    goto :goto_1

    .line 173
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->inflateEmptyFilterContent()Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;

    move-result-object p1

    :goto_1
    return-object p1

    .line 166
    :pswitch_7
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_MANUAL_GROUP_FILTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 167
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->inflateMultiOptionFilterContent()Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

    move-result-object p1

    goto :goto_2

    .line 168
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->inflateEmptyFilterContent()Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;

    move-result-object p1

    :goto_2
    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic lambda$onLoad$3(Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;Lcom/squareup/marin/widgets/MarinActionBar;)Lrx/Subscription;
    .locals 1

    .line 150
    invoke-interface {p0}, Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;->isValid()Lrx/Observable;

    move-result-object p0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 151
    invoke-static {v0}, Lcom/squareup/util/rx/RxTransformers;->distinctUntilChangedWithFirstValueToSkip(Ljava/lang/Object;)Lrx/Observable$Transformer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$Mg33pw2ykhDYYi1B8X_NkLhFaZs;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$Mg33pw2ykhDYYi1B8X_NkLhFaZs;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;)V

    .line 152
    invoke-virtual {p0, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$null$0$EditFilterScreen$Presenter(Lkotlin/Unit;)V
    .locals 1

    .line 134
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;->setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;

    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;->commitEditFilterCard()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$EditFilterScreen$Presenter(Lcom/squareup/ui/crm/cards/EditFilterCardView;)Lrx/Subscription;
    .locals 1

    .line 132
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->onRemoveFilterClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$EditFilterScreen$Presenter$rU3sgEa4MhIE-eKyt5aG80VMBL8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$EditFilterScreen$Presenter$rU3sgEa4MhIE-eKyt5aG80VMBL8;-><init>(Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;)V

    .line 133
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$2$EditFilterScreen$Presenter(Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;)Lrx/Subscription;
    .locals 2

    .line 145
    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;->filter()Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$1uzqBS2JAI2i54kA4kgl9ft_JD8;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$1uzqBS2JAI2i54kA4kgl9ft_JD8;-><init>(Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;)V

    .line 146
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 111
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 112
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/EditFilterScreen;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->screen:Lcom/squareup/ui/crm/cards/EditFilterScreen;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 116
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 117
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/EditFilterCardView;

    .line 118
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->actionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 120
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;

    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;->getFilter()Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/client/rolodex/Filter;->display_name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 121
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$4qwI85sJETfrp9QG95cNbpHdUfw;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$4qwI85sJETfrp9QG95cNbpHdUfw;-><init>(Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 123
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;

    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;->isFilterBeingCreated()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/configure/item/R$string;->add:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 126
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 128
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/EditFilterCardView;->showRemoveFilterButton()V

    .line 131
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$EditFilterScreen$Presenter$RckhrBC70H_JScE-hIZJJX___0E;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$EditFilterScreen$Presenter$RckhrBC70H_JScE-hIZJJX___0E;-><init>(Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;Lcom/squareup/ui/crm/cards/EditFilterCardView;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 138
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$9uQhkdeeCMXqjViMmJYX0Fv65v8;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$9uQhkdeeCMXqjViMmJYX0Fv65v8;-><init>(Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 140
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->inflateContentView(Lcom/squareup/ui/crm/cards/EditFilterCardView;)Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 142
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;

    invoke-interface {v2}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Controller;->getFilter()Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;->setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V

    .line 144
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$EditFilterScreen$Presenter$3n7RFzKjVLgk2G4m9HM_GIwE9Bw;

    invoke-direct {v2, p0, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$EditFilterScreen$Presenter$3n7RFzKjVLgk2G4m9HM_GIwE9Bw;-><init>(Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;)V

    invoke-static {p1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 149
    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$EditFilterScreen$Presenter$6Sw7ZFHvxlk7VScXt7l6yIqUu9w;

    invoke-direct {v2, v1, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$EditFilterScreen$Presenter$6Sw7ZFHvxlk7VScXt7l6yIqUu9w;-><init>(Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;Lcom/squareup/marin/widgets/MarinActionBar;)V

    invoke-static {p1, v2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :cond_1
    return-void
.end method
