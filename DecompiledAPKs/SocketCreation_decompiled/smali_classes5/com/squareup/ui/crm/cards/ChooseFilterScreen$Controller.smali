.class public interface abstract Lcom/squareup/ui/crm/cards/ChooseFilterScreen$Controller;
.super Ljava/lang/Object;
.source "ChooseFilterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ChooseFilterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Controller"
.end annotation


# virtual methods
.method public abstract closeChooseFilterScreen()V
.end method

.method public abstract getCurrentFilters()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showEditFilterScreen(Lcom/squareup/protos/client/rolodex/Filter;)V
.end method
