.class public interface abstract Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Runner;
.super Ljava/lang/Object;
.source "SaveCardCustomerEmailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeCustomerEmailScreen()V
.end method

.method public abstract getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;
.end method

.method public abstract showVerifyZipCodeOrSaveCardSpinnerScreen()V
.end method
