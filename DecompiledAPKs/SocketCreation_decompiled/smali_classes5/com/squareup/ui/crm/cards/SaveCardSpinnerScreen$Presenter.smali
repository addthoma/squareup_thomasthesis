.class Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "SaveCardSpinnerScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private contact:Lcom/squareup/protos/client/rolodex/Contact;

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final runner:Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;

.field private final state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 78
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;

    .line 80
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 81
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 82
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 84
    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;->getContactForSaveCardScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 85
    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;->getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    return-void
.end method

.method private getEntryFlow()Ljava/lang/String;
    .locals 2

    .line 242
    sget-object v0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$1;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const-string v0, "CUSTOMER_PROFILE"

    return-object v0

    :cond_0
    const-string v0, "POST_TRANSACTION"

    return-object v0
.end method

.method private maybeFormattedBrandAndUnmaskedDigits(Lcom/squareup/Card;)Ljava/lang/String;
    .locals 1

    .line 254
    invoke-virtual {p1}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-static {v0, p1}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private onCardSaveFailure(Ljava/lang/CharSequence;Z)V
    .locals 4

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->complete:Z

    const/4 v1, 0x0

    .line 223
    iput-boolean v1, v0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->success:Z

    .line 224
    iput-object p1, v0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->failureMessage:Ljava/lang/CharSequence;

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/R$string;->crm_cardonfile_savecard_save_failure:I

    .line 226
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 227
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;

    .line 228
    invoke-virtual {v1, v0, p1}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->setText(Ljava/lang/String;Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    .line 230
    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->transitionToFailure()V

    goto :goto_0

    .line 232
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->setToFailure()V

    .line 234
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->showOkButton()V

    .line 236
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p2, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;

    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CARD_ON_FILE_LINK_CARD_REQUEST_FAILURE:Lcom/squareup/analytics/RegisterViewName;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    .line 238
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->getEntryFlow()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v3}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getEntryMethod()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object v3

    invoke-direct {p2, v0, v1, v2, v3}, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;-><init>(Lcom/squareup/analytics/RegisterViewName;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    .line 236
    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private onCardSaveResponse(Z)V
    .locals 3

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->complete:Z

    .line 205
    iput-boolean v1, v0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->success:Z

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/R$string;->crm_cardonfile_savecard_saved:I

    .line 207
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    const-string v2, "customer"

    .line 208
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 209
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 210
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 211
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;

    .line 212
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v2}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->maybeFormattedBrandAndUnmaskedDigits(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->setText(Ljava/lang/String;Ljava/lang/CharSequence;)V

    if-eqz p1, :cond_0

    .line 214
    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->transitionToSuccess()V

    goto :goto_0

    .line 216
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->setToSuccess()V

    .line 218
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->showDoneButton()V

    return-void
.end method

.method private saveCard()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    .line 141
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->email:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    .line 142
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    .line 145
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->verifyAndLinkCard()V

    goto :goto_1

    .line 143
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->updateContact()V

    :goto_1
    return-void
.end method

.method private updateContact()V
    .locals 3

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-static {v0, v1}, Lcom/squareup/crm/util/RolodexContactHelper;->withUpdatedProfileFromState(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/ui/crm/flow/SaveCardSharedState;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    .line 151
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/squareup/crm/RolodexServiceHelper;->upsertContact(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardSpinnerScreen$Presenter$WbL5FH9JIwnI0pca182R9YZFiSo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardSpinnerScreen$Presenter$WbL5FH9JIwnI0pca182R9YZFiSo;-><init>(Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;)V

    .line 152
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method private verifyAndLinkCard()V
    .locals 11

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iget-object v1, v1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->firstName:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iget-object v2, v2, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->lastName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/squareup/text/Cards;->formattedCardOwnerName(Lcom/squareup/util/Res;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getEntryMethod()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object v8

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->CARD_ON_FILE_LINK_CARD_REQUEST:Lcom/squareup/analytics/RegisterViewName;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v3, v3, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    .line 176
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->getEntryFlow()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v8}, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;-><init>(Lcom/squareup/analytics/RegisterViewName;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    .line 174
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 178
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v4, v0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v6

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    .line 179
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCardData()Lcom/squareup/protos/client/bills/CardData;

    move-result-object v7

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->getPostalCode()Ljava/lang/String;

    move-result-object v9

    .line 180
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v10

    .line 178
    invoke-interface/range {v3 .. v10}, Lcom/squareup/crm/RolodexServiceHelper;->verifyAndLinkCard(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/Card;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardSpinnerScreen$Presenter$hxeKgXWnFtCWoCTK7eCHVEPaabY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardSpinnerScreen$Presenter$hxeKgXWnFtCWoCTK7eCHVEPaabY;-><init>(Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;)V

    .line 180
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method


# virtual methods
.method getContactForTest()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public synthetic lambda$null$0$SaveCardSpinnerScreen$Presenter(Lcom/squareup/protos/client/rolodex/UpsertContactResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 154
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 155
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->verifyAndLinkCard()V

    return-void
.end method

.method public synthetic lambda$null$1$SaveCardSpinnerScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 158
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/UpsertContactResponse;

    const/4 p2, 0x1

    if-eqz p1, :cond_0

    .line 159
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/UpsertContactResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 160
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertContactResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->onCardSaveFailure(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    .line 162
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_cardonfile_savecard_save_failure_subtitle:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->onCardSaveFailure(Ljava/lang/CharSequence;Z)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$3$SaveCardSpinnerScreen$Presenter(Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x1

    .line 182
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->onCardSaveResponse(Z)V

    .line 183
    iget-object v0, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    invoke-static {v0, v1}, Lcom/squareup/crm/util/RolodexContactHelper;->withAddedInstrument(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/instruments/InstrumentSummary;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v7, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterViewName;->CARD_ON_FILE_LINK_CARD_REQUEST_SUCCESS:Lcom/squareup/analytics/RegisterViewName;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v3, v1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    iget-object v4, v1, Lcom/squareup/protos/client/instruments/InstrumentSummary;->instrument_token:Ljava/lang/String;

    .line 186
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->getEntryFlow()Ljava/lang/String;

    move-result-object v5

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    .line 187
    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getEntryMethod()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object v6

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;-><init>(Lcom/squareup/analytics/RegisterViewName;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    .line 185
    invoke-interface {v0, v7}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iget-object p1, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->instrument:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->setServerCardInfo(Lcom/squareup/protos/client/instruments/InstrumentSummary;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$4$SaveCardSpinnerScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 192
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;

    const/4 p2, 0x1

    if-eqz p1, :cond_0

    .line 193
    iget-object v0, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 194
    iget-object p1, p1, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->onCardSaveFailure(Ljava/lang/CharSequence;Z)V

    goto :goto_0

    .line 196
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_cardonfile_savecard_save_failure_subtitle:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->onCardSaveFailure(Ljava/lang/CharSequence;Z)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$updateContact$2$SaveCardSpinnerScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 152
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardSpinnerScreen$Presenter$-20c0I0h68q8VMKPsqvz749aulI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardSpinnerScreen$Presenter$-20c0I0h68q8VMKPsqvz749aulI;-><init>(Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardSpinnerScreen$Presenter$7FTWjSTj__3hP-z6p7J0XZhtqqY;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardSpinnerScreen$Presenter$7FTWjSTj__3hP-z6p7J0XZhtqqY;-><init>(Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$verifyAndLinkCard$5$SaveCardSpinnerScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 180
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardSpinnerScreen$Presenter$sYe9c1u55uFNUp4jcejVYbuKgAg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardSpinnerScreen$Presenter$sYe9c1u55uFNUp4jcejVYbuKgAg;-><init>(Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;)V

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardSpinnerScreen$Presenter$cY2NPu96cWF5XPaqVz4egRB7RnQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardSpinnerScreen$Presenter$cY2NPu96cWF5XPaqVz4egRB7RnQ;-><init>(Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method onDoneButtonClicked()V
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-interface {v0, v1}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;->finishSaveCardFlow(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 89
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;

    .line 93
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    goto :goto_0

    .line 96
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iget-object v2, v2, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->firstName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iget-object v2, v2, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->lastName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 99
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crm/R$string;->crm_cardonfile_savecard_saving:I

    .line 100
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "customer"

    .line 101
    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 102
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 103
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 105
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v2}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->maybeFormattedBrandAndUnmaskedDigits(Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->setText(Ljava/lang/String;Ljava/lang/CharSequence;)V

    if-nez p1, :cond_1

    .line 108
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->saveCard()V

    goto :goto_1

    .line 110
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iget-boolean p1, p1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->complete:Z

    if-eqz p1, :cond_3

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iget-boolean p1, p1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->success:Z

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    .line 112
    invoke-direct {p0, v0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->onCardSaveResponse(Z)V

    goto :goto_1

    .line 114
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iget-object p1, p1, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->failureMessage:Ljava/lang/CharSequence;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->onCardSaveFailure(Ljava/lang/CharSequence;Z)V

    :cond_3
    :goto_1
    return-void
.end method

.method onOkButtonClicked()V
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Runner;->closeSaveCardSpinner()V

    return-void
.end method
