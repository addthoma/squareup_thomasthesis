.class public interface abstract Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;
.super Ljava/lang/Object;
.source "UpdateLoyaltyPhoneScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeUpdateLoyaltyPhoneScreen()V
.end method

.method public abstract getLoyaltyAccountMapping()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onUpdateLoyaltyError()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract saveLoyaltyPhone(Ljava/lang/String;)V
.end method

.method public abstract saveLoyaltyPhoneInProgress()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showUpdateLoyaltyPhoneConflictDialog(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Contact;)V
.end method
