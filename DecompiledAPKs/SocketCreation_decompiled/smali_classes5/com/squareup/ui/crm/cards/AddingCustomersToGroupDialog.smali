.class public Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;
.super Lcom/squareup/dialog/GlassDialog;
.source "AddingCustomersToGroupDialog.java"


# instance fields
.field private final glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field presenter:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final progressBar:Landroid/widget/ProgressBar;

.field private final textView:Landroid/widget/TextView;

.field private final view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 28
    sget v0, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground_NoDim:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    .line 29
    const-class v0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Component;

    invoke-interface {v0, p0}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Component;->inject(Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;)V

    .line 31
    sget v0, Lcom/squareup/crmscreens/R$layout;->hud_progress_view:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->view:Landroid/view/View;

    .line 32
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->view:Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->setContentView(Landroid/view/View;)V

    .line 34
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->view:Landroid/view/View;

    sget v0, Lcom/squareup/crmscreens/R$id;->hud_progress:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->progressBar:Landroid/widget/ProgressBar;

    .line 35
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->view:Landroid/view/View;

    sget v0, Lcom/squareup/crmscreens/R$id;->hud_glyph:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 36
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->view:Landroid/view/View;

    sget v0, Lcom/squareup/crmscreens/R$id;->hud_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->textView:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method getView()Landroid/view/View;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->view:Landroid/view/View;

    return-object v0
.end method

.method hideProgress()V
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public onAttachedToWindow()V
    .locals 1

    .line 40
    invoke-super {p0}, Lcom/squareup/dialog/GlassDialog;->onAttachedToWindow()V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->presenter:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->presenter:Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupPresenter;->dropView(Ljava/lang/Object;)V

    .line 46
    invoke-super {p0}, Lcom/squareup/dialog/GlassDialog;->onDetachedFromWindow()V

    return-void
.end method

.method showGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 2

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method showText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupDialog;->textView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
