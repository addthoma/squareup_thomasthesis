.class Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;
.super Lmortar/ViewPresenter;
.source "VisitFrequencyFilterContentPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;",
        ">;"
    }
.end annotation


# static fields
.field private static final MINIMUM_VISITS_LOWER_BOUND:I = 0x1

.field private static final MINIMUM_VISITS_UPPER_BOUND:I = 0x64


# instance fields
.field private final filter:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 25
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method

.method private bind(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;)V
    .locals 8

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Filter;

    .line 70
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->minimumVisitsRow()Lcom/squareup/register/widgets/list/EditQuantityRow;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments:Ljava/lang/Integer;

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    const/16 v2, 0x64

    iget-object v4, v0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_x_payments:Ljava/lang/Integer;

    .line 73
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 72
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    .line 70
    :goto_0
    invoke-virtual {v1, v2}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setValue(I)V

    .line 76
    new-instance v1, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    .line 78
    iget-object v2, v0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days_options:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/rolodex/Filter$Option;

    .line 79
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->addRow()Lcom/squareup/ui/crm/rows/CheckableRow;

    move-result-object v5

    .line 80
    iget-object v6, v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->label:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/squareup/ui/crm/rows/CheckableRow;->showTitle(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v6, v0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v6, :cond_1

    iget-object v6, v0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object v6, v6, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    iget-object v7, v4, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    .line 83
    invoke-static {v6, v7}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 84
    invoke-virtual {v5, v3}, Lcom/squareup/ui/crm/rows/CheckableRow;->setChecked(Z)V

    .line 85
    invoke-virtual {v1, v5}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 88
    :cond_1
    new-instance v6, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$M_FQYf4Gh-3Z2myKTjJXyiNRUQk;

    invoke-direct {v6, p0, v5, v1, v4}, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$M_FQYf4Gh-3Z2myKTjJXyiNRUQk;-><init>(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/protos/client/rolodex/Filter$Option;)V

    invoke-static {v5, v6}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method static synthetic lambda$isValid$0(Lcom/squareup/protos/client/rolodex/Filter;)Ljava/lang/Boolean;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    if-eqz v0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Filter;->xpilyd_y_days:Lcom/squareup/protos/client/rolodex/Filter$Option;

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Filter$Option;->value:Ljava/lang/String;

    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method filter()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method isValid()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$NHpH6x0qlEqJqKIvHVdJCA7q3aw;->INSTANCE:Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$NHpH6x0qlEqJqKIvHVdJCA7q3aw;

    .line 57
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$bind$2$VisitFrequencyFilterContentPresenter(Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/protos/client/rolodex/Filter$Option;)Lrx/Subscription;
    .locals 2

    .line 89
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CheckableRow;->onClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$OOYmBfkQR_gUL5yVwIdasV5FLe0;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/squareup/ui/crm/cards/filters/-$$Lambda$VisitFrequencyFilterContentPresenter$OOYmBfkQR_gUL5yVwIdasV5FLe0;-><init>(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/protos/client/rolodex/Filter$Option;)V

    .line 90
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$VisitFrequencyFilterContentPresenter(Lcom/squareup/ui/crm/rows/CheckableRow;Ljava/util/concurrent/atomic/AtomicReference;Lcom/squareup/protos/client/rolodex/Filter$Option;Lkotlin/Unit;)V
    .locals 1

    .line 91
    invoke-virtual {p1}, Lcom/squareup/ui/crm/rows/CheckableRow;->isChecked()Z

    move-result p4

    if-nez p4, :cond_1

    .line 92
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p4

    if-eqz p4, :cond_0

    .line 93
    invoke-virtual {p2}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Lcom/squareup/ui/crm/rows/CheckableRow;

    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/squareup/ui/crm/rows/CheckableRow;->setChecked(Z)V

    :cond_0
    const/4 p4, 0x1

    .line 95
    invoke-virtual {p1, p4}, Lcom/squareup/ui/crm/rows/CheckableRow;->setChecked(Z)V

    .line 96
    invoke-virtual {p2, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/rolodex/Filter;

    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/Filter;->newBuilder()Lcom/squareup/protos/client/rolodex/Filter$Builder;

    move-result-object p2

    .line 99
    invoke-virtual {p2, p3}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_y_days(Lcom/squareup/protos/client/rolodex/Filter$Option;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    move-result-object p2

    .line 100
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->build()Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p2

    .line 98
    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 32
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;

    .line 35
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->minimumVisitsRow()Lcom/squareup/register/widgets/list/EditQuantityRow;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Lcom/squareup/register/widgets/list/EditQuantityRow;->setMaxQuantity(J)V

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->bind(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;)V

    return-void
.end method

.method onMinimumVisitsChanged(I)V
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Filter;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/Filter;->newBuilder()Lcom/squareup/protos/client/rolodex/Filter$Builder;

    move-result-object v1

    .line 63
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->xpilyd_x_payments(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/Filter$Builder;

    move-result-object p1

    .line 64
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/Filter$Builder;->build()Lcom/squareup/protos/client/rolodex/Filter;

    move-result-object p1

    .line 62
    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->filter:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;

    if-eqz p1, :cond_0

    .line 45
    invoke-static {p1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetachNow(Landroid/view/View;)V

    .line 46
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;->clearRows()V

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentPresenter;->bind(Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;)V

    :cond_0
    return-void
.end method
