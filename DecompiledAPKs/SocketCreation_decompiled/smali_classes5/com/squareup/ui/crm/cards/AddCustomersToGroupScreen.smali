.class public Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "AddCustomersToGroupScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;,
        Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 166
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$4v1zI0muFrXJ8I2b5YFIDJ4ixPo;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$AddCustomersToGroupScreen$4v1zI0muFrXJ8I2b5YFIDJ4ixPo;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;
    .locals 1

    .line 167
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 168
    new-instance v0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 162
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_ADD_CUSTOMERS_TO_GROUP:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 53
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_add_customers_to_group_view:I

    return v0
.end method
