.class public Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;
.super Landroid/widget/LinearLayout;
.source "SingleOptionFilterContentView.java"

# interfaces
.implements Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;


# instance fields
.field private optionList:Landroid/widget/LinearLayout;

.field presenter:Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const-class p2, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;->inject(Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;)V

    return-void
.end method


# virtual methods
.method addRow()Lcom/squareup/ui/crm/rows/CheckableRow;
    .locals 2

    .line 62
    sget v0, Lcom/squareup/crm/R$layout;->crm_single_select_row:I

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;->optionList:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/rows/CheckableRow;

    .line 63
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;->optionList:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method clearRows()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;->optionList:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method public filter()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->filter()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public isValid()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->isValid()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 36
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->dropView(Ljava/lang/Object;)V

    .line 42
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 31
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 32
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_option_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;->optionList:Landroid/widget/LinearLayout;

    return-void
.end method

.method public setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentPresenter;->setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V

    return-void
.end method
