.class public Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "CustomerInvoiceCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;
    }
.end annotation


# static fields
.field public static final INVOICES_FIRST_PAGE_SIZE:Ljava/lang/Integer;

.field public static final INVOICES_LOAD_MORE_FROM_END:I = 0x32

.field public static final INVOICES_NEXT_PAGE_SIZE:I = 0x32


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final dateFormat:Ljava/text/DateFormat;

.field private invoiceListAdapter:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

.field private invoices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ">;"
        }
    .end annotation
.end field

.field private invoicesList:Landroidx/recyclerview/widget/RecyclerView;

.field private final isAtEndOfList:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final onIsNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final onNearEndOfLists:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x14

    .line 61
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->INVOICES_FIRST_PAGE_SIZE:Ljava/lang/Integer;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 71
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 53
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->onIsNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 54
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->onNearEndOfLists:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 55
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->isAtEndOfList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 58
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->invoices:Ljava/util/List;

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->runner:Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;

    .line 73
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 74
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->res:Lcom/squareup/util/Res;

    .line 75
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->dateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->isAtEndOfList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->onIsNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Ljava/util/List;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->invoices:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->runner:Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)Ljava/text/DateFormat;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->dateFormat:Ljava/text/DateFormat;

    return-object p0
.end method

.method private atEndOfList()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->isAtEndOfList:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 166
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_invoice_list:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->invoicesList:Landroidx/recyclerview/widget/RecyclerView;

    .line 167
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 168
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 169
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_invoice_progress_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->progressBar:Landroid/widget/ProgressBar;

    return-void
.end method

.method private getActionBarConfig(Lcom/squareup/util/Res;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 3

    .line 158
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 159
    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_invoice_list_header:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    const/4 v1, 0x1

    .line 160
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->runner:Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$KHqx64GV_GEymiDX61_BOYwcVUM;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$KHqx64GV_GEymiDX61_BOYwcVUM;-><init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;)V

    .line 161
    invoke-virtual {p1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 162
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$attach$0(Lcom/squareup/util/Optional;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .line 89
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$5(Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)Ljava/lang/Boolean;
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 105
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onNearEndOfList$8(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    return-object p0
.end method

.method static synthetic lambda$onNearEndOfList$9(Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 150
    sget-object p0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p0
.end method

.method private onNearEndOfList()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->onIsNearEndOfList:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 148
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/PublishRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$TQ4BHwsEK-A9eKErfRk7OdtUsJU;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$TQ4BHwsEK-A9eKErfRk7OdtUsJU;

    .line 149
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$dRaQHCx4zSO2Cgbm2Z_tF0zCWac;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$dRaQHCx4zSO2Cgbm2Z_tF0zCWac;

    .line 150
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method private setUpRecyclerView(Landroid/view/View;)V
    .locals 3

    .line 127
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 128
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->invoicesList:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v2, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 129
    new-instance p1, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;-><init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->invoiceListAdapter:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->invoicesList:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->invoiceListAdapter:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->invoicesList:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$1;-><init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;Landroidx/recyclerview/widget/LinearLayoutManager;)V

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    .line 79
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->bindViews(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->res:Lcom/squareup/util/Res;

    invoke-direct {p0, v1}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->getActionBarConfig(Lcom/squareup/util/Res;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 82
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->setUpRecyclerView(Landroid/view/View;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->runner:Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;

    .line 87
    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;->loadingInvoices()Lrx/Observable;

    move-result-object v0

    .line 88
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->atEndOfList()Lrx/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$8o_ih4ztwPGFSFrT_eA7uGIOYvs;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$8o_ih4ztwPGFSFrT_eA7uGIOYvs;

    .line 86
    invoke-static {v0, v1, v2}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    .line 92
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$wTwFKqcllv4THLi1NK4Tef2G8CE;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$wTwFKqcllv4THLi1NK4Tef2G8CE;-><init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;Lrx/Observable;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 98
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$ePFpgGjehzBwQUi-wF8FjNos4Us;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$ePFpgGjehzBwQUi-wF8FjNos4Us;-><init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 104
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$aifw2zOAZ4hnHvV3fH-exjocOIo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$aifw2zOAZ4hnHvV3fH-exjocOIo;-><init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$2$CustomerInvoiceCoordinator(Lrx/Observable;)Lrx/Subscription;
    .locals 1

    .line 92
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$y1nRG990mdzmjlCXPdYxLQoWs84;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$y1nRG990mdzmjlCXPdYxLQoWs84;-><init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)V

    .line 93
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$4$CustomerInvoiceCoordinator()Lrx/Subscription;
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->onNearEndOfLists:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-static {v0}, Lrx/Observable;->switchOnNext(Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$RbR7OIMx_b-DTtuwii6xnZVbDkc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$RbR7OIMx_b-DTtuwii6xnZVbDkc;-><init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)V

    .line 99
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$7$CustomerInvoiceCoordinator()Lrx/Subscription;
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->runner:Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;->invoiceResults()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$mdpFsPm49S5g17BqFgfayV8evHY;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$mdpFsPm49S5g17BqFgfayV8evHY;

    .line 105
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$aI6IPDGZbb2ZT_MgLp8uPa85SmY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerInvoiceCoordinator$aI6IPDGZbb2ZT_MgLp8uPa85SmY;-><init>(Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;)V

    .line 106
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$1$CustomerInvoiceCoordinator(Ljava/lang/Boolean;)V
    .locals 1

    .line 94
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/16 p1, 0x8

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public synthetic lambda$null$3$CustomerInvoiceCoordinator(Lkotlin/Unit;)V
    .locals 1

    .line 100
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->runner:Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;

    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;->loadMoreInvoices(Ljava/lang/Integer;)V

    return-void
.end method

.method public synthetic lambda$null$6$CustomerInvoiceCoordinator(Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)V
    .locals 3

    .line 107
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 110
    iget-object v1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 111
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 114
    :cond_0
    iget-boolean p1, p1, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->hasMore:Z

    if-eqz p1, :cond_1

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->onNearEndOfLists:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->onNearEndOfList()Lrx/Observable;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 120
    :cond_1
    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->invoices:Ljava/util/List;

    .line 121
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->invoiceListAdapter:Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator$Adapter;->notifyDataSetChanged()V

    return-void
.end method
