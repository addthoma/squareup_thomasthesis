.class Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "MergeCustomersConfirmationScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;",
        ">;"
    }
.end annotation


# instance fields
.field private final controller:Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;

.field private final errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final res:Lcom/squareup/util/Res;

.field private final response:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/ui/ErrorsBarPresenter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 86
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 82
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->response:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;

    .line 88
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 89
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 90
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    return-void
.end method

.method private formatMessage(Lcom/squareup/protos/client/rolodex/MergeProposal;)Ljava/lang/String;
    .locals 3

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_customers_will_be_merged_into_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/MergeProposal;->duplicate_contacts:Ljava/util/List;

    .line 139
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const-string v2, "number"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/MergeProposal;->new_contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 140
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->getDisplayNameOrDefault(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object p1

    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 141
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getDisplayNameOrDefault(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 1

    .line 145
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_contact_default_display_name:I

    .line 146
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public synthetic lambda$null$0$MergeCustomersConfirmationScreen$Presenter(Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 118
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->hideProgress()V

    const/4 v0, 0x1

    .line 119
    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 120
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    const-string v0, ""

    invoke-virtual {p2, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->removeError(Ljava/lang/String;)Z

    .line 121
    iget-object p2, p3, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;->merge_proposal:Lcom/squareup/protos/client/rolodex/MergeProposal;

    invoke-direct {p0, p2}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->formatMessage(Lcom/squareup/protos/client/rolodex/MergeProposal;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->showMessage(Ljava/lang/String;)V

    .line 123
    iget-object p2, p3, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;->merge_proposal:Lcom/squareup/protos/client/rolodex/MergeProposal;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/MergeProposal;->duplicate_contacts:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/protos/client/rolodex/Contact;

    .line 124
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->addCustomerRow()Lcom/squareup/ui/crm/rows/ChooseCustomerRow;

    move-result-object v0

    .line 125
    invoke-direct {p0, p3}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->getDisplayNameOrDefault(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/ChooseCustomerRow;->showDisplayName(Ljava/lang/String;)V

    .line 126
    invoke-static {p3}, Lcom/squareup/crm/util/RolodexContactHelper;->getEmail(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v0, p3}, Lcom/squareup/ui/crm/rows/ChooseCustomerRow;->showStatusLine(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$1$MergeCustomersConfirmationScreen$Presenter(Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 130
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->hideProgress()V

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->errorsBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_failed_to_load_customers:I

    .line 132
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string v0, ""

    .line 131
    invoke-virtual {p1, v0, p2}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$2$MergeCustomersConfirmationScreen$Presenter(Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 116
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$MergeCustomersConfirmationScreen$Presenter$PzdRMwIKtpYljFEbWMgnOlEGpGA;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergeCustomersConfirmationScreen$Presenter$PzdRMwIKtpYljFEbWMgnOlEGpGA;-><init>(Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;Lcom/squareup/marin/widgets/MarinActionBar;)V

    new-instance p2, Lcom/squareup/ui/crm/cards/-$$Lambda$MergeCustomersConfirmationScreen$Presenter$73Y78jFgab4qbfN4krd9EoVScZM;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergeCustomersConfirmationScreen$Presenter$73Y78jFgab4qbfN4krd9EoVScZM;-><init>(Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;)V

    invoke-virtual {p3, v0, p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onLoad$3$MergeCustomersConfirmationScreen$Presenter(Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;Lcom/squareup/marin/widgets/MarinActionBar;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->response:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$MergeCustomersConfirmationScreen$Presenter$4mEyI56EhkwvzHt-pj3MJ9vD_K8;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergeCustomersConfirmationScreen$Presenter$4mEyI56EhkwvzHt-pj3MJ9vD_K8;-><init>(Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;Lcom/squareup/marin/widgets/MarinActionBar;)V

    .line 116
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 94
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;

    .line 97
    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;->getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexServiceHelper;->getManualMergeProposal(Lcom/squareup/protos/client/rolodex/ContactSet;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->response:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 96
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 101
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 102
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;

    .line 103
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;->actionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 105
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crm/applet/R$string;->crm_merge_customers_confirmation_title:I

    .line 106
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 107
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$2dh4VeEpWAgBZResRwKVHySLyM0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$2dh4VeEpWAgBZResRwKVHySLyM0;-><init>(Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 109
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_merge_label:I

    .line 110
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;->controller:Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$XDTcAGsxt2vXkqifdrAEn56Tv7k;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$XDTcAGsxt2vXkqifdrAEn56Tv7k;-><init>(Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    const/4 v1, 0x0

    .line 112
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 114
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$MergeCustomersConfirmationScreen$Presenter$mOVdHl2T_oRFa9qTBcSHbw9oqd4;

    invoke-direct {v1, p0, p1, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$MergeCustomersConfirmationScreen$Presenter$mOVdHl2T_oRFa9qTBcSHbw9oqd4;-><init>(Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationView;Lcom/squareup/marin/widgets/MarinActionBar;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
