.class public Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;
.super Landroid/widget/LinearLayout;
.source "AddCustomersToGroupView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private createGroup:Lcom/squareup/marketfont/MarketButton;

.field private groupList:Landroid/widget/LinearLayout;

.field private message:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const-class p2, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Component;->inject(Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;)V

    return-void
.end method


# virtual methods
.method actionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method addGroupRow()Lcom/squareup/ui/crm/rows/CheckableRow;
    .locals 2

    .line 75
    sget v0, Lcom/squareup/crm/R$layout;->crm_single_select_row:I

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->groupList:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/rows/CheckableRow;

    .line 76
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->groupList:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method clearGroupRows()V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->groupList:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 49
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->presenter:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method onCreateGroupClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->createGroup:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->presenter:Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 55
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 38
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 39
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 40
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->message:Lcom/squareup/widgets/MessageView;

    .line 41
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_create_group:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->createGroup:Lcom/squareup/marketfont/MarketButton;

    .line 42
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_group_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->groupList:Landroid/widget/LinearLayout;

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    sget v1, Lcom/squareup/crmviewcustomer/R$id;->non_stable_action_bar:I

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/ActionBarView;->setId(I)V

    return-void
.end method

.method showMessage(Ljava/lang/String;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AddCustomersToGroupView;->message:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
