.class public Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory;
.super Ljava/lang/Object;
.source "ProfileAttachmentsOverflowBottomDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Landroid/content/DialogInterface;)V
    .locals 1

    .line 66
    check-cast p0, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    .line 67
    sget v0, Lcom/google/android/material/R$id;->design_bottom_sheet:I

    invoke-virtual {p0, v0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 68
    invoke-static {p0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->from(Landroid/view/View;)Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    move-result-object p0

    const/4 v0, 0x3

    .line 69
    invoke-virtual {p0, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 33
    const-class v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;

    .line 34
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;

    .line 35
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;->runner()Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    move-result-object v0

    .line 36
    new-instance v1, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    invoke-direct {v1, p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;-><init>(Landroid/content/Context;)V

    .line 37
    sget v2, Lcom/squareup/profileattachments/R$layout;->crm_profile_attachments_overflow_bottom_sheet_dialog:I

    const/4 v3, 0x0

    .line 38
    invoke-static {p1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 40
    sget v3, Lcom/squareup/profileattachments/R$id;->download_item:I

    invoke-static {v2, v3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    .line 41
    new-instance v4, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$1;

    invoke-direct {v4, p0, v1, v0, p1}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$1;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory;Lcom/google/android/material/bottomsheet/BottomSheetDialog;Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    sget p1, Lcom/squareup/profileattachments/R$id;->rename_item:I

    invoke-static {v2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 50
    new-instance v3, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$2;

    invoke-direct {v3, p0, v1, v0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$2;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory;Lcom/google/android/material/bottomsheet/BottomSheetDialog;Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    invoke-virtual {p1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    sget p1, Lcom/squareup/profileattachments/R$id;->delete_item:I

    invoke-static {v2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 58
    new-instance v3, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$3;

    invoke-direct {v3, p0, v1, v0}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$3;-><init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory;Lcom/google/android/material/bottomsheet/BottomSheetDialog;Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    invoke-virtual {p1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    sget-object p1, Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsOverflowBottomDialog$Factory$usu1WyvJIzoWXHIE1i7UDhoDvLc;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$ProfileAttachmentsOverflowBottomDialog$Factory$usu1WyvJIzoWXHIE1i7UDhoDvLc;

    invoke-virtual {v1, p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 71
    invoke-virtual {v1, v2}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->setContentView(Landroid/view/View;)V

    .line 72
    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
