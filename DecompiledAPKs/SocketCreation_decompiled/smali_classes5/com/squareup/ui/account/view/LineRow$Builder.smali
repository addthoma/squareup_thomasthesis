.class public Lcom/squareup/ui/account/view/LineRow$Builder;
.super Ljava/lang/Object;
.source "LineRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/account/view/LineRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private backgroundId:I

.field private chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

.field private final context:Landroid/content/Context;

.field private displayValueAsPercent:Z

.field private glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private nameWeight:Lcom/squareup/marketfont/MarketFont$Weight;

.field private note:Ljava/lang/CharSequence;

.field private noteWeight:Lcom/squareup/marketfont/MarketFont$Weight;

.field private quantity:I

.field private title:Ljava/lang/CharSequence;

.field private titleTextSize:I

.field private value:Ljava/lang/CharSequence;

.field private valueWeight:Lcom/squareup/marketfont/MarketFont$Weight;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    .line 43
    iput v0, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->backgroundId:I

    .line 46
    sget-object v0, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    iput-object v0, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    .line 47
    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    iput-object v0, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->nameWeight:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 48
    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    iput-object v0, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->noteWeight:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 49
    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    iput-object v0, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->valueWeight:Lcom/squareup/marketfont/MarketFont$Weight;

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/ui/account/view/LineRow;
    .locals 17

    move-object/from16 v0, p0

    .line 121
    new-instance v16, Lcom/squareup/ui/account/view/LineRow;

    iget-object v2, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->context:Landroid/content/Context;

    iget-object v3, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->title:Ljava/lang/CharSequence;

    iget-object v5, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->note:Ljava/lang/CharSequence;

    iget v6, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->quantity:I

    iget-object v7, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->value:Ljava/lang/CharSequence;

    iget v8, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->backgroundId:I

    iget-object v9, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    iget-boolean v10, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->displayValueAsPercent:Z

    iget-object v11, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->nameWeight:Lcom/squareup/marketfont/MarketFont$Weight;

    iget-object v12, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->noteWeight:Lcom/squareup/marketfont/MarketFont$Weight;

    iget-object v13, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->valueWeight:Lcom/squareup/marketfont/MarketFont$Weight;

    iget v14, v0, Lcom/squareup/ui/account/view/LineRow$Builder;->titleTextSize:I

    const/4 v15, 0x0

    move-object/from16 v1, v16

    invoke-direct/range {v1 .. v15}, Lcom/squareup/ui/account/view/LineRow;-><init>(Landroid/content/Context;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;ILcom/squareup/marin/widgets/ChevronVisibility;ZLcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/marketfont/MarketFont$Weight;Lcom/squareup/marketfont/MarketFont$Weight;ILcom/squareup/ui/account/view/LineRow$1;)V

    return-object v16
.end method

.method public displayValueAsPercent(Z)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 0

    .line 86
    iput-boolean p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->displayValueAsPercent:Z

    return-object p0
.end method

.method public setBackground(I)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 0

    .line 91
    iput p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->backgroundId:I

    return-object p0
.end method

.method public setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->chevronVisibility:Lcom/squareup/marin/widgets/ChevronVisibility;

    return-object p0
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public setNote(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->note:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setNoteWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->noteWeight:Lcom/squareup/marketfont/MarketFont$Weight;

    return-object p0
.end method

.method public setQuantity(I)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 0

    .line 76
    iput p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->quantity:I

    return-object p0
.end method

.method public setTitle(I)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/LineRow$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;

    move-result-object p1

    return-object p1
.end method

.method public setTitle(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 0

    .line 66
    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->title:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setTitleTextSize(I)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 0

    .line 116
    iput p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->titleTextSize:I

    return-object p0
.end method

.method public setValue(Ljava/lang/CharSequence;)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->value:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setValueWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->valueWeight:Lcom/squareup/marketfont/MarketFont$Weight;

    return-object p0
.end method

.method public setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/ui/account/view/LineRow$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/ui/account/view/LineRow$Builder;->nameWeight:Lcom/squareup/marketfont/MarketFont$Weight;

    return-object p0
.end method
