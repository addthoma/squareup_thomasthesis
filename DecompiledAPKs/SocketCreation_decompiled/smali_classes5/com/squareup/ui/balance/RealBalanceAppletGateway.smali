.class public final Lcom/squareup/ui/balance/RealBalanceAppletGateway;
.super Ljava/lang/Object;
.source "RealBalanceAppletGateway.kt"

# interfaces
.implements Lcom/squareup/ui/balance/BalanceAppletGateway;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\u000e\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eH\u0016J\u0008\u0010\u0010\u001a\u00020\u000fH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/balance/RealBalanceAppletGateway;",
        "Lcom/squareup/ui/balance/BalanceAppletGateway;",
        "balanceApplet",
        "Lcom/squareup/ui/balance/BalanceApplet;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "passcodeEmployees",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "(Lcom/squareup/ui/balance/BalanceApplet;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;)V",
        "activateInitialScreen",
        "",
        "hasPermissionToActivate",
        "Lio/reactivex/Observable;",
        "",
        "isVisible",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final balanceApplet:Lcom/squareup/ui/balance/BalanceApplet;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final passcodeEmployees:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/balance/BalanceApplet;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "balanceApplet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "passcodeEmployees"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionGatekeeper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->balanceApplet:Lcom/squareup/ui/balance/BalanceApplet;

    iput-object p2, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->features:Lcom/squareup/settings/server/Features;

    iput-object p3, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->passcodeEmployees:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iput-object p4, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    return-void
.end method

.method public static final synthetic access$getBalanceApplet$p(Lcom/squareup/ui/balance/RealBalanceAppletGateway;)Lcom/squareup/ui/balance/BalanceApplet;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->balanceApplet:Lcom/squareup/ui/balance/BalanceApplet;

    return-object p0
.end method

.method public static final synthetic access$getPasscodeEmployees$p(Lcom/squareup/ui/balance/RealBalanceAppletGateway;)Lcom/squareup/permissions/PasscodeEmployeeManagement;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->passcodeEmployees:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    return-object p0
.end method

.method public static final synthetic access$getPermissionGatekeeper$p(Lcom/squareup/ui/balance/RealBalanceAppletGateway;)Lcom/squareup/permissions/PermissionGatekeeper;
    .locals 0

    .line 10
    iget-object p0, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    return-object p0
.end method


# virtual methods
.method public activateInitialScreen()V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->balanceApplet:Lcom/squareup/ui/balance/BalanceApplet;

    invoke-virtual {v0}, Lcom/squareup/ui/balance/BalanceApplet;->activate()V

    return-void
.end method

.method public hasPermissionToActivate()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->passcodeEmployees:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onCurrentEmployeeChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/squareup/ui/balance/RealBalanceAppletGateway$hasPermissionToActivate$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/RealBalanceAppletGateway$hasPermissionToActivate$1;-><init>(Lcom/squareup/ui/balance/RealBalanceAppletGateway;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "passcodeEmployees.onCurr\u2026())\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public isVisible()Z
    .locals 2

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_BALANCE_APPLET:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
