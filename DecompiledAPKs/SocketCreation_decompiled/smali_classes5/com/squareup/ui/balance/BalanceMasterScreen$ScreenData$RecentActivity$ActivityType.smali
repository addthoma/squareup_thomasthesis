.class final enum Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;
.super Ljava/lang/Enum;
.source "BalanceMasterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ActivityType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

.field public static final enum CARD:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

.field public static final enum DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

.field public static final enum NONE:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

.field public static final enum UNIFIED_ACTIVITY:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 107
    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    const/4 v1, 0x0

    const-string v2, "NONE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->NONE:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    const/4 v2, 0x1

    const-string v3, "DEPOSIT"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    const/4 v3, 0x2

    const-string v4, "CARD"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->CARD:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    new-instance v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    const/4 v4, 0x3

    const-string v5, "UNIFIED_ACTIVITY"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->UNIFIED_ACTIVITY:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    sget-object v5, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->NONE:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->DEPOSIT:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->CARD:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->UNIFIED_ACTIVITY:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->$VALUES:[Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 107
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;
    .locals 1

    .line 107
    const-class v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;
    .locals 1

    .line 107
    sget-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->$VALUES:[Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    invoke-virtual {v0}, [Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity$ActivityType;

    return-object v0
.end method
