.class public final Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;
.super Ljava/lang/Object;
.source "SquareCardActivityRequester.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;,
        Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;,
        Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardActivityRequester.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardActivityRequester.kt\ncom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,251:1\n1462#2,8:252\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardActivityRequester.kt\ncom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester\n*L\n191#1,8:252\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 52\u00020\u0001:\u0003345B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J \u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u00132\u000e\u0010\u0015\u001a\n\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0013H\u0002J\u0014\u0010\u0016\u001a\u00020\u00172\n\u0008\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u000cH\u0002J\u0008\u0010\u0019\u001a\u00020\u0011H\u0002J\u0008\u0010\u001a\u001a\u00020\u0007H\u0002J\u000e\u0010\u001b\u001a\u00020\u00112\u0006\u0010\u001c\u001a\u00020\u000cJ\u0010\u0010\u001d\u001a\u00020\u00112\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0008\u0010 \u001a\u00020\u0011H\u0016J\u0008\u0010!\u001a\u00020\u0007H\u0002J\u0010\u0010\"\u001a\u00020\u00072\u0006\u0010#\u001a\u00020$H\u0002J\u0008\u0010%\u001a\u00020\u0007H\u0002J\u0010\u0010&\u001a\u00020\u00072\u0006\u0010#\u001a\u00020$H\u0002J\u000c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u00110(J\u0008\u0010)\u001a\u00020\u0011H\u0002J\u0010\u0010*\u001a\u00020\u00112\u0006\u0010#\u001a\u00020\u000fH\u0002J\u0006\u0010+\u001a\u00020\u0011J\u000c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\u00070(J\u0016\u0010-\u001a\u00020\u00112\u0006\u0010.\u001a\u00020\u000c2\u0006\u0010/\u001a\u000200J\u000c\u00101\u001a\u000202*\u00020$H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0005\u001a\u0010\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00070\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u0010\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u000f0\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0010\u001a\u0010\u0012\u000c\u0012\n \u0008*\u0004\u0018\u00010\u00110\u00110\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;",
        "Lmortar/Scoped;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V",
        "cardActivityState",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;",
        "kotlin.jvm.PlatformType",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "loadMoreLastToken",
        "",
        "transactionCategoryUpdated",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
        "updateTransactionCategoryFailure",
        "",
        "combineIntoSingleList",
        "",
        "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
        "events",
        "createCardActivityRequest",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;",
        "batchToken",
        "fetchCardActivityData",
        "latestValue",
        "loadMoreCardActivity",
        "token",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onLoadCardActivityFailure",
        "onLoadCardActivitySuccess",
        "response",
        "Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;",
        "onLoadMoreBalanceError",
        "onLoadMoreBalanceSuccess",
        "onTransactionCategoryFailure",
        "Lio/reactivex/Observable;",
        "onUpdateTransactionCategoryFailed",
        "onUpdateTransactionCategorySuccess",
        "refreshData",
        "state",
        "updateTransactionCategory",
        "transactionToken",
        "isPersonalExpense",
        "",
        "toCardStatus",
        "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;",
        "CardActivityState",
        "CardActivityStatus",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final ALLOWED_ACTIVITY_TYPES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DEFAULT_BATCH_SIZE:I = 0x14


# instance fields
.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

.field private final cardActivityState:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;",
            ">;"
        }
    .end annotation
.end field

.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;

.field private loadMoreLastToken:Ljava/lang/String;

.field private final transactionCategoryUpdated:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final updateTransactionCategoryFailure:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->Companion:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$Companion;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    .line 226
    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->CARD_PAYMENT:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ATM_WITHDRAWAL:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->ATM_WITHDRAWAL_FEE:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;->CARD_PAYMENT_REWARD:Lcom/squareup/protos/client/bizbank/CardActivityEvent$ActivityType;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->ALLOWED_ACTIVITY_TYPES:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    .line 42
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string v0, "BehaviorRelay.create<CardActivityState>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->cardActivityState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 43
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string v0, "PublishRelay.create<SetT\u2026actionCategoryResponse>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->transactionCategoryUpdated:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 44
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string v0, "PublishRelay.create<Unit>()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->updateTransactionCategoryFailure:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 45
    new-instance p1, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    const-string p1, ""

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->loadMoreLastToken:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getALLOWED_ACTIVITY_TYPES$cp()Ljava/util/List;
    .locals 1

    .line 38
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->ALLOWED_ACTIVITY_TYPES:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getCardActivityState$p(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->cardActivityState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object p0
.end method

.method public static final synthetic access$getLoadMoreLastToken$p(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)Ljava/lang/String;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->loadMoreLastToken:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$onLoadCardActivityFailure(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->onLoadCardActivityFailure()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onLoadCardActivitySuccess(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->onLoadCardActivitySuccess(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onLoadMoreBalanceError(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->onLoadMoreBalanceError()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onLoadMoreBalanceSuccess(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->onLoadMoreBalanceSuccess(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onUpdateTransactionCategoryFailed(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->onUpdateTransactionCategoryFailed()V

    return-void
.end method

.method public static final synthetic access$onUpdateTransactionCategorySuccess(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->onUpdateTransactionCategorySuccess(Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;)V

    return-void
.end method

.method public static final synthetic access$setLoadMoreLastToken$p(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Ljava/lang/String;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->loadMoreLastToken:Ljava/lang/String;

    return-void
.end method

.method private final combineIntoSingleList(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;"
        }
    .end annotation

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->cardActivityState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->getCardActivities()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez p1, :cond_1

    return-object v0

    :cond_1
    if-nez v0, :cond_2

    return-object p1

    .line 191
    :cond_2
    check-cast v0, Ljava/util/Collection;

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 252
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 253
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 254
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 255
    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/bizbank/CardActivityEvent;

    .line 191
    iget-object v3, v3, Lcom/squareup/protos/client/bizbank/CardActivityEvent;->token:Ljava/lang/String;

    .line 256
    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 257
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 259
    :cond_4
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method private final createCardActivityRequest(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;
    .locals 3

    .line 209
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;-><init>()V

    .line 210
    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->ALLOWED_ACTIVITY_TYPES:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->include_activity_types(Ljava/util/List;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;

    move-result-object v0

    .line 211
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;

    move-result-object v0

    .line 212
    new-instance v1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest$Builder;-><init>()V

    const/16 v2, 0x14

    .line 213
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest$Builder;->batch_size(Ljava/lang/Integer;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest$Builder;

    move-result-object v1

    .line 214
    invoke-virtual {v1, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest$Builder;->batch_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest$Builder;

    move-result-object p1

    .line 215
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;

    move-result-object p1

    .line 217
    new-instance v1, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;-><init>()V

    .line 218
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->filters(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Filters;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;

    move-result-object v0

    .line 219
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->batch_request(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$BatchRequest;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;

    move-result-object p1

    .line 220
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/GetCardActivityRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    move-result-object p1

    const-string v0, "GetCardActivityRequest.B\u2026Request)\n        .build()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method static synthetic createCardActivityRequest$default(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 208
    check-cast p1, Ljava/lang/String;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->createCardActivityRequest(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    move-result-object p0

    return-object p0
.end method

.method private final fetchCardActivityData()V
    .locals 4

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {p0, v2, v3, v2}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->createCardActivityRequest$default(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->getCardActivity(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v1

    .line 148
    invoke-virtual {v1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v1

    .line 149
    new-instance v2, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$fetchCardActivityData$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$fetchCardActivityData$1;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v1

    .line 155
    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->cardActivityState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    const-string v2, "bizbankService.getCardAc\u2026scribe(cardActivityState)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method private final latestValue()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 2

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->cardActivityState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "cardActivityState.value!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    return-object v0
.end method

.method private final onLoadCardActivityFailure()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 8

    .line 195
    new-instance v7, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    .line 196
    sget-object v1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->COULD_NOT_LOAD_CARD_ACTIVITY:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    .line 198
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v0, v7

    .line 195
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v7
.end method

.method private final onLoadCardActivitySuccess(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 8

    .line 161
    new-instance v7, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    .line 162
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->toCardStatus(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    move-result-object v1

    .line 163
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;->next_batch_token:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v3, v0

    .line 164
    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->activity_list:Ljava/util/List;

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_1
    move-object v4, p1

    const/4 v5, 0x2

    const/4 v6, 0x0

    const/4 v2, 0x0

    move-object v0, v7

    .line 161
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v7
.end method

.method private final onLoadMoreBalanceError()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 8

    const-string v0, ""

    .line 203
    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->loadMoreLastToken:Ljava/lang/String;

    .line 205
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->latestValue()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object v1

    sget-object v3, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->COULD_NOT_LOAD_CARD_ACTIVITY:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xd

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->copy$default(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object v0

    return-object v0
.end method

.method private final onLoadMoreBalanceSuccess(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;
    .locals 4

    .line 169
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    .line 170
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->toCardStatus(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    move-result-object v1

    .line 171
    iget-object v2, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->batch_response:Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;

    if-eqz v2, :cond_0

    iget-object v2, v2, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse$BatchResponse;->next_batch_token:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 172
    :goto_0
    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->activity_list:Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->combineIntoSingleList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    .line 173
    :goto_1
    sget-object v3, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->CARD_ACTIVITY_LOADED:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    .line 169
    invoke-direct {v0, v1, v3, v2, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method private final onUpdateTransactionCategoryFailed()V
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->updateTransactionCategoryFailure:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final onUpdateTransactionCategorySuccess(Lcom/squareup/protos/client/bizbank/SetTransactionCategoryResponse;)V
    .locals 1

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->transactionCategoryUpdated:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final toCardStatus(Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;
    .locals 0

    .line 178
    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/GetCardActivityResponse;->activity_list:Ljava/util/List;

    if-nez p1, :cond_0

    .line 179
    sget-object p1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->COULD_NOT_LOAD_CARD_ACTIVITY:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    return-object p1

    .line 182
    :cond_0
    sget-object p1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->CARD_ACTIVITY_LOADED:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    return-object p1
.end method


# virtual methods
.method public final loadMoreCardActivity(Ljava/lang/String;)V
    .locals 8

    const-string/jumbo v0, "token"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->loadMoreLastToken:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->cardActivityState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->latestValue()Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;->LOADING:Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xd

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;->copy$default(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->createCardActivityRequest(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->getCardActivity(Lcom/squareup/protos/client/bizbank/GetCardActivityRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v1

    .line 96
    invoke-virtual {v1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v1

    .line 97
    new-instance v2, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$loadMoreCardActivity$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$loadMoreCardActivity$1;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    .line 98
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$loadMoreCardActivity$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$loadMoreCardActivity$2;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 104
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->cardActivityState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v1, "bizbankService.getCardAc\u2026scribe(cardActivityState)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-static {v0, p1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->refreshData()V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->transactionCategoryUpdated:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    .line 54
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->state()Lio/reactivex/Observable;

    move-result-object v1

    check-cast v1, Lio/reactivex/ObservableSource;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 55
    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$onEnterScope$1;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    return-void
.end method

.method public final onTransactionCategoryFailure()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->updateTransactionCategoryFailure:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public final refreshData()V
    .locals 9

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->cardActivityState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v8, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xf

    const/4 v7, 0x0

    move-object v1, v8

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityStatus;Ljava/lang/String;Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {v0, v8}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    const-string v0, ""

    .line 134
    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->loadMoreLastToken:Ljava/lang/String;

    .line 135
    invoke-direct {p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->fetchCardActivityData()V

    return-void
.end method

.method public final state()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$CardActivityState;",
            ">;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->cardActivityState:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method

.method public final updateTransactionCategory(Ljava/lang/String;Z)V
    .locals 2

    const-string/jumbo v0, "transactionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    new-instance v0, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;-><init>()V

    .line 117
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;->transaction_token(Ljava/lang/String;)Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;

    move-result-object p1

    .line 118
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;->is_personal_expense(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;

    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest;

    move-result-object p1

    .line 121
    iget-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->setTransactionCategory(Lcom/squareup/protos/client/bizbank/SetTransactionCategoryRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 122
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 123
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$updateTransactionCategory$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester$updateTransactionCategory$1;-><init>(Lcom/squareup/ui/balance/bizbanking/transactions/SquareCardActivityRequester;)V

    check-cast v0, Lio/reactivex/functions/Consumer;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    const-string v0, "bizbankService.setTransa\u2026d()\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    invoke-static {p2, p1}, Lcom/squareup/util/rx2/Rx2Kt;->plusAssign(Lio/reactivex/disposables/CompositeDisposable;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method
