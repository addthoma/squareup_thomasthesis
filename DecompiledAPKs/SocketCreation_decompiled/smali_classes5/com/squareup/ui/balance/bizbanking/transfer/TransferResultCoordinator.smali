.class public Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "TransferResultCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private res:Landroid/content/res/Resources;

.field private final runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;

.field private spinner:Landroid/widget/LinearLayout;

.field private transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 103
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 104
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transfer_result_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 105
    sget v0, Lcom/squareup/balance/applet/impl/R$id;->transfer_result_spinner:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->spinner:Landroid/widget/LinearLayout;

    return-void
.end method

.method public static synthetic lambda$HMbuw_jpTuxY1jGlIqLc5JR1Qzg(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->onScreenData(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;)V

    return-void
.end method

.method private onScreenData(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;)V
    .locals 6

    .line 56
    sget-object v0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator$1;->$SwitchMap$com$squareup$ui$balance$bizbanking$transfer$TransferToBankRequester$DepositStatus:[I

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->requestState:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;

    invoke-virtual {v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester$DepositStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x8

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto/16 :goto_0

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->res:Landroid/content/res/Resources;

    sget v5, Lcom/squareup/balance/applet/impl/R$string;->transfer_result_error_title:I

    .line 91
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 90
    invoke-virtual {v0, v1, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object v1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->message:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 63
    iget-boolean v0, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->isInstant:Z

    if-eqz v0, :cond_3

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->res:Landroid/content/res/Resources;

    sget v5, Lcom/squareup/balance/applet/impl/R$string;->transfer_complete:I

    .line 65
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 64
    invoke-virtual {v0, v1, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->res:Landroid/content/res/Resources;

    sget v4, Lcom/squareup/balance/applet/impl/R$string;->transfer_result_instant_title:I

    invoke-static {v1, v4}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v4, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v5, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->desiredDeposit:Lcom/squareup/protos/common/Money;

    .line 67
    invoke-interface {v4, v5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, "amount"

    invoke-virtual {v1, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 68
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 66
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    .line 69
    iget-boolean v0, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->instantDepositRequiresLinkedCard:Z

    if-eqz v0, :cond_2

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->res:Landroid/content/res/Resources;

    sget v4, Lcom/squareup/balance/applet/impl/R$string;->transfer_result_instant_message_with_card:I

    .line 71
    invoke-static {v1, v4}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v4, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->res:Landroid/content/res/Resources;

    iget v5, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->brandNameId:I

    .line 72
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "card_brand"

    invoke-virtual {v1, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->panSuffix:Ljava/lang/String;

    const-string v4, "card_suffix"

    .line 73
    invoke-virtual {v1, v4, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 74
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 70
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 76
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->res:Landroid/content/res/Resources;

    sget v4, Lcom/squareup/balance/applet/impl/R$string;->transfer_result_instant_message_with_bank:I

    .line 77
    invoke-static {v1, v4}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v4, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->bankName:Ljava/lang/String;

    const-string v5, "bank_name"

    .line 78
    invoke-virtual {v1, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$ScreenData;->accountNumberSuffix:Ljava/lang/String;

    const-string v4, "account_number_suffix"

    .line 79
    invoke-virtual {v1, v4, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 80
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 76
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 83
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->res:Landroid/content/res/Resources;

    sget v4, Lcom/squareup/balance/applet/impl/R$string;->transfer_result_standard_title:I

    .line 84
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 83
    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    sget v0, Lcom/squareup/balance/applet/impl/R$string;->transfer_in_progress:I

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(I)V

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    sget v0, Lcom/squareup/balance/applet/impl/R$string;->transfer_result_standard_message:I

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(I)V

    .line 97
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->show()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->spinner:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, v2}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    return-void

    .line 58
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {p1, v3}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVisibility(I)V

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->spinner:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    .line 42
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->bindViews(Landroid/view/View;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->transferResultMessage:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferResultCoordinator$Bu8W6scuGiD1icsCnfs8QIW0Iik;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferResultCoordinator$Bu8W6scuGiD1icsCnfs8QIW0Iik;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->res:Landroid/content/res/Resources;

    .line 47
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$vBjlhaPboz6EsLXWGcRPJ3TNYTI;

    invoke-direct {v2, v1}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$vBjlhaPboz6EsLXWGcRPJ3TNYTI;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;)V

    .line 48
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->actionBarConfigBuilder:Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hide()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 51
    new-instance v0, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferResultCoordinator$bCXEWoOvbpOpEk4cfThJUyTu3MA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferResultCoordinator$bCXEWoOvbpOpEk4cfThJUyTu3MA;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$TransferResultCoordinator(Landroid/view/View;)V
    .locals 0

    .line 44
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;->onDoneFromTransferResultClicked()V

    return-void
.end method

.method public synthetic lambda$attach$1$TransferResultCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;->runner:Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultScreen$Runner;->transferResultScreenData()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferResultCoordinator$HMbuw_jpTuxY1jGlIqLc5JR1Qzg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/-$$Lambda$TransferResultCoordinator$HMbuw_jpTuxY1jGlIqLc5JR1Qzg;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferResultCoordinator;)V

    .line 52
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
