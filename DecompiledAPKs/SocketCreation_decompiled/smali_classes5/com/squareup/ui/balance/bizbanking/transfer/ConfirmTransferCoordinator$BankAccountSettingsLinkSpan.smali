.class Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$BankAccountSettingsLinkSpan;
.super Lcom/squareup/ui/LinkSpan;
.source "ConfirmTransferCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BankAccountSettingsLinkSpan"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;Landroid/content/res/Resources;)V
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$BankAccountSettingsLinkSpan;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;

    .line 154
    sget p1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$BankAccountSettingsLinkSpan;->DEFAULT_COLOR_ID:I

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 158
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$BankAccountSettingsLinkSpan;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;->access$200(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator;)Lcom/squareup/permissions/PermissionGatekeeper;

    move-result-object p1

    sget-object v0, Lcom/squareup/permissions/Permission;->CREATE_BANK_ACCOUNT:Lcom/squareup/permissions/Permission;

    new-instance v1, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$BankAccountSettingsLinkSpan$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$BankAccountSettingsLinkSpan$1;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/ConfirmTransferCoordinator$BankAccountSettingsLinkSpan;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
