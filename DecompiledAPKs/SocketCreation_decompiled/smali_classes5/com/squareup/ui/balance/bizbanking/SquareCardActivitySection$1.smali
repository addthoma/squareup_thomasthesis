.class public final Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection$1;
.super Lcom/squareup/applet/SectionAccess;
.source "SquareCardActivitySection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"
    }
    d2 = {
        "com/squareup/ui/balance/bizbanking/SquareCardActivitySection$1",
        "Lcom/squareup/applet/SectionAccess;",
        "determineVisibility",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0

    .line 13
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection$1;->$settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/SquareCardActivitySection$1;->$settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getBusinessBankingSettings()Lcom/squareup/settings/server/BusinessBankingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/BusinessBankingSettings;->showCardSpend()Z

    move-result v0

    return v0
.end method
