.class public interface abstract Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;
.super Ljava/lang/Object;
.source "InstantDepositResultScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0006H&J\u0008\u0010\u0007\u001a\u00020\u0006H&J\u0008\u0010\u0008\u001a\u00020\u0006H&J\u0008\u0010\t\u001a\u00020\u0006H&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$Runner;",
        "",
        "instantDepositResultScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;",
        "onDoneFromInstantDepositResultClicked",
        "",
        "onExitFromInstantDepositResultClicked",
        "onLearnMoreFromInstantDepositResultClicked",
        "onSquareCardUpsellClicked",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract instantDepositResultScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/balance/bizbanking/InstantDepositResultScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onDoneFromInstantDepositResultClicked()V
.end method

.method public abstract onExitFromInstantDepositResultClicked()V
.end method

.method public abstract onLearnMoreFromInstantDepositResultClicked()V
.end method

.method public abstract onSquareCardUpsellClicked()V
.end method
