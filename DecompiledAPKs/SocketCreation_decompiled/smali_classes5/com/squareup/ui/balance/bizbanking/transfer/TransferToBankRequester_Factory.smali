.class public final Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;
.super Ljava/lang/Object;
.source "TransferToBankRequester_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/transfers/TransfersService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/transfers/TransfersService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p6, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/instantdeposit/InstantDepositRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/balance/core/server/transfers/TransfersService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)",
            "Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;"
        }
    .end annotation

    .line 51
    new-instance v7, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;
    .locals 8

    .line 57
    new-instance v7, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;
    .locals 7

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/instantdeposit/InstantDepositRunner;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/balance/core/server/bizbank/BizbankService;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/balance/core/server/transfers/TransfersService;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/protos/common/CurrencyCode;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->newInstance(Lcom/squareup/instantdeposit/InstantDepositRunner;Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/balance/core/server/transfers/TransfersService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester_Factory;->get()Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankRequester;

    move-result-object v0

    return-object v0
.end method
