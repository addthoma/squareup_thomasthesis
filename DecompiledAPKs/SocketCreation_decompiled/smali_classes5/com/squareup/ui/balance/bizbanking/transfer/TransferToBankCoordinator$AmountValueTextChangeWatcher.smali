.class Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;
.super Lcom/squareup/debounce/DebouncedTextWatcher;
.source "TransferToBankCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AmountValueTextChangeWatcher"
.end annotation


# instance fields
.field private oldAmount:Lcom/squareup/protos/common/Money;

.field final synthetic this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)V
    .locals 2

    .line 217
    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedTextWatcher;-><init>()V

    .line 218
    sget-object p1, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;->oldAmount:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$1;)V
    .locals 0

    .line 217
    invoke-direct {p0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;-><init>(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)V

    return-void
.end method


# virtual methods
.method public doAfterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->access$100(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Lcom/squareup/widgets/OnScreenRectangleEditText;

    move-result-object v0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/OnScreenRectangleEditText;->setSelection(I)V

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->access$200(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Lcom/squareup/money/MoneyLocaleHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/money/MoneyLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 225
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;->oldAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->isEqual(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 228
    :cond_0
    iput-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;->oldAmount:Lcom/squareup/protos/common/Money;

    .line 229
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->access$400(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->access$300(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 230
    iget-object v1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->access$500(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankScreen$Runner;->updateFee(Lcom/squareup/protos/common/Money;)V

    .line 231
    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;

    invoke-static {v0, p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->access$600(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 232
    iget-object p1, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->access$800(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Lcom/squareup/marketfont/MarketTextView;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator$AmountValueTextChangeWatcher;->this$0:Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;->access$700(Lcom/squareup/ui/balance/bizbanking/transfer/TransferToBankCoordinator;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method
