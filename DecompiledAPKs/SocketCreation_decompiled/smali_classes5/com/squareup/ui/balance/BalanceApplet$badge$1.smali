.class final Lcom/squareup/ui/balance/BalanceApplet$badge$1;
.super Ljava/lang/Object;
.source "BalanceApplet.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/BalanceApplet;->badge()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/applet/Applet$Badge;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/balance/BalanceApplet;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/BalanceApplet;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/BalanceApplet$badge$1;->this$0:Lcom/squareup/ui/balance/BalanceApplet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lcom/squareup/applet/Applet$Badge;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/applet/Applet$Badge;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 78
    new-instance p1, Lcom/squareup/applet/Applet$Badge$Visible;

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/balance/BalanceApplet$badge$1;->this$0:Lcom/squareup/ui/balance/BalanceApplet;

    invoke-static {v0}, Lcom/squareup/ui/balance/BalanceApplet;->access$getResources$p(Lcom/squareup/ui/balance/BalanceApplet;)Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/squareup/balance/applet/impl/R$string;->account_freeze_applet_alert:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "resources.getString(R.st\u2026ount_freeze_applet_alert)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/CharSequence;

    .line 81
    sget-object v2, Lcom/squareup/applet/Applet$Badge$Priority;->FATAL:Lcom/squareup/applet/Applet$Badge$Priority;

    .line 78
    invoke-direct {p1, v1, v0, v2}, Lcom/squareup/applet/Applet$Badge$Visible;-><init>(ILjava/lang/CharSequence;Lcom/squareup/applet/Applet$Badge$Priority;)V

    check-cast p1, Lcom/squareup/applet/Applet$Badge;

    goto :goto_0

    :cond_0
    const-string v0, "showCardUpsell"

    .line 83
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/applet/Applet$Badge;->Companion:Lcom/squareup/applet/Applet$Badge$Companion;

    const/4 v0, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v1, v2, v0, v2}, Lcom/squareup/applet/Applet$Badge$Companion;->toBadge$default(Lcom/squareup/applet/Applet$Badge$Companion;ILcom/squareup/applet/Applet$Badge$Priority;ILjava/lang/Object;)Lcom/squareup/applet/Applet$Badge;

    move-result-object p1

    goto :goto_0

    .line 84
    :cond_1
    sget-object p1, Lcom/squareup/applet/Applet$Badge$Hidden;->INSTANCE:Lcom/squareup/applet/Applet$Badge$Hidden;

    check-cast p1, Lcom/squareup/applet/Applet$Badge;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/BalanceApplet$badge$1;->apply(Lkotlin/Pair;)Lcom/squareup/applet/Applet$Badge;

    move-result-object p1

    return-object p1
.end method
