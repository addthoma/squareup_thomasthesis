.class public Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
.super Ljava/lang/Object;
.source "BalanceMasterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/balance/BalanceMasterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;,
        Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;
    }
.end annotation


# static fields
.field public static final LOADING:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;


# instance fields
.field public final allowAddMoney:Z

.field public final balance:Lcom/squareup/protos/common/Money;

.field public final balanceTitle:Ljava/lang/CharSequence;

.field public final buttonText:Ljava/lang/CharSequence;

.field public final buttonType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

.field public final hint:Ljava/lang/CharSequence;

.field public final instantDepositSnapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

.field public final recentActivity:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 79
    invoke-static {}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->asLoading()Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->LOADING:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
            "Ljava/lang/CharSequence;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/CharSequence;",
            "Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;",
            "Ljava/lang/CharSequence;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;",
            "Lcom/squareup/balance/activity/data/BalanceActivity;",
            "Z)V"
        }
    .end annotation

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->instantDepositSnapshot:Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    .line 97
    iput-object p2, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->balanceTitle:Ljava/lang/CharSequence;

    .line 98
    iput-object p3, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->balance:Lcom/squareup/protos/common/Money;

    .line 99
    iput-object p4, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->buttonText:Ljava/lang/CharSequence;

    .line 100
    iput-object p5, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->buttonType:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    .line 101
    iput-object p6, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->hint:Ljava/lang/CharSequence;

    .line 102
    invoke-static {p7, p8, p9}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;->recentActivity(Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;)Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->recentActivity:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$RecentActivity;

    .line 103
    iput-boolean p10, p0, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;->allowAddMoney:Z

    return-void
.end method

.method private static asLoading()Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;
    .locals 12

    .line 216
    new-instance v11, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;

    new-instance v1, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    invoke-direct {v1}, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;-><init>()V

    sget-object v5, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;->NONE:Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData;-><init>(Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;Ljava/lang/CharSequence;Lcom/squareup/protos/common/Money;Ljava/lang/CharSequence;Lcom/squareup/ui/balance/BalanceMasterScreen$ScreenData$ButtonType;Ljava/lang/CharSequence;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/balance/activity/data/BalanceActivity;Z)V

    return-object v11
.end method
