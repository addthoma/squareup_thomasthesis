.class final Lcom/squareup/ui/balance/RealBalanceAppletGateway$hasPermissionToActivate$1;
.super Ljava/lang/Object;
.source "RealBalanceAppletGateway.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/balance/RealBalanceAppletGateway;->hasPermissionToActivate()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Z"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/balance/RealBalanceAppletGateway;


# direct methods
.method constructor <init>(Lcom/squareup/ui/balance/RealBalanceAppletGateway;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway$hasPermissionToActivate$1;->this$0:Lcom/squareup/ui/balance/RealBalanceAppletGateway;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/balance/RealBalanceAppletGateway$hasPermissionToActivate$1;->apply(Lkotlin/Unit;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lkotlin/Unit;)Z
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object p1, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway$hasPermissionToActivate$1;->this$0:Lcom/squareup/ui/balance/RealBalanceAppletGateway;

    invoke-static {p1}, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->access$getPermissionGatekeeper$p(Lcom/squareup/ui/balance/RealBalanceAppletGateway;)Lcom/squareup/permissions/PermissionGatekeeper;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/permissions/PermissionGatekeeper;->shouldAskForPasscode()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway$hasPermissionToActivate$1;->this$0:Lcom/squareup/ui/balance/RealBalanceAppletGateway;

    invoke-static {p1}, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->access$getPasscodeEmployees$p(Lcom/squareup/ui/balance/RealBalanceAppletGateway;)Lcom/squareup/permissions/PasscodeEmployeeManagement;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 37
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway$hasPermissionToActivate$1;->this$0:Lcom/squareup/ui/balance/RealBalanceAppletGateway;

    invoke-static {p1}, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->access$getPermissionGatekeeper$p(Lcom/squareup/ui/balance/RealBalanceAppletGateway;)Lcom/squareup/permissions/PermissionGatekeeper;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/balance/RealBalanceAppletGateway$hasPermissionToActivate$1;->this$0:Lcom/squareup/ui/balance/RealBalanceAppletGateway;

    invoke-static {v0}, Lcom/squareup/ui/balance/RealBalanceAppletGateway;->access$getBalanceApplet$p(Lcom/squareup/ui/balance/RealBalanceAppletGateway;)Lcom/squareup/ui/balance/BalanceApplet;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/balance/BalanceApplet;->getPermissions()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->hasAnyPermission(Ljava/util/Set;)Z

    move-result p1

    :goto_0
    return p1
.end method
