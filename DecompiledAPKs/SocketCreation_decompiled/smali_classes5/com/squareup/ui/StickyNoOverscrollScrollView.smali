.class public Lcom/squareup/ui/StickyNoOverscrollScrollView;
.super Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;
.source "StickyNoOverscrollScrollView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1, p2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x2

    .line 12
    invoke-virtual {p0, p1}, Lcom/squareup/ui/StickyNoOverscrollScrollView;->setOverScrollMode(I)V

    return-void
.end method


# virtual methods
.method public setOverScrollMode(I)V
    .locals 0

    const/4 p1, 0x2

    .line 16
    invoke-super {p0, p1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->setOverScrollMode(I)V

    return-void
.end method
