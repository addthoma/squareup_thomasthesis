.class Lcom/squareup/ui/library/PriceEntryScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "PriceEntryScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/library/PriceEntryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/library/PriceEntryScreen$Presenter$MoneyEntryKeypadListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/library/PriceEntryView;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final currency:Lcom/squareup/protos/common/CurrencyCode;

.field private final device:Lcom/squareup/util/Device;

.field private listener:Lcom/squareup/padlock/MoneyKeypadListener;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final priceEntryScreenRunner:Lcom/squareup/ui/library/PriceEntryScreenRunner;

.field private final res:Lcom/squareup/util/Res;

.field private state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final vibrator:Landroid/os/Vibrator;

.field private final workingItemBundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/library/PriceEntryScreenRunner;Landroid/os/Vibrator;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/log/cart/TransactionInteractionsLogger;Lcom/squareup/payment/Transaction;Lcom/squareup/BundleKey;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/ui/library/PriceEntryScreenRunner;",
            "Landroid/os/Vibrator;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 81
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->device:Lcom/squareup/util/Device;

    .line 83
    iput-object p2, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 84
    iput-object p3, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 85
    iput-object p4, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    .line 86
    iput-object p5, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 87
    iput-object p6, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->priceEntryScreenRunner:Lcom/squareup/ui/library/PriceEntryScreenRunner;

    .line 88
    iput-object p7, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->vibrator:Landroid/os/Vibrator;

    .line 89
    iput-object p8, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    .line 90
    iput-object p10, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 91
    iput-object p11, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->workingItemBundleKey:Lcom/squareup/BundleKey;

    .line 92
    iput-object p9, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    .line 93
    iput-object p12, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/ui/library/PriceEntryScreen$Presenter;)Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/ui/library/PriceEntryScreen$Presenter;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->currency:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method static synthetic access$1300(Lcom/squareup/ui/library/PriceEntryScreen$Presenter;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->displayAmount()V

    return-void
.end method

.method static synthetic access$1400(Lcom/squareup/ui/library/PriceEntryScreen$Presenter;)V
    .locals 0

    .line 59
    invoke-direct {p0}, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->onCommitSelected()V

    return-void
.end method

.method static synthetic access$800(Lcom/squareup/ui/library/PriceEntryScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 59
    invoke-virtual {p0}, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/ui/library/PriceEntryScreen$Presenter;)Landroid/os/Vibrator;
    .locals 0

    .line 59
    iget-object p0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->vibrator:Landroid/os/Vibrator;

    return-object p0
.end method

.method private displayAmount()V
    .locals 5

    .line 150
    invoke-virtual {p0}, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/PriceEntryView;

    iget-object v1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v2, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    invoke-static {v2}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->access$600(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    .line 151
    invoke-static {v3}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->access$700(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;)Ljava/lang/String;

    move-result-object v3

    .line 150
    invoke-virtual {v1, v2, v3}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/PriceEntryView;->setAmountText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    invoke-static {v0}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->access$600(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    .line 153
    invoke-virtual {p0}, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/PriceEntryView;

    iget-object v1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/PriceEntryView;->setAmountColor(I)V

    goto :goto_0

    .line 155
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/library/PriceEntryView;

    iget-object v1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/library/PriceEntryView;->setAmountColor(I)V

    :goto_0
    return-void
.end method

.method public static synthetic lambda$cZrL2_-wV4z5fjXd1Zo2cod-w54(Lcom/squareup/ui/library/PriceEntryScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->onCommitSelected()V

    return-void
.end method

.method private onCommitSelected()V
    .locals 3

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_PRICE_ENTRY:Lcom/squareup/analytics/RegisterViewName;

    iget-object v2, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    invoke-static {v2}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->access$400(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;)Lcom/squareup/configure/item/WorkingItem;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingItem;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    invoke-static {v0}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->access$500(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;)V

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->device:Lcom/squareup/util/Device;

    invoke-static {v0}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->supportsFlyByAnimations(Lcom/squareup/util/Device;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->getFlyByAnimationData()Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/orderentry/OrderEntryScreenState$FlyByAnimationData;->setQuantity(I)V

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->priceEntryScreenRunner:Lcom/squareup/ui/library/PriceEntryScreenRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/library/PriceEntryScreenRunner;->finishPriceEntry()V

    return-void
.end method


# virtual methods
.method onCancelSelected()V
    .locals 1

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v0}, Lcom/squareup/orderentry/OrderEntryScreenState;->clearFlyByAnimationData()V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->priceEntryScreenRunner:Lcom/squareup/ui/library/PriceEntryScreenRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/library/PriceEntryScreenRunner;->finishPriceEntry()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 97
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/PriceEntryScreen;

    .line 98
    invoke-static {p1}, Lcom/squareup/ui/library/PriceEntryScreen;->access$000(Lcom/squareup/ui/library/PriceEntryScreen;)Lcom/squareup/configure/item/WorkingItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-static {p1}, Lcom/squareup/ui/library/PriceEntryScreen;->access$000(Lcom/squareup/ui/library/PriceEntryScreen;)Lcom/squareup/configure/item/WorkingItem;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->workingItemBundleKey:Lcom/squareup/BundleKey;

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->buildLoadedWorkingItemState(Lcom/squareup/payment/Transaction;Lcom/squareup/configure/item/WorkingItem;Lcom/squareup/BundleKey;)Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    goto :goto_0

    .line 101
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->workingItemBundleKey:Lcom/squareup/BundleKey;

    invoke-static {p1, v0}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->buildEmptyWorkingItemState(Lcom/squareup/payment/Transaction;Lcom/squareup/BundleKey;)Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    :goto_0
    return-void
.end method

.method protected onExitScope()V
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Price Entry Screen Dismissed"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 110
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    invoke-static {v0, p1}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->access$100(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;Landroid/os/Bundle;)V

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    invoke-static {v1}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->access$200(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/ui/library/-$$Lambda$LS0OZPwSkqLtbWh_SJ65wDrB6q4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/library/-$$Lambda$LS0OZPwSkqLtbWh_SJ65wDrB6q4;-><init>(Lcom/squareup/ui/library/PriceEntryScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/configure/item/R$string;->add:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v0, Lcom/squareup/ui/library/-$$Lambda$PriceEntryScreen$Presenter$cZrL2_-wV4z5fjXd1Zo2cod-w54;

    invoke-direct {v0, p0}, Lcom/squareup/ui/library/-$$Lambda$PriceEntryScreen$Presenter$cZrL2_-wV4z5fjXd1Zo2cod-w54;-><init>(Lcom/squareup/ui/library/PriceEntryScreen$Presenter;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    .line 118
    new-instance p1, Lcom/squareup/ui/library/PriceEntryScreen$Presenter$MoneyEntryKeypadListener;

    invoke-direct {p1, p0}, Lcom/squareup/ui/library/PriceEntryScreen$Presenter$MoneyEntryKeypadListener;-><init>(Lcom/squareup/ui/library/PriceEntryScreen$Presenter;)V

    iput-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->listener:Lcom/squareup/padlock/MoneyKeypadListener;

    .line 119
    invoke-direct {p0}, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->displayAmount()V

    .line 120
    invoke-virtual {p0}, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/library/PriceEntryView;

    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->listener:Lcom/squareup/padlock/MoneyKeypadListener;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/library/PriceEntryView;->setListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    .line 121
    iget-object p1, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Price Entry Screen Shown"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    invoke-static {v0, p1}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->access$300(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;Landroid/os/Bundle;)V

    .line 126
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    return-void
.end method

.method onStartVisualTransition()V
    .locals 3

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_PRICE_ENTRY:Lcom/squareup/analytics/RegisterViewName;

    iget-object v2, p0, Lcom/squareup/ui/library/PriceEntryScreen$Presenter;->state:Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;

    invoke-static {v2}, Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;->access$400(Lcom/squareup/ui/library/PriceEntryScreen$WorkingItemState;)Lcom/squareup/configure/item/WorkingItem;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logItemEvent(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/configure/item/WorkingItem;)V

    return-void
.end method
